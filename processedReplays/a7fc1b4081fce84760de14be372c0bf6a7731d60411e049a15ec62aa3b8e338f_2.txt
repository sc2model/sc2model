----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3490
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3469
  player_apm: 166
}
game_duration_loops: 12906
game_duration_seconds: 576.200927734
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12206
score_details {
  idle_production_time: 7852.625
  idle_worker_time: 25.375
  total_value_units: 6750.0
  total_value_structures: 1775.0
  killed_value_units: 875.0
  killed_value_structures: 0.0
  collected_minerals: 9230.0
  collected_vespene: 2276.0
  collection_rate_minerals: 2323.0
  collection_rate_vespene: 671.0
  spent_minerals: 9125.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2100.0
    economy: 6050.0
    technology: 1225.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 100.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 6200.0
    technology: 1375.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 170.0
    shields: 195.0
    energy: 0.0
  }
  total_damage_taken {
    life: 580.364257812
    shields: 50.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 155
vespene: 526
food_cap: 114
food_used: 103
food_army: 51
food_workers: 52
idle_worker_count: 0
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 21
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 11
  }
}

score{
 score_type: Melee
score: 9192
score_details {
  idle_production_time: 11348.1875
  idle_worker_time: 103.6875
  total_value_units: 11200.0
  total_value_structures: 2275.0
  killed_value_units: 4150.0
  killed_value_structures: 0.0
  collected_minerals: 13765.0
  collected_vespene: 3352.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 335.0
  spent_minerals: 12650.0
  spent_vespene: 2975.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2975.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4775.0
    economy: 1700.0
    technology: 275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 25.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 625.0
    economy: 4700.0
    technology: 1100.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 100.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 5375.0
    economy: 7300.0
    technology: 1525.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 3175.0
    economy: 0.0
    technology: 300.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1934.73852539
    shields: 2752.83935547
    energy: 0.0
  }
  total_damage_taken {
    life: 13795.7207031
    shields: 50.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1165
vespene: 377
food_cap: 130
food_used: 53
food_army: 16
food_workers: 37
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  12906
ui_data{
 multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  9192
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
