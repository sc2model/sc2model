----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3015
  player_apm: 95
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2998
  player_apm: 71
}
game_duration_loops: 12947
game_duration_seconds: 578.031433105
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5165
score_details {
  idle_production_time: 3047.5625
  idle_worker_time: 6.625
  total_value_units: 4750.0
  total_value_structures: 875.0
  killed_value_units: 50.0
  killed_value_structures: 1350.0
  collected_minerals: 5670.0
  collected_vespene: 1400.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 111.0
  spent_minerals: 4180.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 1050.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2025.0
    economy: 187.0
    technology: 43.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 1575.0
    technology: 575.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2475.0
    economy: 2200.0
    technology: 725.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1793.76977539
    shields: 2283.39013672
    energy: 0.0
  }
  total_damage_taken {
    life: 5217.02148438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1540
vespene: 725
food_cap: 38
food_used: 25
food_army: 10
food_workers: 15
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 108
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 5143
score_details {
  idle_production_time: 4018.8125
  idle_worker_time: 6.625
  total_value_units: 6000.0
  total_value_structures: 1175.0
  killed_value_units: 50.0
  killed_value_structures: 2250.0
  collected_minerals: 7180.0
  collected_vespene: 1668.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 134.0
  spent_minerals: 6130.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 14.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 650.0
    technology: 1650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3450.0
    economy: 187.0
    technology: 43.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 525.0
    economy: 1875.0
    technology: 575.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 3450.0
    economy: 2550.0
    technology: 725.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2905.49560547
    shields: 3994.59619141
    energy: 0.0
  }
  total_damage_taken {
    life: 8256.44042969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1100
vespene: 443
food_cap: 44
food_used: 26
food_army: 12
food_workers: 14
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  12947
ui_data{
 multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  5143
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
