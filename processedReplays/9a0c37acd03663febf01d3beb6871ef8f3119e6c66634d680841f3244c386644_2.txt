----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3188
  player_apm: 135
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3209
  player_apm: 120
}
game_duration_loops: 8684
game_duration_seconds: 387.705627441
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7036
score_details {
  idle_production_time: 1397.4375
  idle_worker_time: 695.5
  total_value_units: 2575.0
  total_value_structures: 2900.0
  killed_value_units: 1150.0
  killed_value_structures: 350.0
  collected_minerals: 5765.0
  collected_vespene: 996.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 111.0
  spent_minerals: 5275.0
  spent_vespene: 725.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 975.0
    economy: 2800.0
    technology: 1800.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 2450.0
    technology: 1650.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4835.51464844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1209.06005859
    shields: 2236.43505859
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 540
vespene: 271
food_cap: 63
food_used: 39
food_army: 16
food_workers: 23
idle_worker_count: 2
army_count: 5
warp_gate_count: 0

game_loop:  8684
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 62
  count: 4
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  7036
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
