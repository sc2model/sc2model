----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4027
  player_apm: 149
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3897
  player_apm: 155
}
game_duration_loops: 13113
game_duration_seconds: 585.442626953
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7497
score_details {
  idle_production_time: 978.5
  idle_worker_time: 263.125
  total_value_units: 3725.0
  total_value_structures: 3050.0
  killed_value_units: 850.0
  killed_value_structures: 100.0
  collected_minerals: 6780.0
  collected_vespene: 1192.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 403.0
  spent_minerals: 6700.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1600.0
    economy: 3225.0
    technology: 1325.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 325.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 1525.0
    economy: 3825.0
    technology: 1325.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 300.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 592.0
    shields: 913.5
    energy: 0.0
  }
  total_damage_taken {
    life: 959.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 130
vespene: 42
food_cap: 62
food_used: 62
food_army: 32
food_workers: 28
idle_worker_count: 1
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
production {
  unit {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  build_queue {
    unit_type: 54
    build_progress: 0.758928537369
  }
  build_queue {
    unit_type: 54
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 10636
score_details {
  idle_production_time: 1585.9375
  idle_worker_time: 541.6875
  total_value_units: 6575.0
  total_value_structures: 3350.0
  killed_value_units: 4075.0
  killed_value_structures: 100.0
  collected_minerals: 9995.0
  collected_vespene: 2316.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 492.0
  spent_minerals: 9650.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 50.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2400.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1350.0
    economy: 550.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2500.0
    economy: 3975.0
    technology: 1275.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 275.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 3350.0
    economy: 4725.0
    technology: 1325.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 300.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 2364.66064453
    shields: 4010.54736328
    energy: 0.0
  }
  total_damage_taken {
    life: 2779.34765625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 809.889648438
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 395
vespene: 416
food_cap: 94
food_used: 91
food_army: 50
food_workers: 39
idle_worker_count: 1
army_count: 26
warp_gate_count: 0

game_loop:  13113
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 15
}
groups {
  control_group_index: 2
  leader_unit_type: 500
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 69
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 43
  }
}

Score:  10636
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
