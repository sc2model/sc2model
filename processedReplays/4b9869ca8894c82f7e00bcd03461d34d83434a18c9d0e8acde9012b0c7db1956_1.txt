----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3494
  player_apm: 204
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: -36400
  player_apm: 96
}
game_duration_loops: 15939
game_duration_seconds: 711.612182617
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11069
score_details {
  idle_production_time: 10370.9375
  idle_worker_time: 94.75
  total_value_units: 5850.0
  total_value_structures: 1925.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 9395.0
  collected_vespene: 1260.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 470.0
  spent_minerals: 7999.0
  spent_vespene: 912.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 37.0
    army: 50.0
    economy: 500.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 37.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4900.0
    technology: 1275.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 250.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 5450.0
    technology: 1425.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 52.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1100.98730469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1446
vespene: 348
food_cap: 90
food_used: 68
food_army: 28
food_workers: 40
idle_worker_count: 0
army_count: 36
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 12
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9692
score_details {
  idle_production_time: 29413.8125
  idle_worker_time: 94.75
  total_value_units: 14800.0
  total_value_structures: 2325.0
  killed_value_units: 3275.0
  killed_value_structures: 300.0
  collected_minerals: 15785.0
  collected_vespene: 4068.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 492.0
  spent_minerals: 15574.0
  spent_vespene: 3787.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2600.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 37.0
    army: 4825.0
    economy: 2625.0
    technology: 287.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 37.0
    army: 1625.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 4450.0
    technology: 1150.0
    upgrade: 950.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 250.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 0.0
    army: 8350.0
    economy: 7575.0
    technology: 1550.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 350.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 5254.87109375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14417.2890625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 261
vespene: 281
food_cap: 180
food_used: 43
food_army: 16
food_workers: 27
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  15939
ui_data{
 
Score:  9692
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
