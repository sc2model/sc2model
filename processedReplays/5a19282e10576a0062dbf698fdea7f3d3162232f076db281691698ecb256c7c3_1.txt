----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3476
  player_apm: 155
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3321
  player_apm: 111
}
game_duration_loops: 23608
game_duration_seconds: 1054.0020752
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10396
score_details {
  idle_production_time: 1111.375
  idle_worker_time: 61.125
  total_value_units: 5475.0
  total_value_structures: 3525.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 8160.0
  collected_vespene: 2136.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 559.0
  spent_minerals: 8125.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 175.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1950.0
    economy: 4950.0
    technology: 1175.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 1025.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2100.0
    economy: 4750.0
    technology: 1175.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 1125.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 741.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 488.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 85
vespene: 111
food_cap: 94
food_used: 84
food_army: 39
food_workers: 43
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 56
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 99
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 78
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20530
score_details {
  idle_production_time: 5995.5
  idle_worker_time: 2460.8125
  total_value_units: 17600.0
  total_value_structures: 7625.0
  killed_value_units: 6475.0
  killed_value_structures: 1475.0
  collected_minerals: 23570.0
  collected_vespene: 7260.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 470.0
  spent_minerals: 22450.0
  spent_vespene: 7075.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3800.0
    economy: 1450.0
    technology: 675.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6950.0
    economy: 1725.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2450.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 4050.0
    economy: 6825.0
    technology: 2250.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 100.0
    army: 2150.0
    economy: 0.0
    technology: 1000.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 100.0
    army: 9950.0
    economy: 8800.0
    technology: 2375.0
    upgrade: 1100.0
  }
  total_used_vespene {
    none: 100.0
    army: 3700.0
    economy: 0.0
    technology: 1100.0
    upgrade: 1100.0
  }
  total_damage_dealt {
    life: 12293.1347656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11298.5742188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1094.13769531
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1170
vespene: 185
food_cap: 200
food_used: 115
food_army: 68
food_workers: 46
idle_worker_count: 7
army_count: 32
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 48
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 32
  count: 3
}
multi {
  units {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
    add_on {
      unit_type: 132
      build_progress: 0.25357145071
    }
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 52
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 636
    shields: 0
    energy: 22
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1260
    shields: 0
    energy: 40
  }
}

score{
 score_type: Melee
score: 15396
score_details {
  idle_production_time: 10810.375
  idle_worker_time: 4834.0
  total_value_units: 21300.0
  total_value_structures: 7625.0
  killed_value_units: 11575.0
  killed_value_structures: 1875.0
  collected_minerals: 24560.0
  collected_vespene: 8136.0
  collection_rate_minerals: 223.0
  collection_rate_vespene: 313.0
  spent_minerals: 23400.0
  spent_vespene: 7675.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7325.0
    economy: 1550.0
    technology: 1075.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9850.0
    economy: 4450.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3675.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1950.0
    economy: 4250.0
    technology: 2250.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 100.0
    army: 1525.0
    economy: 0.0
    technology: 1000.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 100.0
    army: 11950.0
    economy: 9300.0
    technology: 2375.0
    upgrade: 1275.0
  }
  total_used_vespene {
    none: 100.0
    army: 5200.0
    economy: 0.0
    technology: 1100.0
    upgrade: 1275.0
  }
  total_damage_dealt {
    life: 18820.1347656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19306.0683594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1360.32763672
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1210
vespene: 461
food_cap: 182
food_used: 52
food_army: 33
food_workers: 19
idle_worker_count: 9
army_count: 8
warp_gate_count: 0

game_loop:  23608
ui_data{
 
Score:  15396
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
