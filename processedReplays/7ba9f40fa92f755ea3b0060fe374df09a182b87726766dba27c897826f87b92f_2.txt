----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2817
  player_apm: 54
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2833
  player_apm: 141
}
game_duration_loops: 14653
game_duration_seconds: 654.19744873
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11578
score_details {
  idle_production_time: 5310.0625
  idle_worker_time: 20.625
  total_value_units: 7650.0
  total_value_structures: 1400.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 9090.0
  collected_vespene: 1588.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 671.0
  spent_minerals: 8675.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 64.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2900.0
    economy: 5300.0
    technology: 1000.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2475.0
    economy: 5550.0
    technology: 1150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 870.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 388.392089844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 465
vespene: 63
food_cap: 172
food_used: 103
food_army: 64
food_workers: 39
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 22
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15433
score_details {
  idle_production_time: 13855.0625
  idle_worker_time: 20.625
  total_value_units: 13550.0
  total_value_structures: 1750.0
  killed_value_units: 5850.0
  killed_value_structures: 475.0
  collected_minerals: 14960.0
  collected_vespene: 4248.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 1007.0
  spent_minerals: 14050.0
  spent_vespene: 3900.0
  food_used {
    none: 0.0
    army: 84.5
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3350.0
    economy: 1175.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3200.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4100.0
    economy: 6250.0
    technology: 1000.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 200.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 6625.0
    economy: 6950.0
    technology: 1150.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 300.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 6964.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4908.15966797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 960
vespene: 348
food_cap: 194
food_used: 135
food_army: 84
food_workers: 51
idle_worker_count: 0
army_count: 33
warp_gate_count: 0

game_loop:  14653
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 43
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 153
    shields: 0
    energy: 31
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 86
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 153
    shields: 0
    energy: 85
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 153
    shields: 0
    energy: 74
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 126
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  15433
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
