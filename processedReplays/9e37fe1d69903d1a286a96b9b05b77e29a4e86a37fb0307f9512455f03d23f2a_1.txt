----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3533
  player_apm: 95
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3307
  player_apm: 86
}
game_duration_loops: 14447
game_duration_seconds: 645.000366211
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11704
score_details {
  idle_production_time: 1293.5
  idle_worker_time: 675.4375
  total_value_units: 5375.0
  total_value_structures: 4475.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 9690.0
  collected_vespene: 1876.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 447.0
  spent_minerals: 8512.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 150.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 4675.0
    technology: 2300.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 4825.0
    technology: 2150.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1598.65136719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 635.0
    shields: 1438.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1228
vespene: 501
food_cap: 110
food_used: 77
food_army: 24
food_workers: 53
idle_worker_count: 3
army_count: 9
warp_gate_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 63
  count: 1
}
cargo {
  unit {
    unit_type: 81
    player_relative: 1
    health: 80
    shields: 100
    energy: 0
    transport_slots_taken: 2
  }
  passengers {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  passengers {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  slots_available: 6
}

score{
 score_type: Melee
score: 15086
score_details {
  idle_production_time: 2988.4375
  idle_worker_time: 1605.25
  total_value_units: 11450.0
  total_value_structures: 5125.0
  killed_value_units: 7050.0
  killed_value_structures: 1625.0
  collected_minerals: 15640.0
  collected_vespene: 3308.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 447.0
  spent_minerals: 13712.0
  spent_vespene: 2600.0
  food_used {
    none: 0.0
    army: 61.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4000.0
    economy: 2700.0
    technology: 1275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3475.0
    economy: 300.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3375.0
    economy: 4725.0
    technology: 2300.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 6850.0
    economy: 5025.0
    technology: 2450.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 17867.578125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3707.5
    shields: 6133.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1978
vespene: 708
food_cap: 118
food_used: 113
food_army: 61
food_workers: 52
idle_worker_count: 3
army_count: 29
warp_gate_count: 8

game_loop:  14447
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 63
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 64
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 81
    shields: 42
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 71
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 116
    shields: 100
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 46
    shields: 60
    energy: 104
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 96
    shields: 12
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 28
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 66
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 136
    player_relative: 1
    health: 80
    shields: 100
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 25
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

Score:  15086
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
