----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4211
  player_apm: 152
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4235
  player_apm: 207
}
game_duration_loops: 23201
game_duration_seconds: 1035.83117676
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11541
score_details {
  idle_production_time: 980.875
  idle_worker_time: 361.0
  total_value_units: 5725.0
  total_value_structures: 4075.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 10020.0
  collected_vespene: 2296.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 649.0
  spent_minerals: 9600.0
  spent_vespene: 2100.0
  food_used {
    none: 0.0
    army: 49.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2325.0
    economy: 4600.0
    technology: 1750.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 625.0
    economy: 0.0
    technology: 525.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 2650.0
    economy: 4700.0
    technology: 1700.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 120.0
    shields: 205.0
    energy: 0.0
  }
  total_damage_taken {
    life: 774.75
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 33.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 470
vespene: 196
food_cap: 102
food_used: 95
food_army: 49
food_workers: 46
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 75
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 166
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 63
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12533
score_details {
  idle_production_time: 6286.3125
  idle_worker_time: 2243.3125
  total_value_units: 19375.0
  total_value_structures: 4475.0
  killed_value_units: 8975.0
  killed_value_structures: 2625.0
  collected_minerals: 21335.0
  collected_vespene: 5848.0
  collection_rate_minerals: 279.0
  collection_rate_vespene: 201.0
  spent_minerals: 21175.0
  spent_vespene: 4875.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5975.0
    economy: 2525.0
    technology: 1250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12050.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2125.0
    economy: 4850.0
    technology: 1750.0
    upgrade: 800.0
  }
  used_vespene {
    none: 50.0
    army: 400.0
    economy: 0.0
    technology: 525.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 50.0
    army: 13700.0
    economy: 5000.0
    technology: 1750.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 50.0
    army: 3375.0
    economy: 0.0
    technology: 525.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 12649.1865234
    shields: 15566.9394531
    energy: 0.0
  }
  total_damage_taken {
    life: 11978.1923828
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3062.26586914
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 210
vespene: 973
food_cap: 126
food_used: 85
food_army: 44
food_workers: 41
idle_worker_count: 11
army_count: 13
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
single {
  unit {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 5201
score_details {
  idle_production_time: 7790.5
  idle_worker_time: 3066.3125
  total_value_units: 20600.0
  total_value_structures: 4475.0
  killed_value_units: 11950.0
  killed_value_structures: 4575.0
  collected_minerals: 21450.0
  collected_vespene: 5876.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 21475.0
  spent_vespene: 4900.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7625.0
    economy: 4475.0
    technology: 2000.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14300.0
    economy: 3500.0
    technology: 1375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 475.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 325.0
    economy: 1550.0
    technology: 375.0
    upgrade: 800.0
  }
  used_vespene {
    none: 50.0
    army: 225.0
    economy: 0.0
    technology: 50.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 50.0
    army: 14775.0
    economy: 5000.0
    technology: 1750.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 50.0
    army: 3525.0
    economy: 0.0
    technology: 525.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 19481.1875
    shields: 24698.1894531
    energy: 0.0
  }
  total_damage_taken {
    life: 32164.3007812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4279.90332031
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 0
vespene: 976
food_cap: 87
food_used: 7
food_army: 7
food_workers: 0
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  23201
ui_data{
 
Score:  5201
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
