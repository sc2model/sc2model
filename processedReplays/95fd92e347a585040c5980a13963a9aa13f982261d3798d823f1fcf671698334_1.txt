----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 190
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2359
  player_apm: 26
}
game_duration_loops: 15304
game_duration_seconds: 683.261962891
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10281
score_details {
  idle_production_time: 12328.6875
  idle_worker_time: 44.8125
  total_value_units: 6650.0
  total_value_structures: 1400.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 8005.0
  collected_vespene: 1476.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 425.0
  spent_minerals: 7775.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2675.0
    economy: 4400.0
    technology: 950.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 250.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 4450.0
    technology: 1100.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 100.0
    shields: 60.0
    energy: 0.0
  }
  total_damage_taken {
    life: 551.572265625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 280
vespene: 251
food_cap: 84
food_used: 84
food_army: 47
food_workers: 37
idle_worker_count: 0
army_count: 60
warp_gate_count: 0
larva_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 2
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12701
score_details {
  idle_production_time: 39363.75
  idle_worker_time: 116.6875
  total_value_units: 13400.0
  total_value_structures: 1750.0
  killed_value_units: 2385.0
  killed_value_structures: 1050.0
  collected_minerals: 13915.0
  collected_vespene: 4036.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 851.0
  spent_minerals: 13300.0
  spent_vespene: 2800.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 585.0
    economy: 1750.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3875.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2425.0
    economy: 5600.0
    technology: 950.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 250.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 7400.0
    economy: 6500.0
    technology: 1100.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 3504.44335938
    shields: 6932.55664062
    energy: 0.0
  }
  total_damage_taken {
    life: 4648.30078125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 665
vespene: 1236
food_cap: 178
food_used: 85
food_army: 43
food_workers: 42
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  15304
ui_data{
 single {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  12701
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
