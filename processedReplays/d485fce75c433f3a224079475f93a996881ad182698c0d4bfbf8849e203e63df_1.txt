----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3328
  player_apm: 93
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3332
  player_apm: 82
}
game_duration_loops: 13777
game_duration_seconds: 615.087585449
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7537
score_details {
  idle_production_time: 1494.9375
  idle_worker_time: 580.875
  total_value_units: 4425.0
  total_value_structures: 3150.0
  killed_value_units: 1500.0
  killed_value_structures: 0.0
  collected_minerals: 6880.0
  collected_vespene: 1832.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 313.0
  spent_minerals: 6850.0
  spent_vespene: 1075.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 1350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 875.0
    economy: 3450.0
    technology: 1500.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 4700.0
    technology: 1200.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 619.125
    shields: 710.875
    energy: 0.0
  }
  total_damage_taken {
    life: 846.0
    shields: 1035.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 80
vespene: 757
food_cap: 86
food_used: 49
food_army: 16
food_workers: 31
idle_worker_count: 0
army_count: 5
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

score{
 score_type: Melee
score: 12393
score_details {
  idle_production_time: 2261.375
  idle_worker_time: 700.4375
  total_value_units: 8075.0
  total_value_structures: 4350.0
  killed_value_units: 4500.0
  killed_value_structures: 0.0
  collected_minerals: 10955.0
  collected_vespene: 3388.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 649.0
  spent_minerals: 10500.0
  spent_vespene: 2275.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2000.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2250.0
    economy: 4650.0
    technology: 1800.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 250.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 3050.0
    economy: 5700.0
    technology: 1800.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1815.0
    shields: 1480.875
    energy: 0.0
  }
  total_damage_taken {
    life: 1710.73291016
    shields: 2346.60791016
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 505
vespene: 1113
food_cap: 118
food_used: 79
food_army: 38
food_workers: 41
idle_worker_count: 1
army_count: 16
warp_gate_count: 4

game_loop:  13777
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 56
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 55
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 70
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 26
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 50
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
}

Score:  12393
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
