----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 174
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2681
  player_apm: 99
}
game_duration_loops: 20015
game_duration_seconds: 893.589111328
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5688
score_details {
  idle_production_time: 1345.8125
  idle_worker_time: 274.9375
  total_value_units: 3750.0
  total_value_structures: 2550.0
  killed_value_units: 1100.0
  killed_value_structures: 100.0
  collected_minerals: 5485.0
  collected_vespene: 1028.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 156.0
  spent_minerals: 5275.0
  spent_vespene: 800.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1550.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1125.0
    economy: 2300.0
    technology: 950.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 175.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2525.0
    economy: 2600.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1362.74707031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1552.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 260
vespene: 228
food_cap: 71
food_used: 41
food_army: 23
food_workers: 17
idle_worker_count: 1
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 1001
score_details {
  idle_production_time: 3564.5625
  idle_worker_time: 422.375
  total_value_units: 7100.0
  total_value_structures: 2975.0
  killed_value_units: 3600.0
  killed_value_structures: 100.0
  collected_minerals: 8340.0
  collected_vespene: 1836.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 7975.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 4800.0
    economy: 2700.0
    technology: 1275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 4850.0
    economy: 2850.0
    technology: 1275.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4433.44677734
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19202.3125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 556.681640625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 415
vespene: 386
food_cap: 0
food_used: 0
food_army: 0
food_workers: 0
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  20000
ui_data{
 
Score:  1001
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
