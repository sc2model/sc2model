----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2769
  player_apm: 46
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 114
}
game_duration_loops: 20382
game_duration_seconds: 909.974182129
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11928
score_details {
  idle_production_time: 11674.9375
  idle_worker_time: 4.1875
  total_value_units: 7500.0
  total_value_structures: 1700.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9890.0
  collected_vespene: 1088.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 649.0
  spent_minerals: 8800.0
  spent_vespene: 1025.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3275.0
    economy: 4975.0
    technology: 950.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 250.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 3650.0
    economy: 5150.0
    technology: 1100.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 40.0
    shields: 45.0
    energy: 0.0
  }
  total_damage_taken {
    life: 216.254882812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1140
vespene: 63
food_cap: 106
food_used: 102
food_army: 55
food_workers: 42
idle_worker_count: 0
army_count: 83
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
single {
  unit {
    unit_type: 90
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12327
score_details {
  idle_production_time: 42718.625
  idle_worker_time: 1382.3125
  total_value_units: 20000.0
  total_value_structures: 2675.0
  killed_value_units: 7275.0
  killed_value_structures: 0.0
  collected_minerals: 20795.0
  collected_vespene: 5332.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 694.0
  spent_minerals: 19725.0
  spent_vespene: 4850.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4200.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7225.0
    economy: 2850.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 5450.0
    technology: 1725.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 450.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 10050.0
    economy: 9050.0
    technology: 2525.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 750.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 4359.25
    shields: 5499.375
    energy: 0.0
  }
  total_damage_taken {
    life: 14599.3847656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1120
vespene: 482
food_cap: 108
food_used: 60
food_army: 12
food_workers: 48
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
single {
  unit {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
}

score{
 score_type: Melee
score: 10855
score_details {
  idle_production_time: 42990.1875
  idle_worker_time: 1746.9375
  total_value_units: 20000.0
  total_value_structures: 2675.0
  killed_value_units: 7275.0
  killed_value_structures: 0.0
  collected_minerals: 21065.0
  collected_vespene: 5440.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 649.0
  spent_minerals: 20025.0
  spent_vespene: 4850.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4200.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7575.0
    economy: 2625.0
    technology: 625.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 4475.0
    technology: 1600.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 450.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 10050.0
    economy: 9050.0
    technology: 2525.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 750.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 4411.25
    shields: 5499.375
    energy: 0.0
  }
  total_damage_taken {
    life: 15592.9394531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1090
vespene: 590
food_cap: 100
food_used: 49
food_army: 12
food_workers: 37
idle_worker_count: 37
army_count: 0
warp_gate_count: 0

game_loop:  20382
ui_data{
 
Score:  10855
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
