----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4199
  player_apm: 142
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4063
  player_apm: 159
}
game_duration_loops: 10579
game_duration_seconds: 472.309753418
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12693
score_details {
  idle_production_time: 933.1875
  idle_worker_time: 178.1875
  total_value_units: 5950.0
  total_value_structures: 3725.0
  killed_value_units: 1625.0
  killed_value_structures: 0.0
  collected_minerals: 9555.0
  collected_vespene: 2488.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 649.0
  spent_minerals: 9075.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2275.0
    economy: 5700.0
    technology: 1375.0
    upgrade: 350.0
  }
  used_vespene {
    none: 50.0
    army: 750.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 50.0
    army: 2500.0
    economy: 5500.0
    technology: 1025.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 888.39453125
    shields: 1347.60546875
    energy: 0.0
  }
  total_damage_taken {
    life: 693.295410156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 240.41796875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 530
vespene: 863
food_cap: 126
food_used: 99
food_army: 47
food_workers: 52
idle_worker_count: 1
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 500
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 26
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 186
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 108
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 109
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 82
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 124
    shields: 0
    energy: 81
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12663
score_details {
  idle_production_time: 1131.9375
  idle_worker_time: 214.375
  total_value_units: 6225.0
  total_value_structures: 4475.0
  killed_value_units: 3100.0
  killed_value_structures: 300.0
  collected_minerals: 10470.0
  collected_vespene: 2768.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 627.0
  spent_minerals: 10175.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1475.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2250.0
    economy: 5700.0
    technology: 1375.0
    upgrade: 350.0
  }
  used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 50.0
    army: 2750.0
    economy: 6000.0
    technology: 1225.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 50.0
    army: 875.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 2087.10839844
    shields: 2372.24853516
    energy: 0.0
  }
  total_damage_taken {
    life: 1627.17553711
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 815.799560547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 345
vespene: 1018
food_cap: 149
food_used: 99
food_army: 47
food_workers: 52
idle_worker_count: 1
army_count: 25
warp_gate_count: 0

game_loop:  10579
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 500
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 21
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 171
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 112
    shields: 0
    energy: 96
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 99
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 38
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 124
    shields: 0
    energy: 74
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 119
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
}

Score:  12663
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
