----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3843
  player_apm: 71
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3511
  player_apm: 146
}
game_duration_loops: 13618
game_duration_seconds: 607.988830566
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11697
score_details {
  idle_production_time: 1051.5625
  idle_worker_time: 228.6875
  total_value_units: 7575.0
  total_value_structures: 4150.0
  killed_value_units: 1550.0
  killed_value_structures: 0.0
  collected_minerals: 10445.0
  collected_vespene: 2924.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 671.0
  spent_minerals: 10050.0
  spent_vespene: 2675.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1459.0
    economy: 204.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 530.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 5600.0
    technology: 1100.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 650.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3150.0
    economy: 6100.0
    technology: 1100.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1727.58935547
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2618.78076172
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 158.86328125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 403
vespene: 244
food_cap: 134
food_used: 104
food_army: 46
food_workers: 58
idle_worker_count: 2
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 4
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 14784
score_details {
  idle_production_time: 2334.3125
  idle_worker_time: 617.0625
  total_value_units: 11475.0
  total_value_structures: 4150.0
  killed_value_units: 7275.0
  killed_value_structures: 425.0
  collected_minerals: 14715.0
  collected_vespene: 4672.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 627.0
  spent_minerals: 11950.0
  spent_vespene: 3375.0
  food_used {
    none: 0.0
    army: 50.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4400.0
    economy: 1775.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3314.0
    economy: 739.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1155.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2500.0
    economy: 5200.0
    technology: 1100.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 650.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 5750.0
    economy: 6200.0
    technology: 1100.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 2525.0
    economy: 0.0
    technology: 650.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 10235.4404297
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7062.0859375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1795.96899414
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2392
vespene: 1142
food_cap: 134
food_used: 100
food_army: 50
food_workers: 50
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  13618
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 4
}

Score:  14784
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
