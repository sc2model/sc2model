----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 111
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3122
  player_apm: 118
}
game_duration_loops: 7708
game_duration_seconds: 344.131164551
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1179
score_details {
  idle_production_time: 518.3125
  idle_worker_time: 1078.6875
  total_value_units: 2050.0
  total_value_structures: 1425.0
  killed_value_units: 2900.0
  killed_value_structures: 150.0
  collected_minerals: 2270.0
  collected_vespene: 384.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 2275.0
  spent_vespene: 350.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1250.0
    economy: 1650.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 1975.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 250.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 600.0
    economy: 2375.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4184.16601562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4997.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 45
vespene: 34
food_cap: 16
food_used: 5
food_army: 4
food_workers: 1
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  7708
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 49
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 49
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

Score:  1179
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
