----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4355
  player_apm: 193
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4048
  player_apm: 131
}
game_duration_loops: 13333
game_duration_seconds: 595.264770508
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11418
score_details {
  idle_production_time: 1109.75
  idle_worker_time: 202.9375
  total_value_units: 5550.0
  total_value_structures: 4300.0
  killed_value_units: 225.0
  killed_value_structures: 0.0
  collected_minerals: 9380.0
  collected_vespene: 1888.0
  collection_rate_minerals: 2771.0
  collection_rate_vespene: 470.0
  spent_minerals: 9175.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 625.0
    economy: 100.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 2000.0
    economy: 5250.0
    technology: 1600.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 650.0
    economy: 0.0
    technology: 400.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 2275.0
    economy: 5550.0
    technology: 1600.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 50.0
    army: 825.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 226.0
    shields: 611.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1106.62963867
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 32.6782226562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 255
vespene: 113
food_cap: 101
food_used: 92
food_army: 42
food_workers: 47
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11201
score_details {
  idle_production_time: 3106.3125
  idle_worker_time: 1335.9375
  total_value_units: 9600.0
  total_value_structures: 5600.0
  killed_value_units: 2425.0
  killed_value_structures: 100.0
  collected_minerals: 13735.0
  collected_vespene: 3332.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 67.0
  spent_minerals: 13725.0
  spent_vespene: 2575.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1550.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3675.0
    economy: 2350.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2100.0
    economy: 3950.0
    technology: 2050.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 825.0
    economy: 0.0
    technology: 500.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 5225.0
    economy: 6750.0
    technology: 2050.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 1475.0
    economy: 0.0
    technology: 500.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1558.84057617
    shields: 2873.28442383
    energy: 0.0
  }
  total_damage_taken {
    life: 11974.6953125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1580.66210938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 19
vespene: 757
food_cap: 141
food_used: 61
food_army: 42
food_workers: 19
idle_worker_count: 19
army_count: 17
warp_gate_count: 0

game_loop:  13333
ui_data{
 
Score:  11201
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
