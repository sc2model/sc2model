----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2955
  player_apm: 57
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2957
  player_apm: 79
}
game_duration_loops: 20669
game_duration_seconds: 922.787597656
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11966
score_details {
  idle_production_time: 3424.875
  idle_worker_time: 542.5
  total_value_units: 5150.0
  total_value_structures: 1350.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8820.0
  collected_vespene: 2496.0
  collection_rate_minerals: 2351.0
  collection_rate_vespene: 627.0
  spent_minerals: 7200.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1050.0
    economy: 5850.0
    technology: 850.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 450.0
    economy: 6050.0
    technology: 750.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 5.0
    energy: 0.0
  }
  total_damage_taken {
    life: 246.083984375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1670
vespene: 1846
food_cap: 90
food_used: 82
food_army: 22
food_workers: 60
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
production {
  unit {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 106
    build_progress: 0.0799999833107
  }
}

score{
 score_type: Melee
score: 23541
score_details {
  idle_production_time: 18259.625
  idle_worker_time: 1091.625
  total_value_units: 17900.0
  total_value_structures: 3350.0
  killed_value_units: 12200.0
  killed_value_structures: 750.0
  collected_minerals: 21835.0
  collected_vespene: 8856.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 649.0
  spent_minerals: 19875.0
  spent_vespene: 6325.0
  food_used {
    none: 0.0
    army: 123.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7900.0
    economy: 1850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4550.0
    economy: 1000.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5300.0
    economy: 7450.0
    technology: 950.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 300.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 9525.0
    economy: 9400.0
    technology: 1550.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 5675.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 8200.0
    shields: 8169.375
    energy: 0.0
  }
  total_damage_taken {
    life: 13059.8730469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2010
vespene: 2531
food_cap: 194
food_used: 168
food_army: 123
food_workers: 45
idle_worker_count: 6
army_count: 46
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 7
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 139
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 104
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23916
score_details {
  idle_production_time: 20952.0
  idle_worker_time: 1341.0
  total_value_units: 19250.0
  total_value_structures: 3350.0
  killed_value_units: 15175.0
  killed_value_structures: 1350.0
  collected_minerals: 22490.0
  collected_vespene: 9176.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 627.0
  spent_minerals: 19875.0
  spent_vespene: 6325.0
  food_used {
    none: 0.0
    army: 114.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9125.0
    economy: 3500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4925.0
    economy: 1000.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4925.0
    economy: 7450.0
    technology: 950.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 2625.0
    economy: 0.0
    technology: 300.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 10425.0
    economy: 9400.0
    technology: 1550.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 6125.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 10720.0
    shields: 11022.125
    energy: 0.0
  }
  total_damage_taken {
    life: 13678.0585938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2665
vespene: 2851
food_cap: 194
food_used: 159
food_army: 114
food_workers: 45
idle_worker_count: 6
army_count: 51
warp_gate_count: 0

game_loop:  20669
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 7
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 122
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 81
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  23916
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
