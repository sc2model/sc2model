----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 100
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 90
}
game_duration_loops: 11488
game_duration_seconds: 512.892944336
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13608
score_details {
  idle_production_time: 5930.875
  idle_worker_time: 57.25
  total_value_units: 7300.0
  total_value_structures: 1875.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 10530.0
  collected_vespene: 2128.0
  collection_rate_minerals: 2603.0
  collection_rate_vespene: 649.0
  spent_minerals: 8025.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 5550.0
    technology: 1075.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5800.0
    technology: 1225.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1271.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 190.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2555
vespene: 978
food_cap: 98
food_used: 98
food_army: 40
food_workers: 58
idle_worker_count: 0
army_count: 35
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17147
score_details {
  idle_production_time: 9962.75
  idle_worker_time: 57.25
  total_value_units: 7800.0
  total_value_structures: 2025.0
  killed_value_units: 1400.0
  killed_value_structures: 0.0
  collected_minerals: 13195.0
  collected_vespene: 2852.0
  collection_rate_minerals: 2295.0
  collection_rate_vespene: 649.0
  spent_minerals: 9325.0
  spent_vespene: 2450.0
  food_used {
    none: 0.0
    army: 66.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3600.0
    economy: 5450.0
    technology: 1325.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2350.0
    economy: 6100.0
    technology: 1475.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3506.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 358.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3920
vespene: 402
food_cap: 122
food_used: 122
food_army: 66
food_workers: 56
idle_worker_count: 0
army_count: 37
warp_gate_count: 0

game_loop:  11488
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 87
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
}

Score:  17147
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
