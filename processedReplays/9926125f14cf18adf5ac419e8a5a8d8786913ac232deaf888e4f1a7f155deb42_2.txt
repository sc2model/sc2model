----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2730
  player_apm: 60
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2855
  player_apm: 53
}
game_duration_loops: 7764
game_duration_seconds: 346.631317139
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6652
score_details {
  idle_production_time: 1180.5
  idle_worker_time: 454.6875
  total_value_units: 3550.0
  total_value_structures: 2600.0
  killed_value_units: 1575.0
  killed_value_structures: 900.0
  collected_minerals: 4670.0
  collected_vespene: 1432.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 313.0
  spent_minerals: 4600.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 475.0
    economy: 1550.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1475.0
    economy: 2300.0
    technology: 1200.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 2350.0
    technology: 1200.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3499.25
    shields: 3703.125
    energy: 0.0
  }
  total_damage_taken {
    life: 320.0
    shields: 257.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 120
vespene: 132
food_cap: 63
food_used: 53
food_army: 30
food_workers: 23
idle_worker_count: 2
army_count: 12
warp_gate_count: 5

game_loop:  7764
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 332
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 40
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 340
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 30
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 60
    energy: 0
  }
}

Score:  6652
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
