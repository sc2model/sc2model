----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4602
  player_apm: 261
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4552
  player_apm: 235
}
game_duration_loops: 13597
game_duration_seconds: 607.051269531
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11166
score_details {
  idle_production_time: 6039.1875
  idle_worker_time: 112.125
  total_value_units: 6900.0
  total_value_structures: 1800.0
  killed_value_units: 1000.0
  killed_value_structures: 0.0
  collected_minerals: 9210.0
  collected_vespene: 1548.0
  collection_rate_minerals: 2603.0
  collection_rate_vespene: 627.0
  spent_minerals: 8905.0
  spent_vespene: 1237.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 412.0
    economy: 0.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 12.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 1875.0
    economy: 5900.0
    technology: 1150.0
    upgrade: 600.0
  }
  used_vespene {
    none: -100.0
    army: 425.0
    economy: 0.0
    technology: 200.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 6250.0
    technology: 1300.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2248.42285156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1099.52246094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 355
vespene: 311
food_cap: 106
food_used: 102
food_army: 42
food_workers: 60
idle_worker_count: 0
army_count: 10
warp_gate_count: 0
larva_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 17
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1452
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17799
score_details {
  idle_production_time: 9164.75
  idle_worker_time: 114.0
  total_value_units: 13550.0
  total_value_structures: 2150.0
  killed_value_units: 7175.0
  killed_value_structures: 0.0
  collected_minerals: 16215.0
  collected_vespene: 3776.0
  collection_rate_minerals: 2687.0
  collection_rate_vespene: 940.0
  spent_minerals: 14930.0
  spent_vespene: 3312.0
  food_used {
    none: 0.0
    army: 113.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4625.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2262.0
    economy: 0.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 762.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 4525.0
    economy: 7200.0
    technology: 1150.0
    upgrade: 825.0
  }
  used_vespene {
    none: -100.0
    army: 1525.0
    economy: 0.0
    technology: 200.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 0.0
    army: 6325.0
    economy: 7950.0
    technology: 1300.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 2525.0
    economy: 0.0
    technology: 300.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 11349.0644531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5846.56201172
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1335
vespene: 464
food_cap: 176
food_used: 178
food_army: 113
food_workers: 65
idle_worker_count: 0
army_count: 42
warp_gate_count: 0

game_loop:  13597
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 48
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 59
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 136
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 93
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 85
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  17799
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
