----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3833
  player_apm: 114
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3919
  player_apm: 166
}
game_duration_loops: 15104
game_duration_seconds: 674.332763672
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4522
score_details {
  idle_production_time: 1650.6875
  idle_worker_time: 527.8125
  total_value_units: 3425.0
  total_value_structures: 2475.0
  killed_value_units: 2300.0
  killed_value_structures: 350.0
  collected_minerals: 5415.0
  collected_vespene: 1056.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 156.0
  spent_minerals: 5087.0
  spent_vespene: 937.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1300.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 1349.0
    technology: 307.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 91.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 625.0
    economy: 2450.0
    technology: 600.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1050.0
    economy: 3650.0
    technology: 900.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4022.1640625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8211.58886719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2264.58886719
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 182
vespene: 115
food_cap: 38
food_used: 30
food_army: 12
food_workers: 16
idle_worker_count: 1
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 55
  count: 2
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1230
    shields: 0
    energy: 53
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1365
    shields: 0
    energy: 89
  }
}

score{
 score_type: Melee
score: 10231
score_details {
  idle_production_time: 2740.375
  idle_worker_time: 1056.8125
  total_value_units: 7000.0
  total_value_structures: 4350.0
  killed_value_units: 4550.0
  killed_value_structures: 725.0
  collected_minerals: 10405.0
  collected_vespene: 2244.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 537.0
  spent_minerals: 9137.0
  spent_vespene: 1837.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2650.0
    economy: 950.0
    technology: 375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1100.0
    economy: 1499.0
    technology: 307.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 91.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1375.0
    economy: 4700.0
    technology: 1150.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 2475.0
    economy: 6350.0
    technology: 1400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 1175.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 7883.93896484
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9384.70898438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2663.09570312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1069
vespene: 362
food_cap: 118
food_used: 72
food_army: 28
food_workers: 44
idle_worker_count: 1
army_count: 15
warp_gate_count: 0

game_loop:  15104
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 55
  count: 15
}
multi {
  units {
    unit_type: 55
    player_relative: 1
    health: 57
    shields: 0
    energy: 200
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 55
    player_relative: 1
    health: 140
    shields: 0
    energy: 200
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 69
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 142
    shields: 0
    energy: 47
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 42
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  10231
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
