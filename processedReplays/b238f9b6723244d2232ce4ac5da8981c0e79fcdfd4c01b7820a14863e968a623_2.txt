----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2791
  player_apm: 47
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2768
  player_apm: 36
}
game_duration_loops: 6100
game_duration_seconds: 272.340423584
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3467
score_details {
  idle_production_time: 1917.6875
  idle_worker_time: 1.6875
  total_value_units: 1950.0
  total_value_structures: 1050.0
  killed_value_units: 525.0
  killed_value_structures: 0.0
  collected_minerals: 2920.0
  collected_vespene: 672.0
  collection_rate_minerals: 643.0
  collection_rate_vespene: 134.0
  spent_minerals: 2325.0
  spent_vespene: 350.0
  food_used {
    none: 0.0
    army: 3.0
    economy: 3.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 475.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 150.0
    economy: 950.0
    technology: 950.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 400.0
    economy: 2050.0
    technology: 1100.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 806.406738281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2209.45214844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 645
vespene: 322
food_cap: 30
food_used: 6
food_army: 3
food_workers: 3
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  6100
ui_data{
 
Score:  3467
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
