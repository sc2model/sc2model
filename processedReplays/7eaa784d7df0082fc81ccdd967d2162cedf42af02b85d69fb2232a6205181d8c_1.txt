----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4193
  player_apm: 152
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4134
  player_apm: 201
}
game_duration_loops: 10421
game_duration_seconds: 465.25567627
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8684
score_details {
  idle_production_time: 1013.125
  idle_worker_time: 686.375
  total_value_units: 3575.0
  total_value_structures: 2675.0
  killed_value_units: 875.0
  killed_value_structures: 0.0
  collected_minerals: 6850.0
  collected_vespene: 2060.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 313.0
  spent_minerals: 5300.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: -75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1075.0
    economy: 3400.0
    technology: 550.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 450.0
    economy: 0.0
    technology: 275.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 1625.0
    economy: 3700.0
    technology: 550.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 600.0
    economy: 0.0
    technology: 275.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1654.99755859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 995.143066406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 105.928222656
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1599
vespene: 785
food_cap: 94
food_used: 51
food_army: 24
food_workers: 27
idle_worker_count: 3
army_count: 11
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 102
    shields: 0
    energy: 162
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8665
score_details {
  idle_production_time: 1122.8125
  idle_worker_time: 823.125
  total_value_units: 4025.0
  total_value_structures: 2675.0
  killed_value_units: 1200.0
  killed_value_structures: 0.0
  collected_minerals: 7210.0
  collected_vespene: 2156.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 335.0
  spent_minerals: 5300.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1150.0
    economy: -75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 725.0
    economy: 3400.0
    technology: 550.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 325.0
    economy: 0.0
    technology: 275.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 1925.0
    economy: 3700.0
    technology: 550.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 750.0
    economy: 0.0
    technology: 275.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2152.70947266
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1413.14306641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 105.928222656
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1959
vespene: 881
food_cap: 94
food_used: 44
food_army: 17
food_workers: 27
idle_worker_count: 28
army_count: 9
warp_gate_count: 0

game_loop:  10421
ui_data{
 
Score:  8665
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
