----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3334
  player_apm: 89
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3313
  player_apm: 102
}
game_duration_loops: 25345
game_duration_seconds: 1131.55212402
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11001
score_details {
  idle_production_time: 4475.0625
  idle_worker_time: 4.125
  total_value_units: 5600.0
  total_value_structures: 1875.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 7410.0
  collected_vespene: 2952.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 649.0
  spent_minerals: 7099.0
  spent_vespene: 2137.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12.0
    economy: -38.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 12.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 3900.0
    technology: 1875.0
    upgrade: 350.0
  }
  used_vespene {
    none: 50.0
    army: 1225.0
    economy: 0.0
    technology: 400.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 300.0
    army: 2075.0
    economy: 4800.0
    technology: 1525.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 300.0
    army: 1925.0
    economy: 0.0
    technology: 500.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 144.51171875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 458.290039062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 361
vespene: 815
food_cap: 76
food_used: 72
food_army: 32
food_workers: 40
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 110
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 503
  count: 6
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15259
score_details {
  idle_production_time: 17757.1875
  idle_worker_time: 213.1875
  total_value_units: 15400.0
  total_value_structures: 3675.0
  killed_value_units: 3175.0
  killed_value_structures: 0.0
  collected_minerals: 16985.0
  collected_vespene: 7460.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 313.0
  spent_minerals: 16049.0
  spent_vespene: 6637.0
  food_used {
    none: 0.0
    army: 24.5
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4762.0
    economy: 237.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2562.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1050.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1025.0
    economy: 5000.0
    technology: 3125.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 50.0
    army: 1450.0
    economy: 0.0
    technology: 700.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 300.0
    army: 8675.0
    economy: 6900.0
    technology: 3675.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 300.0
    army: 5425.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 5399.06201172
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9437.74609375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 986
vespene: 823
food_cap: 132
food_used: 64
food_army: 24
food_workers: 40
idle_worker_count: 18
army_count: 16
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 119
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 127
  count: 9
}
multi {
  units {
    unit_type: 119
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 119
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 119
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 119
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 119
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13899
score_details {
  idle_production_time: 24741.3125
  idle_worker_time: 4483.6875
  total_value_units: 16600.0
  total_value_structures: 4450.0
  killed_value_units: 4800.0
  killed_value_structures: 0.0
  collected_minerals: 19085.0
  collected_vespene: 8000.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 0.0
  spent_minerals: 17949.0
  spent_vespene: 7237.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2600.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6137.0
    economy: -88.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4062.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1050.0
    economy: 1550.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 4550.0
    technology: 3750.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 50.0
    army: 350.0
    economy: 0.0
    technology: 700.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 300.0
    army: 9475.0
    economy: 6900.0
    technology: 4500.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 300.0
    army: 5825.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 7597.22070312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16086.0234375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1186
vespene: 763
food_cap: 124
food_used: 47
food_army: 16
food_workers: 31
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  25345
ui_data{
 
Score:  13899
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
