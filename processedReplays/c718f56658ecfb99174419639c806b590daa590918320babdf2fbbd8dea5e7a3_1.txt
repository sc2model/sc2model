----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3681
  player_apm: 95
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3372
  player_apm: 89
}
game_duration_loops: 15911
game_duration_seconds: 710.362060547
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11669
score_details {
  idle_production_time: 1777.9375
  idle_worker_time: 437.25
  total_value_units: 4850.0
  total_value_structures: 4150.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 8725.0
  collected_vespene: 2644.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 694.0
  spent_minerals: 7800.0
  spent_vespene: 2200.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 4550.0
    technology: 1650.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1850.0
    economy: 4500.0
    technology: 1650.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 477.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 158.0
    shields: 560.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 975
vespene: 444
food_cap: 102
food_used: 79
food_army: 36
food_workers: 43
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 10
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 2
}
multi {
  units {
    unit_type: 10
    player_relative: 1
    health: 350
    shields: 350
    energy: 200
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 108
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 116
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 67
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 108
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 65
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 65
  }
}

score{
 score_type: Melee
score: 18753
score_details {
  idle_production_time: 4636.625
  idle_worker_time: 793.125
  total_value_units: 11450.0
  total_value_structures: 8675.0
  killed_value_units: 4800.0
  killed_value_structures: 550.0
  collected_minerals: 15855.0
  collected_vespene: 5648.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 694.0
  spent_minerals: 15525.0
  spent_vespene: 4400.0
  food_used {
    none: 0.0
    army: 71.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3250.0
    economy: 1250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 1200.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3550.0
    economy: 6725.0
    technology: 3600.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 2450.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 5150.0
    economy: 7875.0
    technology: 3600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 3900.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 6154.90283203
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1664.0
    shields: 2761.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 380
vespene: 1248
food_cap: 200
food_used: 120
food_army: 71
food_workers: 48
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  15911
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 10
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 4
}
single {
  unit {
    unit_type: 10
    player_relative: 1
    health: 338
    shields: 0
    energy: 200
  }
}

Score:  18753
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
