----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3853
  player_apm: 151
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3952
  player_apm: 132
}
game_duration_loops: 7069
game_duration_seconds: 315.602386475
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3810
score_details {
  idle_production_time: 774.5
  idle_worker_time: 24.9375
  total_value_units: 1950.0
  total_value_structures: 1400.0
  killed_value_units: 2475.0
  killed_value_structures: 900.0
  collected_minerals: 2990.0
  collected_vespene: 620.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 134.0
  spent_minerals: 2875.0
  spent_vespene: 225.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 14.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 2100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 1525.0
    technology: 600.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 125.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 1675.0
    technology: 600.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 125.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 2865.83300781
    shields: 3351.4831543
    energy: 0.0
  }
  total_damage_taken {
    life: 549.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 165
vespene: 395
food_cap: 31
food_used: 31
food_army: 17
food_workers: 14
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  7069
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 51
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 51
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 104
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 5
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
}

Score:  3810
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
