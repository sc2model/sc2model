----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3061
  player_apm: 0
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3128
  player_apm: 293
}
game_duration_loops: 40
game_duration_seconds: 1.78583884239
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1050
score_details {
  idle_production_time: 1.4375
  idle_worker_time: 0.0
  total_value_units: 600.0
  total_value_structures: 400.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 0.0
  collected_vespene: 0.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 50.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 0
vespene: 0
food_cap: 15
food_used: 13
food_army: 0
food_workers: 12
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  40
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
production {
  unit {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  build_queue {
    unit_type: 84
    build_progress: 0.0718688964844
  }
}

Score:  1050
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
