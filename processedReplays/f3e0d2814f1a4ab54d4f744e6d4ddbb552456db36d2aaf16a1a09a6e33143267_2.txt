----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2622
  player_apm: 51
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2344
  player_apm: 30
}
game_duration_loops: 8599
game_duration_seconds: 383.910705566
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5035
score_details {
  idle_production_time: 1210.6875
  idle_worker_time: 1079.5625
  total_value_units: 1850.0
  total_value_structures: 2825.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 4225.0
  collected_vespene: 1284.0
  collection_rate_minerals: 55.0
  collection_rate_vespene: 134.0
  spent_minerals: 3875.0
  spent_vespene: 250.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 540.0
    technology: 503.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 2375.0
    technology: 800.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 650.0
    economy: 3025.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 580.0
    shields: 456.375
    energy: 0.0
  }
  total_damage_taken {
    life: 4964.89453125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1291.20825195
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 276
vespene: 1034
food_cap: 54
food_used: 22
food_army: 4
food_workers: 18
idle_worker_count: 14
army_count: 4
warp_gate_count: 0

game_loop:  8599
ui_data{
 
Score:  5035
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
