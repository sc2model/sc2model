----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3428
  player_apm: 166
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3392
  player_apm: 69
}
game_duration_loops: 15353
game_duration_seconds: 685.449645996
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12389
score_details {
  idle_production_time: 9394.0625
  idle_worker_time: 3.25
  total_value_units: 6350.0
  total_value_structures: 2175.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 10075.0
  collected_vespene: 1820.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 783.0
  spent_minerals: 8756.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 131.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2100.0
    economy: 5225.0
    technology: 1775.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 25.0
    technology: 500.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1600.0
    economy: 6150.0
    technology: 2225.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 800.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 165.0
    shields: 1415.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1064.40380859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1369
vespene: 445
food_cap: 90
food_used: 90
food_army: 36
food_workers: 54
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 3
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18283
score_details {
  idle_production_time: 20003.25
  idle_worker_time: 241.625
  total_value_units: 17450.0
  total_value_structures: 3650.0
  killed_value_units: 2560.0
  killed_value_structures: 0.0
  collected_minerals: 19525.0
  collected_vespene: 5364.0
  collection_rate_minerals: 2295.0
  collection_rate_vespene: 1142.0
  spent_minerals: 17981.0
  spent_vespene: 4725.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1060.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4000.0
    economy: 481.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1625.0
    economy: 8875.0
    technology: 2750.0
    upgrade: 625.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 25.0
    technology: 950.0
    upgrade: 625.0
  }
  total_used_minerals {
    none: 0.0
    army: 6325.0
    economy: 10700.0
    technology: 3400.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 3225.0
    economy: 50.0
    technology: 1400.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1279.0
    shields: 2908.66455078
    energy: 0.0
  }
  total_damage_taken {
    life: 7038.03222656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1594
vespene: 639
food_cap: 200
food_used: 94
food_army: 28
food_workers: 66
idle_worker_count: 66
army_count: 12
warp_gate_count: 0

game_loop:  15353
ui_data{
 
Score:  18283
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
