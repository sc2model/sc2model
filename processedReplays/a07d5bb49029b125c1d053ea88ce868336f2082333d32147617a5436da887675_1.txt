----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3729
  player_apm: 117
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3839
  player_apm: 184
}
game_duration_loops: 24961
game_duration_seconds: 1114.40808105
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8752
score_details {
  idle_production_time: 1479.25
  idle_worker_time: 354.5625
  total_value_units: 3775.0
  total_value_structures: 4450.0
  killed_value_units: 1050.0
  killed_value_structures: 475.0
  collected_minerals: 7400.0
  collected_vespene: 2252.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 649.0
  spent_minerals: 7000.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 750.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 375.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 500.0
    economy: 3850.0
    technology: 2150.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 850.0
    economy: 4400.0
    technology: 2000.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 2901.89575195
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 503.0
    shields: 1007.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 450
vespene: 727
food_cap: 102
food_used: 46
food_army: 9
food_workers: 37
idle_worker_count: 9
army_count: 4
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 20955
score_details {
  idle_production_time: 5041.3125
  idle_worker_time: 1536.9375
  total_value_units: 17600.0
  total_value_structures: 6800.0
  killed_value_units: 11025.0
  killed_value_structures: 1525.0
  collected_minerals: 19845.0
  collected_vespene: 7860.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 671.0
  spent_minerals: 18975.0
  spent_vespene: 7375.0
  food_used {
    none: 0.0
    army: 85.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6800.0
    economy: 2900.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2375.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3225.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4475.0
    economy: 6150.0
    technology: 3200.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 3475.0
    economy: 0.0
    technology: 550.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 8775.0
    economy: 6600.0
    technology: 3200.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 7075.0
    economy: 0.0
    technology: 550.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 15891.0390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3248.5
    shields: 4665.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 920
vespene: 485
food_cap: 141
food_used: 141
food_army: 85
food_workers: 56
idle_worker_count: 7
army_count: 33
warp_gate_count: 11

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 75
  count: 31
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 5
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 21045
score_details {
  idle_production_time: 7769.1875
  idle_worker_time: 2034.9375
  total_value_units: 23550.0
  total_value_structures: 8400.0
  killed_value_units: 18250.0
  killed_value_structures: 6325.0
  collected_minerals: 24244.0
  collected_vespene: 9876.0
  collection_rate_minerals: 985.0
  collection_rate_vespene: 335.0
  spent_minerals: 23200.0
  spent_vespene: 9450.0
  food_used {
    none: 0.0
    army: 106.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11300.0
    economy: 6375.0
    technology: 2200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4150.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5475.0
    economy: 3800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5850.0
    economy: 4650.0
    technology: 3200.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 550.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 12400.0
    economy: 8450.0
    technology: 3200.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 9150.0
    economy: 0.0
    technology: 550.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 43339.109375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7985.5
    shields: 12423.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1094
vespene: 426
food_cap: 189
food_used: 130
food_army: 106
food_workers: 24
idle_worker_count: 1
army_count: 48
warp_gate_count: 11

game_loop:  24961
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 75
  count: 30
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}

Score:  21045
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
