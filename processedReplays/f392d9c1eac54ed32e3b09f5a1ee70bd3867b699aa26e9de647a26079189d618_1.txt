----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4343
  player_apm: 221
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4424
  player_apm: 215
}
game_duration_loops: 16723
game_duration_seconds: 746.614624023
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13020
score_details {
  idle_production_time: 10100.25
  idle_worker_time: 23.5
  total_value_units: 8550.0
  total_value_structures: 1800.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 10995.0
  collected_vespene: 1800.0
  collection_rate_minerals: 2435.0
  collection_rate_vespene: 403.0
  spent_minerals: 9525.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 60.5
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2450.0
    economy: 5550.0
    technology: 1250.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 3175.0
    economy: 6550.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2059.33056641
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1515.28369141
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1520
vespene: 125
food_cap: 138
food_used: 114
food_army: 60
food_workers: 54
idle_worker_count: 0
army_count: 33
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 23
}
groups {
  control_group_index: 3
  leader_unit_type: 110
  count: 32
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 24268
score_details {
  idle_production_time: 16206.4375
  idle_worker_time: 27.75
  total_value_units: 20950.0
  total_value_structures: 3550.0
  killed_value_units: 11100.0
  killed_value_structures: 0.0
  collected_minerals: 24140.0
  collected_vespene: 6728.0
  collection_rate_minerals: 2967.0
  collection_rate_vespene: 1343.0
  spent_minerals: 23050.0
  spent_vespene: 6000.0
  food_used {
    none: 0.0
    army: 105.0
    economy: 79.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8575.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5300.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4775.0
    economy: 9100.0
    technology: 3050.0
    upgrade: 1225.0
  }
  used_vespene {
    none: 150.0
    army: 2225.0
    economy: 0.0
    technology: 500.0
    upgrade: 1225.0
  }
  total_used_minerals {
    none: 300.0
    army: 10375.0
    economy: 10750.0
    technology: 2700.0
    upgrade: 825.0
  }
  total_used_vespene {
    none: 300.0
    army: 5175.0
    economy: 0.0
    technology: 450.0
    upgrade: 825.0
  }
  total_damage_dealt {
    life: 18228.7617188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12185.9746094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1140
vespene: 728
food_cap: 200
food_used: 184
food_army: 105
food_workers: 79
idle_worker_count: 0
army_count: 48
warp_gate_count: 0

game_loop:  16723
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 36
}
groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 110
  count: 47
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 106
  count: 1
}

Score:  24268
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
