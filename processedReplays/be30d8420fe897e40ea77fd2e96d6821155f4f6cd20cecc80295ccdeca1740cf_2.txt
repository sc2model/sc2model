----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 6227
  player_apm: 320
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5239
  player_apm: 354
}
game_duration_loops: 19219
game_duration_seconds: 858.050964355
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11977
score_details {
  idle_production_time: 1050.75
  idle_worker_time: 117.8125
  total_value_units: 5700.0
  total_value_structures: 3725.0
  killed_value_units: 200.0
  killed_value_structures: 300.0
  collected_minerals: 9085.0
  collected_vespene: 2292.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 671.0
  spent_minerals: 8675.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2500.0
    economy: 5050.0
    technology: 1275.0
    upgrade: 350.0
  }
  used_vespene {
    none: 100.0
    army: 850.0
    economy: 0.0
    technology: 300.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 100.0
    army: 2550.0
    economy: 4950.0
    technology: 1125.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 100.0
    army: 900.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 708.0
    shields: 1009.10009766
    energy: 0.0
  }
  total_damage_taken {
    life: 369.125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 50.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 460
vespene: 642
food_cap: 110
food_used: 98
food_army: 53
food_workers: 45
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18694
score_details {
  idle_production_time: 5892.8125
  idle_worker_time: 712.875
  total_value_units: 15950.0
  total_value_structures: 6875.0
  killed_value_units: 14250.0
  killed_value_structures: 900.0
  collected_minerals: 21445.0
  collected_vespene: 6456.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 627.0
  spent_minerals: 20000.0
  spent_vespene: 4600.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9725.0
    economy: 1300.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3825.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7686.0
    economy: 200.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2186.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 3375.0
    economy: 6300.0
    technology: 2500.0
    upgrade: 800.0
  }
  used_vespene {
    none: 100.0
    army: 800.0
    economy: 0.0
    technology: 750.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 100.0
    army: 10650.0
    economy: 6950.0
    technology: 2550.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 100.0
    army: 2600.0
    economy: 0.0
    technology: 775.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 10083.0615234
    shields: 12282.0380859
    energy: 0.0
  }
  total_damage_taken {
    life: 12367.3515625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5923.24121094
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1404
vespene: 1765
food_cap: 172
food_used: 118
food_army: 68
food_workers: 50
idle_worker_count: 5
army_count: 47
warp_gate_count: 0

game_loop:  19219
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 51
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 7
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 28
  count: 1
}

Score:  18694
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
