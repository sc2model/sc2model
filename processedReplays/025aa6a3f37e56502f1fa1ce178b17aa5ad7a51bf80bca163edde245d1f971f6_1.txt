----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3886
  player_apm: 226
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3862
  player_apm: 270
}
game_duration_loops: 15406
game_duration_seconds: 687.815856934
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11222
score_details {
  idle_production_time: 1198.4375
  idle_worker_time: 268.25
  total_value_units: 5125.0
  total_value_structures: 4125.0
  killed_value_units: 1500.0
  killed_value_structures: 0.0
  collected_minerals: 8900.0
  collected_vespene: 1860.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 447.0
  spent_minerals: 8681.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 50.0
    technology: 6.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2325.0
    economy: 4675.0
    technology: 1750.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2275.0
    economy: 4875.0
    technology: 1425.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 352.0
    shields: 461.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1051.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 17.7788085938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 262
vespene: 235
food_cap: 109
food_used: 87
food_army: 47
food_workers: 40
idle_worker_count: 1
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17251
score_details {
  idle_production_time: 4916.25
  idle_worker_time: 742.1875
  total_value_units: 11175.0
  total_value_structures: 6775.0
  killed_value_units: 7400.0
  killed_value_structures: 400.0
  collected_minerals: 16830.0
  collected_vespene: 4772.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 873.0
  spent_minerals: 16081.0
  spent_vespene: 3525.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4450.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3496.0
    economy: 150.0
    technology: 31.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 996.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 3275.0
    economy: 6000.0
    technology: 2800.0
    upgrade: 625.0
  }
  used_vespene {
    none: 50.0
    army: 1075.0
    economy: 0.0
    technology: 725.0
    upgrade: 625.0
  }
  total_used_minerals {
    none: 50.0
    army: 6450.0
    economy: 6600.0
    technology: 3000.0
    upgrade: 625.0
  }
  total_used_vespene {
    none: 50.0
    army: 1975.0
    economy: 0.0
    technology: 725.0
    upgrade: 625.0
  }
  total_damage_dealt {
    life: 3577.33203125
    shields: 4738.76953125
    energy: 0.0
  }
  total_damage_taken {
    life: 4964.40917969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1166.00048828
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 759
vespene: 1217
food_cap: 157
food_used: 120
food_army: 68
food_workers: 52
idle_worker_count: 2
army_count: 38
warp_gate_count: 0

game_loop:  15406
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 37
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}

Score:  17251
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
