----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 109
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3582
  player_apm: 102
}
game_duration_loops: 19423
game_duration_seconds: 867.158752441
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11572
score_details {
  idle_production_time: 4687.0625
  idle_worker_time: 44.625
  total_value_units: 6450.0
  total_value_structures: 2475.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 8980.0
  collected_vespene: 1792.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 627.0
  spent_minerals: 8500.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1475.0
    economy: 5100.0
    technology: 2025.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 1625.0
    economy: 6000.0
    technology: 2025.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1317.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 865.674804688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 530
vespene: 367
food_cap: 98
food_used: 87
food_army: 34
food_workers: 53
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 124
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 130
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 95
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23500
score_details {
  idle_production_time: 19578.3125
  idle_worker_time: 2110.625
  total_value_units: 17450.0
  total_value_structures: 3275.0
  killed_value_units: 11425.0
  killed_value_structures: 1525.0
  collected_minerals: 21280.0
  collected_vespene: 6932.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 963.0
  spent_minerals: 17962.0
  spent_vespene: 4925.0
  food_used {
    none: 0.0
    army: 74.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6250.0
    economy: 4000.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2375.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3725.0
    economy: 650.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3300.0
    economy: 7450.0
    technology: 2525.0
    upgrade: 1325.0
  }
  used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 400.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 0.0
    army: 7125.0
    economy: 9450.0
    technology: 2675.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 500.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 18582.7167969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8229.1484375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3368
vespene: 2007
food_cap: 200
food_used: 135
food_army: 74
food_workers: 61
idle_worker_count: 13
army_count: 35
warp_gate_count: 0

game_loop:  19423
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 22
}
groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 86
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 109
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 59
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  23500
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
