----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3771
  player_apm: 215
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: -36400
  player_apm: 144
}
game_duration_loops: 9107
game_duration_seconds: 406.590881348
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5023
score_details {
  idle_production_time: 1362.3125
  idle_worker_time: 600.6875
  total_value_units: 2950.0
  total_value_structures: 3150.0
  killed_value_units: 650.0
  killed_value_structures: 75.0
  collected_minerals: 5580.0
  collected_vespene: 608.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 0.0
  spent_minerals: 5350.0
  spent_vespene: 550.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 602.0
    economy: 1496.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 300.0
    economy: 2925.0
    technology: 950.0
    upgrade: 0.0
  }
  used_vespene {
    none: 50.0
    army: 100.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 750.0
    economy: 4700.0
    technology: 950.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 150.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1645.11914062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5639.86230469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1371.87695312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 93
vespene: 55
food_cap: 69
food_used: 21
food_army: 6
food_workers: 15
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  9107
ui_data{
 
Score:  5023
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
