----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4798
  player_apm: 302
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4960
  player_apm: 236
}
game_duration_loops: 8924
game_duration_seconds: 398.420654297
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3473
score_details {
  idle_production_time: 722.75
  idle_worker_time: 349.75
  total_value_units: 4100.0
  total_value_structures: 2275.0
  killed_value_units: 2525.0
  killed_value_structures: 0.0
  collected_minerals: 5775.0
  collected_vespene: 1348.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 5800.0
  spent_vespene: 775.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2125.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1250.0
    economy: 2850.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 1400.0
    technology: 550.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 275.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4200.0
    technology: 600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3992.92333984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6886.57714844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 312.169921875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 25
vespene: 573
food_cap: 38
food_used: 8
food_army: 7
food_workers: 1
idle_worker_count: 1
army_count: 3
warp_gate_count: 0

game_loop:  8924
ui_data{
 
Score:  3473
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
