----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 81
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2442
  player_apm: 60
}
game_duration_loops: 44903
game_duration_seconds: 2004.73815918
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9124
score_details {
  idle_production_time: 2031.8125
  idle_worker_time: 254.4375
  total_value_units: 3500.0
  total_value_structures: 3700.0
  killed_value_units: 1675.0
  killed_value_structures: 0.0
  collected_minerals: 8350.0
  collected_vespene: 1624.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 447.0
  spent_minerals: 6025.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 650.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 275.0
    economy: 3400.0
    technology: 1550.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 825.0
    economy: 4050.0
    technology: 1550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1869.41650391
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2470.0
    shields: 2523.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2375
vespene: 499
food_cap: 62
food_used: 44
food_army: 6
food_workers: 38
idle_worker_count: 1
army_count: 3
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 21189
score_details {
  idle_production_time: 8499.75
  idle_worker_time: 1532.5625
  total_value_units: 7800.0
  total_value_structures: 10900.0
  killed_value_units: 3150.0
  killed_value_structures: 0.0
  collected_minerals: 17795.0
  collected_vespene: 5944.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 671.0
  spent_minerals: 16725.0
  spent_vespene: 3225.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2550.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 1250.0
    technology: 1350.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3575.0
    economy: 5250.0
    technology: 5150.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 700.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 4175.0
    economy: 6500.0
    technology: 6350.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 700.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4030.38037109
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4606.39550781
    shields: 5014.80957031
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1120
vespene: 2719
food_cap: 157
food_used: 112
food_army: 68
food_workers: 44
idle_worker_count: 1
army_count: 20
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 62
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 25432
score_details {
  idle_production_time: 18409.4375
  idle_worker_time: 3432.75
  total_value_units: 12680.0
  total_value_structures: 16250.0
  killed_value_units: 12650.0
  killed_value_structures: 950.0
  collected_minerals: 27920.0
  collected_vespene: 10472.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 492.0
  spent_minerals: 26815.0
  spent_vespene: 6575.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8750.0
    economy: 400.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3500.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4375.0
    economy: 2750.0
    technology: 2850.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2630.0
    economy: 6000.0
    technology: 7150.0
    upgrade: 950.0
  }
  used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 900.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 0.0
    army: 6705.0
    economy: 8750.0
    technology: 9850.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 3925.0
    economy: 0.0
    technology: 900.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 14022.8828125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11006.3955078
    shields: 10711.1845703
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1155
vespene: 3897
food_cap: 196
food_used: 96
food_army: 54
food_workers: 42
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 62
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
single {
  unit {
    unit_type: 496
    player_relative: 1
    health: 300
    shields: 150
    energy: 0
  }
}

score{
 score_type: Melee
score: 27801
score_details {
  idle_production_time: 27467.875
  idle_worker_time: 3828.0
  total_value_units: 21480.0
  total_value_structures: 18250.0
  killed_value_units: 25350.0
  killed_value_structures: 950.0
  collected_minerals: 37365.0
  collected_vespene: 13976.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 313.0
  spent_minerals: 36745.0
  spent_vespene: 10050.0
  food_used {
    none: 0.0
    army: 49.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15950.0
    economy: 2300.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7100.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10375.0
    economy: 4100.0
    technology: 4950.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2730.0
    economy: 5875.0
    technology: 7150.0
    upgrade: 2575.0
  }
  used_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 900.0
    upgrade: 2575.0
  }
  total_used_minerals {
    none: 0.0
    army: 14005.0
    economy: 9900.0
    technology: 11050.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 0.0
    army: 7475.0
    economy: 0.0
    technology: 900.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 26365.5976562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21221.4804688
    shields: 19862.8652344
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 670
vespene: 3926
food_cap: 180
food_used: 94
food_army: 49
food_workers: 45
idle_worker_count: 2
army_count: 18
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 62
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 63
  count: 3
}
multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 26
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 33067
score_details {
  idle_production_time: 33303.6875
  idle_worker_time: 4481.3125
  total_value_units: 21480.0
  total_value_structures: 22800.0
  killed_value_units: 30900.0
  killed_value_structures: 1900.0
  collected_minerals: 43485.0
  collected_vespene: 15572.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 425.0
  spent_minerals: 40720.0
  spent_vespene: 10300.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 19550.0
    economy: 3300.0
    technology: 650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8750.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11875.0
    economy: 4400.0
    technology: 5100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1580.0
    economy: 6950.0
    technology: 9250.0
    upgrade: 2575.0
  }
  used_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 900.0
    upgrade: 2575.0
  }
  total_used_minerals {
    none: 0.0
    army: 14005.0
    economy: 11150.0
    technology: 14350.0
    upgrade: 2575.0
  }
  total_used_vespene {
    none: 0.0
    army: 7475.0
    economy: 0.0
    technology: 900.0
    upgrade: 2575.0
  }
  total_damage_dealt {
    life: 34100.9804688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23315.3945312
    shields: 22026.8652344
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2815
vespene: 5272
food_cap: 200
food_used: 72
food_army: 27
food_workers: 45
idle_worker_count: 2
army_count: 4
warp_gate_count: 0

game_loop:  44903
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 62
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 63
  count: 3
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  33067
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
