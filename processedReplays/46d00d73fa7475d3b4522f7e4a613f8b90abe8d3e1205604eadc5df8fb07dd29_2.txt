----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3994
  player_apm: 270
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3719
  player_apm: 177
}
game_duration_loops: 15406
game_duration_seconds: 687.815856934
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11539
score_details {
  idle_production_time: 7103.875
  idle_worker_time: 1.5
  total_value_units: 7150.0
  total_value_structures: 1650.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 9580.0
  collected_vespene: 1684.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 470.0
  spent_minerals: 9350.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 42.5
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5425.0
    technology: 1250.0
    upgrade: 775.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 200.0
    upgrade: 775.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 6050.0
    technology: 1250.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 456.778808594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1235.03222656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 280
vespene: 309
food_cap: 122
food_used: 92
food_army: 42
food_workers: 50
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 91
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 90
  count: 2
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 32
  }
}

score{
 score_type: Melee
score: 15720
score_details {
  idle_production_time: 19509.0625
  idle_worker_time: 107.125
  total_value_units: 14950.0
  total_value_structures: 2900.0
  killed_value_units: 4025.0
  killed_value_structures: 0.0
  collected_minerals: 18170.0
  collected_vespene: 5100.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 716.0
  spent_minerals: 17825.0
  spent_vespene: 4625.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5350.0
    economy: 1250.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2050.0
    economy: 6300.0
    technology: 2000.0
    upgrade: 1625.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 500.0
    upgrade: 1625.0
  }
  total_used_minerals {
    none: 0.0
    army: 6350.0
    economy: 8650.0
    technology: 2650.0
    upgrade: 1225.0
  }
  total_used_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 750.0
    upgrade: 1225.0
  }
  total_damage_dealt {
    life: 3421.68359375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11857.8535156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 395
vespene: 475
food_cap: 178
food_used: 92
food_army: 36
food_workers: 56
idle_worker_count: 56
army_count: 4
warp_gate_count: 0

game_loop:  15406
ui_data{
 
Score:  15720
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
