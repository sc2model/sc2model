----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4603
  player_apm: 307
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4575
  player_apm: 202
}
game_duration_loops: 11684
game_duration_seconds: 521.643554688
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11199
score_details {
  idle_production_time: 1128.3125
  idle_worker_time: 1042.0
  total_value_units: 6575.0
  total_value_structures: 4150.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 9325.0
  collected_vespene: 2324.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 649.0
  spent_minerals: 9300.0
  spent_vespene: 2075.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 375.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1775.0
    economy: 5200.0
    technology: 1850.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 5800.0
    technology: 1400.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 702.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 433.0
    shields: 757.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 75
vespene: 249
food_cap: 117
food_used: 91
food_army: 35
food_workers: 53
idle_worker_count: 1
army_count: 14
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 311
  count: 12
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 6
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

score{
 score_type: Melee
score: 10817
score_details {
  idle_production_time: 1548.125
  idle_worker_time: 1376.0625
  total_value_units: 9200.0
  total_value_structures: 4600.0
  killed_value_units: 1450.0
  killed_value_structures: 0.0
  collected_minerals: 12005.0
  collected_vespene: 3012.0
  collection_rate_minerals: 2351.0
  collection_rate_vespene: 537.0
  spent_minerals: 11175.0
  spent_vespene: 2575.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2750.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 625.0
    economy: 5400.0
    technology: 1850.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 3375.0
    economy: 6450.0
    technology: 1850.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1949.64257812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2526.0
    shields: 2603.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 880
vespene: 437
food_cap: 117
food_used: 71
food_army: 11
food_workers: 59
idle_worker_count: 3
army_count: 5
warp_gate_count: 6

game_loop:  11684
ui_data{
 multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  10817
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
