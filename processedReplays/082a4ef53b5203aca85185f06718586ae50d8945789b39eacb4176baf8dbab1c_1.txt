----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 223
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4450
  player_apm: 257
}
game_duration_loops: 18200
game_duration_seconds: 812.55670166
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9953
score_details {
  idle_production_time: 8077.125
  idle_worker_time: 10.3125
  total_value_units: 5100.0
  total_value_structures: 1850.0
  killed_value_units: 1075.0
  killed_value_structures: 0.0
  collected_minerals: 7780.0
  collected_vespene: 1848.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 627.0
  spent_minerals: 7425.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 29.5
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 775.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 425.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1600.0
    economy: 4600.0
    technology: 1400.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 4900.0
    technology: 1550.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2095.69433594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 838.845703125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 405
vespene: 73
food_cap: 92
food_used: 72
food_army: 29
food_workers: 43
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 103
  count: 11
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21823
score_details {
  idle_production_time: 18965.25
  idle_worker_time: 11.0625
  total_value_units: 19400.0
  total_value_structures: 3250.0
  killed_value_units: 7875.0
  killed_value_structures: 425.0
  collected_minerals: 21510.0
  collected_vespene: 6312.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 828.0
  spent_minerals: 19187.0
  spent_vespene: 5137.0
  food_used {
    none: 0.0
    army: 118.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5100.0
    economy: 1525.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3487.0
    economy: 1100.0
    technology: 425.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1637.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4750.0
    economy: 7000.0
    technology: 2175.0
    upgrade: 1025.0
  }
  used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 600.0
    upgrade: 1025.0
  }
  total_used_minerals {
    none: 0.0
    army: 8675.0
    economy: 9250.0
    technology: 2550.0
    upgrade: 925.0
  }
  total_used_vespene {
    none: 0.0
    army: 3500.0
    economy: 0.0
    technology: 550.0
    upgrade: 925.0
  }
  total_damage_dealt {
    life: 14302.1318359
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11963.4511719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2373
vespene: 1175
food_cap: 200
food_used: 169
food_army: 118
food_workers: 51
idle_worker_count: 0
army_count: 56
warp_gate_count: 0

game_loop:  18200
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 84
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 57
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 54
    shields: 0
    energy: 0
  }
}

Score:  21823
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
