----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3477
  player_apm: 158
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3352
  player_apm: 181
}
game_duration_loops: 15298
game_duration_seconds: 682.99407959
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10161
score_details {
  idle_production_time: 1254.8125
  idle_worker_time: 664.75
  total_value_units: 4700.0
  total_value_structures: 4100.0
  killed_value_units: 1800.0
  killed_value_structures: 0.0
  collected_minerals: 8285.0
  collected_vespene: 1876.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 649.0
  spent_minerals: 7875.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 975.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1300.0
    economy: 4550.0
    technology: 1475.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 300.0
    economy: 0.0
    technology: 625.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5000.0
    technology: 1275.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 625.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2544.85058594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1904.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 460
vespene: 451
food_cap: 101
food_used: 63
food_army: 22
food_workers: 41
idle_worker_count: 1
army_count: 10
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 53
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 85
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16898
score_details {
  idle_production_time: 3410.875
  idle_worker_time: 1203.25
  total_value_units: 9650.0
  total_value_structures: 5625.0
  killed_value_units: 9050.0
  killed_value_structures: 300.0
  collected_minerals: 15105.0
  collected_vespene: 3588.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 156.0
  spent_minerals: 13700.0
  spent_vespene: 3375.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4525.0
    economy: 2200.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1550.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3700.0
    economy: 4950.0
    technology: 2475.0
    upgrade: 675.0
  }
  used_vespene {
    none: 50.0
    army: 1850.0
    economy: 0.0
    technology: 750.0
    upgrade: 675.0
  }
  total_used_minerals {
    none: 50.0
    army: 5050.0
    economy: 5900.0
    technology: 2475.0
    upgrade: 575.0
  }
  total_used_vespene {
    none: 50.0
    army: 1900.0
    economy: 0.0
    technology: 750.0
    upgrade: 575.0
  }
  total_damage_dealt {
    life: 12643.8974609
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4852.15917969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1074.94287109
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1416
vespene: 207
food_cap: 109
food_used: 104
food_army: 68
food_workers: 36
idle_worker_count: 1
army_count: 22
warp_gate_count: 0

game_loop:  15298
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 484
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 52
    player_relative: 1
    health: 373
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 357
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 41
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 110
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 106
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 5
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 24
    shields: 0
    energy: 14
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 129
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 127
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 116
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 79
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 44
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 93
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
}

Score:  16898
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
