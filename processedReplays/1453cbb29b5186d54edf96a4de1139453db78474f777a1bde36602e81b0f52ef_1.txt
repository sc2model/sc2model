----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 78
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: -36400
  player_apm: 75
}
game_duration_loops: 8105
game_duration_seconds: 361.855621338
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6850
score_details {
  idle_production_time: 929.0
  idle_worker_time: 37.0
  total_value_units: 3100.0
  total_value_structures: 1850.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 5595.0
  collected_vespene: 1480.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 335.0
  spent_minerals: 3500.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 2350.0
    technology: 550.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 2500.0
    technology: 550.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 196.0
    shields: 538.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1000.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2145
vespene: 530
food_cap: 55
food_used: 35
food_army: 12
food_workers: 23
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  8105
ui_data{
 
Score:  6850
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
