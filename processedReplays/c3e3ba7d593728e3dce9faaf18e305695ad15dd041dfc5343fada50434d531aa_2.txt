----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3906
  player_apm: 181
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 120
}
game_duration_loops: 6507
game_duration_seconds: 290.511352539
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4300
score_details {
  idle_production_time: 5111.0625
  idle_worker_time: 1.0
  total_value_units: 4100.0
  total_value_structures: 825.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 4580.0
  collected_vespene: 120.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 0.0
  spent_minerals: 4125.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 425.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1050.0
    economy: 2275.0
    technology: 250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 2425.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 470.0
    shields: 1380.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2224.03662109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 505
vespene: 20
food_cap: 52
food_used: 39
food_army: 19
food_workers: 20
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  6507
ui_data{
 production {
  unit {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 105
    build_progress: 0.317708313465
  }
}

Score:  4300
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
