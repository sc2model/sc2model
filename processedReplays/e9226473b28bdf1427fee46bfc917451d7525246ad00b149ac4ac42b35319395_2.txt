----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3575
  player_apm: 58
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3241
  player_apm: 137
}
game_duration_loops: 21740
game_duration_seconds: 970.60345459
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8138
score_details {
  idle_production_time: 1757.5
  idle_worker_time: 630.75
  total_value_units: 4850.0
  total_value_structures: 3075.0
  killed_value_units: 2925.0
  killed_value_structures: 100.0
  collected_minerals: 7450.0
  collected_vespene: 1488.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 268.0
  spent_minerals: 7400.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1875.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1150.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1250.0
    economy: 3825.0
    technology: 1325.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 4100.0
    technology: 1325.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1550.0
    shields: 1864.25
    energy: 0.0
  }
  total_damage_taken {
    life: 1959.28271484
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 359.96875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 100
vespene: 363
food_cap: 62
food_used: 62
food_army: 25
food_workers: 36
idle_worker_count: 2
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22941
score_details {
  idle_production_time: 7160.25
  idle_worker_time: 2393.9375
  total_value_units: 13375.0
  total_value_structures: 5750.0
  killed_value_units: 9225.0
  killed_value_structures: 5325.0
  collected_minerals: 22965.0
  collected_vespene: 5876.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 761.0
  spent_minerals: 20300.0
  spent_vespene: 4700.0
  food_used {
    none: 0.0
    army: 99.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4825.0
    economy: 4775.0
    technology: 1850.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2650.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5250.0
    economy: 6800.0
    technology: 2500.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 50.0
    army: 1500.0
    economy: 0.0
    technology: 900.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 50.0
    army: 7600.0
    economy: 7550.0
    technology: 1900.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 1875.0
    economy: 0.0
    technology: 700.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 17454.0
    shields: 18509.75
    energy: 0.0
  }
  total_damage_taken {
    life: 4010.83691406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2438.49511719
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2715
vespene: 1176
food_cap: 164
food_used: 164
food_army: 99
food_workers: 65
idle_worker_count: 0
army_count: 66
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 29060
score_details {
  idle_production_time: 7512.4375
  idle_worker_time: 3032.8125
  total_value_units: 15900.0
  total_value_structures: 7150.0
  killed_value_units: 9975.0
  killed_value_structures: 6125.0
  collected_minerals: 25805.0
  collected_vespene: 6780.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 559.0
  spent_minerals: 20650.0
  spent_vespene: 4950.0
  food_used {
    none: 0.0
    army: 119.0
    economy: 72.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4925.0
    economy: 6225.0
    technology: 1850.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2950.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 6400.0
    economy: 7650.0
    technology: 2600.0
    upgrade: 1150.0
  }
  used_vespene {
    none: 50.0
    army: 1975.0
    economy: 0.0
    technology: 1000.0
    upgrade: 1150.0
  }
  total_used_minerals {
    none: 50.0
    army: 9200.0
    economy: 8450.0
    technology: 2500.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 2500.0
    economy: 0.0
    technology: 900.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 19818.0
    shields: 21147.875
    energy: 0.0
  }
  total_damage_taken {
    life: 4260.83691406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2701.61572266
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5205
vespene: 1830
food_cap: 200
food_used: 191
food_army: 119
food_workers: 71
idle_worker_count: 13
army_count: 83
warp_gate_count: 0

game_loop:  21740
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
single {
  unit {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      build_progress: 0.00625002384186
    }
  }
}

Score:  29060
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
