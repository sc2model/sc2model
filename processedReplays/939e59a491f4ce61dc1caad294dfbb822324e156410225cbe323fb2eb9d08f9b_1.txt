----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4096
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4092
  player_apm: 299
}
game_duration_loops: 15299
game_duration_seconds: 683.038757324
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13415
score_details {
  idle_production_time: 1145.375
  idle_worker_time: 267.1875
  total_value_units: 6300.0
  total_value_structures: 4725.0
  killed_value_units: 125.0
  killed_value_structures: 0.0
  collected_minerals: 9830.0
  collected_vespene: 2660.0
  collection_rate_minerals: 2379.0
  collection_rate_vespene: 627.0
  spent_minerals: 9050.0
  spent_vespene: 2100.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5425.0
    technology: 1850.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 400.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5425.0
    technology: 1850.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 400.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 246.689453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20.0
    shields: 45.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 830
vespene: 560
food_cap: 117
food_used: 105
food_army: 46
food_workers: 58
idle_worker_count: 0
army_count: 18
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 488
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 14431
score_details {
  idle_production_time: 3387.875
  idle_worker_time: 1042.1875
  total_value_units: 14950.0
  total_value_structures: 7250.0
  killed_value_units: 8350.0
  killed_value_structures: 0.0
  collected_minerals: 18785.0
  collected_vespene: 6296.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 963.0
  spent_minerals: 18375.0
  spent_vespene: 5075.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6250.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8275.0
    economy: 350.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 6100.0
    technology: 3250.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 500.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 9625.0
    economy: 6450.0
    technology: 3400.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 5825.0
    economy: 0.0
    technology: 500.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 12162.0957031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7539.60839844
    shields: 9145.37402344
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 460
vespene: 1221
food_cap: 165
food_used: 75
food_army: 16
food_workers: 59
idle_worker_count: 3
army_count: 5
warp_gate_count: 11

game_loop:  15299
ui_data{
 single {
  unit {
    unit_type: 83
    player_relative: 1
    health: 34
    shields: 0
    energy: 0
  }
}

Score:  14431
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
