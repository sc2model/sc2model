----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4326
  player_apm: 361
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4256
  player_apm: 128
}
game_duration_loops: 4745
game_duration_seconds: 211.84513855
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3923
score_details {
  idle_production_time: 127.4375
  idle_worker_time: 42.9375
  total_value_units: 1450.0
  total_value_structures: 1550.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 2920.0
  collected_vespene: 328.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 246.0
  spent_minerals: 2550.0
  spent_vespene: 225.0
  food_used {
    none: 0.0
    army: 1.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 100.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 50.0
    economy: 2600.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 250.0
    economy: 2950.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 220.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 501.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 420
vespene: 103
food_cap: 46
food_used: 24
food_army: 1
food_workers: 22
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  4745
ui_data{
 multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1182
    shields: 0
    energy: 0
  }
}

Score:  3923
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
