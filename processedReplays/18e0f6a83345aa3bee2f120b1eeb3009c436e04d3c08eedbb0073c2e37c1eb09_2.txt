----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3323
  player_apm: 162
}
game_duration_loops: 10379
game_duration_seconds: 463.380554199
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7204
score_details {
  idle_production_time: 3875.25
  idle_worker_time: 18.25
  total_value_units: 6800.0
  total_value_structures: 1325.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 7700.0
  collected_vespene: 860.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 335.0
  spent_minerals: 6781.0
  spent_vespene: 550.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1425.0
    economy: 731.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1125.0
    economy: 3925.0
    technology: 450.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 5075.0
    technology: 450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1727.82666016
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5041.82080078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 969
vespene: 310
food_cap: 74
food_used: 63
food_army: 24
food_workers: 39
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 144
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7051
score_details {
  idle_production_time: 4160.625
  idle_worker_time: 18.25
  total_value_units: 6800.0
  total_value_structures: 1325.0
  killed_value_units: 875.0
  killed_value_structures: 0.0
  collected_minerals: 8130.0
  collected_vespene: 952.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 335.0
  spent_minerals: 7156.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1650.0
    economy: 1081.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 3575.0
    technology: 450.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 5075.0
    technology: 450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2065.27832031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5696.08789062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1024
vespene: 302
food_cap: 74
food_used: 60
food_army: 28
food_workers: 32
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10379
ui_data{
 multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 128
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 24
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 143
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 129
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 130
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 142
    shields: 0
    energy: 0
  }
}

Score:  7051
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
