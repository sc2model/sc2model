----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3234
  player_apm: 64
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 121
}
game_duration_loops: 33749
game_duration_seconds: 1506.75695801
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12081
score_details {
  idle_production_time: 1279.875
  idle_worker_time: 59.9375
  total_value_units: 7550.0
  total_value_structures: 3700.0
  killed_value_units: 150.0
  killed_value_structures: 700.0
  collected_minerals: 9620.0
  collected_vespene: 2836.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 694.0
  spent_minerals: 8850.0
  spent_vespene: 2075.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2975.0
    economy: 4350.0
    technology: 1400.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3750.0
    economy: 4350.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2811.27734375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 680.625
    shields: 1191.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 820
vespene: 761
food_cap: 110
food_used: 92
food_army: 47
food_workers: 45
idle_worker_count: 0
army_count: 21
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 26
}
groups {
  control_group_index: 2
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 18371
score_details {
  idle_production_time: 4722.4375
  idle_worker_time: 834.9375
  total_value_units: 22900.0
  total_value_structures: 6700.0
  killed_value_units: 4050.0
  killed_value_structures: 875.0
  collected_minerals: 24465.0
  collected_vespene: 8756.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 492.0
  spent_minerals: 23575.0
  spent_vespene: 7575.0
  food_used {
    none: 0.0
    army: 52.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 1275.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10875.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2950.0
    economy: 7400.0
    technology: 2100.0
    upgrade: 900.0
  }
  used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 500.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 0.0
    army: 13575.0
    economy: 7350.0
    technology: 2100.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 6075.0
    economy: 0.0
    technology: 500.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 11047.2988281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7956.75
    shields: 9818.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 940
vespene: 1181
food_cap: 200
food_used: 110
food_army: 52
food_workers: 58
idle_worker_count: 15
army_count: 14
warp_gate_count: 6

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 83
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 63
  count: 2
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 26388
score_details {
  idle_production_time: 10769.4375
  idle_worker_time: 3455.625
  total_value_units: 38425.0
  total_value_structures: 9450.0
  killed_value_units: 9150.0
  killed_value_structures: 2500.0
  collected_minerals: 42585.0
  collected_vespene: 15628.0
  collection_rate_minerals: 2939.0
  collection_rate_vespene: 1366.0
  spent_minerals: 39525.0
  spent_vespene: 13725.0
  food_used {
    none: 0.0
    army: 56.0
    economy: 82.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5100.0
    economy: 3750.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2100.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 22150.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 9875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3275.0
    economy: 10000.0
    technology: 2850.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 800.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 23200.0
    economy: 10650.0
    technology: 2850.0
    upgrade: 1300.0
  }
  total_used_vespene {
    none: 0.0
    army: 10375.0
    economy: 0.0
    technology: 800.0
    upgrade: 1300.0
  }
  total_damage_dealt {
    life: 26527.578125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14701.4257812
    shields: 17371.3007812
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3110
vespene: 1903
food_cap: 200
food_used: 138
food_army: 56
food_workers: 82
idle_worker_count: 1
army_count: 16
warp_gate_count: 6

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 15
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 63
  count: 2
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

score{
 score_type: Melee
score: 32219
score_details {
  idle_production_time: 12797.125
  idle_worker_time: 4464.5
  total_value_units: 47425.0
  total_value_structures: 10300.0
  killed_value_units: 12450.0
  killed_value_structures: 3650.0
  collected_minerals: 50385.0
  collected_vespene: 17784.0
  collection_rate_minerals: 3191.0
  collection_rate_vespene: 694.0
  spent_minerals: 44700.0
  spent_vespene: 15825.0
  food_used {
    none: 0.0
    army: 84.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6850.0
    economy: 5150.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3050.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 24775.0
    economy: 1250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 10975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4875.0
    economy: 9950.0
    technology: 3300.0
    upgrade: 1550.0
  }
  used_vespene {
    none: 0.0
    army: 2500.0
    economy: 0.0
    technology: 800.0
    upgrade: 1550.0
  }
  total_used_minerals {
    none: 0.0
    army: 29150.0
    economy: 11200.0
    technology: 3300.0
    upgrade: 1400.0
  }
  total_used_vespene {
    none: 0.0
    army: 13275.0
    economy: 0.0
    technology: 800.0
    upgrade: 1400.0
  }
  total_damage_dealt {
    life: 33945.671875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16889.5507812
    shields: 20169.6757812
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5735
vespene: 1959
food_cap: 200
food_used: 159
food_army: 84
food_workers: 75
idle_worker_count: 4
army_count: 29
warp_gate_count: 7

game_loop:  33749
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 80
  count: 29
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 63
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 15
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 48
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 72
    shields: 14
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 50
    shields: 24
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 78
    shields: 15
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  32219
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
