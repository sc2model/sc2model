----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3740
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3749
  player_apm: 118
}
game_duration_loops: 11756
game_duration_seconds: 524.858032227
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5368
score_details {
  idle_production_time: 5362.875
  idle_worker_time: 531.375
  total_value_units: 7200.0
  total_value_structures: 1950.0
  killed_value_units: 2900.0
  killed_value_structures: 100.0
  collected_minerals: 7835.0
  collected_vespene: 1620.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 156.0
  spent_minerals: 7787.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 1700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2725.0
    economy: 1250.0
    technology: 137.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 625.0
    economy: 2725.0
    technology: 1175.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 3200.0
    economy: 4275.0
    technology: 1625.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4226.70703125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9607.1328125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 98
vespene: 520
food_cap: 68
food_used: 36
food_army: 14
food_workers: 17
idle_worker_count: 3
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 5029
score_details {
  idle_production_time: 6658.5625
  idle_worker_time: 863.5
  total_value_units: 8350.0
  total_value_structures: 2500.0
  killed_value_units: 3300.0
  killed_value_structures: 100.0
  collected_minerals: 8905.0
  collected_vespene: 1836.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 179.0
  spent_minerals: 8862.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1300.0
    economy: 1750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3575.0
    economy: 1625.0
    technology: 237.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 150.0
    economy: 2750.0
    technology: 1375.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 3575.0
    economy: 4975.0
    technology: 1925.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 5146.57421875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12577.1767578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 93
vespene: 311
food_cap: 68
food_used: 28
food_army: 4
food_workers: 24
idle_worker_count: 4
army_count: 0
warp_gate_count: 0

game_loop:  11756
ui_data{
 multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  5029
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
