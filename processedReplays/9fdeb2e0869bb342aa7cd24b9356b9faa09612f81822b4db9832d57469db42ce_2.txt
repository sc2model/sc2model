----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2900
  player_apm: 88
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3222
  player_apm: 69
}
game_duration_loops: 1866
game_duration_seconds: 83.309387207
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2043
score_details {
  idle_production_time: 8.3125
  idle_worker_time: 6.25
  total_value_units: 900.0
  total_value_structures: 575.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 965.0
  collected_vespene: 28.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 111.0
  spent_minerals: 675.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1525.0
    technology: 150.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 1475.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 340
vespene: 28
food_cap: 23
food_used: 19
food_army: 0
food_workers: 18
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  1866
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 18
  count: 1
}
production {
  unit {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0772058963776
  }
}

Score:  2043
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
