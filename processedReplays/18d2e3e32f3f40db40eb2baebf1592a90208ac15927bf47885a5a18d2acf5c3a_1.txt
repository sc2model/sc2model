----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2903
  player_apm: 110
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2846
  player_apm: 117
}
game_duration_loops: 9808
game_duration_seconds: 437.887695312
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5729
score_details {
  idle_production_time: 1804.3125
  idle_worker_time: 295.75
  total_value_units: 3475.0
  total_value_structures: 2875.0
  killed_value_units: 1575.0
  killed_value_structures: 0.0
  collected_minerals: 5595.0
  collected_vespene: 1884.0
  collection_rate_minerals: 391.0
  collection_rate_vespene: 156.0
  spent_minerals: 5575.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 2300.0
    technology: 1225.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 3300.0
    technology: 1225.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1493.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2027.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 70
vespene: 709
food_cap: 54
food_used: 27
food_army: 12
food_workers: 13
idle_worker_count: 2
army_count: 2
warp_gate_count: 0

game_loop:  9808
ui_data{
 
Score:  5729
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
