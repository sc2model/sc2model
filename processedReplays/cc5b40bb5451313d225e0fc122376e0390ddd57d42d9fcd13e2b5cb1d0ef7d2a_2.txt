----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3411
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3400
  player_apm: 117
}
game_duration_loops: 23490
game_duration_seconds: 1048.73388672
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11115
score_details {
  idle_production_time: 1659.3125
  idle_worker_time: 150.1875
  total_value_units: 4075.0
  total_value_structures: 4550.0
  killed_value_units: 1350.0
  killed_value_structures: 0.0
  collected_minerals: 8025.0
  collected_vespene: 2740.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 1007.0
  spent_minerals: 7600.0
  spent_vespene: 1875.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 1350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 4550.0
    technology: 2300.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1000.0
    economy: 4600.0
    technology: 1850.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1314.48486328
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 122.5
    shields: 289.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 475
vespene: 865
food_cap: 101
food_used: 62
food_army: 18
food_workers: 44
idle_worker_count: 0
army_count: 8
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 77
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 78
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 488
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 33992
score_details {
  idle_production_time: 6305.6875
  idle_worker_time: 320.125
  total_value_units: 17195.0
  total_value_structures: 9150.0
  killed_value_units: 4600.0
  killed_value_structures: 0.0
  collected_minerals: 25725.0
  collected_vespene: 11612.0
  collection_rate_minerals: 2491.0
  collection_rate_vespene: 1142.0
  spent_minerals: 21810.0
  spent_vespene: 8075.0
  food_used {
    none: 0.0
    army: 119.0
    economy: 77.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1150.0
    economy: 3250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3400.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6165.0
    economy: 8300.0
    technology: 3800.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 0.0
    army: 4825.0
    economy: 0.0
    technology: 900.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 0.0
    army: 9445.0
    economy: 8350.0
    technology: 3800.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 6400.0
    economy: 0.0
    technology: 900.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 6916.8671875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2880.5
    shields: 2695.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3965
vespene: 3537
food_cap: 200
food_used: 196
food_army: 119
food_workers: 77
idle_worker_count: 15
army_count: 36
warp_gate_count: 10

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 75
  count: 15
}
groups {
  control_group_index: 3
  leader_unit_type: 78
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 10
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 8
  leader_unit_type: 10
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 136
  count: 1
}
multi {
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 496
    player_relative: 1
    health: 300
    shields: 150
    energy: 0
  }
  units {
    unit_type: 496
    player_relative: 1
    health: 300
    shields: 150
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 496
    player_relative: 1
    health: 300
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 496
    player_relative: 1
    health: 300
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
}

score{
 score_type: Melee
score: 39293
score_details {
  idle_production_time: 7689.875
  idle_worker_time: 2091.875
  total_value_units: 21685.0
  total_value_structures: 9150.0
  killed_value_units: 11050.0
  killed_value_structures: 550.0
  collected_minerals: 32925.0
  collected_vespene: 13948.0
  collection_rate_minerals: 3079.0
  collection_rate_vespene: 828.0
  spent_minerals: 24975.0
  spent_vespene: 9650.0
  food_used {
    none: 0.0
    army: 115.0
    economy: 76.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 5550.0
    economy: 3600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 1900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6130.0
    economy: 200.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 435.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6245.0
    economy: 8150.0
    technology: 3650.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 5250.0
    economy: 0.0
    technology: 900.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 12460.0
    economy: 8350.0
    technology: 3800.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 0.0
    army: 7875.0
    economy: 0.0
    technology: 900.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 15166.1855469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6307.30664062
    shields: 6106.28808594
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 8000
vespene: 4298
food_cap: 200
food_used: 191
food_army: 115
food_workers: 76
idle_worker_count: 3
army_count: 31
warp_gate_count: 10

game_loop:  23490
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 75
  count: 17
}
groups {
  control_group_index: 3
  leader_unit_type: 78
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 10
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 8
  leader_unit_type: 10
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 81
  count: 1
}

Score:  39293
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
