----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2765
  player_apm: 104
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 34
}
game_duration_loops: 17788
game_duration_seconds: 794.162536621
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9341
score_details {
  idle_production_time: 1450.375
  idle_worker_time: 631.4375
  total_value_units: 3150.0
  total_value_structures: 3900.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 6695.0
  collected_vespene: 1720.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 425.0
  spent_minerals: 6087.0
  spent_vespene: 1537.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 3600.0
    technology: 1950.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 450.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 850.0
    economy: 3550.0
    technology: 1800.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 341.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20.0
    shields: 40.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 658
vespene: 183
food_cap: 86
food_used: 52
food_army: 18
food_workers: 34
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  10000
ui_data{
 multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 8812
score_details {
  idle_production_time: 3615.75
  idle_worker_time: 1215.8125
  total_value_units: 6350.0
  total_value_structures: 6650.0
  killed_value_units: 1475.0
  killed_value_structures: 725.0
  collected_minerals: 10750.0
  collected_vespene: 3936.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 335.0
  spent_minerals: 10487.0
  spent_vespene: 2787.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 825.0
    economy: 1000.0
    technology: 375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2500.0
    economy: 1750.0
    technology: 787.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 187.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 2750.0
    technology: 2450.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 4500.0
    technology: 3500.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 650.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 6808.04101562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6677.0
    shields: 6666.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 313
vespene: 1149
food_cap: 86
food_used: 28
food_army: 6
food_workers: 22
idle_worker_count: 2
army_count: 2
warp_gate_count: 0

game_loop:  17788
ui_data{
 single {
  unit {
    unit_type: 80
    player_relative: 1
    health: 83
    shields: 36
    energy: 0
  }
}

Score:  8812
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
