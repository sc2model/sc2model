----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3218
  player_apm: 261
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 71
}
game_duration_loops: 10060
game_duration_seconds: 449.13848877
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4513
score_details {
  idle_production_time: 845.6875
  idle_worker_time: 448.8125
  total_value_units: 3800.0
  total_value_structures: 2850.0
  killed_value_units: 2550.0
  killed_value_structures: 0.0
  collected_minerals: 6385.0
  collected_vespene: 1728.0
  collection_rate_minerals: 195.0
  collection_rate_vespene: 0.0
  spent_minerals: 6100.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 2900.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 1250.0
    technology: 1100.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 3750.0
    technology: 1100.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1639.0
    shields: 1041.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3801.4909668
    shields: 6975.75830078
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 335
vespene: 778
food_cap: 38
food_used: 12
food_army: 8
food_workers: 4
idle_worker_count: 0
army_count: 2
warp_gate_count: 1

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 83
    player_relative: 4
    health: 31
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4418
score_details {
  idle_production_time: 853.1875
  idle_worker_time: 456.3125
  total_value_units: 3800.0
  total_value_structures: 2850.0
  killed_value_units: 2550.0
  killed_value_structures: 0.0
  collected_minerals: 6390.0
  collected_vespene: 1728.0
  collection_rate_minerals: 195.0
  collection_rate_vespene: 0.0
  spent_minerals: 6100.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1100.0
    economy: 2900.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 1250.0
    technology: 1100.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 3750.0
    technology: 1100.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1647.0
    shields: 1041.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3891.14526367
    shields: 6975.75830078
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 340
vespene: 778
food_cap: 38
food_used: 10
food_army: 6
food_workers: 4
idle_worker_count: 4
army_count: 1
warp_gate_count: 1

game_loop:  10060
ui_data{
 
Score:  4418
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
