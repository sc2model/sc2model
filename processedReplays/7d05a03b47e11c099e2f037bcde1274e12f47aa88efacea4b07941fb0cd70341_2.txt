----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2755
  player_apm: 79
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 71
}
game_duration_loops: 31446
game_duration_seconds: 1403.93725586
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9142
score_details {
  idle_production_time: 1892.6875
  idle_worker_time: 209.9375
  total_value_units: 3350.0
  total_value_structures: 3400.0
  killed_value_units: 1400.0
  killed_value_structures: 0.0
  collected_minerals: 6765.0
  collected_vespene: 1892.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 627.0
  spent_minerals: 6650.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 900.0
    economy: 4000.0
    technology: 1850.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 150.0
    technology: 250.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1050.0
    economy: 4050.0
    technology: 1550.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 300.0
    technology: 250.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 1820.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 793.5
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 194.696289062
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 156
vespene: 636
food_cap: 70
food_used: 52
food_army: 18
food_workers: 34
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 130
  count: 2
}
production {
  unit {
    unit_type: 37
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 24064
score_details {
  idle_production_time: 8189.6875
  idle_worker_time: 290.5
  total_value_units: 10325.0
  total_value_structures: 8550.0
  killed_value_units: 4900.0
  killed_value_structures: 875.0
  collected_minerals: 19840.0
  collected_vespene: 7084.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 873.0
  spent_minerals: 19400.0
  spent_vespene: 5150.0
  food_used {
    none: 0.0
    army: 77.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2900.0
    economy: 1550.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1000.0
    economy: 150.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1379.0
    economy: 653.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 495.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 3975.0
    economy: 7300.0
    technology: 4500.0
    upgrade: 1500.0
  }
  used_vespene {
    none: 50.0
    army: 1550.0
    economy: 300.0
    technology: 900.0
    upgrade: 1500.0
  }
  total_used_minerals {
    none: 50.0
    army: 4850.0
    economy: 8400.0
    technology: 4300.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 50.0
    army: 1725.0
    economy: 600.0
    technology: 700.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 6621.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3873.98632812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1449.17236328
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 420
vespene: 1869
food_cap: 200
food_used: 142
food_army: 77
food_workers: 62
idle_worker_count: 3
army_count: 40
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 51
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1495
    shields: 0
    energy: 41
  }
  units {
    unit_type: 130
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1445
    shields: 0
    energy: 14
  }
  units {
    unit_type: 130
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 41715
score_details {
  idle_production_time: 19699.9375
  idle_worker_time: 1286.25
  total_value_units: 23050.0
  total_value_structures: 15125.0
  killed_value_units: 16375.0
  killed_value_structures: 1200.0
  collected_minerals: 36995.0
  collected_vespene: 12804.0
  collection_rate_minerals: 2323.0
  collection_rate_vespene: 649.0
  spent_minerals: 33825.0
  spent_vespene: 11250.0
  food_used {
    none: 0.0
    army: 121.0
    economy: 68.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12475.0
    economy: 1600.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2850.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4729.0
    economy: 1003.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1545.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 7225.0
    economy: 10200.0
    technology: 7875.0
    upgrade: 2500.0
  }
  used_vespene {
    none: 50.0
    army: 5250.0
    economy: 600.0
    technology: 1350.0
    upgrade: 2500.0
  }
  total_used_minerals {
    none: 50.0
    army: 11900.0
    economy: 12100.0
    technology: 7775.0
    upgrade: 2000.0
  }
  total_used_vespene {
    none: 50.0
    army: 6750.0
    economy: 1200.0
    technology: 1350.0
    upgrade: 2000.0
  }
  total_damage_dealt {
    life: 19314.2480469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11421.1464844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5244.37255859
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2834
vespene: 1281
food_cap: 200
food_used: 189
food_army: 121
food_workers: 68
idle_worker_count: 0
army_count: 35
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 51
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 28
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 9
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 41692
score_details {
  idle_production_time: 22951.3125
  idle_worker_time: 1311.1875
  total_value_units: 23050.0
  total_value_structures: 16675.0
  killed_value_units: 21350.0
  killed_value_structures: 1575.0
  collected_minerals: 39490.0
  collected_vespene: 13812.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 963.0
  spent_minerals: 37275.0
  spent_vespene: 13350.0
  food_used {
    none: 0.0
    army: 127.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 16775.0
    economy: 2300.0
    technology: 425.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3075.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6425.0
    economy: 1053.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2818.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 7625.0
    economy: 10150.0
    technology: 9225.0
    upgrade: 2500.0
  }
  used_vespene {
    none: 50.0
    army: 5550.0
    economy: 600.0
    technology: 1350.0
    upgrade: 2500.0
  }
  total_used_minerals {
    none: 50.0
    army: 11900.0
    economy: 12100.0
    technology: 9325.0
    upgrade: 2500.0
  }
  total_used_vespene {
    none: 50.0
    army: 6750.0
    economy: 1200.0
    technology: 1350.0
    upgrade: 2500.0
  }
  total_damage_dealt {
    life: 27399.7480469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14260.1464844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5898.37255859
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1751
vespene: 91
food_cap: 200
food_used: 194
food_army: 127
food_workers: 67
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  31446
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 57
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 28
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 9
}
multi {
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
}

Score:  41692
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
