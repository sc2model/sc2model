----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2999
  player_apm: 50
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2883
  player_apm: 62
}
game_duration_loops: 23418
game_duration_seconds: 1045.51940918
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8425
score_details {
  idle_production_time: 1322.0625
  idle_worker_time: 129.6875
  total_value_units: 4875.0
  total_value_structures: 3000.0
  killed_value_units: 2900.0
  killed_value_structures: 500.0
  collected_minerals: 6390.0
  collected_vespene: 1760.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 335.0
  spent_minerals: 6225.0
  spent_vespene: 1200.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1600.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 625.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2200.0
    economy: 3050.0
    technology: 1100.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2825.0
    economy: 3050.0
    technology: 950.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 5223.38085938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 898.0
    shields: 1061.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 215
vespene: 560
food_cap: 102
food_used: 67
food_army: 43
food_workers: 23
idle_worker_count: 0
army_count: 22
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 311
  count: 19
}
groups {
  control_group_index: 7
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 16
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 58
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 47
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 37
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 84
    shields: 0
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 57
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 66
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 30
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 58
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
}

score{
 score_type: Melee
score: 21528
score_details {
  idle_production_time: 4434.0625
  idle_worker_time: 1145.6875
  total_value_units: 12575.0
  total_value_structures: 5850.0
  killed_value_units: 3000.0
  killed_value_structures: 500.0
  collected_minerals: 16655.0
  collected_vespene: 5548.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 470.0
  spent_minerals: 15875.0
  spent_vespene: 5050.0
  food_used {
    none: 0.0
    army: 125.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1600.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1075.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6700.0
    economy: 6200.0
    technology: 1800.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 650.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 7175.0
    economy: 5850.0
    technology: 1800.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 2950.0
    economy: 0.0
    technology: 650.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 5774.41455078
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1238.625
    shields: 1831.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 830
vespene: 498
food_cap: 197
food_used: 173
food_army: 125
food_workers: 48
idle_worker_count: 4
army_count: 47
warp_gate_count: 4

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 77
  count: 38
}
groups {
  control_group_index: 9
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 21
    shields: 40
    energy: 200
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 61
    shields: 70
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 47
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 71
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 37
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 49
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 66
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 13
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 12
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 16997
score_details {
  idle_production_time: 5767.625
  idle_worker_time: 1956.5
  total_value_units: 15675.0
  total_value_structures: 6550.0
  killed_value_units: 4500.0
  killed_value_structures: 500.0
  collected_minerals: 21165.0
  collected_vespene: 6832.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 694.0
  spent_minerals: 17825.0
  spent_vespene: 5800.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2400.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8050.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1225.0
    economy: 6300.0
    technology: 1800.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 650.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 9275.0
    economy: 6550.0
    technology: 1800.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 3950.0
    economy: 0.0
    technology: 650.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 7469.58642578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6749.43896484
    shields: 7402.96191406
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3390
vespene: 1032
food_cap: 196
food_used: 73
food_army: 25
food_workers: 48
idle_worker_count: 0
army_count: 9
warp_gate_count: 3

game_loop:  23418
ui_data{
 
Score:  16997
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
