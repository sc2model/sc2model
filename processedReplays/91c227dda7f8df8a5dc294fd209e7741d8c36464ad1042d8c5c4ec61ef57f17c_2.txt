----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 111
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3022
  player_apm: 131
}
game_duration_loops: 17303
game_duration_seconds: 772.509277344
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10959
score_details {
  idle_production_time: 1240.1875
  idle_worker_time: 616.8125
  total_value_units: 4975.0
  total_value_structures: 3675.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 8195.0
  collected_vespene: 2164.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 649.0
  spent_minerals: 7800.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1900.0
    economy: 4600.0
    technology: 1350.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 475.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2200.0
    economy: 4700.0
    technology: 1200.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 675.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 526.854003906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 646.471191406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 114.782226562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 445
vespene: 414
food_cap: 102
food_used: 82
food_army: 38
food_workers: 42
idle_worker_count: 1
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 692
  count: 12
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 56
    shields: 0
    energy: 113
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 151
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 87
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22232
score_details {
  idle_production_time: 4527.3125
  idle_worker_time: 1626.375
  total_value_units: 10700.0
  total_value_structures: 6675.0
  killed_value_units: 14100.0
  killed_value_structures: 2475.0
  collected_minerals: 18420.0
  collected_vespene: 6012.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 873.0
  spent_minerals: 16400.0
  spent_vespene: 4250.0
  food_used {
    none: 0.0
    army: 84.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7350.0
    economy: 1850.0
    technology: 1675.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5400.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 4200.0
    economy: 6850.0
    technology: 2250.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 50.0
    army: 1525.0
    economy: 0.0
    technology: 775.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 50.0
    army: 6250.0
    economy: 7150.0
    technology: 2250.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 50.0
    army: 1750.0
    economy: 0.0
    technology: 775.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 20034.4667969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3799.36181641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2295.54101562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2070
vespene: 1762
food_cap: 196
food_used: 138
food_army: 84
food_workers: 54
idle_worker_count: 4
army_count: 55
warp_gate_count: 0

game_loop:  17303
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 49
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
}

Score:  22232
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
