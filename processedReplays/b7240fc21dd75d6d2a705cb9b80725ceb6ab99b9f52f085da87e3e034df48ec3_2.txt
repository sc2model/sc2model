----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4235
  player_apm: 165
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4063
  player_apm: 160
}
game_duration_loops: 14119
game_duration_seconds: 630.356506348
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7190
score_details {
  idle_production_time: 8774.625
  idle_worker_time: 13.875
  total_value_units: 5400.0
  total_value_structures: 1625.0
  killed_value_units: 1125.0
  killed_value_structures: 0.0
  collected_minerals: 6945.0
  collected_vespene: 932.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 179.0
  spent_minerals: 6656.0
  spent_vespene: 506.0
  food_used {
    none: 0.5
    army: 17.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1006.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 56.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 1025.0
    economy: 3650.0
    technology: 1250.0
    upgrade: 200.0
  }
  used_vespene {
    none: -25.0
    army: 25.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 4275.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2088.21875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3251.82470703
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 339
vespene: 426
food_cap: 68
food_used: 52
food_army: 17
food_workers: 35
idle_worker_count: 0
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8899
score_details {
  idle_production_time: 15755.25
  idle_worker_time: 106.375
  total_value_units: 9800.0
  total_value_structures: 2550.0
  killed_value_units: 2450.0
  killed_value_structures: 0.0
  collected_minerals: 12100.0
  collected_vespene: 2336.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 492.0
  spent_minerals: 11281.0
  spent_vespene: 2131.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3031.0
    economy: 1475.0
    technology: 850.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 56.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 1425.0
    economy: 3925.0
    technology: 300.0
    upgrade: 600.0
  }
  used_vespene {
    none: -25.0
    army: 1000.0
    economy: 0.0
    technology: 50.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 4450.0
    economy: 6300.0
    technology: 1650.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 450.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 5482.99121094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14156.1005859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 869
vespene: 205
food_cap: 86
food_used: 64
food_army: 27
food_workers: 37
idle_worker_count: 37
army_count: 13
warp_gate_count: 0

game_loop:  14119
ui_data{
 
Score:  8899
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
