----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4114
  player_apm: 106
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4056
  player_apm: 106
}
game_duration_loops: 16709
game_duration_seconds: 745.989562988
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11863
score_details {
  idle_production_time: 1264.8125
  idle_worker_time: 255.875
  total_value_units: 5275.0
  total_value_structures: 4075.0
  killed_value_units: 675.0
  killed_value_structures: 0.0
  collected_minerals: 8835.0
  collected_vespene: 2408.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 671.0
  spent_minerals: 8550.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2275.0
    economy: 5050.0
    technology: 1450.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 400.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2175.0
    economy: 4950.0
    technology: 1575.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 750.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 333.0
    shields: 428.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1279.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 85.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 330
vespene: 508
food_cap: 102
food_used: 93
food_army: 46
food_workers: 47
idle_worker_count: 1
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 29
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 55
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 59
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20554
score_details {
  idle_production_time: 4042.625
  idle_worker_time: 881.0625
  total_value_units: 11900.0
  total_value_structures: 5225.0
  killed_value_units: 7150.0
  killed_value_structures: 1175.0
  collected_minerals: 17065.0
  collected_vespene: 5444.0
  collection_rate_minerals: 2799.0
  collection_rate_vespene: 895.0
  spent_minerals: 15675.0
  spent_vespene: 4900.0
  food_used {
    none: 0.0
    army: 97.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4700.0
    economy: 1325.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1850.0
    economy: 350.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 4825.0
    economy: 6350.0
    technology: 1900.0
    upgrade: 1025.0
  }
  used_vespene {
    none: 50.0
    army: 2600.0
    economy: 0.0
    technology: 500.0
    upgrade: 1025.0
  }
  total_used_minerals {
    none: 50.0
    army: 6225.0
    economy: 6750.0
    technology: 1575.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 2975.0
    economy: 0.0
    technology: 400.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 7644.375
    shields: 8051.125
    energy: 0.0
  }
  total_damage_taken {
    life: 4576.89746094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1704.71118164
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1435
vespene: 544
food_cap: 149
food_used: 148
food_army: 97
food_workers: 51
idle_worker_count: 0
army_count: 46
warp_gate_count: 0

game_loop:  16709
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 40
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 29
  count: 1
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 182
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 108
    shields: 0
    energy: 67
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 54
    shields: 0
    energy: 105
    transport_slots_taken: 1
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 132
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 68
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 70
  }
}

Score:  20554
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
