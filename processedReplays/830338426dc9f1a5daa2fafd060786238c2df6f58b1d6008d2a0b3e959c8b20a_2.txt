----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5032
  player_apm: 298
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5163
  player_apm: 398
}
game_duration_loops: 13340
game_duration_seconds: 595.577270508
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11288
score_details {
  idle_production_time: 1031.125
  idle_worker_time: 288.75
  total_value_units: 5725.0
  total_value_structures: 4225.0
  killed_value_units: 1275.0
  killed_value_structures: 0.0
  collected_minerals: 10050.0
  collected_vespene: 2088.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 627.0
  spent_minerals: 9425.0
  spent_vespene: 1975.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1025.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1350.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1550.0
    economy: 5000.0
    technology: 1750.0
    upgrade: 575.0
  }
  used_vespene {
    none: 50.0
    army: 375.0
    economy: 0.0
    technology: 575.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 50.0
    army: 2550.0
    economy: 5050.0
    technology: 1650.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3461.55273438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1840.69873047
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 61.720703125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 675
vespene: 113
food_cap: 85
food_used: 78
food_army: 32
food_workers: 46
idle_worker_count: 1
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 110
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 40
  }
  units {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13930
score_details {
  idle_production_time: 2336.0
  idle_worker_time: 794.25
  total_value_units: 9875.0
  total_value_structures: 5525.0
  killed_value_units: 3600.0
  killed_value_structures: 0.0
  collected_minerals: 14905.0
  collected_vespene: 3700.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 627.0
  spent_minerals: 14475.0
  spent_vespene: 3250.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3075.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3500.0
    economy: 1525.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2600.0
    economy: 4550.0
    technology: 2250.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 725.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 50.0
    army: 5750.0
    economy: 6450.0
    technology: 2100.0
    upgrade: 575.0
  }
  total_used_vespene {
    none: 50.0
    army: 1275.0
    economy: 0.0
    technology: 625.0
    upgrade: 575.0
  }
  total_damage_dealt {
    life: 6855.09521484
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6941.64160156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 762.266113281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 480
vespene: 450
food_cap: 126
food_used: 90
food_army: 51
food_workers: 39
idle_worker_count: 1
army_count: 13
warp_gate_count: 0

game_loop:  13340
ui_data{
 multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 28
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 57
  }
}

Score:  13930
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
