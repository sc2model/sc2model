----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4218
  player_apm: 165
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3855
  player_apm: 235
}
game_duration_loops: 25194
game_duration_seconds: 1124.81066895
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11847
score_details {
  idle_production_time: 1300.3125
  idle_worker_time: 420.4375
  total_value_units: 5050.0
  total_value_structures: 3950.0
  killed_value_units: 775.0
  killed_value_structures: 350.0
  collected_minerals: 8835.0
  collected_vespene: 2112.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 582.0
  spent_minerals: 8550.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 50.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2475.0
    economy: 4750.0
    technology: 1850.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 5100.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2670.08935547
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 367.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 242.224609375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 335
vespene: 412
food_cap: 101
food_used: 92
food_army: 50
food_workers: 40
idle_worker_count: 4
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
cargo {
  unit {
    unit_type: 54
    player_relative: 1
    health: 118
    shields: 0
    energy: 134
    transport_slots_taken: 6
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  slots_available: 2
}

score{
 score_type: Melee
score: 24616
score_details {
  idle_production_time: 5950.125
  idle_worker_time: 3615.625
  total_value_units: 15825.0
  total_value_structures: 7875.0
  killed_value_units: 10350.0
  killed_value_structures: 500.0
  collected_minerals: 23335.0
  collected_vespene: 8156.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 604.0
  spent_minerals: 21625.0
  spent_vespene: 6750.0
  food_used {
    none: 0.0
    army: 94.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6750.0
    economy: 1200.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5200.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5000.0
    economy: 7450.0
    technology: 2875.0
    upgrade: 1450.0
  }
  used_vespene {
    none: 50.0
    army: 2025.0
    economy: 150.0
    technology: 950.0
    upgrade: 1450.0
  }
  total_used_minerals {
    none: 50.0
    army: 9250.0
    economy: 7950.0
    technology: 2875.0
    upgrade: 1450.0
  }
  total_used_vespene {
    none: 50.0
    army: 3475.0
    economy: 0.0
    technology: 950.0
    upgrade: 1450.0
  }
  total_damage_dealt {
    life: 16308.4804688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7170.93994141
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3200.33129883
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1760
vespene: 1406
food_cap: 200
food_used: 154
food_army: 94
food_workers: 60
idle_worker_count: 15
army_count: 50
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 33
  count: 12
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 4
}
single {
  unit {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
    add_on {
      unit_type: 130
      build_progress: 0.0149999856949
    }
  }
}

score{
 score_type: Melee
score: 19962
score_details {
  idle_production_time: 11206.0625
  idle_worker_time: 4801.3125
  total_value_units: 25275.0
  total_value_structures: 8800.0
  killed_value_units: 20075.0
  killed_value_structures: 1850.0
  collected_minerals: 31025.0
  collected_vespene: 10512.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 447.0
  spent_minerals: 28825.0
  spent_vespene: 8650.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12625.0
    economy: 3500.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13850.0
    economy: 1800.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5350.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 850.0
    economy: 50.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2000.0
    economy: 5900.0
    technology: 3225.0
    upgrade: 1450.0
  }
  used_vespene {
    none: 50.0
    army: 450.0
    economy: 150.0
    technology: 1025.0
    upgrade: 1450.0
  }
  total_used_minerals {
    none: 50.0
    army: 16250.0
    economy: 8450.0
    technology: 3475.0
    upgrade: 1450.0
  }
  total_used_vespene {
    none: 50.0
    army: 5875.0
    economy: 300.0
    technology: 1125.0
    upgrade: 1450.0
  }
  total_damage_dealt {
    life: 33721.28125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21540.5527344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4846.54638672
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2250
vespene: 1862
food_cap: 181
food_used: 85
food_army: 39
food_workers: 46
idle_worker_count: 48
army_count: 13
warp_gate_count: 0

game_loop:  25194
ui_data{
 
Score:  19962
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
