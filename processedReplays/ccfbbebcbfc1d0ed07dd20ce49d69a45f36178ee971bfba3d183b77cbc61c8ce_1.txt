----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3157
  player_apm: 92
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3165
  player_apm: 25
}
game_duration_loops: 18242
game_duration_seconds: 814.43182373
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10237
score_details {
  idle_production_time: 1816.4375
  idle_worker_time: 546.75
  total_value_units: 4425.0
  total_value_structures: 4525.0
  killed_value_units: 1175.0
  killed_value_structures: 725.0
  collected_minerals: 8470.0
  collected_vespene: 2292.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 537.0
  spent_minerals: 7825.0
  spent_vespene: 2125.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 475.0
    economy: 925.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 900.0
    economy: 5000.0
    technology: 1625.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 650.0
    economy: 0.0
    technology: 900.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 4950.0
    technology: 1475.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 750.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2802.0
    shields: 3129.5
    energy: 0.0
  }
  total_damage_taken {
    life: 990.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 695
vespene: 167
food_cap: 126
food_used: 58
food_army: 18
food_workers: 40
idle_worker_count: 6
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 2
}
single {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 85
  }
}

score{
 score_type: Melee
score: 24008
score_details {
  idle_production_time: 7648.0625
  idle_worker_time: 1730.5
  total_value_units: 9775.0
  total_value_structures: 6075.0
  killed_value_units: 4125.0
  killed_value_structures: 725.0
  collected_minerals: 18965.0
  collected_vespene: 7768.0
  collection_rate_minerals: 2547.0
  collection_rate_vespene: 963.0
  spent_minerals: 12400.0
  spent_vespene: 4150.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2025.0
    economy: 1425.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2350.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2250.0
    economy: 6050.0
    technology: 2025.0
    upgrade: 625.0
  }
  used_vespene {
    none: 50.0
    army: 1200.0
    economy: 0.0
    technology: 900.0
    upgrade: 625.0
  }
  total_used_minerals {
    none: 50.0
    army: 4600.0
    economy: 6550.0
    technology: 2025.0
    upgrade: 625.0
  }
  total_used_vespene {
    none: 50.0
    army: 2575.0
    economy: 0.0
    technology: 900.0
    upgrade: 625.0
  }
  total_damage_dealt {
    life: 4020.0
    shields: 4543.25
    energy: 0.0
  }
  total_damage_taken {
    life: 2547.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 6615
vespene: 3618
food_cap: 157
food_used: 94
food_army: 43
food_workers: 51
idle_worker_count: 3
army_count: 16
warp_gate_count: 0

game_loop:  18242
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 2
}
single {
  unit {
    unit_type: 691
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
}

Score:  24008
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
