----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3368
  player_apm: 75
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3084
  player_apm: 72
}
game_duration_loops: 6985
game_duration_seconds: 311.852111816
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2162
score_details {
  idle_production_time: 653.125
  idle_worker_time: 551.9375
  total_value_units: 1525.0
  total_value_structures: 1600.0
  killed_value_units: 250.0
  killed_value_structures: 0.0
  collected_minerals: 3125.0
  collected_vespene: 312.0
  collection_rate_minerals: 223.0
  collection_rate_vespene: 0.0
  spent_minerals: 2300.0
  spent_vespene: 75.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 10.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 1200.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 400.0
    economy: 2200.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 182.0
    shields: 389.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3805.75488281
    shields: 3945.75488281
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 875
vespene: 237
food_cap: 15
food_used: 10
food_army: 0
food_workers: 9
idle_worker_count: 5
army_count: 0
warp_gate_count: 0

game_loop:  6985
ui_data{
 
Score:  2162
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
