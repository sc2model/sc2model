----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2917
  player_apm: 159
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2855
  player_apm: 124
}
game_duration_loops: 12343
game_duration_seconds: 551.065246582
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3429
score_details {
  idle_production_time: 1130.5625
  idle_worker_time: 527.375
  total_value_units: 3150.0
  total_value_structures: 1575.0
  killed_value_units: 250.0
  killed_value_structures: 1050.0
  collected_minerals: 3755.0
  collected_vespene: 724.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 179.0
  spent_minerals: 3725.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 550.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 475.0
    economy: 1025.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 575.0
    economy: 1900.0
    technology: 450.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1050.0
    economy: 2475.0
    technology: 650.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1483.32397461
    shields: 1953.91162109
    energy: 0.0
  }
  total_damage_taken {
    life: 2429.75
    shields: 3083.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 80
vespene: 124
food_cap: 31
food_used: 25
food_army: 10
food_workers: 14
idle_worker_count: 1
army_count: 5
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 74
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

score{
 score_type: Melee
score: 2237
score_details {
  idle_production_time: 1401.625
  idle_worker_time: 681.125
  total_value_units: 4600.0
  total_value_structures: 1975.0
  killed_value_units: 1475.0
  killed_value_structures: 1050.0
  collected_minerals: 4735.0
  collected_vespene: 952.0
  collection_rate_minerals: 195.0
  collection_rate_vespene: 0.0
  spent_minerals: 4725.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 600.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1725.0
    economy: 1725.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 225.0
    economy: 1350.0
    technology: 450.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1950.0
    economy: 3075.0
    technology: 650.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 2231.32055664
    shields: 2910.53320312
    energy: 0.0
  }
  total_damage_taken {
    life: 3542.0
    shields: 4854.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 60
vespene: 2
food_cap: 46
food_used: 8
food_army: 4
food_workers: 4
idle_worker_count: 4
army_count: 2
warp_gate_count: 2

game_loop:  12343
ui_data{
 
Score:  2237
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
