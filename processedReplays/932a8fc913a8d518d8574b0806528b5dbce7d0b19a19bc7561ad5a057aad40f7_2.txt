----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2901
  player_apm: 71
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2857
  player_apm: 72
}
game_duration_loops: 17816
game_duration_seconds: 795.412658691
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9846
score_details {
  idle_production_time: 4588.5625
  idle_worker_time: 12.9375
  total_value_units: 5950.0
  total_value_structures: 2475.0
  killed_value_units: 0.0
  killed_value_structures: 350.0
  collected_minerals: 8595.0
  collected_vespene: 1376.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 403.0
  spent_minerals: 7900.0
  spent_vespene: 1200.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 750.0
    economy: 4225.0
    technology: 2550.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 400.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 5275.0
    technology: 2450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 550.0
    shields: 1531.625
    energy: 0.0
  }
  total_damage_taken {
    life: 1963.00292969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 745
vespene: 176
food_cap: 98
food_used: 49
food_army: 10
food_workers: 39
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12325
score_details {
  idle_production_time: 17717.5
  idle_worker_time: 12.9375
  total_value_units: 16000.0
  total_value_structures: 3300.0
  killed_value_units: 1560.0
  killed_value_structures: 450.0
  collected_minerals: 16275.0
  collected_vespene: 6500.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 963.0
  spent_minerals: 15775.0
  spent_vespene: 5425.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 860.0
    economy: 350.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6400.0
    economy: 1125.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 825.0
    economy: 3950.0
    technology: 2950.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 750.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 7225.0
    economy: 6600.0
    technology: 3300.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 3675.0
    economy: 0.0
    technology: 1000.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 1520.0
    shields: 2794.625
    energy: 0.0
  }
  total_damage_taken {
    life: 12869.8554688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 550
vespene: 1075
food_cap: 58
food_used: 53
food_army: 14
food_workers: 39
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  17816
ui_data{
 multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
}

Score:  12325
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
