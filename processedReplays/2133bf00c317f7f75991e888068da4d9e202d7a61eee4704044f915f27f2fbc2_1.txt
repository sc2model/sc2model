----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4164
  player_apm: 139
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4145
  player_apm: 135
}
game_duration_loops: 16319
game_duration_seconds: 728.577636719
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11176
score_details {
  idle_production_time: 2148.0625
  idle_worker_time: 103.9375
  total_value_units: 4275.0
  total_value_structures: 4700.0
  killed_value_units: 950.0
  killed_value_structures: 0.0
  collected_minerals: 9300.0
  collected_vespene: 2012.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 649.0
  spent_minerals: 7411.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 75.0
    technology: -189.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 925.0
    economy: 4300.0
    technology: 2150.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1375.0
    economy: 4450.0
    technology: 2150.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1256.30371094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 643.362548828
    shields: 1160.6862793
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1939
vespene: 862
food_cap: 110
food_used: 60
food_army: 16
food_workers: 44
idle_worker_count: 0
army_count: 8
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 488
  count: 1
}
single {
  unit {
    unit_type: 105
    player_relative: 4
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10168
score_details {
  idle_production_time: 4553.0
  idle_worker_time: 503.875
  total_value_units: 12225.0
  total_value_structures: 5850.0
  killed_value_units: 6525.0
  killed_value_structures: 250.0
  collected_minerals: 16395.0
  collected_vespene: 4884.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 604.0
  spent_minerals: 15211.0
  spent_vespene: 3850.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5575.0
    economy: 600.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6575.0
    economy: 1375.0
    technology: 711.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 550.0
    economy: 3950.0
    technology: 1850.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 550.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 7250.0
    economy: 5400.0
    technology: 2300.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 3025.0
    economy: 0.0
    technology: 550.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 10255.4082031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10856.6123047
    shields: 10975.8974609
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1234
vespene: 1034
food_cap: 102
food_used: 49
food_army: 10
food_workers: 39
idle_worker_count: 39
army_count: 3
warp_gate_count: 4

game_loop:  16319
ui_data{
 
Score:  10168
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
