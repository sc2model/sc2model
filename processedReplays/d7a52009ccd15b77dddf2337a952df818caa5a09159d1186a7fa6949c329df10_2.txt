----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3886
  player_apm: 146
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3954
  player_apm: 265
}
game_duration_loops: 12857
game_duration_seconds: 574.013244629
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5227
score_details {
  idle_production_time: 7984.6875
  idle_worker_time: 11.375
  total_value_units: 5850.0
  total_value_structures: 900.0
  killed_value_units: 3025.0
  killed_value_structures: 0.0
  collected_minerals: 6820.0
  collected_vespene: 588.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 179.0
  spent_minerals: 6406.0
  spent_vespene: 450.0
  food_used {
    none: 0.0
    army: 15.5
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3175.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 875.0
    economy: 2425.0
    technology: 525.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3900.0
    economy: 2725.0
    technology: 375.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 5068.140625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6100.62207031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 464
vespene: 138
food_cap: 44
food_used: 40
food_army: 15
food_workers: 25
idle_worker_count: 0
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 36
  }
}

score{
 score_type: Melee
score: 7434
score_details {
  idle_production_time: 13264.5
  idle_worker_time: 11.375
  total_value_units: 7800.0
  total_value_structures: 975.0
  killed_value_units: 4875.0
  killed_value_structures: 0.0
  collected_minerals: 9210.0
  collected_vespene: 1180.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 358.0
  spent_minerals: 8881.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4200.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3950.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 3350.0
    technology: 1075.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 4950.0
    economy: 3850.0
    technology: 675.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 7832.70458984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7659.515625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 379
vespene: 530
food_cap: 92
food_used: 45
food_army: 20
food_workers: 25
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  12857
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 24
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
}

Score:  7434
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
