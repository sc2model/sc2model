----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3233
  player_apm: 134
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3261
  player_apm: 92
}
game_duration_loops: 22759
game_duration_seconds: 1016.09771729
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12783
score_details {
  idle_production_time: 971.1875
  idle_worker_time: 706.5
  total_value_units: 5325.0
  total_value_structures: 4350.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9225.0
  collected_vespene: 2708.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 649.0
  spent_minerals: 8400.0
  spent_vespene: 2525.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 5300.0
    technology: 1500.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 450.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4800.0
    technology: 1650.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 450.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 236.5625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 78.0
    shields: 60.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 875
vespene: 183
food_cap: 110
food_used: 88
food_army: 35
food_workers: 51
idle_worker_count: 0
army_count: 12
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 31696
score_details {
  idle_production_time: 5585.0625
  idle_worker_time: 1744.6875
  total_value_units: 19215.0
  total_value_structures: 9925.0
  killed_value_units: 3425.0
  killed_value_structures: 750.0
  collected_minerals: 25105.0
  collected_vespene: 10736.0
  collection_rate_minerals: 2435.0
  collection_rate_vespene: 1007.0
  spent_minerals: 22550.0
  spent_vespene: 10200.0
  food_used {
    none: 0.0
    army: 122.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2275.0
    economy: 900.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3885.0
    economy: 425.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6380.0
    economy: 7300.0
    technology: 4350.0
    upgrade: 1525.0
  }
  used_vespene {
    none: 0.0
    army: 6525.0
    economy: 0.0
    technology: 950.0
    upgrade: 1525.0
  }
  total_used_minerals {
    none: 0.0
    army: 9865.0
    economy: 7725.0
    technology: 4800.0
    upgrade: 1525.0
  }
  total_used_vespene {
    none: 0.0
    army: 8350.0
    economy: 0.0
    technology: 950.0
    upgrade: 1525.0
  }
  total_damage_dealt {
    life: 9916.54199219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5236.0
    shields: 5527.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2605
vespene: 536
food_cap: 200
food_used: 188
food_army: 122
food_workers: 66
idle_worker_count: 3
army_count: 31
warp_gate_count: 12

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 80
  count: 15
}
groups {
  control_group_index: 2
  leader_unit_type: 80
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 67
  count: 3
}
multi {
  units {
    unit_type: 10
    player_relative: 1
    health: 350
    shields: 350
    energy: 200
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 50
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 57
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.380208313465
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 330
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 173
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 99
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 91
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 91
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 91
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.302083313465
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.0572916865349
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 91
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.69791662693
  }
}

score{
 score_type: Melee
score: 34231
score_details {
  idle_production_time: 5813.25
  idle_worker_time: 2018.1875
  total_value_units: 24295.0
  total_value_structures: 11075.0
  killed_value_units: 15800.0
  killed_value_structures: 1050.0
  collected_minerals: 29930.0
  collected_vespene: 12736.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 940.0
  spent_minerals: 27695.0
  spent_vespene: 12150.0
  food_used {
    none: 0.0
    army: 126.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10850.0
    economy: 900.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4400.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5605.0
    economy: 425.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: -75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 230.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7610.0
    economy: 8550.0
    technology: 4550.0
    upgrade: 2000.0
  }
  used_vespene {
    none: 0.0
    army: 5600.0
    economy: 0.0
    technology: 1050.0
    upgrade: 2000.0
  }
  total_used_minerals {
    none: 0.0
    army: 14045.0
    economy: 8575.0
    technology: 5000.0
    upgrade: 1525.0
  }
  total_used_vespene {
    none: 0.0
    army: 11050.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1525.0
  }
  total_damage_dealt {
    life: 22127.2558594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6843.125
    shields: 9260.4609375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2285
vespene: 586
food_cap: 200
food_used: 192
food_army: 126
food_workers: 66
idle_worker_count: 7
army_count: 34
warp_gate_count: 12

game_loop:  22759
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 10
  count: 19
}
groups {
  control_group_index: 2
  leader_unit_type: 80
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 10
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 67
  count: 3
}
multi {
  units {
    unit_type: 75
    player_relative: 1
    health: 16
    shields: 40
    energy: 39
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 26
    energy: 89
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 23
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 38
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 16
    shields: 40
    energy: 38
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 89
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 15
    shields: 40
    energy: 38
  }
}

Score:  34231
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
