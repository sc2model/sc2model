----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3596
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3463
  player_apm: 145
}
game_duration_loops: 19170
game_duration_seconds: 855.86328125
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12641
score_details {
  idle_production_time: 1229.3125
  idle_worker_time: 185.5
  total_value_units: 6550.0
  total_value_structures: 4900.0
  killed_value_units: 175.0
  killed_value_structures: 0.0
  collected_minerals: 9225.0
  collected_vespene: 2616.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 649.0
  spent_minerals: 9025.0
  spent_vespene: 2525.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2025.0
    economy: 5150.0
    technology: 2100.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 300.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2225.0
    economy: 5150.0
    technology: 2100.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 1675.0
    economy: 0.0
    technology: 300.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 353.614257812
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 140.0
    shields: 235.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 250
vespene: 91
food_cap: 125
food_used: 101
food_army: 48
food_workers: 53
idle_worker_count: 0
army_count: 24
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 84
  count: 1
}
production {
  unit {
    unit_type: 63
    player_relative: 1
    health: 400
    shields: 400
    energy: 0
  }
}

score{
 score_type: Melee
score: 13084
score_details {
  idle_production_time: 5432.125
  idle_worker_time: 1649.375
  total_value_units: 19800.0
  total_value_structures: 6850.0
  killed_value_units: 8900.0
  killed_value_structures: 350.0
  collected_minerals: 21645.0
  collected_vespene: 8364.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 694.0
  spent_minerals: 21275.0
  spent_vespene: 7475.0
  food_used {
    none: 0.0
    army: 5.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5975.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10825.0
    economy: 1650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 225.0
    economy: 6550.0
    technology: 2450.0
    upgrade: 950.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 550.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 0.0
    army: 11550.0
    economy: 7000.0
    technology: 2450.0
    upgrade: 950.0
  }
  total_used_vespene {
    none: 0.0
    army: 7350.0
    economy: 0.0
    technology: 550.0
    upgrade: 950.0
  }
  total_damage_dealt {
    life: 12594.4316406
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10660.7001953
    shields: 11685.7001953
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 420
vespene: 889
food_cap: 149
food_used: 61
food_army: 5
food_workers: 56
idle_worker_count: 20
army_count: 3
warp_gate_count: 9

game_loop:  19170
ui_data{
 
Score:  13084
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
