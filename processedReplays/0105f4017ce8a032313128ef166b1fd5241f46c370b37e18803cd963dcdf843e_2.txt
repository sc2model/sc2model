----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3743
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3811
  player_apm: 103
}
game_duration_loops: 15561
game_duration_seconds: 694.735961914
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10390
score_details {
  idle_production_time: 1867.0
  idle_worker_time: 460.3125
  total_value_units: 5000.0
  total_value_structures: 3950.0
  killed_value_units: 325.0
  killed_value_structures: 0.0
  collected_minerals: 7795.0
  collected_vespene: 2020.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 604.0
  spent_minerals: 7450.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: -25.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1875.0
    economy: 3950.0
    technology: 2150.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1925.0
    economy: 3950.0
    technology: 1700.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 145.0
    shields: 175.0
    energy: 0.0
  }
  total_damage_taken {
    life: 184.0
    shields: 336.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 395
vespene: 495
food_cap: 78
food_used: 78
food_army: 33
food_workers: 44
idle_worker_count: 2
army_count: 15
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 5
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 13447
score_details {
  idle_production_time: 4587.5625
  idle_worker_time: 1816.0
  total_value_units: 10350.0
  total_value_structures: 5300.0
  killed_value_units: 11375.0
  killed_value_structures: 550.0
  collected_minerals: 14375.0
  collected_vespene: 4372.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 559.0
  spent_minerals: 13350.0
  spent_vespene: 2425.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7825.0
    economy: 650.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5100.0
    economy: -25.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 5750.0
    technology: 2150.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 6750.0
    economy: 5000.0
    technology: 2150.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 6803.0
    shields: 7325.5
    energy: 0.0
  }
  total_damage_taken {
    life: 4685.875
    shields: 5079.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1075
vespene: 1947
food_cap: 150
food_used: 70
food_army: 23
food_workers: 47
idle_worker_count: 4
army_count: 12
warp_gate_count: 8

game_loop:  15561
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 8
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 55
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 33
    shields: 3
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 61
    shields: 24
    energy: 0
  }
}

Score:  13447
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
