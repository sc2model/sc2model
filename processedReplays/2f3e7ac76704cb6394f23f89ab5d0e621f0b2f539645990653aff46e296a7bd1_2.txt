----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3536
  player_apm: 90
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3519
  player_apm: 229
}
game_duration_loops: 10345
game_duration_seconds: 461.862579346
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9733
score_details {
  idle_production_time: 9778.25
  idle_worker_time: 19.125
  total_value_units: 6000.0
  total_value_structures: 2000.0
  killed_value_units: 925.0
  killed_value_structures: 0.0
  collected_minerals: 8715.0
  collected_vespene: 1972.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 627.0
  spent_minerals: 8330.0
  spent_vespene: 1324.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 974.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 24.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -200.0
    army: 1250.0
    economy: 4350.0
    technology: 1550.0
    upgrade: 750.0
  }
  used_vespene {
    none: -100.0
    army: 50.0
    economy: 0.0
    technology: 250.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 5200.0
    technology: 1550.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 706.0
    shields: 1163.25
    energy: 0.0
  }
  total_damage_taken {
    life: 1931.38769531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 435
vespene: 648
food_cap: 82
food_used: 63
food_army: 19
food_workers: 44
idle_worker_count: 0
army_count: 1
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 103
  count: 13
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8504
score_details {
  idle_production_time: 10383.375
  idle_worker_time: 19.125
  total_value_units: 6650.0
  total_value_structures: 2000.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 9100.0
  collected_vespene: 2108.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 492.0
  spent_minerals: 8330.0
  spent_vespene: 1324.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1674.0
    economy: 981.0
    technology: -125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 24.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -200.0
    army: 550.0
    economy: 3450.0
    technology: 1400.0
    upgrade: 750.0
  }
  used_vespene {
    none: -100.0
    army: 50.0
    economy: 0.0
    technology: 250.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 5200.0
    technology: 1550.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 852.25
    shields: 1491.375
    energy: 0.0
  }
  total_damage_taken {
    life: 3832.07666016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 820
vespene: 784
food_cap: 82
food_used: 32
food_army: 6
food_workers: 26
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  10345
ui_data{
 
Score:  8504
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
