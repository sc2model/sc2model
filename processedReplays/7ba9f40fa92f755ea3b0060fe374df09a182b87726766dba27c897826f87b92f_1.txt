----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2817
  player_apm: 54
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2833
  player_apm: 141
}
game_duration_loops: 14653
game_duration_seconds: 654.19744873
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9128
score_details {
  idle_production_time: 1769.0
  idle_worker_time: 192.0625
  total_value_units: 4500.0
  total_value_structures: 3075.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 6790.0
  collected_vespene: 2088.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 582.0
  spent_minerals: 6150.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1800.0
    economy: 3400.0
    technology: 950.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 525.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 3650.0
    technology: 950.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 525.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 388.392089844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 870.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 690
vespene: 238
food_cap: 70
food_used: 66
food_army: 36
food_workers: 29
idle_worker_count: 1
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7784
score_details {
  idle_production_time: 3907.3125
  idle_worker_time: 234.3125
  total_value_units: 7750.0
  total_value_structures: 3650.0
  killed_value_units: 4875.0
  killed_value_structures: 0.0
  collected_minerals: 10995.0
  collected_vespene: 4364.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 515.0
  spent_minerals: 10325.0
  spent_vespene: 4025.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3200.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3350.0
    economy: 875.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 750.0
    economy: 3350.0
    technology: 950.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 525.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 3800.0
    economy: 4925.0
    technology: 950.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 525.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 4908.15966797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6964.5
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 720
vespene: 339
food_cap: 61
food_used: 39
food_army: 15
food_workers: 23
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  14653
ui_data{
 
Score:  7784
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
