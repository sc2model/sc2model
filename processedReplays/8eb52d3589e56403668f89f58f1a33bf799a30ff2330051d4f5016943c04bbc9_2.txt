----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3113
  player_apm: 145
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3155
  player_apm: 81
}
game_duration_loops: 11082
game_duration_seconds: 494.766662598
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8209
score_details {
  idle_production_time: 1340.4375
  idle_worker_time: 560.0
  total_value_units: 4650.0
  total_value_structures: 3275.0
  killed_value_units: 150.0
  killed_value_structures: 850.0
  collected_minerals: 6230.0
  collected_vespene: 1604.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 335.0
  spent_minerals: 6075.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 550.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 225.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1900.0
    economy: 3150.0
    technology: 1400.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1975.0
    economy: 3375.0
    technology: 1400.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1310.0
    shields: 1685.75
    energy: 0.0
  }
  total_damage_taken {
    life: 1278.0
    shields: 1200.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 205
vespene: 329
food_cap: 78
food_used: 63
food_army: 31
food_workers: 32
idle_worker_count: 1
army_count: 13
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 80
  count: 13
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 2
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
}

score{
 score_type: Melee
score: 8579
score_details {
  idle_production_time: 1667.625
  idle_worker_time: 652.125
  total_value_units: 5200.0
  total_value_structures: 3525.0
  killed_value_units: 1000.0
  killed_value_structures: 2050.0
  collected_minerals: 7240.0
  collected_vespene: 1864.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 313.0
  spent_minerals: 6650.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 1250.0
    technology: 1050.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 625.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1925.0
    economy: 3050.0
    technology: 1250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2225.0
    economy: 3675.0
    technology: 1550.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3627.98364258
    shields: 4467.890625
    energy: 0.0
  }
  total_damage_taken {
    life: 1763.0
    shields: 1890.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 640
vespene: 414
food_cap: 86
food_used: 60
food_army: 32
food_workers: 28
idle_worker_count: 1
army_count: 13
warp_gate_count: 2

game_loop:  11082
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 80
  count: 12
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 2
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 873
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
}

Score:  8579
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
