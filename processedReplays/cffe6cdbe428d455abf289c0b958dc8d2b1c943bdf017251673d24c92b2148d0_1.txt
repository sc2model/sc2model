----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2654
  player_apm: 44
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2568
  player_apm: 110
}
game_duration_loops: 16785
game_duration_seconds: 749.382629395
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8137
score_details {
  idle_production_time: 2205.625
  idle_worker_time: 408.4375
  total_value_units: 3150.0
  total_value_structures: 3300.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 6360.0
  collected_vespene: 1320.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 515.0
  spent_minerals: 6337.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 137.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 3300.0
    technology: 1750.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 150.0
    technology: 400.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 3600.0
    technology: 1500.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 300.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 620.851318359
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 764.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 140.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 67
vespene: 195
food_cap: 62
food_used: 60
food_army: 30
food_workers: 28
idle_worker_count: 0
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 5
}
production {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 171
  }
  build_queue {
    unit_type: 45
    build_progress: 0.257352948189
  }
}

score{
 score_type: Melee
score: 7136
score_details {
  idle_production_time: 5881.9375
  idle_worker_time: 673.9375
  total_value_units: 5600.0
  total_value_structures: 3400.0
  killed_value_units: 1500.0
  killed_value_structures: 200.0
  collected_minerals: 9000.0
  collected_vespene: 2552.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 223.0
  spent_minerals: 8037.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 3024.0
    economy: 1862.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 474.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 0.0
    economy: 2125.0
    technology: 1750.0
    upgrade: 300.0
  }
  used_vespene {
    none: -50.0
    army: 0.0
    economy: 150.0
    technology: 400.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 3000.0
    economy: 4250.0
    technology: 1600.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 300.0
    technology: 300.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 3939.49047852
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9081.20996094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 355.143798828
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 983
vespene: 1228
food_cap: 47
food_used: 19
food_army: 0
food_workers: 19
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  16785
ui_data{
 
Score:  7136
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
