----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2693
  player_apm: 83
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2859
  player_apm: 85
}
game_duration_loops: 13981
game_duration_seconds: 624.1953125
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7185
score_details {
  idle_production_time: 9586.875
  idle_worker_time: 103.25
  total_value_units: 5100.0
  total_value_structures: 975.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 6190.0
  collected_vespene: 720.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 156.0
  spent_minerals: 5800.0
  spent_vespene: 425.0
  food_used {
    none: 0.0
    army: 32.5
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 475.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1550.0
    economy: 3775.0
    technology: 600.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 100.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 3875.0
    technology: 750.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1116.66113281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1471.49365234
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 440
vespene: 295
food_cap: 76
food_used: 66
food_army: 32
food_workers: 32
idle_worker_count: 0
army_count: 25
warp_gate_count: 0
larva_count: 1

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12826
score_details {
  idle_production_time: 17650.6875
  idle_worker_time: 103.25
  total_value_units: 8600.0
  total_value_structures: 1525.0
  killed_value_units: 2000.0
  killed_value_structures: 350.0
  collected_minerals: 11100.0
  collected_vespene: 1876.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 470.0
  spent_minerals: 8725.0
  spent_vespene: 1050.0
  food_used {
    none: 0.0
    army: 63.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2650.0
    economy: 4925.0
    technology: 750.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3450.0
    economy: 5775.0
    technology: 900.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 4199.62304688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2734.95019531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2425
vespene: 826
food_cap: 146
food_used: 104
food_army: 63
food_workers: 41
idle_worker_count: 0
army_count: 36
warp_gate_count: 0

game_loop:  13981
ui_data{
 multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  12826
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
