----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3766
  player_apm: 218
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3690
  player_apm: 104
}
game_duration_loops: 18810
game_duration_seconds: 839.790771484
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8828
score_details {
  idle_production_time: 1603.6875
  idle_worker_time: 374.875
  total_value_units: 4050.0
  total_value_structures: 2825.0
  killed_value_units: 1700.0
  killed_value_structures: 0.0
  collected_minerals: 6195.0
  collected_vespene: 2260.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 604.0
  spent_minerals: 5825.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1300.0
    economy: 3600.0
    technology: 1225.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 3900.0
    technology: 925.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1440.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1572.125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 587.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 362
vespene: 416
food_cap: 70
food_used: 59
food_army: 25
food_workers: 33
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 56
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 126
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 120
  }
}

score{
 score_type: Melee
score: 21575
score_details {
  idle_production_time: 5304.125
  idle_worker_time: 1080.875
  total_value_units: 10200.0
  total_value_structures: 5725.0
  killed_value_units: 4925.0
  killed_value_structures: 0.0
  collected_minerals: 15545.0
  collected_vespene: 6432.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 627.0
  spent_minerals: 12750.0
  spent_vespene: 4275.0
  food_used {
    none: 0.0
    army: 74.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2350.0
    economy: 1600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 928.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 444.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3850.0
    economy: 6050.0
    technology: 2075.0
    upgrade: 825.0
  }
  used_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 550.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 0.0
    army: 4750.0
    economy: 6250.0
    technology: 2075.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: 550.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 5342.20898438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3443.29150391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1232.85253906
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2787
vespene: 2138
food_cap: 173
food_used: 124
food_army: 74
food_workers: 50
idle_worker_count: 6
army_count: 43
warp_gate_count: 0

game_loop:  18810
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 48
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 56
  count: 25
}
groups {
  control_group_index: 2
  leader_unit_type: 32
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 734
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

Score:  21575
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
