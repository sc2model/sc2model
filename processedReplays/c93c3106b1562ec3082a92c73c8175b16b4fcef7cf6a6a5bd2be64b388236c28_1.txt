----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3217
  player_apm: 162
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3114
  player_apm: 111
}
game_duration_loops: 6584
game_duration_seconds: 293.94909668
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5967
score_details {
  idle_production_time: 4237.375
  idle_worker_time: 2.625
  total_value_units: 4050.0
  total_value_structures: 975.0
  killed_value_units: 1500.0
  killed_value_structures: 0.0
  collected_minerals: 4980.0
  collected_vespene: 512.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 156.0
  spent_minerals: 4725.0
  spent_vespene: 350.0
  food_used {
    none: 0.0
    army: 29.5
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 2975.0
    technology: 600.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 3125.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2852.93359375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 262.41015625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 305
vespene: 162
food_cap: 60
food_used: 59
food_army: 29
food_workers: 30
idle_worker_count: 0
army_count: 19
warp_gate_count: 0
larva_count: 1

game_loop:  6584
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 28
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  5967
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
