----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3652
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3637
  player_apm: 100
}
game_duration_loops: 13738
game_duration_seconds: 613.346374512
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3606
score_details {
  idle_production_time: 1790.875
  idle_worker_time: 482.4375
  total_value_units: 2675.0
  total_value_structures: 2200.0
  killed_value_units: 1325.0
  killed_value_structures: 0.0
  collected_minerals: 3800.0
  collected_vespene: 756.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 3775.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 1450.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 1450.0
    technology: 625.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 225.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 3200.0
    technology: 725.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 225.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 566.0
    shields: 800.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4611.44140625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 75
vespene: 106
food_cap: 46
food_used: 15
food_army: 15
food_workers: 0
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 134
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 134
  count: 2
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 805
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 370
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4136
score_details {
  idle_production_time: 2870.5625
  idle_worker_time: 561.3125
  total_value_units: 3400.0
  total_value_structures: 2200.0
  killed_value_units: 1725.0
  killed_value_structures: 0.0
  collected_minerals: 4915.0
  collected_vespene: 756.0
  collection_rate_minerals: 195.0
  collection_rate_vespene: 0.0
  spent_minerals: 4650.0
  spent_vespene: 725.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 7.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 1625.0
    technology: 375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 825.0
    economy: 1725.0
    technology: 500.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 225.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1225.0
    economy: 3600.0
    technology: 725.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 225.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 743.0
    shields: 1062.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7496.44140625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 711.299804688
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 255
vespene: 31
food_cap: 46
food_used: 26
food_army: 19
food_workers: 6
idle_worker_count: 4
army_count: 8
warp_gate_count: 0

game_loop:  13738
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  4136
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
