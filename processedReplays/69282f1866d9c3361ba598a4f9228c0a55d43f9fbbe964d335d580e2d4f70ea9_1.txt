----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4059
  player_apm: 139
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3800
  player_apm: 200
}
game_duration_loops: 11393
game_duration_seconds: 508.651550293
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8130
score_details {
  idle_production_time: 1437.3125
  idle_worker_time: 568.375
  total_value_units: 5625.0
  total_value_structures: 3250.0
  killed_value_units: 3100.0
  killed_value_structures: 0.0
  collected_minerals: 7930.0
  collected_vespene: 1500.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 313.0
  spent_minerals: 7550.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2050.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 850.0
    economy: 900.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 3400.0
    technology: 1275.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2525.0
    economy: 4500.0
    technology: 1375.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 325.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1495.5
    shields: 1484.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2217.56542969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 183.774414062
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 430
vespene: 225
food_cap: 78
food_used: 63
food_army: 34
food_workers: 29
idle_worker_count: 2
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 48
  count: 18
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 995
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 384
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 9395
score_details {
  idle_production_time: 2016.375
  idle_worker_time: 800.625
  total_value_units: 6550.0
  total_value_structures: 3350.0
  killed_value_units: 5125.0
  killed_value_structures: 600.0
  collected_minerals: 9105.0
  collected_vespene: 1840.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 335.0
  spent_minerals: 8700.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2850.0
    economy: 1700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1150.0
    economy: 900.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2075.0
    economy: 3550.0
    technology: 1575.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3175.0
    economy: 4750.0
    technology: 1375.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3895.0
    shields: 4407.125
    energy: 0.0
  }
  total_damage_taken {
    life: 2630.52636719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 669.460449219
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 455
vespene: 315
food_cap: 86
food_used: 74
food_army: 42
food_workers: 32
idle_worker_count: 2
army_count: 29
warp_gate_count: 0

game_loop:  11393
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 48
  count: 13
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 131
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 10
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

Score:  9395
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
