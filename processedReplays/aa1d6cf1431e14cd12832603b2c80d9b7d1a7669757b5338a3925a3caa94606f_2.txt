----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3951
  player_apm: 107
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4335
  player_apm: 146
}
game_duration_loops: 24722
game_duration_seconds: 1103.73779297
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7294
score_details {
  idle_production_time: 1561.0625
  idle_worker_time: 629.25
  total_value_units: 4725.0
  total_value_structures: 3225.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 6745.0
  collected_vespene: 1572.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 201.0
  spent_minerals: 6650.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 875.0
    economy: 550.0
    technology: -102.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1475.0
    economy: 2850.0
    technology: 1275.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 575.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 2250.0
    economy: 3600.0
    technology: 1375.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 925.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 510.0
    shields: 694.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3141.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 313.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 122
vespene: 197
food_cap: 54
food_used: 54
food_army: 30
food_workers: 24
idle_worker_count: 2
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 59
    shields: 0
    energy: 139
    transport_slots_taken: 7
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 62
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 62
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13981
score_details {
  idle_production_time: 6510.625
  idle_worker_time: 3141.25
  total_value_units: 13500.0
  total_value_structures: 5875.0
  killed_value_units: 8325.0
  killed_value_structures: 1250.0
  collected_minerals: 17460.0
  collected_vespene: 4244.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 403.0
  spent_minerals: 17150.0
  spent_vespene: 3475.0
  food_used {
    none: 0.0
    army: 70.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5175.0
    economy: 1600.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5450.0
    economy: 1450.0
    technology: -2.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 3500.0
    economy: 4850.0
    technology: 2250.0
    upgrade: 350.0
  }
  used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 525.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 50.0
    army: 8500.0
    economy: 6800.0
    technology: 2350.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 50.0
    army: 2300.0
    economy: 0.0
    technology: 525.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 6805.0
    shields: 9105.37402344
    energy: 0.0
  }
  total_damage_taken {
    life: 9907.68945312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2807.24584961
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 337
vespene: 769
food_cap: 125
food_used: 110
food_army: 70
food_workers: 40
idle_worker_count: 2
army_count: 41
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 14541
score_details {
  idle_production_time: 9867.75
  idle_worker_time: 4670.8125
  total_value_units: 18775.0
  total_value_structures: 6625.0
  killed_value_units: 11175.0
  killed_value_structures: 1575.0
  collected_minerals: 23370.0
  collected_vespene: 5944.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 447.0
  spent_minerals: 23175.0
  spent_vespene: 5100.0
  food_used {
    none: 0.0
    army: 49.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6800.0
    economy: 2075.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10400.0
    economy: 2000.0
    technology: -2.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 350.0
    army: 2550.0
    economy: 5200.0
    technology: 2350.0
    upgrade: 800.0
  }
  used_vespene {
    none: 50.0
    army: 700.0
    economy: 150.0
    technology: 525.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 50.0
    army: 12550.0
    economy: 7900.0
    technology: 2450.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 3375.0
    economy: 300.0
    technology: 525.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 9165.375
    shields: 13185.3740234
    energy: 0.0
  }
  total_damage_taken {
    life: 14294.5878906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3970.98901367
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 222
vespene: 844
food_cap: 148
food_used: 82
food_army: 49
food_workers: 32
idle_worker_count: 3
army_count: 12
warp_gate_count: 0

game_loop:  24722
ui_data{
 
Score:  14541
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
