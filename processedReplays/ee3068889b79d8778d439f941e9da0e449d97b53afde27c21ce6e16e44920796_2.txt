----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4590
  player_apm: 186
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4861
  player_apm: 68
}
game_duration_loops: 13717
game_duration_seconds: 612.408813477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7296
score_details {
  idle_production_time: 1955.75
  idle_worker_time: 357.75
  total_value_units: 3825.0
  total_value_structures: 3075.0
  killed_value_units: 2550.0
  killed_value_structures: 400.0
  collected_minerals: 5955.0
  collected_vespene: 1716.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 313.0
  spent_minerals: 4900.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 525.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 2575.0
    technology: 1050.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1475.0
    economy: 3175.0
    technology: 1050.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2285.0
    shields: 3072.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1608.37353516
    shields: 2788.50146484
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1105
vespene: 416
food_cap: 54
food_used: 43
food_army: 18
food_workers: 25
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 80
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 84
  count: 10
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 2
}
multi {
  units {
    unit_type: 80
    player_relative: 1
    health: 49
    shields: 47
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 100
    shields: 91
    energy: 0
  }
}

score{
 score_type: Melee
score: 10068
score_details {
  idle_production_time: 3059.4375
  idle_worker_time: 774.25
  total_value_units: 6250.0
  total_value_structures: 4850.0
  killed_value_units: 5300.0
  killed_value_structures: 1350.0
  collected_minerals: 9105.0
  collected_vespene: 2588.0
  collection_rate_minerals: 1601.0
  collection_rate_vespene: 537.0
  spent_minerals: 8775.0
  spent_vespene: 2500.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2100.0
    economy: 3200.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1250.0
    economy: 525.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1975.0
    economy: 3875.0
    technology: 1800.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 550.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 4350.0
    technology: 1800.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 550.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 5994.125
    shields: 6863.5
    energy: 0.0
  }
  total_damage_taken {
    life: 1909.12353516
    shields: 3444.50146484
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 380
vespene: 88
food_cap: 93
food_used: 66
food_army: 32
food_workers: 33
idle_worker_count: 0
army_count: 12
warp_gate_count: 4

game_loop:  13717
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 80
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 84
  count: 8
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 2
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  10068
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
