----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3550
  player_apm: 109
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3529
  player_apm: 54
}
game_duration_loops: 8839
game_duration_seconds: 394.625762939
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6039
score_details {
  idle_production_time: 1013.1875
  idle_worker_time: 286.1875
  total_value_units: 2100.0
  total_value_structures: 3150.0
  killed_value_units: 825.0
  killed_value_structures: 0.0
  collected_minerals: 4779.0
  collected_vespene: 1364.0
  collection_rate_minerals: 677.0
  collection_rate_vespene: 134.0
  spent_minerals: 4225.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 325.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 75.0
    economy: 700.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 350.0
    economy: 2675.0
    technology: 1175.0
    upgrade: 0.0
  }
  used_vespene {
    none: 50.0
    army: 350.0
    economy: 300.0
    technology: 350.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 325.0
    economy: 3625.0
    technology: 1225.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 600.0
    technology: 400.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 643.0
    shields: 1167.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3879.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 802.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 523
vespene: 216
food_cap: 54
food_used: 28
food_army: 7
food_workers: 20
idle_worker_count: 13
army_count: 3
warp_gate_count: 0

game_loop:  8839
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 49
  count: 2
}

Score:  6039
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
