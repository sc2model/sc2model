----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4575
  player_apm: 317
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4565
  player_apm: 236
}
game_duration_loops: 24177
game_duration_seconds: 1079.40563965
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11553
score_details {
  idle_production_time: 7298.125
  idle_worker_time: 400.5625
  total_value_units: 7900.0
  total_value_structures: 1750.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 10690.0
  collected_vespene: 1256.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 492.0
  spent_minerals: 10018.0
  spent_vespene: 725.0
  food_used {
    none: 0.0
    army: 25.5
    economy: 69.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1025.0
    economy: 450.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1725.0
    economy: 5975.0
    technology: 1300.0
    upgrade: 575.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 0.0
    army: 2750.0
    economy: 6575.0
    technology: 1325.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 1810.72290039
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2756.27001953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 722
vespene: 531
food_cap: 106
food_used: 94
food_army: 25
food_workers: 57
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26994
score_details {
  idle_production_time: 46745.0
  idle_worker_time: 781.4375
  total_value_units: 26600.0
  total_value_structures: 3700.0
  killed_value_units: 6925.0
  killed_value_structures: 75.0
  collected_minerals: 27860.0
  collected_vespene: 8276.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 1276.0
  spent_minerals: 27142.0
  spent_vespene: 7675.0
  food_used {
    none: 0.0
    army: 129.5
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4800.0
    economy: 475.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7025.0
    economy: 550.0
    technology: -233.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: -125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7150.0
    economy: 8175.0
    technology: 2825.0
    upgrade: 1925.0
  }
  used_vespene {
    none: 0.0
    army: 2925.0
    economy: 0.0
    technology: 700.0
    upgrade: 1925.0
  }
  total_used_minerals {
    none: 0.0
    army: 16350.0
    economy: 9825.0
    technology: 3025.0
    upgrade: 1625.0
  }
  total_used_vespene {
    none: 0.0
    army: 5175.0
    economy: 0.0
    technology: 950.0
    upgrade: 1625.0
  }
  total_damage_dealt {
    life: 7745.24121094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11080.3300781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 768
vespene: 601
food_cap: 190
food_used: 188
food_army: 129
food_workers: 59
idle_worker_count: 0
army_count: 93
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 89
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 66
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 34
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20879
score_details {
  idle_production_time: 59612.8125
  idle_worker_time: 1174.875
  total_value_units: 32300.0
  total_value_structures: 4450.0
  killed_value_units: 16350.0
  killed_value_structures: 925.0
  collected_minerals: 32710.0
  collected_vespene: 12236.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 1455.0
  spent_minerals: 32042.0
  spent_vespene: 9025.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12250.0
    economy: 2375.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2500.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16775.0
    economy: 275.0
    technology: -233.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5375.0
    economy: 0.0
    technology: -125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 225.0
    economy: 8500.0
    technology: 3450.0
    upgrade: 1925.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 900.0
    upgrade: 1925.0
  }
  total_used_minerals {
    none: 0.0
    army: 20400.0
    economy: 10800.0
    technology: 3800.0
    upgrade: 1925.0
  }
  total_used_vespene {
    none: 0.0
    army: 6325.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1925.0
  }
  total_damage_dealt {
    life: 20335.1269531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24479.8242188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 718
vespene: 3211
food_cap: 200
food_used: 72
food_army: 10
food_workers: 62
idle_worker_count: 62
army_count: 12
warp_gate_count: 0

game_loop:  24177
ui_data{
 
Score:  20879
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
