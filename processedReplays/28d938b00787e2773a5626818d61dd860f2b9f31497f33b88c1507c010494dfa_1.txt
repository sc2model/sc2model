----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3531
  player_apm: 58
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 148
}
game_duration_loops: 19174
game_duration_seconds: 856.041870117
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7100
score_details {
  idle_production_time: 1786.375
  idle_worker_time: 988.875
  total_value_units: 4075.0
  total_value_structures: 2650.0
  killed_value_units: 800.0
  killed_value_structures: 100.0
  collected_minerals: 4895.0
  collected_vespene: 1980.0
  collection_rate_minerals: 531.0
  collection_rate_vespene: 313.0
  spent_minerals: 4600.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 500.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 375.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1275.0
    economy: 2400.0
    technology: 1250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 2250.0
    technology: 1250.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1901.33276367
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 612.0
    shields: 610.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 345
vespene: 355
food_cap: 55
food_used: 47
food_army: 28
food_workers: 19
idle_worker_count: 3
army_count: 14
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 73
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 93
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 43
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 43
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 73
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 72
  }
}

score{
 score_type: Melee
score: 4449
score_details {
  idle_production_time: 4147.875
  idle_worker_time: 2336.625
  total_value_units: 7825.0
  total_value_structures: 4075.0
  killed_value_units: 5075.0
  killed_value_structures: 300.0
  collected_minerals: 9305.0
  collected_vespene: 2744.0
  collection_rate_minerals: 195.0
  collection_rate_vespene: 0.0
  spent_minerals: 8800.0
  spent_vespene: 2200.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 5.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2350.0
    economy: 2300.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3200.0
    economy: 2600.0
    technology: 1100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 825.0
    economy: 1125.0
    technology: 900.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 4025.0
    economy: 3725.0
    technology: 2000.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 7427.58007812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8636.046875
    shields: 10391.5458984
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 555
vespene: 544
food_cap: 47
food_used: 22
food_army: 17
food_workers: 5
idle_worker_count: 5
army_count: 9
warp_gate_count: 4

game_loop:  19174
ui_data{
 
Score:  4449
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
