----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3439
  player_apm: 268
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 142
}
game_duration_loops: 7794
game_duration_seconds: 347.970703125
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4516
score_details {
  idle_production_time: 696.3125
  idle_worker_time: 523.1875
  total_value_units: 3000.0
  total_value_structures: 1875.0
  killed_value_units: 1450.0
  killed_value_structures: 0.0
  collected_minerals: 3945.0
  collected_vespene: 1280.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 22.0
  spent_minerals: 3450.0
  spent_vespene: 825.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 825.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: 475.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 675.0
    economy: 1675.0
    technology: 550.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 2550.0
    technology: 600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2386.97314453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5390.75
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1040.13500977
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 461
vespene: 455
food_cap: 31
food_used: 31
food_army: 14
food_workers: 16
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  7794
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}

Score:  4516
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
