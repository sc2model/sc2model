----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2361
  player_apm: 34
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2377
  player_apm: 48
}
game_duration_loops: 7136
game_duration_seconds: 318.593658447
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6483
score_details {
  idle_production_time: 541.6875
  idle_worker_time: 45.3125
  total_value_units: 2950.0
  total_value_structures: 1875.0
  killed_value_units: 750.0
  killed_value_structures: 100.0
  collected_minerals: 4475.0
  collected_vespene: 1008.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 313.0
  spent_minerals: 3875.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 2500.0
    technology: 725.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 2650.0
    technology: 725.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 540.0
    shields: 1384.0
    energy: 0.0
  }
  total_damage_taken {
    life: 211.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 650
vespene: 408
food_cap: 63
food_used: 53
food_army: 27
food_workers: 26
idle_worker_count: 1
army_count: 17
warp_gate_count: 0

game_loop:  7136
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 86
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  6483
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
