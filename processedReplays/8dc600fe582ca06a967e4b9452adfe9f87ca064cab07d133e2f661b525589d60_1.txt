----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3308
  player_apm: 235
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3618
  player_apm: 68
}
game_duration_loops: 13918
game_duration_seconds: 621.382629395
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13196
score_details {
  idle_production_time: 9055.5
  idle_worker_time: 16.125
  total_value_units: 6950.0
  total_value_structures: 1925.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9840.0
  collected_vespene: 2556.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 649.0
  spent_minerals: 9800.0
  spent_vespene: 2200.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 75.0
    economy: 100.0
    technology: -400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -300.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2800.0
    economy: 5525.0
    technology: 1475.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 350.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 6050.0
    technology: 1475.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 40.0
    shields: 40.0
    energy: 0.0
  }
  total_damage_taken {
    life: 542.717285156
    shields: 50.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 90
vespene: 356
food_cap: 114
food_used: 98
food_army: 47
food_workers: 51
idle_worker_count: 0
army_count: 31
warp_gate_count: 0
larva_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 26
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17809
score_details {
  idle_production_time: 22963.875
  idle_worker_time: 26.9375
  total_value_units: 13300.0
  total_value_structures: 2775.0
  killed_value_units: 6250.0
  killed_value_structures: 650.0
  collected_minerals: 16015.0
  collected_vespene: 5044.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 985.0
  spent_minerals: 15725.0
  spent_vespene: 4300.0
  food_used {
    none: 0.0
    army: 76.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3625.0
    economy: 400.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2575.0
    economy: 100.0
    technology: -400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: -300.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3800.0
    economy: 7400.0
    technology: 1475.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 350.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 5700.0
    economy: 8450.0
    technology: 1625.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: 450.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 3604.0
    shields: 3462.5
    energy: 0.0
  }
  total_damage_taken {
    life: 3936.61523438
    shields: 300.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 340
vespene: 744
food_cap: 190
food_used: 134
food_army: 76
food_workers: 58
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  13918
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 13
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 71
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 88
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  17809
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
