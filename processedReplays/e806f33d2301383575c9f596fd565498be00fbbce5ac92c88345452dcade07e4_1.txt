----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5064
  player_apm: 215
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5433
  player_apm: 226
}
game_duration_loops: 16844
game_duration_seconds: 752.016784668
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14502
score_details {
  idle_production_time: 1040.0625
  idle_worker_time: 88.75
  total_value_units: 5770.0
  total_value_structures: 5050.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 10795.0
  collected_vespene: 3092.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 985.0
  spent_minerals: 10190.0
  spent_vespene: 2700.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2480.0
    economy: 6200.0
    technology: 1950.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 750.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2320.0
    economy: 6250.0
    technology: 1500.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 500.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 243.332519531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 190.0
    shields: 224.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 655
vespene: 392
food_cap: 157
food_used: 107
food_army: 44
food_workers: 63
idle_worker_count: 0
army_count: 10
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 10
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 80
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 3
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 262
    shields: 262
    energy: 0
    build_progress: 0.373958349228
  }
}

score{
 score_type: Melee
score: 14207
score_details {
  idle_production_time: 5453.875
  idle_worker_time: 401.3125
  total_value_units: 18990.0
  total_value_structures: 6700.0
  killed_value_units: 15850.0
  killed_value_structures: 350.0
  collected_minerals: 19855.0
  collected_vespene: 6832.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 604.0
  spent_minerals: 19785.0
  spent_vespene: 6275.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11075.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8750.0
    economy: 1525.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2905.0
    economy: 5225.0
    technology: 2150.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 850.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 11715.0
    economy: 6750.0
    technology: 2300.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 5275.0
    economy: 0.0
    technology: 850.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 14970.5175781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14217.1855469
    shields: 15457.9521484
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 120
vespene: 557
food_cap: 150
food_used: 104
food_army: 53
food_workers: 51
idle_worker_count: 1
army_count: 18
warp_gate_count: 5

game_loop:  16844
ui_data{
 
Score:  14207
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
