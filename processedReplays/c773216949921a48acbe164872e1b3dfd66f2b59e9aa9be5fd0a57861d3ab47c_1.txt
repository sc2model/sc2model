----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4185
  player_apm: 265
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4204
  player_apm: 124
}
game_duration_loops: 5677
game_duration_seconds: 253.455184937
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2578
score_details {
  idle_production_time: 128.625
  idle_worker_time: 32.5
  total_value_units: 2200.0
  total_value_structures: 1650.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 2900.0
  collected_vespene: 412.0
  collection_rate_minerals: 83.0
  collection_rate_vespene: 0.0
  spent_minerals: 2925.0
  spent_vespene: 300.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 1350.0
    technology: -75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 1650.0
    technology: 350.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 550.0
    economy: 3250.0
    technology: 350.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 521.375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2018.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 13.97265625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 20
vespene: 108
food_cap: 46
food_used: 8
food_army: 4
food_workers: 3
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  5677
ui_data{
 multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 158
    shields: 0
    energy: 0
  }
}

Score:  2578
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
