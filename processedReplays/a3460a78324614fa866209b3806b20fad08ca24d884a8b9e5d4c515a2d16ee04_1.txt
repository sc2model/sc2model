----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 62
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2786
  player_apm: 72
}
game_duration_loops: 29776
game_duration_seconds: 1329.37841797
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10446
score_details {
  idle_production_time: 1210.9375
  idle_worker_time: 399.1875
  total_value_units: 4100.0
  total_value_structures: 3750.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 7600.0
  collected_vespene: 1896.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 335.0
  spent_minerals: 6550.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4150.0
    technology: 1650.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 250.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4250.0
    technology: 1650.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 356.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 93.0
    shields: 283.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1100
vespene: 996
food_cap: 102
food_used: 72
food_army: 26
food_workers: 46
idle_worker_count: 1
army_count: 13
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 3
}
multi {
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 30
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 61
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 26148
score_details {
  idle_production_time: 4426.3125
  idle_worker_time: 1267.5625
  total_value_units: 13600.0
  total_value_structures: 9450.0
  killed_value_units: 4550.0
  killed_value_structures: 100.0
  collected_minerals: 25070.0
  collected_vespene: 8328.0
  collection_rate_minerals: 3555.0
  collection_rate_vespene: 940.0
  spent_minerals: 23775.0
  spent_vespene: 3775.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 86.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2900.0
    economy: 1000.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5400.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2700.0
    economy: 9600.0
    technology: 4650.0
    upgrade: 1225.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 250.0
    upgrade: 1225.0
  }
  total_used_minerals {
    none: 0.0
    army: 8000.0
    economy: 9650.0
    technology: 3900.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 250.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 5127.73144531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4958.98095703
    shields: 3751.68383789
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1345
vespene: 4553
food_cap: 200
food_used: 134
food_army: 48
food_workers: 85
idle_worker_count: 0
army_count: 24
warp_gate_count: 12

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 3
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 38984
score_details {
  idle_production_time: 10939.9375
  idle_worker_time: 1805.3125
  total_value_units: 31050.0
  total_value_structures: 13000.0
  killed_value_units: 12475.0
  killed_value_structures: 8050.0
  collected_minerals: 41500.0
  collected_vespene: 11684.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 335.0
  spent_minerals: 36875.0
  spent_vespene: 9325.0
  food_used {
    none: 0.0
    army: 162.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 7450.0
    economy: 6050.0
    technology: 3650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 2075.0
    economy: 300.0
    technology: 900.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10000.0
    economy: 3500.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 10100.0
    economy: 6700.0
    technology: 5750.0
    upgrade: 1525.0
  }
  used_vespene {
    none: 0.0
    army: 4900.0
    economy: 0.0
    technology: 1450.0
    upgrade: 1525.0
  }
  total_used_minerals {
    none: 0.0
    army: 20000.0
    economy: 10200.0
    technology: 6050.0
    upgrade: 1525.0
  }
  total_used_vespene {
    none: 0.0
    army: 6350.0
    economy: 0.0
    technology: 1450.0
    upgrade: 1525.0
  }
  total_damage_dealt {
    life: 41582.03125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12798.4804688
    shields: 12475.4121094
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 4675
vespene: 2359
food_cap: 200
food_used: 199
food_army: 162
food_workers: 37
idle_worker_count: 1
army_count: 81
warp_gate_count: 12

game_loop:  29776
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 67
  count: 6
}

Score:  38984
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
