----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3227
  player_apm: 135
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3235
  player_apm: 113
}
game_duration_loops: 24408
game_duration_seconds: 1089.71887207
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8378
score_details {
  idle_production_time: 2052.5625
  idle_worker_time: 315.75
  total_value_units: 3800.0
  total_value_structures: 3600.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 7060.0
  collected_vespene: 1468.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 201.0
  spent_minerals: 6750.0
  spent_vespene: 1325.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 425.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 2925.0
    technology: 1775.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 375.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 3775.0
    technology: 1625.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 830.0
    shields: 1016.375
    energy: 0.0
  }
  total_damage_taken {
    life: 1633.32080078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 219.064941406
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 360
vespene: 143
food_cap: 78
food_used: 54
food_army: 34
food_workers: 20
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 995
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      build_progress: 0.117500007153
    }
  }
}

score{
 score_type: Melee
score: 3417
score_details {
  idle_production_time: 4279.8125
  idle_worker_time: 1057.25
  total_value_units: 7025.0
  total_value_structures: 4325.0
  killed_value_units: 3800.0
  killed_value_structures: 350.0
  collected_minerals: 10205.0
  collected_vespene: 1964.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 9812.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 2.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1925.0
    economy: 1550.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3275.0
    economy: 3526.0
    technology: 1727.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 550.0
    economy: 850.0
    technology: 500.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 125.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3675.0
    economy: 4675.0
    technology: 2075.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 425.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3290.0
    shields: 5797.375
    energy: 0.0
  }
  total_damage_taken {
    life: 22733.9296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1397.26733398
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 413
vespene: 129
food_cap: 31
food_used: 13
food_army: 11
food_workers: 2
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 55
    player_relative: 1
    health: 115
    shields: 0
    energy: 154
  }
  units {
    unit_type: 55
    player_relative: 1
    health: 67
    shields: 0
    energy: 192
  }
}

score{
 score_type: Melee
score: 3068
score_details {
  idle_production_time: 4919.5
  idle_worker_time: 1154.8125
  total_value_units: 7525.0
  total_value_structures: 4700.0
  killed_value_units: 4600.0
  killed_value_structures: 750.0
  collected_minerals: 10630.0
  collected_vespene: 1964.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 0.0
  spent_minerals: 10149.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2350.0
    economy: 1950.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3575.0
    economy: 3826.0
    technology: 1664.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 500.0
    economy: 550.0
    technology: 500.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 125.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 4075.0
    economy: 4675.0
    technology: 2425.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 4170.0
    shields: 7379.375
    energy: 0.0
  }
  total_damage_taken {
    life: 24944.6796875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1561.54296875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 464
vespene: 79
food_cap: 15
food_used: 10
food_army: 10
food_workers: 0
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  24408
ui_data{
 multi {
  units {
    unit_type: 55
    player_relative: 1
    health: 50
    shields: 0
    energy: 173
  }
  units {
    unit_type: 55
    player_relative: 1
    health: 140
    shields: 0
    energy: 200
  }
}

Score:  3068
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
