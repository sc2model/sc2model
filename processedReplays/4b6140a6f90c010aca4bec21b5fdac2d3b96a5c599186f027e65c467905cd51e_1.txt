----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3881
  player_apm: 127
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3991
  player_apm: 103
}
game_duration_loops: 17971
game_duration_seconds: 802.332763672
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11537
score_details {
  idle_production_time: 10009.3125
  idle_worker_time: 3.0
  total_value_units: 6700.0
  total_value_structures: 2300.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 10065.0
  collected_vespene: 996.0
  collection_rate_minerals: 2547.0
  collection_rate_vespene: 582.0
  spent_minerals: 9024.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 174.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 5300.0
    technology: 2000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 1325.0
    economy: 6700.0
    technology: 2150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 395.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 738.7109375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1091
vespene: 96
food_cap: 98
food_used: 91
food_army: 32
food_workers: 59
idle_worker_count: 0
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 27
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 140
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13005
score_details {
  idle_production_time: 25011.1875
  idle_worker_time: 15.9375
  total_value_units: 19850.0
  total_value_structures: 4100.0
  killed_value_units: 3775.0
  killed_value_structures: 0.0
  collected_minerals: 23630.0
  collected_vespene: 5772.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 582.0
  spent_minerals: 21511.0
  spent_vespene: 5636.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 300.0
    army: 8012.0
    economy: 4074.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 250.0
    army: 3061.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 600.0
    economy: 300.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 500.0
    economy: 4750.0
    technology: 2250.0
    upgrade: 1325.0
  }
  used_vespene {
    none: -100.0
    army: 250.0
    economy: 0.0
    technology: 550.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 300.0
    army: 9600.0
    economy: 10200.0
    technology: 2850.0
    upgrade: 1325.0
  }
  total_used_vespene {
    none: 300.0
    army: 3375.0
    economy: 0.0
    technology: 650.0
    upgrade: 1325.0
  }
  total_damage_dealt {
    life: 4621.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27885.0507812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2169
vespene: 136
food_cap: 172
food_used: 41
food_army: 6
food_workers: 35
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  17971
ui_data{
 
Score:  13005
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
