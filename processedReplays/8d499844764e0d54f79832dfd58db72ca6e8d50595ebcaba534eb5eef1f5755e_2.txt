----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3184
  player_apm: 132
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2995
  player_apm: 92
}
game_duration_loops: 16392
game_duration_seconds: 731.836791992
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8911
score_details {
  idle_production_time: 1534.375
  idle_worker_time: 102.125
  total_value_units: 5125.0
  total_value_structures: 3250.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 7020.0
  collected_vespene: 1728.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 335.0
  spent_minerals: 6812.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 50.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 3600.0
    technology: 1850.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2025.0
    economy: 3600.0
    technology: 1400.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 908.399902344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 400.0
    shields: 452.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 258
vespene: 228
food_cap: 70
food_used: 70
food_army: 30
food_workers: 39
idle_worker_count: 0
army_count: 12
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
production {
  unit {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  build_queue {
    unit_type: 84
    build_progress: 0.00845515727997
  }
}

score{
 score_type: Melee
score: 4389
score_details {
  idle_production_time: 3315.5
  idle_worker_time: 147.375
  total_value_units: 10300.0
  total_value_structures: 4800.0
  killed_value_units: 2800.0
  killed_value_structures: 1650.0
  collected_minerals: 12765.0
  collected_vespene: 4172.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 11048.0
  spent_vespene: 3600.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1800.0
    economy: 1950.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4125.0
    economy: 4100.0
    technology: 1548.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 600.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 5250.0
    economy: 4400.0
    technology: 2000.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 2950.0
    economy: 0.0
    technology: 500.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 12206.4199219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16418.125
    shields: 16773.9960938
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1767
vespene: 572
food_cap: 0
food_used: 16
food_army: 16
food_workers: 0
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  16392
ui_data{
 multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 5
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 54
    shields: 0
    energy: 0
  }
}

Score:  4389
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
