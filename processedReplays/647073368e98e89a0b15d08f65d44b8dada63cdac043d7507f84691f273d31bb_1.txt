----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 184
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2437
  player_apm: 92
}
game_duration_loops: 16110
game_duration_seconds: 719.246643066
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12536
score_details {
  idle_production_time: 7259.875
  idle_worker_time: 16.0
  total_value_units: 7450.0
  total_value_structures: 1675.0
  killed_value_units: 1150.0
  killed_value_structures: 0.0
  collected_minerals: 10270.0
  collected_vespene: 1716.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 671.0
  spent_minerals: 9450.0
  spent_vespene: 1050.0
  food_used {
    none: 6.5
    army: 27.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 875.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 175.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2450.0
    economy: 5950.0
    technology: 1175.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2400.0
    economy: 6475.0
    technology: 900.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 50.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 884.868164062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1469.05517578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 870
vespene: 666
food_cap: 122
food_used: 89
food_army: 27
food_workers: 56
idle_worker_count: 0
army_count: 35
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 30
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 90
  count: 2
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 23
  }
}

score{
 score_type: Melee
score: 27184
score_details {
  idle_production_time: 31942.5
  idle_worker_time: 16.0
  total_value_units: 16750.0
  total_value_structures: 3425.0
  killed_value_units: 5025.0
  killed_value_structures: 0.0
  collected_minerals: 24140.0
  collected_vespene: 6544.0
  collection_rate_minerals: 3555.0
  collection_rate_vespene: 1299.0
  spent_minerals: 20125.0
  spent_vespene: 3400.0
  food_used {
    none: 0.0
    army: 80.5
    economy: 89.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4225.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1475.0
    economy: 275.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4875.0
    economy: 9800.0
    technology: 1900.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 150.0
    technology: 600.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 9000.0
    economy: 11425.0
    technology: 1850.0
    upgrade: 1075.0
  }
  total_used_vespene {
    none: 0.0
    army: 1875.0
    economy: 300.0
    technology: 550.0
    upgrade: 1075.0
  }
  total_damage_dealt {
    life: 6475.07861328
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3780.75341797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 4065
vespene: 3144
food_cap: 200
food_used: 169
food_army: 80
food_workers: 89
idle_worker_count: 0
army_count: 102
warp_gate_count: 0

game_loop:  16110
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 7
  leader_unit_type: 90
  count: 2
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 27
    shields: 0
    energy: 86
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 188
    shields: 0
    energy: 85
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 182
    shields: 0
    energy: 85
  }
}

Score:  27184
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
