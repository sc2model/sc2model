----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4496
  player_apm: 218
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4500
  player_apm: 183
}
game_duration_loops: 15590
game_duration_seconds: 696.030700684
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7715
score_details {
  idle_production_time: 6562.8125
  idle_worker_time: 147.875
  total_value_units: 7900.0
  total_value_structures: 1725.0
  killed_value_units: 2300.0
  killed_value_structures: 0.0
  collected_minerals: 9960.0
  collected_vespene: 1292.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 335.0
  spent_minerals: 9781.0
  spent_vespene: 1106.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2581.0
    economy: 900.0
    technology: 675.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 231.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 675.0
    economy: 4525.0
    technology: 1075.0
    upgrade: 350.0
  }
  used_vespene {
    none: -25.0
    army: 50.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3400.0
    economy: 5875.0
    technology: 1350.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 2459.92016602
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10322.7685547
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 229
vespene: 186
food_cap: 84
food_used: 55
food_army: 8
food_workers: 47
idle_worker_count: 0
army_count: 1
warp_gate_count: 0
larva_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 129
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 100
    player_relative: 1
    health: 1972
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12728
score_details {
  idle_production_time: 16332.5625
  idle_worker_time: 148.0
  total_value_units: 15100.0
  total_value_structures: 3200.0
  killed_value_units: 5150.0
  killed_value_structures: 0.0
  collected_minerals: 18920.0
  collected_vespene: 3920.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 671.0
  spent_minerals: 17106.0
  spent_vespene: 2656.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4125.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6506.0
    economy: 1600.0
    technology: 950.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1006.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 1125.0
    economy: 5450.0
    technology: 1050.0
    upgrade: 825.0
  }
  used_vespene {
    none: -25.0
    army: 50.0
    economy: 0.0
    technology: 350.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 0.0
    army: 9000.0
    economy: 8100.0
    technology: 1850.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 450.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 6111.31347656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19340.640625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1864
vespene: 1264
food_cap: 120
food_used: 69
food_army: 18
food_workers: 51
idle_worker_count: 0
army_count: 21
warp_gate_count: 0

game_loop:  15590
ui_data{
 
Score:  12728
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
