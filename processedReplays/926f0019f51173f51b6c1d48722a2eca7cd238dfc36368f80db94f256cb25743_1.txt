----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3617
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3728
  player_apm: 145
}
game_duration_loops: 26077
game_duration_seconds: 1164.23303223
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8833
score_details {
  idle_production_time: 1625.8125
  idle_worker_time: 54.0625
  total_value_units: 3625.0
  total_value_structures: 3325.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 6755.0
  collected_vespene: 1328.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 223.0
  spent_minerals: 6600.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2000.0
    economy: 3275.0
    technology: 1425.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 300.0
    economy: 0.0
    technology: 500.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 2200.0
    economy: 3400.0
    technology: 1275.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 175.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 820.425292969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 480.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 205
vespene: 28
food_cap: 86
food_used: 68
food_army: 41
food_workers: 25
idle_worker_count: 0
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 53
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 54
  count: 1
}
multi {
  units {
    unit_type: 53
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 74
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 66
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15862
score_details {
  idle_production_time: 6818.25
  idle_worker_time: 879.6875
  total_value_units: 12075.0
  total_value_structures: 5600.0
  killed_value_units: 5400.0
  killed_value_structures: 650.0
  collected_minerals: 16995.0
  collected_vespene: 5304.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 470.0
  spent_minerals: 16762.0
  spent_vespene: 3150.0
  food_used {
    none: 0.0
    army: 67.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3625.0
    economy: 800.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5800.0
    economy: 637.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3200.0
    economy: 4950.0
    technology: 1775.0
    upgrade: 900.0
  }
  used_vespene {
    none: 100.0
    army: 725.0
    economy: 0.0
    technology: 725.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 100.0
    army: 8750.0
    economy: 5450.0
    technology: 1775.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 100.0
    army: 1375.0
    economy: 0.0
    technology: 725.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 9332.04394531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6932.55908203
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1868.28222656
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 283
vespene: 2154
food_cap: 158
food_used: 102
food_army: 67
food_workers: 35
idle_worker_count: 1
army_count: 47
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 48
  count: 43
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 46
    shields: 0
    energy: 112
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 51
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 57
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 53
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 56
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17774
score_details {
  idle_production_time: 10829.8125
  idle_worker_time: 1145.6875
  total_value_units: 18975.0
  total_value_structures: 6325.0
  killed_value_units: 11525.0
  killed_value_structures: 1075.0
  collected_minerals: 23935.0
  collected_vespene: 7676.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 761.0
  spent_minerals: 22412.0
  spent_vespene: 5025.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6825.0
    economy: 1825.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12100.0
    economy: 837.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1600.0
    economy: 5475.0
    technology: 1875.0
    upgrade: 1325.0
  }
  used_vespene {
    none: 100.0
    army: 975.0
    economy: 0.0
    technology: 725.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 100.0
    army: 13800.0
    economy: 6725.0
    technology: 1875.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 100.0
    army: 2875.0
    economy: 0.0
    technology: 725.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 20233.6699219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14907.7314453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 6258.00292969
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1573
vespene: 2651
food_cap: 173
food_used: 74
food_army: 36
food_workers: 38
idle_worker_count: 1
army_count: 18
warp_gate_count: 0

game_loop:  26077
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 3
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
}

Score:  17774
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
