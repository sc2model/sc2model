----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3314
  player_apm: 215
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3405
  player_apm: 151
}
game_duration_loops: 24272
game_duration_seconds: 1083.64709473
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8903
score_details {
  idle_production_time: 1361.5625
  idle_worker_time: 141.25
  total_value_units: 5150.0
  total_value_structures: 3050.0
  killed_value_units: 1500.0
  killed_value_structures: 150.0
  collected_minerals: 7525.0
  collected_vespene: 2228.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 515.0
  spent_minerals: 7250.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 300.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 3550.0
    technology: 1650.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2400.0
    economy: 3450.0
    technology: 1200.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2818.16650391
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 840.0
    shields: 994.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 325
vespene: 378
food_cap: 70
food_used: 66
food_army: 27
food_workers: 37
idle_worker_count: 0
army_count: 11
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 4
}
production {
  unit {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  build_queue {
    unit_type: 84
    build_progress: 0.890619635582
  }
  build_queue {
    unit_type: 84
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 18354
score_details {
  idle_production_time: 5059.8125
  idle_worker_time: 617.4375
  total_value_units: 17025.0
  total_value_structures: 6700.0
  killed_value_units: 9700.0
  killed_value_structures: 150.0
  collected_minerals: 20445.0
  collected_vespene: 6184.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 559.0
  spent_minerals: 20450.0
  spent_vespene: 5625.0
  food_used {
    none: 0.0
    army: 81.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6300.0
    economy: 1050.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5600.0
    economy: 650.0
    technology: 1050.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4850.0
    economy: 5800.0
    technology: 2850.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 550.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 9850.0
    economy: 6050.0
    technology: 3000.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 4275.0
    economy: 0.0
    technology: 550.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 10419.4628906
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6302.42431641
    shields: 7196.13769531
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 45
vespene: 559
food_cap: 157
food_used: 128
food_army: 81
food_workers: 47
idle_worker_count: 2
army_count: 40
warp_gate_count: 6

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 15
}
groups {
  control_group_index: 2
  leader_unit_type: 495
  count: 17
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 488
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 12474
score_details {
  idle_production_time: 7312.5625
  idle_worker_time: 657.75
  total_value_units: 23450.0
  total_value_structures: 7750.0
  killed_value_units: 12975.0
  killed_value_structures: 150.0
  collected_minerals: 25440.0
  collected_vespene: 8184.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 582.0
  spent_minerals: 25200.0
  spent_vespene: 7300.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8175.0
    economy: 2050.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13950.0
    economy: 1750.0
    technology: 1200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 5000.0
    technology: 2850.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 550.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 14550.0
    economy: 6750.0
    technology: 3450.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 5900.0
    economy: 0.0
    technology: 550.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 14211.5253906
    shields: 100.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14643.4238281
    shields: 15336.5566406
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 290
vespene: 884
food_cap: 157
food_used: 51
food_army: 12
food_workers: 39
idle_worker_count: 0
army_count: 4
warp_gate_count: 9

game_loop:  24272
ui_data{
 multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 311
    shields: 0
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  12474
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
