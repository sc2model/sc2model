----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3171
  player_apm: 74
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3215
  player_apm: 106
}
game_duration_loops: 6599
game_duration_seconds: 294.618774414
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3299
score_details {
  idle_production_time: 2261.5
  idle_worker_time: 106.25
  total_value_units: 3500.0
  total_value_structures: 1000.0
  killed_value_units: 725.0
  killed_value_structures: 0.0
  collected_minerals: 3990.0
  collected_vespene: 596.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 179.0
  spent_minerals: 3837.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 14.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 625.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1375.0
    economy: 437.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 425.0
    economy: 1700.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 2800.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1323.71728516
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4325.85449219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 203
vespene: 446
food_cap: 46
food_used: 23
food_army: 9
food_workers: 14
idle_worker_count: 1
army_count: 1
warp_gate_count: 0

game_loop:  6599
ui_data{
 production {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 126
    build_progress: 0.706250011921
  }
}

Score:  3299
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
