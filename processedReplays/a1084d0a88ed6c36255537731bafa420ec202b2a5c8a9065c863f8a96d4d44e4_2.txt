----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 166
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3322
  player_apm: 152
}
game_duration_loops: 28971
game_duration_seconds: 1293.43847656
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11182
score_details {
  idle_production_time: 4616.0625
  idle_worker_time: 185.5625
  total_value_units: 7400.0
  total_value_structures: 1450.0
  killed_value_units: 1100.0
  killed_value_structures: 0.0
  collected_minerals: 9180.0
  collected_vespene: 1708.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 559.0
  spent_minerals: 8431.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 71.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 381.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2850.0
    economy: 4850.0
    technology: 1000.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2725.0
    economy: 5550.0
    technology: 900.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1321.12158203
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2893.13183594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 799
vespene: 583
food_cap: 140
food_used: 109
food_army: 71
food_workers: 38
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 104
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11340
score_details {
  idle_production_time: 21026.3125
  idle_worker_time: 830.3125
  total_value_units: 21550.0
  total_value_structures: 2550.0
  killed_value_units: 10500.0
  killed_value_structures: 1750.0
  collected_minerals: 21040.0
  collected_vespene: 5956.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 425.0
  spent_minerals: 20231.0
  spent_vespene: 5425.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6975.0
    economy: 2825.0
    technology: 825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9650.0
    economy: 2631.0
    technology: 700.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2325.0
    economy: 4200.0
    technology: 1200.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 200.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 11500.0
    economy: 7850.0
    technology: 2050.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 4700.0
    economy: 0.0
    technology: 300.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 24313.5957031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 26079.28125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 859
vespene: 531
food_cap: 172
food_used: 68
food_army: 44
food_workers: 24
idle_worker_count: 1
army_count: 15
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 90
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 77
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 187
    shields: 0
    energy: 183
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 9
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7679
score_details {
  idle_production_time: 48008.8125
  idle_worker_time: 1330.5
  total_value_units: 28250.0
  total_value_structures: 3550.0
  killed_value_units: 14250.0
  killed_value_structures: 2175.0
  collected_minerals: 25655.0
  collected_vespene: 8080.0
  collection_rate_minerals: 27.0
  collection_rate_vespene: 201.0
  spent_minerals: 25531.0
  spent_vespene: 7325.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 3.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9025.0
    economy: 3800.0
    technology: 825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 15325.0
    economy: 4481.0
    technology: 850.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 250.0
    economy: 3450.0
    technology: 1500.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 200.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 15750.0
    economy: 9250.0
    technology: 2500.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 6800.0
    economy: 0.0
    technology: 300.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 31923.8046875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 35433.8125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 174
vespene: 755
food_cap: 154
food_used: 7
food_army: 4
food_workers: 3
idle_worker_count: 3
army_count: 1
warp_gate_count: 0

game_loop:  28971
ui_data{
 
Score:  7679
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
