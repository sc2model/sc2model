----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3221
  player_apm: 228
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3251
  player_apm: 92
}
game_duration_loops: 7385
game_duration_seconds: 329.710510254
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3216
score_details {
  idle_production_time: 525.8125
  idle_worker_time: 397.75
  total_value_units: 2000.0
  total_value_structures: 1400.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 2910.0
  collected_vespene: 556.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 335.0
  spent_minerals: 2200.0
  spent_vespene: 450.0
  food_used {
    none: 0.0
    army: 1.0
    economy: 16.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 500.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 50.0
    economy: 1800.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 350.0
    economy: 2450.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 956.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1634.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 760
vespene: 106
food_cap: 39
food_used: 17
food_army: 1
food_workers: 16
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  7385
ui_data{
 
Score:  3216
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
