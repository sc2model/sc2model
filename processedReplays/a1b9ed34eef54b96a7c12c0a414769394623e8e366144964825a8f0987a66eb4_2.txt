----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4402
  player_apm: 104
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4229
  player_apm: 137
}
game_duration_loops: 6964
game_duration_seconds: 310.914550781
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5512
score_details {
  idle_production_time: 569.1875
  idle_worker_time: 60.5625
  total_value_units: 2675.0
  total_value_structures: 2375.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 4765.0
  collected_vespene: 1124.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 358.0
  spent_minerals: 3925.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 805.0
    economy: 150.0
    technology: 104.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 415.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 300.0
    economy: 2900.0
    technology: 725.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 800.0
    economy: 3350.0
    technology: 825.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 679.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2141.16748047
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 278.096435547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 828
vespene: 84
food_cap: 54
food_used: 33
food_army: 6
food_workers: 27
idle_worker_count: 4
army_count: 1
warp_gate_count: 0

game_loop:  6964
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

Score:  5512
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
