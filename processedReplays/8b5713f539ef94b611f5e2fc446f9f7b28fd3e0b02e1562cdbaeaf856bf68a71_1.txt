----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2839
  player_apm: 103
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3204
  player_apm: 89
}
game_duration_loops: 13409
game_duration_seconds: 598.657836914
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9439
score_details {
  idle_production_time: 2033.3125
  idle_worker_time: 928.75
  total_value_units: 2950.0
  total_value_structures: 4875.0
  killed_value_units: 525.0
  killed_value_structures: 150.0
  collected_minerals: 7145.0
  collected_vespene: 1644.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 403.0
  spent_minerals: 7125.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1475.0
    economy: 3900.0
    technology: 2125.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 650.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1125.0
    economy: 4150.0
    technology: 2125.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3477.10498047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 548.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 70
vespene: 219
food_cap: 93
food_used: 60
food_army: 30
food_workers: 29
idle_worker_count: 5
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 49
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12566
score_details {
  idle_production_time: 4239.4375
  idle_worker_time: 1982.9375
  total_value_units: 4925.0
  total_value_structures: 5350.0
  killed_value_units: 4950.0
  killed_value_structures: 750.0
  collected_minerals: 9955.0
  collected_vespene: 3148.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 582.0
  spent_minerals: 9812.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3150.0
    economy: 1350.0
    technology: 650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: 100.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2725.0
    economy: 4200.0
    technology: 2175.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 675.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 4700.0
    technology: 2175.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 675.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 9738.41699219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3050.5
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 193
vespene: 1298
food_cap: 109
food_used: 87
food_army: 55
food_workers: 32
idle_worker_count: 6
army_count: 16
warp_gate_count: 0

game_loop:  13409
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 53
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 53
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  12566
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
