----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 132
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3543
  player_apm: 89
}
game_duration_loops: 14930
game_duration_seconds: 666.56439209
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10666
score_details {
  idle_production_time: 5339.6875
  idle_worker_time: 4.25
  total_value_units: 5550.0
  total_value_structures: 2150.0
  killed_value_units: 325.0
  killed_value_structures: 0.0
  collected_minerals: 8295.0
  collected_vespene: 1908.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 649.0
  spent_minerals: 8087.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 36.5
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 212.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1575.0
    economy: 4500.0
    technology: 1600.0
    upgrade: 675.0
  }
  used_vespene {
    none: 150.0
    army: 650.0
    economy: 0.0
    technology: 350.0
    upgrade: 675.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4950.0
    technology: 1750.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 676.834472656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2258.66699219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 258
vespene: 83
food_cap: 92
food_used: 75
food_army: 36
food_workers: 35
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15769
score_details {
  idle_production_time: 13663.5
  idle_worker_time: 4.25
  total_value_units: 10250.0
  total_value_structures: 2825.0
  killed_value_units: 6575.0
  killed_value_structures: 0.0
  collected_minerals: 13385.0
  collected_vespene: 4296.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 649.0
  spent_minerals: 12237.0
  spent_vespene: 3850.0
  food_used {
    none: 0.0
    army: 66.5
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4875.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1600.0
    economy: 212.0
    technology: 275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3275.0
    economy: 4850.0
    technology: 1950.0
    upgrade: 825.0
  }
  used_vespene {
    none: 150.0
    army: 1600.0
    economy: 0.0
    technology: 500.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 300.0
    army: 5325.0
    economy: 6150.0
    technology: 2575.0
    upgrade: 675.0
  }
  total_used_vespene {
    none: 300.0
    army: 3375.0
    economy: 0.0
    technology: 750.0
    upgrade: 675.0
  }
  total_damage_dealt {
    life: 10122.0195312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6634.65917969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1198
vespene: 446
food_cap: 138
food_used: 106
food_army: 66
food_workers: 40
idle_worker_count: 0
army_count: 34
warp_gate_count: 0

game_loop:  14930
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 502
    player_relative: 1
    health: 74
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 99
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 193
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 172
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 170
    shields: 0
    energy: 0
  }
}

Score:  15769
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
