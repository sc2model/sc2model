----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3853
  player_apm: 151
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3952
  player_apm: 132
}
game_duration_loops: 7069
game_duration_seconds: 315.602386475
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2456
score_details {
  idle_production_time: 532.6875
  idle_worker_time: 318.25
  total_value_units: 2525.0
  total_value_structures: 2000.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 4275.0
  collected_vespene: 656.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 4100.0
  spent_vespene: 500.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 2.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 1800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 750.0
    technology: 750.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 2500.0
    technology: 750.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 549.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2865.83300781
    shields: 3351.4831543
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 225
vespene: 156
food_cap: 23
food_used: 8
food_army: 6
food_workers: 1
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  7069
ui_data{
 production {
  unit {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 930
    energy: 0
  }
  build_queue {
    unit_type: 84
    build_progress: 0.900475025177
  }
  build_queue {
    unit_type: 84
    build_progress: 0.0
  }
}

Score:  2456
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
