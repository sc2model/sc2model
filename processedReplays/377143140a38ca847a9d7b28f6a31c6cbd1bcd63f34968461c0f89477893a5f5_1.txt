----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4596
  player_apm: 270
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4432
  player_apm: 151
}
game_duration_loops: 40391
game_duration_seconds: 1803.29553223
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14350
score_details {
  idle_production_time: 7688.9375
  idle_worker_time: 87.4375
  total_value_units: 8850.0
  total_value_structures: 2225.0
  killed_value_units: 200.0
  killed_value_structures: 350.0
  collected_minerals: 11310.0
  collected_vespene: 2740.0
  collection_rate_minerals: 2379.0
  collection_rate_vespene: 963.0
  spent_minerals: 10175.0
  spent_vespene: 2100.0
  food_used {
    none: 0.0
    army: 70.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3050.0
    economy: 5875.0
    technology: 1175.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 25.0
    technology: 500.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 3100.0
    economy: 6550.0
    technology: 1325.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 50.0
    technology: 600.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 3474.4699707
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1529.32666016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1185
vespene: 640
food_cap: 130
food_used: 129
food_army: 70
food_workers: 59
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 110
  count: 22
}
groups {
  control_group_index: 4
  leader_unit_type: 893
  count: 1
}
multi {
  units {
    unit_type: 129
    player_relative: 1
    health: 31
    shields: 0
    energy: 55
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 62
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 17
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 88
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 61
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22123
score_details {
  idle_production_time: 23241.875
  idle_worker_time: 180.5625
  total_value_units: 28050.0
  total_value_structures: 3250.0
  killed_value_units: 6700.0
  killed_value_structures: 1850.0
  collected_minerals: 27815.0
  collected_vespene: 10008.0
  collection_rate_minerals: 2603.0
  collection_rate_vespene: 985.0
  spent_minerals: 26725.0
  spent_vespene: 9525.0
  food_used {
    none: 0.0
    army: 128.0
    economy: 71.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4475.0
    economy: 800.0
    technology: 1500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10775.0
    economy: 100.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5050.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 5750.0
    economy: 8550.0
    technology: 1275.0
    upgrade: 700.0
  }
  used_vespene {
    none: 150.0
    army: 2600.0
    economy: 25.0
    technology: 600.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 300.0
    army: 18150.0
    economy: 9475.0
    technology: 1525.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 300.0
    army: 10250.0
    economy: 50.0
    technology: 800.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 17747.71875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19033.6503906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1140
vespene: 483
food_cap: 200
food_used: 199
food_army: 128
food_workers: 71
idle_worker_count: 0
army_count: 13
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 129
  count: 4
}
single {
  unit {
    unit_type: 129
    player_relative: 1
    health: 134
    shields: 0
    energy: 142
  }
}

score{
 score_type: Melee
score: 29066
score_details {
  idle_production_time: 36032.875
  idle_worker_time: 1827.8125
  total_value_units: 43350.0
  total_value_structures: 4925.0
  killed_value_units: 15200.0
  killed_value_structures: 3100.0
  collected_minerals: 45102.0
  collected_vespene: 16464.0
  collection_rate_minerals: 2777.0
  collection_rate_vespene: 1231.0
  spent_minerals: 41625.0
  spent_vespene: 16100.0
  food_used {
    none: 0.0
    army: 132.0
    economy: 68.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9250.0
    economy: 1200.0
    technology: 2400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 22000.0
    economy: 100.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 10225.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 5400.0
    economy: 11975.0
    technology: 1525.0
    upgrade: 850.0
  }
  used_vespene {
    none: 150.0
    army: 3500.0
    economy: 25.0
    technology: 750.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 300.0
    army: 29325.0
    economy: 13250.0
    technology: 2275.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 300.0
    army: 17175.0
    economy: 50.0
    technology: 1400.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 31313.0976562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 36138.7265625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3527
vespene: 364
food_cap: 200
food_used: 200
food_army: 132
food_workers: 68
idle_worker_count: 0
army_count: 69
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 16
}
groups {
  control_group_index: 3
  leader_unit_type: 502
  count: 70
}
groups {
  control_group_index: 4
  leader_unit_type: 502
  count: 14
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.961629390717
    }
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.959633409977
    }
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.954523444176
    }
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.964588344097
    }
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.961826741695
    }
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.95726364851
    }
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.95532977581
    }
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 501
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
    add_on {
      unit_type: 502
      build_progress: 0.955646395683
    }
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 79
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 83
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 79
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 79
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27271
score_details {
  idle_production_time: 45255.0
  idle_worker_time: 1934.75
  total_value_units: 63350.0
  total_value_structures: 6275.0
  killed_value_units: 33400.0
  killed_value_structures: 8425.0
  collected_minerals: 63010.0
  collected_vespene: 25236.0
  collection_rate_minerals: 1803.0
  collection_rate_vespene: 1052.0
  spent_minerals: 60200.0
  spent_vespene: 24750.0
  food_used {
    none: 0.0
    army: 133.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 18500.0
    economy: 4550.0
    technology: 6775.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 11900.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 40575.0
    economy: -25.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 19900.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2600.0
    economy: 13375.0
    technology: 2275.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 150.0
    army: 2050.0
    economy: 25.0
    technology: 750.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 300.0
    army: 44350.0
    economy: 15450.0
    technology: 3025.0
    upgrade: 1275.0
  }
  total_used_vespene {
    none: 300.0
    army: 27000.0
    economy: 50.0
    technology: 1400.0
    upgrade: 1275.0
  }
  total_damage_dealt {
    life: 65914.7109375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 64965.0078125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2860
vespene: 486
food_cap: 200
food_used: 194
food_army: 133
food_workers: 61
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 110
  count: 60
}
groups {
  control_group_index: 4
  leader_unit_type: 499
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26770
score_details {
  idle_production_time: 45782.5625
  idle_worker_time: 1934.75
  total_value_units: 66350.0
  total_value_structures: 6275.0
  killed_value_units: 35050.0
  killed_value_structures: 9300.0
  collected_minerals: 63535.0
  collected_vespene: 25560.0
  collection_rate_minerals: 1668.0
  collection_rate_vespene: 1052.0
  spent_minerals: 61425.0
  spent_vespene: 25425.0
  food_used {
    none: 0.0
    army: 133.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 19650.0
    economy: 4975.0
    technology: 7125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 12400.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 41525.0
    economy: -25.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 20300.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2875.0
    economy: 13375.0
    technology: 2275.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 150.0
    army: 2325.0
    economy: 25.0
    technology: 750.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 300.0
    army: 46525.0
    economy: 15450.0
    technology: 3025.0
    upgrade: 1275.0
  }
  total_used_vespene {
    none: 300.0
    army: 27825.0
    economy: 50.0
    technology: 1400.0
    upgrade: 1275.0
  }
  total_damage_dealt {
    life: 68922.0078125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 66339.375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2160
vespene: 135
food_cap: 200
food_used: 194
food_army: 133
food_workers: 61
idle_worker_count: 0
army_count: 52
warp_gate_count: 0

game_loop:  40391
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 110
  count: 63
}
groups {
  control_group_index: 4
  leader_unit_type: 499
  count: 1
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  26770
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
