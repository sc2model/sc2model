----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3600
  player_apm: 55
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2866
  player_apm: 327
}
game_duration_loops: 12776
game_duration_seconds: 570.396972656
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10034
score_details {
  idle_production_time: 1606.0625
  idle_worker_time: 278.25
  total_value_units: 3400.0
  total_value_structures: 4200.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 6840.0
  collected_vespene: 2044.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 335.0
  spent_minerals: 6100.0
  spent_vespene: 1800.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 3450.0
    technology: 2200.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 500.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 1300.0
    economy: 3450.0
    technology: 2000.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 249.9140625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 185.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 790
vespene: 244
food_cap: 86
food_used: 55
food_army: 22
food_workers: 33
idle_worker_count: 1
army_count: 9
warp_gate_count: 7

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 9113
score_details {
  idle_production_time: 2903.8125
  idle_worker_time: 390.375
  total_value_units: 4950.0
  total_value_structures: 5200.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 9415.0
  collected_vespene: 3148.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 649.0
  spent_minerals: 8325.0
  spent_vespene: 2225.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 825.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2525.0
    economy: 550.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 3350.0
    technology: 1900.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 500.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 2625.0
    economy: 3900.0
    technology: 2400.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 700.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 1898.62158203
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4971.16552734
    shields: 5247.09521484
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1140
vespene: 923
food_cap: 70
food_used: 35
food_army: 0
food_workers: 35
idle_worker_count: 0
army_count: 0
warp_gate_count: 4

game_loop:  12776
ui_data{
 
Score:  9113
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
