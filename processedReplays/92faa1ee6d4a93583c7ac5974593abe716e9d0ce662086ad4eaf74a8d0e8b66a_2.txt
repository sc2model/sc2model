----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2856
  player_apm: 100
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2967
  player_apm: 92
}
game_duration_loops: 20680
game_duration_seconds: 923.278686523
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10023
score_details {
  idle_production_time: 12025.9375
  idle_worker_time: 3.4375
  total_value_units: 5350.0
  total_value_structures: 1350.0
  killed_value_units: 325.0
  killed_value_structures: 100.0
  collected_minerals: 9165.0
  collected_vespene: 932.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 156.0
  spent_minerals: 6687.0
  spent_vespene: 687.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 737.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 137.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 1700.0
    economy: 3675.0
    technology: 1175.0
    upgrade: 350.0
  }
  used_vespene {
    none: -50.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 3425.0
    technology: 1025.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 50.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 890.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 985.520019531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2528
vespene: 245
food_cap: 60
food_used: 60
food_army: 29
food_workers: 31
idle_worker_count: 0
army_count: 42
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 123
  }
}

score{
 score_type: Melee
score: 13503
score_details {
  idle_production_time: 47613.6875
  idle_worker_time: 91.6875
  total_value_units: 11900.0
  total_value_structures: 2875.0
  killed_value_units: 3300.0
  killed_value_structures: 100.0
  collected_minerals: 20675.0
  collected_vespene: 3152.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 0.0
  spent_minerals: 14737.0
  spent_vespene: 1712.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2650.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4787.0
    economy: 3425.0
    technology: 1925.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 337.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 650.0
    economy: 4025.0
    technology: -200.0
    upgrade: 875.0
  }
  used_vespene {
    none: -50.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 6200.0
    economy: 8450.0
    technology: 1775.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 550.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 4896.87792969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25284.4765625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5988
vespene: 1440
food_cap: 124
food_used: 32
food_army: 13
food_workers: 19
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13328
score_details {
  idle_production_time: 48208.0
  idle_worker_time: 91.6875
  total_value_units: 11900.0
  total_value_structures: 3075.0
  killed_value_units: 4175.0
  killed_value_structures: 100.0
  collected_minerals: 21100.0
  collected_vespene: 3152.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 0.0
  spent_minerals: 15187.0
  spent_vespene: 1712.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5287.0
    economy: 3525.0
    technology: 1925.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 337.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 600.0
    economy: 3925.0
    technology: -200.0
    upgrade: 875.0
  }
  used_vespene {
    none: -50.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 6200.0
    economy: 8450.0
    technology: 2025.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 550.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 5494.80419922
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27861.3886719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5963
vespene: 1440
food_cap: 124
food_used: 30
food_army: 13
food_workers: 17
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  20680
ui_data{
 
Score:  13328
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
