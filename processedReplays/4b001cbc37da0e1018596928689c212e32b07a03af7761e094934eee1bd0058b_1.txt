----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3601
  player_apm: 64
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3714
  player_apm: 274
}
game_duration_loops: 5920
game_duration_seconds: 264.304168701
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3229
score_details {
  idle_production_time: 3757.9375
  idle_worker_time: 12.1875
  total_value_units: 2100.0
  total_value_structures: 525.0
  killed_value_units: 125.0
  killed_value_structures: 0.0
  collected_minerals: 2550.0
  collected_vespene: 504.0
  collection_rate_minerals: 531.0
  collection_rate_vespene: 156.0
  spent_minerals: 2300.0
  spent_vespene: 200.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 75.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: -125.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 400.0
    economy: 1375.0
    technology: 550.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 850.0
    economy: 1675.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1296.21289062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1516.55078125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 300
vespene: 304
food_cap: 22
food_used: 22
food_army: 7
food_workers: 15
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  5920
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}

Score:  3229
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
