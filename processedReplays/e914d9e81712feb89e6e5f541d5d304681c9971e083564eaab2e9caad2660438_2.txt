----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4215
  player_apm: 114
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 134
}
game_duration_loops: 6186
game_duration_seconds: 276.179992676
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4874
score_details {
  idle_production_time: 187.625
  idle_worker_time: 94.1875
  total_value_units: 2250.0
  total_value_structures: 1900.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 4125.0
  collected_vespene: 796.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 313.0
  spent_minerals: 3900.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 459.0
    economy: 400.0
    technology: 129.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 109.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 2900.0
    technology: 750.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 550.0
    economy: 3600.0
    technology: 400.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 677.983398438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2220.07568359
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 430.075683594
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 237
vespene: 187
food_cap: 54
food_used: 30
food_army: 6
food_workers: 24
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  6186
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  4874
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
