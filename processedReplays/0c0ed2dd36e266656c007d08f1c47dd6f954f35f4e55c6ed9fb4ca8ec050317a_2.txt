----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4319
  player_apm: 159
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4557
  player_apm: 270
}
game_duration_loops: 14049
game_duration_seconds: 627.231262207
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14364
score_details {
  idle_production_time: 5133.9375
  idle_worker_time: 0.4375
  total_value_units: 9250.0
  total_value_structures: 1750.0
  killed_value_units: 600.0
  killed_value_structures: 400.0
  collected_minerals: 10610.0
  collected_vespene: 3004.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 627.0
  spent_minerals: 9275.0
  spent_vespene: 2875.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2800.0
    economy: 5900.0
    technology: 900.0
    upgrade: 475.0
  }
  used_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 300.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 6200.0
    technology: 1050.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1803.33642578
    shields: 2810.4777832
    energy: 0.0
  }
  total_damage_taken {
    life: 1260.64941406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1385
vespene: 129
food_cap: 122
food_used: 108
food_army: 53
food_workers: 55
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 20
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21268
score_details {
  idle_production_time: 15438.6875
  idle_worker_time: 0.4375
  total_value_units: 14800.0
  total_value_structures: 2600.0
  killed_value_units: 5800.0
  killed_value_structures: 1900.0
  collected_minerals: 17810.0
  collected_vespene: 5008.0
  collection_rate_minerals: 2379.0
  collection_rate_vespene: 1007.0
  spent_minerals: 15675.0
  spent_vespene: 4225.0
  food_used {
    none: 0.0
    army: 99.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3000.0
    economy: 3250.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1600.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5150.0
    economy: 7400.0
    technology: 1050.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 2200.0
    economy: 0.0
    technology: 350.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 5650.0
    economy: 8700.0
    technology: 1200.0
    upgrade: 625.0
  }
  total_used_vespene {
    none: 0.0
    army: 2800.0
    economy: 0.0
    technology: 450.0
    upgrade: 625.0
  }
  total_damage_dealt {
    life: 7135.73339844
    shields: 10745.8398438
    energy: 0.0
  }
  total_damage_taken {
    life: 5355.71679688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2185
vespene: 783
food_cap: 198
food_used: 158
food_army: 99
food_workers: 59
idle_worker_count: 0
army_count: 72
warp_gate_count: 0

game_loop:  14049
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 72
}
groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 22
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 93
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 88
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  21268
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
