----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2562
  player_apm: 51
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2481
  player_apm: 67
}
game_duration_loops: 10706
game_duration_seconds: 477.979797363
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9038
score_details {
  idle_production_time: 6389.75
  idle_worker_time: 1387.75
  total_value_units: 4800.0
  total_value_structures: 1275.0
  killed_value_units: 200.0
  killed_value_structures: 600.0
  collected_minerals: 6950.0
  collected_vespene: 988.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 425.0
  spent_minerals: 5425.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 75.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4050.0
    technology: 875.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4200.0
    technology: 1025.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1103.29736328
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1575
vespene: 388
food_cap: 84
food_used: 72
food_army: 29
food_workers: 37
idle_worker_count: 8
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 9
}
single {
  unit {
    unit_type: 97
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8516
score_details {
  idle_production_time: 6829.375
  idle_worker_time: 1655.0625
  total_value_units: 5100.0
  total_value_structures: 1275.0
  killed_value_units: 400.0
  killed_value_structures: 800.0
  collected_minerals: 7600.0
  collected_vespene: 1216.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 358.0
  spent_minerals: 6425.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 100.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1125.0
    economy: 4050.0
    technology: 875.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 150.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4500.0
    technology: 1025.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1928.75634766
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2263.03027344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1225
vespene: 116
food_cap: 84
food_used: 69
food_army: 26
food_workers: 43
idle_worker_count: 2
army_count: 1
warp_gate_count: 0

game_loop:  10706
ui_data{
 
Score:  8516
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
