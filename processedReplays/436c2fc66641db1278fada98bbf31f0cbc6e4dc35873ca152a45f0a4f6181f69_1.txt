----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2711
  player_apm: 78
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2734
  player_apm: 83
}
game_duration_loops: 20935
game_duration_seconds: 934.663452148
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8481
score_details {
  idle_production_time: 1921.125
  idle_worker_time: 667.25
  total_value_units: 4150.0
  total_value_structures: 3800.0
  killed_value_units: 2450.0
  killed_value_structures: 0.0
  collected_minerals: 7135.0
  collected_vespene: 2096.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 515.0
  spent_minerals: 6500.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1325.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 50.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 3650.0
    technology: 1650.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 500.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 3600.0
    technology: 1500.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 500.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3820.23828125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1101.0
    shields: 1115.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 685
vespene: 596
food_cap: 86
food_used: 51
food_army: 14
food_workers: 35
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 496
    player_relative: 1
    health: 300
    shields: 42
    energy: 0
  }
}

score{
 score_type: Melee
score: 21049
score_details {
  idle_production_time: 6444.0
  idle_worker_time: 1646.5
  total_value_units: 14450.0
  total_value_structures: 8650.0
  killed_value_units: 5600.0
  killed_value_structures: 0.0
  collected_minerals: 19135.0
  collected_vespene: 7064.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 649.0
  spent_minerals: 18125.0
  spent_vespene: 6725.0
  food_used {
    none: 0.0
    army: 61.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2475.0
    economy: 2400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3550.0
    economy: 50.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4100.0
    economy: 6300.0
    technology: 4050.0
    upgrade: 775.0
  }
  used_vespene {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 800.0
    upgrade: 775.0
  }
  total_used_minerals {
    none: 0.0
    army: 6950.0
    economy: 6350.0
    technology: 4350.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 4650.0
    economy: 0.0
    technology: 800.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 8889.52832031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3812.75
    shields: 3142.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1060
vespene: 339
food_cap: 164
food_used: 117
food_army: 61
food_workers: 56
idle_worker_count: 4
army_count: 22
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
multi {
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 93
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 186
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 18
    energy: 137
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 187
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 0
    energy: 186
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 18
    energy: 138
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 13
    shields: 0
    energy: 171
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 118
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 93
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 69
    shields: 0
    energy: 93
  }
}

score{
 score_type: Melee
score: 21094
score_details {
  idle_production_time: 7051.5625
  idle_worker_time: 1833.5625
  total_value_units: 14450.0
  total_value_structures: 8650.0
  killed_value_units: 6000.0
  killed_value_structures: 0.0
  collected_minerals: 20720.0
  collected_vespene: 7524.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 671.0
  spent_minerals: 19075.0
  spent_vespene: 6725.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2775.0
    economy: 2400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4750.0
    economy: 50.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2900.0
    economy: 6500.0
    technology: 4800.0
    upgrade: 775.0
  }
  used_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 800.0
    upgrade: 775.0
  }
  total_used_minerals {
    none: 0.0
    army: 6950.0
    economy: 6350.0
    technology: 4350.0
    upgrade: 775.0
  }
  total_used_vespene {
    none: 0.0
    army: 4650.0
    economy: 0.0
    technology: 800.0
    upgrade: 775.0
  }
  total_damage_dealt {
    life: 9336.953125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4665.75
    shields: 3501.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1695
vespene: 799
food_cap: 164
food_used: 101
food_army: 45
food_workers: 56
idle_worker_count: 4
army_count: 14
warp_gate_count: 1

game_loop:  20935
ui_data{
 
Score:  21094
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
