----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3586
  player_apm: 169
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3631
  player_apm: 149
}
game_duration_loops: 18957
game_duration_seconds: 846.35369873
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8788
score_details {
  idle_production_time: 1614.375
  idle_worker_time: 263.75
  total_value_units: 4950.0
  total_value_structures: 3125.0
  killed_value_units: 1400.0
  killed_value_structures: 0.0
  collected_minerals: 7210.0
  collected_vespene: 1892.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 335.0
  spent_minerals: 6879.0
  spent_vespene: 1536.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 493.0
    technology: -114.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: -114.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1900.0
    economy: 3650.0
    technology: 1125.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 675.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 2050.0
    economy: 4300.0
    technology: 1000.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 375.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1315.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1241.49023438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 88.9326171875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 367
vespene: 346
food_cap: 86
food_used: 70
food_army: 38
food_workers: 30
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 25
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 186
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7701
score_details {
  idle_production_time: 6179.875
  idle_worker_time: 469.8125
  total_value_units: 12950.0
  total_value_structures: 4675.0
  killed_value_units: 9200.0
  killed_value_structures: 1675.0
  collected_minerals: 16190.0
  collected_vespene: 3912.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 0.0
  spent_minerals: 15829.0
  spent_vespene: 3811.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 11.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5875.0
    economy: 2925.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1925.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6485.0
    economy: 3418.0
    technology: 186.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2178.0
    economy: 0.0
    technology: 11.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1250.0
    economy: 2675.0
    technology: 1475.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 425.0
    economy: 0.0
    technology: 375.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 7400.0
    economy: 6600.0
    technology: 1475.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 2450.0
    economy: 0.0
    technology: 500.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 13599.4824219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15915.4550781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2615.13476562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 352
vespene: 49
food_cap: 94
food_used: 36
food_army: 25
food_workers: 11
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  18957
ui_data{
 
Score:  7701
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
