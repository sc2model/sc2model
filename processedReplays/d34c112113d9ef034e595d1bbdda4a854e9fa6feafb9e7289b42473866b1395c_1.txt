----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3781
  player_apm: 274
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3804
  player_apm: 149
}
game_duration_loops: 26300
game_duration_seconds: 1174.18908691
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11131
score_details {
  idle_production_time: 9355.9375
  idle_worker_time: 1.0
  total_value_units: 6350.0
  total_value_structures: 2200.0
  killed_value_units: 250.0
  killed_value_structures: 400.0
  collected_minerals: 9220.0
  collected_vespene: 1736.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 582.0
  spent_minerals: 8525.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 25.5
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 725.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1475.0
    economy: 5025.0
    technology: 1550.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 25.0
    technology: 400.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 1950.0
    economy: 6100.0
    technology: 1450.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 50.0
    technology: 300.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 624.5
    shields: 1407.07861328
    energy: 0.0
  }
  total_damage_taken {
    life: 1489.60449219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 745
vespene: 211
food_cap: 104
food_used: 68
food_army: 25
food_workers: 42
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 28117
score_details {
  idle_production_time: 36790.5625
  idle_worker_time: 141.0
  total_value_units: 24695.0
  total_value_structures: 5175.0
  killed_value_units: 1665.0
  killed_value_structures: 3400.0
  collected_minerals: 27730.0
  collected_vespene: 9772.0
  collection_rate_minerals: 2743.0
  collection_rate_vespene: 1433.0
  spent_minerals: 27585.0
  spent_vespene: 8125.0
  food_used {
    none: 0.0
    army: 101.5
    economy: 78.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 440.0
    economy: 2650.0
    technology: 1800.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7045.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4875.0
    economy: 9425.0
    technology: 4675.0
    upgrade: 2175.0
  }
  used_vespene {
    none: 0.0
    army: 2275.0
    economy: 25.0
    technology: 650.0
    upgrade: 2175.0
  }
  total_used_minerals {
    none: 0.0
    army: 11270.0
    economy: 11800.0
    technology: 5025.0
    upgrade: 1775.0
  }
  total_used_vespene {
    none: 0.0
    army: 5325.0
    economy: 50.0
    technology: 900.0
    upgrade: 1775.0
  }
  total_damage_dealt {
    life: 7339.31445312
    shields: 11295.6435547
    energy: 0.0
  }
  total_damage_taken {
    life: 8678.0390625
    shields: 574.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 195
vespene: 1647
food_cap: 188
food_used: 179
food_army: 101
food_workers: 78
idle_worker_count: 0
army_count: 39
warp_gate_count: 0
larva_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 127
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8951
score_details {
  idle_production_time: 47574.4375
  idle_worker_time: 692.125
  total_value_units: 36395.0
  total_value_structures: 6750.0
  killed_value_units: 7245.0
  killed_value_structures: 4450.0
  collected_minerals: 36190.0
  collected_vespene: 12396.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 134.0
  spent_minerals: 35810.0
  spent_vespene: 11625.0
  food_used {
    none: 0.0
    army: 8.5
    economy: 16.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3570.0
    economy: 4050.0
    technology: 1950.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 17795.0
    economy: 7625.0
    technology: 5550.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 8575.0
    economy: 0.0
    technology: 950.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 125.0
    economy: 3675.0
    technology: 75.0
    upgrade: 1775.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 25.0
    technology: 0.0
    upgrade: 1775.0
  }
  total_used_minerals {
    none: 0.0
    army: 18170.0
    economy: 14275.0
    technology: 5825.0
    upgrade: 1775.0
  }
  total_used_vespene {
    none: 0.0
    army: 9125.0
    economy: 50.0
    technology: 1300.0
    upgrade: 1775.0
  }
  total_damage_dealt {
    life: 13592.4394531
    shields: 21755.3945312
    energy: 0.0
  }
  total_damage_taken {
    life: 65402.8789062
    shields: 574.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 430
vespene: 771
food_cap: 76
food_used: 24
food_army: 8
food_workers: 16
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  26300
ui_data{
 
Score:  8951
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
