----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3469
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 187
}
game_duration_loops: 8488
game_duration_seconds: 378.95501709
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7713
score_details {
  idle_production_time: 9992.8125
  idle_worker_time: 9.9375
  total_value_units: 5400.0
  total_value_structures: 1525.0
  killed_value_units: 1850.0
  killed_value_structures: 350.0
  collected_minerals: 6450.0
  collected_vespene: 744.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 156.0
  spent_minerals: 6256.0
  spent_vespene: 525.0
  food_used {
    none: 0.0
    army: 46.5
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1225.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 475.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 3225.0
    technology: 850.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2625.0
    economy: 3725.0
    technology: 850.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4732.4140625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1727.25927734
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 244
vespene: 219
food_cap: 90
food_used: 70
food_army: 46
food_workers: 24
idle_worker_count: 0
army_count: 54
warp_gate_count: 0

game_loop:  8488
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 21
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 90
  count: 2
}

Score:  7713
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
