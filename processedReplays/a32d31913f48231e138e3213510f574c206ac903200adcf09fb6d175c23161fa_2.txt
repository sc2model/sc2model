----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3739
  player_apm: 197
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3811
  player_apm: 123
}
game_duration_loops: 6807
game_duration_seconds: 303.90512085
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5390
score_details {
  idle_production_time: 552.9375
  idle_worker_time: 328.9375
  total_value_units: 2700.0
  total_value_structures: 1750.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 4105.0
  collected_vespene: 1060.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 223.0
  spent_minerals: 3550.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 650.0
    economy: 2650.0
    technology: 550.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1050.0
    economy: 2400.0
    technology: 550.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 756.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 554.375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 605
vespene: 85
food_cap: 47
food_used: 36
food_army: 13
food_workers: 23
idle_worker_count: 24
army_count: 5
warp_gate_count: 0

game_loop:  6807
ui_data{
 
Score:  5390
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
