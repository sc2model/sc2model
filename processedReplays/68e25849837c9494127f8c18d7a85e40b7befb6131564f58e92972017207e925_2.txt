----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3696
  player_apm: 187
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3475
  player_apm: 100
}
game_duration_loops: 12698
game_duration_seconds: 566.914550781
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6693
score_details {
  idle_production_time: 6309.875
  idle_worker_time: 3.0
  total_value_units: 5500.0
  total_value_structures: 1500.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 6835.0
  collected_vespene: 908.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 156.0
  spent_minerals: 6350.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1600.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 3350.0
    technology: 700.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 4000.0
    technology: 700.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2803.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4534.45410156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 535
vespene: 758
food_cap: 68
food_used: 46
food_army: 17
food_workers: 29
idle_worker_count: 3
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 5
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4501
score_details {
  idle_production_time: 9598.1875
  idle_worker_time: 185.9375
  total_value_units: 6800.0
  total_value_structures: 1800.0
  killed_value_units: 1450.0
  killed_value_structures: 0.0
  collected_minerals: 7960.0
  collected_vespene: 1116.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 156.0
  spent_minerals: 7925.0
  spent_vespene: 225.0
  food_used {
    none: 1.5
    army: 2.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2975.0
    economy: 2050.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 2200.0
    technology: 700.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3500.0
    economy: 4850.0
    technology: 700.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4224.38671875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10152.1572266
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 85
vespene: 891
food_cap: 52
food_used: 20
food_army: 2
food_workers: 17
idle_worker_count: 17
army_count: 3
warp_gate_count: 0

game_loop:  12698
ui_data{
 
Score:  4501
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
