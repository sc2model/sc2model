----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4435
  player_apm: 198
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4307
  player_apm: 125
}
game_duration_loops: 7765
game_duration_seconds: 346.675964355
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7115
score_details {
  idle_production_time: 3266.0
  idle_worker_time: 1.5
  total_value_units: 5050.0
  total_value_structures: 1500.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 6885.0
  collected_vespene: 736.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 268.0
  spent_minerals: 5956.0
  spent_vespene: 350.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1150.0
    economy: 381.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 3975.0
    technology: 675.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 4875.0
    technology: 675.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 529.163574219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2929.68310547
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 979
vespene: 386
food_cap: 82
food_used: 44
food_army: 6
food_workers: 38
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  7765
ui_data{
 
Score:  7115
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
