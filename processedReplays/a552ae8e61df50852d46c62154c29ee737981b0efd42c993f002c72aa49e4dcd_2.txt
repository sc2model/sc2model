----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3234
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3317
  player_apm: 114
}
game_duration_loops: 7007
game_duration_seconds: 312.834320068
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4656
score_details {
  idle_production_time: 493.375
  idle_worker_time: 125.375
  total_value_units: 3175.0
  total_value_structures: 1825.0
  killed_value_units: 475.0
  killed_value_structures: 0.0
  collected_minerals: 4115.0
  collected_vespene: 1216.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 313.0
  spent_minerals: 3400.0
  spent_vespene: 700.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 875.0
    economy: 225.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 375.0
    economy: 2150.0
    technology: 600.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1375.0
    economy: 2375.0
    technology: 600.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 527.625
    shields: 595.375
    energy: 0.0
  }
  total_damage_taken {
    life: 1652.375
    shields: 1851.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 765
vespene: 516
food_cap: 55
food_used: 28
food_army: 6
food_workers: 22
idle_worker_count: 2
army_count: 3
warp_gate_count: 2

game_loop:  7007
ui_data{
 single {
  unit {
    unit_type: 74
    player_relative: 3
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  4656
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
