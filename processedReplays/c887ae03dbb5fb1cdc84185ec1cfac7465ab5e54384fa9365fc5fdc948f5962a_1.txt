----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3380
  player_apm: 139
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3345
  player_apm: 90
}
game_duration_loops: 23175
game_duration_seconds: 1034.67041016
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12469
score_details {
  idle_production_time: 4868.5
  idle_worker_time: 0.375
  total_value_units: 9000.0
  total_value_structures: 1550.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 10460.0
  collected_vespene: 2184.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 739.0
  spent_minerals: 10325.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 62.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2700.0
    economy: 6150.0
    technology: 850.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 100.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2775.0
    economy: 7150.0
    technology: 1000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 961.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1821.85009766
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 185
vespene: 934
food_cap: 130
food_used: 127
food_army: 62
food_workers: 65
idle_worker_count: 0
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 110
  count: 27
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 28707
score_details {
  idle_production_time: 27422.0
  idle_worker_time: 279.0
  total_value_units: 25775.0
  total_value_structures: 3525.0
  killed_value_units: 7150.0
  killed_value_structures: 1000.0
  collected_minerals: 29900.0
  collected_vespene: 9032.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 1052.0
  spent_minerals: 25675.0
  spent_vespene: 8300.0
  food_used {
    none: 0.0
    army: 130.5
    economy: 69.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5150.0
    economy: 1250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7650.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5875.0
    economy: 8950.0
    technology: 1975.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 0.0
    army: 2500.0
    economy: 0.0
    technology: 650.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 0.0
    army: 13450.0
    economy: 10500.0
    technology: 2325.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 6125.0
    economy: 0.0
    technology: 900.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 10212.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12799.5019531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 4275
vespene: 732
food_cap: 200
food_used: 199
food_army: 130
food_workers: 69
idle_worker_count: 3
army_count: 82
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 110
  count: 49
}
groups {
  control_group_index: 5
  leader_unit_type: 493
  count: 18
}
multi {
  units {
    unit_type: 494
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 494
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 494
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 494
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 493
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 494
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 494
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 29600
score_details {
  idle_production_time: 33849.4375
  idle_worker_time: 2650.625
  total_value_units: 29450.0
  total_value_structures: 3525.0
  killed_value_units: 10000.0
  killed_value_structures: 3200.0
  collected_minerals: 33520.0
  collected_vespene: 11580.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 918.0
  spent_minerals: 29450.0
  spent_vespene: 8775.0
  food_used {
    none: 0.0
    army: 130.0
    economy: 69.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7200.0
    economy: 2100.0
    technology: 950.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2500.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11550.0
    economy: 400.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5750.0
    economy: 8950.0
    technology: 1850.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 650.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 0.0
    army: 16450.0
    economy: 10500.0
    technology: 2325.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 0.0
    army: 6800.0
    economy: 0.0
    technology: 900.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 24065.6484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 18252.1914062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 4120
vespene: 2805
food_cap: 200
food_used: 199
food_army: 130
food_workers: 69
idle_worker_count: 18
army_count: 84
warp_gate_count: 0

game_loop:  23175
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 88
}
groups {
  control_group_index: 5
  leader_unit_type: 493
  count: 17
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 34
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  29600
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
