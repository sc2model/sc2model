----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3886
  player_apm: 257
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3834
  player_apm: 132
}
game_duration_loops: 27792
game_duration_seconds: 1240.80090332
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7411
score_details {
  idle_production_time: 1703.5625
  idle_worker_time: 58.9375
  total_value_units: 4350.0
  total_value_structures: 2675.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 6745.0
  collected_vespene: 2016.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 358.0
  spent_minerals: 6375.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 750.0
    economy: 3800.0
    technology: 1075.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1950.0
    economy: 3700.0
    technology: 925.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1798.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1620.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 420
vespene: 566
food_cap: 70
food_used: 47
food_army: 14
food_workers: 33
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 22
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 127
  }
}

score{
 score_type: Melee
score: 26039
score_details {
  idle_production_time: 7745.875
  idle_worker_time: 1528.25
  total_value_units: 13325.0
  total_value_structures: 9875.0
  killed_value_units: 1450.0
  killed_value_structures: 100.0
  collected_minerals: 22200.0
  collected_vespene: 8232.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 963.0
  spent_minerals: 22112.0
  spent_vespene: 6956.0
  food_used {
    none: 0.0
    army: 95.0
    economy: 72.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 550.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3600.0
    economy: 0.0
    technology: 212.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 131.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5300.0
    economy: 8600.0
    technology: 4275.0
    upgrade: 850.0
  }
  used_vespene {
    none: 50.0
    army: 2775.0
    economy: 150.0
    technology: 1725.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 50.0
    army: 7150.0
    economy: 9100.0
    technology: 4000.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 50.0
    army: 2575.0
    economy: 300.0
    technology: 1775.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 3150.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5985.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 138
vespene: 1276
food_cap: 187
food_used: 167
food_army: 95
food_workers: 72
idle_worker_count: 2
army_count: 35
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 29
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 5
}
groups {
  control_group_index: 7
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 28
  count: 6
}
multi {
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 27391
score_details {
  idle_production_time: 19965.5
  idle_worker_time: 4693.375
  total_value_units: 27450.0
  total_value_structures: 11825.0
  killed_value_units: 10850.0
  killed_value_structures: 875.0
  collected_minerals: 33750.0
  collected_vespene: 14868.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 671.0
  spent_minerals: 32837.0
  spent_vespene: 12856.0
  food_used {
    none: 0.0
    army: 74.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5450.0
    economy: 2375.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3650.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12544.0
    economy: 1737.0
    technology: 937.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6191.0
    economy: 150.0
    technology: 231.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 4325.0
    economy: 8050.0
    technology: 4200.0
    upgrade: 2125.0
  }
  used_vespene {
    none: 50.0
    army: 2675.0
    economy: 0.0
    technology: 1650.0
    upgrade: 2125.0
  }
  total_used_minerals {
    none: 50.0
    army: 15550.0
    economy: 10500.0
    technology: 4775.0
    upgrade: 1625.0
  }
  total_used_vespene {
    none: 50.0
    army: 7950.0
    economy: 300.0
    technology: 1900.0
    upgrade: 1625.0
  }
  total_damage_dealt {
    life: 15439.1640625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25541.1582031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2052.01635742
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 506
vespene: 1635
food_cap: 188
food_used: 141
food_army: 74
food_workers: 67
idle_worker_count: 15
army_count: 16
warp_gate_count: 0

game_loop:  27792
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  27391
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
