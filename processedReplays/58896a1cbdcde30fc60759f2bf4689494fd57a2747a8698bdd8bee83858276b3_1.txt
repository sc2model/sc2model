----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 6102
  player_apm: 258
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 6552
  player_apm: 320
}
game_duration_loops: 15197
game_duration_seconds: 678.484863281
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8771
score_details {
  idle_production_time: 1177.4375
  idle_worker_time: 298.4375
  total_value_units: 5875.0
  total_value_structures: 3100.0
  killed_value_units: 1925.0
  killed_value_structures: 0.0
  collected_minerals: 7665.0
  collected_vespene: 2412.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 649.0
  spent_minerals: 7350.0
  spent_vespene: 2050.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1550.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1000.0
    economy: 4600.0
    technology: 1000.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1850.0
    economy: 5600.0
    technology: 700.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2349.84130859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3350.875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 397.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 314
vespene: 332
food_cap: 93
food_used: 61
food_army: 20
food_workers: 38
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 47
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 56
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12134
score_details {
  idle_production_time: 3722.375
  idle_worker_time: 682.8125
  total_value_units: 11950.0
  total_value_structures: 5675.0
  killed_value_units: 6725.0
  killed_value_structures: 0.0
  collected_minerals: 14635.0
  collected_vespene: 4920.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 649.0
  spent_minerals: 14050.0
  spent_vespene: 4725.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5300.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3491.0
    economy: 1400.0
    technology: 116.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2424.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1500.0
    economy: 5950.0
    technology: 1900.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 1000.0
    economy: 0.0
    technology: 825.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 4700.0
    economy: 7950.0
    technology: 2000.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 3250.0
    economy: 0.0
    technology: 825.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 7404.44628906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10977.6982422
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 574.822998047
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 543
vespene: 116
food_cap: 124
food_used: 83
food_army: 26
food_workers: 56
idle_worker_count: 56
army_count: 8
warp_gate_count: 0

game_loop:  15197
ui_data{
 
Score:  12134
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
