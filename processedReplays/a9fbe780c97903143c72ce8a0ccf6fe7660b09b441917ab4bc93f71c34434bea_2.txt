----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3137
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3262
  player_apm: 156
}
game_duration_loops: 20990
game_duration_seconds: 937.11895752
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12176
score_details {
  idle_production_time: 1222.875
  idle_worker_time: 62.1875
  total_value_units: 6700.0
  total_value_structures: 4000.0
  killed_value_units: 550.0
  killed_value_structures: 0.0
  collected_minerals: 9635.0
  collected_vespene: 2316.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 649.0
  spent_minerals: 9225.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 50.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3100.0
    economy: 4100.0
    technology: 2350.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 3100.0
    economy: 4200.0
    technology: 1900.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 698.125
    shields: 725.875
    energy: 0.0
  }
  total_damage_taken {
    life: 515.0
    shields: 972.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 460
vespene: 466
food_cap: 94
food_used: 94
food_army: 50
food_workers: 44
idle_worker_count: 0
army_count: 18
warp_gate_count: 9

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 83
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 2
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 19878
score_details {
  idle_production_time: 5045.25
  idle_worker_time: 911.0
  total_value_units: 19950.0
  total_value_structures: 5950.0
  killed_value_units: 8800.0
  killed_value_structures: 0.0
  collected_minerals: 22330.0
  collected_vespene: 7048.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 335.0
  spent_minerals: 20200.0
  spent_vespene: 6325.0
  food_used {
    none: 0.0
    army: 91.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7825.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7000.0
    economy: 175.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5000.0
    economy: 5500.0
    technology: 2500.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 2075.0
    economy: 0.0
    technology: 300.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 12125.0
    economy: 5750.0
    technology: 2500.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 5225.0
    economy: 0.0
    technology: 300.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 7618.77636719
    shields: 5888.90136719
    energy: 0.0
  }
  total_damage_taken {
    life: 5751.63720703
    shields: 7362.24951172
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2180
vespene: 723
food_cap: 157
food_used: 140
food_army: 91
food_workers: 49
idle_worker_count: 14
army_count: 43
warp_gate_count: 9

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 35
}
groups {
  control_group_index: 3
  leader_unit_type: 77
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 2
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 15280
score_details {
  idle_production_time: 5516.5
  idle_worker_time: 1469.25
  total_value_units: 22525.0
  total_value_structures: 5950.0
  killed_value_units: 15175.0
  killed_value_structures: 0.0
  collected_minerals: 23440.0
  collected_vespene: 7340.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 627.0
  spent_minerals: 22625.0
  spent_vespene: 7175.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10850.0
    economy: 2050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11325.0
    economy: 175.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3100.0
    economy: 5500.0
    technology: 2500.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 300.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 14050.0
    economy: 5750.0
    technology: 2500.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 5875.0
    economy: 0.0
    technology: 300.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 11040.7763672
    shields: 11556.2763672
    energy: 0.0
  }
  total_damage_taken {
    life: 8949.63671875
    shields: 10043.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 865
vespene: 165
food_cap: 157
food_used: 106
food_army: 57
food_workers: 49
idle_worker_count: 0
army_count: 26
warp_gate_count: 9

game_loop:  20990
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 73
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 2
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 61
    shields: 36
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

Score:  15280
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
