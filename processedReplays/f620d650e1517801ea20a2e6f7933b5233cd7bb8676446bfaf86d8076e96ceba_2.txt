----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3015
  player_apm: 95
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2998
  player_apm: 71
}
game_duration_loops: 12947
game_duration_seconds: 578.031433105
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7445
score_details {
  idle_production_time: 1634.8125
  idle_worker_time: 205.4375
  total_value_units: 1400.0
  total_value_structures: 5600.0
  killed_value_units: 2750.0
  killed_value_structures: 225.0
  collected_minerals: 6630.0
  collected_vespene: 1264.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 268.0
  spent_minerals: 6299.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2025.0
    economy: 325.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 275.0
    technology: 824.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 2700.0
    technology: 3000.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 100.0
    economy: 3050.0
    technology: 3750.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 5217.02148438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1793.76977539
    shields: 2283.39013672
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 381
vespene: 1164
food_cap: 95
food_used: 25
food_army: 2
food_workers: 23
idle_worker_count: 1
army_count: 1
warp_gate_count: 0

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 8799
score_details {
  idle_production_time: 2371.5625
  idle_worker_time: 320.8125
  total_value_units: 1400.0
  total_value_structures: 7000.0
  killed_value_units: 4550.0
  killed_value_structures: 225.0
  collected_minerals: 8260.0
  collected_vespene: 1888.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 291.0
  spent_minerals: 7999.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3450.0
    economy: 325.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 575.0
    technology: 1424.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 2900.0
    technology: 3600.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 100.0
    economy: 3550.0
    technology: 4650.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 8256.44042969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2905.49560547
    shields: 3994.59619141
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 311
vespene: 1788
food_cap: 111
food_used: 25
food_army: 2
food_workers: 23
idle_worker_count: 1
army_count: 1
warp_gate_count: 0

game_loop:  12947
ui_data{
 single {
  unit {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 200
  }
}

Score:  8799
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
