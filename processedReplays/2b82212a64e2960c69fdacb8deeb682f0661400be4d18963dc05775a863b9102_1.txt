----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3824
  player_apm: 172
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 120
}
game_duration_loops: 17800
game_duration_seconds: 794.698303223
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8618
score_details {
  idle_production_time: 1414.4375
  idle_worker_time: 776.8125
  total_value_units: 4675.0
  total_value_structures: 3650.0
  killed_value_units: 1350.0
  killed_value_structures: 0.0
  collected_minerals: 7520.0
  collected_vespene: 2000.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 380.0
  spent_minerals: 7350.0
  spent_vespene: 1875.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 950.0
    economy: 4550.0
    technology: 1150.0
    upgrade: 300.0
  }
  used_vespene {
    none: 100.0
    army: 525.0
    economy: 0.0
    technology: 300.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 100.0
    army: 1550.0
    economy: 5050.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 100.0
    army: 975.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2036.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1523.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 18.001953125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 218
vespene: 125
food_cap: 85
food_used: 61
food_army: 19
food_workers: 39
idle_worker_count: 2
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 5
}
groups {
  control_group_index: 1
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 32
  count: 6
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22091
score_details {
  idle_production_time: 5540.875
  idle_worker_time: 1396.0625
  total_value_units: 15675.0
  total_value_structures: 7150.0
  killed_value_units: 8675.0
  killed_value_structures: 350.0
  collected_minerals: 21420.0
  collected_vespene: 6448.0
  collection_rate_minerals: 2659.0
  collection_rate_vespene: 1075.0
  spent_minerals: 20900.0
  spent_vespene: 4650.0
  food_used {
    none: 0.0
    army: 101.0
    economy: 71.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6025.0
    economy: 750.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4600.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5300.0
    economy: 7950.0
    technology: 2650.0
    upgrade: 900.0
  }
  used_vespene {
    none: 100.0
    army: 1125.0
    economy: 0.0
    technology: 700.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 100.0
    army: 9100.0
    economy: 8550.0
    technology: 2650.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 100.0
    army: 2825.0
    economy: 0.0
    technology: 700.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 11137.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5862.21142578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1335.39111328
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 568
vespene: 1798
food_cap: 196
food_used: 172
food_army: 101
food_workers: 71
idle_worker_count: 0
army_count: 65
warp_gate_count: 0

game_loop:  17800
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 1
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 32
  count: 27
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 8
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 120
    shields: 0
    energy: 149
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 140
    shields: 0
    energy: 150
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 69
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

Score:  22091
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
