----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3249
  player_apm: 169
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 68
}
game_duration_loops: 9169
game_duration_seconds: 409.358917236
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10411
score_details {
  idle_production_time: 1079.25
  idle_worker_time: 405.375
  total_value_units: 5625.0
  total_value_structures: 3100.0
  killed_value_units: 3350.0
  killed_value_structures: 250.0
  collected_minerals: 8065.0
  collected_vespene: 1996.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 649.0
  spent_minerals: 6775.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2350.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2075.0
    economy: 3900.0
    technology: 1100.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2575.0
    economy: 3900.0
    technology: 1100.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2198.10473633
    shields: 3332.16552734
    energy: 0.0
  }
  total_damage_taken {
    life: 510.0
    shields: 945.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1340
vespene: 646
food_cap: 86
food_used: 74
food_army: 32
food_workers: 42
idle_worker_count: 0
army_count: 14
warp_gate_count: 4

game_loop:  9169
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 65
  count: 1
}

Score:  10411
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
