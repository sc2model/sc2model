----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5343
  player_apm: 323
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5537
  player_apm: 195
}
game_duration_loops: 21830
game_duration_seconds: 974.621582031
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13730
score_details {
  idle_production_time: 991.375
  idle_worker_time: 189.4375
  total_value_units: 5750.0
  total_value_structures: 4350.0
  killed_value_units: 425.0
  killed_value_structures: 0.0
  collected_minerals: 10555.0
  collected_vespene: 2800.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 985.0
  spent_minerals: 10175.0
  spent_vespene: 2200.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1775.0
    economy: 5950.0
    technology: 2400.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 500.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1625.0
    economy: 5600.0
    technology: 1700.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 664.956054688
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 154.0
    shields: 156.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 430
vespene: 600
food_cap: 109
food_used: 103
food_army: 37
food_workers: 63
idle_worker_count: 1
army_count: 19
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 13
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 60
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 20243
score_details {
  idle_production_time: 5167.9375
  idle_worker_time: 1566.125
  total_value_units: 23250.0
  total_value_structures: 8900.0
  killed_value_units: 19725.0
  killed_value_structures: 0.0
  collected_minerals: 26215.0
  collected_vespene: 9528.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 851.0
  spent_minerals: 25675.0
  spent_vespene: 8025.0
  food_used {
    none: 0.0
    army: 67.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15225.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8100.0
    economy: 3250.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 225.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3750.0
    economy: 6200.0
    technology: 3600.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 500.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 12075.0
    economy: 9200.0
    technology: 3900.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 0.0
    army: 6475.0
    economy: 0.0
    technology: 500.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 20719.2832031
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10502.6572266
    shields: 14936.3935547
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 590
vespene: 1503
food_cap: 164
food_used: 121
food_army: 67
food_workers: 54
idle_worker_count: 5
army_count: 33
warp_gate_count: 9

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 28
}
groups {
  control_group_index: 2
  leader_unit_type: 694
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 33
    shields: 9
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 63
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 24
    shields: 9
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 3
    shields: 71
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 12
    shields: 9
    energy: 47
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 48
    shields: 9
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 28
    shields: 9
    energy: 47
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 53
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 12
    shields: 8
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 33
    energy: 0
  }
}

score{
 score_type: Melee
score: 16350
score_details {
  idle_production_time: 6158.875
  idle_worker_time: 2295.125
  total_value_units: 26675.0
  total_value_structures: 8900.0
  killed_value_units: 25950.0
  killed_value_structures: 0.0
  collected_minerals: 28785.0
  collected_vespene: 10540.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 649.0
  spent_minerals: 28050.0
  spent_vespene: 9075.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 19700.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13175.0
    economy: 3250.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 225.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 6400.0
    technology: 3600.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 500.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 14250.0
    economy: 9400.0
    technology: 3900.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 0.0
    army: 7525.0
    economy: 0.0
    technology: 500.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 26187.2128906
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13598.6572266
    shields: 19179.7695312
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 785
vespene: 1465
food_cap: 164
food_used: 74
food_army: 16
food_workers: 58
idle_worker_count: 8
army_count: 8
warp_gate_count: 9

game_loop:  21830
ui_data{
 multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 72
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 41
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 35
    energy: 0
  }
}

Score:  16350
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
