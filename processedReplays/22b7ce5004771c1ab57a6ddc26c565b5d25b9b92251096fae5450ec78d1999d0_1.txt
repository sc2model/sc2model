----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4194
  player_apm: 236
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 193
}
game_duration_loops: 12264
game_duration_seconds: 547.538208008
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8831
score_details {
  idle_production_time: 10725.0625
  idle_worker_time: 24.5
  total_value_units: 6600.0
  total_value_structures: 1575.0
  killed_value_units: 1200.0
  killed_value_structures: 150.0
  collected_minerals: 8600.0
  collected_vespene: 956.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 156.0
  spent_minerals: 7675.0
  spent_vespene: 700.0
  food_used {
    none: 0.0
    army: 14.5
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 550.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1325.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 875.0
    economy: 4675.0
    technology: 1000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 5225.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2076.56152344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2355.30761719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 975
vespene: 256
food_cap: 74
food_used: 68
food_army: 14
food_workers: 54
idle_worker_count: 6
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 105
  count: 17
}
groups {
  control_group_index: 1
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 24
  }
}

score{
 score_type: Melee
score: 11395
score_details {
  idle_production_time: 13935.0
  idle_worker_time: 196.25
  total_value_units: 9900.0
  total_value_structures: 1675.0
  killed_value_units: 1750.0
  killed_value_structures: 150.0
  collected_minerals: 12685.0
  collected_vespene: 2060.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 783.0
  spent_minerals: 11750.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1075.0
    economy: 550.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2475.0
    economy: 300.0
    technology: 237.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5875.0
    technology: 550.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 4125.0
    economy: 6825.0
    technology: 1150.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 250.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 3814.38037109
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7896.3125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 985
vespene: 635
food_cap: 122
food_used: 106
food_army: 45
food_workers: 61
idle_worker_count: 61
army_count: 14
warp_gate_count: 0

game_loop:  12264
ui_data{
 
Score:  11395
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
