----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4949
  player_apm: 261
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5129
  player_apm: 250
}
game_duration_loops: 15484
game_duration_seconds: 691.298217773
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12200
score_details {
  idle_production_time: 929.6875
  idle_worker_time: 192.5
  total_value_units: 5500.0
  total_value_structures: 4900.0
  killed_value_units: 625.0
  killed_value_structures: 0.0
  collected_minerals: 9720.0
  collected_vespene: 2280.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 671.0
  spent_minerals: 9600.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 5400.0
    technology: 1800.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 600.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 5750.0
    technology: 1800.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 600.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 503.0
    shields: 717.0
    energy: 0.0
  }
  total_damage_taken {
    life: 696.422363281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 40.4223632812
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 170
vespene: 555
food_cap: 133
food_used: 96
food_army: 47
food_workers: 47
idle_worker_count: 1
army_count: 29
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 49
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15206
score_details {
  idle_production_time: 3751.6875
  idle_worker_time: 675.9375
  total_value_units: 13900.0
  total_value_structures: 6950.0
  killed_value_units: 8725.0
  killed_value_structures: 100.0
  collected_minerals: 19515.0
  collected_vespene: 5200.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 671.0
  spent_minerals: 18975.0
  spent_vespene: 4375.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6525.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7600.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1300.0
    economy: 5675.0
    technology: 2600.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 875.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 0.0
    army: 8650.0
    economy: 7575.0
    technology: 2600.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 875.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 6006.42675781
    shields: 7871.38378906
    energy: 0.0
  }
  total_damage_taken {
    life: 11095.4443359
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2372.80761719
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 556
vespene: 825
food_cap: 173
food_used: 69
food_army: 26
food_workers: 43
idle_worker_count: 2
army_count: 11
warp_gate_count: 0

game_loop:  15484
ui_data{
 multi {
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 71
    shields: 0
    energy: 6
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 70
    shields: 0
    energy: 4
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 99
    shields: 0
    energy: 5
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 135
    shields: 0
    energy: 5
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
}

Score:  15206
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
