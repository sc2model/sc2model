----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2729
  player_apm: 145
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2544
  player_apm: 32
}
game_duration_loops: 4460
game_duration_seconds: 199.121032715
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1638
score_details {
  idle_production_time: 345.375
  idle_worker_time: 104.9375
  total_value_units: 1200.0
  total_value_structures: 1475.0
  killed_value_units: 75.0
  killed_value_structures: 0.0
  collected_minerals: 1580.0
  collected_vespene: 464.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 1600.0
  spent_vespene: 175.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 1106.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 600.0
    technology: 500.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 250.0
    economy: 1700.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 175.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 190.429199219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3106.67724609
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 11.6772460938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 24
vespene: 289
food_cap: 15
food_used: 3
food_army: 2
food_workers: 1
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  4460
ui_data{
 
Score:  1638
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
