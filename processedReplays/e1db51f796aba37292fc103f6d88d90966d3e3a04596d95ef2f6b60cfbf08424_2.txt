----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: -36400
  player_apm: 132
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3323
  player_apm: 82
}
game_duration_loops: 33786
game_duration_seconds: 1508.40881348
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7511
score_details {
  idle_production_time: 892.9375
  idle_worker_time: 270.9375
  total_value_units: 4500.0
  total_value_structures: 2825.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 6970.0
  collected_vespene: 1416.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 223.0
  spent_minerals: 6950.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1800.0
    economy: 2950.0
    technology: 1100.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 375.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 3950.0
    technology: 1100.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 375.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1205.46386719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1603.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 65.0581054688
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 70
vespene: 466
food_cap: 62
food_used: 62
food_army: 36
food_workers: 26
idle_worker_count: 0
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12781
score_details {
  idle_production_time: 4085.0
  idle_worker_time: 1727.0625
  total_value_units: 12625.0
  total_value_structures: 4475.0
  killed_value_units: 4875.0
  killed_value_structures: 125.0
  collected_minerals: 17265.0
  collected_vespene: 3116.0
  collection_rate_minerals: 2631.0
  collection_rate_vespene: 134.0
  spent_minerals: 17000.0
  spent_vespene: 2225.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3650.0
    economy: 850.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5900.0
    economy: 1500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 2850.0
    economy: 5200.0
    technology: 1600.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 475.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 8150.0
    economy: 6750.0
    technology: 1600.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 7844.45507812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10337.7529297
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1865.56152344
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 315
vespene: 891
food_cap: 109
food_used: 98
food_army: 57
food_workers: 41
idle_worker_count: 6
army_count: 37
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27477
score_details {
  idle_production_time: 7629.3125
  idle_worker_time: 6335.8125
  total_value_units: 21100.0
  total_value_structures: 8500.0
  killed_value_units: 5500.0
  killed_value_structures: 1100.0
  collected_minerals: 31921.0
  collected_vespene: 5756.0
  collection_rate_minerals: 1730.0
  collection_rate_vespene: 671.0
  spent_minerals: 26900.0
  spent_vespene: 4500.0
  food_used {
    none: 0.0
    army: 127.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3825.0
    economy: 2150.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8650.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 6100.0
    economy: 7800.0
    technology: 2800.0
    upgrade: 850.0
  }
  used_vespene {
    none: 100.0
    army: 1250.0
    economy: 150.0
    technology: 1150.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 100.0
    army: 14750.0
    economy: 9900.0
    technology: 2650.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 100.0
    army: 2250.0
    economy: 300.0
    technology: 1050.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 15989.9902344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12408.7529297
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2178.73583984
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5071
vespene: 1256
food_cap: 200
food_used: 181
food_army: 127
food_workers: 54
idle_worker_count: 3
army_count: 94
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 11883
score_details {
  idle_production_time: 10340.25
  idle_worker_time: 6931.625
  total_value_units: 27050.0
  total_value_structures: 8750.0
  killed_value_units: 7800.0
  killed_value_structures: 1100.0
  collected_minerals: 36975.0
  collected_vespene: 7108.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 44.0
  spent_minerals: 31900.0
  spent_vespene: 6950.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5725.0
    economy: 2150.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 18325.0
    economy: 8250.0
    technology: 1250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3925.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 975.0
    economy: 750.0
    technology: 1550.0
    upgrade: 850.0
  }
  used_vespene {
    none: 100.0
    army: 475.0
    economy: 0.0
    technology: 950.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 100.0
    army: 18850.0
    economy: 9900.0
    technology: 2800.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 100.0
    army: 4100.0
    economy: 300.0
    technology: 1150.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 27049.4316406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 49081.5546875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3036.97363281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5125
vespene: 158
food_cap: 15
food_used: 24
food_army: 23
food_workers: 1
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  33786
ui_data{
 multi {
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  11883
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
