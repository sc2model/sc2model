----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3287
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 123
}
game_duration_loops: 13440
game_duration_seconds: 600.041870117
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9319
score_details {
  idle_production_time: 6325.4375
  idle_worker_time: 602.3125
  total_value_units: 5100.0
  total_value_structures: 2125.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 7775.0
  collected_vespene: 1444.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 246.0
  spent_minerals: 6650.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1325.0
    economy: 4150.0
    technology: 1075.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 450.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 4650.0
    technology: 1375.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 600.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 1044.86352539
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3034.67724609
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1175
vespene: 69
food_cap: 90
food_used: 60
food_army: 22
food_workers: 37
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
production {
  unit {
    unit_type: 96
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10228
score_details {
  idle_production_time: 18650.75
  idle_worker_time: 602.3125
  total_value_units: 9450.0
  total_value_structures: 2475.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 11075.0
  collected_vespene: 2952.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 671.0
  spent_minerals: 10287.0
  spent_vespene: 2162.0
  food_used {
    none: 0.0
    army: 18.5
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3312.0
    economy: 400.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 912.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 1075.0
    economy: 5000.0
    technology: 1075.0
    upgrade: 400.0
  }
  used_vespene {
    none: -50.0
    army: 350.0
    economy: 0.0
    technology: 450.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 4500.0
    economy: 6050.0
    technology: 1375.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 600.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 1948.84790039
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6661.87011719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 838
vespene: 790
food_cap: 120
food_used: 58
food_army: 18
food_workers: 40
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  13440
ui_data{
 multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
}

Score:  10228
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
