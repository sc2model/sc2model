----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3848
  player_apm: 167
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3726
  player_apm: 184
}
game_duration_loops: 28933
game_duration_seconds: 1291.74194336
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5876
score_details {
  idle_production_time: 6008.8125
  idle_worker_time: 75.625
  total_value_units: 4750.0
  total_value_structures: 1275.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 5885.0
  collected_vespene: 616.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 156.0
  spent_minerals: 5700.0
  spent_vespene: 375.0
  food_used {
    none: 0.0
    army: 15.5
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 875.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 975.0
    economy: 3025.0
    technology: 1000.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 3825.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1388.57128906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2943.8359375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 235
vespene: 241
food_cap: 52
food_used: 43
food_army: 15
food_workers: 28
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16855
score_details {
  idle_production_time: 19145.1875
  idle_worker_time: 545.9375
  total_value_units: 17850.0
  total_value_structures: 2925.0
  killed_value_units: 9825.0
  killed_value_structures: 1075.0
  collected_minerals: 16905.0
  collected_vespene: 6000.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 940.0
  spent_minerals: 16025.0
  spent_vespene: 5350.0
  food_used {
    none: 0.0
    army: 74.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5575.0
    economy: 2050.0
    technology: 375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3250.0
    economy: 1650.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3850.0
    economy: 6000.0
    technology: 1325.0
    upgrade: 475.0
  }
  used_vespene {
    none: 0.0
    army: 2800.0
    economy: 0.0
    technology: 350.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 0.0
    army: 7050.0
    economy: 8600.0
    technology: 1775.0
    upgrade: 475.0
  }
  total_used_vespene {
    none: 0.0
    army: 4525.0
    economy: 0.0
    technology: 450.0
    upgrade: 475.0
  }
  total_damage_dealt {
    life: 14517.5449219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10623.9335938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 930
vespene: 650
food_cap: 126
food_used: 126
food_army: 74
food_workers: 52
idle_worker_count: 2
army_count: 36
warp_gate_count: 0
larva_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 14
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1488
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21906
score_details {
  idle_production_time: 46100.0625
  idle_worker_time: 2212.6875
  total_value_units: 30800.0
  total_value_structures: 4125.0
  killed_value_units: 24725.0
  killed_value_structures: 2550.0
  collected_minerals: 29690.0
  collected_vespene: 11516.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 604.0
  spent_minerals: 29425.0
  spent_vespene: 11200.0
  food_used {
    none: 0.0
    army: 104.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13325.0
    economy: 5975.0
    technology: 375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11300.0
    economy: 1650.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4650.0
    economy: 8500.0
    technology: 2125.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 650.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 16525.0
    economy: 11100.0
    technology: 2575.0
    upgrade: 1075.0
  }
  total_used_vespene {
    none: 0.0
    army: 9975.0
    economy: 0.0
    technology: 750.0
    upgrade: 1075.0
  }
  total_damage_dealt {
    life: 35679.2734375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27215.5
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 315
vespene: 316
food_cap: 200
food_used: 161
food_army: 104
food_workers: 55
idle_worker_count: 10
army_count: 34
warp_gate_count: 0

game_loop:  28933
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 16
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 121
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 157
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  21906
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
