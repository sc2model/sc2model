----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3177
  player_apm: 165
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3232
  player_apm: 67
}
game_duration_loops: 12059
game_duration_seconds: 538.385803223
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9412
score_details {
  idle_production_time: 7807.0
  idle_worker_time: 61.5
  total_value_units: 5950.0
  total_value_structures: 1725.0
  killed_value_units: 725.0
  killed_value_structures: 0.0
  collected_minerals: 8285.0
  collected_vespene: 1276.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 515.0
  spent_minerals: 7962.0
  spent_vespene: 1137.0
  food_used {
    none: 0.0
    army: 40.5
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 562.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 12.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 2125.0
    economy: 4125.0
    technology: 1150.0
    upgrade: 550.0
  }
  used_vespene {
    none: -50.0
    army: 400.0
    economy: 0.0
    technology: 150.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5025.0
    technology: 1300.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1122.44628906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2017.76220703
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 373
vespene: 139
food_cap: 82
food_used: 81
food_army: 40
food_workers: 41
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11650
score_details {
  idle_production_time: 10670.25
  idle_worker_time: 61.5
  total_value_units: 8500.0
  total_value_structures: 1725.0
  killed_value_units: 3750.0
  killed_value_structures: 600.0
  collected_minerals: 10975.0
  collected_vespene: 2024.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 515.0
  spent_minerals: 9712.0
  spent_vespene: 1612.0
  food_used {
    none: 0.0
    army: 43.5
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2575.0
    economy: 850.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1312.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 62.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 2275.0
    economy: 4400.0
    technology: 1400.0
    upgrade: 700.0
  }
  used_vespene {
    none: -50.0
    army: 250.0
    economy: 0.0
    technology: 350.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 4175.0
    economy: 5325.0
    technology: 1300.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 250.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 7116.59375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3478.12207031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1313
vespene: 412
food_cap: 106
food_used: 82
food_army: 43
food_workers: 39
idle_worker_count: 0
army_count: 33
warp_gate_count: 0

game_loop:  12059
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 122
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 142
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 142
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 119
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 138
    shields: 0
    energy: 0
  }
}

Score:  11650
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
