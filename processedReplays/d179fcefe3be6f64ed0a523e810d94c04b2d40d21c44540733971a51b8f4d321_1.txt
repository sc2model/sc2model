----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4924
  player_apm: 223
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5068
  player_apm: 212
}
game_duration_loops: 31883
game_duration_seconds: 1423.44750977
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11269
score_details {
  idle_production_time: 1210.9375
  idle_worker_time: 82.125
  total_value_units: 5475.0
  total_value_structures: 3350.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 8525.0
  collected_vespene: 2548.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 649.0
  spent_minerals: 8275.0
  spent_vespene: 2125.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: -300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2000.0
    economy: 5050.0
    technology: 1300.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 400.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2025.0
    economy: 5400.0
    technology: 850.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1216.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 656.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 204.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 268
vespene: 401
food_cap: 101
food_used: 83
food_army: 35
food_workers: 46
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 35
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 3
}
production {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 51
  }
  build_queue {
    unit_type: 45
    build_progress: 0.352941155434
  }
}

score{
 score_type: Melee
score: 26949
score_details {
  idle_production_time: 3946.6875
  idle_worker_time: 1085.5625
  total_value_units: 16975.0
  total_value_structures: 7300.0
  killed_value_units: 9225.0
  killed_value_structures: 400.0
  collected_minerals: 24765.0
  collected_vespene: 8564.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 649.0
  spent_minerals: 23549.0
  spent_vespene: 6718.0
  food_used {
    none: 0.0
    army: 118.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6950.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4907.0
    economy: 637.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 922.0
    economy: 37.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6000.0
    economy: 8050.0
    technology: 2600.0
    upgrade: 1525.0
  }
  used_vespene {
    none: 0.0
    army: 3500.0
    economy: 0.0
    technology: 750.0
    upgrade: 1525.0
  }
  total_used_minerals {
    none: 0.0
    army: 9775.0
    economy: 8200.0
    technology: 2600.0
    upgrade: 1350.0
  }
  total_used_vespene {
    none: 0.0
    army: 3850.0
    economy: 0.0
    technology: 750.0
    upgrade: 1350.0
  }
  total_damage_dealt {
    life: 9504.14550781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9542.14453125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3889.00439453
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1175
vespene: 1824
food_cap: 197
food_used: 175
food_army: 118
food_workers: 57
idle_worker_count: 1
army_count: 57
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 33
  count: 46
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 125
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 178
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 101
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 176
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 177
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 180
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 188
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 186
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 181
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 111
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 115
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 119
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 101
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 161
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 34450
score_details {
  idle_production_time: 8484.0625
  idle_worker_time: 1877.0625
  total_value_units: 34400.0
  total_value_structures: 9950.0
  killed_value_units: 21900.0
  killed_value_structures: 1550.0
  collected_minerals: 41520.0
  collected_vespene: 15360.0
  collection_rate_minerals: 3219.0
  collection_rate_vespene: 851.0
  spent_minerals: 38299.0
  spent_vespene: 11543.0
  food_used {
    none: 0.0
    army: 128.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 17250.0
    economy: 1400.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4200.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16807.0
    economy: 637.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5397.0
    economy: 37.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 400.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 6650.0
    economy: 9750.0
    technology: 2900.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 50.0
    army: 2875.0
    economy: 300.0
    technology: 950.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 50.0
    army: 22775.0
    economy: 11700.0
    technology: 2900.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 50.0
    army: 8025.0
    economy: 600.0
    technology: 950.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 24993.9589844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24759.7988281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 8202.49609375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3180
vespene: 3795
food_cap: 200
food_used: 190
food_army: 128
food_workers: 62
idle_worker_count: 6
army_count: 77
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 52
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 10
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 5
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 33322
score_details {
  idle_production_time: 8919.125
  idle_worker_time: 2644.875
  total_value_units: 36950.0
  total_value_structures: 9950.0
  killed_value_units: 29650.0
  killed_value_structures: 1775.0
  collected_minerals: 45025.0
  collected_vespene: 16352.0
  collection_rate_minerals: 2435.0
  collection_rate_vespene: 694.0
  spent_minerals: 40824.0
  spent_vespene: 12643.0
  food_used {
    none: 0.0
    army: 99.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 22950.0
    economy: 1400.0
    technology: 375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6250.0
    economy: 150.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 20407.0
    economy: 737.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6347.0
    economy: 37.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 400.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5000.0
    economy: 9650.0
    technology: 2900.0
    upgrade: 2050.0
  }
  used_vespene {
    none: 50.0
    army: 2475.0
    economy: 300.0
    technology: 950.0
    upgrade: 2050.0
  }
  total_used_minerals {
    none: 50.0
    army: 24725.0
    economy: 11700.0
    technology: 2900.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 50.0
    army: 8625.0
    economy: 600.0
    technology: 950.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 31519.2734375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 28986.9160156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 10224.03125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 4160
vespene: 3687
food_cap: 200
food_used: 159
food_army: 99
food_workers: 60
idle_worker_count: 6
army_count: 52
warp_gate_count: 0

game_loop:  31883
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 26
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 10
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 5
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 72
    shields: 0
    energy: 133
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 124
    shields: 0
    energy: 149
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 12
    shields: 0
    energy: 174
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 108
    shields: 0
    energy: 171
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 156
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 117
    shields: 0
    energy: 176
  }
}

Score:  33322
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
