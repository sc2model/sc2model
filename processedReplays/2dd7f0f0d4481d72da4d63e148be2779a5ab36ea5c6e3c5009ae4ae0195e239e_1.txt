----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3167
  player_apm: 88
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3075
  player_apm: 97
}
game_duration_loops: 21289
game_duration_seconds: 950.468139648
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11710
score_details {
  idle_production_time: 1345.875
  idle_worker_time: 220.5625
  total_value_units: 4375.0
  total_value_structures: 4250.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 8200.0
  collected_vespene: 2460.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 671.0
  spent_minerals: 7950.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5050.0
    technology: 1600.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1250.0
    economy: 4600.0
    technology: 1600.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 20.0
    shields: 28.25
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 50.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 300
vespene: 635
food_cap: 118
food_used: 86
food_army: 37
food_workers: 48
idle_worker_count: 2
army_count: 13
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 25791
score_details {
  idle_production_time: 4392.6875
  idle_worker_time: 967.6875
  total_value_units: 14950.0
  total_value_structures: 9350.0
  killed_value_units: 7125.0
  killed_value_structures: 0.0
  collected_minerals: 26045.0
  collected_vespene: 9696.0
  collection_rate_minerals: 2547.0
  collection_rate_vespene: 1007.0
  spent_minerals: 21000.0
  spent_vespene: 8350.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 73.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4325.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6425.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 8600.0
    technology: 3000.0
    upgrade: 2225.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 1300.0
    upgrade: 2225.0
  }
  total_used_minerals {
    none: 0.0
    army: 7825.0
    economy: 8700.0
    technology: 3000.0
    upgrade: 1800.0
  }
  total_used_vespene {
    none: 0.0
    army: 4675.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1800.0
  }
  total_damage_dealt {
    life: 3508.375
    shields: 4133.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5119.75
    shields: 3783.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 5095
vespene: 1346
food_cap: 200
food_used: 95
food_army: 22
food_workers: 73
idle_worker_count: 4
army_count: 11
warp_gate_count: 5

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 5
}
multi {
  units {
    unit_type: 80
    player_relative: 1
    health: 15
    shields: 4
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 102
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23463
score_details {
  idle_production_time: 5219.3125
  idle_worker_time: 1289.9375
  total_value_units: 17850.0
  total_value_structures: 9350.0
  killed_value_units: 7925.0
  killed_value_structures: 0.0
  collected_minerals: 27880.0
  collected_vespene: 10408.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 671.0
  spent_minerals: 25100.0
  spent_vespene: 10200.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4775.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8525.0
    economy: 2125.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2700.0
    economy: 6575.0
    technology: 2700.0
    upgrade: 2675.0
  }
  used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 1300.0
    upgrade: 2675.0
  }
  total_used_minerals {
    none: 0.0
    army: 10025.0
    economy: 8700.0
    technology: 3000.0
    upgrade: 2225.0
  }
  total_used_vespene {
    none: 0.0
    army: 5375.0
    economy: 0.0
    technology: 1300.0
    upgrade: 2225.0
  }
  total_damage_dealt {
    life: 3858.0
    shields: 4617.25
    energy: 0.0
  }
  total_damage_taken {
    life: 10067.0
    shields: 9553.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2830
vespene: 208
food_cap: 200
food_used: 90
food_army: 46
food_workers: 44
idle_worker_count: 4
army_count: 9
warp_gate_count: 3

game_loop:  21289
ui_data{
 
Score:  23463
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
