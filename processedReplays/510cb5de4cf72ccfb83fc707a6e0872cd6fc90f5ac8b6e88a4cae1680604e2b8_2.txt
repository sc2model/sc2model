----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2482
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3247
  player_apm: 96
}
game_duration_loops: 10142
game_duration_seconds: 452.799438477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5395
score_details {
  idle_production_time: 1364.0
  idle_worker_time: 178.6875
  total_value_units: 4775.0
  total_value_structures: 2775.0
  killed_value_units: 1975.0
  killed_value_structures: 0.0
  collected_minerals: 7045.0
  collected_vespene: 2000.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 313.0
  spent_minerals: 5900.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2175.0
    economy: 1675.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 1700.0
    technology: 950.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2175.0
    economy: 3375.0
    technology: 950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1118.0
    shields: 1310.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4220.0
    shields: 5191.72851562
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1195
vespene: 700
food_cap: 39
food_used: 23
food_army: 6
food_workers: 17
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 production {
  unit {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 69
    energy: 0
  }
  build_queue {
    unit_type: 83
    build_progress: 0.552272737026
  }
}

score{
 score_type: Melee
score: 5034
score_details {
  idle_production_time: 1381.75
  idle_worker_time: 178.6875
  total_value_units: 4775.0
  total_value_structures: 2775.0
  killed_value_units: 1975.0
  killed_value_structures: 0.0
  collected_minerals: 7110.0
  collected_vespene: 2024.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 268.0
  spent_minerals: 5900.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 8.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2175.0
    economy: 2125.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 1250.0
    technology: 950.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2175.0
    economy: 3375.0
    technology: 950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1118.0
    shields: 1310.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4372.0
    shields: 5457.72851562
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1260
vespene: 724
food_cap: 39
food_used: 14
food_army: 6
food_workers: 8
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  10142
ui_data{
 
Score:  5034
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
