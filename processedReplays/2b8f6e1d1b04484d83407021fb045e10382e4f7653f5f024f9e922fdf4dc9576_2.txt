----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3382
  player_apm: 149
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3376
  player_apm: 120
}
game_duration_loops: 28719
game_duration_seconds: 1282.18774414
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11454
score_details {
  idle_production_time: 7297.0
  idle_worker_time: 9.375
  total_value_units: 6550.0
  total_value_structures: 1700.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 8940.0
  collected_vespene: 1764.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 694.0
  spent_minerals: 8625.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2375.0
    economy: 5550.0
    technology: 1000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2075.0
    economy: 5400.0
    technology: 1150.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 300.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 647.467285156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 614.110351562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 365
vespene: 489
food_cap: 106
food_used: 102
food_army: 55
food_workers: 47
idle_worker_count: 0
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 25
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 4
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 24
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 138
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21694
score_details {
  idle_production_time: 30013.9375
  idle_worker_time: 19.0625
  total_value_units: 24250.0
  total_value_structures: 3300.0
  killed_value_units: 12250.0
  killed_value_structures: 0.0
  collected_minerals: 24060.0
  collected_vespene: 8684.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 1299.0
  spent_minerals: 24000.0
  spent_vespene: 6950.0
  food_used {
    none: 0.0
    army: 124.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8300.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8525.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5800.0
    economy: 7950.0
    technology: 1600.0
    upgrade: 825.0
  }
  used_vespene {
    none: 0.0
    army: 2200.0
    economy: 0.0
    technology: 650.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 0.0
    army: 13425.0
    economy: 9000.0
    technology: 1950.0
    upgrade: 825.0
  }
  total_used_vespene {
    none: 0.0
    army: 5125.0
    economy: 0.0
    technology: 900.0
    upgrade: 825.0
  }
  total_damage_dealt {
    life: 17719.2773438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14225.7509766
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 110
vespene: 1734
food_cap: 196
food_used: 187
food_army: 124
food_workers: 59
idle_worker_count: 0
army_count: 48
warp_gate_count: 0
larva_count: 17

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 56
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 4
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10814
score_details {
  idle_production_time: 47404.3125
  idle_worker_time: 1485.0625
  total_value_units: 41450.0
  total_value_structures: 3650.0
  killed_value_units: 25600.0
  killed_value_structures: 125.0
  collected_minerals: 36220.0
  collected_vespene: 13744.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 313.0
  spent_minerals: 35600.0
  spent_vespene: 12350.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 17075.0
    economy: 1650.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 23875.0
    economy: 4550.0
    technology: 1300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 10275.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 4650.0
    technology: 750.0
    upgrade: 1325.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 550.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 0.0
    army: 24225.0
    economy: 10500.0
    technology: 1950.0
    upgrade: 1325.0
  }
  total_used_vespene {
    none: 0.0
    army: 10525.0
    economy: 0.0
    technology: 900.0
    upgrade: 1325.0
  }
  total_damage_dealt {
    life: 33123.453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 52996.6679688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 670
vespene: 1394
food_cap: 162
food_used: 25
food_army: 7
food_workers: 18
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  28719
ui_data{
 multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  10814
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
