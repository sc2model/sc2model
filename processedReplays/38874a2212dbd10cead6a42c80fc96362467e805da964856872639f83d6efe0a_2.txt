----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3055
  player_apm: 123
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 89
}
game_duration_loops: 6907
game_duration_seconds: 308.369720459
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3770
score_details {
  idle_production_time: 397.3125
  idle_worker_time: 129.1875
  total_value_units: 1950.0
  total_value_structures: 1150.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 3470.0
  collected_vespene: 0.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 0.0
  spent_minerals: 2250.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 1450.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 1450.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 321.0
    shields: 566.625
    energy: 0.0
  }
  total_damage_taken {
    life: 675.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1270
vespene: 0
food_cap: 39
food_used: 27
food_army: 12
food_workers: 15
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  6907
ui_data{
 
Score:  3770
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
