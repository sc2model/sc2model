----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 96
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3403
  player_apm: 118
}
game_duration_loops: 23602
game_duration_seconds: 1053.73425293
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9229
score_details {
  idle_production_time: 5563.625
  idle_worker_time: 11.375
  total_value_units: 5700.0
  total_value_structures: 1500.0
  killed_value_units: 950.0
  killed_value_structures: 200.0
  collected_minerals: 7785.0
  collected_vespene: 1456.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 447.0
  spent_minerals: 6587.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 150.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 475.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4350.0
    technology: 850.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 100.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1975.0
    economy: 4850.0
    technology: 1000.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1581.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1114.92431641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1248
vespene: 281
food_cap: 74
food_used: 73
food_army: 31
food_workers: 42
idle_worker_count: 0
army_count: 14
warp_gate_count: 0
larva_count: 16

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 25946
score_details {
  idle_production_time: 19858.0625
  idle_worker_time: 607.75
  total_value_units: 19850.0
  total_value_structures: 2775.0
  killed_value_units: 7575.0
  killed_value_structures: 850.0
  collected_minerals: 22420.0
  collected_vespene: 7788.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 1321.0
  spent_minerals: 20012.0
  spent_vespene: 5475.0
  food_used {
    none: 0.0
    army: 139.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5200.0
    economy: 1175.0
    technology: 175.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3500.0
    economy: 262.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6450.0
    economy: 7450.0
    technology: 2125.0
    upgrade: 1125.0
  }
  used_vespene {
    none: 0.0
    army: 2450.0
    economy: 0.0
    technology: 450.0
    upgrade: 1125.0
  }
  total_used_minerals {
    none: 0.0
    army: 10350.0
    economy: 9000.0
    technology: 2475.0
    upgrade: 1125.0
  }
  total_used_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 700.0
    upgrade: 1125.0
  }
  total_damage_dealt {
    life: 12748.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7713.38427734
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2458
vespene: 2313
food_cap: 200
food_used: 200
food_army: 139
food_workers: 61
idle_worker_count: 7
army_count: 68
warp_gate_count: 0
larva_count: 16

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 29085
score_details {
  idle_production_time: 25698.875
  idle_worker_time: 1233.1875
  total_value_units: 21950.0
  total_value_structures: 3375.0
  killed_value_units: 14525.0
  killed_value_structures: 2725.0
  collected_minerals: 27695.0
  collected_vespene: 10552.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 1007.0
  spent_minerals: 24687.0
  spent_vespene: 7600.0
  food_used {
    none: 0.0
    army: 136.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9550.0
    economy: 4300.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2475.0
    economy: 150.0
    technology: 175.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6975.0
    economy: 262.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6375.0
    economy: 8125.0
    technology: 2125.0
    upgrade: 1725.0
  }
  used_vespene {
    none: 0.0
    army: 2475.0
    economy: 75.0
    technology: 450.0
    upgrade: 1725.0
  }
  total_used_minerals {
    none: 0.0
    army: 11825.0
    economy: 9850.0
    technology: 2475.0
    upgrade: 1225.0
  }
  total_used_vespene {
    none: 0.0
    army: 5125.0
    economy: 150.0
    technology: 700.0
    upgrade: 1225.0
  }
  total_damage_dealt {
    life: 24339.9375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14740.9619141
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3058
vespene: 2952
food_cap: 200
food_used: 195
food_army: 136
food_workers: 59
idle_worker_count: 0
army_count: 43
warp_gate_count: 0

game_loop:  23602
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 893
  count: 11
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 129
    player_relative: 1
    health: 198
    shields: 0
    energy: 200
  }
}

Score:  29085
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
