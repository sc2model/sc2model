----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4611
  player_apm: 218
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4665
  player_apm: 204
}
game_duration_loops: 44417
game_duration_seconds: 1983.04016113
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14126
score_details {
  idle_production_time: 6458.5625
  idle_worker_time: 2.8125
  total_value_units: 7950.0
  total_value_structures: 2225.0
  killed_value_units: 850.0
  killed_value_structures: 100.0
  collected_minerals: 11220.0
  collected_vespene: 2156.0
  collection_rate_minerals: 2435.0
  collection_rate_vespene: 963.0
  spent_minerals: 10850.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 100.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 275.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2550.0
    economy: 6650.0
    technology: 1625.0
    upgrade: 725.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 350.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 7150.0
    technology: 1525.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 1466.34863281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 868.71484375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 420
vespene: 781
food_cap: 120
food_used: 108
food_army: 47
food_workers: 61
idle_worker_count: 0
army_count: 42
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 51
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 141
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27883
score_details {
  idle_production_time: 48107.5625
  idle_worker_time: 1482.9375
  total_value_units: 25400.0
  total_value_structures: 4200.0
  killed_value_units: 8300.0
  killed_value_structures: 600.0
  collected_minerals: 30115.0
  collected_vespene: 8928.0
  collection_rate_minerals: 2939.0
  collection_rate_vespene: 1388.0
  spent_minerals: 28255.0
  spent_vespene: 8205.0
  food_used {
    none: 0.0
    army: 105.5
    economy: 83.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4975.0
    economy: 2550.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6305.0
    economy: 2725.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1930.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6550.0
    economy: 9150.0
    technology: 2075.0
    upgrade: 1675.0
  }
  used_vespene {
    none: 0.0
    army: 3375.0
    economy: 0.0
    technology: 750.0
    upgrade: 1675.0
  }
  total_used_minerals {
    none: 0.0
    army: 15350.0
    economy: 13375.0
    technology: 2525.0
    upgrade: 975.0
  }
  total_used_vespene {
    none: 0.0
    army: 6800.0
    economy: 0.0
    technology: 1150.0
    upgrade: 975.0
  }
  total_damage_dealt {
    life: 13472.9248047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19140.8710938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1910
vespene: 723
food_cap: 196
food_used: 188
food_army: 105
food_workers: 83
idle_worker_count: 0
army_count: 106
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 91
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 6
}
multi {
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 28009
score_details {
  idle_production_time: 87602.1875
  idle_worker_time: 3826.6875
  total_value_units: 38800.0
  total_value_structures: 5500.0
  killed_value_units: 24200.0
  killed_value_structures: 1400.0
  collected_minerals: 41325.0
  collected_vespene: 13644.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 649.0
  spent_minerals: 40505.0
  spent_vespene: 13155.0
  food_used {
    none: 0.0
    army: 110.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 17175.0
    economy: 3950.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4125.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13555.0
    economy: 6600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5630.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6200.0
    economy: 9425.0
    technology: 2275.0
    upgrade: 2025.0
  }
  used_vespene {
    none: 0.0
    army: 3750.0
    economy: 0.0
    technology: 950.0
    upgrade: 2025.0
  }
  total_used_minerals {
    none: 0.0
    army: 23600.0
    economy: 17225.0
    technology: 2725.0
    upgrade: 2025.0
  }
  total_used_vespene {
    none: 0.0
    army: 12100.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2025.0
  }
  total_damage_dealt {
    life: 30744.7363281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 39701.8164062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 870
vespene: 489
food_cap: 200
food_used: 167
food_army: 110
food_workers: 57
idle_worker_count: 1
army_count: 35
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 33
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 6
}
multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 74
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 110
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 153
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 65
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 405
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 193
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 193
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 68
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 274
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 153
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 73
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 350
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 197
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 30142
score_details {
  idle_production_time: 116755.75
  idle_worker_time: 6984.9375
  total_value_units: 46700.0
  total_value_structures: 6550.0
  killed_value_units: 37575.0
  killed_value_structures: 3350.0
  collected_minerals: 49115.0
  collected_vespene: 17012.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 537.0
  spent_minerals: 48830.0
  spent_vespene: 16630.0
  food_used {
    none: 0.0
    army: 127.5
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 24925.0
    economy: 7125.0
    technology: 650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7900.0
    economy: 150.0
    technology: 175.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16680.0
    economy: 10300.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7280.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7725.0
    economy: 8650.0
    technology: 2250.0
    upgrade: 2350.0
  }
  used_vespene {
    none: 0.0
    army: 5250.0
    economy: 0.0
    technology: 850.0
    upgrade: 2350.0
  }
  total_used_minerals {
    none: 0.0
    army: 30200.0
    economy: 20500.0
    technology: 2850.0
    upgrade: 2350.0
  }
  total_used_vespene {
    none: 0.0
    army: 16800.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2350.0
  }
  total_damage_dealt {
    life: 51560.6171875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 61228.2890625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 335
vespene: 382
food_cap: 200
food_used: 176
food_army: 127
food_workers: 43
idle_worker_count: 11
army_count: 49
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 499
  count: 24
}
groups {
  control_group_index: 2
  leader_unit_type: 127
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 105
  count: 18
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 35386
score_details {
  idle_production_time: 132874.125
  idle_worker_time: 9760.75
  total_value_units: 50800.0
  total_value_structures: 7000.0
  killed_value_units: 43175.0
  killed_value_structures: 5300.0
  collected_minerals: 52875.0
  collected_vespene: 19396.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 828.0
  spent_minerals: 51680.0
  spent_vespene: 17730.0
  food_used {
    none: 0.0
    army: 143.5
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 28675.0
    economy: 9825.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8900.0
    economy: 150.0
    technology: 175.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 17680.0
    economy: 10500.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7280.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 9075.0
    economy: 9000.0
    technology: 2500.0
    upgrade: 2350.0
  }
  used_vespene {
    none: 0.0
    army: 6350.0
    economy: 0.0
    technology: 850.0
    upgrade: 2350.0
  }
  total_used_minerals {
    none: 0.0
    army: 32550.0
    economy: 21500.0
    technology: 3100.0
    upgrade: 2350.0
  }
  total_used_vespene {
    none: 0.0
    army: 17900.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2350.0
  }
  total_damage_dealt {
    life: 63604.1367188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 67875.265625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1245
vespene: 1666
food_cap: 200
food_used: 188
food_army: 143
food_workers: 45
idle_worker_count: 10
army_count: 48
warp_gate_count: 0

game_loop:  44417
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 499
  count: 34
}
groups {
  control_group_index: 2
  leader_unit_type: 127
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 105
  count: 8
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  35386
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
