----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3296
  player_apm: 95
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3379
  player_apm: 125
}
game_duration_loops: 8579
game_duration_seconds: 383.017791748
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5462
score_details {
  idle_production_time: 1643.3125
  idle_worker_time: 821.375
  total_value_units: 3000.0
  total_value_structures: 2750.0
  killed_value_units: 375.0
  killed_value_structures: 75.0
  collected_minerals: 5140.0
  collected_vespene: 1072.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 358.0
  spent_minerals: 4500.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 325.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 1075.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 550.0
    economy: 2450.0
    technology: 1075.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 1325.0
    economy: 2900.0
    technology: 1075.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 425.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 679.0
    shields: 1088.625
    energy: 0.0
  }
  total_damage_taken {
    life: 4691.30175781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 104.534179688
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 690
vespene: 97
food_cap: 46
food_used: 34
food_army: 11
food_workers: 23
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  8579
ui_data{
 single {
  unit {
    unit_type: 36
    player_relative: 1
    health: 1037
    shields: 0
    energy: 0
  }
}

Score:  5462
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
