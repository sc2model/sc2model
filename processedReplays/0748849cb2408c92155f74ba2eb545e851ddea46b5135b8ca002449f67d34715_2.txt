----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 97
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3265
  player_apm: 88
}
game_duration_loops: 16481
game_duration_seconds: 735.810302734
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6707
score_details {
  idle_production_time: 952.9375
  idle_worker_time: 402.1875
  total_value_units: 5600.0
  total_value_structures: 2450.0
  killed_value_units: 2425.0
  killed_value_structures: 625.0
  collected_minerals: 6700.0
  collected_vespene: 1432.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 335.0
  spent_minerals: 6675.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1575.0
    economy: 550.0
    technology: 275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1375.0
    economy: 400.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1600.0
    economy: 3150.0
    technology: 750.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2750.0
    economy: 3300.0
    technology: 900.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 6222.61132812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2292.91259766
    shields: 3294.43334961
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 75
vespene: 32
food_cap: 70
food_used: 60
food_army: 26
food_workers: 33
idle_worker_count: 1
army_count: 12
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 7085
score_details {
  idle_production_time: 2172.875
  idle_worker_time: 852.375
  total_value_units: 7700.0
  total_value_structures: 4700.0
  killed_value_units: 4300.0
  killed_value_structures: 3550.0
  collected_minerals: 11580.0
  collected_vespene: 1692.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 0.0
  spent_minerals: 11237.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2725.0
    economy: 1900.0
    technology: 1950.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2350.0
    economy: 2700.0
    technology: 787.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 3400.0
    technology: 900.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 3800.0
    economy: 5300.0
    technology: 1800.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 21682.96875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9581.91210938
    shields: 11771.5585938
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 393
vespene: 42
food_cap: 62
food_used: 55
food_army: 24
food_workers: 29
idle_worker_count: 0
army_count: 12
warp_gate_count: 5

game_loop:  16481
ui_data{
 single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  7085
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
