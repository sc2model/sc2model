----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3223
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3159
  player_apm: 99
}
game_duration_loops: 11525
game_duration_seconds: 514.54486084
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10647
score_details {
  idle_production_time: 1506.5625
  idle_worker_time: 44.0
  total_value_units: 4500.0
  total_value_structures: 3750.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 8015.0
  collected_vespene: 2156.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 492.0
  spent_minerals: 7250.0
  spent_vespene: 1800.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 3800.0
    technology: 1550.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 500.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 4100.0
    technology: 1550.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 500.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 1563.95898438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 840.918457031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 331.770996094
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 778
vespene: 319
food_cap: 78
food_used: 69
food_army: 33
food_workers: 36
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
cargo {
  unit {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 132
    transport_slots_taken: 5
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  slots_available: 3
}

score{
 score_type: Melee
score: 10505
score_details {
  idle_production_time: 2189.25
  idle_worker_time: 337.25
  total_value_units: 5425.0
  total_value_structures: 3750.0
  killed_value_units: 3925.0
  killed_value_structures: 0.0
  collected_minerals: 9105.0
  collected_vespene: 2580.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 335.0
  spent_minerals: 8450.0
  spent_vespene: 1925.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2350.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 400.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 3550.0
    technology: 1450.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 425.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 4200.0
    technology: 1550.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 500.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 5371.69628906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5607.84814453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2607.43017578
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 612
vespene: 618
food_cap: 70
food_used: 69
food_army: 36
food_workers: 32
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  11525
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

Score:  10505
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
