----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3101
  player_apm: 39
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3078
  player_apm: 95
}
game_duration_loops: 8648
game_duration_seconds: 386.098358154
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7497
score_details {
  idle_production_time: 2654.1875
  idle_worker_time: 4.0
  total_value_units: 3150.0
  total_value_structures: 1100.0
  killed_value_units: 1600.0
  killed_value_structures: 300.0
  collected_minerals: 4685.0
  collected_vespene: 2012.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 649.0
  spent_minerals: 4100.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 650.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1300.0
    economy: 3000.0
    technology: 550.0
    upgrade: 0.0
  }
  used_vespene {
    none: 150.0
    army: 900.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 300.0
    army: 1250.0
    economy: 3250.0
    technology: 700.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 300.0
    army: 1350.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3093.16235352
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 496.922851562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 635
vespene: 612
food_cap: 52
food_used: 52
food_army: 24
food_workers: 28
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  8648
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 503
  count: 4
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  7497
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
