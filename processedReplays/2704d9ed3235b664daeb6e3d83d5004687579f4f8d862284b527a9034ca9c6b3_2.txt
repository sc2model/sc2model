----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4788
  player_apm: 209
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4478
  player_apm: 215
}
game_duration_loops: 23081
game_duration_seconds: 1030.47363281
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13567
score_details {
  idle_production_time: 5497.5625
  idle_worker_time: 0.8125
  total_value_units: 7550.0
  total_value_structures: 2400.0
  killed_value_units: 350.0
  killed_value_structures: 400.0
  collected_minerals: 11455.0
  collected_vespene: 1668.0
  collection_rate_minerals: 2575.0
  collection_rate_vespene: 873.0
  spent_minerals: 10506.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 331.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 7000.0
    technology: 1550.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 400.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 8200.0
    technology: 1550.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 400.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 977.0
    shields: 1231.0
    energy: 0.0
  }
  total_damage_taken {
    life: 942.036621094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 999
vespene: 218
food_cap: 160
food_used: 99
food_army: 32
food_workers: 67
idle_worker_count: 0
army_count: 4
warp_gate_count: 0
larva_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17384
score_details {
  idle_production_time: 30799.4375
  idle_worker_time: 1241.4375
  total_value_units: 27800.0
  total_value_structures: 4300.0
  killed_value_units: 15250.0
  killed_value_structures: 900.0
  collected_minerals: 29144.0
  collected_vespene: 8096.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 537.0
  spent_minerals: 29006.0
  spent_vespene: 7525.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9200.0
    economy: 3100.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3550.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13250.0
    economy: 3506.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2550.0
    economy: 7200.0
    technology: 2075.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 750.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 0.0
    army: 14700.0
    economy: 12175.0
    technology: 2425.0
    upgrade: 1125.0
  }
  total_used_vespene {
    none: 0.0
    army: 4800.0
    economy: 0.0
    technology: 1000.0
    upgrade: 1125.0
  }
  total_damage_dealt {
    life: 10187.125
    shields: 12354.75
    energy: 0.0
  }
  total_damage_taken {
    life: 28167.8671875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 188
vespene: 571
food_cap: 164
food_used: 100
food_army: 46
food_workers: 54
idle_worker_count: 3
army_count: 15
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 101
  count: 6
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20235
score_details {
  idle_production_time: 41977.75
  idle_worker_time: 1537.75
  total_value_units: 33600.0
  total_value_structures: 4375.0
  killed_value_units: 25550.0
  killed_value_structures: 3550.0
  collected_minerals: 33441.0
  collected_vespene: 9300.0
  collection_rate_minerals: 1730.0
  collection_rate_vespene: 671.0
  spent_minerals: 32756.0
  spent_vespene: 8375.0
  food_used {
    none: 0.0
    army: 86.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15025.0
    economy: 6600.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6575.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14700.0
    economy: 4206.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4550.0
    economy: 6700.0
    technology: 2175.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 900.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 0.0
    army: 18800.0
    economy: 12375.0
    technology: 2550.0
    upgrade: 1275.0
  }
  total_used_vespene {
    none: 0.0
    army: 6300.0
    economy: 0.0
    technology: 1000.0
    upgrade: 1275.0
  }
  total_damage_dealt {
    life: 21504.9335938
    shields: 26487.8007812
    energy: 0.0
  }
  total_damage_taken {
    life: 33254.6484375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 735
vespene: 925
food_cap: 142
food_used: 141
food_army: 86
food_workers: 55
idle_worker_count: 0
army_count: 66
warp_gate_count: 0

game_loop:  23081
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 101
  count: 5
}

Score:  20235
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
