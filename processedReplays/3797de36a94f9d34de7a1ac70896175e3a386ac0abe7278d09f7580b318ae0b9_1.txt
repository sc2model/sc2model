----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4975
  player_apm: 253
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5331
  player_apm: 278
}
game_duration_loops: 10725
game_duration_seconds: 478.828063965
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7751
score_details {
  idle_production_time: 1212.5
  idle_worker_time: 334.6875
  total_value_units: 4600.0
  total_value_structures: 3125.0
  killed_value_units: 2075.0
  killed_value_structures: 0.0
  collected_minerals: 7715.0
  collected_vespene: 1784.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 313.0
  spent_minerals: 7625.0
  spent_vespene: 1075.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1825.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1023.0
    economy: 1625.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1550.0
    economy: 2650.0
    technology: 1150.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 200.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2200.0
    economy: 4250.0
    technology: 1150.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 200.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 3355.89453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5680.44042969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 391.061523438
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 117
vespene: 709
food_cap: 54
food_used: 53
food_army: 31
food_workers: 22
idle_worker_count: 2
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 29
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 155
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 94
    shields: 0
    energy: 109
    transport_slots_taken: 7
  }
}

score{
 score_type: Melee
score: 6750
score_details {
  idle_production_time: 1485.25
  idle_worker_time: 458.8125
  total_value_units: 4950.0
  total_value_structures: 3125.0
  killed_value_units: 3850.0
  killed_value_structures: 0.0
  collected_minerals: 8070.0
  collected_vespene: 1952.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 313.0
  spent_minerals: 7925.0
  spent_vespene: 1075.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3225.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2273.0
    economy: 1875.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 650.0
    economy: 2400.0
    technology: 1150.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 200.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2550.0
    economy: 4250.0
    technology: 1150.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 200.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 4590.61132812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7988.6953125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 961.484375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 148
vespene: 877
food_cap: 46
food_used: 32
food_army: 13
food_workers: 19
idle_worker_count: 19
army_count: 4
warp_gate_count: 0

game_loop:  10725
ui_data{
 
Score:  6750
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
