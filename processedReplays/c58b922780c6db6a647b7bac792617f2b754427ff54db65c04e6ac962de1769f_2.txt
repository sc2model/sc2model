----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2940
  player_apm: 78
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 124
}
game_duration_loops: 16690
game_duration_seconds: 745.141296387
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13908
score_details {
  idle_production_time: 1294.0
  idle_worker_time: 34.1875
  total_value_units: 6450.0
  total_value_structures: 4550.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 10335.0
  collected_vespene: 2748.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 627.0
  spent_minerals: 9725.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2500.0
    economy: 5300.0
    technology: 2450.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 4950.0
    technology: 2150.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 490.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 148.0
    shields: 306.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 660
vespene: 923
food_cap: 118
food_used: 95
food_army: 41
food_workers: 54
idle_worker_count: 0
army_count: 18
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 26030
score_details {
  idle_production_time: 5309.375
  idle_worker_time: 355.875
  total_value_units: 13675.0
  total_value_structures: 8300.0
  killed_value_units: 4350.0
  killed_value_structures: 3650.0
  collected_minerals: 20065.0
  collected_vespene: 7940.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 1343.0
  spent_minerals: 16875.0
  spent_vespene: 4900.0
  food_used {
    none: 0.0
    army: 67.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2550.0
    economy: 2175.0
    technology: 1625.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2125.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4200.0
    economy: 7150.0
    technology: 3950.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 6325.0
    economy: 7200.0
    technology: 3950.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 4150.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 20963.2050781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1419.0
    shields: 2154.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3240
vespene: 3040
food_cap: 200
food_used: 130
food_army: 67
food_workers: 63
idle_worker_count: 1
army_count: 32
warp_gate_count: 10

game_loop:  16690
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 16
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 10
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
}

Score:  26030
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
