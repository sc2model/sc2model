----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4427
  player_apm: 194
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4286
  player_apm: 239
}
game_duration_loops: 46271
game_duration_seconds: 2065.8137207
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10488
score_details {
  idle_production_time: 797.8125
  idle_worker_time: 340.375
  total_value_units: 5400.0
  total_value_structures: 3175.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 9120.0
  collected_vespene: 1668.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 313.0
  spent_minerals: 9150.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 4600.0
    technology: 1650.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 475.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 5150.0
    technology: 1050.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 375.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2066.99243164
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1407.29736328
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 746.157714844
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 20
vespene: 143
food_cap: 94
food_used: 87
food_army: 44
food_workers: 43
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
single {
  unit {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 16820
score_details {
  idle_production_time: 5491.625
  idle_worker_time: 1614.3125
  total_value_units: 16650.0
  total_value_structures: 5975.0
  killed_value_units: 9050.0
  killed_value_structures: 475.0
  collected_minerals: 21570.0
  collected_vespene: 4668.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 223.0
  spent_minerals: 21212.0
  spent_vespene: 3481.0
  food_used {
    none: 0.0
    army: 81.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7150.0
    economy: 1200.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6200.0
    economy: 3100.0
    technology: 262.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 31.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4000.0
    economy: 4850.0
    technology: 2150.0
    upgrade: 1150.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 575.0
    upgrade: 1150.0
  }
  total_used_minerals {
    none: 0.0
    army: 10400.0
    economy: 8400.0
    technology: 2450.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 625.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 16916.5039062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14734.1376953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 6897.25195312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 408
vespene: 1187
food_cap: 149
food_used: 115
food_army: 81
food_workers: 34
idle_worker_count: 1
army_count: 59
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 49
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 9
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 89
    shields: 0
    energy: 176
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 102
    shields: 0
    energy: 104
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 35
    shields: 0
    energy: 84
  }
}

score{
 score_type: Melee
score: 27021
score_details {
  idle_production_time: 10442.875
  idle_worker_time: 3167.5625
  total_value_units: 31350.0
  total_value_structures: 8075.0
  killed_value_units: 13400.0
  killed_value_structures: 700.0
  collected_minerals: 37825.0
  collected_vespene: 7564.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 918.0
  spent_minerals: 35912.0
  spent_vespene: 6906.0
  food_used {
    none: 0.0
    army: 142.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9925.0
    economy: 2225.0
    technology: 275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13425.0
    economy: 3100.0
    technology: 262.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 31.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7775.0
    economy: 7650.0
    technology: 2450.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 3000.0
    economy: 0.0
    technology: 725.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 21350.0
    economy: 11200.0
    technology: 2750.0
    upgrade: 1400.0
  }
  total_used_vespene {
    none: 0.0
    army: 4250.0
    economy: 0.0
    technology: 775.0
    upgrade: 1400.0
  }
  total_damage_dealt {
    life: 25354.5527344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23064.6152344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 10101.2617188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1963
vespene: 658
food_cap: 200
food_used: 199
food_army: 142
food_workers: 57
idle_worker_count: 6
army_count: 85
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 65
}
groups {
  control_group_index: 3
  leader_unit_type: 50
  count: 9
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 9
}
multi {
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 114
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 136
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 45
    shields: 0
    energy: 200
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 73
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 33
    shields: 0
    energy: 200
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 31
    shields: 0
    energy: 181
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 117
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 97
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 200
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 136
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 171
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 164
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 139
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19454
score_details {
  idle_production_time: 16296.0
  idle_worker_time: 6218.25
  total_value_units: 38850.0
  total_value_structures: 9175.0
  killed_value_units: 26500.0
  killed_value_structures: 5425.0
  collected_minerals: 44155.0
  collected_vespene: 10880.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 42812.0
  spent_vespene: 9256.0
  food_used {
    none: 0.0
    army: 106.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 17750.0
    economy: 5800.0
    technology: 2375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5200.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 20050.0
    economy: 8600.0
    technology: 1162.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4350.0
    economy: 0.0
    technology: 181.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1300.0
    economy: 50.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5400.0
    economy: 3350.0
    technology: 2150.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 2250.0
    economy: 0.0
    technology: 675.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 26350.0
    economy: 12050.0
    technology: 3350.0
    upgrade: 1400.0
  }
  total_used_vespene {
    none: 0.0
    army: 6600.0
    economy: 0.0
    technology: 875.0
    upgrade: 1400.0
  }
  total_damage_dealt {
    life: 60181.9765625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 49879.9140625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 16663.640625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1299
vespene: 1530
food_cap: 149
food_used: 110
food_army: 106
food_workers: 4
idle_worker_count: 1
army_count: 57
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 500
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 50
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 134
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 7
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12483
score_details {
  idle_production_time: 20626.375
  idle_worker_time: 6592.8125
  total_value_units: 42925.0
  total_value_structures: 9275.0
  killed_value_units: 33375.0
  killed_value_structures: 5500.0
  collected_minerals: 46470.0
  collected_vespene: 10880.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 0.0
  spent_minerals: 46212.0
  spent_vespene: 9631.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 23025.0
    economy: 5875.0
    technology: 2375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6800.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 26419.0
    economy: 9250.0
    technology: 1462.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6469.0
    economy: 0.0
    technology: 181.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1500.0
    economy: 50.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1075.0
    economy: 4050.0
    technology: 2050.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 675.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 28550.0
    economy: 13700.0
    technology: 3450.0
    upgrade: 1400.0
  }
  total_used_vespene {
    none: 0.0
    army: 7125.0
    economy: 0.0
    technology: 875.0
    upgrade: 1400.0
  }
  total_damage_dealt {
    life: 68518.9765625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 60565.6601562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 19444.3359375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 128
vespene: 1155
food_cap: 149
food_used: 40
food_army: 22
food_workers: 18
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  46271
ui_data{
 multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  12483
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
