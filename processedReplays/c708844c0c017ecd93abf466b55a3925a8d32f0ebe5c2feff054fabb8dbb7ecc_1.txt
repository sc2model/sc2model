----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5063
  player_apm: 246
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5317
  player_apm: 179
}
game_duration_loops: 11403
game_duration_seconds: 509.098022461
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10341
score_details {
  idle_production_time: 1306.4375
  idle_worker_time: 333.625
  total_value_units: 5650.0
  total_value_structures: 3925.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 8790.0
  collected_vespene: 1952.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 291.0
  spent_minerals: 8650.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 175.0
    economy: 900.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 75.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2575.0
    economy: 3825.0
    technology: 1625.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 275.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 50.0
    army: 2350.0
    economy: 4925.0
    technology: 1675.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 325.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 120.0
    shields: 368.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2315.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2.18041992188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 189
vespene: 202
food_cap: 102
food_used: 85
food_army: 51
food_workers: 32
idle_worker_count: 3
army_count: 28
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 22
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 101
    transport_slots_taken: 1
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 97
    transport_slots_taken: 2
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9709
score_details {
  idle_production_time: 2002.75
  idle_worker_time: 683.375
  total_value_units: 6675.0
  total_value_structures: 3925.0
  killed_value_units: 2400.0
  killed_value_structures: 0.0
  collected_minerals: 10350.0
  collected_vespene: 2260.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 358.0
  spent_minerals: 9600.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1575.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 1925.0
    economy: 900.0
    technology: 12.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 700.0
    economy: 0.0
    technology: 12.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 925.0
    economy: 4225.0
    technology: 1925.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 275.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 50.0
    army: 2750.0
    economy: 5325.0
    technology: 1675.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 50.0
    army: 1025.0
    economy: 0.0
    technology: 325.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 1064.0
    shields: 1463.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3935.88330078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 303.214111328
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 799
vespene: 410
food_cap: 102
food_used: 60
food_army: 18
food_workers: 40
idle_worker_count: 42
army_count: 10
warp_gate_count: 0

game_loop:  11403
ui_data{
 
Score:  9709
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
