----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2620
  player_apm: 53
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 69
}
game_duration_loops: 11961
game_duration_seconds: 534.010498047
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7570
score_details {
  idle_production_time: 1334.4375
  idle_worker_time: 234.5625
  total_value_units: 4200.0
  total_value_structures: 2700.0
  killed_value_units: 1875.0
  killed_value_structures: 500.0
  collected_minerals: 7415.0
  collected_vespene: 1780.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 313.0
  spent_minerals: 7125.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 250.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1050.0
    economy: 3250.0
    technology: 1025.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 3000.0
    technology: 1025.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 1674.29345703
    shields: 2182.70654297
    energy: 0.0
  }
  total_damage_taken {
    life: 2463.34667969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 338.594726562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 340
vespene: 630
food_cap: 79
food_used: 48
food_army: 21
food_workers: 27
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7525
score_details {
  idle_production_time: 1923.8125
  idle_worker_time: 357.5
  total_value_units: 5900.0
  total_value_structures: 3100.0
  killed_value_units: 2925.0
  killed_value_structures: 600.0
  collected_minerals: 8830.0
  collected_vespene: 2220.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 268.0
  spent_minerals: 8575.0
  spent_vespene: 1600.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1950.0
    economy: 350.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3450.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 900.0
    economy: 3250.0
    technology: 1025.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 4050.0
    economy: 3400.0
    technology: 1025.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 2344.29345703
    shields: 3038.83154297
    energy: 0.0
  }
  total_damage_taken {
    life: 4336.34667969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 338.594726562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 305
vespene: 620
food_cap: 94
food_used: 45
food_army: 18
food_workers: 27
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  11961
ui_data{
 
Score:  7525
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
