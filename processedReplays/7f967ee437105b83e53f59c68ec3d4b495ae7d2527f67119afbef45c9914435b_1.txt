----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4136
  player_apm: 93
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4140
  player_apm: 170
}
game_duration_loops: 6647
game_duration_seconds: 296.761779785
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3163
score_details {
  idle_production_time: 378.5625
  idle_worker_time: 1142.625
  total_value_units: 2300.0
  total_value_structures: 1400.0
  killed_value_units: 275.0
  killed_value_structures: 200.0
  collected_minerals: 3215.0
  collected_vespene: 972.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 335.0
  spent_minerals: 2424.0
  spent_vespene: 450.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 475.0
    economy: 150.0
    technology: -226.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1400.0
    technology: 300.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 700.0
    economy: 2150.0
    technology: 450.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 655.0
    shields: 906.875
    energy: 0.0
  }
  total_damage_taken {
    life: 600.0
    shields: 936.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 841
vespene: 522
food_cap: 23
food_used: 15
food_army: 0
food_workers: 15
idle_worker_count: 15
army_count: 0
warp_gate_count: 0

game_loop:  6647
ui_data{
 
Score:  3163
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
