----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4201
  player_apm: 132
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4206
  player_apm: 281
}
game_duration_loops: 19619
game_duration_seconds: 875.909362793
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12332
score_details {
  idle_production_time: 1280.8125
  idle_worker_time: 189.8125
  total_value_units: 5875.0
  total_value_structures: 4425.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 9470.0
  collected_vespene: 2380.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 649.0
  spent_minerals: 9025.0
  spent_vespene: 2125.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2350.0
    economy: 5300.0
    technology: 1625.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 925.0
    economy: 0.0
    technology: 900.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 2500.0
    economy: 5600.0
    technology: 1325.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 975.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1035.03051758
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 471.5
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 62.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 477
vespene: 255
food_cap: 117
food_used: 97
food_army: 46
food_workers: 48
idle_worker_count: 1
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 29
  count: 2
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 21416
score_details {
  idle_production_time: 8308.9375
  idle_worker_time: 1136.0625
  total_value_units: 19000.0
  total_value_structures: 8500.0
  killed_value_units: 15500.0
  killed_value_structures: 5275.0
  collected_minerals: 23585.0
  collected_vespene: 7552.0
  collection_rate_minerals: 2295.0
  collection_rate_vespene: 649.0
  spent_minerals: 22950.0
  spent_vespene: 6725.0
  food_used {
    none: 0.0
    army: 120.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8800.0
    economy: 4950.0
    technology: 2875.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3250.0
    economy: 0.0
    technology: 900.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3650.0
    economy: 3250.0
    technology: 928.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 1250.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 400.0
    army: 6350.0
    economy: 5050.0
    technology: 2700.0
    upgrade: 475.0
  }
  used_vespene {
    none: 100.0
    army: 3225.0
    economy: 0.0
    technology: 1150.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 100.0
    army: 10150.0
    economy: 8275.0
    technology: 3625.0
    upgrade: 475.0
  }
  total_used_vespene {
    none: 100.0
    army: 4900.0
    economy: 0.0
    technology: 1250.0
    upgrade: 475.0
  }
  total_damage_dealt {
    life: 41901.2265625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 17635.2636719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 75.75
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 664
vespene: 827
food_cap: 165
food_used: 157
food_army: 120
food_workers: 34
idle_worker_count: 2
army_count: 42
warp_gate_count: 0

game_loop:  19619
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 484
  count: 25
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 29
  count: 2
}
single {
  unit {
    unit_type: 52
    player_relative: 1
    health: 247
    shields: 0
    energy: 0
  }
}

Score:  21416
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
