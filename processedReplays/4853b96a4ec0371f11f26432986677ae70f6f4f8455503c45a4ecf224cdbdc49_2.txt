----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3838
  player_apm: 145
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3821
  player_apm: 144
}
game_duration_loops: 13218
game_duration_seconds: 590.130493164
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11958
score_details {
  idle_production_time: 1111.5625
  idle_worker_time: 150.3125
  total_value_units: 5550.0
  total_value_structures: 4150.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 9250.0
  collected_vespene: 2508.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 671.0
  spent_minerals: 8875.0
  spent_vespene: 2175.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1525.0
    economy: 5150.0
    technology: 1900.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 1725.0
    economy: 5250.0
    technology: 1450.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 387.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 390.0
    shields: 640.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 425
vespene: 333
food_cap: 109
food_used: 86
food_army: 29
food_workers: 57
idle_worker_count: 1
army_count: 13
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 200
  }
}

score{
 score_type: Melee
score: 11821
score_details {
  idle_production_time: 2202.4375
  idle_worker_time: 1231.5625
  total_value_units: 8925.0
  total_value_structures: 5950.0
  killed_value_units: 1425.0
  killed_value_structures: 0.0
  collected_minerals: 13025.0
  collected_vespene: 3896.0
  collection_rate_minerals: 587.0
  collection_rate_vespene: 335.0
  spent_minerals: 12900.0
  spent_vespene: 3275.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3050.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1175.0
    economy: 4700.0
    technology: 2350.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 400.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 4225.0
    economy: 6150.0
    technology: 2350.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 2250.88720703
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5909.0
    shields: 6308.70507812
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 175
vespene: 621
food_cap: 110
food_used: 80
food_army: 25
food_workers: 55
idle_worker_count: 55
army_count: 11
warp_gate_count: 6

game_loop:  13218
ui_data{
 
Score:  11821
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
