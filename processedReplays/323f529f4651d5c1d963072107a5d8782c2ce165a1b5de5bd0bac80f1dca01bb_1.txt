----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3047
  player_apm: 79
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3035
  player_apm: 51
}
game_duration_loops: 12231
game_duration_seconds: 546.064880371
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11030
score_details {
  idle_production_time: 1518.9375
  idle_worker_time: 19.625
  total_value_units: 5850.0
  total_value_structures: 4200.0
  killed_value_units: 2650.0
  killed_value_structures: 100.0
  collected_minerals: 9540.0
  collected_vespene: 2640.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 627.0
  spent_minerals: 9250.0
  spent_vespene: 2600.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1850.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2350.0
    economy: 4700.0
    technology: 1350.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2100.0
    economy: 5000.0
    technology: 1350.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1530.0
    shields: 1260.0
    energy: 0.0
  }
  total_damage_taken {
    life: 721.0
    shields: 1924.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 340
vespene: 40
food_cap: 101
food_used: 85
food_army: 38
food_workers: 44
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 62
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 2
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 17113
score_details {
  idle_production_time: 2120.25
  idle_worker_time: 133.875
  total_value_units: 8750.0
  total_value_structures: 5300.0
  killed_value_units: 4550.0
  killed_value_structures: 800.0
  collected_minerals: 12945.0
  collected_vespene: 3968.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 1007.0
  spent_minerals: 11425.0
  spent_vespene: 3125.0
  food_used {
    none: 0.0
    army: 63.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2800.0
    economy: 850.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3825.0
    economy: 5750.0
    technology: 1850.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 400.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 3475.0
    economy: 6200.0
    technology: 1850.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 2125.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 3619.16430664
    shields: 4675.26660156
    energy: 0.0
  }
  total_damage_taken {
    life: 895.0
    shields: 2224.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1570
vespene: 843
food_cap: 141
food_used: 121
food_army: 63
food_workers: 58
idle_worker_count: 1
army_count: 15
warp_gate_count: 0

game_loop:  12231
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 62
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 80
  count: 7
}
multi {
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 102
    shields: 39
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 72
    shields: 57
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 102
    shields: 51
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
}

Score:  17113
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
