----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4070
  player_apm: 165
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4133
  player_apm: 177
}
game_duration_loops: 15928
game_duration_seconds: 711.121032715
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10430
score_details {
  idle_production_time: 1161.4375
  idle_worker_time: 201.0
  total_value_units: 5850.0
  total_value_structures: 3250.0
  killed_value_units: 900.0
  killed_value_structures: 100.0
  collected_minerals: 8175.0
  collected_vespene: 2092.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 492.0
  spent_minerals: 7587.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2050.0
    economy: 4450.0
    technology: 1250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 4150.0
    technology: 1250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1409.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1109.625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 12.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 638
vespene: 242
food_cap: 78
food_used: 78
food_army: 39
food_workers: 39
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 692
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 22
  count: 2
}
cargo {
  unit {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 86
    transport_slots_taken: 3
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  slots_available: 5
}

score{
 score_type: Melee
score: 9868
score_details {
  idle_production_time: 4214.0
  idle_worker_time: 422.75
  total_value_units: 13075.0
  total_value_structures: 4875.0
  killed_value_units: 7675.0
  killed_value_structures: 100.0
  collected_minerals: 15665.0
  collected_vespene: 4540.0
  collection_rate_minerals: 643.0
  collection_rate_vespene: 291.0
  spent_minerals: 15537.0
  spent_vespene: 3975.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4600.0
    economy: 1250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6350.0
    economy: 2712.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 2775.0
    technology: 2000.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 575.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 7800.0
    economy: 5950.0
    technology: 2000.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 2525.0
    economy: 0.0
    technology: 575.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 7257.47558594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11667.9667969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 397.149902344
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 178
vespene: 565
food_cap: 102
food_used: 48
food_army: 35
food_workers: 13
idle_worker_count: 13
army_count: 16
warp_gate_count: 0

game_loop:  15928
ui_data{
 
Score:  9868
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
