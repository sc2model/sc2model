----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3086
  player_apm: 68
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3154
  player_apm: 37
}
game_duration_loops: 6811
game_duration_seconds: 304.083709717
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4745
score_details {
  idle_production_time: 6779.6875
  idle_worker_time: 7.25
  total_value_units: 2900.0
  total_value_structures: 825.0
  killed_value_units: 1600.0
  killed_value_structures: 500.0
  collected_minerals: 4570.0
  collected_vespene: 100.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 0.0
  spent_minerals: 3425.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 16.5
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 1900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1025.0
    economy: 2075.0
    technology: 250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 2225.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1960.0
    shields: 2412.375
    energy: 0.0
  }
  total_damage_taken {
    life: 1261.12011719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1195
vespene: 0
food_cap: 44
food_used: 34
food_army: 16
food_workers: 18
idle_worker_count: 0
army_count: 17
warp_gate_count: 0

game_loop:  6811
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}

Score:  4745
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
