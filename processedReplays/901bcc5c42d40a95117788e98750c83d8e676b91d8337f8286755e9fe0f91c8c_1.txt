----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4211
  player_apm: 152
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4235
  player_apm: 207
}
game_duration_loops: 23201
game_duration_seconds: 1035.83117676
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12750
score_details {
  idle_production_time: 1149.5625
  idle_worker_time: 96.9375
  total_value_units: 4800.0
  total_value_structures: 4950.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 9260.0
  collected_vespene: 2640.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 649.0
  spent_minerals: 8950.0
  spent_vespene: 2450.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4700.0
    technology: 2450.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 400.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4600.0
    technology: 2150.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 400.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 774.75
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 120.0
    shields: 205.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 360
vespene: 190
food_cap: 117
food_used: 79
food_army: 33
food_workers: 43
idle_worker_count: 0
army_count: 15
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 75
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
    add_on {
      unit_type: 133
      build_progress: 0.181249976158
    }
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
    add_on {
      unit_type: 133
      build_progress: 0.168749988079
    }
  }
}

score{
 score_type: Melee
score: 17630
score_details {
  idle_production_time: 4526.125
  idle_worker_time: 861.3125
  total_value_units: 15250.0
  total_value_structures: 7600.0
  killed_value_units: 15350.0
  killed_value_structures: 0.0
  collected_minerals: 20490.0
  collected_vespene: 7664.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 335.0
  spent_minerals: 20049.0
  spent_vespene: 5375.0
  food_used {
    none: 0.0
    army: 59.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12050.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5475.0
    economy: 2525.0
    technology: 1024.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2875.0
    economy: 4825.0
    technology: 2700.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 300.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 8950.0
    economy: 7300.0
    technology: 3350.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 0.0
    army: 5100.0
    economy: 0.0
    technology: 400.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 11978.1923828
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12649.1865234
    shields: 15566.9394531
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 491
vespene: 2289
food_cap: 124
food_used: 100
food_army: 59
food_workers: 40
idle_worker_count: 0
army_count: 25
warp_gate_count: 10

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 141
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 12
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 15407
score_details {
  idle_production_time: 5631.8125
  idle_worker_time: 947.9375
  total_value_units: 17100.0
  total_value_structures: 8650.0
  killed_value_units: 19825.0
  killed_value_structures: 3100.0
  collected_minerals: 22310.0
  collected_vespene: 8396.0
  collection_rate_minerals: 475.0
  collection_rate_vespene: 156.0
  spent_minerals: 22099.0
  spent_vespene: 5875.0
  food_used {
    none: 0.0
    army: 56.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 14300.0
    economy: 3500.0
    technology: 1375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 475.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7125.0
    economy: 4475.0
    technology: 1774.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2625.0
    economy: 3375.0
    technology: 2250.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 1975.0
    economy: 0.0
    technology: 300.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 10350.0
    economy: 7850.0
    technology: 3800.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 0.0
    army: 5400.0
    economy: 0.0
    technology: 600.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 32164.3007812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19481.1875
    shields: 24698.1894531
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 261
vespene: 2521
food_cap: 93
food_used: 80
food_army: 56
food_workers: 24
idle_worker_count: 7
army_count: 23
warp_gate_count: 8

game_loop:  23201
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 73
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 75
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 48
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 278
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 140
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 24
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 127
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 127
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 39
    shields: 40
    energy: 145
  }
}

Score:  15407
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
