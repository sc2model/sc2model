----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3237
  player_apm: 95
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3244
  player_apm: 184
}
game_duration_loops: 24207
game_duration_seconds: 1080.74511719
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10960
score_details {
  idle_production_time: 1240.375
  idle_worker_time: 23.9375
  total_value_units: 6675.0
  total_value_structures: 3950.0
  killed_value_units: 175.0
  killed_value_structures: 0.0
  collected_minerals: 8610.0
  collected_vespene: 2000.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 649.0
  spent_minerals: 8175.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3025.0
    economy: 4250.0
    technology: 1150.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2825.0
    economy: 4950.0
    technology: 1150.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 395.050292969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1520.0
    shields: 1585.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 485
vespene: 250
food_cap: 110
food_used: 98
food_army: 55
food_workers: 43
idle_worker_count: 0
army_count: 23
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 200
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 70
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 70
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 169
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 167
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 21060
score_details {
  idle_production_time: 4588.1875
  idle_worker_time: 1739.0
  total_value_units: 18250.0
  total_value_structures: 7300.0
  killed_value_units: 8400.0
  killed_value_structures: 775.0
  collected_minerals: 20755.0
  collected_vespene: 7380.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 627.0
  spent_minerals: 19400.0
  spent_vespene: 6350.0
  food_used {
    none: 0.0
    army: 93.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6725.0
    economy: 400.0
    technology: 425.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5750.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4500.0
    economy: 6100.0
    technology: 3150.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 3475.0
    economy: 0.0
    technology: 700.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 10650.0
    economy: 6450.0
    technology: 3150.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 6400.0
    economy: 0.0
    technology: 700.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 16791.6601562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6405.8359375
    shields: 6891.7890625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1405
vespene: 1030
food_cap: 149
food_used: 148
food_army: 93
food_workers: 55
idle_worker_count: 0
army_count: 39
warp_gate_count: 12

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 12
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 23144
score_details {
  idle_production_time: 6931.5625
  idle_worker_time: 1951.0
  total_value_units: 22425.0
  total_value_structures: 8350.0
  killed_value_units: 20300.0
  killed_value_structures: 1400.0
  collected_minerals: 26500.0
  collected_vespene: 9444.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 783.0
  spent_minerals: 23975.0
  spent_vespene: 8400.0
  food_used {
    none: 0.0
    army: 75.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15225.0
    economy: 1450.0
    technology: 700.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9325.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3950.0
    economy: 6700.0
    technology: 3150.0
    upgrade: 1100.0
  }
  used_vespene {
    none: 0.0
    army: 2825.0
    economy: 0.0
    technology: 700.0
    upgrade: 1100.0
  }
  total_used_minerals {
    none: 0.0
    army: 13875.0
    economy: 7500.0
    technology: 3150.0
    upgrade: 1100.0
  }
  total_used_vespene {
    none: 0.0
    army: 8250.0
    economy: 0.0
    technology: 700.0
    upgrade: 1100.0
  }
  total_damage_dealt {
    life: 36230.6367188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9468.8359375
    shields: 11155.4140625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2575
vespene: 1044
food_cap: 200
food_used: 129
food_army: 75
food_workers: 54
idle_worker_count: 9
army_count: 31
warp_gate_count: 12

game_loop:  24207
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 12
}
multi {
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 127
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 45
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 7
    shields: 40
    energy: 21
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 33
    shields: 100
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 72
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 140
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 26
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 30
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 78
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 95
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 56
  }
}

Score:  23144
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
