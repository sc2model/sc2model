----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 6102
  player_apm: 258
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 6552
  player_apm: 320
}
game_duration_loops: 15197
game_duration_seconds: 678.484863281
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11632
score_details {
  idle_production_time: 869.3125
  idle_worker_time: 220.3125
  total_value_units: 6100.0
  total_value_structures: 4150.0
  killed_value_units: 2225.0
  killed_value_structures: 0.0
  collected_minerals: 10285.0
  collected_vespene: 2148.0
  collection_rate_minerals: 2603.0
  collection_rate_vespene: 627.0
  spent_minerals: 9524.0
  spent_vespene: 1787.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1566.0
    economy: 50.0
    technology: -76.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1150.0
    economy: 5650.0
    technology: 1750.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2300.0
    economy: 5850.0
    technology: 1700.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3350.875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2349.84130859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 401.341308594
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 783
vespene: 349
food_cap: 93
food_used: 83
food_army: 23
food_workers: 58
idle_worker_count: 5
army_count: 11
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 48
  count: 7
}
groups {
  control_group_index: 1
  leader_unit_type: 43
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 22
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 56
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20425
score_details {
  idle_production_time: 3549.875
  idle_worker_time: 368.25
  total_value_units: 14525.0
  total_value_structures: 7050.0
  killed_value_units: 6975.0
  killed_value_structures: 350.0
  collected_minerals: 20860.0
  collected_vespene: 5716.0
  collection_rate_minerals: 3275.0
  collection_rate_vespene: 985.0
  spent_minerals: 20249.0
  spent_vespene: 4287.0
  food_used {
    none: 0.0
    army: 86.0
    economy: 73.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3450.0
    economy: 1400.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5316.0
    economy: 50.0
    technology: -76.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 4300.0
    economy: 7900.0
    technology: 2625.0
    upgrade: 800.0
  }
  used_vespene {
    none: 50.0
    army: 1175.0
    economy: 0.0
    technology: 675.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 50.0
    army: 8600.0
    economy: 8550.0
    technology: 2625.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 50.0
    army: 2225.0
    economy: 0.0
    technology: 675.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 10977.6982422
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7404.44628906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2302.28710938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 633
vespene: 1417
food_cap: 200
food_used: 159
food_army: 86
food_workers: 73
idle_worker_count: 0
army_count: 41
warp_gate_count: 0

game_loop:  15197
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 43
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 22
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 48
  count: 7
}
groups {
  control_group_index: 7
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 369
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  20425
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
