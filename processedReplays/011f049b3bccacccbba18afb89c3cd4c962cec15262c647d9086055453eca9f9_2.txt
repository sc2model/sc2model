----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 146
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3992
  player_apm: 76
}
game_duration_loops: 6979
game_duration_seconds: 311.584228516
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4300
score_details {
  idle_production_time: 2885.4375
  idle_worker_time: 150.875
  total_value_units: 3300.0
  total_value_structures: 1050.0
  killed_value_units: 475.0
  killed_value_structures: 0.0
  collected_minerals: 3910.0
  collected_vespene: 640.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 179.0
  spent_minerals: 3275.0
  spent_vespene: 475.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 2175.0
    technology: 575.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1525.0
    economy: 2425.0
    technology: 575.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 338.0
    shields: 1020.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1236.22558594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 685
vespene: 165
food_cap: 52
food_used: 26
food_army: 8
food_workers: 18
idle_worker_count: 18
army_count: 2
warp_gate_count: 0

game_loop:  6979
ui_data{
 
Score:  4300
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
