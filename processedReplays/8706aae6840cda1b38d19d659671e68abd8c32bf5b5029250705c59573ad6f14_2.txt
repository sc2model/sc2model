----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3446
  player_apm: 200
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3443
  player_apm: 338
}
game_duration_loops: 22357
game_duration_seconds: 998.150024414
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10774
score_details {
  idle_production_time: 1445.0
  idle_worker_time: 54.5625
  total_value_units: 5675.0
  total_value_structures: 3825.0
  killed_value_units: 1550.0
  killed_value_structures: 0.0
  collected_minerals: 9045.0
  collected_vespene: 1972.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 358.0
  spent_minerals: 8262.0
  spent_vespene: 1356.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1075.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 975.0
    economy: 150.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2025.0
    economy: 4450.0
    technology: 1400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 2800.0
    economy: 4900.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2340.0234375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1390.86035156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 460.36328125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 833
vespene: 616
food_cap: 109
food_used: 80
food_army: 41
food_workers: 39
idle_worker_count: 1
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 10
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 6
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 9922
score_details {
  idle_production_time: 6674.4375
  idle_worker_time: 1003.0
  total_value_units: 16400.0
  total_value_structures: 4825.0
  killed_value_units: 10350.0
  killed_value_structures: 0.0
  collected_minerals: 18865.0
  collected_vespene: 4256.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 0.0
  spent_minerals: 18737.0
  spent_vespene: 4081.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7650.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8450.0
    economy: 2956.0
    technology: -13.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1950.0
    economy: 3500.0
    technology: 1550.0
    upgrade: 650.0
  }
  used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 475.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 50.0
    army: 10300.0
    economy: 6700.0
    technology: 1650.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 50.0
    army: 2900.0
    economy: 0.0
    technology: 475.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 13164.0527344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12311.34375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2517.58764648
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 172
vespene: 175
food_cap: 118
food_used: 58
food_army: 39
food_workers: 18
idle_worker_count: 1
army_count: 26
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 18
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 6
}
multi {
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 148
    shields: 0
    energy: 56
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 2
    shields: 0
    energy: 66
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9668
score_details {
  idle_production_time: 8270.5
  idle_worker_time: 1360.375
  total_value_units: 17850.0
  total_value_structures: 5375.0
  killed_value_units: 16050.0
  killed_value_structures: 975.0
  collected_minerals: 20445.0
  collected_vespene: 4472.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 156.0
  spent_minerals: 20387.0
  spent_vespene: 4181.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11575.0
    economy: 2050.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10100.0
    economy: 2956.0
    technology: -13.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1650.0
    economy: 4100.0
    technology: 1550.0
    upgrade: 650.0
  }
  used_vespene {
    none: 50.0
    army: 100.0
    economy: 0.0
    technology: 475.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 50.0
    army: 11550.0
    economy: 7350.0
    technology: 1650.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 50.0
    army: 3000.0
    economy: 0.0
    technology: 475.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 23808.25
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13707.9287109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2745.35864258
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 102
vespene: 291
food_cap: 133
food_used: 53
food_army: 33
food_workers: 20
idle_worker_count: 3
army_count: 26
warp_gate_count: 0

game_loop:  22357
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 26
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 6
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 111
    shields: 0
    energy: 44
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 1
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 2
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  9668
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
