----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3532
  player_apm: 169
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 201
}
game_duration_loops: 10567
game_duration_seconds: 471.773986816
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7912
score_details {
  idle_production_time: 5352.3125
  idle_worker_time: 15.3125
  total_value_units: 6300.0
  total_value_structures: 1850.0
  killed_value_units: 1800.0
  killed_value_structures: 0.0
  collected_minerals: 9660.0
  collected_vespene: 1052.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 0.0
  spent_minerals: 8550.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 9.5
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 625.0
    economy: 1900.0
    technology: 1275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 675.0
    economy: 3950.0
    technology: 825.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 6500.0
    technology: 1600.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 474.5
    shields: 1138.5
    energy: 0.0
  }
  total_damage_taken {
    life: 8106.51464844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1160
vespene: 452
food_cap: 92
food_used: 34
food_army: 9
food_workers: 25
idle_worker_count: 2
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 129
  count: 18
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
single {
  unit {
    unit_type: 140
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7042
score_details {
  idle_production_time: 6074.0625
  idle_worker_time: 60.4375
  total_value_units: 6500.0
  total_value_structures: 1925.0
  killed_value_units: 2000.0
  killed_value_structures: 0.0
  collected_minerals: 10165.0
  collected_vespene: 1052.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 0.0
  spent_minerals: 9100.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 1487.0
    technology: 1225.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 3900.0
    technology: 425.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 6700.0
    technology: 1725.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 674.5
    shields: 1380.5
    energy: 0.0
  }
  total_damage_taken {
    life: 9019.015625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1115
vespene: 452
food_cap: 92
food_used: 33
food_army: 2
food_workers: 31
idle_worker_count: 1
army_count: 3
warp_gate_count: 0

game_loop:  10567
ui_data{
 multi {
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 124
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 123
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 123
  }
}

Score:  7042
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
