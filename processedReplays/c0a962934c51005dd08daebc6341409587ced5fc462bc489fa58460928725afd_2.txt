----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4883
  player_apm: 218
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4673
  player_apm: 219
}
game_duration_loops: 13712
game_duration_seconds: 612.185546875
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11807
score_details {
  idle_production_time: 1103.4375
  idle_worker_time: 276.0
  total_value_units: 6000.0
  total_value_structures: 4500.0
  killed_value_units: 1000.0
  killed_value_structures: 0.0
  collected_minerals: 9875.0
  collected_vespene: 2132.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 627.0
  spent_minerals: 9625.0
  spent_vespene: 2050.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 575.0
    economy: 425.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1850.0
    economy: 5300.0
    technology: 1750.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 1025.0
    economy: 0.0
    technology: 500.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2225.0
    economy: 6100.0
    technology: 1600.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 975.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2275.70849609
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2566.63525391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 335.391113281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 300
vespene: 82
food_cap: 117
food_used: 86
food_army: 37
food_workers: 46
idle_worker_count: 1
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 48
  count: 13
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 139
    shields: 0
    energy: 70
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 71
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 138
    shields: 0
    energy: 71
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16889
score_details {
  idle_production_time: 2870.75
  idle_worker_time: 490.625
  total_value_units: 9600.0
  total_value_structures: 6375.0
  killed_value_units: 2875.0
  killed_value_structures: 0.0
  collected_minerals: 16500.0
  collected_vespene: 3964.0
  collection_rate_minerals: 2323.0
  collection_rate_vespene: 694.0
  spent_minerals: 14600.0
  spent_vespene: 2575.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2000.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3075.0
    economy: 425.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2300.0
    economy: 6450.0
    technology: 2575.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 475.0
    economy: 0.0
    technology: 750.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 4725.0
    economy: 7400.0
    technology: 2575.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 1325.0
    economy: 0.0
    technology: 750.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 5862.99023438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4806.72363281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 707.661621094
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1950
vespene: 1389
food_cap: 149
food_used: 107
food_army: 46
food_workers: 61
idle_worker_count: 1
army_count: 26
warp_gate_count: 0

game_loop:  13712
ui_data{
 single {
  unit {
    unit_type: 32
    player_relative: 3
    health: 175
    shields: 0
    energy: 0
  }
}

Score:  16889
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
