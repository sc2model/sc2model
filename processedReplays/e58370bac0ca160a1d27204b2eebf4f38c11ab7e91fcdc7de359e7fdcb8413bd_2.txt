----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2098
  player_apm: 43
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2031
  player_apm: 43
}
game_duration_loops: 5631
game_duration_seconds: 251.401473999
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2605
score_details {
  idle_production_time: 441.1875
  idle_worker_time: 667.125
  total_value_units: 1200.0
  total_value_structures: 1475.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 2340.0
  collected_vespene: 432.0
  collection_rate_minerals: 419.0
  collection_rate_vespene: 246.0
  spent_minerals: 2000.0
  spent_vespene: 225.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 14.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 250.0
    technology: 67.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1600.0
    technology: 350.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 250.0
    economy: 2100.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 228.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2752.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1700.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 323
vespene: 207
food_cap: 31
food_used: 14
food_army: 0
food_workers: 14
idle_worker_count: 14
army_count: 0
warp_gate_count: 0

game_loop:  5631
ui_data{
 
Score:  2605
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
