----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4343
  player_apm: 221
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4424
  player_apm: 215
}
game_duration_loops: 16723
game_duration_seconds: 746.614624023
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10617
score_details {
  idle_production_time: 5457.5
  idle_worker_time: 31.6875
  total_value_units: 7550.0
  total_value_structures: 1375.0
  killed_value_units: 875.0
  killed_value_structures: 0.0
  collected_minerals: 9355.0
  collected_vespene: 1292.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 335.0
  spent_minerals: 8780.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 675.0
    economy: -275.0
    technology: -270.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 575.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2850.0
    economy: 4650.0
    technology: 975.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 100.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 3150.0
    economy: 4550.0
    technology: 1375.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 1515.28369141
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2059.33056641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 625
vespene: 67
food_cap: 108
food_used: 106
food_army: 68
food_workers: 38
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 137
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 136
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 96
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 144
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11631
score_details {
  idle_production_time: 14395.625
  idle_worker_time: 145.5625
  total_value_units: 17800.0
  total_value_structures: 2275.0
  killed_value_units: 7700.0
  killed_value_structures: 0.0
  collected_minerals: 18055.0
  collected_vespene: 4404.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 1007.0
  spent_minerals: 17692.0
  spent_vespene: 3461.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5300.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8587.0
    economy: 25.0
    technology: -270.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2261.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 575.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 5750.0
    technology: 1325.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 10225.0
    economy: 6900.0
    technology: 1925.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 2525.0
    economy: 0.0
    technology: 600.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 12185.9746094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 18228.7617188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 413
vespene: 943
food_cap: 152
food_used: 80
food_army: 34
food_workers: 46
idle_worker_count: 46
army_count: 8
warp_gate_count: 0

game_loop:  16723
ui_data{
 
Score:  11631
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
