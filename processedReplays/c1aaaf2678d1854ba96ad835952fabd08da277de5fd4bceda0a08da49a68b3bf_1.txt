----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3563
  player_apm: 131
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3558
  player_apm: 75
}
game_duration_loops: 18276
game_duration_seconds: 815.949768066
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12275
score_details {
  idle_production_time: 7668.25
  idle_worker_time: 48.6875
  total_value_units: 7200.0
  total_value_structures: 1700.0
  killed_value_units: 0.0
  killed_value_structures: 100.0
  collected_minerals: 9220.0
  collected_vespene: 1992.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 671.0
  spent_minerals: 8487.0
  spent_vespene: 1325.0
  food_used {
    none: 0.0
    army: 53.5
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25.0
    economy: -138.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 5600.0
    technology: 1000.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 200.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2175.0
    economy: 5850.0
    technology: 1150.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 300.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 200.0
    shields: 220.0
    energy: 0.0
  }
  total_damage_taken {
    life: 176.145019531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 783
vespene: 667
food_cap: 138
food_used: 101
food_army: 53
food_workers: 48
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 105
  count: 32
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 30004
score_details {
  idle_production_time: 22252.1875
  idle_worker_time: 49.875
  total_value_units: 21600.0
  total_value_structures: 3400.0
  killed_value_units: 9125.0
  killed_value_structures: 3250.0
  collected_minerals: 26390.0
  collected_vespene: 9476.0
  collection_rate_minerals: 2855.0
  collection_rate_vespene: 1299.0
  spent_minerals: 20262.0
  spent_vespene: 7800.0
  food_used {
    none: 0.0
    army: 86.0
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5200.0
    economy: 4400.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2025.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4625.0
    economy: -138.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4500.0
    economy: 8600.0
    technology: 1850.0
    upgrade: 1725.0
  }
  used_vespene {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: 850.0
    upgrade: 1725.0
  }
  total_used_minerals {
    none: 0.0
    army: 9125.0
    economy: 9600.0
    technology: 2200.0
    upgrade: 1325.0
  }
  total_used_vespene {
    none: 0.0
    army: 5225.0
    economy: 0.0
    technology: 1100.0
    upgrade: 1325.0
  }
  total_damage_dealt {
    life: 12203.0
    shields: 14365.0400391
    energy: 0.0
  }
  total_damage_taken {
    life: 8820.55175781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 6178
vespene: 1676
food_cap: 200
food_used: 167
food_army: 86
food_workers: 81
idle_worker_count: 0
army_count: 39
warp_gate_count: 0

game_loop:  18276
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 19
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 107
  count: 20
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 86
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 81
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
}

Score:  30004
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
