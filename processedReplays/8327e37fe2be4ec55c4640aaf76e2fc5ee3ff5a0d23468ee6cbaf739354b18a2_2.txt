----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5354
  player_apm: 222
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5314
  player_apm: 186
}
game_duration_loops: 11079
game_duration_seconds: 494.632720947
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9605
score_details {
  idle_production_time: 5858.5625
  idle_worker_time: 26.75
  total_value_units: 7700.0
  total_value_structures: 1600.0
  killed_value_units: 950.0
  killed_value_structures: 0.0
  collected_minerals: 9020.0
  collected_vespene: 1260.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 492.0
  spent_minerals: 8800.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 40.5
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 725.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1975.0
    economy: 5175.0
    technology: 875.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 150.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2775.0
    economy: 6175.0
    technology: 1025.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1577.81835938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2010.03222656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 270
vespene: 110
food_cap: 98
food_used: 89
food_army: 40
food_workers: 49
idle_worker_count: 0
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 16
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 129
  count: 1
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 71
    shields: 0
    energy: 33
  }
}

score{
 score_type: Melee
score: 8665
score_details {
  idle_production_time: 6328.6875
  idle_worker_time: 26.75
  total_value_units: 9650.0
  total_value_structures: 1600.0
  killed_value_units: 2275.0
  killed_value_structures: 0.0
  collected_minerals: 10805.0
  collected_vespene: 1660.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 515.0
  spent_minerals: 10625.0
  spent_vespene: 1475.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1725.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2600.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1250.0
    economy: 5125.0
    technology: 875.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 150.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 3750.0
    economy: 6875.0
    technology: 1025.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 250.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 3862.21142578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6034.04785156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 230
vespene: 185
food_cap: 114
food_used: 74
food_army: 24
food_workers: 50
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  11079
ui_data{
 
Score:  8665
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
