----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4305
  player_apm: 209
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4116
  player_apm: 179
}
game_duration_loops: 22192
game_duration_seconds: 990.783447266
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11157
score_details {
  idle_production_time: 6040.125
  idle_worker_time: 56.4375
  total_value_units: 7000.0
  total_value_structures: 2475.0
  killed_value_units: 1550.0
  killed_value_structures: 200.0
  collected_minerals: 9870.0
  collected_vespene: 1912.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 627.0
  spent_minerals: 9550.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 300.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1100.0
    economy: -175.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 5200.0
    technology: 1775.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 450.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 5800.0
    technology: 1925.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 550.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2428.25
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1906.59619141
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 370
vespene: 262
food_cap: 106
food_used: 81
food_army: 33
food_workers: 48
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 25
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 35
  }
}

score{
 score_type: Melee
score: 23904
score_details {
  idle_production_time: 38630.625
  idle_worker_time: 65.625
  total_value_units: 23950.0
  total_value_structures: 3650.0
  killed_value_units: 10375.0
  killed_value_structures: 400.0
  collected_minerals: 27989.0
  collected_vespene: 6940.0
  collection_rate_minerals: 2906.0
  collection_rate_vespene: 627.0
  spent_minerals: 26650.0
  spent_vespene: 6775.0
  food_used {
    none: 0.0
    army: 105.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7675.0
    economy: 800.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7075.0
    economy: 225.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1950.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6150.0
    economy: 8225.0
    technology: 2325.0
    upgrade: 1625.0
  }
  used_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 800.0
    upgrade: 1625.0
  }
  total_used_minerals {
    none: 0.0
    army: 16250.0
    economy: 9275.0
    technology: 2675.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 0.0
    army: 4925.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 10609.1777344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12535.9804688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1389
vespene: 165
food_cap: 198
food_used: 162
food_army: 105
food_workers: 57
idle_worker_count: 0
army_count: 111
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 112
  count: 121
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 6
}
single {
  unit {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23714
score_details {
  idle_production_time: 50750.5625
  idle_worker_time: 357.0
  total_value_units: 30200.0
  total_value_structures: 3950.0
  killed_value_units: 21375.0
  killed_value_structures: 1025.0
  collected_minerals: 32121.0
  collected_vespene: 7868.0
  collection_rate_minerals: 2055.0
  collection_rate_vespene: 492.0
  spent_minerals: 31150.0
  spent_vespene: 7625.0
  food_used {
    none: 0.0
    army: 102.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13625.0
    economy: 4425.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10625.0
    economy: 225.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 3000.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6150.0
    economy: 8225.0
    technology: 2325.0
    upgrade: 1625.0
  }
  used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 800.0
    upgrade: 1625.0
  }
  total_used_minerals {
    none: 0.0
    army: 21700.0
    economy: 10025.0
    technology: 2675.0
    upgrade: 1400.0
  }
  total_used_vespene {
    none: 0.0
    army: 5975.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1400.0
  }
  total_damage_dealt {
    life: 23463.5234375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 18040.4589844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1021
vespene: 243
food_cap: 200
food_used: 159
food_army: 102
food_workers: 57
idle_worker_count: 4
army_count: 75
warp_gate_count: 0

game_loop:  22192
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 47
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 6
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 315
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 497
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 320
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  23714
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
