----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3305
  player_apm: 88
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3331
  player_apm: 78
}
game_duration_loops: 26288
game_duration_seconds: 1173.65332031
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12336
score_details {
  idle_production_time: 1648.125
  idle_worker_time: 277.0
  total_value_units: 3610.0
  total_value_structures: 5100.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 8930.0
  collected_vespene: 2476.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 627.0
  spent_minerals: 8180.0
  spent_vespene: 2100.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1560.0
    economy: 4650.0
    technology: 2450.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 700.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 860.0
    economy: 4450.0
    technology: 2300.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 600.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 342.683105469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 20.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 800
vespene: 376
food_cap: 102
food_used: 69
food_army: 24
food_workers: 45
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
production {
  unit {
    unit_type: 72
    player_relative: 1
    health: 550
    shields: 550
    energy: 0
  }
}

score{
 score_type: Melee
score: 27047
score_details {
  idle_production_time: 9282.5
  idle_worker_time: 517.625
  total_value_units: 12870.0
  total_value_structures: 10850.0
  killed_value_units: 7975.0
  killed_value_structures: 350.0
  collected_minerals: 20955.0
  collected_vespene: 8632.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 1299.0
  spent_minerals: 20820.0
  spent_vespene: 7950.0
  food_used {
    none: 0.0
    army: 92.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4875.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1740.0
    economy: -300.0
    technology: 750.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6080.0
    economy: 7100.0
    technology: 4400.0
    upgrade: 1650.0
  }
  used_vespene {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1650.0
  }
  total_used_minerals {
    none: 0.0
    army: 6070.0
    economy: 7450.0
    technology: 5150.0
    upgrade: 1500.0
  }
  total_used_vespene {
    none: 0.0
    army: 3750.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1500.0
  }
  total_damage_dealt {
    life: 8103.9296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2259.0
    shields: 2807.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 185
vespene: 682
food_cap: 200
food_used: 150
food_army: 92
food_workers: 58
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 80
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 82
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 6
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 63
    player_relative: 1
    health: 400
    shields: 400
    energy: 0
  }
}

score{
 score_type: Melee
score: 37444
score_details {
  idle_production_time: 17493.625
  idle_worker_time: 2475.4375
  total_value_units: 22580.0
  total_value_structures: 11250.0
  killed_value_units: 23625.0
  killed_value_structures: 3425.0
  collected_minerals: 31842.0
  collected_vespene: 12352.0
  collection_rate_minerals: 2900.0
  collection_rate_vespene: 649.0
  spent_minerals: 27340.0
  spent_vespene: 11600.0
  food_used {
    none: 0.0
    army: 133.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13425.0
    economy: 4100.0
    technology: 1375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4790.0
    economy: -300.0
    technology: 750.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 8965.0
    economy: 7900.0
    technology: 4400.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 0.0
    army: 5825.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 0.0
    army: 11955.0
    economy: 8250.0
    technology: 5150.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 0.0
    army: 7175.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 34710.4726562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6360.875
    shields: 7929.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 4552
vespene: 752
food_cap: 200
food_used: 199
food_army: 133
food_workers: 66
idle_worker_count: 7
army_count: 29
warp_gate_count: 0

game_loop:  26288
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 80
  count: 13
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 82
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 6
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
}

Score:  37444
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
