----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2711
  player_apm: 75
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2640
  player_apm: 75
}
game_duration_loops: 24243
game_duration_seconds: 1082.35229492
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8493
score_details {
  idle_production_time: 1064.0625
  idle_worker_time: 918.625
  total_value_units: 3425.0
  total_value_structures: 3350.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 6585.0
  collected_vespene: 1108.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 291.0
  spent_minerals: 6075.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4075.0
    technology: 1050.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 4075.0
    technology: 1000.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1454.68994141
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 423.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 560
vespene: 708
food_cap: 101
food_used: 69
food_army: 36
food_workers: 33
idle_worker_count: 3
army_count: 29
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 2
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12190
score_details {
  idle_production_time: 5867.9375
  idle_worker_time: 2218.5
  total_value_units: 7125.0
  total_value_structures: 6875.0
  killed_value_units: 4100.0
  killed_value_structures: 825.0
  collected_minerals: 15450.0
  collected_vespene: 2440.0
  collection_rate_minerals: 419.0
  collection_rate_vespene: 0.0
  spent_minerals: 12975.0
  spent_vespene: 2175.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 2400.0
    technology: 525.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1700.0
    economy: 3200.0
    technology: 1100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2250.0
    economy: 4175.0
    technology: 850.0
    upgrade: 650.0
  }
  used_vespene {
    none: 50.0
    army: 575.0
    economy: 0.0
    technology: 150.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 50.0
    army: 3950.0
    economy: 7425.0
    technology: 1950.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 50.0
    army: 875.0
    economy: 0.0
    technology: 600.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 9286.203125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 17212.3867188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 198.588867188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2525
vespene: 265
food_cap: 190
food_used: 65
food_army: 45
food_workers: 20
idle_worker_count: 11
army_count: 34
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 109
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9360
score_details {
  idle_production_time: 7223.75
  idle_worker_time: 4187.8125
  total_value_units: 8775.0
  total_value_structures: 8175.0
  killed_value_units: 6650.0
  killed_value_structures: 2875.0
  collected_minerals: 16295.0
  collected_vespene: 2440.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 16325.0
  spent_vespene: 2425.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 11.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3700.0
    economy: 3525.0
    technology: 1375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 175.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4400.0
    economy: 3650.0
    technology: 1200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1350.0
    economy: 4475.0
    technology: 1550.0
    upgrade: 650.0
  }
  used_vespene {
    none: 50.0
    army: 150.0
    economy: 150.0
    technology: 250.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 50.0
    army: 5600.0
    economy: 8325.0
    technology: 2550.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 50.0
    army: 875.0
    economy: 300.0
    technology: 700.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 18870.5898438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21669.3945312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 260.588867188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 20
vespene: 15
food_cap: 200
food_used: 38
food_army: 27
food_workers: 11
idle_worker_count: 8
army_count: 21
warp_gate_count: 0

game_loop:  24243
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 73
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
}

Score:  9360
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
