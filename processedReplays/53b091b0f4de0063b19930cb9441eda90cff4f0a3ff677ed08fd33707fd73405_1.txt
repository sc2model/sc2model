----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3622
  player_apm: 180
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3630
  player_apm: 138
}
game_duration_loops: 24899
game_duration_seconds: 1111.64001465
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11496
score_details {
  idle_production_time: 7717.0
  idle_worker_time: 58.0
  total_value_units: 8050.0
  total_value_structures: 1800.0
  killed_value_units: 900.0
  killed_value_structures: 0.0
  collected_minerals: 9655.0
  collected_vespene: 2320.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 963.0
  spent_minerals: 9380.0
  spent_vespene: 2174.0
  food_used {
    none: 0.0
    army: 48.5
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 299.0
    economy: 881.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 24.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -200.0
    army: 2775.0
    economy: 5150.0
    technology: 800.0
    upgrade: 450.0
  }
  used_vespene {
    none: -100.0
    army: 1350.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2400.0
    economy: 6650.0
    technology: 950.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1050.98339844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2276.68554688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 325
vespene: 146
food_cap: 122
food_used: 95
food_army: 48
food_workers: 47
idle_worker_count: 0
army_count: 36
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 29
}
groups {
  control_group_index: 3
  leader_unit_type: 108
  count: 13
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 134
    shields: 0
    energy: 47
  }
}

score{
 score_type: Melee
score: 25568
score_details {
  idle_production_time: 33071.4375
  idle_worker_time: 192.6875
  total_value_units: 26100.0
  total_value_structures: 3100.0
  killed_value_units: 10975.0
  killed_value_structures: 700.0
  collected_minerals: 28190.0
  collected_vespene: 10332.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 1119.0
  spent_minerals: 25180.0
  spent_vespene: 8274.0
  food_used {
    none: 0.0
    army: 73.0
    economy: 70.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7550.0
    economy: 1750.0
    technology: 700.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7749.0
    economy: 881.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2849.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1600.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -200.0
    army: 4250.0
    economy: 8700.0
    technology: 1400.0
    upgrade: 1775.0
  }
  used_vespene {
    none: -100.0
    army: 2250.0
    economy: 0.0
    technology: 600.0
    upgrade: 1775.0
  }
  total_used_minerals {
    none: 0.0
    army: 14100.0
    economy: 10600.0
    technology: 1750.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 6075.0
    economy: 0.0
    technology: 850.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 13930.6582031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16661.8789062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3060
vespene: 2058
food_cap: 200
food_used: 143
food_army: 73
food_workers: 70
idle_worker_count: 3
army_count: 56
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 40
}
groups {
  control_group_index: 3
  leader_unit_type: 108
  count: 20
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 154
  }
}

score{
 score_type: Melee
score: 32133
score_details {
  idle_production_time: 49540.25
  idle_worker_time: 346.5625
  total_value_units: 35350.0
  total_value_structures: 3450.0
  killed_value_units: 16850.0
  killed_value_structures: 1500.0
  collected_minerals: 38060.0
  collected_vespene: 13852.0
  collection_rate_minerals: 2799.0
  collection_rate_vespene: 649.0
  spent_minerals: 34280.0
  spent_vespene: 10924.0
  food_used {
    none: 0.0
    army: 119.5
    economy: 70.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11400.0
    economy: 3650.0
    technology: 800.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2350.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11949.0
    economy: 881.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3524.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 3050.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -200.0
    army: 6825.0
    economy: 9200.0
    technology: 1500.0
    upgrade: 2275.0
  }
  used_vespene {
    none: -100.0
    army: 2850.0
    economy: 0.0
    technology: 750.0
    upgrade: 2275.0
  }
  total_used_minerals {
    none: 0.0
    army: 22600.0
    economy: 11250.0
    technology: 1950.0
    upgrade: 2275.0
  }
  total_used_vespene {
    none: 0.0
    army: 8075.0
    economy: 0.0
    technology: 1150.0
    upgrade: 2275.0
  }
  total_damage_dealt {
    life: 22265.5351562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23336.0390625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3830
vespene: 2928
food_cap: 200
food_used: 189
food_army: 119
food_workers: 70
idle_worker_count: 0
army_count: 82
warp_gate_count: 0

game_loop:  24899
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 57
}
groups {
  control_group_index: 3
  leader_unit_type: 108
  count: 18
}
groups {
  control_group_index: 4
  leader_unit_type: 112
  count: 6
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  32133
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
