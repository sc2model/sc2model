----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3834
  player_apm: 159
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3893
  player_apm: 187
}
game_duration_loops: 4879
game_duration_seconds: 217.827697754
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2167
score_details {
  idle_production_time: 188.4375
  idle_worker_time: 199.25
  total_value_units: 1550.0
  total_value_structures: 1400.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 2180.0
  collected_vespene: 688.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 134.0
  spent_minerals: 1950.0
  spent_vespene: 350.0
  food_used {
    none: 0.0
    army: 1.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 1151.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 50.0
    economy: 1050.0
    technology: 300.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 250.0
    economy: 2200.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 306.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2742.9855957
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.6708984375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 279
vespene: 338
food_cap: 31
food_used: 2
food_army: 1
food_workers: 1
idle_worker_count: 1
army_count: 1
warp_gate_count: 0

game_loop:  4879
ui_data{
 
Score:  2167
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
