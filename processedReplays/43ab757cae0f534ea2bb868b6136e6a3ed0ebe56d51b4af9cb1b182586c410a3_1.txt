----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3666
  player_apm: 172
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 173
}
game_duration_loops: 7059
game_duration_seconds: 315.155914307
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5500
score_details {
  idle_production_time: 6202.9375
  idle_worker_time: 2.25
  total_value_units: 3850.0
  total_value_structures: 1000.0
  killed_value_units: 25.0
  killed_value_structures: 150.0
  collected_minerals: 4790.0
  collected_vespene: 660.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 313.0
  spent_minerals: 4700.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: -538.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 3750.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 900.0
    economy: 3800.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 913.764160156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 974.077148438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 140
vespene: 510
food_cap: 76
food_used: 49
food_army: 7
food_workers: 38
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  7059
ui_data{
 
Score:  5500
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
