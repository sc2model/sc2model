----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5094
  player_apm: 208
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4335
  player_apm: 89
}
game_duration_loops: 12222
game_duration_seconds: 545.663085938
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6787
score_details {
  idle_production_time: 1580.0
  idle_worker_time: 176.8125
  total_value_units: 4350.0
  total_value_structures: 3125.0
  killed_value_units: 400.0
  killed_value_structures: 125.0
  collected_minerals: 6145.0
  collected_vespene: 1992.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 358.0
  spent_minerals: 6075.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1700.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 3275.0
    technology: 1600.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1850.0
    economy: 3175.0
    technology: 1300.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1451.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1281.0
    shields: 1066.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 120
vespene: 642
food_cap: 70
food_used: 39
food_army: 6
food_workers: 31
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 488
  count: 1
}
single {
  unit {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
    add_on {
      unit_type: 133
      build_progress: 0.137499988079
    }
  }
}

score{
 score_type: Melee
score: 7002
score_details {
  idle_production_time: 2639.875
  idle_worker_time: 239.8125
  total_value_units: 6425.0
  total_value_structures: 3675.0
  killed_value_units: 700.0
  killed_value_structures: 125.0
  collected_minerals: 7325.0
  collected_vespene: 2552.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 335.0
  spent_minerals: 7075.0
  spent_vespene: 2125.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2350.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 3025.0
    technology: 1600.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 3050.0
    economy: 3375.0
    technology: 1600.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1906.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1762.625
    shields: 1775.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 300
vespene: 427
food_cap: 78
food_used: 39
food_army: 11
food_workers: 28
idle_worker_count: 28
army_count: 6
warp_gate_count: 4

game_loop:  12222
ui_data{
 
Score:  7002
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
