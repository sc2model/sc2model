----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3188
  player_apm: 135
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3209
  player_apm: 120
}
game_duration_loops: 8684
game_duration_seconds: 387.705627441
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5383
score_details {
  idle_production_time: 3661.375
  idle_worker_time: 53.25
  total_value_units: 3750.0
  total_value_structures: 1000.0
  killed_value_units: 525.0
  killed_value_structures: 250.0
  collected_minerals: 4245.0
  collected_vespene: 1544.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 313.0
  spent_minerals: 3531.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 481.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 875.0
    economy: 1850.0
    technology: 450.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 2650.0
    technology: 450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1209.06005859
    shields: 2236.43505859
    energy: 0.0
  }
  total_damage_taken {
    life: 4835.51464844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 764
vespene: 619
food_cap: 46
food_used: 42
food_army: 25
food_workers: 17
idle_worker_count: 17
army_count: 10
warp_gate_count: 0

game_loop:  8684
ui_data{
 
Score:  5383
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
