----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3058
  player_apm: 127
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2952
  player_apm: 233
}
game_duration_loops: 31631
game_duration_seconds: 1412.19677734
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11324
score_details {
  idle_production_time: 1952.9375
  idle_worker_time: 284.375
  total_value_units: 4700.0
  total_value_structures: 4325.0
  killed_value_units: 475.0
  killed_value_structures: 0.0
  collected_minerals: 8315.0
  collected_vespene: 2168.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 537.0
  spent_minerals: 8275.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 325.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2200.0
    economy: 4200.0
    technology: 2225.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 1000.0
    economy: 0.0
    technology: 550.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 1950.0
    economy: 4400.0
    technology: 1775.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 550.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 827.233398438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 176.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 109.375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 81
vespene: 268
food_cap: 94
food_used: 81
food_army: 43
food_workers: 38
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21845
score_details {
  idle_production_time: 9802.5625
  idle_worker_time: 1439.8125
  total_value_units: 13325.0
  total_value_structures: 6625.0
  killed_value_units: 10225.0
  killed_value_structures: 0.0
  collected_minerals: 20460.0
  collected_vespene: 6544.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 537.0
  spent_minerals: 17900.0
  spent_vespene: 4875.0
  food_used {
    none: 0.0
    army: 101.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7150.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2825.0
    economy: 200.0
    technology: 109.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5000.0
    economy: 5950.0
    technology: 2475.0
    upgrade: 600.0
  }
  used_vespene {
    none: 50.0
    army: 2100.0
    economy: 150.0
    technology: 600.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 50.0
    army: 8175.0
    economy: 6600.0
    technology: 2575.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 50.0
    army: 2800.0
    economy: 300.0
    technology: 600.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 13656.8974609
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4844.72998047
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1803.63549805
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2601
vespene: 1669
food_cap: 148
food_used: 148
food_army: 101
food_workers: 47
idle_worker_count: 5
army_count: 52
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 110
    shields: 0
    energy: 177
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 94
    shields: 0
    energy: 200
  }
}

score{
 score_type: Melee
score: 23166
score_details {
  idle_production_time: 19261.5625
  idle_worker_time: 6034.0
  total_value_units: 20525.0
  total_value_structures: 8925.0
  killed_value_units: 22325.0
  killed_value_structures: 1450.0
  collected_minerals: 26650.0
  collected_vespene: 9144.0
  collection_rate_minerals: 475.0
  collection_rate_vespene: 358.0
  spent_minerals: 26075.0
  spent_vespene: 8900.0
  food_used {
    none: 0.0
    army: 122.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 15075.0
    economy: 3175.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 4850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5600.0
    economy: 2864.0
    technology: 509.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2175.0
    economy: 305.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 6000.0
    economy: 4850.0
    technology: 3175.0
    upgrade: 2200.0
  }
  used_vespene {
    none: 50.0
    army: 2800.0
    economy: 150.0
    technology: 850.0
    upgrade: 2200.0
  }
  total_used_minerals {
    none: 50.0
    army: 12900.0
    economy: 8050.0
    technology: 3775.0
    upgrade: 1500.0
  }
  total_used_vespene {
    none: 50.0
    army: 5275.0
    economy: 900.0
    technology: 850.0
    upgrade: 1500.0
  }
  total_damage_dealt {
    life: 28929.8769531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15744.2314453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2971.02905273
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 602
vespene: 239
food_cap: 189
food_used: 149
food_army: 122
food_workers: 27
idle_worker_count: 12
army_count: 58
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 23303
score_details {
  idle_production_time: 21034.125
  idle_worker_time: 6534.125
  total_value_units: 21050.0
  total_value_structures: 8925.0
  killed_value_units: 27475.0
  killed_value_structures: 1800.0
  collected_minerals: 27295.0
  collected_vespene: 9536.0
  collection_rate_minerals: 503.0
  collection_rate_vespene: 313.0
  spent_minerals: 26075.0
  spent_vespene: 8900.0
  food_used {
    none: 0.0
    army: 108.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 18425.0
    economy: 3525.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 6650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6400.0
    economy: 2864.0
    technology: 609.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2300.0
    economy: 305.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5300.0
    economy: 4850.0
    technology: 3075.0
    upgrade: 2200.0
  }
  used_vespene {
    none: 50.0
    army: 2700.0
    economy: 150.0
    technology: 850.0
    upgrade: 2200.0
  }
  total_used_minerals {
    none: 50.0
    army: 13350.0
    economy: 8050.0
    technology: 3775.0
    upgrade: 1700.0
  }
  total_used_vespene {
    none: 50.0
    army: 5350.0
    economy: 900.0
    technology: 850.0
    upgrade: 1700.0
  }
  total_damage_dealt {
    life: 35096.078125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 17336.4414062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3665.60229492
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1247
vespene: 631
food_cap: 189
food_used: 135
food_army: 108
food_workers: 27
idle_worker_count: 2
army_count: 58
warp_gate_count: 0

game_loop:  31631
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 121
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 111
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 185
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 170
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 110
    shields: 0
    energy: 163
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 58
    shields: 0
    energy: 186
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 104
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 72
    shields: 0
    energy: 183
    transport_slots_taken: 1
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 159
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 15
    shields: 0
    energy: 200
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 86
    shields: 0
    energy: 93
  }
  units {
    unit_type: 55
    player_relative: 1
    health: 81
    shields: 0
    energy: 116
  }
}

Score:  23303
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
