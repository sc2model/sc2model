----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3449
  player_apm: 123
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3355
  player_apm: 218
}
game_duration_loops: 19472
game_duration_seconds: 869.346374512
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11870
score_details {
  idle_production_time: 1370.5
  idle_worker_time: 384.0
  total_value_units: 4800.0
  total_value_structures: 3875.0
  killed_value_units: 1325.0
  killed_value_structures: 75.0
  collected_minerals: 9780.0
  collected_vespene: 1608.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 335.0
  spent_minerals: 8350.0
  spent_vespene: 1475.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2050.0
    economy: 4700.0
    technology: 1600.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 475.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5300.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 425.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3245.94384766
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1406.24291992
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 627.221435547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1462
vespene: 133
food_cap: 101
food_used: 86
food_army: 42
food_workers: 44
idle_worker_count: 1
army_count: 19
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 142
    shields: 0
    energy: 81
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 142
    shields: 0
    energy: 102
  }
}

score{
 score_type: Melee
score: 18854
score_details {
  idle_production_time: 6864.5625
  idle_worker_time: 4444.25
  total_value_units: 16250.0
  total_value_structures: 6300.0
  killed_value_units: 16675.0
  killed_value_structures: 575.0
  collected_minerals: 23625.0
  collected_vespene: 4172.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 291.0
  spent_minerals: 20975.0
  spent_vespene: 4025.0
  food_used {
    none: 0.0
    army: 83.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12200.0
    economy: 1825.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7200.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4075.0
    economy: 5950.0
    technology: 2700.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 800.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 10775.0
    economy: 6950.0
    technology: 2700.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 800.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 21095.5976562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11441.9570312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5593.2578125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2682
vespene: 147
food_cap: 149
food_used: 137
food_army: 83
food_workers: 54
idle_worker_count: 22
army_count: 48
warp_gate_count: 0

game_loop:  19472
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 37
    shields: 0
    energy: 4
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 124
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 54
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 9
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 81
    shields: 0
    energy: 5
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

Score:  18854
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
