----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2957
  player_apm: 67
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2896
  player_apm: 158
}
game_duration_loops: 21229
game_duration_seconds: 947.789367676
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7569
score_details {
  idle_production_time: 1743.125
  idle_worker_time: 381.4375
  total_value_units: 3375.0
  total_value_structures: 2450.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 5880.0
  collected_vespene: 1476.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 380.0
  spent_minerals: 5237.0
  spent_vespene: 575.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 500.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1200.0
    economy: 2875.0
    technology: 1025.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1300.0
    economy: 3475.0
    technology: 1025.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 401.625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1068.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 693
vespene: 901
food_cap: 54
food_used: 54
food_army: 24
food_workers: 29
idle_worker_count: 1
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17329
score_details {
  idle_production_time: 5977.75
  idle_worker_time: 1212.1875
  total_value_units: 7800.0
  total_value_structures: 4500.0
  killed_value_units: 5200.0
  killed_value_structures: 0.0
  collected_minerals: 14325.0
  collected_vespene: 5584.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 492.0
  spent_minerals: 10462.0
  spent_vespene: 2475.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4025.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2170.0
    economy: 550.0
    technology: 196.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 466.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2200.0
    economy: 3650.0
    technology: 2075.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 950.0
    economy: 150.0
    technology: 425.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 4200.0
    economy: 4500.0
    technology: 2375.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 50.0
    army: 1300.0
    economy: 300.0
    technology: 425.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 6115.77392578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5041.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 408.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3798
vespene: 3081
food_cap: 94
food_used: 79
food_army: 44
food_workers: 35
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 18
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 24
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13798
score_details {
  idle_production_time: 6693.5
  idle_worker_time: 1632.625
  total_value_units: 8150.0
  total_value_structures: 4500.0
  killed_value_units: 8100.0
  killed_value_structures: 0.0
  collected_minerals: 15130.0
  collected_vespene: 5996.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 335.0
  spent_minerals: 11112.0
  spent_vespene: 2775.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6425.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4284.0
    economy: 1773.0
    technology: 348.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1428.0
    economy: 155.0
    technology: 101.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 650.0
    economy: 2500.0
    technology: 1925.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 275.0
    economy: 0.0
    technology: 325.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 4450.0
    economy: 4500.0
    technology: 2375.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 1400.0
    economy: 300.0
    technology: 425.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 9955.41503906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10532.1269531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 586.126464844
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3936
vespene: 3187
food_cap: 79
food_used: 36
food_army: 13
food_workers: 23
idle_worker_count: 7
army_count: 4
warp_gate_count: 0

game_loop:  21229
ui_data{
 production {
  unit {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  build_queue {
    unit_type: 689
    build_progress: 0.286458313465
  }
}

Score:  13798
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
