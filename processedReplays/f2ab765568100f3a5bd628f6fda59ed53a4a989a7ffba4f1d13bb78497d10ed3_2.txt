----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 172
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3503
  player_apm: 159
}
game_duration_loops: 15098
game_duration_seconds: 674.064880371
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13082
score_details {
  idle_production_time: 7824.9375
  idle_worker_time: 47.5
  total_value_units: 7500.0
  total_value_structures: 1850.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 9980.0
  collected_vespene: 2152.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 694.0
  spent_minerals: 8900.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2550.0
    economy: 5150.0
    technology: 1600.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 600.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 2950.0
    economy: 6000.0
    technology: 1750.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 650.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 105.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 524.169433594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1130
vespene: 502
food_cap: 106
food_used: 91
food_army: 37
food_workers: 54
idle_worker_count: 0
army_count: 63
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 129
  count: 1
}
single {
  unit {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 75
  }
}

score{
 score_type: Melee
score: 15525
score_details {
  idle_production_time: 26109.3125
  idle_worker_time: 47.5
  total_value_units: 14700.0
  total_value_structures: 2750.0
  killed_value_units: 5975.0
  killed_value_structures: 2225.0
  collected_minerals: 17605.0
  collected_vespene: 4520.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 313.0
  spent_minerals: 15675.0
  spent_vespene: 3725.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3675.0
    economy: 2925.0
    technology: 650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3025.0
    economy: 2600.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2650.0
    economy: 4125.0
    technology: 2225.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 600.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 7100.0
    economy: 7525.0
    technology: 2825.0
    upgrade: 975.0
  }
  total_used_vespene {
    none: 0.0
    army: 2200.0
    economy: 0.0
    technology: 850.0
    upgrade: 975.0
  }
  total_damage_dealt {
    life: 13977.9121094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14132.1904297
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1980
vespene: 795
food_cap: 132
food_used: 86
food_army: 54
food_workers: 32
idle_worker_count: 0
army_count: 38
warp_gate_count: 0
larva_count: 3

game_loop:  15098
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 109
  count: 10
}
multi {
  units {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1170
    shields: 0
    energy: 0
  }
}

Score:  15525
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
