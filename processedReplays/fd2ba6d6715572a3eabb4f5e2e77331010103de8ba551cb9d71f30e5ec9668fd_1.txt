----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3491
  player_apm: 114
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4397
  player_apm: 235
}
game_duration_loops: 15619
game_duration_seconds: 697.325439453
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10989
score_details {
  idle_production_time: 1681.3125
  idle_worker_time: 616.9375
  total_value_units: 4900.0
  total_value_structures: 4350.0
  killed_value_units: 850.0
  killed_value_structures: 0.0
  collected_minerals: 8190.0
  collected_vespene: 2276.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 694.0
  spent_minerals: 7825.0
  spent_vespene: 1975.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: -25.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1800.0
    economy: 5050.0
    technology: 1400.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 1075.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 1550.0
    economy: 5400.0
    technology: 1400.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 950.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 974.658691406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 901.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 149.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 384
vespene: 280
food_cap: 125
food_used: 82
food_army: 35
food_workers: 47
idle_worker_count: 6
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12229
score_details {
  idle_production_time: 5731.9375
  idle_worker_time: 2050.1875
  total_value_units: 9625.0
  total_value_structures: 6350.0
  killed_value_units: 4400.0
  killed_value_structures: 0.0
  collected_minerals: 15895.0
  collected_vespene: 5136.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 492.0
  spent_minerals: 13875.0
  spent_vespene: 3075.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3450.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4881.0
    economy: 2525.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1796.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 200.0
    economy: 4000.0
    technology: 2100.0
    upgrade: 500.0
  }
  used_vespene {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 750.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 50.0
    army: 4850.0
    economy: 7050.0
    technology: 2350.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 50.0
    army: 1775.0
    economy: 0.0
    technology: 750.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 5044.84472656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13053.96875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 533.773681641
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2039
vespene: 2040
food_cap: 110
food_used: 35
food_army: 4
food_workers: 31
idle_worker_count: 6
army_count: 0
warp_gate_count: 0

game_loop:  15619
ui_data{
 
Score:  12229
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
