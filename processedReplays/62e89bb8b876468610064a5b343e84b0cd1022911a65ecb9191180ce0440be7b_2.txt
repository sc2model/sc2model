----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2975
  player_apm: 68
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2998
  player_apm: 173
}
game_duration_loops: 6208
game_duration_seconds: 277.162200928
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5702
score_details {
  idle_production_time: 2542.625
  idle_worker_time: 1.75
  total_value_units: 3400.0
  total_value_structures: 850.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 4295.0
  collected_vespene: 588.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 313.0
  spent_minerals: 3856.0
  spent_vespene: 200.0
  food_used {
    none: 0.0
    army: 11.5
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 175.0
    economy: -69.0
    technology: -125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 775.0
    economy: 3400.0
    technology: 250.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 650.0
    economy: 3600.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 95.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 361.529296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 489
vespene: 388
food_cap: 92
food_used: 42
food_army: 11
food_workers: 31
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  6208
ui_data{
 
Score:  5702
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
