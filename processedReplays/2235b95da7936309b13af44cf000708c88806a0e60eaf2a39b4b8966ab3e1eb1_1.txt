----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3912
  player_apm: 132
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 207
}
game_duration_loops: 13991
game_duration_seconds: 624.641784668
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10084
score_details {
  idle_production_time: 1828.3125
  idle_worker_time: 253.1875
  total_value_units: 5925.0
  total_value_structures: 4325.0
  killed_value_units: 3950.0
  killed_value_structures: 0.0
  collected_minerals: 9700.0
  collected_vespene: 1684.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 447.0
  spent_minerals: 9075.0
  spent_vespene: 1075.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1525.0
    economy: 1500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1725.0
    economy: 3925.0
    technology: 2000.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3300.0
    economy: 3925.0
    technology: 2300.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 5381.64111328
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3075.0
    shields: 3146.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 675
vespene: 609
food_cap: 94
food_used: 72
food_army: 30
food_workers: 42
idle_worker_count: 1
army_count: 13
warp_gate_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 14723
score_details {
  idle_production_time: 2976.8125
  idle_worker_time: 502.625
  total_value_units: 9625.0
  total_value_structures: 5250.0
  killed_value_units: 7500.0
  killed_value_structures: 1000.0
  collected_minerals: 14475.0
  collected_vespene: 3148.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 492.0
  spent_minerals: 14200.0
  spent_vespene: 2275.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3325.0
    economy: 2950.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2900.0
    economy: 5125.0
    technology: 2900.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 200.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 6025.0
    economy: 4650.0
    technology: 2750.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 14861.4550781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4239.75
    shields: 4265.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 325
vespene: 873
food_cap: 126
food_used: 101
food_army: 54
food_workers: 47
idle_worker_count: 1
army_count: 26
warp_gate_count: 9

game_loop:  13991
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 23
}
groups {
  control_group_index: 2
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}

Score:  14723
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
