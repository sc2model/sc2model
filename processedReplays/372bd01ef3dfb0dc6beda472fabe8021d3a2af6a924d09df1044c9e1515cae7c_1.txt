----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5098
  player_apm: 202
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4781
  player_apm: 156
}
game_duration_loops: 8504
game_duration_seconds: 379.669342041
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4758
score_details {
  idle_production_time: 6410.1875
  idle_worker_time: 138.625
  total_value_units: 7200.0
  total_value_structures: 1600.0
  killed_value_units: 1450.0
  killed_value_structures: 300.0
  collected_minerals: 8025.0
  collected_vespene: 1008.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 179.0
  spent_minerals: 7975.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 8.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2875.0
    economy: 1400.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 2675.0
    technology: 700.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3700.0
    economy: 4675.0
    technology: 975.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2177.125
    shields: 2122.625
    energy: 0.0
  }
  total_damage_taken {
    life: 7387.23535156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 100
vespene: 83
food_cap: 98
food_used: 21
food_army: 13
food_workers: 8
idle_worker_count: 8
army_count: 4
warp_gate_count: 0

game_loop:  8504
ui_data{
 
Score:  4758
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
