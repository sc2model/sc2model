----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5081
  player_apm: 278
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5154
  player_apm: 292
}
game_duration_loops: 32258
game_duration_seconds: 1440.18981934
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10259
score_details {
  idle_production_time: 787.125
  idle_worker_time: 298.0625
  total_value_units: 6200.0
  total_value_structures: 3550.0
  killed_value_units: 1600.0
  killed_value_structures: 0.0
  collected_minerals: 8860.0
  collected_vespene: 2064.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 627.0
  spent_minerals: 8525.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 975.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 800.0
    technology: 140.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2300.0
    economy: 4000.0
    technology: 1125.0
    upgrade: 300.0
  }
  used_vespene {
    none: 100.0
    army: 875.0
    economy: 0.0
    technology: 325.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 3000.0
    economy: 4800.0
    technology: 1225.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 900.0
    economy: 0.0
    technology: 325.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2848.85791016
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3701.45703125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 985.565429688
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 345
vespene: 439
food_cap: 86
food_used: 79
food_army: 45
food_workers: 32
idle_worker_count: 0
army_count: 30
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 23
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
single {
  unit {
    unit_type: 49
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18978
score_details {
  idle_production_time: 4262.4375
  idle_worker_time: 2006.6875
  total_value_units: 16850.0
  total_value_structures: 6500.0
  killed_value_units: 8850.0
  killed_value_structures: 475.0
  collected_minerals: 20940.0
  collected_vespene: 5688.0
  collection_rate_minerals: 643.0
  collection_rate_vespene: 537.0
  spent_minerals: 20500.0
  spent_vespene: 4475.0
  food_used {
    none: 0.0
    army: 117.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5550.0
    economy: 2250.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4334.0
    economy: 2850.0
    technology: 165.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 823.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5900.0
    economy: 5175.0
    technology: 1925.0
    upgrade: 725.0
  }
  used_vespene {
    none: 100.0
    army: 2250.0
    economy: 0.0
    technology: 600.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 100.0
    army: 9800.0
    economy: 8625.0
    technology: 2125.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 100.0
    army: 2900.0
    economy: 0.0
    technology: 600.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 16928.9589844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13591.7412109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5162.35546875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 352
vespene: 1126
food_cap: 173
food_used: 148
food_army: 117
food_workers: 28
idle_worker_count: 6
army_count: 73
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 43
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 113
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1245
    shields: 0
    energy: 37
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1140
    shields: 0
    energy: 45
  }
}

score{
 score_type: Melee
score: 19441
score_details {
  idle_production_time: 11182.5
  idle_worker_time: 3921.0
  total_value_units: 24275.0
  total_value_structures: 6800.0
  killed_value_units: 29300.0
  killed_value_structures: 2175.0
  collected_minerals: 27305.0
  collected_vespene: 8336.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 179.0
  spent_minerals: 26875.0
  spent_vespene: 5975.0
  food_used {
    none: 0.0
    army: 90.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15975.0
    economy: 7425.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10610.0
    economy: 3550.0
    technology: 165.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1999.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 4550.0
    economy: 5225.0
    technology: 1925.0
    upgrade: 1150.0
  }
  used_vespene {
    none: 100.0
    army: 2025.0
    economy: 0.0
    technology: 600.0
    upgrade: 1150.0
  }
  total_used_minerals {
    none: 100.0
    army: 15400.0
    economy: 9525.0
    technology: 2125.0
    upgrade: 1150.0
  }
  total_used_vespene {
    none: 100.0
    army: 4125.0
    economy: 0.0
    technology: 600.0
    upgrade: 1150.0
  }
  total_damage_dealt {
    life: 44395.1992188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23914.7480469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 12084.1386719
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 342
vespene: 2274
food_cap: 197
food_used: 116
food_army: 90
food_workers: 26
idle_worker_count: 3
army_count: 62
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 51
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 19541
score_details {
  idle_production_time: 12991.875
  idle_worker_time: 4425.125
  total_value_units: 25200.0
  total_value_structures: 6800.0
  killed_value_units: 32325.0
  killed_value_structures: 3825.0
  collected_minerals: 27835.0
  collected_vespene: 8556.0
  collection_rate_minerals: 531.0
  collection_rate_vespene: 0.0
  spent_minerals: 27575.0
  spent_vespene: 6200.0
  food_used {
    none: 0.0
    army: 97.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 16200.0
    economy: 10875.0
    technology: 700.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 7675.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10810.0
    economy: 4050.0
    technology: 265.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2149.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 400.0
    army: 4900.0
    economy: 4875.0
    technology: 1825.0
    upgrade: 1150.0
  }
  used_vespene {
    none: 100.0
    army: 2100.0
    economy: 0.0
    technology: 600.0
    upgrade: 1150.0
  }
  total_used_minerals {
    none: 100.0
    army: 15950.0
    economy: 9675.0
    technology: 2125.0
    upgrade: 1150.0
  }
  total_used_vespene {
    none: 100.0
    army: 4350.0
    economy: 0.0
    technology: 600.0
    upgrade: 1150.0
  }
  total_damage_dealt {
    life: 56942.2851562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24943.8984375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 12928.0585938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 172
vespene: 2269
food_cap: 197
food_used: 116
food_army: 97
food_workers: 19
idle_worker_count: 2
army_count: 68
warp_gate_count: 0

game_loop:  32258
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 51
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 157
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 40
    shields: 0
    energy: 169
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 119
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 46
    shields: 0
    energy: 200
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 92
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 158
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 90
    shields: 0
    energy: 190
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 49
    shields: 0
    energy: 112
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 96
    shields: 0
    energy: 69
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 137
    shields: 0
    energy: 67
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 169
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

Score:  19541
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
