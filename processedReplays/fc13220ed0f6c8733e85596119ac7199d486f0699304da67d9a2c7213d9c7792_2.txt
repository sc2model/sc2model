----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3040
  player_apm: 43
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2996
  player_apm: 71
}
game_duration_loops: 18705
game_duration_seconds: 835.102905273
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12367
score_details {
  idle_production_time: 1477.0625
  idle_worker_time: 285.0
  total_value_units: 6025.0
  total_value_structures: 4200.0
  killed_value_units: 550.0
  killed_value_structures: 0.0
  collected_minerals: 8960.0
  collected_vespene: 2432.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 649.0
  spent_minerals: 8975.0
  spent_vespene: 1950.0
  food_used {
    none: 0.0
    army: 50.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3025.0
    economy: 4825.0
    technology: 1900.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2525.0
    economy: 4350.0
    technology: 1750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 235.0
    shields: 385.0
    energy: 0.0
  }
  total_damage_taken {
    life: 50.0
    shields: 60.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 35
vespene: 482
food_cap: 110
food_used: 95
food_army: 50
food_workers: 45
idle_worker_count: 1
army_count: 16
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 495
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 25029
score_details {
  idle_production_time: 4896.9375
  idle_worker_time: 607.25
  total_value_units: 17400.0
  total_value_structures: 8850.0
  killed_value_units: 11625.0
  killed_value_structures: 1075.0
  collected_minerals: 22735.0
  collected_vespene: 7744.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 783.0
  spent_minerals: 21850.0
  spent_vespene: 7300.0
  food_used {
    none: 0.0
    army: 116.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6100.0
    economy: 2375.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6950.0
    economy: 7050.0
    technology: 3600.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 10125.0
    economy: 7050.0
    technology: 3600.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 4325.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 6365.0
    shields: 7891.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3238.25
    shields: 3823.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 935
vespene: 444
food_cap: 200
food_used: 175
food_army: 116
food_workers: 59
idle_worker_count: 5
army_count: 33
warp_gate_count: 8

game_loop:  18705
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 22
}
groups {
  control_group_index: 2
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 67
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 83
    player_relative: 1
    health: 106
    shields: 41
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 63
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 72
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 102
    shields: 0
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 102
    shields: 49
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 33
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 66
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 58
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 12
    shields: 25
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 68
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  25029
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
