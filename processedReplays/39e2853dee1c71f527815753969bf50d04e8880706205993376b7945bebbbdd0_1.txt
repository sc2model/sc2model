----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3533
  player_apm: 305
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3478
  player_apm: 184
}
game_duration_loops: 32243
game_duration_seconds: 1439.5201416
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11238
score_details {
  idle_production_time: 14588.375
  idle_worker_time: 2.1875
  total_value_units: 7900.0
  total_value_structures: 1725.0
  killed_value_units: 750.0
  killed_value_structures: 100.0
  collected_minerals: 10115.0
  collected_vespene: 1172.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 313.0
  spent_minerals: 8974.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 39.5
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 31.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 75.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2350.0
    economy: 4900.0
    technology: 1300.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 3550.0
    economy: 5700.0
    technology: 1025.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 50.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1363.91064453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1666.15429688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1191
vespene: 272
food_cap: 138
food_used: 80
food_army: 39
food_workers: 41
idle_worker_count: 0
army_count: 63
warp_gate_count: 0
larva_count: 1

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
    add_on {
      unit_type: 100
      build_progress: 0.0554687380791
    }
  }
}

score{
 score_type: Melee
score: 12775
score_details {
  idle_production_time: 48143.8125
  idle_worker_time: 912.0625
  total_value_units: 22500.0
  total_value_structures: 3225.0
  killed_value_units: 8525.0
  killed_value_structures: 100.0
  collected_minerals: 23485.0
  collected_vespene: 5188.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 156.0
  spent_minerals: 23086.0
  spent_vespene: 4537.0
  food_used {
    none: 0.0
    army: 17.5
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7575.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11312.0
    economy: 1081.0
    technology: 18.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2137.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1650.0
    economy: 75.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 1125.0
    economy: 6000.0
    technology: 2050.0
    upgrade: 1075.0
  }
  used_vespene {
    none: -50.0
    army: 150.0
    economy: 0.0
    technology: 350.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 15950.0
    economy: 8350.0
    technology: 2325.0
    upgrade: 1075.0
  }
  total_used_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 450.0
    upgrade: 1075.0
  }
  total_damage_dealt {
    life: 9838.78027344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20546.4414062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 449
vespene: 651
food_cap: 136
food_used: 70
food_army: 17
food_workers: 53
idle_worker_count: 4
army_count: 14
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21541
score_details {
  idle_production_time: 93524.75
  idle_worker_time: 2181.1875
  total_value_units: 34600.0
  total_value_structures: 3950.0
  killed_value_units: 14175.0
  killed_value_structures: 900.0
  collected_minerals: 34360.0
  collected_vespene: 9004.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 940.0
  spent_minerals: 34111.0
  spent_vespene: 8337.0
  food_used {
    none: 0.0
    army: 94.5
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11050.0
    economy: 2850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14312.0
    economy: 1981.0
    technology: 143.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2537.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2800.0
    economy: 75.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 5775.0
    economy: 7150.0
    technology: 2050.0
    upgrade: 1350.0
  }
  used_vespene {
    none: -50.0
    army: 2700.0
    economy: 0.0
    technology: 350.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 0.0
    army: 25500.0
    economy: 10650.0
    technology: 2450.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 6175.0
    economy: 0.0
    technology: 450.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 17866.1308594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27638.3867188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 299
vespene: 667
food_cap: 200
food_used: 139
food_army: 94
food_workers: 45
idle_worker_count: 0
army_count: 116
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 185
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 185
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 1
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17658
score_details {
  idle_production_time: 110385.0625
  idle_worker_time: 2292.6875
  total_value_units: 38100.0
  total_value_structures: 3950.0
  killed_value_units: 17550.0
  killed_value_structures: 6250.0
  collected_minerals: 36550.0
  collected_vespene: 10416.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 425.0
  spent_minerals: 36316.0
  spent_vespene: 8692.0
  food_used {
    none: 0.0
    army: 71.5
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 14000.0
    economy: 6200.0
    technology: 1600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16867.0
    economy: 3531.0
    technology: 268.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4467.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 3450.0
    economy: 75.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -350.0
    army: 4150.0
    economy: 6250.0
    technology: 1925.0
    upgrade: 1350.0
  }
  used_vespene {
    none: -175.0
    army: 800.0
    economy: 0.0
    technology: 350.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 0.0
    army: 27550.0
    economy: 11300.0
    technology: 2450.0
    upgrade: 1350.0
  }
  total_used_vespene {
    none: 0.0
    army: 7000.0
    economy: 0.0
    technology: 450.0
    upgrade: 1350.0
  }
  total_damage_dealt {
    life: 44213.0351562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 36125.2265625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 284
vespene: 1724
food_cap: 158
food_used: 115
food_army: 71
food_workers: 44
idle_worker_count: 23
army_count: 114
warp_gate_count: 0

game_loop:  32243
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  17658
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
