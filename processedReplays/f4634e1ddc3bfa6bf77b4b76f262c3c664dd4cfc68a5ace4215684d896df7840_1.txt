----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3697
  player_apm: 177
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3662
  player_apm: 54
}
game_duration_loops: 21237
game_duration_seconds: 948.14654541
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13861
score_details {
  idle_production_time: 7554.125
  idle_worker_time: 0.3125
  total_value_units: 7400.0
  total_value_structures: 1850.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 10645.0
  collected_vespene: 2516.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 985.0
  spent_minerals: 8550.0
  spent_vespene: 1050.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 225.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 5850.0
    technology: 1100.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 6750.0
    technology: 1250.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 81.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 519.682128906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2145
vespene: 1466
food_cap: 106
food_used: 89
food_army: 26
food_workers: 63
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 24
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20902
score_details {
  idle_production_time: 37904.375
  idle_worker_time: 60.6875
  total_value_units: 25900.0
  total_value_structures: 2800.0
  killed_value_units: 8275.0
  killed_value_structures: 4700.0
  collected_minerals: 26530.0
  collected_vespene: 9872.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 492.0
  spent_minerals: 23875.0
  spent_vespene: 7575.0
  food_used {
    none: 0.0
    army: 94.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3150.0
    economy: 6000.0
    technology: 1025.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2175.0
    economy: 450.0
    technology: 175.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 300.0
    army: 8675.0
    economy: 1775.0
    technology: 1375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 250.0
    army: 3525.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 4900.0
    economy: 6525.0
    technology: 225.0
    upgrade: 1125.0
  }
  used_vespene {
    none: -100.0
    army: 2050.0
    economy: 100.0
    technology: 100.0
    upgrade: 1125.0
  }
  total_used_minerals {
    none: 300.0
    army: 14050.0
    economy: 9100.0
    technology: 1950.0
    upgrade: 1125.0
  }
  total_used_vespene {
    none: 300.0
    army: 5750.0
    economy: 200.0
    technology: 750.0
    upgrade: 1125.0
  }
  total_damage_dealt {
    life: 25922.3476562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19898.1582031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2705
vespene: 2297
food_cap: 152
food_used: 136
food_army: 94
food_workers: 42
idle_worker_count: 1
army_count: 41
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 107
  count: 33
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 83
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19801
score_details {
  idle_production_time: 45182.875
  idle_worker_time: 138.0
  total_value_units: 25900.0
  total_value_structures: 2800.0
  killed_value_units: 11625.0
  killed_value_structures: 8275.0
  collected_minerals: 27291.0
  collected_vespene: 10160.0
  collection_rate_minerals: 1607.0
  collection_rate_vespene: 246.0
  spent_minerals: 24100.0
  spent_vespene: 7575.0
  food_used {
    none: 0.0
    army: 72.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4750.0
    economy: 8150.0
    technology: 2275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3375.0
    economy: 450.0
    technology: 900.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 300.0
    army: 9925.0
    economy: 2275.0
    technology: 1375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 250.0
    army: 3925.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 3650.0
    economy: 6000.0
    technology: 475.0
    upgrade: 1125.0
  }
  used_vespene {
    none: -100.0
    army: 1650.0
    economy: 100.0
    technology: 100.0
    upgrade: 1125.0
  }
  total_used_minerals {
    none: 300.0
    army: 14050.0
    economy: 9100.0
    technology: 1950.0
    upgrade: 1125.0
  }
  total_used_vespene {
    none: 300.0
    army: 5750.0
    economy: 200.0
    technology: 750.0
    upgrade: 1125.0
  }
  total_damage_dealt {
    life: 40903.3476562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22629.3964844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3241
vespene: 2585
food_cap: 144
food_used: 107
food_army: 72
food_workers: 35
idle_worker_count: 1
army_count: 33
warp_gate_count: 0

game_loop:  21237
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 107
  count: 31
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}

Score:  19801
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
