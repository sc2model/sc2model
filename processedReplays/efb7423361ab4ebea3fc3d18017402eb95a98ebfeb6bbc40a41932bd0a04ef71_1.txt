----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3700
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3718
  player_apm: 200
}
game_duration_loops: 20051
game_duration_seconds: 895.196411133
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7466
score_details {
  idle_production_time: 905.375
  idle_worker_time: 530.625
  total_value_units: 4600.0
  total_value_structures: 2850.0
  killed_value_units: 2300.0
  killed_value_structures: 0.0
  collected_minerals: 6780.0
  collected_vespene: 1820.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 313.0
  spent_minerals: 6275.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1500.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 866.0
    economy: 500.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 709.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1150.0
    economy: 3100.0
    technology: 1225.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1600.0
    economy: 3900.0
    technology: 1375.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 225.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2181.85571289
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3587.8125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 472.5625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 483
vespene: 108
food_cap: 54
food_used: 52
food_army: 21
food_workers: 31
idle_worker_count: 4
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
single {
  unit {
    unit_type: 35
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18392
score_details {
  idle_production_time: 5930.625
  idle_worker_time: 1607.625
  total_value_units: 12800.0
  total_value_structures: 6000.0
  killed_value_units: 9900.0
  killed_value_structures: 875.0
  collected_minerals: 18650.0
  collected_vespene: 5764.0
  collection_rate_minerals: 3135.0
  collection_rate_vespene: 380.0
  spent_minerals: 15975.0
  spent_vespene: 4075.0
  food_used {
    none: 0.0
    army: 66.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6650.0
    economy: 1100.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4452.0
    economy: 550.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1674.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 3500.0
    economy: 5300.0
    technology: 2300.0
    upgrade: 675.0
  }
  used_vespene {
    none: 50.0
    army: 1000.0
    economy: 0.0
    technology: 775.0
    upgrade: 675.0
  }
  total_used_minerals {
    none: 50.0
    army: 7500.0
    economy: 6300.0
    technology: 2600.0
    upgrade: 675.0
  }
  total_used_vespene {
    none: 50.0
    army: 2400.0
    economy: 0.0
    technology: 800.0
    upgrade: 675.0
  }
  total_damage_dealt {
    life: 13260.8232422
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9850.75390625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3594.42675781
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2313
vespene: 1504
food_cap: 149
food_used: 113
food_army: 66
food_workers: 47
idle_worker_count: 4
army_count: 50
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 32
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 32
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 37
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 178
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 177
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18474
score_details {
  idle_production_time: 5987.625
  idle_worker_time: 1620.375
  total_value_units: 13250.0
  total_value_structures: 6000.0
  killed_value_units: 9900.0
  killed_value_structures: 875.0
  collected_minerals: 18720.0
  collected_vespene: 5776.0
  collection_rate_minerals: 3191.0
  collection_rate_vespene: 358.0
  spent_minerals: 15975.0
  spent_vespene: 4075.0
  food_used {
    none: 0.0
    army: 66.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6650.0
    economy: 1100.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4452.0
    economy: 550.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1674.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 3500.0
    economy: 5300.0
    technology: 2300.0
    upgrade: 675.0
  }
  used_vespene {
    none: 50.0
    army: 1000.0
    economy: 0.0
    technology: 775.0
    upgrade: 675.0
  }
  total_used_minerals {
    none: 50.0
    army: 7800.0
    economy: 6300.0
    technology: 2600.0
    upgrade: 675.0
  }
  total_used_vespene {
    none: 50.0
    army: 2550.0
    economy: 0.0
    technology: 800.0
    upgrade: 675.0
  }
  total_damage_dealt {
    life: 13260.8232422
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9850.75390625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3635.45361328
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2383
vespene: 1516
food_cap: 149
food_used: 113
food_army: 66
food_workers: 47
idle_worker_count: 4
army_count: 52
warp_gate_count: 0

game_loop:  20051
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 33
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}

Score:  18474
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
