----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4852
  player_apm: 173
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4718
  player_apm: 177
}
game_duration_loops: 17761
game_duration_seconds: 792.957092285
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10586
score_details {
  idle_production_time: 1484.6875
  idle_worker_time: 712.125
  total_value_units: 4475.0
  total_value_structures: 3825.0
  killed_value_units: 1000.0
  killed_value_structures: 0.0
  collected_minerals: 8425.0
  collected_vespene: 1936.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 627.0
  spent_minerals: 8075.0
  spent_vespene: 1325.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1000.0
    economy: 4700.0
    technology: 2075.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 600.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1250.0
    economy: 5800.0
    technology: 1475.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 260.0
    shields: 423.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1083.37207031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 163.372070312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 400
vespene: 611
food_cap: 85
food_used: 65
food_army: 20
food_workers: 45
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 2
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10465
score_details {
  idle_production_time: 7570.375
  idle_worker_time: 2015.8125
  total_value_units: 12325.0
  total_value_structures: 6450.0
  killed_value_units: 15050.0
  killed_value_structures: 0.0
  collected_minerals: 16690.0
  collected_vespene: 4772.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 134.0
  spent_minerals: 16375.0
  spent_vespene: 3950.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5150.0
    economy: 4175.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 600.0
    economy: 3725.0
    technology: 2250.0
    upgrade: 775.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 775.0
    upgrade: 775.0
  }
  total_used_minerals {
    none: 0.0
    army: 5650.0
    economy: 8150.0
    technology: 2750.0
    upgrade: 775.0
  }
  total_used_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 850.0
    upgrade: 775.0
  }
  total_damage_dealt {
    life: 6188.0
    shields: 8157.25
    energy: 0.0
  }
  total_damage_taken {
    life: 17737.3457031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4867.44335938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 243
vespene: 822
food_cap: 117
food_used: 31
food_army: 12
food_workers: 19
idle_worker_count: 22
army_count: 3
warp_gate_count: 0

game_loop:  17761
ui_data{
 
Score:  10465
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
