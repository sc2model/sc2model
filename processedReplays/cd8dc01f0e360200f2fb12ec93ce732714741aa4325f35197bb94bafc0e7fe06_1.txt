----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3451
  player_apm: 151
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3423
  player_apm: 104
}
game_duration_loops: 18921
game_duration_seconds: 844.746459961
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9188
score_details {
  idle_production_time: 8775.1875
  idle_worker_time: 119.4375
  total_value_units: 6700.0
  total_value_structures: 1800.0
  killed_value_units: 675.0
  killed_value_structures: 0.0
  collected_minerals: 8050.0
  collected_vespene: 1088.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 447.0
  spent_minerals: 7875.0
  spent_vespene: 850.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1475.0
    economy: 5050.0
    technology: 1325.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 150.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 5525.0
    technology: 1325.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1609.34545898
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1138.3984375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 225
vespene: 238
food_cap: 82
food_used: 81
food_army: 28
food_workers: 51
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13108
score_details {
  idle_production_time: 17499.5625
  idle_worker_time: 341.0
  total_value_units: 17900.0
  total_value_structures: 2725.0
  killed_value_units: 8375.0
  killed_value_structures: 0.0
  collected_minerals: 17730.0
  collected_vespene: 5728.0
  collection_rate_minerals: 643.0
  collection_rate_vespene: 649.0
  spent_minerals: 17625.0
  spent_vespene: 5550.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5675.0
    economy: 1350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5725.0
    economy: 2000.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2625.0
    economy: 5050.0
    technology: 1875.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1975.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 8975.0
    economy: 7500.0
    technology: 1875.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 4875.0
    economy: 0.0
    technology: 450.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 11923.5644531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16493.1757812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 155
vespene: 178
food_cap: 124
food_used: 90
food_army: 54
food_workers: 36
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  18921
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

Score:  13108
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
