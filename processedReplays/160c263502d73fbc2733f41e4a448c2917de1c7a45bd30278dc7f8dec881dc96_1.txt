----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2652
  player_apm: 39
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2306
  player_apm: 37
}
game_duration_loops: 23221
game_duration_seconds: 1036.72412109
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9427
score_details {
  idle_production_time: 2256.9375
  idle_worker_time: 1457.0625
  total_value_units: 3675.0
  total_value_structures: 3050.0
  killed_value_units: 50.0
  killed_value_structures: 400.0
  collected_minerals: 6035.0
  collected_vespene: 2592.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 627.0
  spent_minerals: 5900.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 201.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1350.0
    economy: 4250.0
    technology: 1050.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 625.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 4300.0
    technology: 850.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 520.0
    shields: 568.25
    energy: 0.0
  }
  total_damage_taken {
    life: 1338.83862305
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 748.799560547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 135
vespene: 942
food_cap: 70
food_used: 60
food_army: 21
food_workers: 38
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 30
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 30348
score_details {
  idle_production_time: 13271.8125
  idle_worker_time: 2290.5625
  total_value_units: 16425.0
  total_value_structures: 7775.0
  killed_value_units: 50.0
  killed_value_structures: 400.0
  collected_minerals: 20040.0
  collected_vespene: 9808.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 1299.0
  spent_minerals: 19200.0
  spent_vespene: 9000.0
  food_used {
    none: 0.0
    army: 143.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 201.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 9450.0
    economy: 7450.0
    technology: 2175.0
    upgrade: 525.0
  }
  used_vespene {
    none: 0.0
    army: 7225.0
    economy: 0.0
    technology: 1250.0
    upgrade: 525.0
  }
  total_used_minerals {
    none: 0.0
    army: 7850.0
    economy: 8400.0
    technology: 2175.0
    upgrade: 525.0
  }
  total_used_vespene {
    none: 0.0
    army: 6025.0
    economy: 0.0
    technology: 1250.0
    upgrade: 525.0
  }
  total_damage_dealt {
    life: 520.0
    shields: 568.25
    energy: 0.0
  }
  total_damage_taken {
    life: 1338.83862305
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 748.799560547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 840
vespene: 808
food_cap: 195
food_used: 192
food_army: 143
food_workers: 49
idle_worker_count: 3
army_count: 21
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 57
  count: 19
}
multi {
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 34925
score_details {
  idle_production_time: 13271.8125
  idle_worker_time: 2663.5625
  total_value_units: 19925.0
  total_value_structures: 9125.0
  killed_value_units: 3500.0
  killed_value_structures: 2450.0
  collected_minerals: 23685.0
  collected_vespene: 13040.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 1388.0
  spent_minerals: 21150.0
  spent_vespene: 9600.0
  food_used {
    none: 0.0
    army: 149.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1925.0
    economy: 300.0
    technology: 2100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 326.0
    technology: 950.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 625.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 9850.0
    economy: 8175.0
    technology: 1725.0
    upgrade: 525.0
  }
  used_vespene {
    none: 0.0
    army: 7525.0
    economy: 0.0
    technology: 625.0
    upgrade: 525.0
  }
  total_used_minerals {
    none: 0.0
    army: 9850.0
    economy: 9250.0
    technology: 2675.0
    upgrade: 525.0
  }
  total_used_vespene {
    none: 0.0
    army: 7525.0
    economy: 0.0
    technology: 1250.0
    upgrade: 525.0
  }
  total_damage_dealt {
    life: 5124.0
    shields: 6251.375
    energy: 0.0
  }
  total_damage_taken {
    life: 11412.8388672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 748.799560547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2535
vespene: 3440
food_cap: 200
food_used: 197
food_army: 149
food_workers: 48
idle_worker_count: 2
army_count: 25
warp_gate_count: 0

game_loop:  23221
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 57
  count: 21
}
multi {
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 409
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 319
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 505
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 490
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 475
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 520
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
}

Score:  34925
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
