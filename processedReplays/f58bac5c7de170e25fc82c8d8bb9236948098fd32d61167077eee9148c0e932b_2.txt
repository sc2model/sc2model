----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2780
  player_apm: 76
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 127
}
game_duration_loops: 17397
game_duration_seconds: 776.705993652
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10353
score_details {
  idle_production_time: 1700.4375
  idle_worker_time: 110.5
  total_value_units: 3825.0
  total_value_structures: 4650.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 7475.0
  collected_vespene: 1828.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 470.0
  spent_minerals: 6975.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1175.0
    economy: 3750.0
    technology: 2750.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 800.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1175.0
    economy: 3750.0
    technology: 2450.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 500.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 180.0
    shields: 145.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 30.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 550
vespene: 128
food_cap: 78
food_used: 63
food_army: 22
food_workers: 41
idle_worker_count: 0
army_count: 11
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 30
    energy: 33
  }
}

score{
 score_type: Melee
score: 8857
score_details {
  idle_production_time: 5631.75
  idle_worker_time: 182.5
  total_value_units: 9700.0
  total_value_structures: 6250.0
  killed_value_units: 6175.0
  killed_value_structures: 0.0
  collected_minerals: 12335.0
  collected_vespene: 4172.0
  collection_rate_minerals: 363.0
  collection_rate_vespene: 335.0
  spent_minerals: 11950.0
  spent_vespene: 3850.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 16.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4000.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3125.0
    economy: 2450.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 875.0
    economy: 2750.0
    technology: 2450.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 800.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 5250.0
    economy: 5100.0
    technology: 2750.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 3850.0
    economy: 0.0
    technology: 800.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 2708.0
    shields: 3587.375
    energy: 0.0
  }
  total_damage_taken {
    life: 5426.0
    shields: 7075.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 435
vespene: 322
food_cap: 93
food_used: 31
food_army: 15
food_workers: 14
idle_worker_count: 0
army_count: 5
warp_gate_count: 4

game_loop:  17397
ui_data{
 multi {
  units {
    unit_type: 80
    player_relative: 1
    health: 64
    shields: 0
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 98
    shields: 0
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 104
    shields: 0
    energy: 0
  }
}

Score:  8857
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
