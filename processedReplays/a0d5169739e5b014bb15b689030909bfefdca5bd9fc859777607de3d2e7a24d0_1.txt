----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4507
  player_apm: 251
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4429
  player_apm: 240
}
game_duration_loops: 17182
game_duration_seconds: 767.107116699
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13172
score_details {
  idle_production_time: 6599.75
  idle_worker_time: 0.0
  total_value_units: 7800.0
  total_value_structures: 1800.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 10210.0
  collected_vespene: 2112.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 649.0
  spent_minerals: 9650.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2575.0
    economy: 5750.0
    technology: 1250.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 150.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 6200.0
    technology: 1400.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 255.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1179.24023438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 610
vespene: 762
food_cap: 122
food_used: 99
food_army: 44
food_workers: 55
idle_worker_count: 0
army_count: 43
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 43
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 7
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 25
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 25
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 40
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 60
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 15
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 32
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 31
  }
}

score{
 score_type: Melee
score: 15900
score_details {
  idle_production_time: 32561.4375
  idle_worker_time: 429.25
  total_value_units: 21000.0
  total_value_structures: 3700.0
  killed_value_units: 7900.0
  killed_value_structures: 0.0
  collected_minerals: 22630.0
  collected_vespene: 6956.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 985.0
  spent_minerals: 22543.0
  spent_vespene: 4018.0
  food_used {
    none: 0.0
    army: 21.5
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10068.0
    economy: 125.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2018.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1350.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 1250.0
    economy: 7200.0
    technology: 2400.0
    upgrade: 875.0
  }
  used_vespene {
    none: -75.0
    army: 0.0
    economy: 0.0
    technology: 450.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 13250.0
    economy: 8900.0
    technology: 2800.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 550.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 8486.33496094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19170.3789062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 137
vespene: 2938
food_cap: 174
food_used: 83
food_army: 21
food_workers: 62
idle_worker_count: 62
army_count: 1
warp_gate_count: 0

game_loop:  17182
ui_data{
 
Score:  15900
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
