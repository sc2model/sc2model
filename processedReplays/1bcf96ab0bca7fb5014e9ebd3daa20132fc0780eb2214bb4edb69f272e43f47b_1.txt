----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4585
  player_apm: 76
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4538
  player_apm: 193
}
game_duration_loops: 10827
game_duration_seconds: 483.381958008
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7118
score_details {
  idle_production_time: 1366.25
  idle_worker_time: 193.0
  total_value_units: 5800.0
  total_value_structures: 2500.0
  killed_value_units: 1825.0
  killed_value_structures: 200.0
  collected_minerals: 6430.0
  collected_vespene: 1832.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 6025.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 11.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 50.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 478.0
    economy: 950.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 215.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2500.0
    economy: 1950.0
    technology: 675.0
    upgrade: 0.0
  }
  used_vespene {
    none: 50.0
    army: 1250.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 2900.0
    economy: 3000.0
    technology: 925.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 1450.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2254.31152344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3341.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 243.042236328
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 375
vespene: 68
food_cap: 71
food_used: 53
food_army: 42
food_workers: 10
idle_worker_count: 5
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 77
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 2888
score_details {
  idle_production_time: 1510.1875
  idle_worker_time: 512.375
  total_value_units: 6000.0
  total_value_structures: 2500.0
  killed_value_units: 4025.0
  killed_value_structures: 300.0
  collected_minerals: 6460.0
  collected_vespene: 1832.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 5925.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 10.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2350.0
    economy: 1050.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 2386.0
    economy: 1505.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 1232.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 1350.0
    technology: 525.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 3000.0
    economy: 3100.0
    technology: 925.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 1450.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4225.31152344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7832.94140625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 557.585693359
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 406
vespene: 7
food_cap: 56
food_used: 17
food_army: 7
food_workers: 10
idle_worker_count: 10
army_count: 5
warp_gate_count: 0

game_loop:  10827
ui_data{
 
Score:  2888
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
