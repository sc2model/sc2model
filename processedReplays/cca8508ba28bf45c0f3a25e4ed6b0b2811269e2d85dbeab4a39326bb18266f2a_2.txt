----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4632
  player_apm: 249
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4530
  player_apm: 128
}
game_duration_loops: 8713
game_duration_seconds: 389.000366211
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8528
score_details {
  idle_production_time: 1118.6875
  idle_worker_time: 40.625
  total_value_units: 4150.0
  total_value_structures: 4050.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 7850.0
  collected_vespene: 1528.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 515.0
  spent_minerals: 7150.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1350.0
    economy: 3350.0
    technology: 1200.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 650.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 1600.0
    economy: 5100.0
    technology: 1200.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 450.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 250.5
    shields: 1005.375
    energy: 0.0
  }
  total_damage_taken {
    life: 2679.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 40.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 750
vespene: 178
food_cap: 101
food_used: 44
food_army: 27
food_workers: 17
idle_worker_count: 0
army_count: 17
warp_gate_count: 0

game_loop:  8713
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 137
    shields: 0
    energy: 96
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 96
  }
}

Score:  8528
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
