----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: -36400
  player_apm: 137
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3669
  player_apm: 132
}
game_duration_loops: 23998
game_duration_seconds: 1071.4140625
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9635
score_details {
  idle_production_time: 1817.625
  idle_worker_time: 220.0
  total_value_units: 4400.0
  total_value_structures: 4225.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 7505.0
  collected_vespene: 1780.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 447.0
  spent_minerals: 7125.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4000.0
    technology: 1675.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 850.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1950.0
    economy: 4250.0
    technology: 1575.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 850.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1780.1875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 500.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 430
vespene: 330
food_cap: 86
food_used: 73
food_army: 35
food_workers: 36
idle_worker_count: 2
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 53
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1496
    shields: 0
    energy: 161
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1488
    shields: 0
    energy: 142
  }
}

score{
 score_type: Melee
score: 27032
score_details {
  idle_production_time: 7883.875
  idle_worker_time: 1813.0625
  total_value_units: 15500.0
  total_value_structures: 8250.0
  killed_value_units: 3600.0
  killed_value_structures: 200.0
  collected_minerals: 21705.0
  collected_vespene: 6952.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 918.0
  spent_minerals: 20050.0
  spent_vespene: 6300.0
  food_used {
    none: 0.0
    army: 132.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2575.0
    economy: 950.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 875.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 6800.0
    economy: 8550.0
    technology: 2275.0
    upgrade: 950.0
  }
  used_vespene {
    none: 50.0
    army: 3750.0
    economy: 150.0
    technology: 1150.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 50.0
    army: 7650.0
    economy: 9475.0
    technology: 2375.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 50.0
    army: 3900.0
    economy: 300.0
    technology: 1150.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 7228.21679688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4585.46337891
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 725.630615234
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1705
vespene: 652
food_cap: 200
food_used: 199
food_army: 132
food_workers: 67
idle_worker_count: 5
army_count: 48
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 1
  leader_unit_type: 484
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 29
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 6
}
multi {
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 119
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 200
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 109
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 155
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 194
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 200
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 47
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 51
  }
}

score{
 score_type: Melee
score: 27077
score_details {
  idle_production_time: 11146.8125
  idle_worker_time: 3157.25
  total_value_units: 19900.0
  total_value_structures: 8800.0
  killed_value_units: 16925.0
  killed_value_structures: 1325.0
  collected_minerals: 26615.0
  collected_vespene: 9612.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 851.0
  spent_minerals: 25100.0
  spent_vespene: 8100.0
  food_used {
    none: 0.0
    army: 103.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11050.0
    economy: 3550.0
    technology: 375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3175.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5800.0
    economy: 875.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5450.0
    economy: 8850.0
    technology: 2275.0
    upgrade: 1300.0
  }
  used_vespene {
    none: 50.0
    army: 3275.0
    economy: 300.0
    technology: 1150.0
    upgrade: 1300.0
  }
  total_used_minerals {
    none: 50.0
    army: 11250.0
    economy: 10325.0
    technology: 2375.0
    upgrade: 950.0
  }
  total_used_vespene {
    none: 50.0
    army: 4700.0
    economy: 600.0
    technology: 1150.0
    upgrade: 950.0
  }
  total_damage_dealt {
    life: 28099.53125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13333.546875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1464.22680664
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1565
vespene: 1512
food_cap: 200
food_used: 170
food_army: 103
food_workers: 67
idle_worker_count: 7
army_count: 32
warp_gate_count: 0

game_loop:  23998
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 29
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 6
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 152
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
}

Score:  27077
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
