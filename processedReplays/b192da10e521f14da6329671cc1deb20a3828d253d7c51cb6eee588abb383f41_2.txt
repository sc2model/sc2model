----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2966
  player_apm: 32
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 62
}
game_duration_loops: 10408
game_duration_seconds: 464.675292969
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5311
score_details {
  idle_production_time: 1450.75
  idle_worker_time: 2259.0625
  total_value_units: 2000.0
  total_value_structures: 3825.0
  killed_value_units: 50.0
  killed_value_structures: 175.0
  collected_minerals: 5765.0
  collected_vespene: 1908.0
  collection_rate_minerals: 391.0
  collection_rate_vespene: 111.0
  spent_minerals: 5299.0
  spent_vespene: 962.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 225.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 100.0
    economy: 2675.0
    technology: 237.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 450.0
    economy: 1775.0
    technology: 800.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 100.0
    economy: 4850.0
    technology: 1150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 625.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 670.0
    shields: 682.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10945.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1232.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 471
vespene: 915
food_cap: 23
food_used: 27
food_army: 9
food_workers: 17
idle_worker_count: 11
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 multi {
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 5377
score_details {
  idle_production_time: 1649.0
  idle_worker_time: 2569.9375
  total_value_units: 2800.0
  total_value_structures: 3825.0
  killed_value_units: 50.0
  killed_value_structures: 175.0
  collected_minerals: 5865.0
  collected_vespene: 1924.0
  collection_rate_minerals: 391.0
  collection_rate_vespene: 67.0
  spent_minerals: 5299.0
  spent_vespene: 962.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 225.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 100.0
    economy: 2675.0
    technology: 237.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 450.0
    economy: 1775.0
    technology: 800.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 550.0
    economy: 4900.0
    technology: 1150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 625.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 670.0
    shields: 694.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10945.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1232.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 571
vespene: 931
food_cap: 23
food_used: 27
food_army: 9
food_workers: 18
idle_worker_count: 19
army_count: 3
warp_gate_count: 0

game_loop:  10408
ui_data{
 
Score:  5377
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
