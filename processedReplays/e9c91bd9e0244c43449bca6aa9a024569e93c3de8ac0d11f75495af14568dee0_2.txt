----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4371
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4101
  player_apm: 135
}
game_duration_loops: 15799
game_duration_seconds: 705.361694336
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7272
score_details {
  idle_production_time: 4212.5625
  idle_worker_time: 100.875
  total_value_units: 5450.0
  total_value_structures: 1300.0
  killed_value_units: 4000.0
  killed_value_structures: 400.0
  collected_minerals: 7045.0
  collected_vespene: 1664.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 246.0
  spent_minerals: 6462.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1800.0
    economy: 1950.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1125.0
    economy: 412.0
    technology: -125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 3850.0
    technology: 600.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 4400.0
    technology: 750.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 7085.83447266
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4321.99023438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 633
vespene: 389
food_cap: 92
food_used: 62
food_army: 29
food_workers: 33
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8728
score_details {
  idle_production_time: 10646.0
  idle_worker_time: 120.75
  total_value_units: 8800.0
  total_value_structures: 2650.0
  killed_value_units: 5550.0
  killed_value_structures: 2675.0
  collected_minerals: 10215.0
  collected_vespene: 3300.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 246.0
  spent_minerals: 9987.0
  spent_vespene: 2600.0
  food_used {
    none: 0.0
    army: 52.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2350.0
    economy: 3950.0
    technology: 950.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 425.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1600.0
    economy: 2212.0
    technology: 625.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 3650.0
    technology: 500.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 4000.0
    economy: 6650.0
    technology: 1000.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 19600.9511719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13711.5761719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 278
vespene: 700
food_cap: 84
food_used: 80
food_army: 52
food_workers: 27
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  15799
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 97
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 97
  count: 1
}
single {
  unit {
    unit_type: 32
    player_relative: 4
    health: 22
    shields: 0
    energy: 0
  }
}

Score:  8728
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
