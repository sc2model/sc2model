----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3612
  player_apm: 130
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3560
  player_apm: 146
}
game_duration_loops: 41332
game_duration_seconds: 1845.30737305
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10340
score_details {
  idle_production_time: 1488.625
  idle_worker_time: 198.5625
  total_value_units: 4450.0
  total_value_structures: 3775.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 7470.0
  collected_vespene: 2044.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 627.0
  spent_minerals: 7287.0
  spent_vespene: 1337.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1550.0
    economy: 4850.0
    technology: 1625.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 5000.0
    technology: 1325.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 222.25
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 277.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 233
vespene: 707
food_cap: 93
food_used: 75
food_army: 29
food_workers: 44
idle_worker_count: 1
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 82
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 55
  }
  units {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
    add_on {
      unit_type: 132
      build_progress: 0.258928596973
    }
  }
}

score{
 score_type: Melee
score: 15234
score_details {
  idle_production_time: 8698.8125
  idle_worker_time: 2535.0
  total_value_units: 12300.0
  total_value_structures: 8275.0
  killed_value_units: 8675.0
  killed_value_structures: 0.0
  collected_minerals: 20570.0
  collected_vespene: 6992.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 515.0
  spent_minerals: 19087.0
  spent_vespene: 6762.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6650.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5071.0
    economy: 2126.0
    technology: 262.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2479.0
    economy: 187.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 6000.0
    technology: 3050.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 1025.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 5825.0
    economy: 8600.0
    technology: 3350.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 2975.0
    economy: 300.0
    technology: 1025.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 7834.32861328
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12942.5371094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2759.65209961
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1140
vespene: 44
food_cap: 164
food_used: 77
food_army: 34
food_workers: 43
idle_worker_count: 14
army_count: 5
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 18719
score_details {
  idle_production_time: 17915.625
  idle_worker_time: 7343.0625
  total_value_units: 25475.0
  total_value_structures: 10125.0
  killed_value_units: 22250.0
  killed_value_structures: 800.0
  collected_minerals: 30010.0
  collected_vespene: 11840.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 694.0
  spent_minerals: 28574.0
  spent_vespene: 10612.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 14750.0
    economy: 2250.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5800.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10945.0
    economy: 4223.0
    technology: 549.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6699.0
    economy: 531.0
    technology: -113.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2600.0
    economy: 6575.0
    technology: 3650.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 1450.0
    economy: 150.0
    technology: 1225.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 13025.0
    economy: 10900.0
    technology: 4350.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 7900.0
    economy: 900.0
    technology: 1225.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 21170.671875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 30193.5371094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 10001.6523438
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 421
vespene: 848
food_cap: 172
food_used: 99
food_army: 51
food_workers: 48
idle_worker_count: 1
army_count: 16
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 5085
score_details {
  idle_production_time: 26158.625
  idle_worker_time: 9067.9375
  total_value_units: 30575.0
  total_value_structures: 10250.0
  killed_value_units: 31875.0
  killed_value_structures: 1900.0
  collected_minerals: 33724.0
  collected_vespene: 13448.0
  collection_rate_minerals: 335.0
  collection_rate_vespene: 0.0
  spent_minerals: 32399.0
  spent_vespene: 11862.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 8.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 20150.0
    economy: 5500.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7875.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16620.0
    economy: 10889.0
    technology: 4059.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 9374.0
    economy: 758.0
    technology: 1012.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 150.0
    economy: 950.0
    technology: 875.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 16475.0
    economy: 11200.0
    technology: 4475.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 9250.0
    economy: 900.0
    technology: 1225.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 36777.8632812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 71581.8671875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 12344.4482422
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 31
vespene: 1129
food_cap: 15
food_used: 12
food_army: 4
food_workers: 7
idle_worker_count: 0
army_count: 1
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 3581
score_details {
  idle_production_time: 26297.75
  idle_worker_time: 9088.5
  total_value_units: 30825.0
  total_value_structures: 10950.0
  killed_value_units: 34275.0
  killed_value_structures: 1900.0
  collected_minerals: 34154.0
  collected_vespene: 13448.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 32499.0
  spent_vespene: 11862.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 5.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 21600.0
    economy: 5500.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8825.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16770.0
    economy: 11714.0
    technology: 4940.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 9424.0
    economy: 758.0
    technology: 1115.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 250.0
    technology: 125.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 16550.0
    economy: 11350.0
    technology: 5175.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 9275.0
    economy: 900.0
    technology: 1225.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 38513.1601562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 77170.8671875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 12689.4482422
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 330
vespene: 1126
food_cap: 0
food_used: 5
food_army: 0
food_workers: 5
idle_worker_count: 4
army_count: 0
warp_gate_count: 0

game_loop:  41332
ui_data{
 
Score:  3581
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
