----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3212
  player_apm: 78
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3216
  player_apm: 79
}
game_duration_loops: 12028
game_duration_seconds: 537.00177002
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6780
score_details {
  idle_production_time: 1379.0625
  idle_worker_time: 444.3125
  total_value_units: 3950.0
  total_value_structures: 2525.0
  killed_value_units: 2150.0
  killed_value_structures: 200.0
  collected_minerals: 6730.0
  collected_vespene: 1900.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 358.0
  spent_minerals: 6750.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1450.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 3400.0
    technology: 1050.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 425.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 2950.0
    technology: 950.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 325.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 1774.34179688
    shields: 1913.55273438
    energy: 0.0
  }
  total_damage_taken {
    life: 1975.84008789
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 30
vespene: 275
food_cap: 71
food_used: 38
food_army: 10
food_workers: 27
idle_worker_count: 1
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 22
  count: 1
}
single {
  unit {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 7246
score_details {
  idle_production_time: 1928.0625
  idle_worker_time: 591.875
  total_value_units: 5600.0
  total_value_structures: 3125.0
  killed_value_units: 2650.0
  killed_value_structures: 200.0
  collected_minerals: 7890.0
  collected_vespene: 2372.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 268.0
  spent_minerals: 7800.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2200.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2600.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 600.0
    economy: 3500.0
    technology: 1050.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 425.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2800.0
    economy: 3700.0
    technology: 1050.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 425.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 2292.34179688
    shields: 2520.80273438
    energy: 0.0
  }
  total_damage_taken {
    life: 3911.84008789
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 231.5625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 124
vespene: 347
food_cap: 102
food_used: 42
food_army: 12
food_workers: 29
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  12028
ui_data{
 single {
  unit {
    unit_type: 692
    player_relative: 1
    health: 61
    shields: 0
    energy: 0
  }
}

Score:  7246
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
