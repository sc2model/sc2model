----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4497
  player_apm: 130
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4406
  player_apm: 172
}
game_duration_loops: 9331
game_duration_seconds: 416.591583252
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3612
score_details {
  idle_production_time: 7400.25
  idle_worker_time: 72.5
  total_value_units: 5050.0
  total_value_structures: 1275.0
  killed_value_units: 875.0
  killed_value_structures: 100.0
  collected_minerals: 5350.0
  collected_vespene: 868.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 156.0
  spent_minerals: 5031.0
  spent_vespene: 500.0
  food_used {
    none: 0.0
    army: 3.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2200.0
    economy: 781.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 150.0
    economy: 2075.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3000.0
    economy: 3175.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1892.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6651.48535156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 369
vespene: 368
food_cap: 54
food_used: 24
food_army: 3
food_workers: 21
idle_worker_count: 21
army_count: 6
warp_gate_count: 0

game_loop:  9331
ui_data{
 
Score:  3612
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
