----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3449
  player_apm: 123
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3355
  player_apm: 218
}
game_duration_loops: 19472
game_duration_seconds: 869.346374512
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10101
score_details {
  idle_production_time: 6994.5
  idle_worker_time: 83.375
  total_value_units: 6800.0
  total_value_structures: 1800.0
  killed_value_units: 650.0
  killed_value_structures: 0.0
  collected_minerals: 9000.0
  collected_vespene: 1532.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 470.0
  spent_minerals: 8631.0
  spent_vespene: 575.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 356.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1300.0
    economy: 4925.0
    technology: 1800.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 5700.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1406.24291992
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3245.94384766
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 419
vespene: 957
food_cap: 90
food_used: 67
food_army: 19
food_workers: 44
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11124
score_details {
  idle_production_time: 31396.6875
  idle_worker_time: 197.0625
  total_value_units: 24150.0
  total_value_structures: 3100.0
  killed_value_units: 9100.0
  killed_value_structures: 0.0
  collected_minerals: 23530.0
  collected_vespene: 6312.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 380.0
  spent_minerals: 23262.0
  spent_vespene: 4856.0
  food_used {
    none: 0.0
    army: 23.5
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7200.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12206.0
    economy: 1756.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3031.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1900.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 950.0
    economy: 5450.0
    technology: 1800.0
    upgrade: 350.0
  }
  used_vespene {
    none: -25.0
    army: 150.0
    economy: 25.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 16150.0
    economy: 8400.0
    technology: 2100.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 4375.0
    economy: 50.0
    technology: 450.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 11441.9570312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21095.5976562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 318
vespene: 1456
food_cap: 120
food_used: 62
food_army: 23
food_workers: 39
idle_worker_count: 3
army_count: 4
warp_gate_count: 0

game_loop:  19472
ui_data{
 multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 74
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  11124
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
