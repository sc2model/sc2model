----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4013
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4003
  player_apm: 128
}
game_duration_loops: 9310
game_duration_seconds: 415.653991699
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7529
score_details {
  idle_production_time: 10760.625
  idle_worker_time: 38.75
  total_value_units: 5450.0
  total_value_structures: 1425.0
  killed_value_units: 1850.0
  killed_value_structures: 100.0
  collected_minerals: 7005.0
  collected_vespene: 736.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 179.0
  spent_minerals: 6887.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1250.0
    economy: 500.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 350.0
    technology: -288.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4125.0
    technology: 650.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4825.0
    technology: 650.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2522.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2115.14355469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 168
vespene: 336
food_cap: 74
food_used: 72
food_army: 26
food_workers: 46
idle_worker_count: 0
army_count: 22
warp_gate_count: 0

game_loop:  9310
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 105
  count: 9
}

Score:  7529
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
