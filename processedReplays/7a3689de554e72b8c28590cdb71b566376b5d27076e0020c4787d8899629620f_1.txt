----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4355
  player_apm: 90
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 110
}
game_duration_loops: 8167
game_duration_seconds: 364.623657227
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5234
score_details {
  idle_production_time: 2316.3125
  idle_worker_time: 87.1875
  total_value_units: 3750.0
  total_value_structures: 1850.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 4750.0
  collected_vespene: 1284.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 313.0
  spent_minerals: 4300.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 17.5
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 575.0
    economy: 175.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 750.0
    economy: 2000.0
    technology: 950.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1600.0
    economy: 2800.0
    technology: 1300.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 600.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 184.0
    shields: 702.375
    energy: 0.0
  }
  total_damage_taken {
    life: 3745.65722656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 500
vespene: 359
food_cap: 46
food_used: 37
food_army: 17
food_workers: 20
idle_worker_count: 20
army_count: 3
warp_gate_count: 0

game_loop:  8167
ui_data{
 
Score:  5234
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
