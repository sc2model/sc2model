----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4009
  player_apm: 139
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3803
  player_apm: 177
}
game_duration_loops: 10777
game_duration_seconds: 481.149658203
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9037
score_details {
  idle_production_time: 1460.75
  idle_worker_time: 109.25
  total_value_units: 4350.0
  total_value_structures: 3600.0
  killed_value_units: 800.0
  killed_value_structures: 100.0
  collected_minerals: 7690.0
  collected_vespene: 1672.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 0.0
  spent_minerals: 7075.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 925.0
    economy: 4100.0
    technology: 1500.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 1875.0
    economy: 4450.0
    technology: 1200.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 875.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 720.75
    shields: 1441.25
    energy: 0.0
  }
  total_damage_taken {
    life: 1189.81005859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3.3720703125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 665
vespene: 97
food_cap: 93
food_used: 53
food_army: 19
food_workers: 31
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8682
score_details {
  idle_production_time: 1790.3125
  idle_worker_time: 190.125
  total_value_units: 4700.0
  total_value_structures: 3750.0
  killed_value_units: 800.0
  killed_value_structures: 100.0
  collected_minerals: 8845.0
  collected_vespene: 1812.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 179.0
  spent_minerals: 7562.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1350.0
    economy: 550.0
    technology: 87.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 825.0
    economy: 3850.0
    technology: 1150.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 350.0
    economy: 0.0
    technology: 300.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 1875.0
    economy: 4800.0
    technology: 1350.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 875.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 729.75
    shields: 1831.25
    energy: 0.0
  }
  total_damage_taken {
    life: 5364.55419922
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 312.272460938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1314
vespene: 93
food_cap: 69
food_used: 52
food_army: 17
food_workers: 34
idle_worker_count: 9
army_count: 3
warp_gate_count: 0

game_loop:  10777
ui_data{
 multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 40
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 79
  }
}

Score:  8682
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
