----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3580
  player_apm: 135
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3492
  player_apm: 97
}
game_duration_loops: 6876
game_duration_seconds: 306.985717773
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6448
score_details {
  idle_production_time: 563.625
  idle_worker_time: 81.75
  total_value_units: 2525.0
  total_value_structures: 2400.0
  killed_value_units: 375.0
  killed_value_structures: 0.0
  collected_minerals: 4360.0
  collected_vespene: 1288.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 335.0
  spent_minerals: 3600.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 2800.0
    technology: 700.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 800.0
    economy: 2950.0
    technology: 700.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 556.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 263.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 810
vespene: 313
food_cap: 70
food_used: 41
food_army: 17
food_workers: 24
idle_worker_count: 3
army_count: 7
warp_gate_count: 0

game_loop:  6876
ui_data{
 production {
  unit {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 48
    build_progress: 0.892499983311
  }
}

Score:  6448
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
