----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3554
  player_apm: 97
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3810
  player_apm: 93
}
game_duration_loops: 11734
game_duration_seconds: 523.875854492
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11304
score_details {
  idle_production_time: 8156.4375
  idle_worker_time: 26.4375
  total_value_units: 5850.0
  total_value_structures: 1550.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 9265.0
  collected_vespene: 1664.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 470.0
  spent_minerals: 7775.0
  spent_vespene: 1325.0
  food_used {
    none: 0.0
    army: 28.5
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 375.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1775.0
    economy: 4700.0
    technology: 800.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 50.0
    technology: 150.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5300.0
    technology: 950.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 100.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 753.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1867.45849609
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1540
vespene: 339
food_cap: 74
food_used: 73
food_army: 28
food_workers: 44
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 893
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 893
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9955
score_details {
  idle_production_time: 10251.1875
  idle_worker_time: 26.4375
  total_value_units: 7600.0
  total_value_structures: 1550.0
  killed_value_units: 1725.0
  killed_value_structures: 200.0
  collected_minerals: 11545.0
  collected_vespene: 2384.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 470.0
  spent_minerals: 9362.0
  spent_vespene: 1612.0
  food_used {
    none: 3.0
    army: 7.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 1200.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2437.0
    economy: 1375.0
    technology: -75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 187.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -100.0
    army: 800.0
    economy: 3825.0
    technology: 675.0
    upgrade: 725.0
  }
  used_vespene {
    none: -50.0
    army: 150.0
    economy: 50.0
    technology: 150.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 0.0
    army: 3350.0
    economy: 5850.0
    technology: 950.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 100.0
    technology: 250.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 3185.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8103.75830078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2233
vespene: 772
food_cap: 68
food_used: 50
food_army: 7
food_workers: 40
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  11734
ui_data{
 
Score:  9955
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
