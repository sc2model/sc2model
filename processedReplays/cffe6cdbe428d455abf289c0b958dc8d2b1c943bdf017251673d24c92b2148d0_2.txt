----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2654
  player_apm: 44
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2568
  player_apm: 110
}
game_duration_loops: 16785
game_duration_seconds: 749.382629395
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11003
score_details {
  idle_production_time: 1329.125
  idle_worker_time: 399.375
  total_value_units: 4775.0
  total_value_structures: 3325.0
  killed_value_units: 550.0
  killed_value_structures: 0.0
  collected_minerals: 8605.0
  collected_vespene: 1964.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 492.0
  spent_minerals: 7750.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 216.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1575.0
    economy: 4600.0
    technology: 1525.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 375.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 1975.0
    economy: 4575.0
    technology: 1225.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 375.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 764.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 620.851318359
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 50.8513183594
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 889
vespene: 589
food_cap: 86
food_used: 74
food_army: 32
food_workers: 41
idle_worker_count: 1
army_count: 10
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 97
    transport_slots_taken: 7
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 97
    transport_slots_taken: 5
  }
}

score{
 score_type: Melee
score: 21622
score_details {
  idle_production_time: 5092.75
  idle_worker_time: 1216.6875
  total_value_units: 8425.0
  total_value_structures: 7475.0
  killed_value_units: 4650.0
  killed_value_structures: 725.0
  collected_minerals: 16910.0
  collected_vespene: 5536.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 1052.0
  spent_minerals: 14150.0
  spent_vespene: 3100.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 3000.0
    economy: 1825.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 166.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3325.0
    economy: 6150.0
    technology: 3225.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 150.0
    technology: 1000.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 4525.0
    economy: 7050.0
    technology: 3075.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 1250.0
    economy: 300.0
    technology: 900.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 9081.20996094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3939.49047852
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1738.77856445
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2765
vespene: 2407
food_cap: 148
food_used: 113
food_army: 65
food_workers: 48
idle_worker_count: 4
army_count: 41
warp_gate_count: 0

game_loop:  16785
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 51
  count: 30
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 96
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 28
    shields: 0
    energy: 160
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 167
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
}

Score:  21622
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
