----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3811
  player_apm: 127
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3732
  player_apm: 65
}
game_duration_loops: 6719
game_duration_seconds: 299.976287842
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5687
score_details {
  idle_production_time: 286.5
  idle_worker_time: 328.0
  total_value_units: 3250.0
  total_value_structures: 1750.0
  killed_value_units: 1725.0
  killed_value_structures: 450.0
  collected_minerals: 3875.0
  collected_vespene: 1112.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 313.0
  spent_minerals: 3700.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1150.0
    economy: 400.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 2300.0
    technology: 400.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 2450.0
    technology: 400.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2050.0
    shields: 2655.875
    energy: 0.0
  }
  total_damage_taken {
    life: 609.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 225
vespene: 112
food_cap: 63
food_used: 53
food_army: 33
food_workers: 20
idle_worker_count: 2
army_count: 14
warp_gate_count: 0

game_loop:  6719
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}

Score:  5687
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
