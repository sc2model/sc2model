----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2875
  player_apm: 41
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2783
  player_apm: 82
}
game_duration_loops: 21089
game_duration_seconds: 941.53894043
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7893
score_details {
  idle_production_time: 1045.0
  idle_worker_time: 350.625
  total_value_units: 3725.0
  total_value_structures: 2800.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 6135.0
  collected_vespene: 1708.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 335.0
  spent_minerals: 6025.0
  spent_vespene: 850.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: -50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1825.0
    economy: 3100.0
    technology: 1050.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1575.0
    economy: 3250.0
    technology: 1050.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 618.591796875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 270.0
    shields: 680.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 160
vespene: 858
food_cap: 62
food_used: 62
food_army: 32
food_workers: 30
idle_worker_count: 0
army_count: 13
warp_gate_count: 2

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 23502
score_details {
  idle_production_time: 4179.0
  idle_worker_time: 913.625
  total_value_units: 12070.0
  total_value_structures: 6800.0
  killed_value_units: 625.0
  killed_value_structures: 0.0
  collected_minerals: 17930.0
  collected_vespene: 6512.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 828.0
  spent_minerals: 17160.0
  spent_vespene: 6250.0
  food_used {
    none: 0.0
    army: 128.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: -50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7320.0
    economy: 6350.0
    technology: 2400.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 650.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 6920.0
    economy: 6600.0
    technology: 2250.0
    upgrade: 625.0
  }
  total_used_vespene {
    none: 0.0
    army: 3800.0
    economy: 0.0
    technology: 500.0
    upgrade: 625.0
  }
  total_damage_dealt {
    life: 854.958984375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 625.0
    shields: 1680.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 820
vespene: 262
food_cap: 200
food_used: 176
food_army: 128
food_workers: 48
idle_worker_count: 5
army_count: 41
warp_gate_count: 3

game_loop:  20000
ui_data{
 multi {
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 10
    player_relative: 1
    health: 350
    shields: 350
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 133
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 133
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 133
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 23884
score_details {
  idle_production_time: 4848.875
  idle_worker_time: 1387.0625
  total_value_units: 13450.0
  total_value_structures: 7100.0
  killed_value_units: 5650.0
  killed_value_structures: 0.0
  collected_minerals: 19200.0
  collected_vespene: 7044.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 649.0
  spent_minerals: 17270.0
  spent_vespene: 6250.0
  food_used {
    none: 0.0
    army: 102.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2200.0
    economy: -50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6010.0
    economy: 6350.0
    technology: 2400.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 650.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 7800.0
    economy: 6600.0
    technology: 2400.0
    upgrade: 625.0
  }
  total_used_vespene {
    none: 0.0
    army: 4300.0
    economy: 0.0
    technology: 650.0
    upgrade: 625.0
  }
  total_damage_dealt {
    life: 5302.61572266
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2910.0
    shields: 4006.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1980
vespene: 794
food_cap: 200
food_used: 150
food_army: 102
food_workers: 48
idle_worker_count: 7
army_count: 24
warp_gate_count: 3

game_loop:  21089
ui_data{
 multi {
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 130
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 8
    energy: 0
  }
  units {
    unit_type: 10
    player_relative: 1
    health: 350
    shields: 350
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 48
    shields: 21
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 18
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 130
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 130
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 97
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 96
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 96
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 4
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
}

Score:  23884
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
