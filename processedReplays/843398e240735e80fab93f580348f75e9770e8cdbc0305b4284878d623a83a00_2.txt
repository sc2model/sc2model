----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3861
  player_apm: 179
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3774
  player_apm: 120
}
game_duration_loops: 14358
game_duration_seconds: 641.026855469
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11694
score_details {
  idle_production_time: 5084.1875
  idle_worker_time: 0.0
  total_value_units: 6900.0
  total_value_structures: 1875.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9340.0
  collected_vespene: 1404.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 627.0
  spent_minerals: 8750.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2550.0
    economy: 5500.0
    technology: 1250.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 6225.0
    technology: 1400.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 316.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 260.629882812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 640
vespene: 54
food_cap: 114
food_used: 110
food_army: 54
food_workers: 56
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11220
score_details {
  idle_production_time: 11991.25
  idle_worker_time: 0.375
  total_value_units: 12750.0
  total_value_structures: 2475.0
  killed_value_units: 3375.0
  killed_value_structures: 0.0
  collected_minerals: 16370.0
  collected_vespene: 3700.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 291.0
  spent_minerals: 13675.0
  spent_vespene: 3100.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2050.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5375.0
    economy: 2375.0
    technology: 175.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 37.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 4350.0
    technology: 1400.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 275.0
    technology: 300.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 5575.0
    economy: 7900.0
    technology: 1675.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 1875.0
    economy: 550.0
    technology: 400.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 3949.65039062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14746.5898438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2745
vespene: 600
food_cap: 122
food_used: 38
food_army: 8
food_workers: 30
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  14358
ui_data{
 
Score:  11220
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
