----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5753
  player_apm: 264
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5306
  player_apm: 211
}
game_duration_loops: 46541
game_duration_seconds: 2077.86816406
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11985
score_details {
  idle_production_time: 7441.125
  idle_worker_time: 38.8125
  total_value_units: 8300.0
  total_value_structures: 1975.0
  killed_value_units: 1100.0
  killed_value_structures: 0.0
  collected_minerals: 10915.0
  collected_vespene: 1520.0
  collection_rate_minerals: 2491.0
  collection_rate_vespene: 649.0
  spent_minerals: 10825.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2250.0
    economy: 6275.0
    technology: 1300.0
    upgrade: 575.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 350.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 0.0
    army: 2350.0
    economy: 7725.0
    technology: 1200.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 1676.72924805
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2472.13623047
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 140
vespene: 420
food_cap: 144
food_used: 97
food_army: 37
food_workers: 60
idle_worker_count: 0
army_count: 28
warp_gate_count: 0
larva_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 38
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 106
  count: 1
}
production {
  unit {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 126
    build_progress: 0.783749997616
  }
}

score{
 score_type: Melee
score: 27901
score_details {
  idle_production_time: 43357.0625
  idle_worker_time: 429.25
  total_value_units: 28600.0
  total_value_structures: 4200.0
  killed_value_units: 8900.0
  killed_value_structures: 200.0
  collected_minerals: 30260.0
  collected_vespene: 8216.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 985.0
  spent_minerals: 29525.0
  spent_vespene: 7850.0
  food_used {
    none: 0.0
    army: 115.0
    economy: 68.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6700.0
    economy: 1050.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6550.0
    economy: 1500.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7475.0
    economy: 8850.0
    technology: 2350.0
    upgrade: 1700.0
  }
  used_vespene {
    none: 0.0
    army: 3725.0
    economy: 0.0
    technology: 950.0
    upgrade: 1700.0
  }
  total_used_minerals {
    none: 0.0
    army: 17900.0
    economy: 10750.0
    technology: 3150.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 0.0
    army: 5400.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 12100.2080078
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16499.4238281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 785
vespene: 366
food_cap: 200
food_used: 183
food_army: 115
food_workers: 68
idle_worker_count: 1
army_count: 121
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 112
  count: 96
}
groups {
  control_group_index: 2
  leader_unit_type: 112
  count: 31
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 12
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 9
  count: 24
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 25984
score_details {
  idle_production_time: 76721.1875
  idle_worker_time: 3553.5625
  total_value_units: 46300.0
  total_value_structures: 5950.0
  killed_value_units: 24125.0
  killed_value_structures: 1025.0
  collected_minerals: 46660.0
  collected_vespene: 11924.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 156.0
  spent_minerals: 46425.0
  spent_vespene: 11350.0
  food_used {
    none: 0.0
    army: 125.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 17200.0
    economy: 2325.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 21375.0
    economy: 3600.0
    technology: 650.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7300.0
    economy: 7800.0
    technology: 2225.0
    upgrade: 1900.0
  }
  used_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 950.0
    upgrade: 1900.0
  }
  total_used_minerals {
    none: 0.0
    army: 32800.0
    economy: 13100.0
    technology: 3750.0
    upgrade: 1900.0
  }
  total_used_vespene {
    none: 0.0
    army: 8500.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1900.0
  }
  total_damage_dealt {
    life: 27787.4570312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 51589.578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 285
vespene: 574
food_cap: 200
food_used: 181
food_army: 125
food_workers: 56
idle_worker_count: 10
army_count: 67
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 109
  count: 63
}
groups {
  control_group_index: 2
  leader_unit_type: 112
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 12
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 105
  count: 8
}
multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 66
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 162
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 119
    shields: 0
    energy: 66
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 172
    shields: 0
    energy: 66
  }
}

score{
 score_type: Melee
score: 23406
score_details {
  idle_production_time: 115202.9375
  idle_worker_time: 7123.8125
  total_value_units: 58050.0
  total_value_structures: 7275.0
  killed_value_units: 33325.0
  killed_value_structures: 1975.0
  collected_minerals: 57535.0
  collected_vespene: 16808.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 627.0
  spent_minerals: 57506.0
  spent_vespene: 14106.0
  food_used {
    none: 0.0
    army: 64.5
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 23550.0
    economy: 3750.0
    technology: 625.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7275.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 31831.0
    economy: 7375.0
    technology: 1075.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7056.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 3600.0
    economy: 7175.0
    technology: 2225.0
    upgrade: 2250.0
  }
  used_vespene {
    none: -25.0
    army: 2250.0
    economy: 0.0
    technology: 950.0
    upgrade: 2250.0
  }
  total_used_minerals {
    none: 0.0
    army: 41100.0
    economy: 16250.0
    technology: 4175.0
    upgrade: 2250.0
  }
  total_used_vespene {
    none: 0.0
    army: 10650.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2250.0
  }
  total_damage_dealt {
    life: 39759.015625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 81036.2578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 79
vespene: 2702
food_cap: 178
food_used: 100
food_army: 64
food_workers: 36
idle_worker_count: 1
army_count: 49
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 109
  count: 36
}
groups {
  control_group_index: 2
  leader_unit_type: 112
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 105
  count: 8
}
multi {
  units {
    unit_type: 113
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
    add_on {
      unit_type: 114
      build_progress: 0.332503497601
    }
  }
  units {
    unit_type: 113
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
    add_on {
      unit_type: 114
      build_progress: 0.323267281055
    }
  }
  units {
    unit_type: 113
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
    add_on {
      unit_type: 114
      build_progress: 0.136695861816
    }
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13903
score_details {
  idle_production_time: 131203.5
  idle_worker_time: 8252.8125
  total_value_units: 58200.0
  total_value_structures: 8075.0
  killed_value_units: 39300.0
  killed_value_structures: 2350.0
  collected_minerals: 59845.0
  collected_vespene: 17720.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 0.0
  spent_minerals: 59606.0
  spent_vespene: 14406.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 28150.0
    economy: 3900.0
    technology: 825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8650.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 34406.0
    economy: 11725.0
    technology: 3525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 8531.0
    economy: 0.0
    technology: 950.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 1425.0
    economy: 3550.0
    technology: -100.0
    upgrade: 2250.0
  }
  used_vespene {
    none: -25.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 2250.0
  }
  total_used_minerals {
    none: 0.0
    army: 42600.0
    economy: 17100.0
    technology: 4425.0
    upgrade: 2250.0
  }
  total_used_vespene {
    none: 0.0
    army: 12150.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2250.0
  }
  total_damage_dealt {
    life: 49324.0625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 121399.296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 289
vespene: 3314
food_cap: 100
food_used: 49
food_army: 29
food_workers: 20
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  46541
ui_data{
 multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 61
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 56
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 142
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

Score:  13903
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
