----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3818
  player_apm: 120
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3704
  player_apm: 132
}
game_duration_loops: 4981
game_duration_seconds: 222.381591797
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4748
score_details {
  idle_production_time: 101.625
  idle_worker_time: 55.625
  total_value_units: 1700.0
  total_value_structures: 1900.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 3185.0
  collected_vespene: 588.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 335.0
  spent_minerals: 3175.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 675.0
    economy: 2850.0
    technology: 500.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 375.0
    economy: 3050.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 30.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 60
vespene: 188
food_cap: 46
food_used: 40
food_army: 14
food_workers: 26
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  4981
ui_data{
 single {
  unit {
    unit_type: 105
    player_relative: 3
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  4748
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
