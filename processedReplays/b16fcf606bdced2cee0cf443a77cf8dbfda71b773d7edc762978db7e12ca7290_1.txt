----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3814
  player_apm: 117
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3807
  player_apm: 114
}
game_duration_loops: 9478
game_duration_seconds: 423.154541016
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8665
score_details {
  idle_production_time: 3345.9375
  idle_worker_time: 21.0
  total_value_units: 8000.0
  total_value_structures: 1400.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 10560.0
  collected_vespene: 1580.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 380.0
  spent_minerals: 8775.0
  spent_vespene: 725.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2525.0
    economy: 1725.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 400.0
    economy: 4275.0
    technology: 600.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2675.0
    economy: 6500.0
    technology: 750.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 978.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7899.54785156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1835
vespene: 855
food_cap: 124
food_used: 43
food_army: 7
food_workers: 36
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  9478
ui_data{
 
Score:  8665
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
