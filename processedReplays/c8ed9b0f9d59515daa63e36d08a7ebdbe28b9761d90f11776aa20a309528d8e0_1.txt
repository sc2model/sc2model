----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2840
  player_apm: 69
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2713
  player_apm: 64
}
game_duration_loops: 15855
game_duration_seconds: 707.861877441
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8083
score_details {
  idle_production_time: 4526.875
  idle_worker_time: 176.1875
  total_value_units: 4450.0
  total_value_structures: 2050.0
  killed_value_units: 0.0
  killed_value_structures: 150.0
  collected_minerals: 5875.0
  collected_vespene: 1808.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 335.0
  spent_minerals: 5675.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 2850.0
    technology: 1100.0
    upgrade: 625.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 500.0
    upgrade: 625.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 3800.0
    technology: 1250.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 600.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 166.050292969
    shields: 361.207519531
    energy: 0.0
  }
  total_damage_taken {
    life: 2369.68212891
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 250
vespene: 183
food_cap: 76
food_used: 50
food_army: 26
food_workers: 24
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 95
  count: 1
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11372
score_details {
  idle_production_time: 15820.8125
  idle_worker_time: 202.5625
  total_value_units: 8850.0
  total_value_structures: 2450.0
  killed_value_units: 3975.0
  killed_value_structures: 2850.0
  collected_minerals: 10830.0
  collected_vespene: 3592.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 671.0
  spent_minerals: 9825.0
  spent_vespene: 2725.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1625.0
    economy: 2500.0
    technology: 1950.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1850.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1550.0
    economy: 4400.0
    technology: 1350.0
    upgrade: 625.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 650.0
    upgrade: 625.0
  }
  total_used_minerals {
    none: 0.0
    army: 3950.0
    economy: 5200.0
    technology: 1500.0
    upgrade: 625.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 750.0
    upgrade: 625.0
  }
  total_damage_dealt {
    life: 7757.05029297
    shields: 8692.83203125
    energy: 0.0
  }
  total_damage_taken {
    life: 4179.32910156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1055
vespene: 867
food_cap: 124
food_used: 60
food_army: 27
food_workers: 33
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  15855
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 95
  count: 1
}

Score:  11372
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
