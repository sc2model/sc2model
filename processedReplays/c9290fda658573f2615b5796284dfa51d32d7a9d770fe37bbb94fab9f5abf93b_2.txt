----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4327
  player_apm: 198
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4847
  player_apm: 310
}
game_duration_loops: 13418
game_duration_seconds: 599.059631348
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13811
score_details {
  idle_production_time: 8348.75
  idle_worker_time: 17.875
  total_value_units: 9250.0
  total_value_structures: 1875.0
  killed_value_units: 425.0
  killed_value_structures: 100.0
  collected_minerals: 10825.0
  collected_vespene: 2148.0
  collection_rate_minerals: 2659.0
  collection_rate_vespene: 627.0
  spent_minerals: 10062.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2900.0
    economy: 6250.0
    technology: 1200.0
    upgrade: 350.0
  }
  used_vespene {
    none: 150.0
    army: 1100.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 300.0
    army: 3000.0
    economy: 6725.0
    technology: 1350.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 300.0
    army: 1150.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 409.0
    shields: 449.0
    energy: 0.0
  }
  total_damage_taken {
    life: 375.708984375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 813
vespene: 298
food_cap: 130
food_used: 121
food_army: 54
food_workers: 59
idle_worker_count: 0
army_count: 34
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 12
}
groups {
  control_group_index: 2
  leader_unit_type: 107
  count: 21
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18082
score_details {
  idle_production_time: 17958.6875
  idle_worker_time: 17.875
  total_value_units: 15000.0
  total_value_structures: 2800.0
  killed_value_units: 3060.0
  killed_value_structures: 3700.0
  collected_minerals: 17040.0
  collected_vespene: 4504.0
  collection_rate_minerals: 2379.0
  collection_rate_vespene: 1030.0
  spent_minerals: 15937.0
  spent_vespene: 3775.0
  food_used {
    none: 0.0
    army: 74.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1010.0
    economy: 2700.0
    technology: 2100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2600.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3550.0
    economy: 8550.0
    technology: 1200.0
    upgrade: 350.0
  }
  used_vespene {
    none: 150.0
    army: 1650.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 300.0
    army: 6050.0
    economy: 9650.0
    technology: 1350.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 300.0
    army: 2725.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 9430.28027344
    shields: 11900.3125
    energy: 0.0
  }
  total_damage_taken {
    life: 3232.82421875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1153
vespene: 729
food_cap: 200
food_used: 138
food_army: 74
food_workers: 64
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  13418
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 107
  count: 32
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}

Score:  18082
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
