----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4675
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4740
  player_apm: 240
}
game_duration_loops: 9110
game_duration_seconds: 406.724822998
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11299
score_details {
  idle_production_time: 721.8125
  idle_worker_time: 48.1875
  total_value_units: 6500.0
  total_value_structures: 3700.0
  killed_value_units: 900.0
  killed_value_structures: 0.0
  collected_minerals: 8890.0
  collected_vespene: 2084.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 716.0
  spent_minerals: 8700.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2500.0
    economy: 5150.0
    technology: 1400.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 4800.0
    technology: 1250.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1830.32519531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 606.895263672
    shields: 1178.99609375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 240
vespene: 434
food_cap: 102
food_used: 98
food_army: 43
food_workers: 54
idle_worker_count: 0
army_count: 20
warp_gate_count: 4

game_loop:  9110
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 29
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 69
    shields: 69
    energy: 0
    build_progress: 0.987484931946
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 67
    shields: 67
    energy: 0
    build_progress: 0.94998550415
  }
}

Score:  11299
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
