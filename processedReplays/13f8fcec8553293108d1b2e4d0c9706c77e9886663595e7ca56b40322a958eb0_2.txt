----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3013
  player_apm: 96
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3056
  player_apm: 82
}
game_duration_loops: 17359
game_duration_seconds: 775.009460449
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2294
score_details {
  idle_production_time: 1030.625
  idle_worker_time: 577.0625
  total_value_units: 2950.0
  total_value_structures: 2600.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 5065.0
  collected_vespene: 1160.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 5000.0
  spent_vespene: 525.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 3.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 952.0
    economy: 2950.0
    technology: 652.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 151.0
    economy: 0.0
    technology: 176.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 950.0
    technology: 300.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 3650.0
    technology: 825.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 275.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 643.335449219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12360.5175781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 49.4370117188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 111
vespene: 633
food_cap: 15
food_used: 3
food_army: 0
food_workers: 3
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 18
  count: 1
}
single {
  unit {
    unit_type: 43
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 879
score_details {
  idle_production_time: 1251.875
  idle_worker_time: 785.375
  total_value_units: 3450.0
  total_value_structures: 2600.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 5075.0
  collected_vespene: 1160.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 5100.0
  spent_vespene: 700.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1102.0
    economy: 3250.0
    technology: 1002.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 251.0
    economy: 0.0
    technology: 301.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1250.0
    economy: 3700.0
    technology: 825.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 275.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 926.604003906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15698.0517578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 49.4370117188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 21
vespene: 458
food_cap: 0
food_used: 4
food_army: 4
food_workers: 0
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  17359
ui_data{
 
Score:  879
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
