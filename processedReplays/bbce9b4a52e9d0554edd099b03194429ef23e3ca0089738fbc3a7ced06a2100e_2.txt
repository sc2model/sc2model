----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3668
  player_apm: 162
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3732
  player_apm: 205
}
game_duration_loops: 44333
game_duration_seconds: 1979.28991699
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8211
score_details {
  idle_production_time: 8529.0625
  idle_worker_time: 419.75
  total_value_units: 6600.0
  total_value_structures: 1850.0
  killed_value_units: 500.0
  killed_value_structures: 100.0
  collected_minerals: 7775.0
  collected_vespene: 1236.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 313.0
  spent_minerals: 7700.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 275.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1075.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1425.0
    economy: 3675.0
    technology: 1550.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 150.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3050.0
    economy: 4925.0
    technology: 1575.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1768.86816406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2296.04931641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 125
vespene: 311
food_cap: 114
food_used: 44
food_army: 20
food_workers: 23
idle_worker_count: 2
army_count: 29
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 90
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20430
score_details {
  idle_production_time: 35456.9375
  idle_worker_time: 978.0625
  total_value_units: 19200.0
  total_value_structures: 3525.0
  killed_value_units: 4550.0
  killed_value_structures: 100.0
  collected_minerals: 23550.0
  collected_vespene: 5664.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 963.0
  spent_minerals: 22417.0
  spent_vespene: 5542.0
  food_used {
    none: 0.0
    army: 65.5
    economy: 72.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3975.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6017.0
    economy: 1400.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1217.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -350.0
    army: 3600.0
    economy: 7425.0
    technology: 3275.0
    upgrade: 1325.0
  }
  used_vespene {
    none: -175.0
    army: 1900.0
    economy: 0.0
    technology: 800.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 0.0
    army: 9850.0
    economy: 10375.0
    technology: 3500.0
    upgrade: 1125.0
  }
  total_used_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 850.0
    upgrade: 1125.0
  }
  total_damage_dealt {
    life: 6641.16113281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11876.4091797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1183
vespene: 122
food_cap: 168
food_used: 137
food_army: 65
food_workers: 72
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 30446
score_details {
  idle_production_time: 56365.4375
  idle_worker_time: 3541.0625
  total_value_units: 36300.0
  total_value_structures: 5075.0
  killed_value_units: 8975.0
  killed_value_structures: 200.0
  collected_minerals: 35975.0
  collected_vespene: 11680.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 783.0
  spent_minerals: 34842.0
  spent_vespene: 11092.0
  food_used {
    none: 0.0
    army: 131.5
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7025.0
    economy: 200.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11117.0
    economy: 2775.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2817.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -350.0
    army: 8325.0
    economy: 7500.0
    technology: 3350.0
    upgrade: 2075.0
  }
  used_vespene {
    none: -175.0
    army: 4925.0
    economy: 0.0
    technology: 950.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 0.0
    army: 22250.0
    economy: 12325.0
    technology: 4000.0
    upgrade: 1975.0
  }
  total_used_vespene {
    none: 0.0
    army: 8225.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1975.0
  }
  total_damage_dealt {
    life: 11729.2246094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 26702.796875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1183
vespene: 588
food_cap: 200
food_used: 176
food_army: 131
food_workers: 45
idle_worker_count: 3
army_count: 85
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 112
  count: 86
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 483
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 370
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 95
    shields: 0
    energy: 0
  }
  units {
    unit_type: 111
    player_relative: 1
    health: 90
    shields: 0
    energy: 182
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 488
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 181
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19071
score_details {
  idle_production_time: 90246.0625
  idle_worker_time: 4948.0625
  total_value_units: 49450.0
  total_value_structures: 5125.0
  killed_value_units: 26350.0
  killed_value_structures: 5275.0
  collected_minerals: 47755.0
  collected_vespene: 16696.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 604.0
  spent_minerals: 47690.0
  spent_vespene: 15890.0
  food_used {
    none: 7.0
    army: 40.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 18700.0
    economy: 5950.0
    technology: 925.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5650.0
    economy: 300.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25365.0
    economy: 4375.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 10465.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -1550.0
    army: 3225.0
    economy: 7650.0
    technology: 3650.0
    upgrade: 2075.0
  }
  used_vespene {
    none: -1175.0
    army: 1250.0
    economy: 0.0
    technology: 950.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 0.0
    army: 33800.0
    economy: 13975.0
    technology: 4200.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 0.0
    army: 13925.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 40268.75
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 52329.3203125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 115
vespene: 806
food_cap: 164
food_used: 85
food_army: 40
food_workers: 38
idle_worker_count: 0
army_count: 55
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 112
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}

score{
 score_type: Melee
score: 22542
score_details {
  idle_production_time: 110731.6875
  idle_worker_time: 5274.6875
  total_value_units: 53800.0
  total_value_structures: 5675.0
  killed_value_units: 28900.0
  killed_value_structures: 5575.0
  collected_minerals: 52000.0
  collected_vespene: 18472.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 358.0
  spent_minerals: 50765.0
  spent_vespene: 17965.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 20850.0
    economy: 6350.0
    technology: 1225.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5650.0
    economy: 300.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 27315.0
    economy: 4425.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 10915.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -1550.0
    army: 4250.0
    economy: 7650.0
    technology: 3650.0
    upgrade: 2075.0
  }
  used_vespene {
    none: -1175.0
    army: 2825.0
    economy: 0.0
    technology: 950.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 0.0
    army: 38850.0
    economy: 14475.0
    technology: 4500.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 0.0
    army: 17150.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 45124.75
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 57118.6015625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1285
vespene: 507
food_cap: 162
food_used: 95
food_army: 60
food_workers: 35
idle_worker_count: 5
army_count: 57
warp_gate_count: 0

game_loop:  44333
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 114
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}

Score:  22542
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
