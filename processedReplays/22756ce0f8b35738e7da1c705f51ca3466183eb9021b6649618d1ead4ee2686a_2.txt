----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3690
  player_apm: 120
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3720
  player_apm: 116
}
game_duration_loops: 22999
game_duration_seconds: 1026.81274414
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13514
score_details {
  idle_production_time: 7067.125
  idle_worker_time: 3.9375
  total_value_units: 6250.0
  total_value_structures: 1725.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 10795.0
  collected_vespene: 1812.0
  collection_rate_minerals: 2519.0
  collection_rate_vespene: 783.0
  spent_minerals: 7393.0
  spent_vespene: 850.0
  food_used {
    none: 0.0
    army: 18.5
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1425.0
    economy: 5425.0
    technology: 950.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 250.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 6025.0
    technology: 1100.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 159.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 297.239257812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3452
vespene: 962
food_cap: 90
food_used: 80
food_army: 18
food_workers: 62
idle_worker_count: 1
army_count: 10
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 9
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 38764
score_details {
  idle_production_time: 34171.625
  idle_worker_time: 406.6875
  total_value_units: 18550.0
  total_value_structures: 2850.0
  killed_value_units: 1850.0
  killed_value_structures: 100.0
  collected_minerals: 30080.0
  collected_vespene: 9552.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 963.0
  spent_minerals: 19768.0
  spent_vespene: 5400.0
  food_used {
    none: 0.0
    army: 127.0
    economy: 73.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 1000.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1600.0
    economy: 250.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6725.0
    economy: 8375.0
    technology: 1825.0
    upgrade: 1975.0
  }
  used_vespene {
    none: 0.0
    army: 2675.0
    economy: 0.0
    technology: 700.0
    upgrade: 1975.0
  }
  total_used_minerals {
    none: 0.0
    army: 8725.0
    economy: 9575.0
    technology: 2175.0
    upgrade: 1225.0
  }
  total_used_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 950.0
    upgrade: 1225.0
  }
  total_damage_dealt {
    life: 3178.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3492.60839844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 10362
vespene: 4152
food_cap: 200
food_used: 200
food_army: 127
food_workers: 73
idle_worker_count: 5
army_count: 64
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 48
    player_relative: 4
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27612
score_details {
  idle_production_time: 36733.875
  idle_worker_time: 1943.0625
  total_value_units: 25500.0
  total_value_structures: 2850.0
  killed_value_units: 6625.0
  killed_value_structures: 100.0
  collected_minerals: 34075.0
  collected_vespene: 11680.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 716.0
  spent_minerals: 25068.0
  spent_vespene: 8250.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4350.0
    economy: 1000.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11175.0
    economy: 1850.0
    technology: 143.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 6500.0
    technology: 1575.0
    upgrade: 1975.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 700.0
    upgrade: 1975.0
  }
  total_used_minerals {
    none: 0.0
    army: 13200.0
    economy: 9575.0
    technology: 2175.0
    upgrade: 1975.0
  }
  total_used_vespene {
    none: 0.0
    army: 5225.0
    economy: 0.0
    technology: 950.0
    upgrade: 1975.0
  }
  total_damage_dealt {
    life: 7767.21484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21803.546875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 9057
vespene: 3430
food_cap: 200
food_used: 69
food_army: 32
food_workers: 37
idle_worker_count: 7
army_count: 5
warp_gate_count: 0

game_loop:  22999
ui_data{
 
Score:  27612
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
