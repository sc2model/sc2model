----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2020
  player_apm: 61
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: -36400
  player_apm: 67
}
game_duration_loops: 15362
game_duration_seconds: 685.85144043
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11079
score_details {
  idle_production_time: 1430.4375
  idle_worker_time: 297.625
  total_value_units: 4025.0
  total_value_structures: 4300.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 9040.0
  collected_vespene: 1664.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 604.0
  spent_minerals: 7600.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4050.0
    technology: 2200.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1300.0
    economy: 4050.0
    technology: 2200.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 136.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 210.0
    shields: 276.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1490
vespene: 139
food_cap: 86
food_used: 71
food_army: 26
food_workers: 45
idle_worker_count: 1
army_count: 9
warp_gate_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 63
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 72
  count: 1
}
single {
  unit {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 200
  }
}

score{
 score_type: Melee
score: 15983
score_details {
  idle_production_time: 3425.1875
  idle_worker_time: 592.3125
  total_value_units: 11875.0
  total_value_structures: 5300.0
  killed_value_units: 4175.0
  killed_value_structures: 750.0
  collected_minerals: 15440.0
  collected_vespene: 4268.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 671.0
  spent_minerals: 14375.0
  spent_vespene: 3650.0
  food_used {
    none: 0.0
    army: 64.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3400.0
    economy: 650.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3400.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3700.0
    economy: 4950.0
    technology: 2200.0
    upgrade: 925.0
  }
  used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 300.0
    upgrade: 925.0
  }
  total_used_minerals {
    none: 0.0
    army: 7200.0
    economy: 5050.0
    technology: 2200.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 300.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 6986.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2816.0
    shields: 2851.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1115
vespene: 618
food_cap: 141
food_used: 109
food_army: 64
food_workers: 45
idle_worker_count: 1
army_count: 32
warp_gate_count: 8

game_loop:  15362
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 63
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 72
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 26
    energy: 100
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 136
    player_relative: 1
    health: 80
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 44
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 57
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 65
    shields: 0
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

Score:  15983
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
