----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4999
  player_apm: 291
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4953
  player_apm: 358
}
game_duration_loops: 14144
game_duration_seconds: 631.47265625
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11184
score_details {
  idle_production_time: 1220.4375
  idle_worker_time: 104.0625
  total_value_units: 6200.0
  total_value_structures: 4300.0
  killed_value_units: 1500.0
  killed_value_structures: 0.0
  collected_minerals: 9345.0
  collected_vespene: 2464.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 671.0
  spent_minerals: 8225.0
  spent_vespene: 1600.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1600.0
    economy: 4450.0
    technology: 1650.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2800.0
    economy: 4600.0
    technology: 1650.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2069.3984375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1296.0
    shields: 1522.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1170
vespene: 864
food_cap: 126
food_used: 76
food_army: 33
food_workers: 43
idle_worker_count: 0
army_count: 15
warp_gate_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 8
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 11274
score_details {
  idle_production_time: 2470.25
  idle_worker_time: 104.0625
  total_value_units: 12575.0
  total_value_structures: 4850.0
  killed_value_units: 6925.0
  killed_value_structures: 125.0
  collected_minerals: 14185.0
  collected_vespene: 4464.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 671.0
  spent_minerals: 13125.0
  spent_vespene: 3825.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4925.0
    economy: 1100.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6200.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 4650.0
    technology: 1800.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 450.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 7600.0
    economy: 4800.0
    technology: 1800.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 3675.0
    economy: 0.0
    technology: 450.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 8353.390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4706.0
    shields: 5301.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1110
vespene: 639
food_cap: 142
food_used: 69
food_army: 26
food_workers: 43
idle_worker_count: 0
army_count: 11
warp_gate_count: 8

game_loop:  14144
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 8
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 801
    player_relative: 1
    health: 90
    shields: 50
    energy: 0
  }
  units {
    unit_type: 801
    player_relative: 1
    health: 90
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 801
    player_relative: 1
    health: 90
    shields: 50
    energy: 0
  }
  units {
    unit_type: 801
    player_relative: 1
    health: 90
    shields: 50
    energy: 0
  }
  units {
    unit_type: 801
    player_relative: 1
    health: 90
    shields: 50
    energy: 0
  }
}

Score:  11274
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
