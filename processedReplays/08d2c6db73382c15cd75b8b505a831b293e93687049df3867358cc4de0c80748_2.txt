----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4934
  player_apm: 229
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4617
  player_apm: 291
}
game_duration_loops: 17105
game_duration_seconds: 763.669372559
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14062
score_details {
  idle_production_time: 882.8125
  idle_worker_time: 597.3125
  total_value_units: 6550.0
  total_value_structures: 4575.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 11465.0
  collected_vespene: 2472.0
  collection_rate_minerals: 2911.0
  collection_rate_vespene: 694.0
  spent_minerals: 10675.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 68.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 850.0
    economy: 50.0
    technology: -75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2100.0
    economy: 6450.0
    technology: 1800.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 525.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 6700.0
    technology: 1650.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 1493.96875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1124.16992188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 286.278320312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 840
vespene: 847
food_cap: 125
food_used: 111
food_army: 43
food_workers: 65
idle_worker_count: 5
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 134
    shields: 0
    energy: 35
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 79
    transport_slots_taken: 5
  }
}

score{
 score_type: Melee
score: 12505
score_details {
  idle_production_time: 4306.25
  idle_worker_time: 3148.25
  total_value_units: 17475.0
  total_value_structures: 6575.0
  killed_value_units: 12500.0
  killed_value_structures: 0.0
  collected_minerals: 22150.0
  collected_vespene: 5580.0
  collection_rate_minerals: 167.0
  collection_rate_vespene: 313.0
  spent_minerals: 22175.0
  spent_vespene: 3925.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 14.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8150.0
    economy: 5525.0
    technology: 375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 50.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2550.0
    economy: 2825.0
    technology: 1950.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 675.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 10750.0
    economy: 8850.0
    technology: 2400.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 675.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 17334.7519531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24910.59375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5170.06835938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 25
vespene: 1655
food_cap: 94
food_used: 67
food_army: 53
food_workers: 14
idle_worker_count: 5
army_count: 25
warp_gate_count: 0

game_loop:  17105
ui_data{
 multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 987
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  12505
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
