----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 82
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 107
}
game_duration_loops: 21118
game_duration_seconds: 942.833618164
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7767
score_details {
  idle_production_time: 2393.0
  idle_worker_time: 344.875
  total_value_units: 3850.0
  total_value_structures: 3275.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 6585.0
  collected_vespene: 1732.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 313.0
  spent_minerals: 6250.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 850.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1725.0
    economy: 2800.0
    technology: 1375.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 450.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2025.0
    economy: 3050.0
    technology: 1375.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 450.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 588.0
    shields: 1056.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1266.18212891
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 12.744140625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 385
vespene: 107
food_cap: 78
food_used: 58
food_army: 34
food_workers: 24
idle_worker_count: 2
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
production {
  unit {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  build_queue {
    unit_type: 498
    build_progress: 0.078125
  }
  build_queue {
    unit_type: 498
    build_progress: 0.0734375119209
  }
  build_queue {
    unit_type: 498
    build_progress: 0.0
  }
  build_queue {
    unit_type: 498
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 9489
score_details {
  idle_production_time: 8844.4375
  idle_worker_time: 2988.9375
  total_value_units: 8625.0
  total_value_structures: 5700.0
  killed_value_units: 13825.0
  killed_value_structures: 0.0
  collected_minerals: 12450.0
  collected_vespene: 3964.0
  collection_rate_minerals: 223.0
  collection_rate_vespene: 156.0
  spent_minerals: 12425.0
  spent_vespene: 3275.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9000.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4550.0
    economy: 1500.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 925.0
    economy: 2250.0
    technology: 2950.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 800.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 5375.0
    economy: 4100.0
    technology: 3050.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 800.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 6816.0
    shields: 7430.125
    energy: 0.0
  }
  total_damage_taken {
    life: 10307.2099609
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1210.04174805
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 75
vespene: 689
food_cap: 78
food_used: 34
food_army: 21
food_workers: 13
idle_worker_count: 6
army_count: 8
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7875
score_details {
  idle_production_time: 9680.4375
  idle_worker_time: 3547.0625
  total_value_units: 9200.0
  total_value_structures: 5700.0
  killed_value_units: 14575.0
  killed_value_structures: 0.0
  collected_minerals: 12565.0
  collected_vespene: 3992.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 12562.0
  spent_vespene: 3512.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 10.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9500.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5200.0
    economy: 2095.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1500.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 375.0
    economy: 1700.0
    technology: 2750.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 800.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 5725.0
    economy: 4100.0
    technology: 3050.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 800.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 7147.0
    shields: 7689.875
    energy: 0.0
  }
  total_damage_taken {
    life: 12259.7871094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1382.57543945
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 45
vespene: 480
food_cap: 63
food_used: 18
food_army: 8
food_workers: 10
idle_worker_count: 10
army_count: 4
warp_gate_count: 0

game_loop:  21118
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  7875
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
