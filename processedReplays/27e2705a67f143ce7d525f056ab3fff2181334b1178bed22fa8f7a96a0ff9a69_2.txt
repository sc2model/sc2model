----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3141
  player_apm: 116
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2997
  player_apm: 57
}
game_duration_loops: 41866
game_duration_seconds: 1869.14831543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8484
score_details {
  idle_production_time: 2662.875
  idle_worker_time: 510.125
  total_value_units: 3250.0
  total_value_structures: 3000.0
  killed_value_units: 625.0
  killed_value_structures: 0.0
  collected_minerals: 6070.0
  collected_vespene: 1532.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 67.0
  spent_minerals: 5787.0
  spent_vespene: 1431.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 425.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1650.0
    economy: 3075.0
    technology: 1550.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 500.0
    economy: 0.0
    technology: 575.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 1500.0
    economy: 2575.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 500.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1014.17480469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 440.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 333
vespene: 101
food_cap: 55
food_used: 55
food_army: 33
food_workers: 22
idle_worker_count: 0
army_count: 22
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18546
score_details {
  idle_production_time: 9611.8125
  idle_worker_time: 860.9375
  total_value_units: 9700.0
  total_value_structures: 4975.0
  killed_value_units: 8875.0
  killed_value_structures: 0.0
  collected_minerals: 14125.0
  collected_vespene: 5752.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 851.0
  spent_minerals: 13405.0
  spent_vespene: 4356.0
  food_used {
    none: 0.0
    army: 77.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6825.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 193.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 175.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3850.0
    economy: 5500.0
    technology: 2300.0
    upgrade: 775.0
  }
  used_vespene {
    none: 50.0
    army: 1825.0
    economy: 300.0
    technology: 1125.0
    upgrade: 775.0
  }
  total_used_minerals {
    none: 50.0
    army: 5050.0
    economy: 6200.0
    technology: 1550.0
    upgrade: 675.0
  }
  total_used_vespene {
    none: 50.0
    army: 2100.0
    economy: 600.0
    technology: 575.0
    upgrade: 675.0
  }
  total_damage_dealt {
    life: 10807.8779297
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4163.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2205.18212891
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 638
vespene: 1358
food_cap: 141
food_used: 121
food_army: 77
food_workers: 44
idle_worker_count: 0
army_count: 47
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 5
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 21036
score_details {
  idle_production_time: 22954.375
  idle_worker_time: 1946.4375
  total_value_units: 16625.0
  total_value_structures: 7750.0
  killed_value_units: 28350.0
  killed_value_structures: 350.0
  collected_minerals: 22565.0
  collected_vespene: 9852.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 313.0
  spent_minerals: 21655.0
  spent_vespene: 9206.0
  food_used {
    none: 0.0
    army: 62.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 20225.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6200.0
    economy: 2925.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2250.0
    economy: 338.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 450.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3750.0
    economy: 4250.0
    technology: 2800.0
    upgrade: 2100.0
  }
  used_vespene {
    none: 50.0
    army: 2925.0
    economy: 150.0
    technology: 1425.0
    upgrade: 2100.0
  }
  total_used_minerals {
    none: 50.0
    army: 9400.0
    economy: 7275.0
    technology: 2800.0
    upgrade: 1675.0
  }
  total_used_vespene {
    none: 50.0
    army: 4575.0
    economy: 900.0
    technology: 1425.0
    upgrade: 1675.0
  }
  total_damage_dealt {
    life: 34570.609375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16378.7011719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5178.32617188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 828
vespene: 608
food_cap: 142
food_used: 85
food_army: 62
food_workers: 23
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 6
}
multi {
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15142
score_details {
  idle_production_time: 39522.125
  idle_worker_time: 4164.8125
  total_value_units: 21425.0
  total_value_structures: 9050.0
  killed_value_units: 43200.0
  killed_value_structures: 350.0
  collected_minerals: 25995.0
  collected_vespene: 10408.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 0.0
  spent_minerals: 25555.0
  spent_vespene: 10256.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 30625.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 11725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10300.0
    economy: 4125.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5150.0
    economy: 488.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 750.0
    economy: 700.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1600.0
    economy: 4100.0
    technology: 2800.0
    upgrade: 2100.0
  }
  used_vespene {
    none: 50.0
    army: 550.0
    economy: 150.0
    technology: 1425.0
    upgrade: 2100.0
  }
  total_used_minerals {
    none: 50.0
    army: 12650.0
    economy: 8925.0
    technology: 2800.0
    upgrade: 2100.0
  }
  total_used_vespene {
    none: 50.0
    army: 6075.0
    economy: 1200.0
    technology: 1425.0
    upgrade: 2100.0
  }
  total_damage_dealt {
    life: 49576.6875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25433.5878906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 7781.27050781
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 168
vespene: 49
food_cap: 172
food_used: 39
food_army: 30
food_workers: 8
idle_worker_count: 2
army_count: 21
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 6
}
single {
  unit {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12916
score_details {
  idle_production_time: 42575.0625
  idle_worker_time: 4195.3125
  total_value_units: 21575.0
  total_value_structures: 9200.0
  killed_value_units: 44700.0
  killed_value_structures: 350.0
  collected_minerals: 26265.0
  collected_vespene: 10412.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 25655.0
  spent_vespene: 10256.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 3.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 31950.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 11900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11550.0
    economy: 5100.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5450.0
    economy: 488.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 750.0
    economy: 825.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 350.0
    economy: 3250.0
    technology: 2800.0
    upgrade: 2100.0
  }
  used_vespene {
    none: 50.0
    army: 250.0
    economy: 150.0
    technology: 1425.0
    upgrade: 2100.0
  }
  total_used_minerals {
    none: 50.0
    army: 12650.0
    economy: 9225.0
    technology: 2800.0
    upgrade: 2100.0
  }
  total_used_vespene {
    none: 50.0
    army: 6075.0
    economy: 1200.0
    technology: 1425.0
    upgrade: 2100.0
  }
  total_damage_dealt {
    life: 51791.6484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 29989.1054688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 7781.27050781
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 338
vespene: 53
food_cap: 157
food_used: 10
food_army: 7
food_workers: 3
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  41866
ui_data{
 
Score:  12916
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
