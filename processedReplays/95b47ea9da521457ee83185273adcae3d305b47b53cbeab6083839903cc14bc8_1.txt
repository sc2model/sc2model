----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3289
  player_apm: 137
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 184
}
game_duration_loops: 12128
game_duration_seconds: 541.466369629
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9553
score_details {
  idle_production_time: 1435.375
  idle_worker_time: 1236.0
  total_value_units: 3700.0
  total_value_structures: 3075.0
  killed_value_units: 550.0
  killed_value_structures: 0.0
  collected_minerals: 7305.0
  collected_vespene: 2048.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 470.0
  spent_minerals: 6225.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 600.0
    economy: 3875.0
    technology: 1400.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 500.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 1000.0
    economy: 4475.0
    technology: 1150.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1000.625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1042.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1130
vespene: 598
food_cap: 86
food_used: 51
food_army: 12
food_workers: 39
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
single {
  unit {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 51
  }
}

score{
 score_type: Melee
score: 9953
score_details {
  idle_production_time: 2112.5625
  idle_worker_time: 1236.0
  total_value_units: 5775.0
  total_value_structures: 4150.0
  killed_value_units: 1550.0
  killed_value_structures: 0.0
  collected_minerals: 9755.0
  collected_vespene: 2848.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 537.0
  spent_minerals: 9550.0
  spent_vespene: 2450.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2000.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 950.0
    economy: 4050.0
    technology: 1850.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 600.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 4650.0
    technology: 1850.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 600.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 2540.40380859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5412.59667969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 47.7709960938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 255
vespene: 398
food_cap: 54
food_used: 60
food_army: 19
food_workers: 41
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  12128
ui_data{
 multi {
  units {
    unit_type: 56
    player_relative: 1
    health: 96
    shields: 0
    energy: 26
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 50
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 53
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
}

Score:  9953
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
