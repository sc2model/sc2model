----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2560
  player_apm: 181
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2541
  player_apm: 92
}
game_duration_loops: 32105
game_duration_seconds: 1433.35900879
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9126
score_details {
  idle_production_time: 1347.375
  idle_worker_time: 204.5625
  total_value_units: 4225.0
  total_value_structures: 3200.0
  killed_value_units: 1000.0
  killed_value_structures: 200.0
  collected_minerals: 6640.0
  collected_vespene: 2036.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 425.0
  spent_minerals: 5625.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1525.0
    economy: 3300.0
    technology: 1250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1525.0
    economy: 3550.0
    technology: 1250.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1831.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 304.919433594
    shields: 503.294433594
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1065
vespene: 786
food_cap: 78
food_used: 58
food_army: 26
food_workers: 32
idle_worker_count: 4
army_count: 13
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 14746
score_details {
  idle_production_time: 5083.3125
  idle_worker_time: 1077.625
  total_value_units: 11900.0
  total_value_structures: 6250.0
  killed_value_units: 4775.0
  killed_value_structures: 200.0
  collected_minerals: 15205.0
  collected_vespene: 5116.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 403.0
  spent_minerals: 15075.0
  spent_vespene: 4825.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3625.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3325.0
    economy: 1150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3500.0
    economy: 5100.0
    technology: 1900.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 2075.0
    economy: 0.0
    technology: 700.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 6325.0
    economy: 6100.0
    technology: 1900.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 3125.0
    economy: 0.0
    technology: 700.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 4782.22021484
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3961.06787109
    shields: 4924.14599609
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 180
vespene: 291
food_cap: 157
food_used: 101
food_army: 60
food_workers: 38
idle_worker_count: 0
army_count: 28
warp_gate_count: 5

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 63
  count: 1
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 19115
score_details {
  idle_production_time: 10795.25
  idle_worker_time: 2951.0
  total_value_units: 19850.0
  total_value_structures: 9050.0
  killed_value_units: 15275.0
  killed_value_structures: 1500.0
  collected_minerals: 23500.0
  collected_vespene: 9240.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 313.0
  spent_minerals: 22850.0
  spent_vespene: 8000.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11375.0
    economy: 1800.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7975.0
    economy: 2525.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3325.0
    economy: 6325.0
    technology: 2950.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 1000.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 10550.0
    economy: 8500.0
    technology: 2950.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 5900.0
    economy: 0.0
    technology: 1000.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 18173.1484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9573.91992188
    shields: 10857.6464844
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 700
vespene: 1240
food_cap: 200
food_used: 97
food_army: 57
food_workers: 40
idle_worker_count: 4
army_count: 23
warp_gate_count: 6

game_loop:  30000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 63
  count: 1
}
multi {
  units {
    unit_type: 80
    player_relative: 1
    health: 82
    shields: 11
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 66
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 12
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 2
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 7
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 66
    shields: 46
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 22
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 60
    shields: 0
    energy: 161
  }
}

score{
 score_type: Melee
score: 15916
score_details {
  idle_production_time: 12108.0625
  idle_worker_time: 3506.375
  total_value_units: 22350.0
  total_value_structures: 9525.0
  killed_value_units: 17000.0
  killed_value_structures: 1700.0
  collected_minerals: 25525.0
  collected_vespene: 9816.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 425.0
  spent_minerals: 24275.0
  spent_vespene: 8750.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12700.0
    economy: 1800.0
    technology: 800.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11525.0
    economy: 2525.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1025.0
    economy: 6500.0
    technology: 2950.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 1000.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 12050.0
    economy: 9075.0
    technology: 2950.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 6800.0
    economy: 0.0
    technology: 1000.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 20304.6933594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11488.9199219
    shields: 12749.3964844
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1300
vespene: 1066
food_cap: 200
food_used: 59
food_army: 17
food_workers: 42
idle_worker_count: 4
army_count: 4
warp_gate_count: 6

game_loop:  32105
ui_data{
 
Score:  15916
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
