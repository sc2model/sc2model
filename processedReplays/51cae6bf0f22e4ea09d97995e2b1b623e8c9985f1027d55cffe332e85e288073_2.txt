----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3985
  player_apm: 128
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4155
  player_apm: 214
}
game_duration_loops: 29399
game_duration_seconds: 1312.54699707
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9560
score_details {
  idle_production_time: 9512.5
  idle_worker_time: 52.3125
  total_value_units: 5600.0
  total_value_structures: 1875.0
  killed_value_units: 550.0
  killed_value_structures: 100.0
  collected_minerals: 8225.0
  collected_vespene: 1384.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 515.0
  spent_minerals: 7774.0
  spent_vespene: 575.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 275.0
    economy: 100.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 625.0
    economy: 56.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 425.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 4650.0
    technology: 1500.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 150.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 5200.0
    technology: 1525.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 773.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1553.05517578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 501
vespene: 809
food_cap: 66
food_used: 66
food_army: 20
food_workers: 42
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 26
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 89
  count: 4
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 69
  }
}

score{
 score_type: Melee
score: 25280
score_details {
  idle_production_time: 38902.5625
  idle_worker_time: 177.1875
  total_value_units: 22200.0
  total_value_structures: 4000.0
  killed_value_units: 6600.0
  killed_value_structures: 925.0
  collected_minerals: 25595.0
  collected_vespene: 8632.0
  collection_rate_minerals: 2519.0
  collection_rate_vespene: 1119.0
  spent_minerals: 25323.0
  spent_vespene: 6874.0
  food_used {
    none: 0.5
    army: 93.5
    economy: 71.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4450.0
    economy: 1500.0
    technology: 425.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5974.0
    economy: 806.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1299.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 850.0
    economy: 425.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4775.0
    economy: 9000.0
    technology: 2550.0
    upgrade: 1725.0
  }
  used_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 950.0
    upgrade: 1725.0
  }
  total_used_minerals {
    none: 0.0
    army: 12050.0
    economy: 11450.0
    technology: 3000.0
    upgrade: 1525.0
  }
  total_used_vespene {
    none: 0.0
    army: 3900.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1525.0
  }
  total_damage_dealt {
    life: 8283.99902344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12440.5634766
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 322
vespene: 1758
food_cap: 200
food_used: 165
food_army: 93
food_workers: 71
idle_worker_count: 0
army_count: 45
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 109
  count: 45
}
groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 89
  count: 6
}
multi {
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26320
score_details {
  idle_production_time: 77243.4375
  idle_worker_time: 1440.6875
  total_value_units: 40800.0
  total_value_structures: 4875.0
  killed_value_units: 24150.0
  killed_value_structures: 6100.0
  collected_minerals: 42280.0
  collected_vespene: 14784.0
  collection_rate_minerals: 2883.0
  collection_rate_vespene: 627.0
  spent_minerals: 41659.0
  spent_vespene: 14460.0
  food_used {
    none: 0.0
    army: 87.5
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15225.0
    economy: 8675.0
    technology: 825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4975.0
    economy: 450.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 19010.0
    economy: 2006.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7835.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1500.0
    economy: 775.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5175.0
    economy: 9725.0
    technology: 2825.0
    upgrade: 1725.0
  }
  used_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 950.0
    upgrade: 1725.0
  }
  total_used_minerals {
    none: 0.0
    army: 27650.0
    economy: 13750.0
    technology: 3275.0
    upgrade: 1725.0
  }
  total_used_vespene {
    none: 0.0
    army: 12750.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1725.0
  }
  total_damage_dealt {
    life: 36808.578125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 42748.5195312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 671
vespene: 324
food_cap: 200
food_used: 154
food_army: 87
food_workers: 67
idle_worker_count: 3
army_count: 42
warp_gate_count: 0

game_loop:  29399
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 23
}
groups {
  control_group_index: 2
  leader_unit_type: 112
  count: 24
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 89
  count: 6
}
multi {
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 155
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 121
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 159
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 160
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 113
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
    add_on {
      unit_type: 114
      build_progress: 0.0923621058464
    }
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  26320
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
