----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3600
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4167
  player_apm: 176
}
game_duration_loops: 33308
game_duration_seconds: 1487.06799316
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13369
score_details {
  idle_production_time: 6001.375
  idle_worker_time: 3.5
  total_value_units: 6600.0
  total_value_structures: 1750.0
  killed_value_units: 250.0
  killed_value_structures: 0.0
  collected_minerals: 10505.0
  collected_vespene: 1864.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 671.0
  spent_minerals: 8900.0
  spent_vespene: 1800.0
  food_used {
    none: 0.0
    army: 50.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2425.0
    economy: 5500.0
    technology: 1000.0
    upgrade: 925.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 200.0
    upgrade: 925.0
  }
  total_used_minerals {
    none: 0.0
    army: 1775.0
    economy: 6000.0
    technology: 1150.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 300.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 783.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 755.955566406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1655
vespene: 64
food_cap: 114
food_used: 106
food_army: 50
food_workers: 52
idle_worker_count: 0
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 30841
score_details {
  idle_production_time: 21419.3125
  idle_worker_time: 204.0
  total_value_units: 17950.0
  total_value_structures: 4450.0
  killed_value_units: 7575.0
  killed_value_structures: 0.0
  collected_minerals: 28420.0
  collected_vespene: 10196.0
  collection_rate_minerals: 3135.0
  collection_rate_vespene: 1567.0
  spent_minerals: 23475.0
  spent_vespene: 8375.0
  food_used {
    none: 0.0
    army: 85.0
    economy: 82.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5300.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5150.0
    economy: 975.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4500.0
    economy: 9325.0
    technology: 2450.0
    upgrade: 2125.0
  }
  used_vespene {
    none: 0.0
    army: 2650.0
    economy: 0.0
    technology: 850.0
    upgrade: 2125.0
  }
  total_used_minerals {
    none: 0.0
    army: 9075.0
    economy: 11550.0
    technology: 2800.0
    upgrade: 1725.0
  }
  total_used_vespene {
    none: 0.0
    army: 6275.0
    economy: 0.0
    technology: 1100.0
    upgrade: 1725.0
  }
  total_damage_dealt {
    life: 8527.56445312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13671.6416016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 4995
vespene: 1821
food_cap: 200
food_used: 167
food_army: 85
food_workers: 82
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 1
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 55
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 23
    shields: 0
    energy: 70
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 127
    shields: 0
    energy: 45
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 55
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 127
    shields: 0
    energy: 37
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 55
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 95
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 64
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 102
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 58
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 106
    shields: 0
    energy: 70
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 68
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 127
    shields: 0
    energy: 69
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 84
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 35064
score_details {
  idle_production_time: 45149.4375
  idle_worker_time: 1200.6875
  total_value_units: 48650.0
  total_value_structures: 6025.0
  killed_value_units: 24425.0
  killed_value_structures: 2575.0
  collected_minerals: 49995.0
  collected_vespene: 21144.0
  collection_rate_minerals: 2883.0
  collection_rate_vespene: 1612.0
  spent_minerals: 46950.0
  spent_vespene: 20575.0
  food_used {
    none: 0.0
    army: 114.0
    economy: 84.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15800.0
    economy: 3000.0
    technology: 825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7125.0
    economy: 150.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 22675.0
    economy: 975.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 12950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5850.0
    economy: 11750.0
    technology: 3100.0
    upgrade: 3200.0
  }
  used_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 1250.0
    upgrade: 3200.0
  }
  total_used_minerals {
    none: 0.0
    army: 30775.0
    economy: 14525.0
    technology: 3400.0
    upgrade: 2600.0
  }
  total_used_vespene {
    none: 0.0
    army: 18850.0
    economy: 0.0
    technology: 1650.0
    upgrade: 2600.0
  }
  total_damage_dealt {
    life: 32250.6992188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 42150.8984375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3095
vespene: 569
food_cap: 200
food_used: 198
food_army: 114
food_workers: 84
idle_worker_count: 0
army_count: 78
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 7
}
groups {
  control_group_index: 1
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 158
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 96
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 174
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 113
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
    add_on {
      unit_type: 114
      build_progress: 0.772147059441
    }
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 34243
score_details {
  idle_production_time: 54153.1875
  idle_worker_time: 1258.0
  total_value_units: 58050.0
  total_value_structures: 6725.0
  killed_value_units: 30975.0
  killed_value_structures: 3350.0
  collected_minerals: 56040.0
  collected_vespene: 25028.0
  collection_rate_minerals: 2351.0
  collection_rate_vespene: 1411.0
  spent_minerals: 54850.0
  spent_vespene: 24875.0
  food_used {
    none: 0.0
    army: 98.0
    economy: 99.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 19200.0
    economy: 5175.0
    technology: 825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8725.0
    economy: 300.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 29625.0
    economy: 1025.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 16900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5300.0
    economy: 12500.0
    technology: 4000.0
    upgrade: 3200.0
  }
  used_vespene {
    none: 0.0
    army: 3400.0
    economy: 0.0
    technology: 1250.0
    upgrade: 3200.0
  }
  total_used_minerals {
    none: 0.0
    army: 37775.0
    economy: 15125.0
    technology: 4450.0
    upgrade: 3200.0
  }
  total_used_vespene {
    none: 0.0
    army: 23650.0
    economy: 0.0
    technology: 1650.0
    upgrade: 3200.0
  }
  total_damage_dealt {
    life: 42023.328125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 54930.4609375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1240
vespene: 153
food_cap: 200
food_used: 197
food_army: 98
food_workers: 89
idle_worker_count: 6
army_count: 37
warp_gate_count: 0

game_loop:  33308
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 7
}
groups {
  control_group_index: 1
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

Score:  34243
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
