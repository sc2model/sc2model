----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4180
  player_apm: 92
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3826
  player_apm: 86
}
game_duration_loops: 14731
game_duration_seconds: 657.67980957
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10442
score_details {
  idle_production_time: 1270.125
  idle_worker_time: 579.625
  total_value_units: 5375.0
  total_value_structures: 3850.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 8300.0
  collected_vespene: 1992.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 313.0
  spent_minerals: 7500.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: -450.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4700.0
    technology: 1200.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 4300.0
    technology: 1400.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 412.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1126.0
    shields: 1346.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 850
vespene: 217
food_cap: 110
food_used: 79
food_army: 35
food_workers: 44
idle_worker_count: 2
army_count: 17
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 53
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 128
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

score{
 score_type: Melee
score: 14095
score_details {
  idle_production_time: 2640.625
  idle_worker_time: 1101.9375
  total_value_units: 8675.0
  total_value_structures: 7000.0
  killed_value_units: 1325.0
  killed_value_structures: 0.0
  collected_minerals: 15640.0
  collected_vespene: 3880.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 783.0
  spent_minerals: 15025.0
  spent_vespene: 3350.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3400.0
    economy: 800.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 675.0
    economy: 6300.0
    technology: 2800.0
    upgrade: 950.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 550.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 0.0
    army: 3775.0
    economy: 7150.0
    technology: 2550.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 750.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1942.74755859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5786.21679688
    shields: 5197.09179688
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 665
vespene: 530
food_cap: 158
food_used: 72
food_army: 14
food_workers: 58
idle_worker_count: 3
army_count: 5
warp_gate_count: 5

game_loop:  14731
ui_data{
 
Score:  14095
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
