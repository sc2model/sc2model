----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2614
  player_apm: 64
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2803
  player_apm: 88
}
game_duration_loops: 36929
game_duration_seconds: 1648.7310791
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8776
score_details {
  idle_production_time: 1197.0
  idle_worker_time: 942.375
  total_value_units: 3750.0
  total_value_structures: 2700.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 7740.0
  collected_vespene: 1336.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 246.0
  spent_minerals: 6525.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1500.0
    economy: 3300.0
    technology: 1125.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 225.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4100.0
    technology: 1125.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 225.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 187.0
    shields: 245.0
    energy: 0.0
  }
  total_damage_taken {
    life: 500.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1265
vespene: 411
food_cap: 62
food_used: 62
food_army: 29
food_workers: 33
idle_worker_count: 5
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
single {
  unit {
    unit_type: 34
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15978
score_details {
  idle_production_time: 4712.3125
  idle_worker_time: 3757.875
  total_value_units: 10375.0
  total_value_structures: 4425.0
  killed_value_units: 5725.0
  killed_value_structures: 0.0
  collected_minerals: 16770.0
  collected_vespene: 3408.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 447.0
  spent_minerals: 12975.0
  spent_vespene: 2975.0
  food_used {
    none: 0.0
    army: 69.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3200.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2850.0
    economy: 800.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 3550.0
    economy: 3800.0
    technology: 1775.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 550.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 6200.0
    economy: 4900.0
    technology: 1875.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 550.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 2794.0
    shields: 3422.125
    energy: 0.0
  }
  total_damage_taken {
    life: 3942.54638672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 379.069335938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3845
vespene: 433
food_cap: 102
food_used: 99
food_army: 69
food_workers: 30
idle_worker_count: 6
army_count: 38
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18255
score_details {
  idle_production_time: 10368.6875
  idle_worker_time: 10322.125
  total_value_units: 17200.0
  total_value_structures: 5575.0
  killed_value_units: 10900.0
  killed_value_structures: 800.0
  collected_minerals: 19015.0
  collected_vespene: 5640.0
  collection_rate_minerals: 531.0
  collection_rate_vespene: 268.0
  spent_minerals: 18700.0
  spent_vespene: 5100.0
  food_used {
    none: 0.0
    army: 118.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5975.0
    economy: 1900.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4200.0
    economy: 1800.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 400.0
    army: 6600.0
    economy: 4075.0
    technology: 2175.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 750.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 10750.0
    economy: 6050.0
    technology: 2125.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 3800.0
    economy: 0.0
    technology: 650.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 7751.28955078
    shields: 8802.890625
    energy: 0.0
  }
  total_damage_taken {
    life: 6331.91064453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1050.55761719
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 365
vespene: 540
food_cap: 149
food_used: 136
food_army: 118
food_workers: 17
idle_worker_count: 10
army_count: 64
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 16
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9998
score_details {
  idle_production_time: 15152.4375
  idle_worker_time: 14338.125
  total_value_units: 18650.0
  total_value_structures: 5575.0
  killed_value_units: 23510.0
  killed_value_structures: 1350.0
  collected_minerals: 19365.0
  collected_vespene: 6708.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 67.0
  spent_minerals: 19400.0
  spent_vespene: 5250.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15110.0
    economy: 2100.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7050.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10350.0
    economy: 2800.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1250.0
    economy: 3075.0
    technology: 1875.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 750.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 11850.0
    economy: 6100.0
    technology: 2125.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 4100.0
    economy: 0.0
    technology: 650.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 16347.1738281
    shields: 16561.3359375
    energy: 0.0
  }
  total_damage_taken {
    life: 17259.7460938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1050.55761719
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 15
vespene: 1458
food_cap: 134
food_used: 35
food_army: 26
food_workers: 9
idle_worker_count: 8
army_count: 7
warp_gate_count: 0

game_loop:  36929
ui_data{
 single {
  unit {
    unit_type: 691
    player_relative: 1
    health: 81
    shields: 0
    energy: 0
  }
}

Score:  9998
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
