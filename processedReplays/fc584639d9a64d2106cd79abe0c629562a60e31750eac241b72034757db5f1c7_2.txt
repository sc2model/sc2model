----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4750
  player_apm: 295
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4807
  player_apm: 148
}
game_duration_loops: 17617
game_duration_seconds: 786.528076172
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6851
score_details {
  idle_production_time: 8387.75
  idle_worker_time: 1.125
  total_value_units: 4800.0
  total_value_structures: 1225.0
  killed_value_units: 825.0
  killed_value_structures: 0.0
  collected_minerals: 5925.0
  collected_vespene: 1032.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 358.0
  spent_minerals: 5756.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 725.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 925.0
    economy: -69.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1225.0
    economy: 3425.0
    technology: 875.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 150.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 3750.0
    technology: 1025.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2002.80566406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1168.26464844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 219
vespene: 82
food_cap: 76
food_used: 58
food_army: 24
food_workers: 34
idle_worker_count: 0
army_count: 13
warp_gate_count: 0
larva_count: 1

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 105
  count: 19
}
groups {
  control_group_index: 4
  leader_unit_type: 105
  count: 10
}
groups {
  control_group_index: 5
  leader_unit_type: 128
  count: 1
}
single {
  unit {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9099
score_details {
  idle_production_time: 20184.625
  idle_worker_time: 1.125
  total_value_units: 13100.0
  total_value_structures: 2075.0
  killed_value_units: 5400.0
  killed_value_structures: 0.0
  collected_minerals: 12825.0
  collected_vespene: 3780.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 559.0
  spent_minerals: 12331.0
  spent_vespene: 3400.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3125.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5650.0
    economy: 281.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 900.0
    economy: 4050.0
    technology: 1525.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 7175.0
    economy: 5150.0
    technology: 1675.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 9181.18652344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8962.59472656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 544
vespene: 380
food_cap: 114
food_used: 46
food_army: 18
food_workers: 28
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  17617
ui_data{
 
Score:  9099
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
