----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4662
  player_apm: 127
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3777
  player_apm: 92
}
game_duration_loops: 15969
game_duration_seconds: 712.951538086
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12669
score_details {
  idle_production_time: 1371.125
  idle_worker_time: 200.4375
  total_value_units: 4925.0
  total_value_structures: 4350.0
  killed_value_units: 400.0
  killed_value_structures: 200.0
  collected_minerals: 9595.0
  collected_vespene: 2624.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 649.0
  spent_minerals: 8675.0
  spent_vespene: 1600.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1300.0
    economy: 5575.0
    technology: 1850.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1225.0
    economy: 5150.0
    technology: 1700.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 821.650146484
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 112.0
    shields: 324.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 970
vespene: 1024
food_cap: 109
food_used: 81
food_army: 23
food_workers: 56
idle_worker_count: 1
army_count: 12
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 7
  leader_unit_type: 488
  count: 3
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 13915
score_details {
  idle_production_time: 3595.9375
  idle_worker_time: 821.25
  total_value_units: 14425.0
  total_value_structures: 8050.0
  killed_value_units: 4350.0
  killed_value_structures: 375.0
  collected_minerals: 19870.0
  collected_vespene: 6120.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 671.0
  spent_minerals: 18700.0
  spent_vespene: 5200.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3100.0
    economy: 525.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6775.0
    economy: 1875.0
    technology: 900.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3425.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 525.0
    economy: 5975.0
    technology: 2900.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 400.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 7425.0
    economy: 7850.0
    technology: 3650.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 4800.0
    economy: 0.0
    technology: 550.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 6623.46582031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7712.91699219
    shields: 9090.04296875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1220
vespene: 920
food_cap: 172
food_used: 63
food_army: 11
food_workers: 52
idle_worker_count: 1
army_count: 6
warp_gate_count: 11

game_loop:  15969
ui_data{
 
Score:  13915
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
