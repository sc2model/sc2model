----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3382
  player_apm: 155
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3281
  player_apm: 232
}
game_duration_loops: 8891
game_duration_seconds: 396.947357178
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9199
score_details {
  idle_production_time: 551.5
  idle_worker_time: 169.4375
  total_value_units: 4300.0
  total_value_structures: 2525.0
  killed_value_units: 2550.0
  killed_value_structures: 500.0
  collected_minerals: 6555.0
  collected_vespene: 1712.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 492.0
  spent_minerals: 4943.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1900.0
    economy: 1000.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: -57.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1300.0
    economy: 3625.0
    technology: 900.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1300.0
    economy: 3925.0
    technology: 900.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 6311.71826172
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 915.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1662
vespene: 312
food_cap: 78
food_used: 60
food_army: 26
food_workers: 34
idle_worker_count: 2
army_count: 24
warp_gate_count: 0

game_loop:  8891
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 49
  count: 20
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}

Score:  9199
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
