----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 55
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3227
  player_apm: 142
}
game_duration_loops: 5681
game_duration_seconds: 253.633773804
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5419
score_details {
  idle_production_time: 327.9375
  idle_worker_time: 294.8125
  total_value_units: 2325.0
  total_value_structures: 1550.0
  killed_value_units: 950.0
  killed_value_structures: 0.0
  collected_minerals: 3385.0
  collected_vespene: 984.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 313.0
  spent_minerals: 2700.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 2500.0
    technology: 450.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 700.0
    economy: 2100.0
    technology: 450.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 998.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 64.0
    shields: 203.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 735
vespene: 309
food_cap: 47
food_used: 37
food_army: 14
food_workers: 23
idle_worker_count: 2
army_count: 6
warp_gate_count: 0

game_loop:  5681
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 62
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}

Score:  5419
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
