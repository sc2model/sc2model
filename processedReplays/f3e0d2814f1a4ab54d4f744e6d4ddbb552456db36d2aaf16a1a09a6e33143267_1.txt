----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2622
  player_apm: 51
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2344
  player_apm: 30
}
game_duration_loops: 8599
game_duration_seconds: 383.910705566
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6903
score_details {
  idle_production_time: 1085.8125
  idle_worker_time: 193.5625
  total_value_units: 3750.0
  total_value_structures: 2575.0
  killed_value_units: 750.0
  killed_value_structures: 650.0
  collected_minerals: 5375.0
  collected_vespene: 1528.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 313.0
  spent_minerals: 5425.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 500.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 3025.0
    technology: 750.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 2975.0
    technology: 750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 4964.89453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 580.0
    shields: 456.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 0
vespene: 878
food_cap: 94
food_used: 52
food_army: 28
food_workers: 22
idle_worker_count: 0
army_count: 14
warp_gate_count: 4

game_loop:  8599
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

Score:  6903
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
