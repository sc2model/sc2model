----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3367
  player_apm: 165
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3305
  player_apm: 151
}
game_duration_loops: 20015
game_duration_seconds: 893.589111328
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12771
score_details {
  idle_production_time: 6315.0625
  idle_worker_time: 2.0625
  total_value_units: 7200.0
  total_value_structures: 2025.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 10570.0
  collected_vespene: 1332.0
  collection_rate_minerals: 2435.0
  collection_rate_vespene: 515.0
  spent_minerals: 10031.0
  spent_vespene: 1200.0
  food_used {
    none: 0.0
    army: 52.5
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 75.0
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2625.0
    economy: 6250.0
    technology: 1525.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 6650.0
    technology: 1675.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 477.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 608.221191406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 589
vespene: 132
food_cap: 114
food_used: 109
food_army: 52
food_workers: 57
idle_worker_count: 0
army_count: 26
warp_gate_count: 0
larva_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8978
score_details {
  idle_production_time: 31634.4375
  idle_worker_time: 3277.9375
  total_value_units: 18350.0
  total_value_structures: 2525.0
  killed_value_units: 7975.0
  killed_value_structures: 2500.0
  collected_minerals: 20280.0
  collected_vespene: 5004.0
  collection_rate_minerals: 27.0
  collection_rate_vespene: 223.0
  spent_minerals: 19306.0
  spent_vespene: 4150.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6375.0
    economy: 2750.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1100.0
    economy: 150.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9225.0
    economy: 2806.0
    technology: 975.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 750.0
    economy: 775.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: -200.0
    economy: 4625.0
    technology: 825.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 250.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 10625.0
    economy: 8150.0
    technology: 1825.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 350.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 19175.3261719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 28387.7148438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1024
vespene: 854
food_cap: 140
food_used: 24
food_army: 4
food_workers: 20
idle_worker_count: 2
army_count: 2
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 58
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8232
score_details {
  idle_production_time: 31646.375
  idle_worker_time: 3279.8125
  total_value_units: 18350.0
  total_value_structures: 2525.0
  killed_value_units: 7975.0
  killed_value_structures: 2500.0
  collected_minerals: 20280.0
  collected_vespene: 5008.0
  collection_rate_minerals: 27.0
  collection_rate_vespene: 201.0
  spent_minerals: 19306.0
  spent_vespene: 4150.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6375.0
    economy: 2750.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1100.0
    economy: 150.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9325.0
    economy: 2256.0
    technology: 975.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 750.0
    economy: 775.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: -300.0
    economy: 4025.0
    technology: 825.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 250.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 10625.0
    economy: 8150.0
    technology: 1825.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 350.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 19197.3261719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 28469.1171875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1024
vespene: 858
food_cap: 140
food_used: 24
food_army: 2
food_workers: 22
idle_worker_count: 2
army_count: 1
warp_gate_count: 0

game_loop:  20015
ui_data{
 
Score:  8232
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
