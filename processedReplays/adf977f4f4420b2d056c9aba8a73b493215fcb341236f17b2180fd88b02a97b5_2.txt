----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 90
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2608
  player_apm: 61
}
game_duration_loops: 6086
game_duration_seconds: 271.715393066
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5195
score_details {
  idle_production_time: 730.9375
  idle_worker_time: 452.75
  total_value_units: 1350.0
  total_value_structures: 2200.0
  killed_value_units: 325.0
  killed_value_structures: 500.0
  collected_minerals: 3675.0
  collected_vespene: 620.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 358.0
  spent_minerals: 2500.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 325.0
    economy: 350.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 2300.0
    technology: 900.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 100.0
    economy: 2300.0
    technology: 1050.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2252.23388672
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 150.0
    shields: 390.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1225
vespene: 470
food_cap: 63
food_used: 25
food_army: 2
food_workers: 23
idle_worker_count: 2
army_count: 1
warp_gate_count: 0

game_loop:  6086
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
single {
  unit {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 67
  }
}

Score:  5195
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
