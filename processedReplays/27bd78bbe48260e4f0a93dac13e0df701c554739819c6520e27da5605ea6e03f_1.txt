----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3634
  player_apm: 165
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3600
  player_apm: 62
}
game_duration_loops: 20204
game_duration_seconds: 902.02722168
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8969
score_details {
  idle_production_time: 983.0625
  idle_worker_time: 566.5625
  total_value_units: 4625.0
  total_value_structures: 3600.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 7660.0
  collected_vespene: 1484.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 649.0
  spent_minerals: 7250.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 600.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1450.0
    economy: 3950.0
    technology: 1475.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 425.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4750.0
    technology: 1475.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 100.0
    shields: 180.0
    energy: 0.0
  }
  total_damage_taken {
    life: 820.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 15.736328125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 460
vespene: 134
food_cap: 86
food_used: 66
food_army: 29
food_workers: 35
idle_worker_count: 2
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 61
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 100
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 25059
score_details {
  idle_production_time: 7563.4375
  idle_worker_time: 1148.875
  total_value_units: 14575.0
  total_value_structures: 7650.0
  killed_value_units: 10700.0
  killed_value_structures: 1700.0
  collected_minerals: 21685.0
  collected_vespene: 6224.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 716.0
  spent_minerals: 20375.0
  spent_vespene: 4675.0
  food_used {
    none: 0.0
    army: 141.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6100.0
    economy: 2950.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2400.0
    economy: 600.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7050.0
    economy: 7100.0
    technology: 2850.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 800.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 0.0
    army: 9150.0
    economy: 8300.0
    technology: 2950.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 800.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 10619.0
    shields: 10293.25
    energy: 0.0
  }
  total_damage_taken {
    life: 3572.11083984
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2682.95556641
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1360
vespene: 1549
food_cap: 196
food_used: 193
food_army: 141
food_workers: 52
idle_worker_count: 0
army_count: 106
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 48
  count: 43
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 6
  leader_unit_type: 43
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 2
}
multi {
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 24911
score_details {
  idle_production_time: 7714.625
  idle_worker_time: 1148.875
  total_value_units: 14575.0
  total_value_structures: 7650.0
  killed_value_units: 13350.0
  killed_value_structures: 2175.0
  collected_minerals: 21975.0
  collected_vespene: 6336.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 783.0
  spent_minerals: 20375.0
  spent_vespene: 4675.0
  food_used {
    none: 0.0
    army: 132.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7350.0
    economy: 4175.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2850.0
    economy: 600.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6600.0
    economy: 7100.0
    technology: 2850.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 800.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 0.0
    army: 9150.0
    economy: 8300.0
    technology: 2950.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 800.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 12504.0
    shields: 11631.25
    energy: 0.0
  }
  total_damage_taken {
    life: 4656.37011719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3385.00048828
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1650
vespene: 1661
food_cap: 196
food_used: 184
food_army: 132
food_workers: 52
idle_worker_count: 0
army_count: 101
warp_gate_count: 0

game_loop:  20204
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 48
  count: 38
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 6
  leader_unit_type: 43
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

Score:  24911
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
