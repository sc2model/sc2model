----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5519
  player_apm: 284
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5173
  player_apm: 355
}
game_duration_loops: 25092
game_duration_seconds: 1120.25671387
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13609
score_details {
  idle_production_time: 7435.625
  idle_worker_time: 769.3125
  total_value_units: 8250.0
  total_value_structures: 1975.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 11045.0
  collected_vespene: 2032.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 873.0
  spent_minerals: 10768.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 400.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2525.0
    economy: 6650.0
    technology: 1475.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 400.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 7500.0
    technology: 1425.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 689.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1101.79833984
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 327
vespene: 357
food_cap: 146
food_used: 109
food_army: 45
food_workers: 64
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 17
}
groups {
  control_group_index: 2
  leader_unit_type: 687
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 126
  count: 4
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 128
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
    add_on {
      unit_type: 129
      build_progress: 0.119994163513
    }
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 31206
score_details {
  idle_production_time: 26252.3125
  idle_worker_time: 1065.25
  total_value_units: 29700.0
  total_value_structures: 5350.0
  killed_value_units: 15025.0
  killed_value_structures: 100.0
  collected_minerals: 34275.0
  collected_vespene: 10736.0
  collection_rate_minerals: 2547.0
  collection_rate_vespene: 1299.0
  spent_minerals: 31755.0
  spent_vespene: 9225.0
  food_used {
    none: 0.0
    army: 101.5
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11250.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3775.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9225.0
    economy: 1450.0
    technology: 305.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2975.0
    economy: 0.0
    technology: -50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4800.0
    economy: 9975.0
    technology: 4375.0
    upgrade: 2075.0
  }
  used_vespene {
    none: 0.0
    army: 3275.0
    economy: 50.0
    technology: 500.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 0.0
    army: 15200.0
    economy: 13975.0
    technology: 4575.0
    upgrade: 1575.0
  }
  total_used_vespene {
    none: 0.0
    army: 7425.0
    economy: 100.0
    technology: 850.0
    upgrade: 1575.0
  }
  total_damage_dealt {
    life: 15129.5410156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21951.5253906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2570
vespene: 1511
food_cap: 200
food_used: 182
food_army: 101
food_workers: 81
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 127
  count: 22
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 127
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 126
  count: 2
}
multi {
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 90
    shields: 0
    energy: 138
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 90
    shields: 0
    energy: 162
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 280
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 87
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 90
    shields: 0
    energy: 63
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 161
    shields: 0
    energy: 87
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 90
    shields: 0
    energy: 87
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 167
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 488
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 429
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 35067
score_details {
  idle_production_time: 34467.5625
  idle_worker_time: 1636.5
  total_value_units: 37300.0
  total_value_structures: 6950.0
  killed_value_units: 29775.0
  killed_value_structures: 1700.0
  collected_minerals: 43805.0
  collected_vespene: 15792.0
  collection_rate_minerals: 2978.0
  collection_rate_vespene: 1119.0
  spent_minerals: 41955.0
  spent_vespene: 14850.0
  food_used {
    none: 0.0
    army: 116.0
    economy: 83.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 21350.0
    economy: 2875.0
    technology: 175.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6775.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 15100.0
    economy: 2450.0
    technology: 305.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6675.0
    economy: 0.0
    technology: -50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6300.0
    economy: 10775.0
    technology: 5100.0
    upgrade: 2225.0
  }
  used_vespene {
    none: 0.0
    army: 4600.0
    economy: 50.0
    technology: 950.0
    upgrade: 2225.0
  }
  total_used_minerals {
    none: 0.0
    army: 20800.0
    economy: 15775.0
    technology: 6275.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 0.0
    army: 11575.0
    economy: 100.0
    technology: 1450.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 32995.9648438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 34782.203125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1900
vespene: 942
food_cap: 200
food_used: 199
food_army: 116
food_workers: 83
idle_worker_count: 3
army_count: 20
warp_gate_count: 0

game_loop:  25092
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 1
  leader_unit_type: 112
  count: 15
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 127
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 126
  count: 2
}
multi {
  units {
    unit_type: 289
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 90
    shields: 0
    energy: 92
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 88
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 90
    shields: 0
    energy: 116
  }
  units {
    unit_type: 289
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 88
    shields: 0
    energy: 0
  }
  units {
    unit_type: 289
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 185
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 108
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 129
    shields: 0
    energy: 200
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 90
    shields: 0
    energy: 17
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 88
    shields: 0
    energy: 200
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 259
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 309
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  35067
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
