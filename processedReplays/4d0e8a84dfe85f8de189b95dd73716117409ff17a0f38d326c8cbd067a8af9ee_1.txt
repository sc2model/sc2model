----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4456
  player_apm: 300
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4755
  player_apm: 265
}
game_duration_loops: 26062
game_duration_seconds: 1163.56335449
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14623
score_details {
  idle_production_time: 808.125
  idle_worker_time: 144.4375
  total_value_units: 6250.0
  total_value_structures: 5325.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 11840.0
  collected_vespene: 2320.0
  collection_rate_minerals: 2771.0
  collection_rate_vespene: 895.0
  spent_minerals: 11712.0
  spent_vespene: 2200.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 50.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2875.0
    economy: 6500.0
    technology: 1900.0
    upgrade: 750.0
  }
  used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 575.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 50.0
    army: 2525.0
    economy: 6900.0
    technology: 1900.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 475.0
    economy: 0.0
    technology: 575.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 1234.39453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 493.625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 178
vespene: 120
food_cap: 133
food_used: 124
food_army: 60
food_workers: 64
idle_worker_count: 2
army_count: 30
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 30
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 6
}
multi {
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 57
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 58
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16864
score_details {
  idle_production_time: 7677.0
  idle_worker_time: 4482.0
  total_value_units: 21700.0
  total_value_structures: 7175.0
  killed_value_units: 10825.0
  killed_value_structures: 400.0
  collected_minerals: 25415.0
  collected_vespene: 8496.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 739.0
  spent_minerals: 25337.0
  spent_vespene: 6375.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8450.0
    economy: 1000.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12500.0
    economy: 1330.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3500.0
    economy: 155.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1425.0
    economy: 6525.0
    technology: 2450.0
    upgrade: 1525.0
  }
  used_vespene {
    none: 50.0
    army: 350.0
    economy: 0.0
    technology: 725.0
    upgrade: 1525.0
  }
  total_used_minerals {
    none: 50.0
    army: 14425.0
    economy: 8450.0
    technology: 2450.0
    upgrade: 1525.0
  }
  total_used_vespene {
    none: 50.0
    army: 3925.0
    economy: 300.0
    technology: 725.0
    upgrade: 1525.0
  }
  total_damage_dealt {
    life: 17540.3671875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 18077.2832031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3945.50219727
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 123
vespene: 2116
food_cap: 181
food_used: 84
food_army: 31
food_workers: 53
idle_worker_count: 53
army_count: 20
warp_gate_count: 0

game_loop:  20000
ui_data{
 
Score:  16864
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
