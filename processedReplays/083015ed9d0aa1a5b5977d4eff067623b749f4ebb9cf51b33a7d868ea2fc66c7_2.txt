----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3743
  player_apm: 72
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3224
  player_apm: 93
}
game_duration_loops: 6640
game_duration_seconds: 296.449249268
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5717
score_details {
  idle_production_time: 472.4375
  idle_worker_time: 37.3125
  total_value_units: 2475.0
  total_value_structures: 1775.0
  killed_value_units: 975.0
  killed_value_structures: 0.0
  collected_minerals: 3920.0
  collected_vespene: 1172.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 358.0
  spent_minerals: 3000.0
  spent_vespene: 700.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 2300.0
    technology: 550.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 2500.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1800.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 352.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 970
vespene: 472
food_cap: 47
food_used: 41
food_army: 17
food_workers: 24
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  6640
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  5717
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
