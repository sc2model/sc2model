----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3978
  player_apm: 230
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3910
  player_apm: 240
}
game_duration_loops: 14393
game_duration_seconds: 642.589477539
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13128
score_details {
  idle_production_time: 7424.3125
  idle_worker_time: 312.6875
  total_value_units: 7600.0
  total_value_structures: 1775.0
  killed_value_units: 950.0
  killed_value_structures: 0.0
  collected_minerals: 10695.0
  collected_vespene: 2208.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 761.0
  spent_minerals: 10300.0
  spent_vespene: 1175.0
  food_used {
    none: 1.5
    army: 26.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1950.0
    economy: 6650.0
    technology: 1175.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 150.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 2300.0
    economy: 7100.0
    technology: 1325.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 250.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 933.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2178.13671875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 445
vespene: 1033
food_cap: 138
food_used: 86
food_army: 26
food_workers: 53
idle_worker_count: 5
army_count: 34
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 34
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 8
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 67
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 66
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 66
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21082
score_details {
  idle_production_time: 22168.4375
  idle_worker_time: 1101.125
  total_value_units: 15650.0
  total_value_structures: 3100.0
  killed_value_units: 5925.0
  killed_value_structures: 0.0
  collected_minerals: 19765.0
  collected_vespene: 5192.0
  collection_rate_minerals: 3359.0
  collection_rate_vespene: 1164.0
  spent_minerals: 17250.0
  spent_vespene: 3950.0
  food_used {
    none: 0.0
    army: 45.5
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4700.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3225.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2725.0
    economy: 8400.0
    technology: 1900.0
    upgrade: 1175.0
  }
  used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 600.0
    upgrade: 1175.0
  }
  total_used_minerals {
    none: 0.0
    army: 7300.0
    economy: 10100.0
    technology: 1850.0
    upgrade: 1075.0
  }
  total_used_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 550.0
    upgrade: 1075.0
  }
  total_damage_dealt {
    life: 6337.90771484
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5843.17871094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2565
vespene: 1242
food_cap: 182
food_used: 126
food_army: 45
food_workers: 81
idle_worker_count: 0
army_count: 34
warp_gate_count: 0

game_loop:  14393
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 34
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 116
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 195
    shields: 0
    energy: 115
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 182
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 57
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 7
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  21082
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
