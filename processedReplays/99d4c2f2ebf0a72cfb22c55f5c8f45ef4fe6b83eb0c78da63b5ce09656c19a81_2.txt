----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4339
  player_apm: 181
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4585
  player_apm: 165
}
game_duration_loops: 16091
game_duration_seconds: 718.39831543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9966
score_details {
  idle_production_time: 1141.0625
  idle_worker_time: 262.625
  total_value_units: 4425.0
  total_value_structures: 3825.0
  killed_value_units: 675.0
  killed_value_structures: 0.0
  collected_minerals: 7870.0
  collected_vespene: 1996.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 403.0
  spent_minerals: 7725.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1550.0
    economy: 4650.0
    technology: 1475.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 1800.0
    economy: 4950.0
    technology: 1325.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 675.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 310.0
    shields: 439.875
    energy: 0.0
  }
  total_damage_taken {
    life: 2047.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 76.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 195
vespene: 371
food_cap: 93
food_used: 71
food_army: 31
food_workers: 39
idle_worker_count: 2
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12409
score_details {
  idle_production_time: 4669.0625
  idle_worker_time: 853.3125
  total_value_units: 10525.0
  total_value_structures: 5550.0
  killed_value_units: 3025.0
  killed_value_structures: 500.0
  collected_minerals: 14950.0
  collected_vespene: 4640.0
  collection_rate_minerals: 279.0
  collection_rate_vespene: 470.0
  spent_minerals: 14500.0
  spent_vespene: 3000.0
  food_used {
    none: 0.0
    army: 49.0
    economy: 12.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2000.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2700.0
    economy: 3400.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2450.0
    economy: 3650.0
    technology: 1950.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 1050.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 4700.0
    economy: 7500.0
    technology: 2300.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 1925.0
    economy: 0.0
    technology: 450.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 2898.54003906
    shields: 4045.66503906
    energy: 0.0
  }
  total_damage_taken {
    life: 10467.1542969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2416.21411133
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 419
vespene: 1640
food_cap: 133
food_used: 61
food_army: 49
food_workers: 12
idle_worker_count: 5
army_count: 15
warp_gate_count: 0

game_loop:  16091
ui_data{
 
Score:  12409
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
