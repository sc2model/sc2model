----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3713
  player_apm: 146
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4033
  player_apm: 274
}
game_duration_loops: 15622
game_duration_seconds: 697.459411621
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8472
score_details {
  idle_production_time: 10477.3125
  idle_worker_time: 1.4375
  total_value_units: 7150.0
  total_value_structures: 2050.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 8610.0
  collected_vespene: 1412.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 313.0
  spent_minerals: 8500.0
  spent_vespene: 800.0
  food_used {
    none: 2.0
    army: 28.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 1250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 3750.0
    technology: 1300.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 5650.0
    technology: 1450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1206.23144531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3299.29931641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 160
vespene: 612
food_cap: 66
food_used: 66
food_army: 28
food_workers: 36
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
single {
  unit {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11595
score_details {
  idle_production_time: 24684.0
  idle_worker_time: 7.8125
  total_value_units: 14300.0
  total_value_structures: 2500.0
  killed_value_units: 4525.0
  killed_value_structures: 0.0
  collected_minerals: 15940.0
  collected_vespene: 3880.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 649.0
  spent_minerals: 15600.0
  spent_vespene: 2900.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5500.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 5875.0
    technology: 1750.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 700.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 6625.0
    economy: 8250.0
    technology: 2100.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 950.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 4891.59814453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11061.0361328
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 390
vespene: 980
food_cap: 162
food_used: 71
food_army: 20
food_workers: 51
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  15622
ui_data{
 
Score:  11595
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
