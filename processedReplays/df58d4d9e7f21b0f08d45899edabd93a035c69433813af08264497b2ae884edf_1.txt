----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 247
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5492
  player_apm: 260
}
game_duration_loops: 10739
game_duration_seconds: 479.453094482
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6069
score_details {
  idle_production_time: 918.3125
  idle_worker_time: 120.5
  total_value_units: 6225.0
  total_value_structures: 2900.0
  killed_value_units: 3000.0
  killed_value_structures: 0.0
  collected_minerals: 7915.0
  collected_vespene: 616.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 22.0
  spent_minerals: 7712.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 1950.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 2450.0
    technology: 1250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2975.0
    economy: 4350.0
    technology: 1250.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 4279.64892578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2439.625
    shields: 3788.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 253
vespene: 16
food_cap: 78
food_used: 48
food_army: 30
food_workers: 17
idle_worker_count: 0
army_count: 14
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 14
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 9
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 5541
score_details {
  idle_production_time: 1068.8125
  idle_worker_time: 120.5
  total_value_units: 6475.0
  total_value_structures: 2900.0
  killed_value_units: 4750.0
  killed_value_structures: 0.0
  collected_minerals: 8430.0
  collected_vespene: 648.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 67.0
  spent_minerals: 8112.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3525.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2175.0
    economy: 1950.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 2450.0
    technology: 1250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 3175.0
    economy: 4400.0
    technology: 1250.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 6678.4765625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2881.25
    shields: 4527.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 368
vespene: 48
food_cap: 78
food_used: 40
food_army: 22
food_workers: 18
idle_worker_count: 0
army_count: 10
warp_gate_count: 6

game_loop:  10739
ui_data{
 single {
  unit {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 595
    energy: 0
  }
}

Score:  5541
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
