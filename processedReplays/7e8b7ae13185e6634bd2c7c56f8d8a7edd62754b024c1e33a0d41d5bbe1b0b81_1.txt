----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3650
  player_apm: 141
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3657
  player_apm: 165
}
game_duration_loops: 29341
game_duration_seconds: 1309.95751953
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9931
score_details {
  idle_production_time: 1562.375
  idle_worker_time: 113.8125
  total_value_units: 4200.0
  total_value_structures: 3725.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 8285.0
  collected_vespene: 1796.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 582.0
  spent_minerals: 7650.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1550.0
    economy: 3700.0
    technology: 1800.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 600.0
    economy: 0.0
    technology: 375.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 1350.0
    economy: 4550.0
    technology: 1650.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 500.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 242.25
    shields: 583.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1098.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 685
vespene: 221
food_cap: 70
food_used: 70
food_army: 34
food_workers: 35
idle_worker_count: 1
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
production {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 63
  }
  build_queue {
    unit_type: 45
    build_progress: 0.00367647409439
  }
}

score{
 score_type: Melee
score: 18122
score_details {
  idle_production_time: 8578.5
  idle_worker_time: 617.25
  total_value_units: 15275.0
  total_value_structures: 6250.0
  killed_value_units: 8825.0
  killed_value_structures: 0.0
  collected_minerals: 20010.0
  collected_vespene: 6036.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 559.0
  spent_minerals: 19800.0
  spent_vespene: 5250.0
  food_used {
    none: 0.0
    army: 74.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6050.0
    economy: 1150.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3900.0
    economy: 5950.0
    technology: 2550.0
    upgrade: 950.0
  }
  used_vespene {
    none: 50.0
    army: 1700.0
    economy: 300.0
    technology: 600.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 50.0
    army: 9150.0
    economy: 7350.0
    technology: 2550.0
    upgrade: 775.0
  }
  total_used_vespene {
    none: 50.0
    army: 2975.0
    economy: 600.0
    technology: 600.0
    upgrade: 775.0
  }
  total_damage_dealt {
    life: 4546.0
    shields: 6370.25
    energy: 0.0
  }
  total_damage_taken {
    life: 9767.41503906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2884.28466797
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 243
vespene: 779
food_cap: 148
food_used: 114
food_army: 74
food_workers: 40
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 130
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15124
score_details {
  idle_production_time: 16260.4375
  idle_worker_time: 2185.0
  total_value_units: 23225.0
  total_value_structures: 7775.0
  killed_value_units: 20175.0
  killed_value_structures: 0.0
  collected_minerals: 29235.0
  collected_vespene: 8612.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 335.0
  spent_minerals: 27300.0
  spent_vespene: 8150.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 16.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13725.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14375.0
    economy: 3809.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4725.0
    economy: 153.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 625.0
    economy: 4150.0
    technology: 2700.0
    upgrade: 2025.0
  }
  used_vespene {
    none: 50.0
    army: 300.0
    economy: 150.0
    technology: 650.0
    upgrade: 2025.0
  }
  total_used_minerals {
    none: 50.0
    army: 15000.0
    economy: 8200.0
    technology: 3125.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 50.0
    army: 5025.0
    economy: 600.0
    technology: 750.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 10179.0
    shields: 13314.0
    energy: 0.0
  }
  total_damage_taken {
    life: 26659.8554688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5635.23828125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1952
vespene: 447
food_cap: 142
food_used: 29
food_army: 13
food_workers: 16
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  29341
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  15124
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
