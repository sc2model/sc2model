----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4566
  player_apm: 268
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4946
  player_apm: 158
}
game_duration_loops: 21089
game_duration_seconds: 941.53894043
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8580
score_details {
  idle_production_time: 1119.25
  idle_worker_time: 44.125
  total_value_units: 5550.0
  total_value_structures: 3250.0
  killed_value_units: 3150.0
  killed_value_structures: 175.0
  collected_minerals: 8070.0
  collected_vespene: 2056.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 335.0
  spent_minerals: 8025.0
  spent_vespene: 2000.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1500.0
    economy: 1150.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1550.0
    economy: 4200.0
    technology: 1075.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 650.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 2550.0
    economy: 4250.0
    technology: 1075.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 1100.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4861.09960938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1757.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 44.134765625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 81
vespene: 49
food_cap: 94
food_used: 70
food_army: 30
food_workers: 38
idle_worker_count: 0
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19116
score_details {
  idle_production_time: 7913.5625
  idle_worker_time: 866.0
  total_value_units: 18575.0
  total_value_structures: 7550.0
  killed_value_units: 10675.0
  killed_value_structures: 175.0
  collected_minerals: 23170.0
  collected_vespene: 7252.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 515.0
  spent_minerals: 21950.0
  spent_vespene: 6075.0
  food_used {
    none: 0.0
    army: 67.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6750.0
    economy: 1150.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2775.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7987.0
    economy: 250.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3194.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 3350.0
    economy: 7150.0
    technology: 2375.0
    upgrade: 900.0
  }
  used_vespene {
    none: 50.0
    army: 1050.0
    economy: 0.0
    technology: 750.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 50.0
    army: 11300.0
    economy: 7900.0
    technology: 2900.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 50.0
    army: 4225.0
    economy: 0.0
    technology: 900.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 11868.9208984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12542.15625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 915.778320312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1233
vespene: 1158
food_cap: 188
food_used: 117
food_army: 67
food_workers: 50
idle_worker_count: 0
army_count: 40
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 51
  count: 22
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 2
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 67
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 69
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17027
score_details {
  idle_production_time: 9064.9375
  idle_worker_time: 870.75
  total_value_units: 19325.0
  total_value_structures: 7950.0
  killed_value_units: 11075.0
  killed_value_structures: 175.0
  collected_minerals: 24685.0
  collected_vespene: 7556.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 335.0
  spent_minerals: 23150.0
  spent_vespene: 6625.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7150.0
    economy: 1150.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2775.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9537.0
    economy: 2183.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3519.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2200.0
    economy: 5525.0
    technology: 2375.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 750.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 50.0
    army: 11700.0
    economy: 8600.0
    technology: 2900.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 50.0
    army: 4275.0
    economy: 0.0
    technology: 900.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 12135.9208984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 17958.71875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 981.403320312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1540
vespene: 912
food_cap: 173
food_used: 82
food_army: 44
food_workers: 38
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  21089
ui_data{
 
Score:  17027
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
