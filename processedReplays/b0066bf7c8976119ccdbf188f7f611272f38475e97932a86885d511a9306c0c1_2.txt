----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3203
  player_apm: 176
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2886
  player_apm: 193
}
game_duration_loops: 18845
game_duration_seconds: 841.35333252
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8625
score_details {
  idle_production_time: 8540.25
  idle_worker_time: 756.5
  total_value_units: 7700.0
  total_value_structures: 1725.0
  killed_value_units: 3625.0
  killed_value_structures: 0.0
  collected_minerals: 9670.0
  collected_vespene: 980.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 0.0
  spent_minerals: 9200.0
  spent_vespene: 975.0
  food_used {
    none: 0.5
    army: 28.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2250.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1575.0
    economy: 4225.0
    technology: 1025.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 25.0
    technology: 250.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3650.0
    economy: 5200.0
    technology: 1175.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 50.0
    technology: 350.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 1995.0
    shields: 2479.5
    energy: 0.0
  }
  total_damage_taken {
    life: 4914.03125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 520
vespene: 5
food_cap: 98
food_used: 68
food_army: 28
food_workers: 35
idle_worker_count: 1
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 28
}
groups {
  control_group_index: 3
  leader_unit_type: 893
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18165
score_details {
  idle_production_time: 35970.25
  idle_worker_time: 2703.0
  total_value_units: 20400.0
  total_value_structures: 2425.0
  killed_value_units: 16100.0
  killed_value_structures: 3150.0
  collected_minerals: 22640.0
  collected_vespene: 5612.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 559.0
  spent_minerals: 21356.0
  spent_vespene: 4481.0
  food_used {
    none: 0.0
    army: 82.5
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10300.0
    economy: 3850.0
    technology: 1400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3600.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6631.0
    economy: 1500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1381.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 4400.0
    economy: 6650.0
    technology: 1300.0
    upgrade: 750.0
  }
  used_vespene {
    none: 125.0
    army: 1250.0
    economy: 25.0
    technology: 350.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 300.0
    army: 12300.0
    economy: 9025.0
    technology: 1450.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 300.0
    army: 3600.0
    economy: 50.0
    technology: 450.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 17106.0
    shields: 18841.125
    energy: 0.0
  }
  total_damage_taken {
    life: 13053.7597656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1334
vespene: 1131
food_cap: 192
food_used: 134
food_army: 82
food_workers: 52
idle_worker_count: 9
army_count: 85
warp_gate_count: 0

game_loop:  18845
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 503
  count: 51
}
groups {
  control_group_index: 3
  leader_unit_type: 893
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}

Score:  18165
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
