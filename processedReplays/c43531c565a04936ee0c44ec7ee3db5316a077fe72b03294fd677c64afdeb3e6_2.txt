----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3246
  player_apm: 95
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2684
  player_apm: 116
}
game_duration_loops: 9881
game_duration_seconds: 441.146850586
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9563
score_details {
  idle_production_time: 1558.375
  idle_worker_time: 23.0
  total_value_units: 4900.0
  total_value_structures: 3300.0
  killed_value_units: 2725.0
  killed_value_structures: 550.0
  collected_minerals: 7215.0
  collected_vespene: 1968.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 425.0
  spent_minerals: 6825.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1650.0
    economy: 1400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2250.0
    economy: 4150.0
    technology: 950.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 3750.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4863.90185547
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 702.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 247.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 430
vespene: 308
food_cap: 102
food_used: 80
food_army: 45
food_workers: 35
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  9881
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 18
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 145
    shields: 0
    energy: 189
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 111
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  9563
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
