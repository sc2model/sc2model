----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3531
  player_apm: 102
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3610
  player_apm: 106
}
game_duration_loops: 28239
game_duration_seconds: 1260.75756836
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10494
score_details {
  idle_production_time: 1008.1875
  idle_worker_time: 46.9375
  total_value_units: 5575.0
  total_value_structures: 3400.0
  killed_value_units: 900.0
  killed_value_structures: 350.0
  collected_minerals: 8160.0
  collected_vespene: 1808.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 358.0
  spent_minerals: 7662.0
  spent_vespene: 1687.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 50.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2625.0
    economy: 4050.0
    technology: 1200.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 300.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2525.0
    economy: 4100.0
    technology: 1200.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2567.05712891
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 456.0
    shields: 491.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 548
vespene: 121
food_cap: 94
food_used: 85
food_army: 42
food_workers: 43
idle_worker_count: 0
army_count: 21
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 63
  count: 1
}
single {
  unit {
    unit_type: 74
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20062
score_details {
  idle_production_time: 4410.0625
  idle_worker_time: 101.4375
  total_value_units: 16875.0
  total_value_structures: 7450.0
  killed_value_units: 5400.0
  killed_value_structures: 1050.0
  collected_minerals: 20355.0
  collected_vespene: 6856.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 1187.0
  spent_minerals: 20212.0
  spent_vespene: 6337.0
  food_used {
    none: 0.0
    army: 92.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3500.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5275.0
    economy: 950.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4350.0
    economy: 7450.0
    technology: 2050.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 600.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 0.0
    army: 9525.0
    economy: 8250.0
    technology: 2050.0
    upgrade: 1150.0
  }
  total_used_vespene {
    none: 0.0
    army: 4800.0
    economy: 0.0
    technology: 600.0
    upgrade: 1150.0
  }
  total_damage_dealt {
    life: 9510.21582031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5126.0
    shields: 6857.67089844
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 193
vespene: 519
food_cap: 200
food_used: 157
food_army: 92
food_workers: 62
idle_worker_count: 0
army_count: 42
warp_gate_count: 7

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 75
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 63
  count: 1
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

score{
 score_type: Melee
score: 4323
score_details {
  idle_production_time: 7355.6875
  idle_worker_time: 7801.375
  total_value_units: 21825.0
  total_value_structures: 8350.0
  killed_value_units: 14300.0
  killed_value_structures: 1050.0
  collected_minerals: 25090.0
  collected_vespene: 8832.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 694.0
  spent_minerals: 24312.0
  spent_vespene: 8137.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9350.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12175.0
    economy: 2100.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5750.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 13000.0
    economy: 8400.0
    technology: 2950.0
    upgrade: 1400.0
  }
  total_used_vespene {
    none: 0.0
    army: 7475.0
    economy: 0.0
    technology: 600.0
    upgrade: 1400.0
  }
  total_damage_dealt {
    life: 16788.3574219
    shields: 100.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13151.0
    shields: 14577.6708984
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 828
vespene: 695
food_cap: 0
food_used: 0
food_army: 0
food_workers: 0
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  28239
ui_data{
 
Score:  4323
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
