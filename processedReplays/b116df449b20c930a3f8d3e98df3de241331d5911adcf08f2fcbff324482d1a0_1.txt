----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4685
  player_apm: 249
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4459
  player_apm: 156
}
game_duration_loops: 17758
game_duration_seconds: 792.823181152
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7826
score_details {
  idle_production_time: 617.5625
  idle_worker_time: 230.8125
  total_value_units: 4400.0
  total_value_structures: 3000.0
  killed_value_units: 200.0
  killed_value_structures: 300.0
  collected_minerals: 6440.0
  collected_vespene: 2044.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 358.0
  spent_minerals: 6325.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 829.0
    economy: 251.0
    technology: 157.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 521.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1450.0
    economy: 3150.0
    technology: 1375.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 625.0
    economy: 0.0
    technology: 550.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 1850.0
    economy: 3700.0
    technology: 1100.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1556.48828125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2357.13085938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 222.130859375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 128
vespene: 198
food_cap: 62
food_used: 61
food_army: 31
food_workers: 30
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 48
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 692
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 2
}
multi {
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12330
score_details {
  idle_production_time: 4832.1875
  idle_worker_time: 398.5
  total_value_units: 11525.0
  total_value_structures: 6250.0
  killed_value_units: 3375.0
  killed_value_structures: 300.0
  collected_minerals: 15860.0
  collected_vespene: 4896.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 492.0
  spent_minerals: 15105.0
  spent_vespene: 3562.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3050.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 5529.0
    economy: 2020.0
    technology: 157.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 1671.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1450.0
    economy: 4025.0
    technology: 2150.0
    upgrade: 575.0
  }
  used_vespene {
    none: 50.0
    army: 525.0
    economy: 0.0
    technology: 750.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 50.0
    army: 6700.0
    economy: 6650.0
    technology: 2300.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 2175.0
    economy: 0.0
    technology: 750.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 4969.83154297
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11862.9423828
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1137.2890625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 767
vespene: 1313
food_cap: 149
food_used: 52
food_army: 28
food_workers: 24
idle_worker_count: 26
army_count: 14
warp_gate_count: 0

game_loop:  17758
ui_data{
 
Score:  12330
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
