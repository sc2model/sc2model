----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3699
  player_apm: 302
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3794
  player_apm: 155
}
game_duration_loops: 10939
game_duration_seconds: 488.382293701
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4149
score_details {
  idle_production_time: 5250.8125
  idle_worker_time: 35.5
  total_value_units: 5150.0
  total_value_structures: 1275.0
  killed_value_units: 2400.0
  killed_value_structures: 0.0
  collected_minerals: 5755.0
  collected_vespene: 844.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 179.0
  spent_minerals: 5800.0
  spent_vespene: 550.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2325.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2350.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 2150.0
    technology: 750.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3300.0
    economy: 2975.0
    technology: 700.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3685.58300781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6379.79785156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 5
vespene: 294
food_cap: 36
food_used: 30
food_army: 10
food_workers: 20
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4421
score_details {
  idle_production_time: 5976.75
  idle_worker_time: 35.5
  total_value_units: 5550.0
  total_value_structures: 1450.0
  killed_value_units: 2550.0
  killed_value_structures: 0.0
  collected_minerals: 6335.0
  collected_vespene: 1036.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 313.0
  spent_minerals: 6225.0
  spent_vespene: 625.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2475.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2750.0
    economy: 550.0
    technology: -125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 525.0
    economy: 2150.0
    technology: 750.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3700.0
    economy: 3050.0
    technology: 900.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3889.69970703
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7168.20703125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 160
vespene: 411
food_cap: 36
food_used: 30
food_army: 10
food_workers: 20
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  10939
ui_data{
 single {
  unit {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
}

Score:  4421
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
