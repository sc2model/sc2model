----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3631
  player_apm: 114
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3505
  player_apm: 130
}
game_duration_loops: 11401
game_duration_seconds: 509.008728027
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6000
score_details {
  idle_production_time: 1145.1875
  idle_worker_time: 929.625
  total_value_units: 3950.0
  total_value_structures: 3550.0
  killed_value_units: 975.0
  killed_value_structures: 1200.0
  collected_minerals: 7415.0
  collected_vespene: 1660.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 6325.0
  spent_vespene: 825.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 1400.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 2300.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 900.0
    economy: 2100.0
    technology: 575.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4700.0
    technology: 1175.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4277.0
    shields: 4645.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9614.84472656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 693.588134766
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1140
vespene: 835
food_cap: 53
food_used: 22
food_army: 18
food_workers: 4
idle_worker_count: 2
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 134
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 91
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 93
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4439
score_details {
  idle_production_time: 1485.0625
  idle_worker_time: 1058.0
  total_value_units: 4200.0
  total_value_structures: 3550.0
  killed_value_units: 1650.0
  killed_value_structures: 1500.0
  collected_minerals: 7420.0
  collected_vespene: 1668.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 6812.0
  spent_vespene: 1062.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 1900.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1550.0
    economy: 2887.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 37.0
    technology: 325.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 1550.0
    technology: 575.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 50.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4700.0
    technology: 1175.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 5231.0
    shields: 5961.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12045.4453125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 940.254882812
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 658
vespene: 606
food_cap: 38
food_used: 7
food_army: 6
food_workers: 1
idle_worker_count: 1
army_count: 3
warp_gate_count: 0

game_loop:  11401
ui_data{
 
Score:  4439
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
