----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3989
  player_apm: 216
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3786
  player_apm: 167
}
game_duration_loops: 25420
game_duration_seconds: 1134.90063477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11152
score_details {
  idle_production_time: 5531.125
  idle_worker_time: 74.1875
  total_value_units: 6750.0
  total_value_structures: 1900.0
  killed_value_units: 1900.0
  killed_value_structures: 0.0
  collected_minerals: 10590.0
  collected_vespene: 2312.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 671.0
  spent_minerals: 8600.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1450.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2400.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 4400.0
    technology: 1050.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 5250.0
    technology: 1200.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 450.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 1973.66967773
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6698.03955078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2040
vespene: 862
food_cap: 74
food_used: 62
food_army: 15
food_workers: 47
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 2
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23181
score_details {
  idle_production_time: 23857.8125
  idle_worker_time: 1605.1875
  total_value_units: 19175.0
  total_value_structures: 3100.0
  killed_value_units: 5950.0
  killed_value_structures: 400.0
  collected_minerals: 22230.0
  collected_vespene: 7976.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 806.0
  spent_minerals: 20925.0
  spent_vespene: 7000.0
  food_used {
    none: 0.0
    army: 104.5
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4600.0
    economy: 200.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5525.0
    economy: 1500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5325.0
    economy: 6200.0
    technology: 1700.0
    upgrade: 1675.0
  }
  used_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 950.0
    upgrade: 1675.0
  }
  total_used_minerals {
    none: 0.0
    army: 12050.0
    economy: 8700.0
    technology: 1950.0
    upgrade: 975.0
  }
  total_used_vespene {
    none: 0.0
    army: 5425.0
    economy: 0.0
    technology: 1150.0
    upgrade: 975.0
  }
  total_damage_dealt {
    life: 12540.1748047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20254.046875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1355
vespene: 976
food_cap: 176
food_used: 150
food_army: 104
food_workers: 46
idle_worker_count: 8
army_count: 92
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 494
  count: 17
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 2
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7754
score_details {
  idle_production_time: 32221.375
  idle_worker_time: 5956.5625
  total_value_units: 22725.0
  total_value_structures: 3450.0
  killed_value_units: 12550.0
  killed_value_structures: 2200.0
  collected_minerals: 23485.0
  collected_vespene: 9144.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 0.0
  spent_minerals: 23075.0
  spent_vespene: 8400.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 8700.0
    economy: 1900.0
    technology: 1025.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 2750.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11200.0
    economy: 2600.0
    technology: 1400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4575.0
    economy: 0.0
    technology: 900.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1050.0
    economy: 1275.0
    technology: 50.0
    upgrade: 1675.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 50.0
    upgrade: 1675.0
  }
  total_used_minerals {
    none: 0.0
    army: 14200.0
    economy: 8700.0
    technology: 2150.0
    upgrade: 1675.0
  }
  total_used_vespene {
    none: 0.0
    army: 6825.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1675.0
  }
  total_damage_dealt {
    life: 24560.8105469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 38564.9726562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 460
vespene: 744
food_cap: 56
food_used: 29
food_army: 29
food_workers: 0
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  25420
ui_data{
 
Score:  7754
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
