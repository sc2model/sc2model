----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4227
  player_apm: 151
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4194
  player_apm: 170
}
game_duration_loops: 25997
game_duration_seconds: 1160.66137695
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11034
score_details {
  idle_production_time: 1350.4375
  idle_worker_time: 61.75
  total_value_units: 5550.0
  total_value_structures: 4400.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 8520.0
  collected_vespene: 2564.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 627.0
  spent_minerals: 8300.0
  spent_vespene: 2150.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4700.0
    technology: 1750.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 550.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1850.0
    economy: 5050.0
    technology: 1450.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 550.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 795.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 538.0
    shields: 742.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 270
vespene: 414
food_cap: 109
food_used: 78
food_army: 30
food_workers: 48
idle_worker_count: 0
army_count: 11
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 74
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 74
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 62
    energy: 0
  }
}

score{
 score_type: Melee
score: 26987
score_details {
  idle_production_time: 5351.75
  idle_worker_time: 2802.875
  total_value_units: 16400.0
  total_value_structures: 10175.0
  killed_value_units: 6150.0
  killed_value_structures: 0.0
  collected_minerals: 24725.0
  collected_vespene: 7712.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 851.0
  spent_minerals: 23300.0
  spent_vespene: 6675.0
  food_used {
    none: 0.0
    army: 121.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4350.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3675.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6100.0
    economy: 6725.0
    technology: 5200.0
    upgrade: 1750.0
  }
  used_vespene {
    none: 0.0
    army: 2200.0
    economy: 0.0
    technology: 750.0
    upgrade: 1750.0
  }
  total_used_minerals {
    none: 0.0
    army: 9575.0
    economy: 7575.0
    technology: 5200.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 0.0
    army: 5275.0
    economy: 0.0
    technology: 750.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 7033.49267578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3968.875
    shields: 5202.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1475
vespene: 1037
food_cap: 200
food_used: 177
food_army: 121
food_workers: 56
idle_worker_count: 9
army_count: 57
warp_gate_count: 17

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 74
  count: 52
}
groups {
  control_group_index: 5
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 86
    shields: 43
    energy: 0
    build_progress: 0.849987030029
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 24
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 74
    shields: 37
    energy: 0
    build_progress: 0.712489128113
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 5
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 81
    shields: 40
    energy: 0
    build_progress: 0.787487983704
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 64
    shields: 6
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 27
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 75
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 66
    shields: 33
    energy: 0
    build_progress: 0.624990463257
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 58
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 21
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 78
    shields: 39
    energy: 0
    build_progress: 0.749988555908
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 146
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 57
    shields: 29
    energy: 0
    build_progress: 0.524991989136
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 72
    shields: 2
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 34
    energy: 136
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 31
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 26
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 254
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 10
    shields: 1
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 38
    shields: 2
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 34
    shields: 1
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 42
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 3
    shields: 10
    energy: 74
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 23
    shields: 10
    energy: 74
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 70
    shields: 35
    energy: 0
    build_progress: 0.662489891052
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 84
    shields: 42
    energy: 0
    build_progress: 0.824987411499
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 72
    shields: 9
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 71
    shields: 35
    energy: 0
    build_progress: 0.674989700317
  }
}

score{
 score_type: Melee
score: 28916
score_details {
  idle_production_time: 9781.375
  idle_worker_time: 4326.8125
  total_value_units: 25600.0
  total_value_structures: 11775.0
  killed_value_units: 17600.0
  killed_value_structures: 1475.0
  collected_minerals: 32509.0
  collected_vespene: 11132.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 761.0
  spent_minerals: 30450.0
  spent_vespene: 9875.0
  food_used {
    none: 0.0
    army: 97.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11775.0
    economy: 3000.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4125.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10650.0
    economy: 1200.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4575.0
    economy: 8175.0
    technology: 5050.0
    upgrade: 1975.0
  }
  used_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 750.0
    upgrade: 1975.0
  }
  total_used_minerals {
    none: 0.0
    army: 16825.0
    economy: 9375.0
    technology: 5200.0
    upgrade: 1750.0
  }
  total_used_vespene {
    none: 0.0
    army: 10175.0
    economy: 0.0
    technology: 750.0
    upgrade: 1750.0
  }
  total_damage_dealt {
    life: 22002.34375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10039.0
    shields: 12615.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2109
vespene: 1257
food_cap: 200
food_used: 154
food_army: 97
food_workers: 57
idle_worker_count: 0
army_count: 43
warp_gate_count: 17

game_loop:  25997
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 74
  count: 31
}
groups {
  control_group_index: 5
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 136
  count: 1
}
single {
  unit {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

Score:  28916
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
