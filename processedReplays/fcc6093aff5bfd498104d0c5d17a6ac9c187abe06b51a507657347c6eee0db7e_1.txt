----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3390
  player_apm: 116
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3424
  player_apm: 145
}
game_duration_loops: 4879
game_duration_seconds: 217.827697754
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4278
score_details {
  idle_production_time: 140.25
  idle_worker_time: 247.9375
  total_value_units: 1550.0
  total_value_structures: 1475.0
  killed_value_units: 25.0
  killed_value_structures: 0.0
  collected_minerals: 2840.0
  collected_vespene: 588.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 358.0
  spent_minerals: 2800.0
  spent_vespene: 475.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: -250.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 350.0
    economy: 2450.0
    technology: 700.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 275.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 300.0
    economy: 2250.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 175.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 36.6918945312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 332.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 90
vespene: 113
food_cap: 31
food_used: 30
food_army: 7
food_workers: 23
idle_worker_count: 1
army_count: 6
warp_gate_count: 0

game_loop:  4879
ui_data{
 single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  4278
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
