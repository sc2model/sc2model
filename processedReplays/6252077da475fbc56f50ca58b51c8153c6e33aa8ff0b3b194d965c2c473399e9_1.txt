----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3364
  player_apm: 156
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3311
  player_apm: 104
}
game_duration_loops: 20510
game_duration_seconds: 915.688903809
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10708
score_details {
  idle_production_time: 8718.9375
  idle_worker_time: 20.5
  total_value_units: 6900.0
  total_value_structures: 1650.0
  killed_value_units: 625.0
  killed_value_structures: 0.0
  collected_minerals: 8615.0
  collected_vespene: 1480.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 627.0
  spent_minerals: 8412.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: -63.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2700.0
    economy: 4900.0
    technology: 925.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2400.0
    economy: 5525.0
    technology: 1075.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 369.0
    shields: 504.875
    energy: 0.0
  }
  total_damage_taken {
    life: 1127.31689453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 253
vespene: 30
food_cap: 114
food_used: 92
food_army: 48
food_workers: 44
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22476
score_details {
  idle_production_time: 43250.0
  idle_worker_time: 444.1875
  total_value_units: 23950.0
  total_value_structures: 3625.0
  killed_value_units: 14075.0
  killed_value_structures: 875.0
  collected_minerals: 24215.0
  collected_vespene: 9348.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 1299.0
  spent_minerals: 23487.0
  spent_vespene: 8750.0
  food_used {
    none: 0.0
    army: 81.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8400.0
    economy: 1625.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7600.0
    economy: 137.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4900.0
    economy: 8750.0
    technology: 1850.0
    upgrade: 950.0
  }
  used_vespene {
    none: 150.0
    army: 2550.0
    economy: 0.0
    technology: 850.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 300.0
    army: 12950.0
    economy: 10225.0
    technology: 2000.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 300.0
    army: 8200.0
    economy: 0.0
    technology: 900.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 7730.59667969
    shields: 10531.0341797
    energy: 0.0
  }
  total_damage_taken {
    life: 9927.90722656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 778
vespene: 598
food_cap: 200
food_used: 143
food_army: 81
food_workers: 62
idle_worker_count: 1
army_count: 34
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
single {
  unit {
    unit_type: 90
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22922
score_details {
  idle_production_time: 44967.8125
  idle_worker_time: 537.875
  total_value_units: 24250.0
  total_value_structures: 3625.0
  killed_value_units: 16175.0
  killed_value_structures: 875.0
  collected_minerals: 24925.0
  collected_vespene: 9784.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 1119.0
  spent_minerals: 23812.0
  spent_vespene: 9075.0
  food_used {
    none: 0.0
    army: 77.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9750.0
    economy: 1625.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7800.0
    economy: 137.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4700.0
    economy: 8800.0
    technology: 1850.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 150.0
    army: 2450.0
    economy: 0.0
    technology: 850.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 300.0
    army: 13250.0
    economy: 10225.0
    technology: 2000.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 300.0
    army: 8200.0
    economy: 0.0
    technology: 900.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 8504.59667969
    shields: 11685.0341797
    energy: 0.0
  }
  total_damage_taken {
    life: 10721.8046875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1163
vespene: 709
food_cap: 200
food_used: 137
food_army: 77
food_workers: 60
idle_worker_count: 3
army_count: 32
warp_gate_count: 0

game_loop:  20510
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 182
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 148
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 165
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 69
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 144
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 181
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  22922
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
