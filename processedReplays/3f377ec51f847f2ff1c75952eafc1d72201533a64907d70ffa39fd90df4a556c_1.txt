----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: -36400
  player_apm: 71
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2437
  player_apm: 50
}
game_duration_loops: 19651
game_duration_seconds: 877.338012695
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7213
score_details {
  idle_production_time: 7570.3125
  idle_worker_time: 0.0
  total_value_units: 4650.0
  total_value_structures: 1150.0
  killed_value_units: 450.0
  killed_value_structures: 550.0
  collected_minerals: 6650.0
  collected_vespene: 888.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 156.0
  spent_minerals: 4925.0
  spent_vespene: 575.0
  food_used {
    none: 0.0
    army: 18.5
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 150.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1025.0
    economy: 2950.0
    technology: 800.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 3300.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1130.0
    shields: 1205.0
    energy: 0.0
  }
  total_damage_taken {
    life: 825.424804688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1775
vespene: 313
food_cap: 84
food_used: 42
food_army: 18
food_workers: 24
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 21
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}

score{
 score_type: Melee
score: 14079
score_details {
  idle_production_time: 22593.75
  idle_worker_time: 161.75
  total_value_units: 15650.0
  total_value_structures: 2400.0
  killed_value_units: 7175.0
  killed_value_structures: 1800.0
  collected_minerals: 15990.0
  collected_vespene: 5564.0
  collection_rate_minerals: 587.0
  collection_rate_vespene: 492.0
  spent_minerals: 14825.0
  spent_vespene: 4875.0
  food_used {
    none: 0.0
    army: 49.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4075.0
    economy: 1850.0
    technology: 1200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4675.0
    economy: 1075.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2550.0
    economy: 4825.0
    technology: 1450.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 7650.0
    economy: 6750.0
    technology: 1400.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 3825.0
    economy: 0.0
    technology: 450.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 7037.32519531
    shields: 8227.45410156
    energy: 0.0
  }
  total_damage_taken {
    life: 10306.4472656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1215
vespene: 689
food_cap: 128
food_used: 84
food_army: 49
food_workers: 35
idle_worker_count: 7
army_count: 31
warp_gate_count: 0

game_loop:  19651
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 110
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 58
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  14079
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
