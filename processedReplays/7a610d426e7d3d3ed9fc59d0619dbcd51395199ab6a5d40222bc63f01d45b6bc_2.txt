----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4311
  player_apm: 372
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4442
  player_apm: 215
}
game_duration_loops: 15900
game_duration_seconds: 709.87097168
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11580
score_details {
  idle_production_time: 1153.8125
  idle_worker_time: 197.0
  total_value_units: 4500.0
  total_value_structures: 4725.0
  killed_value_units: 800.0
  killed_value_structures: 350.0
  collected_minerals: 8720.0
  collected_vespene: 1960.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 559.0
  spent_minerals: 8600.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2150.0
    economy: 4750.0
    technology: 1800.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 625.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 2100.0
    economy: 4950.0
    technology: 1800.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 625.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 2261.92285156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 319.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 380.852539062
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 170
vespene: 135
food_cap: 117
food_used: 83
food_army: 43
food_workers: 40
idle_worker_count: 1
army_count: 37
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 18
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 103
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 99
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15158
score_details {
  idle_production_time: 4294.3125
  idle_worker_time: 388.3125
  total_value_units: 10800.0
  total_value_structures: 6950.0
  killed_value_units: 6525.0
  killed_value_structures: 700.0
  collected_minerals: 17780.0
  collected_vespene: 4828.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 447.0
  spent_minerals: 17150.0
  spent_vespene: 3850.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4875.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5050.0
    economy: 1950.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1900.0
    economy: 4600.0
    technology: 2400.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 850.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 6500.0
    economy: 7200.0
    technology: 2600.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 900.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 11381.3730469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14158.5390625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2748.90698242
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 680
vespene: 978
food_cap: 172
food_used: 56
food_army: 38
food_workers: 18
idle_worker_count: 18
army_count: 16
warp_gate_count: 0

game_loop:  15900
ui_data{
 
Score:  15158
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
