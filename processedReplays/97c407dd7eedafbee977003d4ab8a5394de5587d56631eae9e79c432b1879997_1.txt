----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3672
  player_apm: 102
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3685
  player_apm: 99
}
game_duration_loops: 27510
game_duration_seconds: 1228.21069336
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10940
score_details {
  idle_production_time: 1418.0625
  idle_worker_time: 77.5625
  total_value_units: 4525.0
  total_value_structures: 3850.0
  killed_value_units: 375.0
  killed_value_structures: 0.0
  collected_minerals: 7560.0
  collected_vespene: 2380.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 649.0
  spent_minerals: 7450.0
  spent_vespene: 2225.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 4800.0
    technology: 1800.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 450.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4350.0
    technology: 1500.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 645.041503906
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 80.0
    shields: 143.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 160
vespene: 155
food_cap: 94
food_used: 86
food_army: 38
food_workers: 48
idle_worker_count: 0
army_count: 17
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 495
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 311
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 26371
score_details {
  idle_production_time: 5887.0625
  idle_worker_time: 556.75
  total_value_units: 18600.0
  total_value_structures: 9400.0
  killed_value_units: 6000.0
  killed_value_structures: 0.0
  collected_minerals: 22085.0
  collected_vespene: 9036.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 739.0
  spent_minerals: 20150.0
  spent_vespene: 8550.0
  food_used {
    none: 0.0
    army: 130.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4450.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2025.0
    economy: 1850.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6200.0
    economy: 6700.0
    technology: 3550.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 5150.0
    economy: 0.0
    technology: 1000.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 10325.0
    economy: 8150.0
    technology: 3550.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 9925.0
    economy: 0.0
    technology: 1000.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 5392.25048828
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5043.63085938
    shields: 5782.81835938
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1985
vespene: 486
food_cap: 200
food_used: 191
food_army: 130
food_workers: 61
idle_worker_count: 6
army_count: 48
warp_gate_count: 8

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 495
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 311
  count: 50
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 191
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 173
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 90
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 65
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 80
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 65
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 9
    shields: 40
    energy: 200
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 193
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

score{
 score_type: Melee
score: 20978
score_details {
  idle_production_time: 9503.625
  idle_worker_time: 2366.9375
  total_value_units: 22300.0
  total_value_structures: 11800.0
  killed_value_units: 21550.0
  killed_value_structures: 4975.0
  collected_minerals: 27200.0
  collected_vespene: 11428.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 313.0
  spent_minerals: 25400.0
  spent_vespene: 10300.0
  food_used {
    none: 0.0
    army: 135.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 11675.0
    economy: 5800.0
    technology: 1525.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 6625.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3125.0
    economy: 8150.0
    technology: 3250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 1000.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6500.0
    economy: 2450.0
    technology: 1200.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 6550.0
    economy: 0.0
    technology: 0.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 13325.0
    economy: 9800.0
    technology: 4300.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 13325.0
    economy: 0.0
    technology: 1000.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 41148.0898438
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25431.6992188
    shields: 32324.5820312
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1850
vespene: 1128
food_cap: 78
food_used: 150
food_army: 135
food_workers: 15
idle_worker_count: 0
army_count: 45
warp_gate_count: 0

game_loop:  27510
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 36
}
multi {
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 209
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 26
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 96
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 92
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 80
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 200
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 9
    shields: 40
    energy: 200
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 45
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 66
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

Score:  20978
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
