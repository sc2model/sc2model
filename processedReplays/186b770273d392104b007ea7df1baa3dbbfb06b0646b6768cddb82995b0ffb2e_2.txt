----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3873
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3797
  player_apm: 190
}
game_duration_loops: 17672
game_duration_seconds: 788.983642578
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12190
score_details {
  idle_production_time: 7936.5
  idle_worker_time: 504.25
  total_value_units: 6300.0
  total_value_structures: 1950.0
  killed_value_units: 675.0
  killed_value_structures: 100.0
  collected_minerals: 9950.0
  collected_vespene: 1740.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 582.0
  spent_minerals: 8775.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 5575.0
    technology: 1325.0
    upgrade: 1125.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 250.0
    upgrade: 1125.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 6025.0
    technology: 1475.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 756.0
    shields: 825.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1119.88085938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1225
vespene: 315
food_cap: 82
food_used: 78
food_army: 18
food_workers: 60
idle_worker_count: 12
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16940
score_details {
  idle_production_time: 29837.5625
  idle_worker_time: 2895.4375
  total_value_units: 22000.0
  total_value_structures: 3175.0
  killed_value_units: 8675.0
  killed_value_structures: 200.0
  collected_minerals: 23800.0
  collected_vespene: 6240.0
  collection_rate_minerals: 3163.0
  collection_rate_vespene: 985.0
  spent_minerals: 22625.0
  spent_vespene: 5525.0
  food_used {
    none: 0.0
    army: 27.5
    economy: 80.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6625.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 37.0
    army: 8125.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 37.0
    army: 2600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1625.0
    economy: 8650.0
    technology: 1800.0
    upgrade: 1125.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 500.0
    upgrade: 1125.0
  }
  total_used_minerals {
    none: 0.0
    army: 12150.0
    economy: 10175.0
    technology: 2150.0
    upgrade: 1125.0
  }
  total_used_vespene {
    none: 0.0
    army: 3775.0
    economy: 0.0
    technology: 750.0
    upgrade: 1125.0
  }
  total_damage_dealt {
    life: 6755.64453125
    shields: 6732.82226562
    energy: 0.0
  }
  total_damage_taken {
    life: 11608.9492188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1225
vespene: 715
food_cap: 200
food_used: 107
food_army: 27
food_workers: 80
idle_worker_count: 5
army_count: 17
warp_gate_count: 0

game_loop:  17672
ui_data{
 single {
  unit {
    unit_type: 4
    player_relative: 3
    health: 83
    shields: 9
    energy: 0
  }
}

Score:  16940
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
