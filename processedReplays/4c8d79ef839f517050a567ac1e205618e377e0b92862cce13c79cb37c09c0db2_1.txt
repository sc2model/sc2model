----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3834
  player_apm: 142
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3935
  player_apm: 166
}
game_duration_loops: 18699
game_duration_seconds: 834.835021973
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9705
score_details {
  idle_production_time: 7065.25
  idle_worker_time: 0.4375
  total_value_units: 6100.0
  total_value_structures: 1500.0
  killed_value_units: 1700.0
  killed_value_structures: 0.0
  collected_minerals: 8975.0
  collected_vespene: 1604.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 313.0
  spent_minerals: 7993.0
  spent_vespene: 931.0
  food_used {
    none: 6.0
    army: 16.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1431.0
    economy: -138.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 131.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 1575.0
    economy: 4225.0
    technology: 1300.0
    upgrade: 250.0
  }
  used_vespene {
    none: -25.0
    army: 300.0
    economy: 25.0
    technology: 150.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 4650.0
    technology: 1450.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 50.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 996.5
    shields: 2356.5
    energy: 0.0
  }
  total_damage_taken {
    life: 2443.54785156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1032
vespene: 673
food_cap: 84
food_used: 61
food_army: 16
food_workers: 39
idle_worker_count: 0
army_count: 12
warp_gate_count: 0
larva_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 8
  count: 22
}
groups {
  control_group_index: 4
  leader_unit_type: 893
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 3
}
single {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21456
score_details {
  idle_production_time: 31630.4375
  idle_worker_time: 3.1875
  total_value_units: 18350.0
  total_value_structures: 2875.0
  killed_value_units: 10900.0
  killed_value_structures: 150.0
  collected_minerals: 21345.0
  collected_vespene: 6860.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 940.0
  spent_minerals: 20093.0
  spent_vespene: 4556.0
  food_used {
    none: 0.0
    army: 74.5
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6650.0
    economy: 1100.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4606.0
    economy: 312.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1056.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 900.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 4150.0
    economy: 7225.0
    technology: 2325.0
    upgrade: 1125.0
  }
  used_vespene {
    none: 125.0
    army: 1150.0
    economy: 25.0
    technology: 500.0
    upgrade: 1125.0
  }
  total_used_minerals {
    none: 300.0
    army: 10350.0
    economy: 8900.0
    technology: 2275.0
    upgrade: 775.0
  }
  total_used_vespene {
    none: 300.0
    army: 2950.0
    economy: 50.0
    technology: 450.0
    upgrade: 775.0
  }
  total_damage_dealt {
    life: 5771.27880859
    shields: 8971.27929688
    energy: 0.0
  }
  total_damage_taken {
    life: 7997.89160156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1302
vespene: 2304
food_cap: 200
food_used: 133
food_army: 74
food_workers: 59
idle_worker_count: 3
army_count: 54
warp_gate_count: 0

game_loop:  18699
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 54
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 8
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  21456
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
