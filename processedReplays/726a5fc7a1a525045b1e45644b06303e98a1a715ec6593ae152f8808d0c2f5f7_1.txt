----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3496
  player_apm: 68
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3141
  player_apm: 79
}
game_duration_loops: 28031
game_duration_seconds: 1251.47131348
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9765
score_details {
  idle_production_time: 1704.8125
  idle_worker_time: 455.1875
  total_value_units: 3750.0
  total_value_structures: 4000.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 7205.0
  collected_vespene: 1860.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 627.0
  spent_minerals: 6925.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1425.0
    economy: 4000.0
    technology: 1750.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 375.0
    economy: 0.0
    technology: 550.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 1325.0
    economy: 4350.0
    technology: 1650.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 375.0
    economy: 0.0
    technology: 550.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 313.742675781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 374.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 330
vespene: 435
food_cap: 78
food_used: 69
food_army: 29
food_workers: 39
idle_worker_count: 1
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 17476
score_details {
  idle_production_time: 6522.125
  idle_worker_time: 2337.5
  total_value_units: 11525.0
  total_value_structures: 6325.0
  killed_value_units: 5175.0
  killed_value_structures: 125.0
  collected_minerals: 16080.0
  collected_vespene: 6696.0
  collection_rate_minerals: 419.0
  collection_rate_vespene: 649.0
  spent_minerals: 15500.0
  spent_vespene: 4600.0
  food_used {
    none: 0.0
    army: 64.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2725.0
    economy: 1600.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3600.0
    economy: 1050.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3475.0
    economy: 3800.0
    technology: 3075.0
    upgrade: 1150.0
  }
  used_vespene {
    none: 50.0
    army: 1350.0
    economy: 0.0
    technology: 650.0
    upgrade: 1150.0
  }
  total_used_minerals {
    none: 50.0
    army: 6825.0
    economy: 5200.0
    technology: 3175.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 50.0
    army: 2500.0
    economy: 0.0
    technology: 650.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 7624.29833984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6215.06982422
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 446.36328125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 630
vespene: 2096
food_cap: 102
food_used: 94
food_army: 64
food_workers: 30
idle_worker_count: 5
army_count: 36
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 108
    shields: 0
    energy: 170
    transport_slots_taken: 1
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11374
score_details {
  idle_production_time: 10924.875
  idle_worker_time: 5630.125
  total_value_units: 14650.0
  total_value_structures: 7275.0
  killed_value_units: 11350.0
  killed_value_structures: 125.0
  collected_minerals: 21325.0
  collected_vespene: 8100.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 18800.0
  spent_vespene: 6025.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5900.0
    economy: 2450.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8153.0
    economy: 3050.0
    technology: 2672.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3276.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 450.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 725.0
    economy: 1550.0
    technology: 1025.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 50.0
    army: 475.0
    economy: 0.0
    technology: 375.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 50.0
    army: 8775.0
    economy: 5350.0
    technology: 3975.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 50.0
    army: 3675.0
    economy: 0.0
    technology: 650.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 16339.9746094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23926.9707031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 925.77734375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2550
vespene: 2074
food_cap: 71
food_used: 16
food_army: 16
food_workers: 0
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  28031
ui_data{
 multi {
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  11374
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
