----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4118
  player_apm: 145
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4435
  player_apm: 237
}
game_duration_loops: 14148
game_duration_seconds: 631.651245117
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10843
score_details {
  idle_production_time: 8969.25
  idle_worker_time: 13.0
  total_value_units: 7400.0
  total_value_structures: 2100.0
  killed_value_units: 650.0
  killed_value_structures: 0.0
  collected_minerals: 9585.0
  collected_vespene: 1464.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 492.0
  spent_minerals: 9606.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 775.0
    economy: 331.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2425.0
    economy: 4650.0
    technology: 1750.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 250.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 3100.0
    economy: 5600.0
    technology: 1750.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 1081.74389648
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2525.54052734
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 29
vespene: 64
food_cap: 98
food_used: 85
food_army: 39
food_workers: 40
idle_worker_count: 0
army_count: 30
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 19
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11011
score_details {
  idle_production_time: 18112.0625
  idle_worker_time: 13.0
  total_value_units: 12600.0
  total_value_structures: 2425.0
  killed_value_units: 3800.0
  killed_value_structures: 450.0
  collected_minerals: 14275.0
  collected_vespene: 3792.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 851.0
  spent_minerals: 13706.0
  spent_vespene: 2875.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3175.0
    economy: 400.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4825.0
    economy: 131.0
    technology: 275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 600.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 400.0
    economy: 5275.0
    technology: 1750.0
    upgrade: 900.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 250.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 0.0
    army: 6550.0
    economy: 6800.0
    technology: 2175.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 350.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 6731.19628906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8293.41503906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 619
vespene: 917
food_cap: 90
food_used: 63
food_army: 8
food_workers: 55
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  14148
ui_data{
 
Score:  11011
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
