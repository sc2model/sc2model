----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2406
  player_apm: 113
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3600
  player_apm: 89
}
game_duration_loops: 16208
game_duration_seconds: 723.621948242
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11184
score_details {
  idle_production_time: 1169.5625
  idle_worker_time: 486.0
  total_value_units: 4775.0
  total_value_structures: 4500.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 8750.0
  collected_vespene: 1984.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 671.0
  spent_minerals: 8600.0
  spent_vespene: 1925.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4500.0
    technology: 2450.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 400.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 1150.0
    economy: 4950.0
    technology: 2000.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 400.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 80.0
    shields: 157.0
    energy: 0.0
  }
  total_damage_taken {
    life: 220.0
    shields: 235.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 200
vespene: 59
food_cap: 110
food_used: 78
food_army: 30
food_workers: 46
idle_worker_count: 1
army_count: 13
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

score{
 score_type: Melee
score: 20587
score_details {
  idle_production_time: 3544.5
  idle_worker_time: 712.6875
  total_value_units: 9550.0
  total_value_structures: 7600.0
  killed_value_units: 6950.0
  killed_value_structures: 925.0
  collected_minerals: 16895.0
  collected_vespene: 4992.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 627.0
  spent_minerals: 14900.0
  spent_vespene: 4850.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4375.0
    economy: 1075.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1375.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2600.0
    economy: 6700.0
    technology: 3650.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 0.0
    army: 2500.0
    economy: 0.0
    technology: 400.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 0.0
    army: 4425.0
    economy: 7450.0
    technology: 3200.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 4375.0
    economy: 0.0
    technology: 400.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 5311.9375
    shields: 5796.375
    energy: 0.0
  }
  total_damage_taken {
    life: 1520.0
    shields: 3566.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2045
vespene: 142
food_cap: 196
food_used: 113
food_army: 57
food_workers: 56
idle_worker_count: 1
army_count: 23
warp_gate_count: 6

game_loop:  16208
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 18
}
groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  20587
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
