----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 86
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2588
  player_apm: 79
}
game_duration_loops: 12529
game_duration_seconds: 559.369384766
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7118
score_details {
  idle_production_time: 1641.5625
  idle_worker_time: 43.4375
  total_value_units: 3750.0
  total_value_structures: 2850.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 6170.0
  collected_vespene: 1948.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 335.0
  spent_minerals: 5200.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 950.0
    economy: 2150.0
    technology: 1700.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 2150.0
    technology: 1550.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1027.0
    shields: 1636.875
    energy: 0.0
  }
  total_damage_taken {
    life: 1320.0
    shields: 2368.68261719
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1020
vespene: 498
food_cap: 55
food_used: 37
food_army: 15
food_workers: 22
idle_worker_count: 0
army_count: 4
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 8250
score_details {
  idle_production_time: 2160.9375
  idle_worker_time: 44.75
  total_value_units: 6575.0
  total_value_structures: 3450.0
  killed_value_units: 2800.0
  killed_value_structures: 525.0
  collected_minerals: 7715.0
  collected_vespene: 2560.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 313.0
  spent_minerals: 7125.0
  spent_vespene: 2400.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 1675.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 2150.0
    technology: 2000.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 3575.0
    economy: 2150.0
    technology: 2150.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3155.0
    shields: 4189.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1890.0
    shields: 3730.68261719
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 640
vespene: 160
food_cap: 55
food_used: 52
food_army: 30
food_workers: 22
idle_worker_count: 0
army_count: 13
warp_gate_count: 5

game_loop:  12529
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 42
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  8250
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
