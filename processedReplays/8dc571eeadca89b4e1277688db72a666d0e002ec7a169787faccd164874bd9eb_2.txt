----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4543
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4532
  player_apm: 173
}
game_duration_loops: 8849
game_duration_seconds: 395.07220459
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8395
score_details {
  idle_production_time: 1138.125
  idle_worker_time: 123.625
  total_value_units: 5200.0
  total_value_structures: 3750.0
  killed_value_units: 500.0
  killed_value_structures: 600.0
  collected_minerals: 7350.0
  collected_vespene: 2120.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 671.0
  spent_minerals: 7150.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 450.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1150.0
    economy: 3600.0
    technology: 1400.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4500.0
    technology: 1400.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 3227.13330078
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 810.375
    shields: 1058.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 250
vespene: 620
food_cap: 102
food_used: 44
food_army: 20
food_workers: 24
idle_worker_count: 0
army_count: 9
warp_gate_count: 5

game_loop:  8849
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 56
    energy: 0
  }
}

Score:  8395
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
