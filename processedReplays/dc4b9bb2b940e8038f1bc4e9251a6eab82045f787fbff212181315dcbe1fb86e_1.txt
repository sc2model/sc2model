----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3871
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 95
}
game_duration_loops: 9186
game_duration_seconds: 410.117889404
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6366
score_details {
  idle_production_time: 1654.1875
  idle_worker_time: 301.6875
  total_value_units: 3400.0
  total_value_structures: 3400.0
  killed_value_units: 1075.0
  killed_value_structures: 0.0
  collected_minerals: 6705.0
  collected_vespene: 1788.0
  collection_rate_minerals: 195.0
  collection_rate_vespene: 291.0
  spent_minerals: 6475.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 10.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1177.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 500.0
    economy: 3000.0
    technology: 1075.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 525.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1150.0
    economy: 4400.0
    technology: 1075.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 525.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 662.0
    shields: 1289.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2958.80517578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 90.8051757812
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 253
vespene: 788
food_cap: 86
food_used: 20
food_army: 10
food_workers: 8
idle_worker_count: 8
army_count: 0
warp_gate_count: 0

game_loop:  9186
ui_data{
 
Score:  6366
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
