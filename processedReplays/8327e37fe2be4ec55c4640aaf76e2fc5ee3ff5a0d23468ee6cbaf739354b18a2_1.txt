----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5354
  player_apm: 222
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5314
  player_apm: 186
}
game_duration_loops: 11079
game_duration_seconds: 494.632720947
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10831
score_details {
  idle_production_time: 5293.625
  idle_worker_time: 0.4375
  total_value_units: 7500.0
  total_value_structures: 1675.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 8965.0
  collected_vespene: 1872.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 627.0
  spent_minerals: 8206.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 725.0
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 5100.0
    technology: 1025.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2750.0
    economy: 5700.0
    technology: 1175.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 2010.03222656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1577.81835938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 809
vespene: 647
food_cap: 106
food_used: 89
food_army: 38
food_workers: 51
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11664
score_details {
  idle_production_time: 6921.1875
  idle_worker_time: 0.4375
  total_value_units: 8550.0
  total_value_structures: 1675.0
  killed_value_units: 4350.0
  killed_value_structures: 0.0
  collected_minerals: 10745.0
  collected_vespene: 2400.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 671.0
  spent_minerals: 9306.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 34.5
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2600.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1725.0
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 5100.0
    technology: 1025.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 3700.0
    economy: 5800.0
    technology: 1175.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 6034.04785156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3862.21142578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1489
vespene: 1175
food_cap: 114
food_used: 85
food_army: 34
food_workers: 51
idle_worker_count: 0
army_count: 40
warp_gate_count: 0
larva_count: 3

game_loop:  11079
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  11664
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
