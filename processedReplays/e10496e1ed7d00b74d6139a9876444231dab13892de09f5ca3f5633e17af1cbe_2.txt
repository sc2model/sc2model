----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3700
  player_apm: 208
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3717
  player_apm: 319
}
game_duration_loops: 9226
game_duration_seconds: 411.903747559
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4304
score_details {
  idle_production_time: 4665.875
  idle_worker_time: 152.3125
  total_value_units: 6500.0
  total_value_structures: 1875.0
  killed_value_units: 1950.0
  killed_value_structures: 0.0
  collected_minerals: 7105.0
  collected_vespene: 824.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 111.0
  spent_minerals: 7050.0
  spent_vespene: 575.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2500.0
    economy: 1450.0
    technology: 325.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 3000.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 4950.0
    technology: 725.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2614.83349609
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9507.89453125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 105
vespene: 249
food_cap: 82
food_used: 21
food_army: 4
food_workers: 16
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  9226
ui_data{
 single {
  unit {
    unit_type: 110
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
}

Score:  4304
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
