----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4184
  player_apm: 278
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4006
  player_apm: 214
}
game_duration_loops: 17795
game_duration_seconds: 794.475097656
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7754
score_details {
  idle_production_time: 1074.9375
  idle_worker_time: 360.25
  total_value_units: 4775.0
  total_value_structures: 3025.0
  killed_value_units: 2425.0
  killed_value_structures: 0.0
  collected_minerals: 8160.0
  collected_vespene: 1144.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 0.0
  spent_minerals: 7350.0
  spent_vespene: 800.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1700.0
    economy: 700.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 475.0
    economy: 4450.0
    technology: 800.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1975.0
    economy: 4950.0
    technology: 800.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3567.85595703
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4207.57666016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 591.649414062
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 860
vespene: 344
food_cap: 86
food_used: 50
food_army: 10
food_workers: 40
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 240
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1170
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 9531
score_details {
  idle_production_time: 3813.375
  idle_worker_time: 1350.75
  total_value_units: 11500.0
  total_value_structures: 6100.0
  killed_value_units: 6150.0
  killed_value_structures: 0.0
  collected_minerals: 16840.0
  collected_vespene: 3224.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 313.0
  spent_minerals: 16450.0
  spent_vespene: 2525.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5025.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5925.0
    economy: 3275.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1525.0
    economy: 3300.0
    technology: 1650.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 500.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 6975.0
    economy: 7500.0
    technology: 2050.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 500.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 9319.77929688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16855.953125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2471.56542969
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 436
vespene: 695
food_cap: 110
food_used: 52
food_army: 34
food_workers: 18
idle_worker_count: 15
army_count: 15
warp_gate_count: 0

game_loop:  17795
ui_data{
 
Score:  9531
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
