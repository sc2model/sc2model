----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4434
  player_apm: 331
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4129
  player_apm: 190
}
game_duration_loops: 20228
game_duration_seconds: 903.098754883
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13806
score_details {
  idle_production_time: 8583.25
  idle_worker_time: 149.6875
  total_value_units: 7100.0
  total_value_structures: 2375.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 11540.0
  collected_vespene: 1716.0
  collection_rate_minerals: 2659.0
  collection_rate_vespene: 627.0
  spent_minerals: 10050.0
  spent_vespene: 700.0
  food_used {
    none: 4.0
    army: 26.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1950.0
    economy: 6950.0
    technology: 1300.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 150.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 7550.0
    technology: 1025.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 917.810058594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2019.13867188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1540
vespene: 1016
food_cap: 142
food_used: 90
food_army: 26
food_workers: 60
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 14
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17037
score_details {
  idle_production_time: 43110.125
  idle_worker_time: 1721.6875
  total_value_units: 29350.0
  total_value_structures: 3875.0
  killed_value_units: 12500.0
  killed_value_structures: 0.0
  collected_minerals: 29105.0
  collected_vespene: 8132.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 806.0
  spent_minerals: 28775.0
  spent_vespene: 6750.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12875.0
    economy: 1575.0
    technology: 275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2950.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2500.0
    economy: 7025.0
    technology: 2300.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 600.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 20150.0
    economy: 9700.0
    technology: 2525.0
    upgrade: 975.0
  }
  total_used_vespene {
    none: 0.0
    army: 5275.0
    economy: 0.0
    technology: 600.0
    upgrade: 975.0
  }
  total_damage_dealt {
    life: 12433.9140625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24102.5039062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 380
vespene: 1382
food_cap: 150
food_used: 91
food_army: 39
food_workers: 52
idle_worker_count: 0
army_count: 41
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}

score{
 score_type: Melee
score: 14973
score_details {
  idle_production_time: 43980.4375
  idle_worker_time: 1850.875
  total_value_units: 29500.0
  total_value_structures: 3875.0
  killed_value_units: 12900.0
  killed_value_structures: 0.0
  collected_minerals: 29375.0
  collected_vespene: 8248.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 828.0
  spent_minerals: 28775.0
  spent_vespene: 6750.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13725.0
    economy: 1300.0
    technology: 325.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3175.0
    economy: 0.0
    technology: 87.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 6725.0
    technology: 2100.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 450.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 20300.0
    economy: 9700.0
    technology: 2525.0
    upgrade: 975.0
  }
  total_used_vespene {
    none: 0.0
    army: 5275.0
    economy: 0.0
    technology: 600.0
    upgrade: 975.0
  }
  total_damage_dealt {
    life: 13782.5341797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25213.5429688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 650
vespene: 1498
food_cap: 150
food_used: 73
food_army: 20
food_workers: 53
idle_worker_count: 53
army_count: 13
warp_gate_count: 0

game_loop:  20228
ui_data{
 
Score:  14973
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
