----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4322
  player_apm: 145
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4459
  player_apm: 102
}
game_duration_loops: 7380
game_duration_seconds: 329.48727417
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5585
score_details {
  idle_production_time: 841.125
  idle_worker_time: 320.375
  total_value_units: 4050.0
  total_value_structures: 2025.0
  killed_value_units: 2025.0
  killed_value_structures: 275.0
  collected_minerals: 4515.0
  collected_vespene: 1320.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 335.0
  spent_minerals: 4250.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 1250.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 875.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 2200.0
    technology: 700.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2025.0
    economy: 2275.0
    technology: 700.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3718.68017578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1090.0
    shields: 981.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 315
vespene: 145
food_cap: 55
food_used: 46
food_army: 23
food_workers: 23
idle_worker_count: 1
army_count: 6
warp_gate_count: 0

game_loop:  7380
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 62
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 71
  count: 2
}
multi {
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

Score:  5585
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
