----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3527
  player_apm: 50
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3375
  player_apm: 85
}
game_duration_loops: 29092
game_duration_seconds: 1298.84057617
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11698
score_details {
  idle_production_time: 4434.4375
  idle_worker_time: 12.4375
  total_value_units: 5950.0
  total_value_structures: 2300.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8040.0
  collected_vespene: 2608.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 447.0
  spent_minerals: 7400.0
  spent_vespene: 850.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1300.0
    economy: 4900.0
    technology: 2000.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1250.0
    economy: 5750.0
    technology: 2150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 5.0
    energy: 0.0
  }
  total_damage_taken {
    life: 170.734863281
    shields: 50.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 690
vespene: 1758
food_cap: 122
food_used: 67
food_army: 22
food_workers: 43
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 55
  }
}

score{
 score_type: Melee
score: 18365
score_details {
  idle_production_time: 24001.4375
  idle_worker_time: 641.25
  total_value_units: 18150.0
  total_value_structures: 4600.0
  killed_value_units: 3350.0
  killed_value_structures: 550.0
  collected_minerals: 20753.0
  collected_vespene: 5612.0
  collection_rate_minerals: 1467.0
  collection_rate_vespene: 716.0
  spent_minerals: 19775.0
  spent_vespene: 5475.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1875.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5450.0
    economy: 625.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2800.0
    economy: 6200.0
    technology: 4250.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 400.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 8100.0
    economy: 8450.0
    technology: 4900.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 500.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 3505.875
    shields: 3552.875
    energy: 0.0
  }
  total_damage_taken {
    life: 9244.54492188
    shields: 50.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1028
vespene: 137
food_cap: 144
food_used: 97
food_army: 54
food_workers: 43
idle_worker_count: 5
army_count: 24
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9687
score_details {
  idle_production_time: 35555.5
  idle_worker_time: 1244.8125
  total_value_units: 27700.0
  total_value_structures: 5025.0
  killed_value_units: 8200.0
  killed_value_structures: 1100.0
  collected_minerals: 26091.0
  collected_vespene: 9796.0
  collection_rate_minerals: 391.0
  collection_rate_vespene: 223.0
  spent_minerals: 25875.0
  spent_vespene: 9200.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 10.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4025.0
    economy: 2600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13250.0
    economy: 4550.0
    technology: 2300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 250.0
    economy: 2700.0
    technology: 2575.0
    upgrade: 1300.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 400.0
    upgrade: 1300.0
  }
  total_used_minerals {
    none: 0.0
    army: 13600.0
    economy: 9400.0
    technology: 5025.0
    upgrade: 1300.0
  }
  total_used_vespene {
    none: 0.0
    army: 7600.0
    economy: 0.0
    technology: 500.0
    upgrade: 1300.0
  }
  total_damage_dealt {
    life: 7512.20800781
    shields: 8340.12304688
    energy: 0.0
  }
  total_damage_taken {
    life: 37143.015625
    shields: 50.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 266
vespene: 596
food_cap: 126
food_used: 18
food_army: 8
food_workers: 10
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  29092
ui_data{
 
Score:  9687
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
