----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2604
  player_apm: 106
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 96
}
game_duration_loops: 23323
game_duration_seconds: 1041.27807617
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11989
score_details {
  idle_production_time: 1145.375
  idle_worker_time: 9.4375
  total_value_units: 5275.0
  total_value_structures: 4100.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9065.0
  collected_vespene: 2224.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 671.0
  spent_minerals: 8750.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2200.0
    economy: 4800.0
    technology: 1850.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 400.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2175.0
    economy: 4750.0
    technology: 1400.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 127.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 49.0
    shields: 98.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 365
vespene: 549
food_cap: 109
food_used: 98
food_army: 48
food_workers: 48
idle_worker_count: 0
army_count: 25
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 73
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 24795
score_details {
  idle_production_time: 5825.125
  idle_worker_time: 692.9375
  total_value_units: 18525.0
  total_value_structures: 8525.0
  killed_value_units: 9450.0
  killed_value_structures: 725.0
  collected_minerals: 23945.0
  collected_vespene: 9400.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 1142.0
  spent_minerals: 22925.0
  spent_vespene: 7825.0
  food_used {
    none: 0.0
    army: 110.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6450.0
    economy: 1575.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6125.0
    economy: 125.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5225.0
    economy: 8000.0
    technology: 2750.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 750.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 0.0
    army: 11600.0
    economy: 8425.0
    technology: 2750.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 7125.0
    economy: 0.0
    technology: 750.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 12656.2265625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6179.0625
    shields: 7697.11914062
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1070
vespene: 1575
food_cap: 200
food_used: 175
food_army: 110
food_workers: 65
idle_worker_count: 5
army_count: 47
warp_gate_count: 9

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 73
  count: 25
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 6
    shields: 50
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 176
    add_on {
      unit_type: 10
      build_progress: 0.832499980927
    }
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 57
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 11
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 99
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 88
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 96
    shields: 50
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 79
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 34
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 63
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 16
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 149
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 130
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 110
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 92
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 109
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 109
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 98
    energy: 0
  }
}

score{
 score_type: Melee
score: 31762
score_details {
  idle_production_time: 7451.3125
  idle_worker_time: 1111.4375
  total_value_units: 20205.0
  total_value_structures: 9975.0
  killed_value_units: 13975.0
  killed_value_structures: 1625.0
  collected_minerals: 28880.0
  collected_vespene: 12292.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 1321.0
  spent_minerals: 25465.0
  spent_vespene: 9075.0
  food_used {
    none: 0.0
    army: 108.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9200.0
    economy: 2725.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6925.0
    economy: 125.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5255.0
    economy: 8850.0
    technology: 3350.0
    upgrade: 1600.0
  }
  used_vespene {
    none: 0.0
    army: 3675.0
    economy: 0.0
    technology: 750.0
    upgrade: 1600.0
  }
  total_used_minerals {
    none: 0.0
    army: 13230.0
    economy: 9275.0
    technology: 3350.0
    upgrade: 1500.0
  }
  total_used_vespene {
    none: 0.0
    army: 9275.0
    economy: 0.0
    technology: 750.0
    upgrade: 1500.0
  }
  total_damage_dealt {
    life: 20178.6210938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7026.0625
    shields: 8650.11914062
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3465
vespene: 3217
food_cap: 200
food_used: 173
food_army: 108
food_workers: 65
idle_worker_count: 0
army_count: 41
warp_gate_count: 9

game_loop:  23323
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 35
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 84
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 10
    player_relative: 1
    health: 294
    shields: 0
    energy: 200
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 40
    shields: 12
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 74
    shields: 0
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 31
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 91
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 68
    shields: 15
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 96
    shields: 50
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 13
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 80
    shields: 13
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 74
    shields: 5
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 50
    shields: 10
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 43
    shields: 12
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 42
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 39
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 52
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 136
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 135
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 92
    shields: 15
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 68
    shields: 0
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 331
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 18
    shields: 3
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 33
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 299
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 18
    energy: 136
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 136
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 309
    energy: 0
  }
}

Score:  31762
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
