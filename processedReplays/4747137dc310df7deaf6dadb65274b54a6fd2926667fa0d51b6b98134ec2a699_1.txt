----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4047
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4063
  player_apm: 170
}
game_duration_loops: 9694
game_duration_seconds: 432.798065186
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9847
score_details {
  idle_production_time: 1220.5625
  idle_worker_time: 150.125
  total_value_units: 5925.0
  total_value_structures: 4350.0
  killed_value_units: 3500.0
  killed_value_structures: 0.0
  collected_minerals: 8550.0
  collected_vespene: 1872.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 313.0
  spent_minerals: 8575.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2150.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4675.0
    technology: 1700.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2800.0
    economy: 4700.0
    technology: 1700.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1905.0
    shields: 1645.25
    energy: 0.0
  }
  total_damage_taken {
    life: 1803.0
    shields: 1655.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 25
vespene: 497
food_cap: 109
food_used: 82
food_army: 33
food_workers: 48
idle_worker_count: 0
army_count: 13
warp_gate_count: 7

game_loop:  9694
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

Score:  9847
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
