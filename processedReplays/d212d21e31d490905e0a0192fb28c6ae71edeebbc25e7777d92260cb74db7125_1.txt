----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 302
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5912
  player_apm: 302
}
game_duration_loops: 21695
game_duration_seconds: 968.594360352
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13492
score_details {
  idle_production_time: 1450.875
  idle_worker_time: 400.9375
  total_value_units: 6600.0
  total_value_structures: 5050.0
  killed_value_units: 825.0
  killed_value_structures: 350.0
  collected_minerals: 10705.0
  collected_vespene: 2824.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 985.0
  spent_minerals: 10587.0
  spent_vespene: 2550.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 725.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2450.0
    economy: 5900.0
    technology: 2200.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 450.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2950.0
    economy: 5450.0
    technology: 2050.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 2435.26098633
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7.75
    shields: 901.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 168
vespene: 274
food_cap: 117
food_used: 104
food_army: 43
food_workers: 58
idle_worker_count: 1
army_count: 10
warp_gate_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 83
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 81
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 8
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 3
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 25853
score_details {
  idle_production_time: 5900.0
  idle_worker_time: 1813.8125
  total_value_units: 24450.0
  total_value_structures: 10500.0
  killed_value_units: 18650.0
  killed_value_structures: 975.0
  collected_minerals: 28525.0
  collected_vespene: 11240.0
  collection_rate_minerals: 2659.0
  collection_rate_vespene: 1254.0
  spent_minerals: 28012.0
  spent_vespene: 11025.0
  food_used {
    none: 0.0
    army: 107.0
    economy: 78.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13850.0
    economy: 2150.0
    technology: 275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8175.0
    economy: 1200.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5200.0
    economy: 8750.0
    technology: 4450.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 3725.0
    economy: 0.0
    technology: 800.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 14700.0
    economy: 9950.0
    technology: 4150.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 11650.0
    economy: 0.0
    technology: 800.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 24550.9121094
    shields: 150.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7656.79785156
    shields: 13472.6660156
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 563
vespene: 215
food_cap: 200
food_used: 185
food_army: 107
food_workers: 78
idle_worker_count: 6
army_count: 41
warp_gate_count: 12

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 141
  count: 16
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 13
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 5
}
multi {
  units {
    unit_type: 75
    player_relative: 1
    health: 35
    shields: 35
    energy: 50
    build_progress: 0.87498664856
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 23
    shields: 23
    energy: 50
    build_progress: 0.524991989136
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 24
    shields: 24
    energy: 50
    build_progress: 0.562491416931
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 30
    shields: 30
    energy: 50
    build_progress: 0.712489128113
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 50
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 50
    build_progress: 0.999984741211
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 31
    shields: 31
    energy: 50
    build_progress: 0.749988555908
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 37
    shields: 37
    energy: 50
    build_progress: 0.912486076355
  }
}

score{
 score_type: Melee
score: 28857
score_details {
  idle_production_time: 6228.5
  idle_worker_time: 3041.0625
  total_value_units: 28200.0
  total_value_structures: 10800.0
  killed_value_units: 25900.0
  killed_value_structures: 975.0
  collected_minerals: 31495.0
  collected_vespene: 12824.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 1231.0
  spent_minerals: 28537.0
  spent_vespene: 11800.0
  food_used {
    none: 0.0
    army: 101.0
    economy: 78.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 18825.0
    economy: 3100.0
    technology: 275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9375.0
    economy: 1200.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5375.0
    economy: 8750.0
    technology: 4450.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 800.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 17375.0
    economy: 9950.0
    technology: 4450.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 15425.0
    economy: 0.0
    technology: 800.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 31734.0742188
    shields: 150.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8319.79785156
    shields: 16270.7910156
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3008
vespene: 1024
food_cap: 200
food_used: 179
food_army: 101
food_workers: 78
idle_worker_count: 16
army_count: 36
warp_gate_count: 12

game_loop:  21695
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 73
  count: 12
}
groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 141
  count: 13
}
groups {
  control_group_index: 3
  leader_unit_type: 141
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 13
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 5
}
multi {
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 266
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 212
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.177083313465
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 158
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 16
    energy: 0
  }
}

Score:  28857
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
