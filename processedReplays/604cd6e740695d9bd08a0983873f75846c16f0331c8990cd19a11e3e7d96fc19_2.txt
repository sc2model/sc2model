----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2827
  player_apm: 41
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2797
  player_apm: 64
}
game_duration_loops: 9779
game_duration_seconds: 436.592956543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1167
score_details {
  idle_production_time: 909.625
  idle_worker_time: 1065.875
  total_value_units: 1975.0
  total_value_structures: 1875.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 3025.0
  collected_vespene: 992.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 335.0
  spent_minerals: 2575.0
  spent_vespene: 825.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 2.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 550.0
    economy: 2250.0
    technology: 725.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 201.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 330.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 500
vespene: 167
food_cap: 0
food_used: 2
food_army: 0
food_workers: 2
idle_worker_count: 2
army_count: 0
warp_gate_count: 0

game_loop:  9779
ui_data{
 
Score:  1167
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
