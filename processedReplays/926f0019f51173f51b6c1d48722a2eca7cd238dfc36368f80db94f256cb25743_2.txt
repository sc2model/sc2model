----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3617
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3728
  player_apm: 145
}
game_duration_loops: 26077
game_duration_seconds: 1164.23303223
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12440
score_details {
  idle_production_time: 5717.625
  idle_worker_time: 21.0625
  total_value_units: 6200.0
  total_value_structures: 1950.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 9660.0
  collected_vespene: 1980.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 671.0
  spent_minerals: 8450.0
  spent_vespene: 1075.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1325.0
    economy: 5725.0
    technology: 1450.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 150.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 1575.0
    economy: 5900.0
    technology: 1600.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 250.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 480.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 820.425292969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1260
vespene: 905
food_cap: 114
food_used: 79
food_army: 24
food_workers: 49
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 97
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 65
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 28664
score_details {
  idle_production_time: 36953.0625
  idle_worker_time: 452.3125
  total_value_units: 23400.0
  total_value_structures: 4150.0
  killed_value_units: 6650.0
  killed_value_structures: 400.0
  collected_minerals: 25405.0
  collected_vespene: 9184.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 1455.0
  spent_minerals: 23500.0
  spent_vespene: 8150.0
  food_used {
    none: 0.0
    army: 107.5
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5800.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3625.0
    economy: 800.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6175.0
    economy: 8800.0
    technology: 2350.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 0.0
    army: 3650.0
    economy: 0.0
    technology: 950.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 0.0
    army: 11675.0
    economy: 11000.0
    technology: 2900.0
    upgrade: 1425.0
  }
  total_used_vespene {
    none: 0.0
    army: 5575.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1425.0
  }
  total_damage_dealt {
    life: 6932.55908203
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9332.04394531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1955
vespene: 1034
food_cap: 200
food_used: 173
food_army: 107
food_workers: 66
idle_worker_count: 3
army_count: 68
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 36066
score_details {
  idle_production_time: 65107.0625
  idle_worker_time: 457.75
  total_value_units: 24800.0
  total_value_structures: 4900.0
  killed_value_units: 14400.0
  killed_value_structures: 400.0
  collected_minerals: 33635.0
  collected_vespene: 15656.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 1433.0
  spent_minerals: 26600.0
  spent_vespene: 9800.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12100.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6825.0
    economy: 1825.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4025.0
    economy: 8400.0
    technology: 2925.0
    upgrade: 2225.0
  }
  used_vespene {
    none: 0.0
    army: 2375.0
    economy: 0.0
    technology: 950.0
    upgrade: 2225.0
  }
  total_used_minerals {
    none: 0.0
    army: 14025.0
    economy: 11725.0
    technology: 3675.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 0.0
    army: 7675.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 14907.7314453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20233.6699219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 7085
vespene: 5856
food_cap: 200
food_used: 128
food_army: 68
food_workers: 60
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  26077
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 129
  count: 5
}
single {
  unit {
    unit_type: 341
    player_relative: 3
    health: 10000
    shields: 0
    energy: 0
  }
}

Score:  36066
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
