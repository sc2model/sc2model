----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3367
  player_apm: 342
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3323
  player_apm: 286
}
game_duration_loops: 20038
game_duration_seconds: 894.616027832
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10989
score_details {
  idle_production_time: 846.0
  idle_worker_time: 332.625
  total_value_units: 5525.0
  total_value_structures: 3300.0
  killed_value_units: 625.0
  killed_value_structures: 0.0
  collected_minerals: 8475.0
  collected_vespene: 2164.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 627.0
  spent_minerals: 8100.0
  spent_vespene: 1475.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 275.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 4900.0
    technology: 1700.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1950.0
    economy: 4750.0
    technology: 950.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 540.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 454.5
    shields: 1147.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 425
vespene: 689
food_cap: 102
food_used: 76
food_army: 29
food_workers: 47
idle_worker_count: 0
army_count: 13
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 8
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 253
    shields: 253
    energy: 0
    build_progress: 0.449999988079
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 238
    shields: 238
    energy: 0
    build_progress: 0.418269217014
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 208
    shields: 208
    energy: 0
    build_progress: 0.351923048496
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 179
    shields: 179
    energy: 0
    build_progress: 0.285576939583
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 176
    shields: 176
    energy: 0
    build_progress: 0.279807686806
  }
}

score{
 score_type: Melee
score: 22922
score_details {
  idle_production_time: 5566.75
  idle_worker_time: 1267.0
  total_value_units: 20850.0
  total_value_structures: 9400.0
  killed_value_units: 11325.0
  killed_value_structures: 925.0
  collected_minerals: 24785.0
  collected_vespene: 8212.0
  collection_rate_minerals: 2491.0
  collection_rate_vespene: 783.0
  spent_minerals: 24525.0
  spent_vespene: 7825.0
  food_used {
    none: 0.0
    army: 75.0
    economy: 69.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7875.0
    economy: 1525.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7525.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3825.0
    economy: 7750.0
    technology: 4550.0
    upgrade: 1100.0
  }
  used_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1100.0
  }
  total_used_minerals {
    none: 0.0
    army: 11575.0
    economy: 7900.0
    technology: 4250.0
    upgrade: 1100.0
  }
  total_used_vespene {
    none: 0.0
    army: 5375.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1100.0
  }
  total_damage_dealt {
    life: 13510.4160156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6221.5
    shields: 8126.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 310
vespene: 387
food_cap: 196
food_used: 144
food_army: 75
food_workers: 69
idle_worker_count: 3
army_count: 40
warp_gate_count: 16

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 19
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 16
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 16
    shields: 41
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 32
    shields: 53
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 5
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 10
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 34
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 42
    shields: 10
    energy: 0
  }
}

score{
 score_type: Melee
score: 23011
score_details {
  idle_production_time: 5604.75
  idle_worker_time: 1274.125
  total_value_units: 20850.0
  total_value_structures: 9900.0
  killed_value_units: 11450.0
  killed_value_structures: 925.0
  collected_minerals: 24850.0
  collected_vespene: 8236.0
  collection_rate_minerals: 2547.0
  collection_rate_vespene: 783.0
  spent_minerals: 24525.0
  spent_vespene: 7825.0
  food_used {
    none: 0.0
    army: 75.0
    economy: 69.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7975.0
    economy: 1525.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7525.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3825.0
    economy: 7750.0
    technology: 4550.0
    upgrade: 1100.0
  }
  used_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1100.0
  }
  total_used_minerals {
    none: 0.0
    army: 11575.0
    economy: 7900.0
    technology: 4550.0
    upgrade: 1100.0
  }
  total_used_vespene {
    none: 0.0
    army: 5375.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1100.0
  }
  total_damage_dealt {
    life: 13668.5830078
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6221.5
    shields: 8142.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 375
vespene: 411
food_cap: 196
food_used: 144
food_army: 75
food_workers: 69
idle_worker_count: 3
army_count: 40
warp_gate_count: 16

game_loop:  20038
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 19
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 16
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 16
    shields: 45
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 32
    shields: 57
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 5
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 10
    shields: 25
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 34
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 42
    shields: 14
    energy: 0
  }
}

Score:  23011
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
