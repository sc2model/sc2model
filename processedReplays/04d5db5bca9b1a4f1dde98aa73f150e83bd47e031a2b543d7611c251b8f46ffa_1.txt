----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3568
  player_apm: 92
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 134
}
game_duration_loops: 31784
game_duration_seconds: 1419.02758789
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6781
score_details {
  idle_production_time: 1509.0625
  idle_worker_time: 572.3125
  total_value_units: 4775.0
  total_value_structures: 2600.0
  killed_value_units: 2050.0
  killed_value_structures: 0.0
  collected_minerals: 6240.0
  collected_vespene: 1316.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 313.0
  spent_minerals: 5975.0
  spent_vespene: 1200.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1175.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1250.0
    economy: 3050.0
    technology: 925.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4225.0
    technology: 925.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2546.9921875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1994.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 298.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 315
vespene: 116
food_cap: 54
food_used: 52
food_army: 25
food_workers: 25
idle_worker_count: 1
army_count: 11
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 15560
score_details {
  idle_production_time: 6114.5625
  idle_worker_time: 3092.4375
  total_value_units: 13575.0
  total_value_structures: 5500.0
  killed_value_units: 8075.0
  killed_value_structures: 2300.0
  collected_minerals: 16210.0
  collected_vespene: 5052.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 403.0
  spent_minerals: 14800.0
  spent_vespene: 4600.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4700.0
    economy: 4300.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3750.0
    economy: 1252.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3350.0
    economy: 5050.0
    technology: 2025.0
    upgrade: 275.0
  }
  used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 775.0
    upgrade: 275.0
  }
  total_used_minerals {
    none: 0.0
    army: 6850.0
    economy: 6600.0
    technology: 2025.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 3425.0
    economy: 0.0
    technology: 775.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 16109.0839844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6184.51269531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 300.012695312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1458
vespene: 452
food_cap: 149
food_used: 106
food_army: 65
food_workers: 41
idle_worker_count: 3
army_count: 24
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  15560
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
