----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3194
  player_apm: 123
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3470
  player_apm: 121
}
game_duration_loops: 36792
game_duration_seconds: 1642.61462402
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12029
score_details {
  idle_production_time: 1187.875
  idle_worker_time: 83.75
  total_value_units: 5875.0
  total_value_structures: 4300.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8430.0
  collected_vespene: 2836.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 671.0
  spent_minerals: 8412.0
  spent_vespene: 2450.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 50.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1875.0
    economy: 4500.0
    technology: 2650.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1775.0
    economy: 4450.0
    technology: 1900.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 119.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 100.0
    shields: 60.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 68
vespene: 386
food_cap: 102
food_used: 90
food_army: 40
food_workers: 48
idle_worker_count: 0
army_count: 20
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

score{
 score_type: Melee
score: 32018
score_details {
  idle_production_time: 5526.8125
  idle_worker_time: 446.6875
  total_value_units: 17125.0
  total_value_structures: 11950.0
  killed_value_units: 1075.0
  killed_value_structures: 525.0
  collected_minerals: 24580.0
  collected_vespene: 9500.0
  collection_rate_minerals: 2659.0
  collection_rate_vespene: 1388.0
  spent_minerals: 23787.0
  spent_vespene: 8225.0
  food_used {
    none: 0.0
    army: 101.0
    economy: 96.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 775.0
    economy: 150.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2025.0
    economy: 50.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5175.0
    economy: 9150.0
    technology: 7150.0
    upgrade: 1775.0
  }
  used_vespene {
    none: 0.0
    army: 4275.0
    economy: 0.0
    technology: 600.0
    upgrade: 1775.0
  }
  total_used_minerals {
    none: 0.0
    army: 7975.0
    economy: 9200.0
    technology: 7000.0
    upgrade: 1550.0
  }
  total_used_vespene {
    none: 0.0
    army: 7900.0
    economy: 0.0
    technology: 600.0
    upgrade: 1550.0
  }
  total_damage_dealt {
    life: 3386.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1895.60839844
    shields: 1948.54589844
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 843
vespene: 1275
food_cap: 200
food_used: 197
food_army: 101
food_workers: 96
idle_worker_count: 0
army_count: 39
warp_gate_count: 28

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 19
}
groups {
  control_group_index: 2
  leader_unit_type: 694
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 4
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 36825
score_details {
  idle_production_time: 11073.375
  idle_worker_time: 9063.9375
  total_value_units: 35225.0
  total_value_structures: 17050.0
  killed_value_units: 19850.0
  killed_value_structures: 8825.0
  collected_minerals: 45010.0
  collected_vespene: 17352.0
  collection_rate_minerals: 3135.0
  collection_rate_vespene: 985.0
  spent_minerals: 41437.0
  spent_vespene: 14225.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 96.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9425.0
    economy: 7775.0
    technology: 4175.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6325.0
    economy: 600.0
    technology: 375.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 17225.0
    economy: 50.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 10425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3225.0
    economy: 11100.0
    technology: 10750.0
    upgrade: 1775.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 600.0
    upgrade: 1775.0
  }
  total_used_minerals {
    none: 0.0
    army: 22650.0
    economy: 10550.0
    technology: 10750.0
    upgrade: 1775.0
  }
  total_used_vespene {
    none: 0.0
    army: 18075.0
    economy: 0.0
    technology: 600.0
    upgrade: 1775.0
  }
  total_damage_dealt {
    life: 46313.59375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11807.4833984
    shields: 18786.421875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3623
vespene: 3127
food_cap: 200
food_used: 161
food_army: 65
food_workers: 96
idle_worker_count: 0
army_count: 29
warp_gate_count: 34

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 694
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 4
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 42394
score_details {
  idle_production_time: 15345.4375
  idle_worker_time: 9795.125
  total_value_units: 48550.0
  total_value_structures: 19250.0
  killed_value_units: 23650.0
  killed_value_structures: 17725.0
  collected_minerals: 56925.0
  collected_vespene: 20156.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 335.0
  spent_minerals: 52112.0
  spent_vespene: 18875.0
  food_used {
    none: 0.0
    army: 147.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 11700.0
    economy: 10850.0
    technology: 8175.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 7350.0
    economy: 600.0
    technology: 2600.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 20475.0
    economy: 3450.0
    technology: 937.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 11225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 8850.0
    economy: 8100.0
    technology: 10150.0
    upgrade: 2075.0
  }
  used_vespene {
    none: 0.0
    army: 3800.0
    economy: 0.0
    technology: 1200.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 0.0
    army: 32125.0
    economy: 11550.0
    technology: 11350.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 0.0
    army: 21925.0
    economy: 0.0
    technology: 1200.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 86837.328125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21385.0039062
    shields: 29679.8164062
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 4863
vespene: 1281
food_cap: 200
food_used: 192
food_army: 147
food_workers: 45
idle_worker_count: 7
army_count: 71
warp_gate_count: 41

game_loop:  36792
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 694
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 4
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 32
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 52
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 18
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 85
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 63
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 75
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 694
    player_relative: 1
    health: 30
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 85
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  42394
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
