----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3683
  player_apm: 160
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3770
  player_apm: 240
}
game_duration_loops: 20766
game_duration_seconds: 927.118286133
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11222
score_details {
  idle_production_time: 3902.25
  idle_worker_time: 5.0625
  total_value_units: 7750.0
  total_value_structures: 1350.0
  killed_value_units: 875.0
  killed_value_structures: 0.0
  collected_minerals: 9710.0
  collected_vespene: 1612.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 649.0
  spent_minerals: 8700.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2425.0
    economy: 4900.0
    technology: 1000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 3000.0
    economy: 5250.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1295.00537109
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1970.84863281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1060
vespene: 212
food_cap: 108
food_used: 98
food_army: 51
food_workers: 47
idle_worker_count: 0
army_count: 39
warp_gate_count: 0
larva_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21753
score_details {
  idle_production_time: 17702.375
  idle_worker_time: 1456.5625
  total_value_units: 22200.0
  total_value_structures: 2450.0
  killed_value_units: 15375.0
  killed_value_structures: 350.0
  collected_minerals: 23915.0
  collected_vespene: 7888.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 828.0
  spent_minerals: 21375.0
  spent_vespene: 7475.0
  food_used {
    none: 0.0
    army: 103.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10575.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7050.0
    economy: 250.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 5000.0
    economy: 7350.0
    technology: 1300.0
    upgrade: 925.0
  }
  used_vespene {
    none: 150.0
    army: 2450.0
    economy: 0.0
    technology: 500.0
    upgrade: 925.0
  }
  total_used_minerals {
    none: 300.0
    army: 13175.0
    economy: 8500.0
    technology: 1850.0
    upgrade: 925.0
  }
  total_used_vespene {
    none: 300.0
    army: 7600.0
    economy: 0.0
    technology: 750.0
    upgrade: 925.0
  }
  total_damage_dealt {
    life: 21244.9511719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15277.2314453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2590
vespene: 413
food_cap: 200
food_used: 160
food_army: 103
food_workers: 57
idle_worker_count: 6
army_count: 43
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 503
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 139
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 134
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 95
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 71
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 199
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 5
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 129
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21972
score_details {
  idle_production_time: 20273.625
  idle_worker_time: 1743.8125
  total_value_units: 22200.0
  total_value_structures: 2450.0
  killed_value_units: 19275.0
  killed_value_structures: 1000.0
  collected_minerals: 25070.0
  collected_vespene: 8352.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 851.0
  spent_minerals: 21375.0
  spent_vespene: 7475.0
  food_used {
    none: 0.0
    army: 79.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12500.0
    economy: 2200.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8050.0
    economy: 250.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4000.0
    economy: 7350.0
    technology: 1300.0
    upgrade: 925.0
  }
  used_vespene {
    none: 150.0
    army: 2050.0
    economy: 0.0
    technology: 500.0
    upgrade: 925.0
  }
  total_used_minerals {
    none: 300.0
    army: 13175.0
    economy: 8500.0
    technology: 1850.0
    upgrade: 925.0
  }
  total_used_vespene {
    none: 300.0
    army: 7600.0
    economy: 0.0
    technology: 750.0
    upgrade: 925.0
  }
  total_damage_dealt {
    life: 28826.453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16619.2714844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3745
vespene: 877
food_cap: 200
food_used: 136
food_army: 79
food_workers: 57
idle_worker_count: 6
army_count: 31
warp_gate_count: 0

game_loop:  20766
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 503
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 3
}
multi {
  units {
    unit_type: 503
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 147
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 7
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 73
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 3
    shields: 0
    energy: 0
  }
}

Score:  21972
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
