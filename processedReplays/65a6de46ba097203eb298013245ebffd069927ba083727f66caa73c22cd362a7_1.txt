----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3705
  player_apm: 174
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3726
  player_apm: 191
}
game_duration_loops: 9369
game_duration_seconds: 418.288116455
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8483
score_details {
  idle_production_time: 818.0625
  idle_worker_time: 360.1875
  total_value_units: 5250.0
  total_value_structures: 3325.0
  killed_value_units: 3350.0
  killed_value_structures: 0.0
  collected_minerals: 8220.0
  collected_vespene: 1652.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 649.0
  spent_minerals: 8093.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1225.0
    economy: 1850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1432.0
    economy: -7.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1250.0
    economy: 4450.0
    technology: 1150.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 525.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 4750.0
    technology: 1100.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 425.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4827.84033203
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1780.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 180.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 131
vespene: 277
food_cap: 86
food_used: 72
food_army: 25
food_workers: 46
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  9369
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 692
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 692
    player_relative: 1
    health: 165
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  8483
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
