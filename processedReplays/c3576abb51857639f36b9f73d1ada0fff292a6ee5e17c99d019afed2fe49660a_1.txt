----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3776
  player_apm: 162
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3769
  player_apm: 163
}
game_duration_loops: 26887
game_duration_seconds: 1200.39624023
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6684
score_details {
  idle_production_time: 1118.875
  idle_worker_time: 236.5625
  total_value_units: 5175.0
  total_value_structures: 3100.0
  killed_value_units: 1850.0
  killed_value_structures: 0.0
  collected_minerals: 7535.0
  collected_vespene: 1504.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 67.0
  spent_minerals: 7362.0
  spent_vespene: 1131.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1113.0
    economy: 1984.0
    technology: 12.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 186.0
    economy: 0.0
    technology: 6.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1100.0
    economy: 2575.0
    technology: 1100.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 325.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 4750.0
    technology: 1150.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 2918.1484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5460.63134766
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 946.288574219
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 172
vespene: 362
food_cap: 54
food_used: 39
food_army: 22
food_workers: 15
idle_worker_count: 7
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 2
}
cargo {
  unit {
    unit_type: 24
    player_relative: 1
    health: 395
    shields: 0
    energy: 0
    transport_slots_taken: 4
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  slots_available: 0
}

score{
 score_type: Melee
score: 16997
score_details {
  idle_production_time: 5167.375
  idle_worker_time: 455.6875
  total_value_units: 13350.0
  total_value_structures: 6600.0
  killed_value_units: 7450.0
  killed_value_structures: 0.0
  collected_minerals: 20180.0
  collected_vespene: 4932.0
  collection_rate_minerals: 587.0
  collection_rate_vespene: 380.0
  spent_minerals: 19524.0
  spent_vespene: 4406.0
  food_used {
    none: 0.0
    army: 66.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5800.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4006.0
    economy: 2534.0
    technology: -101.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1204.0
    economy: 0.0
    technology: -69.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3000.0
    economy: 5750.0
    technology: 2550.0
    upgrade: 1500.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 850.0
    upgrade: 1500.0
  }
  total_used_minerals {
    none: 0.0
    army: 6650.0
    economy: 8825.0
    technology: 2600.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 875.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 9904.1015625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10433.5605469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2448.73388672
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 600
vespene: 472
food_cap: 126
food_used: 126
food_army: 66
food_workers: 60
idle_worker_count: 1
army_count: 29
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 500
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 9
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 2
}
single {
  unit {
    unit_type: 134
    player_relative: 1
    health: 1500
    shields: 0
    energy: 75
  }
}

score{
 score_type: Melee
score: 9033
score_details {
  idle_production_time: 10162.125
  idle_worker_time: 2484.8125
  total_value_units: 19975.0
  total_value_structures: 7175.0
  killed_value_units: 19575.0
  killed_value_structures: 0.0
  collected_minerals: 25475.0
  collected_vespene: 5864.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 24899.0
  spent_vespene: 5581.0
  food_used {
    none: 0.0
    army: 3.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 14500.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11502.0
    economy: 7611.0
    technology: 329.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2999.0
    economy: 0.0
    technology: -69.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 100.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 150.0
    economy: 1975.0
    technology: 2200.0
    upgrade: 1500.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 875.0
    upgrade: 1500.0
  }
  total_used_minerals {
    none: 0.0
    army: 12025.0
    economy: 9325.0
    technology: 2650.0
    upgrade: 1500.0
  }
  total_used_vespene {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 900.0
    upgrade: 1500.0
  }
  total_damage_dealt {
    life: 22371.46875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 29032.0214844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3552.65673828
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 499
vespene: 209
food_cap: 95
food_used: 7
food_army: 3
food_workers: 4
idle_worker_count: 4
army_count: 1
warp_gate_count: 0

game_loop:  26887
ui_data{
 
Score:  9033
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
