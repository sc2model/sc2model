----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4007
  player_apm: 183
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 211
}
game_duration_loops: 13517
game_duration_seconds: 603.479614258
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10944
score_details {
  idle_production_time: 1271.9375
  idle_worker_time: 25.125
  total_value_units: 4975.0
  total_value_structures: 3900.0
  killed_value_units: 900.0
  killed_value_structures: 0.0
  collected_minerals: 8550.0
  collected_vespene: 2444.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 537.0
  spent_minerals: 8300.0
  spent_vespene: 2175.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4150.0
    technology: 2100.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 850.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4350.0
    technology: 1550.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 855.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 200.0
    shields: 598.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 300
vespene: 269
food_cap: 102
food_used: 75
food_army: 32
food_workers: 43
idle_worker_count: 0
army_count: 13
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 12140
score_details {
  idle_production_time: 3140.875
  idle_worker_time: 123.0
  total_value_units: 9550.0
  total_value_structures: 6800.0
  killed_value_units: 3000.0
  killed_value_structures: 0.0
  collected_minerals: 13205.0
  collected_vespene: 3760.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 492.0
  spent_minerals: 12325.0
  spent_vespene: 3625.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2450.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3325.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1300.0
    economy: 4300.0
    technology: 3150.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 850.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 4625.0
    economy: 5250.0
    technology: 3150.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 850.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 3336.20458984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3826.0
    shields: 4334.13085938
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 930
vespene: 135
food_cap: 118
food_used: 67
food_army: 25
food_workers: 42
idle_worker_count: 42
army_count: 10
warp_gate_count: 5

game_loop:  13517
ui_data{
 
Score:  12140
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
