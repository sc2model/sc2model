----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4439
  player_apm: 160
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4196
  player_apm: 135
}
game_duration_loops: 34817
game_duration_seconds: 1554.43884277
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10482
score_details {
  idle_production_time: 7416.75
  idle_worker_time: 63.4375
  total_value_units: 7600.0
  total_value_structures: 2025.0
  killed_value_units: 450.0
  killed_value_structures: 300.0
  collected_minerals: 9310.0
  collected_vespene: 1572.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 358.0
  spent_minerals: 9150.0
  spent_vespene: 1500.0
  food_used {
    none: 0.5
    army: 35.5
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 850.0
    economy: -175.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2200.0
    economy: 5300.0
    technology: 950.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 250.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 5950.0
    technology: 1225.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 1733.02148438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1327.81787109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 210
vespene: 72
food_cap: 120
food_used: 87
food_army: 35
food_workers: 51
idle_worker_count: 1
army_count: 44
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18496
score_details {
  idle_production_time: 42830.9375
  idle_worker_time: 293.8125
  total_value_units: 21550.0
  total_value_structures: 3775.0
  killed_value_units: 10025.0
  killed_value_structures: 300.0
  collected_minerals: 25650.0
  collected_vespene: 6308.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 604.0
  spent_minerals: 25256.0
  spent_vespene: 5956.0
  food_used {
    none: 0.0
    army: 60.5
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7050.0
    economy: 2400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8906.0
    economy: 2050.0
    technology: 725.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1381.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 800.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 3250.0
    economy: 6500.0
    technology: 1625.0
    upgrade: 2225.0
  }
  used_vespene {
    none: -25.0
    army: 1250.0
    economy: 0.0
    technology: 700.0
    upgrade: 2225.0
  }
  total_used_minerals {
    none: 0.0
    army: 12750.0
    economy: 10075.0
    technology: 2700.0
    upgrade: 2025.0
  }
  total_used_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 950.0
    upgrade: 2025.0
  }
  total_damage_dealt {
    life: 12689.3105469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19839.6347656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 444
vespene: 352
food_cap: 136
food_used: 125
food_army: 60
food_workers: 65
idle_worker_count: 0
army_count: 35
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 9
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 146
    shields: 0
    energy: 59
  }
}

score{
 score_type: Melee
score: 16139
score_details {
  idle_production_time: 69693.25
  idle_worker_time: 5580.125
  total_value_units: 29500.0
  total_value_structures: 5800.0
  killed_value_units: 18050.0
  killed_value_structures: 1825.0
  collected_minerals: 32765.0
  collected_vespene: 8336.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 0.0
  spent_minerals: 32756.0
  spent_vespene: 7781.0
  food_used {
    none: 0.0
    army: 39.5
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 12375.0
    economy: 5125.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 1825.0
    economy: 150.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14556.0
    economy: 5150.0
    technology: 1525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3356.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 800.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 2100.0
    economy: 5750.0
    technology: 1500.0
    upgrade: 2225.0
  }
  used_vespene {
    none: -25.0
    army: 1100.0
    economy: 0.0
    technology: 700.0
    upgrade: 2225.0
  }
  total_used_minerals {
    none: 0.0
    army: 17750.0
    economy: 13025.0
    technology: 3375.0
    upgrade: 2225.0
  }
  total_used_vespene {
    none: 0.0
    army: 4600.0
    economy: 0.0
    technology: 950.0
    upgrade: 2225.0
  }
  total_damage_dealt {
    life: 29895.8046875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 44040.1484375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 59
vespene: 555
food_cap: 166
food_used: 70
food_army: 39
food_workers: 29
idle_worker_count: 1
army_count: 14
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 101
  count: 2
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12188
score_details {
  idle_production_time: 77988.9375
  idle_worker_time: 7113.125
  total_value_units: 34000.0
  total_value_structures: 6525.0
  killed_value_units: 22775.0
  killed_value_structures: 1825.0
  collected_minerals: 35855.0
  collected_vespene: 8920.0
  collection_rate_minerals: 55.0
  collection_rate_vespene: 67.0
  spent_minerals: 35881.0
  spent_vespene: 8881.0
  food_used {
    none: 0.0
    army: 18.5
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 15975.0
    economy: 5125.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 2950.0
    economy: 150.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 17556.0
    economy: 7375.0
    technology: 2125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5056.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 800.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 850.0
    economy: 4025.0
    technology: 1775.0
    upgrade: 2225.0
  }
  used_vespene {
    none: -25.0
    army: 400.0
    economy: 0.0
    technology: 700.0
    upgrade: 2225.0
  }
  total_used_minerals {
    none: 0.0
    army: 20100.0
    economy: 14025.0
    technology: 4250.0
    upgrade: 2225.0
  }
  total_used_vespene {
    none: 0.0
    army: 5900.0
    economy: 0.0
    technology: 1050.0
    upgrade: 2225.0
  }
  total_damage_dealt {
    life: 33259.5390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 53374.2148438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 24
vespene: 39
food_cap: 146
food_used: 27
food_army: 18
food_workers: 9
idle_worker_count: 5
army_count: 14
warp_gate_count: 0

game_loop:  34817
ui_data{
 
Score:  12188
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
