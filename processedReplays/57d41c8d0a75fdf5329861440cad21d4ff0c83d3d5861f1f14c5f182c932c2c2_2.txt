----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3367
  player_apm: 342
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3323
  player_apm: 286
}
game_duration_loops: 20038
game_duration_seconds: 894.616027832
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10989
score_details {
  idle_production_time: 1675.125
  idle_worker_time: 104.8125
  total_value_units: 4650.0
  total_value_structures: 4000.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 8815.0
  collected_vespene: 2092.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 671.0
  spent_minerals: 8362.0
  spent_vespene: 1681.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 275.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 50.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1400.0
    economy: 4400.0
    technology: 2450.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 575.0
    economy: 0.0
    technology: 550.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4300.0
    technology: 2000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 454.5
    shields: 1147.125
    energy: 0.0
  }
  total_damage_taken {
    life: 540.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 67.9992675781
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 503
vespene: 411
food_cap: 70
food_used: 70
food_army: 28
food_workers: 41
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 119
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 73
  }
  units {
    unit_type: 18
    player_relative: 1
    health: 1326
    shields: 0
    energy: 0
    build_progress: 0.878749966621
  }
}

score{
 score_type: Melee
score: 17726
score_details {
  idle_production_time: 9290.6875
  idle_worker_time: 1470.25
  total_value_units: 16375.0
  total_value_structures: 7350.0
  killed_value_units: 10725.0
  killed_value_structures: 100.0
  collected_minerals: 22615.0
  collected_vespene: 6604.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 515.0
  spent_minerals: 21562.0
  spent_vespene: 6156.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7525.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7875.0
    economy: 1525.0
    technology: -13.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1900.0
    economy: 6550.0
    technology: 2800.0
    upgrade: 1625.0
  }
  used_vespene {
    none: 50.0
    army: 950.0
    economy: 0.0
    technology: 625.0
    upgrade: 1625.0
  }
  total_used_minerals {
    none: 50.0
    army: 9425.0
    economy: 8125.0
    technology: 2900.0
    upgrade: 1625.0
  }
  total_used_vespene {
    none: 50.0
    army: 3750.0
    economy: 0.0
    technology: 625.0
    upgrade: 1625.0
  }
  total_damage_dealt {
    life: 6221.5
    shields: 8126.375
    energy: 0.0
  }
  total_damage_taken {
    life: 13510.4160156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3206.84985352
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1103
vespene: 448
food_cap: 149
food_used: 82
food_army: 30
food_workers: 52
idle_worker_count: 1
army_count: 12
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 22
  count: 2
}
single {
  unit {
    unit_type: 51
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17658
score_details {
  idle_production_time: 9330.9375
  idle_worker_time: 1472.625
  total_value_units: 16425.0
  total_value_structures: 7350.0
  killed_value_units: 10725.0
  killed_value_structures: 100.0
  collected_minerals: 22660.0
  collected_vespene: 6616.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 492.0
  spent_minerals: 21562.0
  spent_vespene: 6156.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7525.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7975.0
    economy: 1525.0
    technology: -13.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2875.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1800.0
    economy: 6550.0
    technology: 2800.0
    upgrade: 1625.0
  }
  used_vespene {
    none: 50.0
    army: 925.0
    economy: 0.0
    technology: 625.0
    upgrade: 1625.0
  }
  total_used_minerals {
    none: 50.0
    army: 9475.0
    economy: 8125.0
    technology: 2900.0
    upgrade: 1625.0
  }
  total_used_vespene {
    none: 50.0
    army: 3750.0
    economy: 0.0
    technology: 625.0
    upgrade: 1625.0
  }
  total_damage_dealt {
    life: 6221.5
    shields: 8142.375
    energy: 0.0
  }
  total_damage_taken {
    life: 13668.5830078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3222.02416992
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1148
vespene: 460
food_cap: 149
food_used: 80
food_army: 28
food_workers: 52
idle_worker_count: 1
army_count: 12
warp_gate_count: 0

game_loop:  20038
ui_data{
 
Score:  17658
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
