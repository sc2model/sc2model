----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 76
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: -36400
  player_apm: 79
}
game_duration_loops: 12061
game_duration_seconds: 538.475097656
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12857
score_details {
  idle_production_time: 1231.25
  idle_worker_time: 261.5625
  total_value_units: 6100.0
  total_value_structures: 4500.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 10425.0
  collected_vespene: 2632.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 649.0
  spent_minerals: 9825.0
  spent_vespene: 1975.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2025.0
    economy: 5600.0
    technology: 2000.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2125.0
    economy: 5250.0
    technology: 1700.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 674.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 260.0
    shields: 334.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 650
vespene: 657
food_cap: 126
food_used: 91
food_army: 36
food_workers: 55
idle_worker_count: 1
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 62
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
    add_on {
      unit_type: 133
      build_progress: 0.0562499761581
    }
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
    add_on {
      unit_type: 133
      build_progress: 0.0375000238419
    }
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
    add_on {
      unit_type: 133
      build_progress: 0.0124999880791
    }
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 15564
score_details {
  idle_production_time: 1765.1875
  idle_worker_time: 576.625
  total_value_units: 9475.0
  total_value_structures: 5500.0
  killed_value_units: 3300.0
  killed_value_structures: 100.0
  collected_minerals: 13385.0
  collected_vespene: 3704.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 761.0
  spent_minerals: 13025.0
  spent_vespene: 2750.0
  food_used {
    none: 0.0
    army: 52.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2200.0
    economy: 450.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1850.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2975.0
    economy: 6700.0
    technology: 2000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 4325.0
    economy: 6350.0
    technology: 2000.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 3387.6796875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1339.0
    shields: 1648.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 410
vespene: 954
food_cap: 165
food_used: 115
food_army: 52
food_workers: 63
idle_worker_count: 3
army_count: 22
warp_gate_count: 5

game_loop:  12061
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 20
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 41
    shields: 20
    energy: 0
    build_progress: 0.33984375
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 115
    shields: 0
    energy: 114
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 94
    shields: 20
    energy: 184
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 33
    shields: 16
    energy: 0
    build_progress: 0.25390625
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 27
    shields: 13
    energy: 0
    build_progress: 0.1875
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 10
    energy: 100
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 81
    player_relative: 1
    health: 80
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 37
    shields: 19
    energy: 0
    build_progress: 0.3046875
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 35
    shields: 18
    energy: 0
    build_progress: 0.28125
  }
}

Score:  15564
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
