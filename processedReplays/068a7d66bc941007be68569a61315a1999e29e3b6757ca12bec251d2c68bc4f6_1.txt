----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 155
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4228
  player_apm: 200
}
game_duration_loops: 17075
game_duration_seconds: 762.329956055
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11792
score_details {
  idle_production_time: 1146.875
  idle_worker_time: 112.5625
  total_value_units: 6700.0
  total_value_structures: 4400.0
  killed_value_units: 1900.0
  killed_value_structures: 0.0
  collected_minerals: 8820.0
  collected_vespene: 2872.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 671.0
  spent_minerals: 8550.0
  spent_vespene: 2200.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 4350.0
    technology: 1950.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 4750.0
    technology: 1800.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 890.0
    shields: 670.0
    energy: 0.0
  }
  total_damage_taken {
    life: 565.0
    shields: 625.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 320
vespene: 672
food_cap: 101
food_used: 77
food_army: 34
food_workers: 43
idle_worker_count: 0
army_count: 17
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 78
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 2
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 19341
score_details {
  idle_production_time: 4370.625
  idle_worker_time: 699.3125
  total_value_units: 15825.0
  total_value_structures: 6700.0
  killed_value_units: 11050.0
  killed_value_structures: 750.0
  collected_minerals: 18455.0
  collected_vespene: 6736.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 985.0
  spent_minerals: 17025.0
  spent_vespene: 5700.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5550.0
    economy: 3350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3575.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3900.0
    economy: 6600.0
    technology: 2700.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 550.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 8475.0
    economy: 6750.0
    technology: 2700.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 5850.0
    economy: 0.0
    technology: 550.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 7272.125
    shields: 8131.875
    energy: 0.0
  }
  total_damage_taken {
    life: 2840.0
    shields: 5740.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1480
vespene: 1036
food_cap: 181
food_used: 117
food_army: 60
food_workers: 57
idle_worker_count: 3
army_count: 30
warp_gate_count: 6

game_loop:  17075
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 11
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 78
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 2
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  19341
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
