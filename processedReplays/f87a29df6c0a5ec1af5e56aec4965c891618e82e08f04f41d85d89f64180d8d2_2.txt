----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2796
  player_apm: 50
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2743
  player_apm: 92
}
game_duration_loops: 10653
game_duration_seconds: 475.613555908
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7506
score_details {
  idle_production_time: 1324.75
  idle_worker_time: 262.1875
  total_value_units: 4050.0
  total_value_structures: 2850.0
  killed_value_units: 2900.0
  killed_value_structures: 1600.0
  collected_minerals: 6030.0
  collected_vespene: 1376.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 291.0
  spent_minerals: 5650.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1550.0
    economy: 1650.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 3100.0
    technology: 1050.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 3150.0
    technology: 1050.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 4261.33984375
    shields: 4896.10839844
    energy: 0.0
  }
  total_damage_taken {
    life: 889.0
    shields: 884.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 430
vespene: 726
food_cap: 78
food_used: 56
food_army: 28
food_workers: 27
idle_worker_count: 0
army_count: 14
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 8321
score_details {
  idle_production_time: 1401.0
  idle_worker_time: 262.1875
  total_value_units: 4650.0
  total_value_structures: 2850.0
  killed_value_units: 3400.0
  killed_value_structures: 1925.0
  collected_minerals: 6655.0
  collected_vespene: 1516.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 313.0
  spent_minerals: 6300.0
  spent_vespene: 800.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1950.0
    economy: 1825.0
    technology: 550.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1900.0
    economy: 3250.0
    technology: 1050.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 100.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 3350.0
    technology: 1050.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 6666.14941406
    shields: 7898.25878906
    energy: 0.0
  }
  total_damage_taken {
    life: 929.0
    shields: 982.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 405
vespene: 716
food_cap: 78
food_used: 67
food_army: 36
food_workers: 31
idle_worker_count: 0
army_count: 18
warp_gate_count: 5

game_loop:  10653
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  8321
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
