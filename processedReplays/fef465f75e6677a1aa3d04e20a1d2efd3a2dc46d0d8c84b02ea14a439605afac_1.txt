----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3570
  player_apm: 201
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3561
  player_apm: 146
}
game_duration_loops: 9851
game_duration_seconds: 439.8074646
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3775
score_details {
  idle_production_time: 10573.375
  idle_worker_time: 166.1875
  total_value_units: 5450.0
  total_value_structures: 1275.0
  killed_value_units: 1150.0
  killed_value_structures: 0.0
  collected_minerals: 5680.0
  collected_vespene: 920.0
  collection_rate_minerals: 83.0
  collection_rate_vespene: 134.0
  spent_minerals: 5450.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 3.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2250.0
    economy: 1000.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 775.0
    economy: 1525.0
    technology: 725.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3200.0
    economy: 2875.0
    technology: 850.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3002.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5589.44726562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 280
vespene: 45
food_cap: 60
food_used: 18
food_army: 15
food_workers: 3
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  9851
ui_data{
 
Score:  3775
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
