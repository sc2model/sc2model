----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3281
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3229
  player_apm: 128
}
game_duration_loops: 18933
game_duration_seconds: 845.282226562
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11834
score_details {
  idle_production_time: 982.3125
  idle_worker_time: 36.0
  total_value_units: 6950.0
  total_value_structures: 3700.0
  killed_value_units: 950.0
  killed_value_structures: 0.0
  collected_minerals: 9785.0
  collected_vespene: 2724.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 671.0
  spent_minerals: 9075.0
  spent_vespene: 2000.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1025.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5550.0
    technology: 1100.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 5250.0
    technology: 1100.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 741.75
    shields: 916.5
    energy: 0.0
  }
  total_damage_taken {
    life: 740.0
    shields: 1622.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 760
vespene: 724
food_cap: 110
food_used: 91
food_army: 33
food_workers: 56
idle_worker_count: 0
army_count: 14
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 76
    shields: 76
    energy: 0
    build_progress: 0.937485694885
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 74
    shields: 74
    energy: 0
    build_progress: 0.912486076355
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 68
    shields: 68
    energy: 0
    build_progress: 0.837487220764
  }
}

score{
 score_type: Melee
score: 24820
score_details {
  idle_production_time: 4990.5625
  idle_worker_time: 837.3125
  total_value_units: 18200.0
  total_value_structures: 9300.0
  killed_value_units: 11200.0
  killed_value_structures: 1150.0
  collected_minerals: 24135.0
  collected_vespene: 8260.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 761.0
  spent_minerals: 22825.0
  spent_vespene: 6325.0
  food_used {
    none: 0.0
    army: 86.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6775.0
    economy: 1500.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5650.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5275.0
    economy: 8000.0
    technology: 3800.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 700.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 10600.0
    economy: 8400.0
    technology: 3800.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 5350.0
    economy: 0.0
    technology: 700.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 7106.0
    shields: 10008.125
    energy: 0.0
  }
  total_damage_taken {
    life: 5094.125
    shields: 7041.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1360
vespene: 1935
food_cap: 200
food_used: 152
food_army: 86
food_workers: 66
idle_worker_count: 3
army_count: 35
warp_gate_count: 10

game_loop:  18933
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 25
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  24820
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
