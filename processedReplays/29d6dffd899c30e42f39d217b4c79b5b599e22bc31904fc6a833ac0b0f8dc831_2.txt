----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5177
  player_apm: 312
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4407
  player_apm: 149
}
game_duration_loops: 18653
game_duration_seconds: 832.781311035
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8452
score_details {
  idle_production_time: 6825.75
  idle_worker_time: 12.4375
  total_value_units: 6050.0
  total_value_structures: 1125.0
  killed_value_units: 1200.0
  killed_value_structures: 0.0
  collected_minerals: 8700.0
  collected_vespene: 952.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 179.0
  spent_minerals: 6800.0
  spent_vespene: 200.0
  food_used {
    none: 0.0
    army: 13.5
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2025.0
    economy: -75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 875.0
    economy: 3775.0
    technology: 800.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 4075.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1806.875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5550.17871094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1950
vespene: 752
food_cap: 60
food_used: 51
food_army: 13
food_workers: 38
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 105
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 3
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 170
    shields: 0
    energy: 39
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 9
    shields: 0
    energy: 0
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 165
    shields: 0
    energy: 38
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 125
    shields: 0
    energy: 61
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 24
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4651
score_details {
  idle_production_time: 25250.375
  idle_worker_time: 274.75
  total_value_units: 14750.0
  total_value_structures: 2100.0
  killed_value_units: 9250.0
  killed_value_structures: 2200.0
  collected_minerals: 15175.0
  collected_vespene: 3576.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 15175.0
  spent_vespene: 2650.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 3.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5525.0
    economy: 3950.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7600.0
    economy: 3250.0
    technology: 1025.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 1850.0
    technology: 75.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 150.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 8100.0
    economy: 6850.0
    technology: 1250.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 350.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 18595.1328125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 29204.6738281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 50
vespene: 926
food_cap: 88
food_used: 11
food_army: 8
food_workers: 3
idle_worker_count: 2
army_count: 5
warp_gate_count: 0

game_loop:  18653
ui_data{
 multi {
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 171
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 65
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  4651
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
