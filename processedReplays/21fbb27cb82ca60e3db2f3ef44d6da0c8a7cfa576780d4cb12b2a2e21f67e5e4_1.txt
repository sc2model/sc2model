----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3832
  player_apm: 155
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3382
  player_apm: 176
}
game_duration_loops: 15622
game_duration_seconds: 697.459411621
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9626
score_details {
  idle_production_time: 963.6875
  idle_worker_time: 567.375
  total_value_units: 3500.0
  total_value_structures: 4500.0
  killed_value_units: 1725.0
  killed_value_structures: 0.0
  collected_minerals: 8320.0
  collected_vespene: 1864.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 335.0
  spent_minerals: 7800.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1475.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: 250.0
    technology: 552.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 202.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 5375.0
    technology: 1400.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 425.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1150.0
    economy: 5750.0
    technology: 1475.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 475.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2455.49609375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5618.04296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 136.228515625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 539
vespene: 1187
food_cap: 125
food_used: 61
food_army: 13
food_workers: 45
idle_worker_count: 3
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 735
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      build_progress: 0.697499990463
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 760
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 19488
score_details {
  idle_production_time: 3859.0
  idle_worker_time: 1286.6875
  total_value_units: 9325.0
  total_value_structures: 7550.0
  killed_value_units: 5100.0
  killed_value_structures: 75.0
  collected_minerals: 17360.0
  collected_vespene: 4464.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 694.0
  spent_minerals: 16150.0
  spent_vespene: 3100.0
  food_used {
    none: 0.0
    army: 83.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3100.0
    economy: 1575.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1975.0
    economy: 450.0
    technology: 552.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 202.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3975.0
    economy: 6475.0
    technology: 2925.0
    upgrade: 675.0
  }
  used_vespene {
    none: 50.0
    army: 1175.0
    economy: 0.0
    technology: 900.0
    upgrade: 675.0
  }
  total_used_minerals {
    none: 50.0
    army: 5375.0
    economy: 6975.0
    technology: 3175.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 1150.0
    economy: 0.0
    technology: 1000.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 7162.86230469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8159.34472656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 977.516113281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1226
vespene: 1362
food_cap: 149
food_used: 138
food_army: 83
food_workers: 55
idle_worker_count: 3
army_count: 45
warp_gate_count: 0

game_loop:  15622
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 735
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 760
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  19488
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
