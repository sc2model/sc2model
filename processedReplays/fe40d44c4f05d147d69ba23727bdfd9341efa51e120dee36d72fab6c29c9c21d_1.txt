----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3192
  player_apm: 160
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3386
  player_apm: 74
}
game_duration_loops: 10011
game_duration_seconds: 446.950836182
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10520
score_details {
  idle_production_time: 1607.75
  idle_worker_time: 41.0625
  total_value_units: 5325.0
  total_value_structures: 3475.0
  killed_value_units: 1175.0
  killed_value_structures: 0.0
  collected_minerals: 8270.0
  collected_vespene: 2100.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 627.0
  spent_minerals: 7850.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 4100.0
    technology: 1400.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 4200.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1605.15087891
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 629.681640625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 289.9296875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 470
vespene: 375
food_cap: 86
food_used: 86
food_army: 46
food_workers: 40
idle_worker_count: 0
army_count: 29
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 33
  count: 20
}
groups {
  control_group_index: 4
  leader_unit_type: 48
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10533
score_details {
  idle_production_time: 1613.25
  idle_worker_time: 41.0625
  total_value_units: 5325.0
  total_value_structures: 3475.0
  killed_value_units: 1175.0
  killed_value_structures: 0.0
  collected_minerals: 8275.0
  collected_vespene: 2108.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 671.0
  spent_minerals: 7850.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 4100.0
    technology: 1400.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 4200.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1605.15087891
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 629.681640625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 289.9296875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 475
vespene: 383
food_cap: 86
food_used: 86
food_army: 46
food_workers: 40
idle_worker_count: 0
army_count: 30
warp_gate_count: 0

game_loop:  10011
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 32
  count: 21
}
groups {
  control_group_index: 4
  leader_unit_type: 48
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

Score:  10533
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
