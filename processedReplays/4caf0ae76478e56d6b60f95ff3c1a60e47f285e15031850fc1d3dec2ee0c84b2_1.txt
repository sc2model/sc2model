----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5053
  player_apm: 303
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4841
  player_apm: 355
}
game_duration_loops: 21293
game_duration_seconds: 950.64666748
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13484
score_details {
  idle_production_time: 1148.1875
  idle_worker_time: 231.125
  total_value_units: 6200.0
  total_value_structures: 5250.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 11290.0
  collected_vespene: 2444.0
  collection_rate_minerals: 2743.0
  collection_rate_vespene: 627.0
  spent_minerals: 10225.0
  spent_vespene: 2175.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1275.0
    economy: 6550.0
    technology: 2250.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 1725.0
    economy: 6000.0
    technology: 2100.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 740.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1100.0
    shields: 1401.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1115
vespene: 269
food_cap: 149
food_used: 90
food_army: 28
food_workers: 62
idle_worker_count: 1
army_count: 14
warp_gate_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 77
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 8
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 129
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 132
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 132
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 70
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 70
  }
}

score{
 score_type: Melee
score: 13327
score_details {
  idle_production_time: 5895.8125
  idle_worker_time: 1025.8125
  total_value_units: 25500.0
  total_value_structures: 8525.0
  killed_value_units: 16825.0
  killed_value_structures: 375.0
  collected_minerals: 29300.0
  collected_vespene: 7252.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 313.0
  spent_minerals: 29275.0
  spent_vespene: 6650.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12525.0
    economy: 2275.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16325.0
    economy: 3250.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4200.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1275.0
    economy: 5075.0
    technology: 2900.0
    upgrade: 1200.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 500.0
    upgrade: 1200.0
  }
  total_used_minerals {
    none: 0.0
    army: 18100.0
    economy: 7825.0
    technology: 3350.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 5900.0
    economy: 0.0
    technology: 650.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 17113.5703125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20002.203125
    shields: 19588.2617188
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 75
vespene: 602
food_cap: 149
food_used: 55
food_army: 22
food_workers: 33
idle_worker_count: 4
army_count: 12
warp_gate_count: 10

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 10
}
single {
  unit {
    unit_type: 488
    player_relative: 1
    health: 20
    shields: 0
    energy: 25
  }
}

score{
 score_type: Melee
score: 12191
score_details {
  idle_production_time: 6644.9375
  idle_worker_time: 1349.0625
  total_value_units: 27000.0
  total_value_structures: 8925.0
  killed_value_units: 18050.0
  killed_value_structures: 375.0
  collected_minerals: 30280.0
  collected_vespene: 7636.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 671.0
  spent_minerals: 30175.0
  spent_vespene: 7250.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13575.0
    economy: 2425.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 18075.0
    economy: 3250.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5050.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 575.0
    economy: 5075.0
    technology: 2900.0
    upgrade: 1200.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 500.0
    upgrade: 1200.0
  }
  total_used_minerals {
    none: 0.0
    army: 19350.0
    economy: 8225.0
    technology: 3350.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 7050.0
    economy: 0.0
    technology: 650.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 18402.5507812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20970.953125
    shields: 21142.5117188
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 155
vespene: 386
food_cap: 164
food_used: 43
food_army: 10
food_workers: 33
idle_worker_count: 4
army_count: 6
warp_gate_count: 10

game_loop:  21293
ui_data{
 multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  12191
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
