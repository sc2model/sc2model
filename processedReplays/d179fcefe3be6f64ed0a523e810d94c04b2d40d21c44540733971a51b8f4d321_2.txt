----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4924
  player_apm: 223
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5068
  player_apm: 212
}
game_duration_loops: 31883
game_duration_seconds: 1423.44750977
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10920
score_details {
  idle_production_time: 1175.75
  idle_worker_time: 661.5
  total_value_units: 5825.0
  total_value_structures: 3725.0
  killed_value_units: 850.0
  killed_value_structures: 0.0
  collected_minerals: 8610.0
  collected_vespene: 1792.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 515.0
  spent_minerals: 8275.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2450.0
    economy: 4750.0
    technology: 1175.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 825.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2400.0
    economy: 5550.0
    technology: 1175.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 875.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 656.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1216.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 280.413085938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 360
vespene: 210
food_cap: 101
food_used: 92
food_army: 48
food_workers: 44
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22301
score_details {
  idle_production_time: 8480.3125
  idle_worker_time: 1258.5
  total_value_units: 15175.0
  total_value_structures: 9800.0
  killed_value_units: 6275.0
  killed_value_structures: 400.0
  collected_minerals: 24455.0
  collected_vespene: 7184.0
  collection_rate_minerals: 2323.0
  collection_rate_vespene: 806.0
  spent_minerals: 23856.0
  spent_vespene: 6725.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4875.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7025.0
    economy: 800.0
    technology: -94.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1942.0
    economy: 0.0
    technology: -75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2750.0
    economy: 8150.0
    technology: 4250.0
    upgrade: 1525.0
  }
  used_vespene {
    none: 100.0
    army: 1050.0
    economy: 300.0
    technology: 1600.0
    upgrade: 1525.0
  }
  total_used_minerals {
    none: 100.0
    army: 8750.0
    economy: 9300.0
    technology: 4100.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 100.0
    army: 2675.0
    economy: 600.0
    technology: 1450.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 9542.14453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9504.14550781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2458.23071289
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 559
vespene: 392
food_cap: 200
food_used: 121
food_army: 54
food_workers: 67
idle_worker_count: 3
army_count: 26
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 31728
score_details {
  idle_production_time: 19149.25
  idle_worker_time: 4219.1875
  total_value_units: 34250.0
  total_value_structures: 11625.0
  killed_value_units: 22650.0
  killed_value_structures: 400.0
  collected_minerals: 41635.0
  collected_vespene: 13108.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 940.0
  spent_minerals: 39481.0
  spent_vespene: 11700.0
  food_used {
    none: 0.0
    army: 111.0
    economy: 71.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 16775.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 17363.0
    economy: 1400.0
    technology: 156.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4300.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 6200.0
    economy: 8500.0
    technology: 4825.0
    upgrade: 1925.0
  }
  used_vespene {
    none: 100.0
    army: 3150.0
    economy: 150.0
    technology: 1800.0
    upgrade: 1925.0
  }
  total_used_minerals {
    none: 100.0
    army: 23150.0
    economy: 9850.0
    technology: 5075.0
    upgrade: 1925.0
  }
  total_used_vespene {
    none: 100.0
    army: 7100.0
    economy: 600.0
    technology: 2000.0
    upgrade: 1925.0
  }
  total_damage_dealt {
    life: 24759.7988281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24993.9589844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 7649.36621094
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1838
vespene: 1215
food_cap: 200
food_used: 182
food_army: 111
food_workers: 71
idle_worker_count: 0
army_count: 66
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 54
}
groups {
  control_group_index: 2
  leader_unit_type: 32
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 10
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 28745
score_details {
  idle_production_time: 20659.3125
  idle_worker_time: 4421.8125
  total_value_units: 37575.0
  total_value_structures: 12300.0
  killed_value_units: 27300.0
  killed_value_structures: 400.0
  collected_minerals: 45235.0
  collected_vespene: 14500.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 940.0
  spent_minerals: 42581.0
  spent_vespene: 12550.0
  food_used {
    none: 0.0
    army: 63.0
    economy: 71.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 20375.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 23101.0
    economy: 1400.0
    technology: 281.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6382.0
    economy: 150.0
    technology: 225.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 3450.0
    economy: 8500.0
    technology: 4850.0
    upgrade: 1925.0
  }
  used_vespene {
    none: 100.0
    army: 1825.0
    economy: 150.0
    technology: 1825.0
    upgrade: 1925.0
  }
  total_used_minerals {
    none: 100.0
    army: 25750.0
    economy: 10250.0
    technology: 5225.0
    upgrade: 1925.0
  }
  total_used_vespene {
    none: 100.0
    army: 7825.0
    economy: 600.0
    technology: 2125.0
    upgrade: 1925.0
  }
  total_damage_dealt {
    life: 28986.9160156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 31519.2734375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 8908.18457031
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2338
vespene: 1757
food_cap: 200
food_used: 134
food_army: 63
food_workers: 71
idle_worker_count: 3
army_count: 29
warp_gate_count: 0

game_loop:  31883
ui_data{
 
Score:  28745
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
