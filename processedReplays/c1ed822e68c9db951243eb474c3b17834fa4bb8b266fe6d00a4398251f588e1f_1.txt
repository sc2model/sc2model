----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4259
  player_apm: 110
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4165
  player_apm: 209
}
game_duration_loops: 29420
game_duration_seconds: 1313.48449707
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12003
score_details {
  idle_production_time: 1239.3125
  idle_worker_time: 22.875
  total_value_units: 6325.0
  total_value_structures: 4100.0
  killed_value_units: 750.0
  killed_value_structures: 75.0
  collected_minerals: 9465.0
  collected_vespene: 2588.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 649.0
  spent_minerals: 8550.0
  spent_vespene: 2375.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 275.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2650.0
    economy: 4750.0
    technology: 1350.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 4650.0
    technology: 1350.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1574.86132812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 232.3984375
    shields: 617.6015625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 965
vespene: 213
food_cap: 101
food_used: 101
food_army: 53
food_workers: 48
idle_worker_count: 0
army_count: 23
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 488
  count: 1
}
single {
  unit {
    unit_type: 495
    player_relative: 1
    health: 49
    shields: 30
    energy: 8
  }
}

score{
 score_type: Melee
score: 28364
score_details {
  idle_production_time: 4197.0625
  idle_worker_time: 422.1875
  total_value_units: 19255.0
  total_value_structures: 8500.0
  killed_value_units: 9500.0
  killed_value_structures: 2125.0
  collected_minerals: 24040.0
  collected_vespene: 9320.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 783.0
  spent_minerals: 21314.0
  spent_vespene: 9012.0
  food_used {
    none: 0.0
    army: 115.0
    economy: 69.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5800.0
    economy: 2525.0
    technology: 1350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5125.0
    economy: 50.0
    technology: -226.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5630.0
    economy: 7550.0
    technology: 3450.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 6000.0
    economy: 0.0
    technology: 950.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 10030.0
    economy: 7600.0
    technology: 3450.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 7525.0
    economy: 0.0
    technology: 950.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 18867.8125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5136.3984375
    shields: 5212.3515625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2776
vespene: 308
food_cap: 200
food_used: 184
food_army: 115
food_workers: 69
idle_worker_count: 0
army_count: 33
warp_gate_count: 10

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 21
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 28
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 94
    shields: 60
    energy: 43
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 34
    shields: 26
    energy: 67
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 87
    shields: 60
    energy: 18
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 26
    shields: 60
    energy: 17
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 62
    shields: 49
    energy: 18
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 48
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 85
    shields: 60
    energy: 48
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 91
    shields: 16
    energy: 43
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 95
    shields: 60
    energy: 47
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 26
    shields: 33
    energy: 19
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 36
    energy: 20
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 90
    shields: 26
    energy: 21
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 18
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 30
    shields: 58
    energy: 18
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 91
    shields: 5
    energy: 47
  }
}

score{
 score_type: Melee
score: 25625
score_details {
  idle_production_time: 12224.0625
  idle_worker_time: 2374.4375
  total_value_units: 34295.0
  total_value_structures: 11650.0
  killed_value_units: 30950.0
  killed_value_structures: 5325.0
  collected_minerals: 35045.0
  collected_vespene: 14196.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 425.0
  spent_minerals: 34689.0
  spent_vespene: 14087.0
  food_used {
    none: 0.0
    army: 121.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 19225.0
    economy: 5625.0
    technology: 2900.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7725.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14435.0
    economy: 2050.0
    technology: 674.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7900.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 320.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7235.0
    economy: 7100.0
    technology: 4100.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 3925.0
    economy: 0.0
    technology: 1050.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 22345.0
    economy: 9150.0
    technology: 5000.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 15000.0
    economy: 0.0
    technology: 1050.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 49860.5585938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16028.3984375
    shields: 20661.9765625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 406
vespene: 109
food_cap: 200
food_used: 162
food_army: 121
food_workers: 41
idle_worker_count: 6
army_count: 33
warp_gate_count: 10

game_loop:  29420
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 79
  count: 18
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 3
}
multi {
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 65
    shields: 50
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 7
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 46
    shields: 50
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 120
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 320
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 46
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 120
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 67
    shields: 81
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
}

Score:  25625
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
