----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4221
  player_apm: 204
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4046
  player_apm: 153
}
game_duration_loops: 12896
game_duration_seconds: 575.754455566
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1882
score_details {
  idle_production_time: 889.375
  idle_worker_time: 544.25
  total_value_units: 3325.0
  total_value_structures: 2575.0
  killed_value_units: 1350.0
  killed_value_structures: 650.0
  collected_minerals: 4875.0
  collected_vespene: 828.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 4700.0
  spent_vespene: 750.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 1100.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 1960.0
    technology: 1204.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 1250.0
    technology: 150.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1150.0
    economy: 3500.0
    technology: 1100.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4087.99804688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13562.3095703
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 224.982421875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 204
vespene: 78
food_cap: 38
food_used: 3
food_army: 2
food_workers: 1
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 134
  count: 2
}
multi {
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 1245
score_details {
  idle_production_time: 890.9375
  idle_worker_time: 551.6875
  total_value_units: 3325.0
  total_value_structures: 2575.0
  killed_value_units: 1700.0
  killed_value_structures: 650.0
  collected_minerals: 4875.0
  collected_vespene: 828.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 4587.0
  spent_vespene: 750.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 1450.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 2567.0
    technology: 1091.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1150.0
    economy: 3500.0
    technology: 1100.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 225.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4619.99804688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14682.8544922
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 224.982421875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 317
vespene: 78
food_cap: 23
food_used: 2
food_army: 2
food_workers: 0
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  12896
ui_data{
 
Score:  1245
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
