----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3743
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3811
  player_apm: 103
}
game_duration_loops: 15561
game_duration_seconds: 694.735961914
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11469
score_details {
  idle_production_time: 1173.5625
  idle_worker_time: 415.625
  total_value_units: 6875.0
  total_value_structures: 4000.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 8740.0
  collected_vespene: 2004.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 313.0
  spent_minerals: 8200.0
  spent_vespene: 1875.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2975.0
    economy: 4200.0
    technology: 1700.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 3100.0
    economy: 4250.0
    technology: 1700.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 184.0
    shields: 336.0
    energy: 0.0
  }
  total_damage_taken {
    life: 145.0
    shields: 175.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 590
vespene: 129
food_cap: 102
food_used: 95
food_army: 51
food_workers: 42
idle_worker_count: 1
army_count: 24
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 12
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 7
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 10223
score_details {
  idle_production_time: 3108.75
  idle_worker_time: 1156.125
  total_value_units: 14400.0
  total_value_structures: 5200.0
  killed_value_units: 6475.0
  killed_value_structures: 0.0
  collected_minerals: 16300.0
  collected_vespene: 4748.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 806.0
  spent_minerals: 14875.0
  spent_vespene: 4325.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7825.0
    economy: 650.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 175.0
    economy: 5150.0
    technology: 1700.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 500.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 8100.0
    economy: 5800.0
    technology: 1850.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 3800.0
    economy: 0.0
    technology: 500.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 4685.875
    shields: 5079.375
    energy: 0.0
  }
  total_damage_taken {
    life: 6803.0
    shields: 7325.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1475
vespene: 423
food_cap: 109
food_used: 58
food_army: 4
food_workers: 54
idle_worker_count: 2
army_count: 3
warp_gate_count: 6

game_loop:  15561
ui_data{
 
Score:  10223
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
