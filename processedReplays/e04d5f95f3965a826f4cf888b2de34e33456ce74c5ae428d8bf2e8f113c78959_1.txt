----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3906
  player_apm: 172
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3908
  player_apm: 125
}
game_duration_loops: 28316
game_duration_seconds: 1264.1953125
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11399
score_details {
  idle_production_time: 1541.25
  idle_worker_time: 574.3125
  total_value_units: 4925.0
  total_value_structures: 3825.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 8465.0
  collected_vespene: 2596.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 627.0
  spent_minerals: 8262.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 150.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: -75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1500.0
    economy: 5400.0
    technology: 1625.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 500.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 5700.0
    technology: 1325.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1666.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 647.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 253
vespene: 921
food_cap: 109
food_used: 83
food_army: 31
food_workers: 49
idle_worker_count: 2
army_count: 10
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 49
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 2
}
single {
  unit {
    unit_type: 29
    player_relative: 1
    health: 723
    shields: 0
    energy: 0
    build_progress: 0.960576891899
  }
}

score{
 score_type: Melee
score: 24806
score_details {
  idle_production_time: 6847.75
  idle_worker_time: 3651.0
  total_value_units: 19400.0
  total_value_structures: 8950.0
  killed_value_units: 12775.0
  killed_value_structures: 0.0
  collected_minerals: 26365.0
  collected_vespene: 10512.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 940.0
  spent_minerals: 24887.0
  spent_vespene: 7650.0
  food_used {
    none: 0.0
    army: 69.0
    economy: 79.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10900.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7850.0
    economy: 600.0
    technology: 187.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3400.0
    economy: 0.0
    technology: -75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3850.0
    economy: 8700.0
    technology: 3300.0
    upgrade: 700.0
  }
  used_vespene {
    none: 100.0
    army: 1850.0
    economy: 150.0
    technology: 1150.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 100.0
    army: 10400.0
    economy: 10050.0
    technology: 3600.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 100.0
    army: 4450.0
    economy: 300.0
    technology: 1150.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 13610.9980469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11050.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1785.5
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1335
vespene: 2821
food_cap: 200
food_used: 148
food_army: 69
food_workers: 79
idle_worker_count: 22
army_count: 21
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 35
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 2
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 29933
score_details {
  idle_production_time: 10614.125
  idle_worker_time: 10760.0
  total_value_units: 32425.0
  total_value_structures: 10950.0
  killed_value_units: 33525.0
  killed_value_structures: 4350.0
  collected_minerals: 35085.0
  collected_vespene: 14312.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 0.0
  spent_minerals: 34437.0
  spent_vespene: 13275.0
  food_used {
    none: 0.0
    army: 135.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25950.0
    economy: 3875.0
    technology: 1900.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5025.0
    economy: 0.0
    technology: 1125.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11234.0
    economy: 3562.0
    technology: 612.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4275.0
    economy: 201.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 7950.0
    economy: 7200.0
    technology: 3425.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 100.0
    army: 5700.0
    economy: 0.0
    technology: 1450.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 100.0
    army: 18450.0
    economy: 11400.0
    technology: 4150.0
    upgrade: 1350.0
  }
  total_used_vespene {
    none: 100.0
    army: 9425.0
    economy: 300.0
    technology: 1550.0
    upgrade: 1350.0
  }
  total_damage_dealt {
    life: 46285.484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22887.5019531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3613.15429688
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 233
vespene: 925
food_cap: 200
food_used: 185
food_army: 135
food_workers: 50
idle_worker_count: 20
army_count: 42
warp_gate_count: 0

game_loop:  28316
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 32
  count: 34
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 4
}
single {
  unit {
    unit_type: 134
    player_relative: 1
    health: 1500
    shields: 0
    energy: 53
  }
}

Score:  29933
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
