----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4424
  player_apm: 113
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4321
  player_apm: 190
}
game_duration_loops: 13121
game_duration_seconds: 585.799804688
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8235
score_details {
  idle_production_time: 1799.75
  idle_worker_time: 199.3125
  total_value_units: 4550.0
  total_value_structures: 4200.0
  killed_value_units: 1750.0
  killed_value_structures: 0.0
  collected_minerals: 9015.0
  collected_vespene: 756.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 179.0
  spent_minerals: 8236.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1550.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 775.0
    technology: 311.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 3200.0
    technology: 1700.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 4050.0
    technology: 2150.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2470.78564453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2744.59521484
    shields: 5460.97998047
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 829
vespene: 106
food_cap: 78
food_used: 65
food_army: 32
food_workers: 33
idle_worker_count: 0
army_count: 16
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 73
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 37
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
}

score{
 score_type: Melee
score: 9589
score_details {
  idle_production_time: 2738.125
  idle_worker_time: 199.3125
  total_value_units: 7050.0
  total_value_structures: 4500.0
  killed_value_units: 5125.0
  killed_value_structures: 850.0
  collected_minerals: 12885.0
  collected_vespene: 1140.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 201.0
  spent_minerals: 11386.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4500.0
    economy: 1200.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3150.0
    economy: 525.0
    technology: 311.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1600.0
    economy: 3650.0
    technology: 1700.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 4550.0
    economy: 4550.0
    technology: 2150.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 10057.3710938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5324.62353516
    shields: 8169.49755859
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1549
vespene: 190
food_cap: 102
food_used: 66
food_army: 30
food_workers: 36
idle_worker_count: 0
army_count: 15
warp_gate_count: 7

game_loop:  13121
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  9589
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
