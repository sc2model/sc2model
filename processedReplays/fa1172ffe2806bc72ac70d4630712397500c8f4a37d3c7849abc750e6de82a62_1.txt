----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 69
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2817
  player_apm: 60
}
game_duration_loops: 8964
game_duration_seconds: 400.206512451
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5480
score_details {
  idle_production_time: 1341.4375
  idle_worker_time: 1511.5625
  total_value_units: 1950.0
  total_value_structures: 3550.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 4915.0
  collected_vespene: 664.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 156.0
  spent_minerals: 4649.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 75.0
    technology: 74.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 2850.0
    technology: 1650.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 600.0
    economy: 3000.0
    technology: 1800.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 220.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1504.8840332
    shields: 1647.24975586
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 316
vespene: 564
food_cap: 78
food_used: 26
food_army: 0
food_workers: 26
idle_worker_count: 26
army_count: 0
warp_gate_count: 0

game_loop:  8964
ui_data{
 
Score:  5480
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
