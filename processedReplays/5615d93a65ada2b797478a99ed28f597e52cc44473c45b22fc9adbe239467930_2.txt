----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3587
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3647
  player_apm: 111
}
game_duration_loops: 17490
game_duration_seconds: 780.858032227
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11236
score_details {
  idle_production_time: 13062.625
  idle_worker_time: 372.0625
  total_value_units: 5600.0
  total_value_structures: 1650.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 9395.0
  collected_vespene: 1128.0
  collection_rate_minerals: 2351.0
  collection_rate_vespene: 470.0
  spent_minerals: 7662.0
  spent_vespene: 525.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: -38.0
    technology: -375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1525.0
    economy: 5400.0
    technology: 1050.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 100.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 5450.0
    technology: 1200.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1310.77001953
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 808.81640625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1783
vespene: 603
food_cap: 82
food_used: 82
food_army: 29
food_workers: 53
idle_worker_count: 0
army_count: 34
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11385
score_details {
  idle_production_time: 28461.75
  idle_worker_time: 373.0625
  total_value_units: 15950.0
  total_value_structures: 2400.0
  killed_value_units: 7250.0
  killed_value_structures: 0.0
  collected_minerals: 19685.0
  collected_vespene: 5068.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 313.0
  spent_minerals: 17468.0
  spent_vespene: 4150.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5050.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7725.0
    economy: 2768.0
    technology: -375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 575.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 4050.0
    technology: 1350.0
    upgrade: 925.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 300.0
    upgrade: 925.0
  }
  total_used_minerals {
    none: 0.0
    army: 8300.0
    economy: 8100.0
    technology: 1500.0
    upgrade: 925.0
  }
  total_used_vespene {
    none: 0.0
    army: 3600.0
    economy: 0.0
    technology: 400.0
    upgrade: 925.0
  }
  total_damage_dealt {
    life: 10563.3037109
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23791.0761719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2267
vespene: 918
food_cap: 116
food_used: 42
food_army: 14
food_workers: 28
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  17490
ui_data{
 
Score:  11385
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
