----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3630
  player_apm: 181
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 156
}
game_duration_loops: 6972
game_duration_seconds: 311.271728516
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6039
score_details {
  idle_production_time: 2854.25
  idle_worker_time: 2.1875
  total_value_units: 4000.0
  total_value_structures: 1100.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 4770.0
  collected_vespene: 900.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 291.0
  spent_minerals: 4056.0
  spent_vespene: 775.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: -69.0
    technology: -125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1225.0
    economy: 2600.0
    technology: 750.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1725.0
    economy: 2950.0
    technology: 600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 997.25
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1069.55517578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 764
vespene: 125
food_cap: 60
food_used: 52
food_army: 29
food_workers: 23
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  6972
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 12
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 136
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 139
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 87
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  6039
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
