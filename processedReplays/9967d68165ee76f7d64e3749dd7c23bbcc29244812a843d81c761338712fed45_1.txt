----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3024
  player_apm: 193
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2979
  player_apm: 151
}
game_duration_loops: 25474
game_duration_seconds: 1137.31152344
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11238
score_details {
  idle_production_time: 8494.25
  idle_worker_time: 8.1875
  total_value_units: 5950.0
  total_value_structures: 1675.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 8790.0
  collected_vespene: 1448.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 582.0
  spent_minerals: 7875.0
  spent_vespene: 1200.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2100.0
    economy: 4100.0
    technology: 2125.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 450.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 5050.0
    technology: 1675.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 559.108398438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 721.195800781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 965
vespene: 248
food_cap: 92
food_used: 74
food_army: 32
food_workers: 42
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17447
score_details {
  idle_production_time: 32601.8125
  idle_worker_time: 391.0
  total_value_units: 18175.0
  total_value_structures: 2800.0
  killed_value_units: 8075.0
  killed_value_structures: 225.0
  collected_minerals: 18270.0
  collected_vespene: 5952.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 470.0
  spent_minerals: 17500.0
  spent_vespene: 5675.0
  food_used {
    none: 0.0
    army: 73.5
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6550.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4200.0
    economy: -250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 950.0
    economy: 1400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4500.0
    economy: 4500.0
    technology: 2250.0
    upgrade: 1175.0
  }
  used_vespene {
    none: 0.0
    army: 2300.0
    economy: 0.0
    technology: 450.0
    upgrade: 1175.0
  }
  total_used_minerals {
    none: 0.0
    army: 10950.0
    economy: 6000.0
    technology: 2400.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 4050.0
    economy: 0.0
    technology: 550.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 8266.00390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11161.0292969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 820
vespene: 277
food_cap: 132
food_used: 106
food_army: 73
food_workers: 33
idle_worker_count: 0
army_count: 76
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 108
  count: 8
}
multi {
  units {
    unit_type: 693
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 693
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17348
score_details {
  idle_production_time: 46195.125
  idle_worker_time: 516.3125
  total_value_units: 21675.0
  total_value_structures: 3425.0
  killed_value_units: 14475.0
  killed_value_structures: 225.0
  collected_minerals: 21780.0
  collected_vespene: 8568.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 627.0
  spent_minerals: 21750.0
  spent_vespene: 6275.0
  food_used {
    none: 3.0
    army: 53.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11000.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7900.0
    economy: -475.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1650.0
    economy: 1750.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3150.0
    economy: 4650.0
    technology: 2875.0
    upgrade: 1175.0
  }
  used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 600.0
    upgrade: 1175.0
  }
  total_used_minerals {
    none: 0.0
    army: 13850.0
    economy: 6800.0
    technology: 3225.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 4350.0
    economy: 0.0
    technology: 850.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 13419.9052734
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 18150.9609375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 80
vespene: 2293
food_cap: 138
food_used: 89
food_army: 53
food_workers: 33
idle_worker_count: 0
army_count: 60
warp_gate_count: 0

game_loop:  25474
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 108
  count: 12
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 138
  }
}

Score:  17348
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
