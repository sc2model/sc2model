----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2827
  player_apm: 41
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2797
  player_apm: 64
}
game_duration_loops: 9779
game_duration_seconds: 436.592956543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7321
score_details {
  idle_production_time: 1570.375
  idle_worker_time: 569.875
  total_value_units: 3300.0
  total_value_structures: 2600.0
  killed_value_units: 325.0
  killed_value_structures: 0.0
  collected_minerals: 5415.0
  collected_vespene: 956.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 156.0
  spent_minerals: 4775.0
  spent_vespene: 275.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2200.0
    economy: 2425.0
    technology: 1050.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2300.0
    economy: 2575.0
    technology: 1050.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 330.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 201.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 690
vespene: 681
food_cap: 87
food_used: 64
food_army: 44
food_workers: 20
idle_worker_count: 2
army_count: 44
warp_gate_count: 0

game_loop:  9779
ui_data{
 
Score:  7321
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
