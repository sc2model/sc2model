----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3184
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3365
  player_apm: 120
}
game_duration_loops: 35826
game_duration_seconds: 1599.48657227
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12689
score_details {
  idle_production_time: 1213.625
  idle_worker_time: 17.1875
  total_value_units: 4625.0
  total_value_structures: 4650.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 9430.0
  collected_vespene: 2784.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 694.0
  spent_minerals: 8450.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 75.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4850.0
    technology: 1750.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 400.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4850.0
    technology: 1750.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 400.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 264.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 80.0
    shields: 249.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1030
vespene: 759
food_cap: 125
food_used: 76
food_army: 29
food_workers: 47
idle_worker_count: 0
army_count: 11
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
single {
  unit {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 5
    energy: 0
  }
}

score{
 score_type: Melee
score: 33399
score_details {
  idle_production_time: 4565.75
  idle_worker_time: 856.5625
  total_value_units: 16205.0
  total_value_structures: 10350.0
  killed_value_units: 650.0
  killed_value_structures: 0.0
  collected_minerals: 25100.0
  collected_vespene: 9704.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 1007.0
  spent_minerals: 23290.0
  spent_vespene: 8975.0
  food_used {
    none: 0.0
    army: 136.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1525.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 8035.0
    economy: 8100.0
    technology: 4000.0
    upgrade: 2100.0
  }
  used_vespene {
    none: 0.0
    army: 4925.0
    economy: 0.0
    technology: 1550.0
    upgrade: 2100.0
  }
  total_used_minerals {
    none: 0.0
    army: 8830.0
    economy: 8600.0
    technology: 3850.0
    upgrade: 1950.0
  }
  total_used_vespene {
    none: 0.0
    army: 4925.0
    economy: 0.0
    technology: 1550.0
    upgrade: 1950.0
  }
  total_damage_dealt {
    life: 1482.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1020.0
    shields: 1339.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1860
vespene: 729
food_cap: 200
food_used: 199
food_army: 136
food_workers: 63
idle_worker_count: 0
army_count: 35
warp_gate_count: 7

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 4
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 50243
score_details {
  idle_production_time: 5587.25
  idle_worker_time: 1714.25
  total_value_units: 21125.0
  total_value_structures: 11750.0
  killed_value_units: 14100.0
  killed_value_structures: 2950.0
  collected_minerals: 40595.0
  collected_vespene: 17488.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 447.0
  spent_minerals: 29995.0
  spent_vespene: 12700.0
  food_used {
    none: 0.0
    army: 148.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6725.0
    economy: 4375.0
    technology: 725.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4975.0
    economy: 150.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4445.0
    economy: 1675.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 9180.0
    economy: 7875.0
    technology: 4300.0
    upgrade: 3050.0
  }
  used_vespene {
    none: 0.0
    army: 5800.0
    economy: 0.0
    technology: 1550.0
    upgrade: 3050.0
  }
  total_used_minerals {
    none: 0.0
    army: 11925.0
    economy: 9550.0
    technology: 4300.0
    upgrade: 2900.0
  }
  total_used_vespene {
    none: 0.0
    army: 6750.0
    economy: 0.0
    technology: 1550.0
    upgrade: 2900.0
  }
  total_damage_dealt {
    life: 20835.8183594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5339.5
    shields: 11113.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 10650
vespene: 4788
food_cap: 200
food_used: 189
food_army: 148
food_workers: 41
idle_worker_count: 7
army_count: 26
warp_gate_count: 7

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 4
  count: 25
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 6
}
multi {
  units {
    unit_type: 79
    player_relative: 1
    health: 234
    shields: 11
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 109
    shields: 60
    energy: 200
  }
  units {
    unit_type: 496
    player_relative: 1
    health: 300
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 68
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 145
    shields: 100
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 50
    energy: 0
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 115
    shields: 60
    energy: 200
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 183
    shields: 3
    energy: 0
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 15
    shields: 25
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 30
    shields: 60
    energy: 200
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 132
    energy: 0
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 93
    shields: 4
    energy: 200
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 145
    shields: 88
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 29
    shields: 11
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 145
    shields: 99
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 89
    shields: 11
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 100
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 106
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 238
    shields: 11
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 150
    shields: 66
    energy: 0
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 200
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 59
    energy: 200
  }
}

score{
 score_type: Melee
score: 49443
score_details {
  idle_production_time: 8344.0625
  idle_worker_time: 3389.0625
  total_value_units: 29705.0
  total_value_structures: 11900.0
  killed_value_units: 25650.0
  killed_value_structures: 4850.0
  collected_minerals: 44965.0
  collected_vespene: 19508.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 313.0
  spent_minerals: 33600.0
  spent_vespene: 16175.0
  food_used {
    none: 0.0
    army: 134.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13025.0
    economy: 6075.0
    technology: 1925.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 9225.0
    economy: 150.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8885.0
    economy: 2275.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 8345.0
    economy: 7825.0
    technology: 4000.0
    upgrade: 3525.0
  }
  used_vespene {
    none: 0.0
    army: 5925.0
    economy: 0.0
    technology: 1550.0
    upgrade: 3525.0
  }
  total_used_minerals {
    none: 0.0
    army: 17505.0
    economy: 9700.0
    technology: 4300.0
    upgrade: 3050.0
  }
  total_used_vespene {
    none: 0.0
    army: 10950.0
    economy: 0.0
    technology: 1550.0
    upgrade: 3050.0
  }
  total_damage_dealt {
    life: 34453.8359375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12347.0
    shields: 19130.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 11415
vespene: 3333
food_cap: 200
food_used: 174
food_army: 134
food_workers: 40
idle_worker_count: 1
army_count: 37
warp_gate_count: 7

game_loop:  35826
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 4
  count: 25
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 6
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 10
    shields: 19
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  49443
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
