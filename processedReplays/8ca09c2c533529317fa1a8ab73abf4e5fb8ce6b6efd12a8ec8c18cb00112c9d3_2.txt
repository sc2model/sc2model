----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5171
  player_apm: 230
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5270
  player_apm: 307
}
game_duration_loops: 17696
game_duration_seconds: 790.055114746
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7300
score_details {
  idle_production_time: 1476.4375
  idle_worker_time: 240.9375
  total_value_units: 5350.0
  total_value_structures: 2700.0
  killed_value_units: 2675.0
  killed_value_structures: 0.0
  collected_minerals: 6925.0
  collected_vespene: 1592.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 335.0
  spent_minerals: 6850.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 925.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1000.0
    economy: 3550.0
    technology: 1175.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1775.0
    economy: 4600.0
    technology: 1025.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1813.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2309.75
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 136.503173828
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 81
vespene: 69
food_cap: 62
food_used: 55
food_army: 17
food_workers: 36
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 35
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 35
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 53
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1466
    shields: 0
    energy: 41
  }
}

score{
 score_type: Melee
score: 10224
score_details {
  idle_production_time: 4925.375
  idle_worker_time: 1436.75
  total_value_units: 12225.0
  total_value_structures: 5775.0
  killed_value_units: 9325.0
  killed_value_structures: 0.0
  collected_minerals: 16650.0
  collected_vespene: 3884.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 335.0
  spent_minerals: 15625.0
  spent_vespene: 3475.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6550.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5631.0
    economy: 3221.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1958.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1000.0
    economy: 3750.0
    technology: 2250.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 500.0
    economy: 0.0
    technology: 525.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 6225.0
    economy: 7500.0
    technology: 2250.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 2300.0
    economy: 0.0
    technology: 525.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 8541.40917969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13401.5585938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1955.14550781
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 948
vespene: 351
food_cap: 118
food_used: 48
food_army: 20
food_workers: 28
idle_worker_count: 28
army_count: 5
warp_gate_count: 0

game_loop:  17696
ui_data{
 
Score:  10224
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
