----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 191
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5793
  player_apm: 327
}
game_duration_loops: 5088
game_duration_seconds: 227.158706665
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4078
score_details {
  idle_production_time: 254.75
  idle_worker_time: 48.375
  total_value_units: 2100.0
  total_value_structures: 1500.0
  killed_value_units: 500.0
  killed_value_structures: 100.0
  collected_minerals: 2820.0
  collected_vespene: 608.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 0.0
  spent_minerals: 2625.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 175.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 2100.0
    technology: 650.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 550.0
    economy: 2350.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 673.884277344
    shields: 1087.11572266
    energy: 0.0
  }
  total_damage_taken {
    life: 302.125
    shields: 812.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 245
vespene: 208
food_cap: 55
food_used: 29
food_army: 8
food_workers: 21
idle_worker_count: 2
army_count: 4
warp_gate_count: 0

game_loop:  5088
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 62
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 84
  count: 1
}
single {
  unit {
    unit_type: 488
    player_relative: 1
    health: 43
    shields: 0
    energy: 57
  }
}

Score:  4078
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
