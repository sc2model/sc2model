----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4074
  player_apm: 352
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4067
  player_apm: 167
}
game_duration_loops: 18440
game_duration_seconds: 823.271728516
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10950
score_details {
  idle_production_time: 4754.6875
  idle_worker_time: 16.1875
  total_value_units: 6450.0
  total_value_structures: 1750.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 8840.0
  collected_vespene: 1860.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 492.0
  spent_minerals: 8650.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 4950.0
    technology: 1500.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 300.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 1575.0
    economy: 6350.0
    technology: 1400.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 777.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1723.69824219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 240
vespene: 110
food_cap: 106
food_used: 91
food_army: 44
food_workers: 47
idle_worker_count: 0
army_count: 10
warp_gate_count: 0
larva_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1159
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11893
score_details {
  idle_production_time: 21476.1875
  idle_worker_time: 42.875
  total_value_units: 16950.0
  total_value_structures: 3275.0
  killed_value_units: 7175.0
  killed_value_structures: 325.0
  collected_minerals: 18120.0
  collected_vespene: 6548.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 470.0
  spent_minerals: 17550.0
  spent_vespene: 5675.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 5850.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 1175.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6600.0
    economy: 2650.0
    technology: 275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 4700.0
    technology: 2050.0
    upgrade: 1200.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 800.0
    upgrade: 1200.0
  }
  total_used_minerals {
    none: 0.0
    army: 8600.0
    economy: 8100.0
    technology: 2675.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 0.0
    army: 4175.0
    economy: 0.0
    technology: 1050.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 7113.42480469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15791.3916016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 620
vespene: 873
food_cap: 146
food_used: 42
food_army: 9
food_workers: 33
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  18440
ui_data{
 
Score:  11893
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
