----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3700
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3718
  player_apm: 200
}
game_duration_loops: 20051
game_duration_seconds: 895.196411133
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7173
score_details {
  idle_production_time: 1078.75
  idle_worker_time: 116.1875
  total_value_units: 4850.0
  total_value_structures: 2500.0
  killed_value_units: 1950.0
  killed_value_structures: 250.0
  collected_minerals: 6655.0
  collected_vespene: 1768.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 291.0
  spent_minerals: 5875.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 500.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1500.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 3100.0
    technology: 1075.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 3750.0
    technology: 775.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3587.8125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2181.85571289
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 229.518310547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 830
vespene: 593
food_cap: 70
food_used: 44
food_army: 17
food_workers: 27
idle_worker_count: 1
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 34
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10863
score_details {
  idle_production_time: 5546.5
  idle_worker_time: 1393.75
  total_value_units: 12250.0
  total_value_structures: 4750.0
  killed_value_units: 6400.0
  killed_value_structures: 250.0
  collected_minerals: 15340.0
  collected_vespene: 5248.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 470.0
  spent_minerals: 14150.0
  spent_vespene: 3550.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4300.0
    economy: 550.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6650.0
    economy: 1100.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 450.0
    economy: 4650.0
    technology: 1500.0
    upgrade: 300.0
  }
  used_vespene {
    none: 100.0
    army: 150.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 100.0
    army: 7100.0
    economy: 5250.0
    technology: 1900.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 100.0
    army: 2700.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 9850.75390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13260.8232422
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1386.63452148
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1240
vespene: 1698
food_cap: 126
food_used: 42
food_army: 7
food_workers: 35
idle_worker_count: 3
army_count: 5
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 163
  }
}

score{
 score_type: Melee
score: 10881
score_details {
  idle_production_time: 5584.75
  idle_worker_time: 1481.3125
  total_value_units: 12250.0
  total_value_structures: 4750.0
  killed_value_units: 6400.0
  killed_value_structures: 250.0
  collected_minerals: 15350.0
  collected_vespene: 5256.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 492.0
  spent_minerals: 14150.0
  spent_vespene: 3550.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4300.0
    economy: 550.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6650.0
    economy: 1100.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 450.0
    economy: 4650.0
    technology: 1500.0
    upgrade: 300.0
  }
  used_vespene {
    none: 100.0
    army: 150.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 100.0
    army: 7100.0
    economy: 5250.0
    technology: 1900.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 100.0
    army: 2700.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 9850.75390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13260.8232422
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1386.63452148
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1250
vespene: 1706
food_cap: 126
food_used: 42
food_army: 7
food_workers: 35
idle_worker_count: 35
army_count: 5
warp_gate_count: 0

game_loop:  20051
ui_data{
 
Score:  10881
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
