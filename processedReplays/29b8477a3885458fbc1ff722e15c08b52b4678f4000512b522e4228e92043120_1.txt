----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3740
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3749
  player_apm: 118
}
game_duration_loops: 11756
game_duration_seconds: 524.858032227
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7445
score_details {
  idle_production_time: 1200.625
  idle_worker_time: 606.875
  total_value_units: 5875.0
  total_value_structures: 2975.0
  killed_value_units: 4500.0
  killed_value_structures: 600.0
  collected_minerals: 7825.0
  collected_vespene: 1664.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 67.0
  spent_minerals: 7199.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 12.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2725.0
    economy: 1250.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 961.0
    economy: 1700.0
    technology: -226.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 359.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 2625.0
    technology: 800.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 4625.0
    technology: 800.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 9607.1328125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4226.70703125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1365.50366211
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 665
vespene: 80
food_cap: 86
food_used: 49
food_army: 37
food_workers: 12
idle_worker_count: 10
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 122
    shields: 0
    energy: 67
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 35
  }
}

score{
 score_type: Melee
score: 8699
score_details {
  idle_production_time: 1789.0625
  idle_worker_time: 1165.5625
  total_value_units: 6725.0
  total_value_structures: 2975.0
  killed_value_units: 5850.0
  killed_value_structures: 875.0
  collected_minerals: 9065.0
  collected_vespene: 1828.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 313.0
  spent_minerals: 8099.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3575.0
    economy: 1625.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1311.0
    economy: 1750.0
    technology: -226.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 359.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1950.0
    economy: 3025.0
    technology: 800.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3100.0
    economy: 5025.0
    technology: 800.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 12577.1767578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5146.57421875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2218.50024414
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1005
vespene: 119
food_cap: 86
food_used: 59
food_army: 39
food_workers: 19
idle_worker_count: 1
army_count: 29
warp_gate_count: 0

game_loop:  11756
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 18
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 103
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 93
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 114
    shields: 0
    energy: 5
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 102
    shields: 0
    energy: 13
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

Score:  8699
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
