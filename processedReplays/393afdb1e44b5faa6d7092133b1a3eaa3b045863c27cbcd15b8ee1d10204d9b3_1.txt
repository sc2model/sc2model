----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2986
  player_apm: 106
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3064
  player_apm: 43
}
game_duration_loops: 34624
game_duration_seconds: 1545.82214355
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9427
score_details {
  idle_production_time: 1484.1875
  idle_worker_time: 9.875
  total_value_units: 4500.0
  total_value_structures: 4950.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 8705.0
  collected_vespene: 2472.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 335.0
  spent_minerals: 7950.0
  spent_vespene: 2350.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 1800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 3350.0
    technology: 2050.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 650.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1100.0
    economy: 4750.0
    technology: 1750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 650.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 844.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2466.0
    shields: 3323.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 805
vespene: 122
food_cap: 95
food_used: 39
food_army: 17
food_workers: 22
idle_worker_count: 0
army_count: 6
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 495
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 474
    shields: 474
    energy: 0
    build_progress: 0.941346168518
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 471
    shields: 471
    energy: 0
    build_progress: 0.934615373611
  }
}

score{
 score_type: Melee
score: 15360
score_details {
  idle_production_time: 5953.75
  idle_worker_time: 636.0
  total_value_units: 11575.0
  total_value_structures: 6425.0
  killed_value_units: 4250.0
  killed_value_structures: 0.0
  collected_minerals: 14190.0
  collected_vespene: 6020.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 739.0
  spent_minerals: 13100.0
  spent_vespene: 5200.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2925.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1475.0
    economy: 3125.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 5050.0
    technology: 2050.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 650.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 3425.0
    economy: 8325.0
    technology: 2050.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 4450.0
    economy: 0.0
    technology: 650.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 4241.98046875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4483.0
    shields: 7241.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1140
vespene: 820
food_cap: 133
food_used: 90
food_army: 44
food_workers: 46
idle_worker_count: 4
army_count: 21
warp_gate_count: 6

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 17
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 137
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 137
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 104
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 15
    shields: 60
    energy: 200
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 137
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 105
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 137
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 136
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 105
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 104
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 18652
score_details {
  idle_production_time: 11337.0625
  idle_worker_time: 1740.125
  total_value_units: 21775.0
  total_value_structures: 7975.0
  killed_value_units: 11500.0
  killed_value_structures: 4700.0
  collected_minerals: 23815.0
  collected_vespene: 9912.0
  collection_rate_minerals: 195.0
  collection_rate_vespene: 492.0
  spent_minerals: 21475.0
  spent_vespene: 8775.0
  food_used {
    none: 0.0
    army: 64.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 6625.0
    economy: 4550.0
    technology: 1775.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 2325.0
    economy: 300.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5550.0
    economy: 5375.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4900.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3450.0
    economy: 5000.0
    technology: 2200.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 550.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 9200.0
    economy: 10525.0
    technology: 2350.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 7925.0
    economy: 0.0
    technology: 650.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 31983.96875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9851.28222656
    shields: 15698.3427734
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2390
vespene: 1137
food_cap: 180
food_used: 90
food_army: 64
food_workers: 26
idle_worker_count: 0
army_count: 32
warp_gate_count: 8

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 83
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 74
  count: 24
}
multi {
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 128
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 11
    shields: 40
    energy: 127
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 8
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 128
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 8
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 24190
score_details {
  idle_production_time: 13683.1875
  idle_worker_time: 2722.5625
  total_value_units: 26750.0
  total_value_structures: 7975.0
  killed_value_units: 11500.0
  killed_value_structures: 5650.0
  collected_minerals: 27665.0
  collected_vespene: 11700.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 470.0
  spent_minerals: 24600.0
  spent_vespene: 10625.0
  food_used {
    none: 0.0
    army: 103.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 6625.0
    economy: 4725.0
    technology: 2375.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 2325.0
    economy: 300.0
    technology: 700.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5575.0
    economy: 5375.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4975.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5400.0
    economy: 6150.0
    technology: 2200.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 4200.0
    economy: 0.0
    technology: 550.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 11175.0
    economy: 11675.0
    technology: 2350.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 9775.0
    economy: 0.0
    technology: 650.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 36072.96875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9891.28222656
    shields: 15718.3427734
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3115
vespene: 1075
food_cap: 180
food_used: 152
food_army: 103
food_workers: 49
idle_worker_count: 3
army_count: 48
warp_gate_count: 8

game_loop:  34624
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 83
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 74
  count: 23
}

Score:  24190
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
