----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4424
  player_apm: 113
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4321
  player_apm: 190
}
game_duration_loops: 13121
game_duration_seconds: 585.799804688
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8533
score_details {
  idle_production_time: 8941.5625
  idle_worker_time: 158.8125
  total_value_units: 6350.0
  total_value_structures: 1025.0
  killed_value_units: 1400.0
  killed_value_structures: 1050.0
  collected_minerals: 8105.0
  collected_vespene: 1828.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 492.0
  spent_minerals: 6975.0
  spent_vespene: 600.0
  food_used {
    none: 1.5
    army: 27.5
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 850.0
    technology: 650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1550.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 3575.0
    technology: 550.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3550.0
    economy: 3725.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2744.59521484
    shields: 5460.97998047
    energy: 0.0
  }
  total_damage_taken {
    life: 2470.78564453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1180
vespene: 1228
food_cap: 68
food_used: 61
food_army: 27
food_workers: 32
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 22
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 10
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 6762
score_details {
  idle_production_time: 16233.0
  idle_worker_time: 205.5
  total_value_units: 9500.0
  total_value_structures: 1325.0
  killed_value_units: 3850.0
  killed_value_structures: 1050.0
  collected_minerals: 10700.0
  collected_vespene: 2812.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 111.0
  spent_minerals: 9625.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3150.0
    economy: 900.0
    technology: 650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4500.0
    economy: 1200.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 400.0
    economy: 2375.0
    technology: 700.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 6700.0
    economy: 3725.0
    technology: 850.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 5324.62353516
    shields: 8169.49755859
    energy: 0.0
  }
  total_damage_taken {
    life: 10057.3710938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1125
vespene: 1912
food_cap: 62
food_used: 30
food_army: 8
food_workers: 22
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  13121
ui_data{
 
Score:  6762
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
