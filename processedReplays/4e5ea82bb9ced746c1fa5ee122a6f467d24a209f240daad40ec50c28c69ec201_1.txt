----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3218
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3299
  player_apm: 62
}
game_duration_loops: 4348
game_duration_seconds: 194.120681763
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 905
score_details {
  idle_production_time: 1763.9375
  idle_worker_time: 3.75
  total_value_units: 1250.0
  total_value_structures: 500.0
  killed_value_units: 250.0
  killed_value_structures: 0.0
  collected_minerals: 730.0
  collected_vespene: 0.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 750.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 1.5
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 175.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 75.0
    economy: 550.0
    technology: 250.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 250.0
    economy: 1350.0
    technology: 250.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 920.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 913.344238281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 30
vespene: 0
food_cap: 22
food_used: 1
food_army: 1
food_workers: 0
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  4348
ui_data{
 
Score:  905
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
