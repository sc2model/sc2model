----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4037
  player_apm: 222
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3991
  player_apm: 148
}
game_duration_loops: 17977
game_duration_seconds: 802.600646973
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9693
score_details {
  idle_production_time: 7284.9375
  idle_worker_time: 25.3125
  total_value_units: 5150.0
  total_value_structures: 1850.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 8235.0
  collected_vespene: 1508.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 604.0
  spent_minerals: 7325.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1125.0
    economy: 4300.0
    technology: 1150.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 250.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 925.0
    economy: 5450.0
    technology: 1300.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 885.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1599.11181641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 960
vespene: 333
food_cap: 66
food_used: 62
food_army: 22
food_workers: 38
idle_worker_count: 6
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 54
  }
}

score{
 score_type: Melee
score: 11532
score_details {
  idle_production_time: 19771.9375
  idle_worker_time: 584.0
  total_value_units: 15250.0
  total_value_structures: 2400.0
  killed_value_units: 5625.0
  killed_value_structures: 0.0
  collected_minerals: 17630.0
  collected_vespene: 4152.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 515.0
  spent_minerals: 15850.0
  spent_vespene: 3650.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4800.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5300.0
    economy: 3950.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 4650.0
    technology: 1500.0
    upgrade: 900.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 500.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 0.0
    army: 5600.0
    economy: 9400.0
    technology: 1850.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 750.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 5341.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13617.2578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1830
vespene: 502
food_cap: 98
food_used: 56
food_army: 10
food_workers: 46
idle_worker_count: 46
army_count: 0
warp_gate_count: 0

game_loop:  17977
ui_data{
 
Score:  11532
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
