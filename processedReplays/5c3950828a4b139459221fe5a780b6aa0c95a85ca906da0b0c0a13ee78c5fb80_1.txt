----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4717
  player_apm: 223
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4591
  player_apm: 270
}
game_duration_loops: 28958
game_duration_seconds: 1292.85803223
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10452
score_details {
  idle_production_time: 5931.875
  idle_worker_time: 8.375
  total_value_units: 6700.0
  total_value_structures: 2025.0
  killed_value_units: 1725.0
  killed_value_structures: 400.0
  collected_minerals: 9690.0
  collected_vespene: 2068.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 671.0
  spent_minerals: 9381.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 1000.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1325.0
    economy: 281.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 975.0
    economy: 5400.0
    technology: 1525.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 200.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 5600.0
    technology: 1675.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4855.04296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3101.97705078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 359
vespene: 318
food_cap: 106
food_used: 79
food_army: 20
food_workers: 41
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 89
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 37
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 25433
score_details {
  idle_production_time: 27799.6875
  idle_worker_time: 604.9375
  total_value_units: 23500.0
  total_value_structures: 4125.0
  killed_value_units: 10625.0
  killed_value_structures: 675.0
  collected_minerals: 28870.0
  collected_vespene: 8544.0
  collection_rate_minerals: 2771.0
  collection_rate_vespene: 985.0
  spent_minerals: 25256.0
  spent_vespene: 6525.0
  food_used {
    none: 0.0
    army: 72.5
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6950.0
    economy: 2050.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8300.0
    economy: 1031.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2625.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3450.0
    economy: 8350.0
    technology: 2775.0
    upgrade: 1475.0
  }
  used_vespene {
    none: 150.0
    army: 1225.0
    economy: 0.0
    technology: 700.0
    upgrade: 1475.0
  }
  total_used_minerals {
    none: 300.0
    army: 12000.0
    economy: 11000.0
    technology: 3375.0
    upgrade: 1475.0
  }
  total_used_vespene {
    none: 300.0
    army: 4250.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1475.0
  }
  total_damage_dealt {
    life: 15625.9589844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16152.2363281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3664
vespene: 2019
food_cap: 174
food_used: 153
food_army: 72
food_workers: 81
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 89
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 24
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17975
score_details {
  idle_production_time: 45888.5
  idle_worker_time: 4296.0
  total_value_units: 36800.0
  total_value_structures: 5675.0
  killed_value_units: 24850.0
  killed_value_structures: 1725.0
  collected_minerals: 39910.0
  collected_vespene: 13520.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 447.0
  spent_minerals: 36993.0
  spent_vespene: 13262.0
  food_used {
    none: 0.0
    army: 12.5
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 16850.0
    economy: 3900.0
    technology: 550.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5100.0
    economy: 150.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 18587.0
    economy: 3856.0
    technology: 900.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 9287.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 950.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 1100.0
    economy: 6500.0
    technology: 2575.0
    upgrade: 1675.0
  }
  used_vespene {
    none: -100.0
    army: 425.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1675.0
  }
  total_used_minerals {
    none: 300.0
    army: 22200.0
    economy: 13100.0
    technology: 3925.0
    upgrade: 1475.0
  }
  total_used_vespene {
    none: 300.0
    army: 12400.0
    economy: 0.0
    technology: 1650.0
    upgrade: 1475.0
  }
  total_damage_dealt {
    life: 31740.0800781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 45194.6523438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2967
vespene: 258
food_cap: 184
food_used: 62
food_army: 12
food_workers: 50
idle_worker_count: 2
army_count: 12
warp_gate_count: 0

game_loop:  28958
ui_data{
 
Score:  17975
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
