----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3563
  player_apm: 131
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3558
  player_apm: 75
}
game_duration_loops: 18276
game_duration_seconds: 815.949768066
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12236
score_details {
  idle_production_time: 761.75
  idle_worker_time: 42.125
  total_value_units: 5650.0
  total_value_structures: 3850.0
  killed_value_units: 25.0
  killed_value_structures: 0.0
  collected_minerals: 9070.0
  collected_vespene: 2216.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 649.0
  spent_minerals: 8775.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2575.0
    economy: 5050.0
    technology: 1550.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 200.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 4550.0
    technology: 1550.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 176.145019531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 200.0
    shields: 220.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 345
vespene: 691
food_cap: 102
food_used: 97
food_army: 48
food_workers: 49
idle_worker_count: 0
army_count: 21
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 13
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 82
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
}

score{
 score_type: Melee
score: 14829
score_details {
  idle_production_time: 3750.0
  idle_worker_time: 128.3125
  total_value_units: 11875.0
  total_value_structures: 8900.0
  killed_value_units: 6950.0
  killed_value_structures: 0.0
  collected_minerals: 20020.0
  collected_vespene: 6784.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 537.0
  spent_minerals: 18125.0
  spent_vespene: 5600.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5275.0
    economy: 4400.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 3750.0
    technology: 2500.0
    upgrade: 1300.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 1100.0
    upgrade: 1300.0
  }
  total_used_minerals {
    none: 0.0
    army: 5800.0
    economy: 8150.0
    technology: 3100.0
    upgrade: 1150.0
  }
  total_used_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 1250.0
    upgrade: 1150.0
  }
  total_damage_dealt {
    life: 8820.55175781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12203.0
    shields: 14365.0400391
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1945
vespene: 1184
food_cap: 94
food_used: 52
food_army: 18
food_workers: 34
idle_worker_count: 1
army_count: 3
warp_gate_count: 3

game_loop:  18276
ui_data{
 multi {
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 48
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

Score:  14829
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
