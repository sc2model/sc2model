----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3364
  player_apm: 110
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3437
  player_apm: 162
}
game_duration_loops: 47556
game_duration_seconds: 2123.18383789
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13148
score_details {
  idle_production_time: 3102.6875
  idle_worker_time: 3.875
  total_value_units: 7500.0
  total_value_structures: 2425.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 10010.0
  collected_vespene: 2188.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 1007.0
  spent_minerals: 9750.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1975.0
    economy: 6500.0
    technology: 1425.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 300.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 1775.0
    economy: 7500.0
    technology: 1575.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1110.54296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 733.3984375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 310
vespene: 663
food_cap: 144
food_used: 103
food_army: 40
food_workers: 63
idle_worker_count: 0
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 38884
score_details {
  idle_production_time: 14487.6875
  idle_worker_time: 439.125
  total_value_units: 16300.0
  total_value_structures: 5600.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 28560.0
  collected_vespene: 10524.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 1276.0
  spent_minerals: 23350.0
  spent_vespene: 6900.0
  food_used {
    none: 0.0
    army: 118.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 5625.0
    economy: 9600.0
    technology: 5000.0
    upgrade: 3075.0
  }
  used_vespene {
    none: 150.0
    army: 2175.0
    economy: 0.0
    technology: 1150.0
    upgrade: 3075.0
  }
  total_used_minerals {
    none: 300.0
    army: 5975.0
    economy: 11950.0
    technology: 4950.0
    upgrade: 1825.0
  }
  total_used_vespene {
    none: 300.0
    army: 2375.0
    economy: 0.0
    technology: 1700.0
    upgrade: 1825.0
  }
  total_damage_dealt {
    life: 2185.61132812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1385.02490234
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5260
vespene: 3624
food_cap: 200
food_used: 193
food_army: 118
food_workers: 75
idle_worker_count: 16
army_count: 58
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 59
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 8
}
multi {
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 38576
score_details {
  idle_production_time: 33078.8125
  idle_worker_time: 4068.6875
  total_value_units: 39500.0
  total_value_structures: 7575.0
  killed_value_units: 33350.0
  killed_value_structures: 900.0
  collected_minerals: 40695.0
  collected_vespene: 17356.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 649.0
  spent_minerals: 40375.0
  spent_vespene: 17150.0
  food_used {
    none: 0.0
    army: 146.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 19875.0
    economy: 4225.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 10025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12625.0
    economy: 2275.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 7250.0
    economy: 9700.0
    technology: 4850.0
    upgrade: 3975.0
  }
  used_vespene {
    none: 150.0
    army: 6800.0
    economy: 0.0
    technology: 1150.0
    upgrade: 3975.0
  }
  total_used_minerals {
    none: 300.0
    army: 19775.0
    economy: 14375.0
    technology: 6000.0
    upgrade: 3975.0
  }
  total_used_vespene {
    none: 300.0
    army: 11775.0
    economy: 0.0
    technology: 1700.0
    upgrade: 3975.0
  }
  total_damage_dealt {
    life: 43123.6132812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27852.1328125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 370
vespene: 206
food_cap: 200
food_used: 199
food_army: 146
food_workers: 53
idle_worker_count: 0
army_count: 69
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 58
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 11
}
single {
  unit {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 57324
score_details {
  idle_production_time: 59110.4375
  idle_worker_time: 4545.75
  total_value_units: 45500.0
  total_value_structures: 10475.0
  killed_value_units: 37150.0
  killed_value_structures: 975.0
  collected_minerals: 57897.0
  collected_vespene: 25652.0
  collection_rate_minerals: 167.0
  collection_rate_vespene: 783.0
  spent_minerals: 49025.0
  spent_vespene: 17150.0
  food_used {
    none: 0.0
    army: 82.0
    economy: 112.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 21775.0
    economy: 4300.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 11925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12725.0
    economy: 2725.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4000.0
    economy: 15650.0
    technology: 6750.0
    upgrade: 3975.0
  }
  used_vespene {
    none: 150.0
    army: 4100.0
    economy: 0.0
    technology: 1150.0
    upgrade: 3975.0
  }
  total_used_minerals {
    none: 300.0
    army: 20275.0
    economy: 21725.0
    technology: 7900.0
    upgrade: 3975.0
  }
  total_used_vespene {
    none: 300.0
    army: 12275.0
    economy: 0.0
    technology: 1700.0
    upgrade: 3975.0
  }
  total_damage_dealt {
    life: 46533.2421875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 29043.109375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 8922
vespene: 8502
food_cap: 200
food_used: 194
food_army: 82
food_workers: 112
idle_worker_count: 0
army_count: 43
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 45
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 16
}
multi {
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

Score:  57324
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
