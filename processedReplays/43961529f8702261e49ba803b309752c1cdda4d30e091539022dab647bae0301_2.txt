----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4272
  player_apm: 155
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4153
  player_apm: 180
}
game_duration_loops: 15353
game_duration_seconds: 685.449645996
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7432
score_details {
  idle_production_time: 1084.5625
  idle_worker_time: 1111.9375
  total_value_units: 4300.0
  total_value_structures: 3100.0
  killed_value_units: 2900.0
  killed_value_structures: 0.0
  collected_minerals: 6990.0
  collected_vespene: 1772.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 358.0
  spent_minerals: 5949.0
  spent_vespene: 1081.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1300.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 892.0
    economy: 850.0
    technology: -99.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 497.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 550.0
    economy: 3350.0
    technology: 1150.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 200.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 1200.0
    economy: 4500.0
    technology: 1350.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3045.25488281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4161.09716797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 768.213378906
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 963
vespene: 619
food_cap: 62
food_used: 44
food_army: 10
food_workers: 34
idle_worker_count: 14
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
single {
  unit {
    unit_type: 134
    player_relative: 1
    health: 18
    shields: 0
    energy: 35
  }
}

score{
 score_type: Melee
score: 9513
score_details {
  idle_production_time: 3294.4375
  idle_worker_time: 1630.8125
  total_value_units: 8400.0
  total_value_structures: 3375.0
  killed_value_units: 7550.0
  killed_value_structures: 0.0
  collected_minerals: 11105.0
  collected_vespene: 2964.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 313.0
  spent_minerals: 10649.0
  spent_vespene: 2106.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3250.0
    economy: 2150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2342.0
    economy: 2000.0
    technology: -99.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 872.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1950.0
    economy: 3600.0
    technology: 1200.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 525.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 3750.0
    economy: 5650.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 1150.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 7384.25488281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7584.03466797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3490.62890625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 177
vespene: 786
food_cap: 78
food_used: 73
food_army: 39
food_workers: 32
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  15353
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 970
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  9513
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
