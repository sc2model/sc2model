----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2880
  player_apm: 36
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: -36400
  player_apm: 109
}
game_duration_loops: 35108
game_duration_seconds: 1567.43078613
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7794
score_details {
  idle_production_time: 1383.1875
  idle_worker_time: 107.75
  total_value_units: 1900.0
  total_value_structures: 4200.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 5120.0
  collected_vespene: 1724.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 313.0
  spent_minerals: 4600.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 2200.0
    technology: 2850.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 350.0
    economy: 2300.0
    technology: 2550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 300.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 330.280517578
    shields: 556.344482422
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 570
vespene: 624
food_cap: 71
food_used: 31
food_army: 12
food_workers: 19
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 10230
score_details {
  idle_production_time: 5336.3125
  idle_worker_time: 351.75
  total_value_units: 7180.0
  total_value_structures: 5650.0
  killed_value_units: 2250.0
  killed_value_structures: 450.0
  collected_minerals: 10075.0
  collected_vespene: 4000.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 0.0
  spent_minerals: 9770.0
  spent_vespene: 3800.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1950.0
    economy: 100.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2775.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1575.0
    economy: 3100.0
    technology: 3150.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 600.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 4330.0
    economy: 2850.0
    technology: 3150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 3100.0
    economy: 0.0
    technology: 600.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4845.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2200.28051758
    shields: 3006.96948242
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 355
vespene: 200
food_cap: 86
food_used: 44
food_army: 26
food_workers: 18
idle_worker_count: 6
army_count: 6
warp_gate_count: 0

game_loop:  20000
ui_data{
 single {
  unit {
    unit_type: 734
    player_relative: 4
    health: 136
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16144
score_details {
  idle_production_time: 12730.25
  idle_worker_time: 705.875
  total_value_units: 9990.0
  total_value_structures: 7400.0
  killed_value_units: 6550.0
  killed_value_structures: 550.0
  collected_minerals: 17230.0
  collected_vespene: 5584.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 515.0
  spent_minerals: 13280.0
  spent_vespene: 4400.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5000.0
    economy: 100.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4640.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1260.0
    economy: 3650.0
    technology: 4000.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 6490.0
    economy: 4400.0
    technology: 4000.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 3950.0
    economy: 0.0
    technology: 950.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 7991.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6053.90527344
    shields: 9146.46972656
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 4000
vespene: 1184
food_cap: 101
food_used: 46
food_army: 20
food_workers: 26
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  30000
ui_data{
 multi {
  units {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
  units {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
}

score{
 score_type: Melee
score: 10640
score_details {
  idle_production_time: 16810.6875
  idle_worker_time: 742.0625
  total_value_units: 13700.0
  total_value_structures: 8800.0
  killed_value_units: 8375.0
  killed_value_structures: 550.0
  collected_minerals: 18795.0
  collected_vespene: 6400.0
  collection_rate_minerals: 55.0
  collection_rate_vespene: 0.0
  spent_minerals: 17830.0
  spent_vespene: 6400.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6250.0
    economy: 100.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7200.0
    economy: 3675.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 2375.0
    technology: 4200.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 8400.0
    economy: 6050.0
    technology: 4200.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 5100.0
    economy: 0.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 9781.74609375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11679.9052734
    shields: 15283.6757812
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1015
vespene: 0
food_cap: 151
food_used: 21
food_army: 20
food_workers: 1
idle_worker_count: 1
army_count: 5
warp_gate_count: 0

game_loop:  35108
ui_data{
 
Score:  10640
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
