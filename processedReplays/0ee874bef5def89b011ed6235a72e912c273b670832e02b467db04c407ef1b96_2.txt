----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 47
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3036
  player_apm: 96
}
game_duration_loops: 15636
game_duration_seconds: 698.084411621
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10933
score_details {
  idle_production_time: 5285.0625
  idle_worker_time: 422.375
  total_value_units: 5900.0
  total_value_structures: 1500.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 8235.0
  collected_vespene: 1948.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 694.0
  spent_minerals: 6925.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 4550.0
    technology: 1100.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 300.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1975.0
    economy: 5100.0
    technology: 1000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 461.459960938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1316.89501953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1360
vespene: 673
food_cap: 114
food_used: 80
food_army: 40
food_workers: 40
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 15
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 110
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 127
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 129
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 108
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 91
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 108
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 127
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 127
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19957
score_details {
  idle_production_time: 10775.75
  idle_worker_time: 422.375
  total_value_units: 12200.0
  total_value_structures: 2350.0
  killed_value_units: 4775.0
  killed_value_structures: 725.0
  collected_minerals: 16075.0
  collected_vespene: 5332.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 963.0
  spent_minerals: 12400.0
  spent_vespene: 4500.0
  food_used {
    none: 0.0
    army: 81.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3000.0
    economy: 2125.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3450.0
    economy: 5800.0
    technology: 1550.0
    upgrade: 900.0
  }
  used_vespene {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 400.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 0.0
    army: 5525.0
    economy: 6600.0
    technology: 1700.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 4175.0
    economy: 0.0
    technology: 500.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 10054.9921875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4628.45751953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3725
vespene: 832
food_cap: 146
food_used: 135
food_army: 81
food_workers: 54
idle_worker_count: 0
army_count: 33
warp_gate_count: 0

game_loop:  15636
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 21
}
groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 12
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 90
  count: 2
}
multi {
  units {
    unit_type: 688
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 92
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 59
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 84
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 57
    shields: 0
    energy: 0
  }
}

Score:  19957
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
