----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3814
  player_apm: 117
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3807
  player_apm: 114
}
game_duration_loops: 9478
game_duration_seconds: 423.154541016
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10325
score_details {
  idle_production_time: 1164.8125
  idle_worker_time: 123.1875
  total_value_units: 4950.0
  total_value_structures: 3075.0
  killed_value_units: 4150.0
  killed_value_structures: 425.0
  collected_minerals: 7615.0
  collected_vespene: 2360.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 627.0
  spent_minerals: 6625.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2525.0
    economy: 1725.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1800.0
    economy: 3900.0
    technology: 1075.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 4200.0
    technology: 925.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 7899.54785156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 978.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1040
vespene: 635
food_cap: 86
food_used: 72
food_army: 36
food_workers: 36
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  9478
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 162
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 172
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 7
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 119
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 164
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  10325
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
