----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3395
  player_apm: 89
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3285
  player_apm: 92
}
game_duration_loops: 9226
game_duration_seconds: 411.903747559
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5855
score_details {
  idle_production_time: 1445.0
  idle_worker_time: 118.25
  total_value_units: 2950.0
  total_value_structures: 2250.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 5460.0
  collected_vespene: 1620.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 223.0
  spent_minerals: 4075.0
  spent_vespene: 800.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 12.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 475.0
    economy: 1800.0
    technology: 750.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 975.0
    economy: 2900.0
    technology: 750.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1114.54638672
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1698.0
    shields: 2823.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1435
vespene: 820
food_cap: 38
food_used: 20
food_army: 8
food_workers: 12
idle_worker_count: 12
army_count: 3
warp_gate_count: 0

game_loop:  9226
ui_data{
 
Score:  5855
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
