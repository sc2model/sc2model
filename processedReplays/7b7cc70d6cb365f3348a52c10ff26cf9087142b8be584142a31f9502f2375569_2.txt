----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4039
  player_apm: 103
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4051
  player_apm: 131
}
game_duration_loops: 14456
game_duration_seconds: 645.402160645
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6273
score_details {
  idle_production_time: 1592.25
  idle_worker_time: 205.75
  total_value_units: 3625.0
  total_value_structures: 2875.0
  killed_value_units: 2650.0
  killed_value_structures: 0.0
  collected_minerals: 5605.0
  collected_vespene: 1792.0
  collection_rate_minerals: 643.0
  collection_rate_vespene: 313.0
  spent_minerals: 5450.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 625.0
    economy: 583.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 418.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 600.0
    economy: 3200.0
    technology: 925.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 275.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 4000.0
    technology: 925.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2131.46191406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1993.65820312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 512.697998047
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 68
vespene: 605
food_cap: 69
food_used: 36
food_army: 11
food_workers: 22
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 47
  }
  units {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 58
  }
}

score{
 score_type: Melee
score: 11332
score_details {
  idle_production_time: 2979.4375
  idle_worker_time: 632.1875
  total_value_units: 7150.0
  total_value_structures: 4150.0
  killed_value_units: 4725.0
  killed_value_structures: 0.0
  collected_minerals: 10615.0
  collected_vespene: 2900.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 582.0
  spent_minerals: 10175.0
  spent_vespene: 2475.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2400.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1325.0
    economy: 890.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 743.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1600.0
    economy: 4700.0
    technology: 2150.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 800.0
    economy: 150.0
    technology: 350.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2750.0
    economy: 6000.0
    technology: 1600.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 1400.0
    economy: 300.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3436.46191406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3418.95605469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 698.086181641
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 333
vespene: 349
food_cap: 93
food_used: 71
food_army: 30
food_workers: 41
idle_worker_count: 1
army_count: 17
warp_gate_count: 0

game_loop:  14456
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 104
  }
  units {
    unit_type: 130
    player_relative: 1
    health: 1466
    shields: 0
    energy: 0
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 114
  }
}

Score:  11332
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
