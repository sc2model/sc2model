----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3913
  player_apm: 222
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4380
  player_apm: 159
}
game_duration_loops: 40609
game_duration_seconds: 1813.02832031
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14388
score_details {
  idle_production_time: 2031.0
  idle_worker_time: 382.1875
  total_value_units: 5250.0
  total_value_structures: 5550.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 11850.0
  collected_vespene: 1888.0
  collection_rate_minerals: 3303.0
  collection_rate_vespene: 1030.0
  spent_minerals: 11300.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 74.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1650.0
    economy: 7600.0
    technology: 1950.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 400.0
    economy: 0.0
    technology: 550.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 1350.0
    economy: 7950.0
    technology: 1800.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 350.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 350.306152344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 95.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 600
vespene: 438
food_cap: 123
food_used: 107
food_army: 33
food_workers: 71
idle_worker_count: 1
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 4
}
production {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 53
  }
  build_queue {
    unit_type: 45
    build_progress: 0.136029422283
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 27981
score_details {
  idle_production_time: 8992.5625
  idle_worker_time: 5754.6875
  total_value_units: 22100.0
  total_value_structures: 9075.0
  killed_value_units: 11550.0
  killed_value_structures: 0.0
  collected_minerals: 32005.0
  collected_vespene: 10852.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 940.0
  spent_minerals: 30950.0
  spent_vespene: 7350.0
  food_used {
    none: 0.0
    army: 89.0
    economy: 78.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7600.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9400.0
    economy: 1830.0
    technology: 201.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1850.0
    economy: 154.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 4450.0
    economy: 8575.0
    technology: 3575.0
    upgrade: 2075.0
  }
  used_vespene {
    none: 50.0
    army: 2000.0
    economy: 0.0
    technology: 550.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 50.0
    army: 13400.0
    economy: 11250.0
    technology: 3675.0
    upgrade: 1825.0
  }
  total_used_vespene {
    none: 50.0
    army: 3700.0
    economy: 300.0
    technology: 550.0
    upgrade: 1825.0
  }
  total_damage_dealt {
    life: 15077.2695312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14421.4765625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2691.30810547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1083
vespene: 3498
food_cap: 187
food_used: 167
food_army: 89
food_workers: 78
idle_worker_count: 16
army_count: 40
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 53
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 44
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 57
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 66
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 91
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 71
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 61
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 53
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 32
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 68
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23826
score_details {
  idle_production_time: 16093.8125
  idle_worker_time: 14155.625
  total_value_units: 39025.0
  total_value_structures: 10950.0
  killed_value_units: 33275.0
  killed_value_structures: 4425.0
  collected_minerals: 45325.0
  collected_vespene: 14964.0
  collection_rate_minerals: 363.0
  collection_rate_vespene: 67.0
  spent_minerals: 43950.0
  spent_vespene: 11700.0
  food_used {
    none: 0.0
    army: 64.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 22400.0
    economy: 3275.0
    technology: 2150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 9225.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 20550.0
    economy: 6159.0
    technology: 801.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6275.0
    economy: 154.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1400.0
    economy: 450.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3475.0
    economy: 5200.0
    technology: 4225.0
    upgrade: 2175.0
  }
  used_vespene {
    none: 50.0
    army: 1275.0
    economy: 150.0
    technology: 700.0
    upgrade: 2175.0
  }
  total_used_minerals {
    none: 50.0
    army: 25225.0
    economy: 12675.0
    technology: 4875.0
    upgrade: 2175.0
  }
  total_used_vespene {
    none: 50.0
    army: 8150.0
    economy: 600.0
    technology: 750.0
    upgrade: 2175.0
  }
  total_damage_dealt {
    life: 61543.4453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 36130.6523438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 10529.7714844
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1151
vespene: 3200
food_cap: 180
food_used: 82
food_army: 64
food_workers: 18
idle_worker_count: 6
army_count: 40
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1167
    shields: 0
    energy: 158
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 103
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1400
    shields: 0
    energy: 140
  }
  units {
    unit_type: 130
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22184
score_details {
  idle_production_time: 28113.9375
  idle_worker_time: 21090.5625
  total_value_units: 48250.0
  total_value_structures: 11525.0
  killed_value_units: 45225.0
  killed_value_structures: 7625.0
  collected_minerals: 51410.0
  collected_vespene: 15784.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 134.0
  spent_minerals: 50325.0
  spent_vespene: 15300.0
  food_used {
    none: 0.0
    army: 69.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 28050.0
    economy: 8400.0
    technology: 3875.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 11325.0
    economy: 0.0
    technology: 1200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 26432.0
    economy: 6209.0
    technology: 801.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 9204.0
    economy: 154.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1400.0
    economy: 450.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 3275.0
    economy: 5950.0
    technology: 4275.0
    upgrade: 2175.0
  }
  used_vespene {
    none: 50.0
    army: 1900.0
    economy: 300.0
    technology: 725.0
    upgrade: 2175.0
  }
  total_used_minerals {
    none: 50.0
    army: 30850.0
    economy: 13625.0
    technology: 4925.0
    upgrade: 2175.0
  }
  total_used_vespene {
    none: 50.0
    army: 11600.0
    economy: 900.0
    technology: 775.0
    upgrade: 2175.0
  }
  total_damage_dealt {
    life: 91658.5390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 44051.7578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 13395.5976562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 746
vespene: 413
food_cap: 200
food_used: 89
food_army: 69
food_workers: 20
idle_worker_count: 13
army_count: 25
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 5
}
groups {
  control_group_index: 7
  leader_unit_type: 54
  count: 1
}
single {
  unit {
    unit_type: 134
    player_relative: 1
    health: 599
    shields: 0
    energy: 52
  }
}

score{
 score_type: Melee
score: 21775
score_details {
  idle_production_time: 28881.8125
  idle_worker_time: 21585.375
  total_value_units: 48700.0
  total_value_structures: 11525.0
  killed_value_units: 46375.0
  killed_value_structures: 7625.0
  collected_minerals: 51820.0
  collected_vespene: 15840.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 134.0
  spent_minerals: 50575.0
  spent_vespene: 15300.0
  food_used {
    none: 0.0
    army: 63.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 28750.0
    economy: 8400.0
    technology: 3875.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 11775.0
    economy: 0.0
    technology: 1200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 26932.0
    economy: 6209.0
    technology: 801.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 9579.0
    economy: 154.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1400.0
    economy: 450.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2925.0
    economy: 6050.0
    technology: 4275.0
    upgrade: 2175.0
  }
  used_vespene {
    none: 50.0
    army: 1575.0
    economy: 300.0
    technology: 725.0
    upgrade: 2175.0
  }
  total_used_minerals {
    none: 50.0
    army: 31100.0
    economy: 13675.0
    technology: 4925.0
    upgrade: 2175.0
  }
  total_used_vespene {
    none: 50.0
    army: 11750.0
    economy: 900.0
    technology: 775.0
    upgrade: 2175.0
  }
  total_damage_dealt {
    life: 92690.2421875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 44621.7578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 13443.6982422
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 906
vespene: 469
food_cap: 200
food_used: 85
food_army: 63
food_workers: 21
idle_worker_count: 13
army_count: 30
warp_gate_count: 0

game_loop:  40609
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 5
}
groups {
  control_group_index: 7
  leader_unit_type: 54
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
}

Score:  21775
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
