----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 321
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5390
  player_apm: 310
}
game_duration_loops: 6153
game_duration_seconds: 274.706665039
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2559
score_details {
  idle_production_time: 4510.9375
  idle_worker_time: 0.0
  total_value_units: 3100.0
  total_value_structures: 675.0
  killed_value_units: 2000.0
  killed_value_structures: 0.0
  collected_minerals: 2800.0
  collected_vespene: 496.0
  collection_rate_minerals: 419.0
  collection_rate_vespene: 111.0
  spent_minerals: 2681.0
  spent_vespene: 406.0
  food_used {
    none: 0.0
    army: 7.5
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1056.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 56.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 425.0
    economy: 1275.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: -25.0
    army: 25.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 1625.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2778.38134766
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1969.88574219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 169
vespene: 90
food_cap: 38
food_used: 16
food_army: 7
food_workers: 9
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  6153
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 14
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 96
  count: 1
}

Score:  2559
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
