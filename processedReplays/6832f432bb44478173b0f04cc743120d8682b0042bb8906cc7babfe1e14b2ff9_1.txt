----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: -36400
  player_apm: 190
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3673
  player_apm: 95
}
game_duration_loops: 17286
game_duration_seconds: 771.750305176
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11398
score_details {
  idle_production_time: 6141.25
  idle_worker_time: 1.375
  total_value_units: 6300.0
  total_value_structures: 2250.0
  killed_value_units: 1875.0
  killed_value_structures: 0.0
  collected_minerals: 9820.0
  collected_vespene: 2128.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 537.0
  spent_minerals: 8350.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 4750.0
    technology: 1300.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5600.0
    technology: 1750.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 450.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1986.82885742
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3061.66699219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1520
vespene: 1028
food_cap: 90
food_used: 71
food_army: 21
food_workers: 50
idle_worker_count: 0
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 90
  count: 2
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21675
score_details {
  idle_production_time: 29162.0
  idle_worker_time: 1.375
  total_value_units: 17250.0
  total_value_structures: 3750.0
  killed_value_units: 10025.0
  killed_value_structures: 1175.0
  collected_minerals: 21465.0
  collected_vespene: 7360.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 1187.0
  spent_minerals: 21250.0
  spent_vespene: 6850.0
  food_used {
    none: 0.0
    army: 98.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6050.0
    economy: 2600.0
    technology: 775.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4375.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4900.0
    economy: 7450.0
    technology: 2700.0
    upgrade: 1325.0
  }
  used_vespene {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 800.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 0.0
    army: 8400.0
    economy: 8950.0
    technology: 3350.0
    upgrade: 1325.0
  }
  total_used_vespene {
    none: 0.0
    army: 3025.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1325.0
  }
  total_damage_dealt {
    life: 14456.4462891
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7945.80517578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 265
vespene: 510
food_cap: 200
food_used: 159
food_army: 98
food_workers: 61
idle_worker_count: 0
army_count: 33
warp_gate_count: 0

game_loop:  17286
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 109
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 90
  count: 2
}

Score:  21675
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
