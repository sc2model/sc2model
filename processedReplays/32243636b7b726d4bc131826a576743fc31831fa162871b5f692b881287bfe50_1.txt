----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 39
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2039
  player_apm: 48
}
game_duration_loops: 20469
game_duration_seconds: 913.858398438
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7314
score_details {
  idle_production_time: 1748.125
  idle_worker_time: 540.0625
  total_value_units: 2100.0
  total_value_structures: 3550.0
  killed_value_units: 150.0
  killed_value_structures: 2175.0
  collected_minerals: 6290.0
  collected_vespene: 924.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 291.0
  spent_minerals: 4850.0
  spent_vespene: 800.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 825.0
    technology: 1500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 775.0
    economy: 2200.0
    technology: 2150.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 750.0
    economy: 2200.0
    technology: 2300.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 4931.50390625
    shields: 5283.62890625
    energy: 0.0
  }
  total_damage_taken {
    life: 302.0
    shields: 349.578613281
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1490
vespene: 124
food_cap: 63
food_used: 34
food_army: 13
food_workers: 21
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 62
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 84
  count: 1
}
multi {
  units {
    unit_type: 83
    player_relative: 1
    health: 86
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 162
    shields: 34
    energy: 0
  }
}

score{
 score_type: Melee
score: 20912
score_details {
  idle_production_time: 7282.625
  idle_worker_time: 1267.75
  total_value_units: 7900.0
  total_value_structures: 8550.0
  killed_value_units: 1900.0
  killed_value_structures: 2575.0
  collected_minerals: 15550.0
  collected_vespene: 5712.0
  collection_rate_minerals: 2379.0
  collection_rate_vespene: 963.0
  spent_minerals: 15475.0
  spent_vespene: 5375.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1250.0
    economy: 925.0
    technology: 1800.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3875.0
    economy: 6700.0
    technology: 3700.0
    upgrade: 1300.0
  }
  used_vespene {
    none: 0.0
    army: 2375.0
    economy: 0.0
    technology: 1200.0
    upgrade: 1300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2875.0
    economy: 6750.0
    technology: 3850.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 1200.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 6049.50390625
    shields: 6283.62890625
    energy: 0.0
  }
  total_damage_taken {
    life: 693.0
    shields: 1507.57861328
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 125
vespene: 337
food_cap: 164
food_used: 129
food_army: 65
food_workers: 64
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 63
  count: 7
}
groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 62
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 83
  count: 5
}
multi {
  units {
    unit_type: 63
    player_relative: 1
    health: 400
    shields: 400
    energy: 0
  }
  units {
    unit_type: 72
    player_relative: 1
    health: 550
    shields: 550
    energy: 0
  }
  units {
    unit_type: 63
    player_relative: 1
    health: 400
    shields: 400
    energy: 0
  }
  units {
    unit_type: 63
    player_relative: 1
    health: 400
    shields: 400
    energy: 0
  }
  units {
    unit_type: 64
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 70
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 65
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 21966
score_details {
  idle_production_time: 7575.75
  idle_worker_time: 1267.75
  total_value_units: 7900.0
  total_value_structures: 8550.0
  killed_value_units: 5475.0
  killed_value_structures: 2575.0
  collected_minerals: 16460.0
  collected_vespene: 6056.0
  collection_rate_minerals: 2715.0
  collection_rate_vespene: 940.0
  spent_minerals: 15475.0
  spent_vespene: 5375.0
  food_used {
    none: 0.0
    army: 61.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3575.0
    economy: 925.0
    technology: 1800.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3625.0
    economy: 6700.0
    technology: 3700.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 1200.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2875.0
    economy: 6750.0
    technology: 3850.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 1200.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 7607.50390625
    shields: 7897.50390625
    energy: 0.0
  }
  total_damage_taken {
    life: 959.0
    shields: 2024.57861328
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1035
vespene: 681
food_cap: 164
food_used: 125
food_army: 61
food_workers: 64
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  20469
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 63
  count: 7
}
groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 62
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 83
  count: 5
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

Score:  21966
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
