----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3693
  player_apm: 79
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3699
  player_apm: 102
}
game_duration_loops: 15616
game_duration_seconds: 697.19152832
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8475
score_details {
  idle_production_time: 1291.375
  idle_worker_time: 641.1875
  total_value_units: 5225.0
  total_value_structures: 3700.0
  killed_value_units: 1875.0
  killed_value_structures: 100.0
  collected_minerals: 8235.0
  collected_vespene: 1540.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 694.0
  spent_minerals: 8200.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1075.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: 1025.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1950.0
    economy: 3800.0
    technology: 1500.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 550.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2225.0
    economy: 4300.0
    technology: 1350.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1142.0
    shields: 1568.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1371.0
    shields: 2550.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 85
vespene: 40
food_cap: 70
food_used: 70
food_army: 36
food_workers: 32
idle_worker_count: 0
army_count: 16
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 15
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 4
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 19005
score_details {
  idle_production_time: 3258.8125
  idle_worker_time: 729.375
  total_value_units: 10500.0
  total_value_structures: 7100.0
  killed_value_units: 4375.0
  killed_value_structures: 100.0
  collected_minerals: 15625.0
  collected_vespene: 4592.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 963.0
  spent_minerals: 14062.0
  spent_vespene: 3425.0
  food_used {
    none: 0.0
    army: 69.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 825.0
    economy: 1025.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: -75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3900.0
    economy: 6150.0
    technology: 2700.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 950.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 4825.0
    economy: 7250.0
    technology: 2700.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 950.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 2300.0
    shields: 2705.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1796.0
    shields: 3015.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1613
vespene: 1167
food_cap: 165
food_used: 129
food_army: 69
food_workers: 60
idle_worker_count: 1
army_count: 28
warp_gate_count: 5

game_loop:  15616
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 27
}
groups {
  control_group_index: 2
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 488
  count: 14
}

Score:  19005
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
