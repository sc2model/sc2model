----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3077
  player_apm: 36
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3309
  player_apm: 61
}
game_duration_loops: 23431
game_duration_seconds: 1046.09973145
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6177
score_details {
  idle_production_time: 2351.3125
  idle_worker_time: 932.625
  total_value_units: 2500.0
  total_value_structures: 2675.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 4825.0
  collected_vespene: 952.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 223.0
  spent_minerals: 4250.0
  spent_vespene: 325.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 750.0
    economy: 2625.0
    technology: 1175.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 850.0
    economy: 3425.0
    technology: 1175.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 100.0
    shields: 64.0
    energy: 0.0
  }
  total_damage_taken {
    life: 750.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 625
vespene: 627
food_cap: 46
food_used: 37
food_army: 15
food_workers: 20
idle_worker_count: 2
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 43
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 48
  }
}

score{
 score_type: Melee
score: 17740
score_details {
  idle_production_time: 8147.875
  idle_worker_time: 2987.0625
  total_value_units: 9050.0
  total_value_structures: 6100.0
  killed_value_units: 2270.0
  killed_value_structures: 150.0
  collected_minerals: 16310.0
  collected_vespene: 3880.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 582.0
  spent_minerals: 14350.0
  spent_vespene: 2500.0
  food_used {
    none: 0.0
    army: 49.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1520.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2350.0
    economy: 600.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3050.0
    economy: 5700.0
    technology: 2575.0
    upgrade: 725.0
  }
  used_vespene {
    none: 50.0
    army: 1050.0
    economy: 0.0
    technology: 425.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 50.0
    army: 5100.0
    economy: 6600.0
    technology: 2675.0
    upgrade: 475.0
  }
  total_used_vespene {
    none: 50.0
    army: 1150.0
    economy: 0.0
    technology: 425.0
    upgrade: 475.0
  }
  total_damage_dealt {
    life: 2168.0
    shields: 2912.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3330.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2010
vespene: 1380
food_cap: 156
food_used: 93
food_army: 49
food_workers: 44
idle_worker_count: 6
army_count: 31
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11136
score_details {
  idle_production_time: 10070.6875
  idle_worker_time: 3998.3125
  total_value_units: 11975.0
  total_value_structures: 6100.0
  killed_value_units: 4450.0
  killed_value_structures: 150.0
  collected_minerals: 18685.0
  collected_vespene: 4976.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 403.0
  spent_minerals: 17450.0
  spent_vespene: 3325.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2975.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7050.0
    economy: 2525.0
    technology: 1350.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 450.0
    economy: 4075.0
    technology: 1275.0
    upgrade: 725.0
  }
  used_vespene {
    none: 50.0
    army: 225.0
    economy: 0.0
    technology: 375.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 50.0
    army: 7200.0
    economy: 7050.0
    technology: 2675.0
    upgrade: 725.0
  }
  total_used_vespene {
    none: 50.0
    army: 1825.0
    economy: 0.0
    technology: 425.0
    upgrade: 725.0
  }
  total_damage_dealt {
    life: 4977.5
    shields: 6783.625
    energy: 0.0
  }
  total_damage_taken {
    life: 16819.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1285
vespene: 1651
food_cap: 133
food_used: 32
food_army: 6
food_workers: 23
idle_worker_count: 28
army_count: 1
warp_gate_count: 0

game_loop:  23431
ui_data{
 
Score:  11136
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
