----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3918
  player_apm: 103
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3902
  player_apm: 172
}
game_duration_loops: 8186
game_duration_seconds: 365.471923828
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7569
score_details {
  idle_production_time: 800.375
  idle_worker_time: 238.0625
  total_value_units: 3900.0
  total_value_structures: 2050.0
  killed_value_units: 1575.0
  killed_value_structures: 250.0
  collected_minerals: 5200.0
  collected_vespene: 1456.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 335.0
  spent_minerals: 4612.0
  spent_vespene: 825.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 800.0
    economy: 600.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2325.0
    economy: 2300.0
    technology: 800.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2175.0
    economy: 2300.0
    technology: 800.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 4700.69677734
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 212.0
    shields: 628.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 638
vespene: 631
food_cap: 63
food_used: 63
food_army: 40
food_workers: 23
idle_worker_count: 0
army_count: 15
warp_gate_count: 3

game_loop:  8186
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 136
  count: 15
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  7569
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
