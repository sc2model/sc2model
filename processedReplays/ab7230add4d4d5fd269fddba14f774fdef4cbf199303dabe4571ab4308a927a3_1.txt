----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4074
  player_apm: 352
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4067
  player_apm: 167
}
game_duration_loops: 18440
game_duration_seconds: 823.271728516
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10789
score_details {
  idle_production_time: 1587.875
  idle_worker_time: 628.0625
  total_value_units: 5075.0
  total_value_structures: 4100.0
  killed_value_units: 850.0
  killed_value_structures: 0.0
  collected_minerals: 8465.0
  collected_vespene: 1924.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 492.0
  spent_minerals: 8350.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1500.0
    economy: 5250.0
    technology: 1500.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 650.0
    economy: 150.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2000.0
    economy: 5450.0
    technology: 1250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 625.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1723.69824219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 777.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 165
vespene: 424
food_cap: 133
food_used: 78
food_army: 30
food_workers: 47
idle_worker_count: 4
army_count: 14
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 52
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 15
  }
  units {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
    add_on {
      unit_type: 130
      build_progress: 0.0387499928474
    }
  }
}

score{
 score_type: Melee
score: 21581
score_details {
  idle_production_time: 7522.875
  idle_worker_time: 1621.25
  total_value_units: 14300.0
  total_value_structures: 8650.0
  killed_value_units: 11725.0
  killed_value_structures: 1125.0
  collected_minerals: 21315.0
  collected_vespene: 6716.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 1052.0
  spent_minerals: 20450.0
  spent_vespene: 4925.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6600.0
    economy: 2650.0
    technology: 275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 5850.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 1175.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3100.0
    economy: 7950.0
    technology: 3000.0
    upgrade: 1175.0
  }
  used_vespene {
    none: 50.0
    army: 1150.0
    economy: 300.0
    technology: 925.0
    upgrade: 1175.0
  }
  total_used_minerals {
    none: 100.0
    army: 8800.0
    economy: 8700.0
    technology: 3125.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 100.0
    army: 2300.0
    economy: 600.0
    technology: 1025.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 15791.3916016
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7113.42480469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2720.92236328
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 915
vespene: 1791
food_cap: 200
food_used: 121
food_army: 60
food_workers: 61
idle_worker_count: 5
army_count: 37
warp_gate_count: 0

game_loop:  18440
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 51
  count: 25
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 49
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 62
  }
  units {
    unit_type: 130
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 130
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  21581
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
