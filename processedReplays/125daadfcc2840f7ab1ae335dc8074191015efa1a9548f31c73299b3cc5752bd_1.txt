----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4080
  player_apm: 204
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4185
  player_apm: 124
}
game_duration_loops: 19842
game_duration_seconds: 885.86541748
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11285
score_details {
  idle_production_time: 5223.1875
  idle_worker_time: 11.375
  total_value_units: 8000.0
  total_value_structures: 1850.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 9590.0
  collected_vespene: 2120.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 649.0
  spent_minerals: 9250.0
  spent_vespene: 2075.0
  food_used {
    none: 0.0
    army: 43.5
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 550.0
    technology: -200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2600.0
    economy: 4875.0
    technology: 1200.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2950.0
    economy: 5700.0
    technology: 1100.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 450.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 586.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2892.74365234
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 390
vespene: 45
food_cap: 100
food_used: 90
food_army: 43
food_workers: 47
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 104
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 37
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12142
score_details {
  idle_production_time: 20737.1875
  idle_worker_time: 27.4375
  total_value_units: 23900.0
  total_value_structures: 3825.0
  killed_value_units: 7425.0
  killed_value_structures: 800.0
  collected_minerals: 22540.0
  collected_vespene: 7588.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 806.0
  spent_minerals: 22518.0
  spent_vespene: 7143.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5175.0
    economy: 1200.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1600.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9943.0
    economy: 1900.0
    technology: -200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4593.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 1425.0
    economy: 6175.0
    technology: 1750.0
    upgrade: 800.0
  }
  used_vespene {
    none: -75.0
    army: 100.0
    economy: 0.0
    technology: 800.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 13950.0
    economy: 9125.0
    technology: 2100.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 5525.0
    economy: 0.0
    technology: 1050.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 8700.203125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22676.0917969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 72
vespene: 445
food_cap: 136
food_used: 78
food_army: 23
food_workers: 55
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  19842
ui_data{
 multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  12142
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
