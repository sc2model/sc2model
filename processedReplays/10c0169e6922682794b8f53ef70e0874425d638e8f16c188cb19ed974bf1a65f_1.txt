----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3863
  player_apm: 85
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3952
  player_apm: 95
}
game_duration_loops: 16641
game_duration_seconds: 742.953613281
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11796
score_details {
  idle_production_time: 1535.375
  idle_worker_time: 440.4375
  total_value_units: 4100.0
  total_value_structures: 4850.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8315.0
  collected_vespene: 2756.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 627.0
  spent_minerals: 7900.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 750.0
    economy: 6100.0
    technology: 1375.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 150.0
    technology: 675.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 750.0
    economy: 6600.0
    technology: 1375.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 300.0
    technology: 675.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 161.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 465
vespene: 731
food_cap: 132
food_used: 72
food_army: 18
food_workers: 52
idle_worker_count: 2
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 156
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 87
  }
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 82
  }
}

score{
 score_type: Melee
score: 21784
score_details {
  idle_production_time: 5249.375
  idle_worker_time: 1854.125
  total_value_units: 10350.0
  total_value_structures: 8300.0
  killed_value_units: 4425.0
  killed_value_structures: 0.0
  collected_minerals: 19790.0
  collected_vespene: 5704.0
  collection_rate_minerals: 2939.0
  collection_rate_vespene: 1119.0
  spent_minerals: 15950.0
  spent_vespene: 5100.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 74.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2525.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1775.0
    economy: 637.0
    technology: 24.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: -76.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1775.0
    economy: 7875.0
    technology: 3450.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 1425.0
    economy: 150.0
    technology: 1200.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 3600.0
    economy: 9050.0
    technology: 3450.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 2550.0
    economy: 300.0
    technology: 1200.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 2334.0
    shields: 2741.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6361.90917969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1568.50561523
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3721
vespene: 588
food_cap: 147
food_used: 114
food_army: 40
food_workers: 74
idle_worker_count: 1
army_count: 17
warp_gate_count: 0

game_loop:  16641
ui_data{
 
Score:  21784
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
