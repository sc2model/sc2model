----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2862
  player_apm: 74
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2723
  player_apm: 32
}
game_duration_loops: 20961
game_duration_seconds: 935.82421875
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10191
score_details {
  idle_production_time: 1398.5625
  idle_worker_time: 466.125
  total_value_units: 4275.0
  total_value_structures: 3250.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 8620.0
  collected_vespene: 1696.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 425.0
  spent_minerals: 7075.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1900.0
    economy: 4050.0
    technology: 975.0
    upgrade: 0.0
  }
  used_vespene {
    none: 50.0
    army: 700.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 1950.0
    economy: 4300.0
    technology: 975.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 525.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 500.0
    shields: 251.5
    energy: 0.0
  }
  total_damage_taken {
    life: 903.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1595
vespene: 396
food_cap: 86
food_used: 75
food_army: 38
food_workers: 35
idle_worker_count: 3
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 966
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 24210
score_details {
  idle_production_time: 6104.75
  idle_worker_time: 3338.125
  total_value_units: 15575.0
  total_value_structures: 6525.0
  killed_value_units: 7400.0
  killed_value_structures: 5800.0
  collected_minerals: 21755.0
  collected_vespene: 4780.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 291.0
  spent_minerals: 18900.0
  spent_vespene: 4100.0
  food_used {
    none: 0.0
    army: 155.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4825.0
    economy: 3050.0
    technology: 3300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2500.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 7750.0
    economy: 6400.0
    technology: 2200.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 2950.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 10300.0
    economy: 7000.0
    technology: 2200.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 2975.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 16652.0
    shields: 15461.875
    energy: 0.0
  }
  total_damage_taken {
    life: 5007.65185547
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3607.82617188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2905
vespene: 680
food_cap: 200
food_used: 200
food_army: 155
food_workers: 45
idle_worker_count: 8
army_count: 113
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 966
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 23029
score_details {
  idle_production_time: 6624.3125
  idle_worker_time: 3818.625
  total_value_units: 15950.0
  total_value_structures: 6525.0
  killed_value_units: 8800.0
  killed_value_structures: 7625.0
  collected_minerals: 22920.0
  collected_vespene: 4984.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 268.0
  spent_minerals: 19650.0
  spent_vespene: 4475.0
  food_used {
    none: 0.0
    army: 134.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5350.0
    economy: 4125.0
    technology: 4200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4050.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 6700.0
    economy: 6400.0
    technology: 2200.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 2575.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 10550.0
    economy: 7000.0
    technology: 2200.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 3100.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 20850.0
    shields: 20592.375
    energy: 0.0
  }
  total_damage_taken {
    life: 7312.81396484
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4987.89111328
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3320
vespene: 509
food_cap: 200
food_used: 179
food_army: 134
food_workers: 45
idle_worker_count: 8
army_count: 93
warp_gate_count: 0

game_loop:  20961
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}

Score:  23029
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
