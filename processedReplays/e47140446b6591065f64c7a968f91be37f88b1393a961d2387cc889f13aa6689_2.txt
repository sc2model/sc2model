----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3562
  player_apm: 160
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3485
  player_apm: 117
}
game_duration_loops: 53039
game_duration_seconds: 2367.9777832
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9106
score_details {
  idle_production_time: 1732.1875
  idle_worker_time: 198.3125
  total_value_units: 4775.0
  total_value_structures: 3575.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 7935.0
  collected_vespene: 1968.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 358.0
  spent_minerals: 7775.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 100.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1450.0
    economy: 4250.0
    technology: 1575.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 1800.0
    economy: 4250.0
    technology: 1525.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 925.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 994.034667969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3221.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1317.74414062
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 88
vespene: 143
food_cap: 70
food_used: 68
food_army: 29
food_workers: 39
idle_worker_count: 6
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12281
score_details {
  idle_production_time: 7825.5625
  idle_worker_time: 2847.3125
  total_value_units: 14100.0
  total_value_structures: 6100.0
  killed_value_units: 6775.0
  killed_value_structures: 0.0
  collected_minerals: 17010.0
  collected_vespene: 4728.0
  collection_rate_minerals: 83.0
  collection_rate_vespene: 111.0
  spent_minerals: 16550.0
  spent_vespene: 4150.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5342.0
    economy: 2776.0
    technology: 615.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1642.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2950.0
    economy: 3250.0
    technology: 2425.0
    upgrade: 475.0
  }
  used_vespene {
    none: 50.0
    army: 1300.0
    economy: 0.0
    technology: 725.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 50.0
    army: 8250.0
    economy: 6200.0
    technology: 2925.0
    upgrade: 475.0
  }
  total_used_vespene {
    none: 50.0
    army: 2900.0
    economy: 0.0
    technology: 725.0
    upgrade: 475.0
  }
  total_damage_dealt {
    life: 6821.05273438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16469.9921875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4522.24414062
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 45
vespene: 536
food_cap: 86
food_used: 83
food_army: 57
food_workers: 26
idle_worker_count: 0
army_count: 19
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 5
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16145
score_details {
  idle_production_time: 16800.6875
  idle_worker_time: 3394.4375
  total_value_units: 19625.0
  total_value_structures: 9075.0
  killed_value_units: 18050.0
  killed_value_structures: 625.0
  collected_minerals: 26355.0
  collected_vespene: 6272.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 582.0
  spent_minerals: 25700.0
  spent_vespene: 5350.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13500.0
    economy: 1675.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9442.0
    economy: 3784.0
    technology: 915.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2942.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 2750.0
    economy: 5400.0
    technology: 3825.0
    upgrade: 475.0
  }
  used_vespene {
    none: 50.0
    army: 875.0
    economy: 0.0
    technology: 925.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 50.0
    army: 12150.0
    economy: 9750.0
    technology: 3975.0
    upgrade: 475.0
  }
  total_used_vespene {
    none: 50.0
    army: 3775.0
    economy: 0.0
    technology: 750.0
    upgrade: 475.0
  }
  total_damage_dealt {
    life: 18279.6269531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23768.5390625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5398.19628906
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 240
vespene: 880
food_cap: 148
food_used: 88
food_army: 55
food_workers: 32
idle_worker_count: 0
army_count: 41
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 1
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 9
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 3
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20059
score_details {
  idle_production_time: 26923.8125
  idle_worker_time: 4207.75
  total_value_units: 30475.0
  total_value_structures: 12625.0
  killed_value_units: 26175.0
  killed_value_structures: 700.0
  collected_minerals: 39745.0
  collected_vespene: 10680.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 470.0
  spent_minerals: 37500.0
  spent_vespene: 8300.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 19650.0
    economy: 2025.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5125.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16092.0
    economy: 6859.0
    technology: 2369.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4917.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3100.0
    economy: 5425.0
    technology: 3825.0
    upgrade: 575.0
  }
  used_vespene {
    none: 50.0
    army: 1525.0
    economy: 0.0
    technology: 875.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 50.0
    army: 18500.0
    economy: 12950.0
    technology: 6125.0
    upgrade: 575.0
  }
  total_used_vespene {
    none: 50.0
    army: 6175.0
    economy: 0.0
    technology: 1050.0
    upgrade: 575.0
  }
  total_damage_dealt {
    life: 28236.6191406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 43061.1601562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5831.328125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1758
vespene: 2301
food_cap: 173
food_used: 91
food_army: 55
food_workers: 36
idle_worker_count: 1
army_count: 25
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 35
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 106
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1245
    shields: 0
    energy: 53
  }
  units {
    unit_type: 134
    player_relative: 1
    health: 71
    shields: 0
    energy: 106
  }
}

score{
 score_type: Melee
score: 14796
score_details {
  idle_production_time: 37894.6875
  idle_worker_time: 4608.25
  total_value_units: 37375.0
  total_value_structures: 13925.0
  killed_value_units: 36275.0
  killed_value_structures: 3475.0
  collected_minerals: 43835.0
  collected_vespene: 13152.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 134.0
  spent_minerals: 43200.0
  spent_vespene: 9500.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 26450.0
    economy: 6550.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6025.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 22064.0
    economy: 10367.0
    technology: 2469.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7179.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 450.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1250.0
    economy: 3325.0
    technology: 3725.0
    upgrade: 575.0
  }
  used_vespene {
    none: 50.0
    army: 600.0
    economy: 0.0
    technology: 875.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 50.0
    army: 23350.0
    economy: 14850.0
    technology: 6225.0
    upgrade: 575.0
  }
  total_used_vespene {
    none: 50.0
    army: 7825.0
    economy: 0.0
    technology: 1050.0
    upgrade: 575.0
  }
  total_damage_dealt {
    life: 44524.203125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 55219.5507812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 6389.44677734
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 148
vespene: 3573
food_cap: 142
food_used: 32
food_army: 23
food_workers: 8
idle_worker_count: 3
army_count: 7
warp_gate_count: 0

game_loop:  50000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
production {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1179
    shields: 0
    energy: 55
  }
  build_queue {
    unit_type: 45
    build_progress: 0.121323525906
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 8509
score_details {
  idle_production_time: 39929.0
  idle_worker_time: 4897.3125
  total_value_units: 37475.0
  total_value_structures: 13925.0
  killed_value_units: 37600.0
  killed_value_structures: 4175.0
  collected_minerals: 44465.0
  collected_vespene: 13360.0
  collection_rate_minerals: 167.0
  collection_rate_vespene: 67.0
  spent_minerals: 43200.0
  spent_vespene: 9500.0
  food_used {
    none: 0.0
    army: 3.0
    economy: 5.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 27450.0
    economy: 7175.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6350.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 23214.0
    economy: 12154.0
    technology: 5344.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7679.0
    economy: 0.0
    technology: 1075.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 450.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 150.0
    economy: 1650.0
    technology: 850.0
    upgrade: 575.0
  }
  used_vespene {
    none: 50.0
    army: 100.0
    economy: 0.0
    technology: -50.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 50.0
    army: 23400.0
    economy: 14900.0
    technology: 6225.0
    upgrade: 575.0
  }
  total_used_vespene {
    none: 50.0
    army: 7825.0
    economy: 0.0
    technology: 1050.0
    upgrade: 575.0
  }
  total_damage_dealt {
    life: 47034.203125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 79392.84375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 6502.13964844
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 778
vespene: 3781
food_cap: 71
food_used: 8
food_army: 3
food_workers: 5
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  53039
ui_data{
 
Score:  8509
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
