----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3731
  player_apm: 124
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3727
  player_apm: 198
}
game_duration_loops: 14169
game_duration_seconds: 632.588806152
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7042
score_details {
  idle_production_time: 8298.3125
  idle_worker_time: 1.8125
  total_value_units: 5300.0
  total_value_structures: 1075.0
  killed_value_units: 1725.0
  killed_value_structures: 350.0
  collected_minerals: 6525.0
  collected_vespene: 692.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 268.0
  spent_minerals: 5550.0
  spent_vespene: 575.0
  food_used {
    none: 0.0
    army: 31.5
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 2850.0
    technology: 725.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 100.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2775.0
    economy: 3150.0
    technology: 875.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 5446.58935547
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2946.63671875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1025
vespene: 117
food_cap: 76
food_used: 55
food_army: 31
food_workers: 24
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10482
score_details {
  idle_production_time: 12383.375
  idle_worker_time: 6.4375
  total_value_units: 9300.0
  total_value_structures: 1250.0
  killed_value_units: 4275.0
  killed_value_structures: 500.0
  collected_minerals: 10390.0
  collected_vespene: 1992.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 492.0
  spent_minerals: 10100.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 59.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3750.0
    economy: 450.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2675.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2550.0
    economy: 4225.0
    technology: 975.0
    upgrade: 675.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 100.0
    upgrade: 675.0
  }
  total_used_minerals {
    none: 0.0
    army: 5025.0
    economy: 4425.0
    technology: 1125.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 9515.30273438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6843.95751953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 340
vespene: 292
food_cap: 108
food_used: 94
food_army: 59
food_workers: 35
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  14169
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 171
    shields: 0
    energy: 57
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 68
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 122
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 59
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 64
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 143
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  10482
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
