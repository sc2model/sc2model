----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5022
  player_apm: 225
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4611
  player_apm: 190
}
game_duration_loops: 25737
game_duration_seconds: 1149.05334473
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11810
score_details {
  idle_production_time: 1159.75
  idle_worker_time: 163.75
  total_value_units: 5225.0
  total_value_structures: 4500.0
  killed_value_units: 1000.0
  killed_value_structures: 0.0
  collected_minerals: 9820.0
  collected_vespene: 1992.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 425.0
  spent_minerals: 9650.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2400.0
    economy: 5100.0
    technology: 1900.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 475.0
    economy: 0.0
    technology: 550.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2350.0
    economy: 5600.0
    technology: 1750.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 375.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2165.03564453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 822.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 32.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 218
vespene: 267
food_cap: 101
food_used: 97
food_army: 48
food_workers: 49
idle_worker_count: 0
army_count: 29
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 29
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 27
  count: 1
}
production {
  unit {
    unit_type: 22
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 28223
score_details {
  idle_production_time: 9321.9375
  idle_worker_time: 1861.3125
  total_value_units: 18500.0
  total_value_structures: 10950.0
  killed_value_units: 7875.0
  killed_value_structures: 1475.0
  collected_minerals: 26620.0
  collected_vespene: 8468.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 1097.0
  spent_minerals: 25700.0
  spent_vespene: 5125.0
  food_used {
    none: 0.0
    army: 105.0
    economy: 80.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5025.0
    economy: 2600.0
    technology: 275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5950.0
    economy: 250.0
    technology: 383.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5250.0
    economy: 9300.0
    technology: 4625.0
    upgrade: 850.0
  }
  used_vespene {
    none: 50.0
    army: 1800.0
    economy: 150.0
    technology: 1075.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 50.0
    army: 11250.0
    economy: 10150.0
    technology: 5075.0
    upgrade: 675.0
  }
  total_used_vespene {
    none: 50.0
    army: 3000.0
    economy: 300.0
    technology: 1075.0
    upgrade: 675.0
  }
  total_damage_dealt {
    life: 17284.859375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9850.546875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3267.34521484
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 880
vespene: 3343
food_cap: 200
food_used: 185
food_army: 105
food_workers: 80
idle_worker_count: 5
army_count: 58
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 32
  count: 17
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 13
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 27
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 32439
score_details {
  idle_production_time: 13312.5
  idle_worker_time: 3568.125
  total_value_units: 26975.0
  total_value_structures: 12325.0
  killed_value_units: 20025.0
  killed_value_structures: 2650.0
  collected_minerals: 39840.0
  collected_vespene: 11640.0
  collection_rate_minerals: 2715.0
  collection_rate_vespene: 783.0
  spent_minerals: 36000.0
  spent_vespene: 7550.0
  food_used {
    none: 0.0
    army: 94.0
    economy: 77.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13050.0
    economy: 4825.0
    technology: 475.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4125.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13150.0
    economy: 875.0
    technology: 833.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 4700.0
    economy: 10175.0
    technology: 4925.0
    upgrade: 950.0
  }
  used_vespene {
    none: 50.0
    army: 1525.0
    economy: 300.0
    technology: 950.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 50.0
    army: 17800.0
    economy: 11000.0
    technology: 5825.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 50.0
    army: 4925.0
    economy: 600.0
    technology: 1150.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 37666.75
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24123.1132812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3651.54638672
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3774
vespene: 4090
food_cap: 200
food_used: 171
food_army: 94
food_workers: 77
idle_worker_count: 4
army_count: 52
warp_gate_count: 0

game_loop:  25737
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 14
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 13
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 27
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 836
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 647
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 360
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 994
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 399
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 995
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 617
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 290
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 978
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 312
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  32439
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
