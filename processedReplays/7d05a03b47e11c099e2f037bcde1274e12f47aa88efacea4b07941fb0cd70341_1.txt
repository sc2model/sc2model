----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2755
  player_apm: 79
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 71
}
game_duration_loops: 31446
game_duration_seconds: 1403.93725586
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6157
score_details {
  idle_production_time: 1257.875
  idle_worker_time: 1055.625
  total_value_units: 4050.0
  total_value_structures: 2750.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 5660.0
  collected_vespene: 1072.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 179.0
  spent_minerals: 5425.0
  spent_vespene: 825.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 1000.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 2700.0
    technology: 750.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 425.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 3850.0
    technology: 800.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 793.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1820.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 285
vespene: 247
food_cap: 61
food_used: 50
food_army: 30
food_workers: 20
idle_worker_count: 3
army_count: 19
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13680
score_details {
  idle_production_time: 5695.0625
  idle_worker_time: 3084.25
  total_value_units: 10650.0
  total_value_structures: 5625.0
  killed_value_units: 2425.0
  killed_value_structures: 100.0
  collected_minerals: 15905.0
  collected_vespene: 3400.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 425.0
  spent_minerals: 15800.0
  spent_vespene: 3125.0
  food_used {
    none: 0.0
    army: 56.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1325.0
    economy: 650.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2900.0
    economy: 1550.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1000.0
    economy: 150.0
    technology: 75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2700.0
    economy: 6200.0
    technology: 1950.0
    upgrade: 475.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 550.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 0.0
    army: 5150.0
    economy: 7400.0
    technology: 2000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1700.0
    economy: 300.0
    technology: 625.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3873.98632812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6621.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 35.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 155
vespene: 275
food_cap: 116
food_used: 114
food_army: 56
food_workers: 56
idle_worker_count: 3
army_count: 32
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
production {
  unit {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0294117927551
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 22266
score_details {
  idle_production_time: 14318.5625
  idle_worker_time: 11631.1875
  total_value_units: 22950.0
  total_value_structures: 11525.0
  killed_value_units: 7175.0
  killed_value_structures: 400.0
  collected_minerals: 33685.0
  collected_vespene: 7020.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 694.0
  spent_minerals: 33512.0
  spent_vespene: 6175.0
  food_used {
    none: 0.0
    army: 70.0
    economy: 71.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4675.0
    economy: 1000.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12475.0
    economy: 1637.0
    technology: 325.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2850.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3575.0
    economy: 8700.0
    technology: 5400.0
    upgrade: 900.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 300.0
    technology: 1250.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 0.0
    army: 15400.0
    economy: 11200.0
    technology: 5400.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 0.0
    army: 2950.0
    economy: 900.0
    technology: 1325.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 11421.1464844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19314.2480469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 210.995605469
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 221
vespene: 845
food_cap: 200
food_used: 141
food_army: 70
food_workers: 71
idle_worker_count: 16
army_count: 53
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 9
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 3
}
production {
  unit {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  build_queue {
    unit_type: 48
    build_progress: 0.337499976158
  }
  build_queue {
    unit_type: 48
    build_progress: 0.172500014305
  }
  build_queue {
    unit_type: 48
    build_progress: 0.0
  }
  build_queue {
    unit_type: 48
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 21352
score_details {
  idle_production_time: 16336.625
  idle_worker_time: 13149.3125
  total_value_units: 25125.0
  total_value_structures: 12550.0
  killed_value_units: 10025.0
  killed_value_structures: 400.0
  collected_minerals: 35780.0
  collected_vespene: 7664.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 492.0
  spent_minerals: 33912.0
  spent_vespene: 6175.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6275.0
    economy: 1050.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16775.0
    economy: 2337.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3075.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 525.0
    economy: 8000.0
    technology: 5775.0
    upgrade: 900.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 300.0
    technology: 1250.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 0.0
    army: 17350.0
    economy: 11200.0
    technology: 6300.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 0.0
    army: 3175.0
    economy: 900.0
    technology: 1450.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 14260.1464844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27399.7480469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 259.440917969
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1913
vespene: 1489
food_cap: 194
food_used: 72
food_army: 10
food_workers: 62
idle_worker_count: 18
army_count: 6
warp_gate_count: 0

game_loop:  31446
ui_data{
 
Score:  21352
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
