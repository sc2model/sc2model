----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3470
  player_apm: 166
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3445
  player_apm: 53
}
game_duration_loops: 8623
game_duration_seconds: 384.982208252
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5199
score_details {
  idle_production_time: 2177.3125
  idle_worker_time: 92.75
  total_value_units: 3750.0
  total_value_structures: 1075.0
  killed_value_units: 875.0
  killed_value_structures: 0.0
  collected_minerals: 4545.0
  collected_vespene: 1204.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 313.0
  spent_minerals: 4175.0
  spent_vespene: 800.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 675.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 75.0
    economy: 2100.0
    technology: 1125.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 150.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 750.0
    economy: 3300.0
    technology: 1275.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 250.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1374.71337891
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2226.65478516
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 420
vespene: 404
food_cap: 62
food_used: 20
food_army: 2
food_workers: 18
idle_worker_count: 18
army_count: 1
warp_gate_count: 0

game_loop:  8623
ui_data{
 
Score:  5199
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
