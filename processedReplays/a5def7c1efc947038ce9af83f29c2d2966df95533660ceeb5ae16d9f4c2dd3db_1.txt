----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2920
  player_apm: 74
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2728
  player_apm: 170
}
game_duration_loops: 6615
game_duration_seconds: 295.333099365
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5955
score_details {
  idle_production_time: 377.4375
  idle_worker_time: 212.375
  total_value_units: 2600.0
  total_value_structures: 1800.0
  killed_value_units: 1400.0
  killed_value_structures: 0.0
  collected_minerals: 4025.0
  collected_vespene: 1180.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 313.0
  spent_minerals: 4050.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1375.0
    economy: 2800.0
    technology: 700.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 975.0
    economy: 2550.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2781.97509766
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 248.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 79.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 25
vespene: 230
food_cap: 55
food_used: 54
food_army: 28
food_workers: 26
idle_worker_count: 2
army_count: 14
warp_gate_count: 0

game_loop:  6615
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
single {
  unit {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  5955
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
