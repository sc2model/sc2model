----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3452
  player_apm: 198
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3451
  player_apm: 81
}
game_duration_loops: 5439
game_duration_seconds: 242.829437256
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3773
score_details {
  idle_production_time: 260.5
  idle_worker_time: 11.8125
  total_value_units: 1850.0
  total_value_structures: 1200.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 2015.0
  collected_vespene: 908.0
  collection_rate_minerals: 503.0
  collection_rate_vespene: 313.0
  spent_minerals: 1850.0
  spent_vespene: 750.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 1650.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 550.0
    economy: 1800.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2016.95996094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 264.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 215
vespene: 158
food_cap: 31
food_used: 28
food_army: 13
food_workers: 15
idle_worker_count: 0
army_count: 10
warp_gate_count: 0

game_loop:  5439
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 49
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}

Score:  3773
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
