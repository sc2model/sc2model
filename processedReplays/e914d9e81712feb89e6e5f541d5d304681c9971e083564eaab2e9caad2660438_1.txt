----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4215
  player_apm: 114
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 134
}
game_duration_loops: 6186
game_duration_seconds: 276.179992676
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4502
score_details {
  idle_production_time: 1284.25
  idle_worker_time: 531.0625
  total_value_units: 2750.0
  total_value_structures: 700.0
  killed_value_units: 950.0
  killed_value_structures: 100.0
  collected_minerals: 3090.0
  collected_vespene: 812.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 246.0
  spent_minerals: 2825.0
  spent_vespene: 725.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 400.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 725.0
    economy: 2400.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1025.0
    economy: 2400.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2220.07568359
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 677.983398438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 315
vespene: 87
food_cap: 46
food_used: 40
food_army: 19
food_workers: 21
idle_worker_count: 1
army_count: 5
warp_gate_count: 0

game_loop:  6186
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 688
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  4502
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
