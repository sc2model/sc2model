----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2766
  player_apm: 88
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2661
  player_apm: 32
}
game_duration_loops: 17738
game_duration_seconds: 791.930236816
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11991
score_details {
  idle_production_time: 1524.25
  idle_worker_time: 128.6875
  total_value_units: 4700.0
  total_value_structures: 3450.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 9280.0
  collected_vespene: 2136.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 447.0
  spent_minerals: 8525.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2300.0
    economy: 5025.0
    technology: 1350.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 475.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 1800.0
    economy: 4925.0
    technology: 1200.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 400.0
    economy: 0.0
    technology: 325.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 60.0
    shields: 148.0
    energy: 0.0
  }
  total_damage_taken {
    life: 114.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 805
vespene: 711
food_cap: 110
food_used: 97
food_army: 47
food_workers: 50
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 22
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 2
}
production {
  unit {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  build_queue {
    unit_type: 53
    build_progress: 0.0187500119209
  }
  build_queue {
    unit_type: 53
    build_progress: 0.0124999880791
  }
}

score{
 score_type: Melee
score: 7301
score_details {
  idle_production_time: 6295.375
  idle_worker_time: 3221.8125
  total_value_units: 10125.0
  total_value_structures: 6525.0
  killed_value_units: 3525.0
  killed_value_structures: 675.0
  collected_minerals: 16645.0
  collected_vespene: 3240.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 15200.0
  spent_vespene: 2250.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 3.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1950.0
    economy: 1425.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5725.0
    economy: 5499.0
    technology: 800.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 175.0
    economy: 1975.0
    technology: 1350.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 125.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 5900.0
    economy: 8000.0
    technology: 2050.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 1225.0
    economy: 0.0
    technology: 575.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 3658.625
    shields: 4513.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25788.8535156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2424.82617188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1461
vespene: 990
food_cap: 77
food_used: 7
food_army: 4
food_workers: 3
idle_worker_count: 2
army_count: 2
warp_gate_count: 0

game_loop:  17738
ui_data{
 
Score:  7301
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
