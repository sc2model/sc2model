----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3317
  player_apm: 118
}
game_duration_loops: 20643
game_duration_seconds: 921.626831055
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10894
score_details {
  idle_production_time: 9961.75
  idle_worker_time: 1.1875
  total_value_units: 6550.0
  total_value_structures: 1300.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 8485.0
  collected_vespene: 1784.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 694.0
  spent_minerals: 7750.0
  spent_vespene: 1600.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2650.0
    economy: 4300.0
    technology: 825.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 50.0
    technology: 200.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 4625.0
    technology: 975.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 100.0
    technology: 300.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 55.0
    shields: 390.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1161.69287109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 785
vespene: 184
food_cap: 100
food_used: 87
food_army: 51
food_workers: 36
idle_worker_count: 0
army_count: 41
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16033
score_details {
  idle_production_time: 43658.875
  idle_worker_time: 937.3125
  total_value_units: 19200.0
  total_value_structures: 2300.0
  killed_value_units: 9250.0
  killed_value_structures: 1500.0
  collected_minerals: 20375.0
  collected_vespene: 6508.0
  collection_rate_minerals: 1551.0
  collection_rate_vespene: 515.0
  spent_minerals: 18525.0
  spent_vespene: 4925.0
  food_used {
    none: 0.0
    army: 40.5
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6075.0
    economy: 2200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8375.0
    economy: 375.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2250.0
    economy: 6400.0
    technology: 1225.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 50.0
    technology: 450.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 10175.0
    economy: 7625.0
    technology: 1725.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 3425.0
    economy: 100.0
    technology: 700.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 7840.76074219
    shields: 9412.44042969
    energy: 0.0
  }
  total_damage_taken {
    life: 12982.3632812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1900
vespene: 1583
food_cap: 200
food_used: 86
food_army: 40
food_workers: 46
idle_worker_count: 11
army_count: 27
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 101
  count: 1
}
multi {
  units {
    unit_type: 106
    player_relative: 1
    health: 142
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 693
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 693
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 693
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 173
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 185
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 693
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 128
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
    add_on {
      unit_type: 129
      build_progress: 0.877457141876
    }
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 128
    player_relative: 1
    health: 181
    shields: 0
    energy: 0
    add_on {
      unit_type: 129
      build_progress: 0.903705894947
    }
  }
}

score{
 score_type: Melee
score: 14611
score_details {
  idle_production_time: 44786.5625
  idle_worker_time: 1379.375
  total_value_units: 19650.0
  total_value_structures: 2300.0
  killed_value_units: 9600.0
  killed_value_structures: 1500.0
  collected_minerals: 20937.0
  collected_vespene: 6624.0
  collection_rate_minerals: 1019.0
  collection_rate_vespene: 156.0
  spent_minerals: 18625.0
  spent_vespene: 4975.0
  food_used {
    none: 0.0
    army: 26.5
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6325.0
    economy: 2200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9100.0
    economy: 1525.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1625.0
    economy: 5250.0
    technology: 1225.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 50.0
    technology: 450.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 10725.0
    economy: 7625.0
    technology: 1725.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 3725.0
    economy: 100.0
    technology: 700.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 7932.76074219
    shields: 9736.31542969
    energy: 0.0
  }
  total_damage_taken {
    life: 17130.2910156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2362
vespene: 1649
food_cap: 186
food_used: 58
food_army: 26
food_workers: 32
idle_worker_count: 11
army_count: 11
warp_gate_count: 0

game_loop:  20643
ui_data{
 
Score:  14611
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
