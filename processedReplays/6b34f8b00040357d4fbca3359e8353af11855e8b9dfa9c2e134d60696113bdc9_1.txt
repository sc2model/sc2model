----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3122
  player_apm: 47
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3143
  player_apm: 130
}
game_duration_loops: 10456
game_duration_seconds: 466.81829834
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7827
score_details {
  idle_production_time: 1474.0625
  idle_worker_time: 725.0625
  total_value_units: 3900.0
  total_value_structures: 3300.0
  killed_value_units: 1150.0
  killed_value_structures: 100.0
  collected_minerals: 6395.0
  collected_vespene: 2432.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 537.0
  spent_minerals: 6050.0
  spent_vespene: 450.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 500.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 2950.0
    technology: 1400.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 3600.0
    technology: 1400.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2518.27490234
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2304.10839844
    shields: 2724.09179688
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 395
vespene: 1982
food_cap: 62
food_used: 39
food_army: 10
food_workers: 29
idle_worker_count: 7
army_count: 5
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 8
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 9
  leader_unit_type: 81
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 358
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 119
    shields: 0
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 437
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 458
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 311
    energy: 0
  }
}

score{
 score_type: Melee
score: 6761
score_details {
  idle_production_time: 1577.875
  idle_worker_time: 974.25
  total_value_units: 4000.0
  total_value_structures: 3300.0
  killed_value_units: 1550.0
  killed_value_structures: 200.0
  collected_minerals: 6425.0
  collected_vespene: 2536.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 291.0
  spent_minerals: 6150.0
  spent_vespene: 450.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 975.0
    economy: 500.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1800.0
    economy: 1150.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 2400.0
    technology: 1250.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 3600.0
    technology: 1400.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2910.45703125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3463.35839844
    shields: 3924.09179688
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 325
vespene: 2086
food_cap: 46
food_used: 24
food_army: 2
food_workers: 22
idle_worker_count: 22
army_count: 1
warp_gate_count: 0

game_loop:  10456
ui_data{
 
Score:  6761
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
