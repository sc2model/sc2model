----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3722
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3506
  player_apm: 246
}
game_duration_loops: 21265
game_duration_seconds: 949.396606445
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11953
score_details {
  idle_production_time: 1145.75
  idle_worker_time: 281.8125
  total_value_units: 4950.0
  total_value_structures: 3575.0
  killed_value_units: 250.0
  killed_value_structures: 0.0
  collected_minerals: 9140.0
  collected_vespene: 1912.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 559.0
  spent_minerals: 8212.0
  spent_vespene: 1462.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2150.0
    economy: 5200.0
    technology: 1275.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 375.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5150.0
    technology: 1000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 375.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 405.821777344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 238.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 978
vespene: 450
food_cap: 101
food_used: 90
food_army: 43
food_workers: 47
idle_worker_count: 1
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 55
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 49
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 101
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 128
  }
}

score{
 score_type: Melee
score: 16269
score_details {
  idle_production_time: 7643.6875
  idle_worker_time: 3510.8125
  total_value_units: 19500.0
  total_value_structures: 7150.0
  killed_value_units: 11050.0
  killed_value_structures: 500.0
  collected_minerals: 23900.0
  collected_vespene: 5900.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 403.0
  spent_minerals: 23662.0
  spent_vespene: 5387.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7600.0
    economy: 100.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9786.0
    economy: 1200.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2661.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3050.0
    economy: 7000.0
    technology: 2400.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 600.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 12200.0
    economy: 8800.0
    technology: 2400.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 3850.0
    economy: 0.0
    technology: 600.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 16748.5078125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15973.7929688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5134.47070312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 124
vespene: 370
food_cap: 200
food_used: 108
food_army: 57
food_workers: 51
idle_worker_count: 23
army_count: 23
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 80
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 105
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 25
    shields: 0
    energy: 174
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 20
    shields: 0
    energy: 150
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 106
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 14938
score_details {
  idle_production_time: 8684.375
  idle_worker_time: 4613.4375
  total_value_units: 21550.0
  total_value_structures: 7150.0
  killed_value_units: 12425.0
  killed_value_structures: 500.0
  collected_minerals: 25275.0
  collected_vespene: 6344.0
  collection_rate_minerals: 2827.0
  collection_rate_vespene: 604.0
  spent_minerals: 24862.0
  spent_vespene: 5512.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8725.0
    economy: 100.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12643.0
    economy: 1325.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3768.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1500.0
    economy: 7325.0
    technology: 2400.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 600.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 13600.0
    economy: 9100.0
    technology: 2400.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 4200.0
    economy: 0.0
    technology: 600.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 18450.4902344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21019.421875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5613.29785156
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 299
vespene: 689
food_cap: 200
food_used: 84
food_army: 28
food_workers: 56
idle_worker_count: 8
army_count: 7
warp_gate_count: 0

game_loop:  21265
ui_data{
 multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  14938
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
