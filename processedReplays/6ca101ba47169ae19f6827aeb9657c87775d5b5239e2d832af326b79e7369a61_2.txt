----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3237
  player_apm: 82
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3253
  player_apm: 95
}
game_duration_loops: 12170
game_duration_seconds: 543.341491699
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11233
score_details {
  idle_production_time: 1427.625
  idle_worker_time: 354.75
  total_value_units: 5200.0
  total_value_structures: 4000.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8445.0
  collected_vespene: 1788.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 313.0
  spent_minerals: 7725.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2625.0
    economy: 3750.0
    technology: 1850.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2625.0
    economy: 3800.0
    technology: 1850.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24.0
    shields: 40.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 770
vespene: 413
food_cap: 110
food_used: 91
food_army: 55
food_workers: 36
idle_worker_count: 1
army_count: 28
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 12706
score_details {
  idle_production_time: 2125.0
  idle_worker_time: 490.875
  total_value_units: 6600.0
  total_value_structures: 4000.0
  killed_value_units: 3400.0
  killed_value_structures: 1000.0
  collected_minerals: 10965.0
  collected_vespene: 2316.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 335.0
  spent_minerals: 9425.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2150.0
    economy: 1525.0
    technology: 575.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2825.0
    economy: 3700.0
    technology: 1850.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 200.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 4025.0
    economy: 3800.0
    technology: 1850.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 12238.2470703
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1451.125
    shields: 2361.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1590
vespene: 641
food_cap: 110
food_used: 92
food_army: 57
food_workers: 35
idle_worker_count: 1
army_count: 29
warp_gate_count: 7

game_loop:  12170
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
}

Score:  12706
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
