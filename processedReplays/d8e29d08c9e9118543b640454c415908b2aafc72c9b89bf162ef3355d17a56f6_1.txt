----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3870
  player_apm: 223
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3802
  player_apm: 159
}
game_duration_loops: 16386
game_duration_seconds: 731.568908691
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12069
score_details {
  idle_production_time: 7745.75
  idle_worker_time: 35.3125
  total_value_units: 7850.0
  total_value_structures: 2350.0
  killed_value_units: 550.0
  killed_value_structures: 0.0
  collected_minerals: 10360.0
  collected_vespene: 1484.0
  collection_rate_minerals: 2295.0
  collection_rate_vespene: 627.0
  spent_minerals: 9725.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 38.5
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 725.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2350.0
    economy: 5225.0
    technology: 1725.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 3050.0
    economy: 6225.0
    technology: 1875.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1811.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1304.35400391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 685
vespene: 259
food_cap: 106
food_used: 92
food_army: 38
food_workers: 54
idle_worker_count: 0
army_count: 53
warp_gate_count: 0
larva_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 32
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16670
score_details {
  idle_production_time: 33042.4375
  idle_worker_time: 140.8125
  total_value_units: 17450.0
  total_value_structures: 3175.0
  killed_value_units: 6875.0
  killed_value_structures: 0.0
  collected_minerals: 19515.0
  collected_vespene: 5280.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 806.0
  spent_minerals: 18425.0
  spent_vespene: 3900.0
  food_used {
    none: 0.0
    army: 36.5
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4300.0
    economy: 1600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5225.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2175.0
    economy: 6375.0
    technology: 2525.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 25.0
    technology: 600.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 10950.0
    economy: 7800.0
    technology: 2475.0
    upgrade: 975.0
  }
  total_used_vespene {
    none: 0.0
    army: 2300.0
    economy: 50.0
    technology: 550.0
    upgrade: 975.0
  }
  total_damage_dealt {
    life: 9856.265625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7442.79345703
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1140
vespene: 1380
food_cap: 144
food_used: 96
food_army: 36
food_workers: 60
idle_worker_count: 0
army_count: 40
warp_gate_count: 0

game_loop:  16386
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 32
}
groups {
  control_group_index: 3
  leader_unit_type: 893
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
}

Score:  16670
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
