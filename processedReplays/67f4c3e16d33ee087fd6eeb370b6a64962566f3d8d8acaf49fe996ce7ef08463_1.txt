----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3280
  player_apm: 39
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2480
  player_apm: 47
}
game_duration_loops: 8429
game_duration_seconds: 376.320892334
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6576
score_details {
  idle_production_time: 1755.5
  idle_worker_time: 504.6875
  total_value_units: 2325.0
  total_value_structures: 2450.0
  killed_value_units: 500.0
  killed_value_structures: 400.0
  collected_minerals: 4860.0
  collected_vespene: 1416.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 268.0
  spent_minerals: 4225.0
  spent_vespene: 850.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 2650.0
    technology: 925.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 275.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 750.0
    economy: 2850.0
    technology: 925.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 275.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 845.3671875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1014.96679688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 685
vespene: 566
food_cap: 54
food_used: 38
food_army: 13
food_workers: 25
idle_worker_count: 3
army_count: 8
warp_gate_count: 0

game_loop:  8429
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 55
    player_relative: 1
    health: 134
    shields: 0
    energy: 65
  }
}

Score:  6576
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
