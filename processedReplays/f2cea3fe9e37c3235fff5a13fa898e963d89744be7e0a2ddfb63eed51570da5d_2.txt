----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 58
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2956
  player_apm: 60
}
game_duration_loops: 8469
game_duration_seconds: 378.106750488
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4565
score_details {
  idle_production_time: 962.375
  idle_worker_time: 206.125
  total_value_units: 3025.0
  total_value_structures: 2300.0
  killed_value_units: 950.0
  killed_value_structures: 0.0
  collected_minerals: 5395.0
  collected_vespene: 1288.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 246.0
  spent_minerals: 4775.0
  spent_vespene: 725.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 909.0
    economy: 1700.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 134.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 75.0
    economy: 2050.0
    technology: 650.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 975.0
    economy: 3650.0
    technology: 800.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 435.5
    shields: 554.5
    energy: 0.0
  }
  total_damage_taken {
    life: 5212.45263672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 31.4526367188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 661
vespene: 554
food_cap: 15
food_used: 21
food_army: 2
food_workers: 19
idle_worker_count: 0
army_count: 1
warp_gate_count: 0

game_loop:  8469
ui_data{
 multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  4565
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
