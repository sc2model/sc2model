----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2810
  player_apm: 81
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2405
  player_apm: 69
}
game_duration_loops: 18380
game_duration_seconds: 820.592956543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8637
score_details {
  idle_production_time: 1515.9375
  idle_worker_time: 147.25
  total_value_units: 3300.0
  total_value_structures: 2575.0
  killed_value_units: 1625.0
  killed_value_structures: 75.0
  collected_minerals: 5810.0
  collected_vespene: 2020.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 313.0
  spent_minerals: 4987.0
  spent_vespene: 1531.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 1075.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1600.0
    economy: 2750.0
    technology: 1025.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 400.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 1600.0
    economy: 2500.0
    technology: 1025.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 400.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 3076.625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 233.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 268.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 873
vespene: 489
food_cap: 55
food_used: 55
food_army: 32
food_workers: 23
idle_worker_count: 1
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 25
    shields: 0
    energy: 71
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 88
    shields: 0
    energy: 72
  }
}

score{
 score_type: Melee
score: 5751
score_details {
  idle_production_time: 3720.4375
  idle_worker_time: 2976.5625
  total_value_units: 7275.0
  total_value_structures: 3175.0
  killed_value_units: 6300.0
  killed_value_structures: 4475.0
  collected_minerals: 8770.0
  collected_vespene: 3124.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 8062.0
  spent_vespene: 2706.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3250.0
    economy: 4850.0
    technology: 1525.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2800.0
    economy: 1950.0
    technology: 887.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 331.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1450.0
    economy: 1200.0
    technology: 100.0
    upgrade: 575.0
  }
  used_vespene {
    none: 50.0
    army: 525.0
    economy: 0.0
    technology: 50.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 50.0
    army: 4250.0
    economy: 3300.0
    technology: 1025.0
    upgrade: 575.0
  }
  total_used_vespene {
    none: 50.0
    army: 1675.0
    economy: 0.0
    technology: 400.0
    upgrade: 575.0
  }
  total_damage_dealt {
    life: 24189.3417969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15293.9726562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 959.146484375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 758
vespene: 418
food_cap: 55
food_used: 29
food_army: 29
food_workers: 0
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  18380
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 134
  count: 1
}
cargo {
  unit {
    unit_type: 54
    player_relative: 1
    health: 128
    shields: 0
    energy: 188
    transport_slots_taken: 4
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  slots_available: 4
}

Score:  5751
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
