----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3391
  player_apm: 76
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3325
  player_apm: 130
}
game_duration_loops: 9447
game_duration_seconds: 421.770507812
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4620
score_details {
  idle_production_time: 1137.1875
  idle_worker_time: 372.6875
  total_value_units: 3375.0
  total_value_structures: 2325.0
  killed_value_units: 1400.0
  killed_value_structures: 0.0
  collected_minerals: 5185.0
  collected_vespene: 1408.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 67.0
  spent_minerals: 4481.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1175.0
    economy: 804.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 75.0
    economy: 2575.0
    technology: 450.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1175.0
    economy: 3675.0
    technology: 650.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1255.62207031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7202.25097656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1765.7331543
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 662
vespene: 533
food_cap: 54
food_used: 21
food_army: 2
food_workers: 19
idle_worker_count: 19
army_count: 0
warp_gate_count: 0

game_loop:  9447
ui_data{
 
Score:  4620
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
