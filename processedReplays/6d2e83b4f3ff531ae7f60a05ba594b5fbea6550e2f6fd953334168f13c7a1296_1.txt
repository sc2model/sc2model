----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4185
  player_apm: 256
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3952
  player_apm: 123
}
game_duration_loops: 12484
game_duration_seconds: 557.360351562
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10646
score_details {
  idle_production_time: 988.25
  idle_worker_time: 180.375
  total_value_units: 6275.0
  total_value_structures: 3525.0
  killed_value_units: 650.0
  killed_value_structures: 0.0
  collected_minerals: 8810.0
  collected_vespene: 2136.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 604.0
  spent_minerals: 8775.0
  spent_vespene: 1800.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2200.0
    economy: 5250.0
    technology: 1075.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 750.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 2850.0
    economy: 4950.0
    technology: 1075.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 1075.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 700.124023438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 943.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 85
vespene: 336
food_cap: 102
food_used: 93
food_army: 44
food_workers: 47
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 21
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 268
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12403
score_details {
  idle_production_time: 1687.9375
  idle_worker_time: 478.5625
  total_value_units: 8525.0
  total_value_structures: 4025.0
  killed_value_units: 2750.0
  killed_value_structures: 1150.0
  collected_minerals: 11755.0
  collected_vespene: 2748.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 335.0
  spent_minerals: 11575.0
  spent_vespene: 2350.0
  food_used {
    none: 0.0
    army: 63.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2150.0
    economy: 400.0
    technology: 700.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1450.0
    economy: 1100.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3150.0
    economy: 5200.0
    technology: 1075.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 1200.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 4000.0
    economy: 6600.0
    technology: 1075.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 1325.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 9299.62304688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3517.76757812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 672.930419922
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 230
vespene: 398
food_cap: 125
food_used: 108
food_army: 63
food_workers: 42
idle_worker_count: 3
army_count: 39
warp_gate_count: 0

game_loop:  12484
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 17
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}

Score:  12403
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
