----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 6667
  player_apm: 285
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 6417
  player_apm: 236
}
game_duration_loops: 26120
game_duration_seconds: 1166.15283203
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14611
score_details {
  idle_production_time: 869.125
  idle_worker_time: 71.5
  total_value_units: 6025.0
  total_value_structures: 4350.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 11285.0
  collected_vespene: 2576.0
  collection_rate_minerals: 2855.0
  collection_rate_vespene: 671.0
  spent_minerals: 11200.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 70.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1900.0
    economy: 6600.0
    technology: 2900.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 400.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 6250.0
    technology: 1250.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 309.0
    shields: 400.0
    energy: 0.0
  }
  total_damage_taken {
    life: 690.0
    shields: 740.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 135
vespene: 551
food_cap: 141
food_used: 105
food_army: 35
food_workers: 67
idle_worker_count: 0
army_count: 15
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 74
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 10
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 65
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 84
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 446
    shields: 446
    energy: 0
    build_progress: 0.880769252777
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 444
    shields: 444
    energy: 0
    build_progress: 0.875
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 360
    shields: 360
    energy: 0
    build_progress: 0.68846154213
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 403
    shields: 403
    energy: 0
    build_progress: 0.783653855324
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 330
    shields: 330
    energy: 0
    build_progress: 0.623076915741
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 249
    shields: 249
    energy: 0
    build_progress: 0.442307710648
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 228
    shields: 228
    energy: 0
    build_progress: 0.395192325115
  }
}

score{
 score_type: Melee
score: 38149
score_details {
  idle_production_time: 5577.125
  idle_worker_time: 333.1875
  total_value_units: 21965.0
  total_value_structures: 14100.0
  killed_value_units: 6775.0
  killed_value_structures: 1350.0
  collected_minerals: 32070.0
  collected_vespene: 12344.0
  collection_rate_minerals: 2995.0
  collection_rate_vespene: 1276.0
  spent_minerals: 30010.0
  spent_vespene: 10725.0
  food_used {
    none: 0.0
    army: 120.0
    economy: 78.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5625.0
    economy: 1250.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5875.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 825.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6620.0
    economy: 9200.0
    technology: 7050.0
    upgrade: 2150.0
  }
  used_vespene {
    none: 0.0
    army: 5700.0
    economy: 0.0
    technology: 1550.0
    upgrade: 2150.0
  }
  total_used_minerals {
    none: 0.0
    army: 14090.0
    economy: 9400.0
    technology: 7050.0
    upgrade: 1600.0
  }
  total_used_vespene {
    none: 0.0
    army: 9675.0
    economy: 0.0
    technology: 1550.0
    upgrade: 1600.0
  }
  total_damage_dealt {
    life: 8075.98339844
    shields: 9122.12792969
    energy: 0.0
  }
  total_damage_taken {
    life: 6908.0
    shields: 8117.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2110
vespene: 1619
food_cap: 200
food_used: 198
food_army: 120
food_workers: 78
idle_worker_count: 0
army_count: 29
warp_gate_count: 12

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 10
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 17
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 12
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 63
  count: 5
}
groups {
  control_group_index: 9
  leader_unit_type: 81
  count: 1
}
multi {
  units {
    unit_type: 72
    player_relative: 1
    health: 550
    shields: 550
    energy: 0
  }
  units {
    unit_type: 65
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 63
    player_relative: 1
    health: 400
    shields: 400
    energy: 0
  }
  units {
    unit_type: 63
    player_relative: 1
    health: 400
    shields: 400
    energy: 0
  }
  units {
    unit_type: 72
    player_relative: 1
    health: 550
    shields: 550
    energy: 0
  }
}

score{
 score_type: Melee
score: 41266
score_details {
  idle_production_time: 7165.6875
  idle_worker_time: 519.0625
  total_value_units: 33120.0
  total_value_structures: 17400.0
  killed_value_units: 24425.0
  killed_value_structures: 4475.0
  collected_minerals: 44400.0
  collected_vespene: 18796.0
  collection_rate_minerals: 2491.0
  collection_rate_vespene: 1545.0
  spent_minerals: 42080.0
  spent_vespene: 14400.0
  food_used {
    none: 0.0
    army: 86.0
    economy: 78.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13800.0
    economy: 4775.0
    technology: 1650.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8575.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14670.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 825.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5125.0
    economy: 9950.0
    technology: 9600.0
    upgrade: 2925.0
  }
  used_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 1550.0
    upgrade: 2925.0
  }
  total_used_minerals {
    none: 0.0
    army: 22570.0
    economy: 10150.0
    technology: 9600.0
    upgrade: 2625.0
  }
  total_used_vespene {
    none: 0.0
    army: 13700.0
    economy: 0.0
    technology: 1550.0
    upgrade: 2625.0
  }
  total_damage_dealt {
    life: 19860.4824219
    shields: 28999.7539062
    energy: 0.0
  }
  total_damage_taken {
    life: 14440.8564453
    shields: 19900.3222656
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2370
vespene: 4396
food_cap: 200
food_used: 164
food_army: 86
food_workers: 78
idle_worker_count: 0
army_count: 36
warp_gate_count: 27

game_loop:  26120
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 30
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 27
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 63
  count: 5
}
groups {
  control_group_index: 9
  leader_unit_type: 495
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 48
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 59
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 52
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 73
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  41266
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
