----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3767
  player_apm: 201
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3680
  player_apm: 166
}
game_duration_loops: 10217
game_duration_seconds: 456.147888184
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9739
score_details {
  idle_production_time: 5437.0625
  idle_worker_time: 25.625
  total_value_units: 9150.0
  total_value_structures: 1750.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 10585.0
  collected_vespene: 1904.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 470.0
  spent_minerals: 10475.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2150.0
    economy: 1150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1425.0
    economy: 5350.0
    technology: 1000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2825.0
    economy: 7150.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1780.63134766
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5785.12988281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 160
vespene: 329
food_cap: 138
food_used: 81
food_army: 34
food_workers: 47
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 15
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9142
score_details {
  idle_production_time: 5540.6875
  idle_worker_time: 25.625
  total_value_units: 9850.0
  total_value_structures: 1750.0
  killed_value_units: 900.0
  killed_value_structures: 0.0
  collected_minerals: 10850.0
  collected_vespene: 1992.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 515.0
  spent_minerals: 10850.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2600.0
    economy: 1500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 5000.0
    technology: 1000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 3350.0
    economy: 7150.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2046.26123047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6893.57763672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 50
vespene: 292
food_cap: 138
food_used: 72
food_army: 32
food_workers: 40
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  10217
ui_data{
 multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 122
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 116
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
}

Score:  9142
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
