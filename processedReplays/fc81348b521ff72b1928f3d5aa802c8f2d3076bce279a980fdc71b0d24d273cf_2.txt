----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3819
  player_apm: 158
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3811
  player_apm: 146
}
game_duration_loops: 18786
game_duration_seconds: 838.719238281
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10479
score_details {
  idle_production_time: 9207.75
  idle_worker_time: 63.8125
  total_value_units: 5350.0
  total_value_structures: 2425.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 8055.0
  collected_vespene: 1936.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 492.0
  spent_minerals: 7862.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: -38.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1175.0
    economy: 4525.0
    technology: 2250.0
    upgrade: 200.0
  }
  used_vespene {
    none: 150.0
    army: 325.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 5000.0
    technology: 2275.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 678.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1205.51464844
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 243
vespene: 1061
food_cap: 66
food_used: 68
food_army: 24
food_workers: 38
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 58
  }
}

score{
 score_type: Melee
score: 20773
score_details {
  idle_production_time: 32278.6875
  idle_worker_time: 84.125
  total_value_units: 13100.0
  total_value_structures: 4450.0
  killed_value_units: 7775.0
  killed_value_structures: 3875.0
  collected_minerals: 19975.0
  collected_vespene: 7260.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 582.0
  spent_minerals: 16737.0
  spent_vespene: 4400.0
  food_used {
    none: 0.0
    army: 61.5
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4800.0
    economy: 4500.0
    technology: 775.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3050.0
    economy: 1537.0
    technology: 1250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3050.0
    economy: 4775.0
    technology: 3225.0
    upgrade: 450.0
  }
  used_vespene {
    none: 150.0
    army: 1875.0
    economy: 0.0
    technology: 500.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 300.0
    army: 7225.0
    economy: 8350.0
    technology: 4350.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 300.0
    army: 5275.0
    economy: 0.0
    technology: 600.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 24279.9882812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13950.1601562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3288
vespene: 2860
food_cap: 154
food_used: 92
food_army: 61
food_workers: 31
idle_worker_count: 0
army_count: 40
warp_gate_count: 0

game_loop:  18786
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 2
}

Score:  20773
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
