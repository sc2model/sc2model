----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 128
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3609
  player_apm: 102
}
game_duration_loops: 12998
game_duration_seconds: 580.308349609
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3597
score_details {
  idle_production_time: 1049.75
  idle_worker_time: 209.0
  total_value_units: 3150.0
  total_value_structures: 2375.0
  killed_value_units: 2100.0
  killed_value_structures: 0.0
  collected_minerals: 4505.0
  collected_vespene: 1456.0
  collection_rate_minerals: 335.0
  collection_rate_vespene: 134.0
  spent_minerals: 4337.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 770.0
    economy: 1357.0
    technology: 313.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 338.0
    economy: 0.0
    technology: 92.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 475.0
    economy: 1350.0
    technology: 775.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1075.0
    economy: 2800.0
    technology: 1175.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2207.38769531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8674.02050781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1562.86108398
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 121
vespene: 551
food_cap: 31
food_used: 17
food_army: 8
food_workers: 8
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 1
}
production {
  unit {
    unit_type: 21
    player_relative: 1
    health: 258
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 48
    build_progress: 0.307500004768
  }
}

score{
 score_type: Melee
score: 5151
score_details {
  idle_production_time: 1601.75
  idle_worker_time: 302.9375
  total_value_units: 4175.0
  total_value_structures: 2975.0
  killed_value_units: 2500.0
  killed_value_structures: 0.0
  collected_minerals: 6085.0
  collected_vespene: 1732.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 313.0
  spent_minerals: 5637.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1300.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1020.0
    economy: 1357.0
    technology: 313.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 388.0
    economy: 0.0
    technology: 92.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 475.0
    economy: 2050.0
    technology: 1075.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1425.0
    economy: 3500.0
    technology: 1475.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2631.11621094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10077.0205078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3114.65869141
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 309
vespene: 317
food_cap: 39
food_used: 29
food_army: 8
food_workers: 20
idle_worker_count: 1
army_count: 3
warp_gate_count: 0

game_loop:  12998
ui_data{
 multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 2
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

Score:  5151
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
