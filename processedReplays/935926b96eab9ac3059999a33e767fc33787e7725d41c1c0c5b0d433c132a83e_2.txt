----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4479
  player_apm: 156
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4277
  player_apm: 146
}
game_duration_loops: 12294
game_duration_seconds: 548.877563477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4907
score_details {
  idle_production_time: 3733.4375
  idle_worker_time: 111.0
  total_value_units: 4400.0
  total_value_structures: 1275.0
  killed_value_units: 1325.0
  killed_value_structures: 0.0
  collected_minerals: 5535.0
  collected_vespene: 772.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 156.0
  spent_minerals: 5275.0
  spent_vespene: 375.0
  food_used {
    none: 0.0
    army: 10.5
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 475.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 725.0
    economy: 2475.0
    technology: 700.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2300.0
    economy: 2875.0
    technology: 850.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1891.44873047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5762.54541016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 310
vespene: 397
food_cap: 30
food_used: 29
food_army: 10
food_workers: 19
idle_worker_count: 0
army_count: 9
warp_gate_count: 0
larva_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1275
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 908
    shields: 0
    energy: 0
    build_progress: 0.56124997139
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 797
    shields: 0
    energy: 0
    build_progress: 0.478749990463
  }
}

score{
 score_type: Melee
score: 4707
score_details {
  idle_production_time: 6270.0
  idle_worker_time: 111.0
  total_value_units: 5800.0
  total_value_structures: 1875.0
  killed_value_units: 1700.0
  killed_value_structures: 0.0
  collected_minerals: 7055.0
  collected_vespene: 1052.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 156.0
  spent_minerals: 6925.0
  spent_vespene: 375.0
  food_used {
    none: 0.0
    army: 13.5
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2200.0
    economy: 825.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 450.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 775.0
    economy: 2425.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3300.0
    economy: 3975.0
    technology: 850.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3302.49707031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9967.7734375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 180
vespene: 677
food_cap: 68
food_used: 32
food_army: 13
food_workers: 19
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  12294
ui_data{
 
Score:  4707
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
