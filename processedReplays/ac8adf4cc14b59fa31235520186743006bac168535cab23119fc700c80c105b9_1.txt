----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2943
  player_apm: 68
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3088
  player_apm: 116
}
game_duration_loops: 7525
game_duration_seconds: 335.9609375
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5525
score_details {
  idle_production_time: 717.875
  idle_worker_time: 267.875
  total_value_units: 2400.0
  total_value_structures: 1500.0
  killed_value_units: 3025.0
  killed_value_structures: 700.0
  collected_minerals: 4125.0
  collected_vespene: 1352.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 358.0
  spent_minerals: 2300.0
  spent_vespene: 600.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 2300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 1950.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 900.0
    economy: 1950.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3519.89819336
    shields: 3878.10131836
    energy: 0.0
  }
  total_damage_taken {
    life: 878.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4.86401367188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1873
vespene: 752
food_cap: 39
food_used: 26
food_army: 4
food_workers: 22
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  7525
ui_data{
 
Score:  5525
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
