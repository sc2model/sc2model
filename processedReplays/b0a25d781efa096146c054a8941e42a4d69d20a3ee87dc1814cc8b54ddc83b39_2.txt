----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4314
  player_apm: 254
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4152
  player_apm: 130
}
game_duration_loops: 15396
game_duration_seconds: 687.369384766
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10536
score_details {
  idle_production_time: 7556.875
  idle_worker_time: 193.5625
  total_value_units: 6550.0
  total_value_structures: 1575.0
  killed_value_units: 1600.0
  killed_value_structures: 0.0
  collected_minerals: 9885.0
  collected_vespene: 976.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 425.0
  spent_minerals: 7225.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 20.5
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4350.0
    technology: 925.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 5200.0
    technology: 1075.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1622.9699707
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2794.66186523
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2710
vespene: 326
food_cap: 98
food_used: 60
food_army: 20
food_workers: 40
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16482
score_details {
  idle_production_time: 16660.3125
  idle_worker_time: 212.5
  total_value_units: 15050.0
  total_value_structures: 2850.0
  killed_value_units: 3750.0
  killed_value_structures: 150.0
  collected_minerals: 19435.0
  collected_vespene: 4172.0
  collection_rate_minerals: 2491.0
  collection_rate_vespene: 940.0
  spent_minerals: 17750.0
  spent_vespene: 3950.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5100.0
    economy: 925.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1900.0
    economy: 7825.0
    technology: 2150.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 500.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 6600.0
    economy: 9900.0
    technology: 2500.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 750.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 6006.44824219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11047.8457031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1735
vespene: 222
food_cap: 200
food_used: 109
food_army: 47
food_workers: 62
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  15396
ui_data{
 
Score:  16482
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
