----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4274
  player_apm: 146
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4164
  player_apm: 193
}
game_duration_loops: 18093
game_duration_seconds: 807.779602051
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9707
score_details {
  idle_production_time: 12260.4375
  idle_worker_time: 39.0
  total_value_units: 8350.0
  total_value_structures: 2200.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 10185.0
  collected_vespene: 1172.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 335.0
  spent_minerals: 9600.0
  spent_vespene: 1150.0
  food_used {
    none: 3.5
    army: 33.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1550.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2475.0
    economy: 4600.0
    technology: 975.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 150.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 4800.0
    economy: 5525.0
    technology: 825.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1344.96948242
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4097.17578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 635
vespene: 22
food_cap: 112
food_used: 72
food_army: 33
food_workers: 36
idle_worker_count: 0
army_count: 49
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 4
}
multi {
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10371
score_details {
  idle_production_time: 46358.9375
  idle_worker_time: 330.875
  total_value_units: 18700.0
  total_value_structures: 2900.0
  killed_value_units: 6625.0
  killed_value_structures: 150.0
  collected_minerals: 19817.0
  collected_vespene: 4604.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 671.0
  spent_minerals: 19750.0
  spent_vespene: 3550.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5600.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7050.0
    economy: 3800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 800.0
    economy: 4900.0
    technology: 1250.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 11150.0
    economy: 9150.0
    technology: 1400.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 350.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 8164.0546875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22523.8027344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 117
vespene: 1054
food_cap: 84
food_used: 57
food_army: 14
food_workers: 43
idle_worker_count: 43
army_count: 5
warp_gate_count: 0

game_loop:  18093
ui_data{
 
Score:  10371
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
