----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4367
  player_apm: 159
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4527
  player_apm: 212
}
game_duration_loops: 31174
game_duration_seconds: 1391.7935791
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12905
score_details {
  idle_production_time: 931.125
  idle_worker_time: 52.4375
  total_value_units: 6700.0
  total_value_structures: 4050.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 9355.0
  collected_vespene: 2700.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 649.0
  spent_minerals: 8850.0
  spent_vespene: 2100.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2775.0
    economy: 5150.0
    technology: 1450.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 400.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 4650.0
    technology: 1450.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 176.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 318.0
    shields: 375.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 555
vespene: 600
food_cap: 118
food_used: 102
food_army: 53
food_workers: 49
idle_worker_count: 0
army_count: 24
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 77
  count: 19
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 63
  count: 1
}
single {
  unit {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 54
  }
}

score{
 score_type: Melee
score: 25942
score_details {
  idle_production_time: 5496.5625
  idle_worker_time: 668.6875
  total_value_units: 15750.0
  total_value_structures: 12075.0
  killed_value_units: 7525.0
  killed_value_structures: 100.0
  collected_minerals: 23605.0
  collected_vespene: 8324.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 1142.0
  spent_minerals: 22612.0
  spent_vespene: 7700.0
  food_used {
    none: 0.0
    army: 101.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5800.0
    economy: 350.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3200.0
    economy: 1675.0
    technology: 537.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4550.0
    economy: 7800.0
    technology: 4500.0
    upgrade: 1500.0
  }
  used_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 850.0
    upgrade: 1500.0
  }
  total_used_minerals {
    none: 0.0
    army: 8150.0
    economy: 9475.0
    technology: 4700.0
    upgrade: 1500.0
  }
  total_used_vespene {
    none: 0.0
    army: 6350.0
    economy: 0.0
    technology: 950.0
    upgrade: 1500.0
  }
  total_damage_dealt {
    life: 7420.19873047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7879.0
    shields: 10247.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1043
vespene: 624
food_cap: 200
food_used: 158
food_army: 101
food_workers: 57
idle_worker_count: 0
army_count: 45
warp_gate_count: 7

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 29
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 63
  count: 2
}
multi {
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 147
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 152
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 30
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 56
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 147
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 50
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 20
    shields: 150
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 76
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 146
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 147
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 75
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
}

score{
 score_type: Melee
score: 30526
score_details {
  idle_production_time: 13732.25
  idle_worker_time: 2950.125
  total_value_units: 34075.0
  total_value_structures: 13500.0
  killed_value_units: 21600.0
  killed_value_structures: 1700.0
  collected_minerals: 35485.0
  collected_vespene: 15228.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 761.0
  spent_minerals: 35037.0
  spent_vespene: 14975.0
  food_used {
    none: 0.0
    army: 155.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 14550.0
    economy: 3300.0
    technology: 1000.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10975.0
    economy: 2900.0
    technology: 687.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6075.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7550.0
    economy: 7550.0
    technology: 4350.0
    upgrade: 1700.0
  }
  used_vespene {
    none: 0.0
    army: 6075.0
    economy: 0.0
    technology: 850.0
    upgrade: 1700.0
  }
  total_used_minerals {
    none: 0.0
    army: 20275.0
    economy: 10450.0
    technology: 5150.0
    upgrade: 1700.0
  }
  total_used_vespene {
    none: 0.0
    army: 14800.0
    economy: 0.0
    technology: 950.0
    upgrade: 1700.0
  }
  total_damage_dealt {
    life: 26767.0839844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14727.875
    shields: 20850.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 498
vespene: 253
food_cap: 200
food_used: 191
food_army: 155
food_workers: 36
idle_worker_count: 0
army_count: 66
warp_gate_count: 10

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 44
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 63
  count: 2
}
multi {
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 150
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 74
    shields: 61
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 86
    shields: 50
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 74
    shields: 62
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 35
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 79
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 185
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 189
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 28587
score_details {
  idle_production_time: 14363.3125
  idle_worker_time: 2950.125
  total_value_units: 35675.0
  total_value_structures: 13500.0
  killed_value_units: 27400.0
  killed_value_structures: 2800.0
  collected_minerals: 36370.0
  collected_vespene: 15904.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 716.0
  spent_minerals: 36237.0
  spent_vespene: 15875.0
  food_used {
    none: 0.0
    army: 136.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 18150.0
    economy: 4225.0
    technology: 1225.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6350.0
    economy: 150.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12725.0
    economy: 2950.0
    technology: 687.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7375.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6750.0
    economy: 7500.0
    technology: 4350.0
    upgrade: 1700.0
  }
  used_vespene {
    none: 0.0
    army: 5525.0
    economy: 0.0
    technology: 850.0
    upgrade: 1700.0
  }
  total_used_minerals {
    none: 0.0
    army: 21175.0
    economy: 10450.0
    technology: 5150.0
    upgrade: 1700.0
  }
  total_used_vespene {
    none: 0.0
    army: 15500.0
    economy: 0.0
    technology: 950.0
    upgrade: 1700.0
  }
  total_damage_dealt {
    life: 32555.0644531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16220.0
    shields: 23063.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 183
vespene: 29
food_cap: 200
food_used: 171
food_army: 136
food_workers: 35
idle_worker_count: 0
army_count: 53
warp_gate_count: 10

game_loop:  31174
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 31
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 63
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 118
    shields: 28
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 3
    shields: 21
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 74
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 31
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 190
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 107
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 74
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 79
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 178
    shields: 40
    energy: 0
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 16
  }
  units {
    unit_type: 75
    player_relative: 1
    health: 40
    shields: 40
    energy: 13
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  28587
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
