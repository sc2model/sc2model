----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 40
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2291
  player_apm: 32
}
game_duration_loops: 27683
game_duration_seconds: 1235.93444824
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8051
score_details {
  idle_production_time: 1765.375
  idle_worker_time: 762.125
  total_value_units: 3275.0
  total_value_structures: 3675.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 6125.0
  collected_vespene: 1676.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 447.0
  spent_minerals: 6125.0
  spent_vespene: 1200.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 3850.0
    technology: 1625.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 425.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1150.0
    economy: 3825.0
    technology: 1425.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 375.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1035.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 770.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 50
vespene: 476
food_cap: 69
food_used: 55
food_army: 20
food_workers: 33
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 production {
  unit {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 689
    build_progress: 0.035416662693
  }
}

score{
 score_type: Melee
score: 23437
score_details {
  idle_production_time: 8825.875
  idle_worker_time: 2741.3125
  total_value_units: 10475.0
  total_value_structures: 7050.0
  killed_value_units: 3750.0
  killed_value_structures: 0.0
  collected_minerals: 16635.0
  collected_vespene: 7552.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 806.0
  spent_minerals: 15925.0
  spent_vespene: 6400.0
  food_used {
    none: 0.0
    army: 88.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3250.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 250.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5250.0
    economy: 6200.0
    technology: 2900.0
    upgrade: 1175.0
  }
  used_vespene {
    none: 0.0
    army: 3375.0
    economy: 300.0
    technology: 1150.0
    upgrade: 1175.0
  }
  total_used_minerals {
    none: 0.0
    army: 4700.0
    economy: 6500.0
    technology: 3200.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 2825.0
    economy: 600.0
    technology: 1200.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 4005.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2564.34667969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 760
vespene: 1152
food_cap: 157
food_used: 142
food_army: 88
food_workers: 54
idle_worker_count: 2
army_count: 24
warp_gate_count: 0

game_loop:  20000
ui_data{
 single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 39307
score_details {
  idle_production_time: 15873.6875
  idle_worker_time: 4242.4375
  total_value_units: 18525.0
  total_value_structures: 10100.0
  killed_value_units: 7750.0
  killed_value_structures: 4750.0
  collected_minerals: 27185.0
  collected_vespene: 13472.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 1187.0
  spent_minerals: 23425.0
  spent_vespene: 11400.0
  food_used {
    none: 0.0
    army: 136.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5125.0
    economy: 4625.0
    technology: 2000.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 475.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 250.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 8450.0
    economy: 8050.0
    technology: 3750.0
    upgrade: 2525.0
  }
  used_vespene {
    none: 0.0
    army: 5775.0
    economy: 600.0
    technology: 1750.0
    upgrade: 2525.0
  }
  total_used_minerals {
    none: 0.0
    army: 9100.0
    economy: 9050.0
    technology: 4050.0
    upgrade: 2275.0
  }
  total_used_vespene {
    none: 0.0
    army: 6125.0
    economy: 1200.0
    technology: 1800.0
    upgrade: 2275.0
  }
  total_damage_dealt {
    life: 28246.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3235.34667969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3810
vespene: 2072
food_cap: 200
food_used: 197
food_army: 136
food_workers: 61
idle_worker_count: 6
army_count: 35
warp_gate_count: 0

game_loop:  27683
ui_data{
 
Score:  39307
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
