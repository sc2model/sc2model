----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 86
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2588
  player_apm: 79
}
game_duration_loops: 12529
game_duration_seconds: 559.369384766
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7544
score_details {
  idle_production_time: 2054.0625
  idle_worker_time: 166.75
  total_value_units: 4475.0
  total_value_structures: 3425.0
  killed_value_units: 1900.0
  killed_value_structures: 150.0
  collected_minerals: 6630.0
  collected_vespene: 1464.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 156.0
  spent_minerals: 6475.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 2625.0
    technology: 1600.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1950.0
    economy: 3525.0
    technology: 1450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1320.0
    shields: 2368.68261719
    energy: 0.0
  }
  total_damage_taken {
    life: 1027.0
    shields: 1636.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 205
vespene: 489
food_cap: 70
food_used: 50
food_army: 28
food_workers: 20
idle_worker_count: 1
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 12
}
groups {
  control_group_index: 4
  leader_unit_type: 62
  count: 3
}
production {
  unit {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  build_queue {
    unit_type: 84
    build_progress: 0.0930067896843
  }
  build_queue {
    unit_type: 84
    build_progress: 0.0
  }
  build_queue {
    unit_type: 84
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 8548
score_details {
  idle_production_time: 3135.6875
  idle_worker_time: 248.0
  total_value_units: 6175.0
  total_value_structures: 3575.0
  killed_value_units: 2725.0
  killed_value_structures: 150.0
  collected_minerals: 8875.0
  collected_vespene: 2048.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 268.0
  spent_minerals: 7475.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 1675.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 2350.0
    technology: 1150.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 4125.0
    technology: 1600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1890.0
    shields: 3730.68261719
    energy: 0.0
  }
  total_damage_taken {
    life: 3155.0
    shields: 4189.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1450
vespene: 673
food_cap: 70
food_used: 48
food_army: 30
food_workers: 18
idle_worker_count: 18
army_count: 10
warp_gate_count: 0

game_loop:  12529
ui_data{
 
Score:  8548
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
