----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4597
  player_apm: 359
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4579
  player_apm: 306
}
game_duration_loops: 16637
game_duration_seconds: 742.775024414
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13776
score_details {
  idle_production_time: 638.1875
  idle_worker_time: 288.0
  total_value_units: 5800.0
  total_value_structures: 4175.0
  killed_value_units: 525.0
  killed_value_structures: 0.0
  collected_minerals: 10490.0
  collected_vespene: 2096.0
  collection_rate_minerals: 3163.0
  collection_rate_vespene: 671.0
  spent_minerals: 10175.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 58.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 2825.0
    economy: 6200.0
    technology: 1650.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 475.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2300.0
    economy: 6350.0
    technology: 1200.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 375.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 937.536865234
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 388.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 24.4291992188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 360
vespene: 391
food_cap: 125
food_used: 119
food_army: 58
food_workers: 59
idle_worker_count: 2
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 53
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 49
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 734
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 22
  count: 2
}
cargo {
  unit {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 69
    transport_slots_taken: 2
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  slots_available: 6
}

score{
 score_type: Melee
score: 23644
score_details {
  idle_production_time: 2964.125
  idle_worker_time: 1464.3125
  total_value_units: 17175.0
  total_value_structures: 7150.0
  killed_value_units: 9875.0
  killed_value_structures: 700.0
  collected_minerals: 23505.0
  collected_vespene: 5864.0
  collection_rate_minerals: 2855.0
  collection_rate_vespene: 985.0
  spent_minerals: 22450.0
  spent_vespene: 3975.0
  food_used {
    none: 0.0
    army: 126.0
    economy: 74.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7100.0
    economy: 1600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5900.0
    economy: 8150.0
    technology: 2450.0
    upgrade: 900.0
  }
  used_vespene {
    none: 50.0
    army: 1400.0
    economy: 150.0
    technology: 750.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 50.0
    army: 11425.0
    economy: 8450.0
    technology: 2450.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 50.0
    army: 2050.0
    economy: 0.0
    technology: 750.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 17200.640625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6980.88769531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3365.31347656
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1080
vespene: 1864
food_cap: 200
food_used: 200
food_army: 126
food_workers: 74
idle_worker_count: 4
army_count: 85
warp_gate_count: 0

game_loop:  16637
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 31
}
groups {
  control_group_index: 2
  leader_unit_type: 500
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 10
}
groups {
  control_group_index: 6
  leader_unit_type: 22
  count: 2
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 36
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 148
    shields: 0
    energy: 33
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 110
    shields: 0
    energy: 27
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
}

Score:  23644
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
