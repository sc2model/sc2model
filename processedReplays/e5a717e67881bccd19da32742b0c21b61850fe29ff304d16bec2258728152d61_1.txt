----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2776
  player_apm: 51
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3600
  player_apm: 135
}
game_duration_loops: 16788
game_duration_seconds: 749.516601562
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7806
score_details {
  idle_production_time: 1881.4375
  idle_worker_time: 564.125
  total_value_units: 3250.0
  total_value_structures: 2600.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 4920.0
  collected_vespene: 1936.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 335.0
  spent_minerals: 4600.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 2750.0
    technology: 850.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 2850.0
    technology: 850.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 108.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 370
vespene: 686
food_cap: 70
food_used: 56
food_army: 33
food_workers: 22
idle_worker_count: 1
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 6665
score_details {
  idle_production_time: 4658.8125
  idle_worker_time: 2040.4375
  total_value_units: 6625.0
  total_value_structures: 3525.0
  killed_value_units: 3775.0
  killed_value_structures: 0.0
  collected_minerals: 9585.0
  collected_vespene: 3580.0
  collection_rate_minerals: 587.0
  collection_rate_vespene: 313.0
  spent_minerals: 8125.0
  spent_vespene: 2650.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2825.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3450.0
    economy: 1500.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 2150.0
    technology: 850.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 3600.0
    economy: 3800.0
    technology: 1050.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 425.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 4072.42675781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11447.9746094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 437.024902344
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1510
vespene: 930
food_cap: 55
food_used: 25
food_army: 6
food_workers: 19
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  16788
ui_data{
 
Score:  6665
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
