----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 55
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3227
  player_apm: 142
}
game_duration_loops: 5681
game_duration_seconds: 253.633773804
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3446
score_details {
  idle_production_time: 323.25
  idle_worker_time: 63.625
  total_value_units: 1600.0
  total_value_structures: 2225.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 2860.0
  collected_vespene: 536.0
  collection_rate_minerals: 27.0
  collection_rate_vespene: 111.0
  spent_minerals: 2900.0
  spent_vespene: 375.0
  food_used {
    none: 0.0
    army: 5.0
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 250.0
    economy: 1900.0
    technology: 750.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 250.0
    economy: 3050.0
    technology: 750.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 64.0
    shields: 203.0
    energy: 0.0
  }
  total_damage_taken {
    life: 998.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 10
vespene: 161
food_cap: 46
food_used: 14
food_army: 5
food_workers: 9
idle_worker_count: 10
army_count: 2
warp_gate_count: 0

game_loop:  5681
ui_data{
 
Score:  3446
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
