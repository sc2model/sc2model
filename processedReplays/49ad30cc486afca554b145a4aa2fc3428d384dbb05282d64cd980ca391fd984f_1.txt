----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3181
  player_apm: 67
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2935
  player_apm: 58
}
game_duration_loops: 34942
game_duration_seconds: 1560.01953125
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9088
score_details {
  idle_production_time: 1672.75
  idle_worker_time: 831.375
  total_value_units: 3575.0
  total_value_structures: 3500.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 7030.0
  collected_vespene: 1808.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 559.0
  spent_minerals: 6775.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 3800.0
    technology: 1475.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 325.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 3900.0
    technology: 1475.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 325.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 653.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 273.75
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 305
vespene: 308
food_cap: 86
food_used: 67
food_army: 28
food_workers: 38
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23180
score_details {
  idle_production_time: 9194.375
  idle_worker_time: 2635.5
  total_value_units: 11725.0
  total_value_structures: 8400.0
  killed_value_units: 4075.0
  killed_value_structures: 300.0
  collected_minerals: 19475.0
  collected_vespene: 5740.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 649.0
  spent_minerals: 18525.0
  spent_vespene: 5350.0
  food_used {
    none: 0.0
    army: 99.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2950.0
    economy: 300.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5550.0
    economy: 7500.0
    technology: 3375.0
    upgrade: 800.0
  }
  used_vespene {
    none: 0.0
    army: 2575.0
    economy: 150.0
    technology: 1025.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 0.0
    army: 6000.0
    economy: 8050.0
    technology: 3475.0
    upgrade: 800.0
  }
  total_used_vespene {
    none: 0.0
    army: 2775.0
    economy: 300.0
    technology: 1025.0
    upgrade: 800.0
  }
  total_damage_dealt {
    life: 5546.35205078
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3545.75
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 861.79296875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 929
vespene: 376
food_cap: 200
food_used: 159
food_army: 99
food_workers: 58
idle_worker_count: 5
army_count: 51
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 31901
score_details {
  idle_production_time: 19673.375
  idle_worker_time: 9886.25
  total_value_units: 23125.0
  total_value_structures: 9300.0
  killed_value_units: 15625.0
  killed_value_structures: 3725.0
  collected_minerals: 35325.0
  collected_vespene: 10748.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 470.0
  spent_minerals: 25625.0
  spent_vespene: 8850.0
  food_used {
    none: 0.0
    army: 73.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11500.0
    economy: 2425.0
    technology: 2500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2525.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8589.0
    economy: 1764.0
    technology: 800.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3793.0
    economy: 185.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4450.0
    economy: 6900.0
    technology: 3275.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 2950.0
    economy: 0.0
    technology: 1025.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 12750.0
    economy: 9200.0
    technology: 3975.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 0.0
    army: 6675.0
    economy: 300.0
    technology: 1025.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 29136.9003906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20122.5625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2126.95800781
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 9512
vespene: 1789
food_cap: 188
food_used: 132
food_army: 73
food_workers: 59
idle_worker_count: 6
army_count: 22
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
single {
  unit {
    unit_type: 29
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 37003
score_details {
  idle_production_time: 28592.25
  idle_worker_time: 12363.875
  total_value_units: 28125.0
  total_value_structures: 10950.0
  killed_value_units: 17625.0
  killed_value_structures: 5475.0
  collected_minerals: 39170.0
  collected_vespene: 12988.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 627.0
  spent_minerals: 31425.0
  spent_vespene: 11550.0
  food_used {
    none: 0.0
    army: 121.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13250.0
    economy: 3225.0
    technology: 3350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9053.0
    economy: 1764.0
    technology: 800.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4141.0
    economy: 185.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7450.0
    economy: 8250.0
    technology: 3875.0
    upgrade: 1450.0
  }
  used_vespene {
    none: 0.0
    army: 4750.0
    economy: 150.0
    technology: 1025.0
    upgrade: 1450.0
  }
  total_used_minerals {
    none: 0.0
    army: 15950.0
    economy: 10700.0
    technology: 4425.0
    upgrade: 1350.0
  }
  total_used_vespene {
    none: 0.0
    army: 8475.0
    economy: 600.0
    technology: 1025.0
    upgrade: 1350.0
  }
  total_damage_dealt {
    life: 39051.09375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20804.5625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3009.10302734
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 7395
vespene: 1208
food_cap: 200
food_used: 180
food_army: 121
food_workers: 59
idle_worker_count: 9
army_count: 43
warp_gate_count: 0

game_loop:  34942
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 517
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 222
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 529
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 390
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 472
    shields: 0
    energy: 0
  }
}

Score:  37003
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
