----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2904
  player_apm: 88
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 86
}
game_duration_loops: 43923
game_duration_seconds: 1960.98510742
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9470
score_details {
  idle_production_time: 1970.25
  idle_worker_time: 906.0625
  total_value_units: 3025.0
  total_value_structures: 3700.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 6355.0
  collected_vespene: 2360.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 649.0
  spent_minerals: 5875.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1100.0
    economy: 3850.0
    technology: 1675.0
    upgrade: 0.0
  }
  used_vespene {
    none: 100.0
    army: 725.0
    economy: 150.0
    technology: 325.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 100.0
    army: 500.0
    economy: 3950.0
    technology: 1675.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 100.0
    army: 375.0
    economy: 300.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 584.28125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 901.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1216.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 417
vespene: 1028
food_cap: 62
food_used: 61
food_army: 19
food_workers: 42
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22454
score_details {
  idle_production_time: 7696.0
  idle_worker_time: 4294.6875
  total_value_units: 11075.0
  total_value_structures: 7975.0
  killed_value_units: 500.0
  killed_value_structures: 250.0
  collected_minerals: 15825.0
  collected_vespene: 6924.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 515.0
  spent_minerals: 15325.0
  spent_vespene: 5925.0
  food_used {
    none: 0.0
    army: 82.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 100.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 350.0
    army: 5500.0
    economy: 5950.0
    technology: 3325.0
    upgrade: 450.0
  }
  used_vespene {
    none: 150.0
    army: 3475.0
    economy: 300.0
    technology: 1100.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 150.0
    army: 5600.0
    economy: 6300.0
    technology: 3325.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 150.0
    army: 3325.0
    economy: 600.0
    technology: 1100.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1909.04296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1651.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1216.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 437
vespene: 967
food_cap: 156
food_used: 124
food_army: 82
food_workers: 42
idle_worker_count: 12
army_count: 27
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 26849
score_details {
  idle_production_time: 23314.0
  idle_worker_time: 11792.3125
  total_value_units: 19175.0
  total_value_structures: 8725.0
  killed_value_units: 7200.0
  killed_value_structures: 2800.0
  collected_minerals: 25045.0
  collected_vespene: 9820.0
  collection_rate_minerals: 363.0
  collection_rate_vespene: 0.0
  spent_minerals: 21075.0
  spent_vespene: 9550.0
  food_used {
    none: 0.0
    army: 111.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4175.0
    economy: 1750.0
    technology: 2050.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2700.0
    economy: 4198.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1400.0
    economy: 567.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 7300.0
    economy: 3400.0
    technology: 3225.0
    upgrade: 1225.0
  }
  used_vespene {
    none: 150.0
    army: 5225.0
    economy: 0.0
    technology: 1100.0
    upgrade: 1225.0
  }
  total_used_minerals {
    none: 150.0
    army: 10000.0
    economy: 8050.0
    technology: 3325.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 150.0
    army: 6625.0
    economy: 900.0
    technology: 1100.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 22487.8164062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16684.3515625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3150.3203125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3696
vespene: 153
food_cap: 110
food_used: 131
food_army: 111
food_workers: 20
idle_worker_count: 9
army_count: 26
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13544
score_details {
  idle_production_time: 35456.5625
  idle_worker_time: 17069.375
  total_value_units: 20675.0
  total_value_structures: 9550.0
  killed_value_units: 19850.0
  killed_value_structures: 4200.0
  collected_minerals: 26775.0
  collected_vespene: 10460.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 23900.0
  spent_vespene: 10000.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11575.0
    economy: 3575.0
    technology: 2825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5625.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9650.0
    economy: 6998.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6425.0
    economy: 717.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 750.0
    economy: 2675.0
    technology: 2825.0
    upgrade: 1225.0
  }
  used_vespene {
    none: 150.0
    army: 500.0
    economy: 0.0
    technology: 1100.0
    upgrade: 1225.0
  }
  total_used_minerals {
    none: 150.0
    army: 10400.0
    economy: 9975.0
    technology: 3325.0
    upgrade: 1225.0
  }
  total_used_vespene {
    none: 150.0
    army: 6925.0
    economy: 1200.0
    technology: 1100.0
    upgrade: 1225.0
  }
  total_damage_dealt {
    life: 36618.9179688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 33398.5234375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3150.3203125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2601
vespene: 343
food_cap: 86
food_used: 25
food_army: 12
food_workers: 12
idle_worker_count: 11
army_count: 4
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
production {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 200
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0735294222832
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 14408
score_details {
  idle_production_time: 39408.25
  idle_worker_time: 20256.625
  total_value_units: 21575.0
  total_value_structures: 10625.0
  killed_value_units: 19850.0
  killed_value_structures: 4325.0
  collected_minerals: 28835.0
  collected_vespene: 10464.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 22.0
  spent_minerals: 26175.0
  spent_vespene: 10200.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11575.0
    economy: 3575.0
    technology: 2950.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5625.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10050.0
    economy: 7298.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6725.0
    economy: 717.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 750.0
    economy: 3100.0
    technology: 3775.0
    upgrade: 1225.0
  }
  used_vespene {
    none: 150.0
    army: 200.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1225.0
  }
  total_used_minerals {
    none: 150.0
    army: 10600.0
    economy: 10750.0
    technology: 4125.0
    upgrade: 1225.0
  }
  total_used_vespene {
    none: 150.0
    army: 6925.0
    economy: 1200.0
    technology: 1300.0
    upgrade: 1225.0
  }
  total_damage_dealt {
    life: 37327.4140625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 34840.5234375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3150.3203125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2386
vespene: 147
food_cap: 86
food_used: 34
food_army: 14
food_workers: 20
idle_worker_count: 20
army_count: 7
warp_gate_count: 0

game_loop:  43923
ui_data{
 
Score:  14408
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
