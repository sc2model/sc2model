----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3573
  player_apm: 169
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3725
  player_apm: 69
}
game_duration_loops: 8286
game_duration_seconds: 369.936523438
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4726
score_details {
  idle_production_time: 994.25
  idle_worker_time: 333.3125
  total_value_units: 2825.0
  total_value_structures: 3225.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 5200.0
  collected_vespene: 1280.0
  collection_rate_minerals: 251.0
  collection_rate_vespene: 67.0
  spent_minerals: 4350.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 2.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 1950.0
    technology: 129.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 375.0
    economy: 1575.0
    technology: 825.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 375.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 825.0
    economy: 3875.0
    technology: 1025.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 375.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2013.08251953
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4800.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 52.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 896
vespene: 355
food_cap: 54
food_used: 10
food_army: 8
food_workers: 2
idle_worker_count: 2
army_count: 3
warp_gate_count: 0

game_loop:  8286
ui_data{
 single {
  unit {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  4726
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
