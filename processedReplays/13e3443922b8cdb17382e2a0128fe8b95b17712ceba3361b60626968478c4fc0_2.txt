----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3363
  player_apm: 246
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3429
  player_apm: 151
}
game_duration_loops: 12169
game_duration_seconds: 543.296813965
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11203
score_details {
  idle_production_time: 5222.375
  idle_worker_time: 60.875
  total_value_units: 7700.0
  total_value_structures: 2125.0
  killed_value_units: 1000.0
  killed_value_structures: 0.0
  collected_minerals: 10315.0
  collected_vespene: 2356.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 559.0
  spent_minerals: 9237.0
  spent_vespene: 2206.0
  food_used {
    none: 2.5
    army: 26.5
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 956.0
    economy: 731.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 106.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 1750.0
    economy: 4400.0
    technology: 1325.0
    upgrade: 600.0
  }
  used_vespene {
    none: -25.0
    army: 975.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 6000.0
    technology: 1475.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 450.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 1064.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4529.79980469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1128
vespene: 150
food_cap: 84
food_used: 76
food_army: 26
food_workers: 47
idle_worker_count: 7
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 165
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 173
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 6411
score_details {
  idle_production_time: 7130.125
  idle_worker_time: 920.5
  total_value_units: 10650.0
  total_value_structures: 2125.0
  killed_value_units: 2000.0
  killed_value_structures: 0.0
  collected_minerals: 12160.0
  collected_vespene: 2944.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 44.0
  spent_minerals: 10937.0
  spent_vespene: 2806.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4131.0
    economy: 1981.0
    technology: 650.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1656.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: -75.0
    economy: 2875.0
    technology: 825.0
    upgrade: 600.0
  }
  used_vespene {
    none: -25.0
    army: 0.0
    economy: 0.0
    technology: 250.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 4850.0
    economy: 6000.0
    technology: 1475.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 450.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 3026.03076172
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13594.4238281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1273
vespene: 138
food_cap: 70
food_used: 21
food_army: 0
food_workers: 21
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  12169
ui_data{
 
Score:  6411
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
