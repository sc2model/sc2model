----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2903
  player_apm: 110
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2846
  player_apm: 117
}
game_duration_loops: 9808
game_duration_seconds: 437.887695312
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7639
score_details {
  idle_production_time: 1746.875
  idle_worker_time: 500.3125
  total_value_units: 3500.0
  total_value_structures: 3100.0
  killed_value_units: 2300.0
  killed_value_structures: 0.0
  collected_minerals: 6775.0
  collected_vespene: 2000.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 313.0
  spent_minerals: 6025.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 961.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 650.0
    economy: 3550.0
    technology: 1125.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1300.0
    economy: 4050.0
    technology: 1025.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2027.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1493.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 37.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 789
vespene: 300
food_cap: 77
food_used: 39
food_army: 12
food_workers: 26
idle_worker_count: 3
army_count: 4
warp_gate_count: 0

game_loop:  9808
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 60
  }
}

Score:  7639
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
