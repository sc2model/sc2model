----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4251
  player_apm: 131
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4278
  player_apm: 251
}
game_duration_loops: 17561
game_duration_seconds: 784.027954102
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8368
score_details {
  idle_production_time: 1242.5
  idle_worker_time: 64.5
  total_value_units: 4900.0
  total_value_structures: 3050.0
  killed_value_units: 1250.0
  killed_value_structures: 0.0
  collected_minerals: 7240.0
  collected_vespene: 1860.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 335.0
  spent_minerals: 6875.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 450.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1650.0
    economy: 3700.0
    technology: 950.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 2350.0
    economy: 3850.0
    technology: 1100.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 750.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1222.60766602
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3493.93408203
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 297.925292969
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 399
vespene: 419
food_cap: 62
food_used: 62
food_army: 33
food_workers: 29
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 13
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 159
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13612
score_details {
  idle_production_time: 3897.3125
  idle_worker_time: 447.75
  total_value_units: 13350.0
  total_value_structures: 5775.0
  killed_value_units: 6125.0
  killed_value_structures: 700.0
  collected_minerals: 17825.0
  collected_vespene: 4404.0
  collection_rate_minerals: 2491.0
  collection_rate_vespene: 515.0
  spent_minerals: 16450.0
  spent_vespene: 3450.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4250.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5867.0
    economy: 1550.0
    technology: 375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1267.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2050.0
    economy: 4800.0
    technology: 1825.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 1000.0
    economy: 0.0
    technology: 550.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 7550.0
    economy: 7150.0
    technology: 2275.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 2250.0
    economy: 0.0
    technology: 700.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 8656.81933594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15487.9902344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2052.71704102
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1350
vespene: 937
food_cap: 126
food_used: 85
food_army: 37
food_workers: 48
idle_worker_count: 3
army_count: 14
warp_gate_count: 0

game_loop:  17561
ui_data{
 
Score:  13612
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
