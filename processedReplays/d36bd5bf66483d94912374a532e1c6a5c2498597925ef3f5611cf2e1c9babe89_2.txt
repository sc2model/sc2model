----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3631
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3471
  player_apm: 83
}
game_duration_loops: 3938
game_duration_seconds: 175.815841675
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3152
score_details {
  idle_production_time: 200.0
  idle_worker_time: 165.25
  total_value_units: 1200.0
  total_value_structures: 925.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 2220.0
  collected_vespene: 100.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 179.0
  spent_minerals: 1493.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 143.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1975.0
    technology: 300.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 1975.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 20.0
    shields: 45.0
    energy: 0.0
  }
  total_damage_taken {
    life: 411.721191406
    shields: 688.028808594
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 777
vespene: 100
food_cap: 31
food_used: 22
food_army: 0
food_workers: 22
idle_worker_count: 22
army_count: 0
warp_gate_count: 0

game_loop:  3938
ui_data{
 
Score:  3152
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
