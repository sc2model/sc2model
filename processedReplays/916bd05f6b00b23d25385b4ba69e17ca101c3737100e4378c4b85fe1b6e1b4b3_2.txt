----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5081
  player_apm: 229
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5089
  player_apm: 293
}
game_duration_loops: 8963
game_duration_seconds: 400.161865234
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10645
score_details {
  idle_production_time: 703.5
  idle_worker_time: 54.5
  total_value_units: 4900.0
  total_value_structures: 3650.0
  killed_value_units: 3575.0
  killed_value_structures: 0.0
  collected_minerals: 9025.0
  collected_vespene: 1520.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 313.0
  spent_minerals: 8475.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2725.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 4550.0
    technology: 1775.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 425.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4750.0
    technology: 1475.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 425.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 7932.72070312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 949.223632812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1013.78125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 600
vespene: 145
food_cap: 94
food_used: 93
food_army: 43
food_workers: 48
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  8963
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 28
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}

Score:  10645
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
