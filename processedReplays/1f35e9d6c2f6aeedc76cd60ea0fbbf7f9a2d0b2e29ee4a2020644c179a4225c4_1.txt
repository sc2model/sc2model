----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4596
  player_apm: 216
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4422
  player_apm: 159
}
game_duration_loops: 8003
game_duration_seconds: 357.301727295
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7474
score_details {
  idle_production_time: 11078.375
  idle_worker_time: 2.125
  total_value_units: 5250.0
  total_value_structures: 1050.0
  killed_value_units: 1275.0
  killed_value_structures: 2050.0
  collected_minerals: 6380.0
  collected_vespene: 744.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 179.0
  spent_minerals: 6075.0
  spent_vespene: 425.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 475.0
    economy: 550.0
    technology: 1550.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 3150.0
    technology: 625.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 125.0
    technology: 0.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 3575.0
    technology: 625.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 250.0
    technology: 0.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 5340.0
    shields: 5883.125
    energy: 0.0
  }
  total_damage_taken {
    life: 1262.40478516
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 355
vespene: 319
food_cap: 92
food_used: 65
food_army: 40
food_workers: 25
idle_worker_count: 0
army_count: 48
warp_gate_count: 0

game_loop:  8003
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 55
}
groups {
  control_group_index: 2
  leader_unit_type: 893
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 90
  count: 1
}

Score:  7474
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
