----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3654
  player_apm: 152
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3696
  player_apm: 110
}
game_duration_loops: 7272
game_duration_seconds: 324.665527344
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4977
score_details {
  idle_production_time: 5799.75
  idle_worker_time: 0.0
  total_value_units: 4300.0
  total_value_structures: 1000.0
  killed_value_units: 2000.0
  killed_value_structures: 0.0
  collected_minerals: 4920.0
  collected_vespene: 132.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 0.0
  spent_minerals: 4800.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 15.5
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1750.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 975.0
    economy: 3075.0
    technology: 525.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 2925.0
    technology: 525.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2799.64013672
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2127.44921875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 170
vespene: 32
food_cap: 84
food_used: 43
food_army: 15
food_workers: 20
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  7272
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 4
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  4977
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
