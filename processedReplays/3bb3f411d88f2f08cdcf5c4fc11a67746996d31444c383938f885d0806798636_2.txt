----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3405
  player_apm: 102
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3389
  player_apm: 167
}
game_duration_loops: 17567
game_duration_seconds: 784.295776367
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11846
score_details {
  idle_production_time: 1444.875
  idle_worker_time: 374.9375
  total_value_units: 5700.0
  total_value_structures: 4825.0
  killed_value_units: 450.0
  killed_value_structures: 100.0
  collected_minerals: 11370.0
  collected_vespene: 0.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 0.0
  spent_minerals: 11149.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 175.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: -226.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 3000.0
    economy: 5650.0
    technology: 2725.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 3200.0
    economy: 5500.0
    technology: 2725.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 445.0
    shields: 960.0
    energy: 0.0
  }
  total_damage_taken {
    life: 695.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 271
vespene: 0
food_cap: 117
food_used: 110
food_army: 60
food_workers: 50
idle_worker_count: 1
army_count: 50
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 10
}
groups {
  control_group_index: 4
  leader_unit_type: 48
  count: 31
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 14561
score_details {
  idle_production_time: 5243.1875
  idle_worker_time: 2683.875
  total_value_units: 17650.0
  total_value_structures: 7825.0
  killed_value_units: 6050.0
  killed_value_structures: 200.0
  collected_minerals: 27285.0
  collected_vespene: 0.0
  collection_rate_minerals: 2631.0
  collection_rate_vespene: 0.0
  spent_minerals: 26699.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4675.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12650.0
    economy: 100.0
    technology: 174.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 7200.0
    technology: 4425.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 14750.0
    economy: 7100.0
    technology: 4825.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 5248.0
    shields: 6250.875
    energy: 0.0
  }
  total_damage_taken {
    life: 12588.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 636
vespene: 0
food_cap: 172
food_used: 102
food_army: 46
food_workers: 56
idle_worker_count: 56
army_count: 31
warp_gate_count: 0

game_loop:  17567
ui_data{
 
Score:  14561
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
