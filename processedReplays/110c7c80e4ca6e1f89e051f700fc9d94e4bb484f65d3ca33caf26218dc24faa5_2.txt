----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3285
  player_apm: 104
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3378
  player_apm: 92
}
game_duration_loops: 17302
game_duration_seconds: 772.464599609
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8365
score_details {
  idle_production_time: 7669.5625
  idle_worker_time: 10.3125
  total_value_units: 5150.0
  total_value_structures: 1725.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 6745.0
  collected_vespene: 1320.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 313.0
  spent_minerals: 6675.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1225.0
    economy: 4200.0
    technology: 1400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 100.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2125.0
    economy: 4300.0
    technology: 1125.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 681.852539062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1153.18847656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 120
vespene: 445
food_cap: 82
food_used: 71
food_army: 30
food_workers: 32
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12106
score_details {
  idle_production_time: 16620.5625
  idle_worker_time: 10.8125
  total_value_units: 11950.0
  total_value_structures: 3475.0
  killed_value_units: 4475.0
  killed_value_structures: 350.0
  collected_minerals: 14100.0
  collected_vespene: 4424.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 671.0
  spent_minerals: 13968.0
  spent_vespene: 3250.0
  food_used {
    none: 0.0
    army: 24.5
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2750.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4625.0
    economy: 100.0
    technology: 18.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4600.0
    technology: 3325.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 6225.0
    economy: 6250.0
    technology: 3475.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 2800.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 7549.1484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6883.24755859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 182
vespene: 1174
food_cap: 112
food_used: 60
food_army: 24
food_workers: 36
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  17302
ui_data{
 multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  12106
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
