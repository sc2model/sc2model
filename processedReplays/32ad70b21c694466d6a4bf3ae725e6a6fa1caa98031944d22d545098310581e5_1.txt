----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3305
  player_apm: 88
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3331
  player_apm: 78
}
game_duration_loops: 26288
game_duration_seconds: 1173.65332031
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12354
score_details {
  idle_production_time: 4802.0
  idle_worker_time: 16.125
  total_value_units: 7450.0
  total_value_structures: 2075.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 9350.0
  collected_vespene: 2104.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 649.0
  spent_minerals: 8600.0
  spent_vespene: 1475.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2175.0
    economy: 5150.0
    technology: 1525.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 250.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2525.0
    economy: 6000.0
    technology: 1675.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 20.0
    energy: 0.0
  }
  total_damage_taken {
    life: 342.683105469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 800
vespene: 629
food_cap: 130
food_used: 90
food_army: 42
food_workers: 48
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22923
score_details {
  idle_production_time: 18683.125
  idle_worker_time: 341.5625
  total_value_units: 20550.0
  total_value_structures: 3975.0
  killed_value_units: 2890.0
  killed_value_structures: 750.0
  collected_minerals: 21759.0
  collected_vespene: 8464.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 895.0
  spent_minerals: 21350.0
  spent_vespene: 7675.0
  food_used {
    none: 0.0
    army: 76.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1740.0
    economy: 150.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4875.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5150.0
    economy: 7375.0
    technology: 3350.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 1050.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 9825.0
    economy: 8975.0
    technology: 3600.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 6075.0
    economy: 0.0
    technology: 1150.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 2259.0
    shields: 2807.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8103.9296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 459
vespene: 789
food_cap: 192
food_used: 134
food_army: 76
food_workers: 58
idle_worker_count: 0
army_count: 30
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
single {
  unit {
    unit_type: 92
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13196
score_details {
  idle_production_time: 28117.625
  idle_worker_time: 3275.1875
  total_value_units: 29600.0
  total_value_structures: 5000.0
  killed_value_units: 7540.0
  killed_value_structures: 750.0
  collected_minerals: 28069.0
  collected_vespene: 11152.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 44.0
  spent_minerals: 27800.0
  spent_vespene: 10850.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4790.0
    economy: 150.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13425.0
    economy: 4100.0
    technology: 1375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 8150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 950.0
    economy: 5075.0
    technology: 2850.0
    upgrade: 1025.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1025.0
  }
  total_used_minerals {
    none: 0.0
    army: 15225.0
    economy: 10875.0
    technology: 4425.0
    upgrade: 1025.0
  }
  total_used_vespene {
    none: 0.0
    army: 9875.0
    economy: 0.0
    technology: 1450.0
    upgrade: 1025.0
  }
  total_damage_dealt {
    life: 6360.875
    shields: 7929.875
    energy: 0.0
  }
  total_damage_taken {
    life: 34710.4726562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 319
vespene: 302
food_cap: 180
food_used: 50
food_army: 14
food_workers: 36
idle_worker_count: 21
army_count: 3
warp_gate_count: 0

game_loop:  26288
ui_data{
 multi {
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  13196
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
