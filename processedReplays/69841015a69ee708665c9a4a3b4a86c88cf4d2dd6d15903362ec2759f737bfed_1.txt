----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5366
  player_apm: 166
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5270
  player_apm: 179
}
game_duration_loops: 7172
game_duration_seconds: 320.200927734
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6807
score_details {
  idle_production_time: 2162.75
  idle_worker_time: 6.8125
  total_value_units: 4600.0
  total_value_structures: 1775.0
  killed_value_units: 2100.0
  killed_value_structures: 0.0
  collected_minerals: 5405.0
  collected_vespene: 1288.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 492.0
  spent_minerals: 5261.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 50.0
    technology: -114.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 3125.0
    technology: 900.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1825.0
    economy: 3525.0
    technology: 1150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 600.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2767.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1598.48730469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 194
vespene: 138
food_cap: 68
food_used: 67
food_army: 37
food_workers: 30
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  7172
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 129
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 143
    shields: 0
    energy: 0
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 143
    shields: 0
    energy: 22
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 136
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 58
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 111
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 61
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  6807
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
