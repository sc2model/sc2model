----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 58
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2934
  player_apm: 48
}
game_duration_loops: 6650
game_duration_seconds: 296.895721436
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5354
score_details {
  idle_production_time: 539.125
  idle_worker_time: 161.375
  total_value_units: 2100.0
  total_value_structures: 2300.0
  killed_value_units: 250.0
  killed_value_structures: 100.0
  collected_minerals: 3950.0
  collected_vespene: 1004.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 335.0
  spent_minerals: 3475.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 5.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 250.0
    economy: 2900.0
    technology: 875.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 225.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 600.0
    economy: 3200.0
    technology: 725.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 814.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 437.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 525
vespene: 329
food_cap: 70
food_used: 28
food_army: 5
food_workers: 23
idle_worker_count: 2
army_count: 5
warp_gate_count: 0

game_loop:  6650
ui_data{
 
Score:  5354
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
