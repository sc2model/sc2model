----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3540
  player_apm: 124
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3457
  player_apm: 131
}
game_duration_loops: 20103
game_duration_seconds: 897.518005371
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10362
score_details {
  idle_production_time: 1595.0
  idle_worker_time: 1157.375
  total_value_units: 4400.0
  total_value_structures: 4175.0
  killed_value_units: 1975.0
  killed_value_structures: 0.0
  collected_minerals: 9100.0
  collected_vespene: 2112.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 492.0
  spent_minerals: 7825.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1575.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 550.0
    economy: 5050.0
    technology: 1325.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 150.0
    economy: 0.0
    technology: 725.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 1900.0
    economy: 5000.0
    technology: 1225.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 300.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 3045.63574219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1746.2199707
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 109.592285156
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1325
vespene: 937
food_cap: 101
food_used: 57
food_army: 11
food_workers: 44
idle_worker_count: 3
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 734
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 29
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27591
score_details {
  idle_production_time: 7595.0
  idle_worker_time: 5178.5
  total_value_units: 16800.0
  total_value_structures: 7025.0
  killed_value_units: 12650.0
  killed_value_structures: 700.0
  collected_minerals: 23755.0
  collected_vespene: 8136.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 851.0
  spent_minerals: 20350.0
  spent_vespene: 5625.0
  food_used {
    none: 0.0
    army: 112.0
    economy: 76.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8575.0
    economy: 2750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5600.0
    economy: 8050.0
    technology: 2050.0
    upgrade: 950.0
  }
  used_vespene {
    none: 50.0
    army: 2550.0
    economy: 150.0
    technology: 1225.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 50.0
    army: 9750.0
    economy: 8650.0
    technology: 2050.0
    upgrade: 950.0
  }
  total_used_vespene {
    none: 50.0
    army: 3250.0
    economy: 300.0
    technology: 1225.0
    upgrade: 950.0
  }
  total_damage_dealt {
    life: 18529.0800781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6328.69384766
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 375.072753906
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3455
vespene: 2511
food_cap: 188
food_used: 188
food_army: 112
food_workers: 76
idle_worker_count: 21
army_count: 40
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 33
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 102
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 169
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 155
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27741
score_details {
  idle_production_time: 7685.125
  idle_worker_time: 5313.6875
  total_value_units: 16900.0
  total_value_structures: 7025.0
  killed_value_units: 12800.0
  killed_value_structures: 700.0
  collected_minerals: 23845.0
  collected_vespene: 8196.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 851.0
  spent_minerals: 20350.0
  spent_vespene: 5625.0
  food_used {
    none: 0.0
    army: 112.0
    economy: 76.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8725.0
    economy: 2750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5600.0
    economy: 8050.0
    technology: 2050.0
    upgrade: 950.0
  }
  used_vespene {
    none: 50.0
    army: 2550.0
    economy: 150.0
    technology: 1225.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 50.0
    army: 9850.0
    economy: 8650.0
    technology: 2050.0
    upgrade: 950.0
  }
  total_used_vespene {
    none: 50.0
    army: 3250.0
    economy: 300.0
    technology: 1225.0
    upgrade: 950.0
  }
  total_damage_dealt {
    life: 18932.8164062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6340.69384766
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 377.320800781
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3545
vespene: 2571
food_cap: 188
food_used: 188
food_army: 112
food_workers: 76
idle_worker_count: 21
army_count: 41
warp_gate_count: 0

game_loop:  20103
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 33
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 1
}

Score:  27741
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
