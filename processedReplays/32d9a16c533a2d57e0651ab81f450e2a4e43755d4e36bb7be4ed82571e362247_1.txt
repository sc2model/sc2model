----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4151
  player_apm: 209
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4053
  player_apm: 75
}
game_duration_loops: 7793
game_duration_seconds: 347.926055908
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3869
score_details {
  idle_production_time: 7304.625
  idle_worker_time: 0.4375
  total_value_units: 4150.0
  total_value_structures: 825.0
  killed_value_units: 3150.0
  killed_value_structures: 150.0
  collected_minerals: 4450.0
  collected_vespene: 100.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 0.0
  spent_minerals: 4381.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 24.5
    economy: 14.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2100.0
    economy: 1050.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1775.0
    economy: -69.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1325.0
    economy: 1975.0
    technology: 250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2800.0
    economy: 2125.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4532.89306641
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3130.34228516
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 119
vespene: 0
food_cap: 52
food_used: 38
food_army: 24
food_workers: 14
idle_worker_count: 0
army_count: 29
warp_gate_count: 0

game_loop:  7793
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 35
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  3869
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
