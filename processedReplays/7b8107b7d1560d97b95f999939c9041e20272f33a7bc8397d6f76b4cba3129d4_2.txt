----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3306
  player_apm: 55
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3422
  player_apm: 159
}
game_duration_loops: 8852
game_duration_seconds: 395.20614624
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7119
score_details {
  idle_production_time: 883.625
  idle_worker_time: 389.875
  total_value_units: 4500.0
  total_value_structures: 2700.0
  killed_value_units: 3050.0
  killed_value_structures: 650.0
  collected_minerals: 5515.0
  collected_vespene: 1604.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 313.0
  spent_minerals: 5375.0
  spent_vespene: 1025.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1500.0
    economy: 1150.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1925.0
    economy: 2600.0
    technology: 950.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2475.0
    economy: 2800.0
    technology: 950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3034.0
    shields: 3569.375
    energy: 0.0
  }
  total_damage_taken {
    life: 1221.625
    shields: 1649.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 190
vespene: 579
food_cap: 79
food_used: 55
food_army: 32
food_workers: 23
idle_worker_count: 1
army_count: 14
warp_gate_count: 4

game_loop:  8852
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 1
}

Score:  7119
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
