----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4913
  player_apm: 215
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5054
  player_apm: 172
}
game_duration_loops: 22800
game_duration_seconds: 1017.92816162
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10341
score_details {
  idle_production_time: 3290.0
  idle_worker_time: 87.8125
  total_value_units: 7800.0
  total_value_structures: 1425.0
  killed_value_units: 2200.0
  killed_value_structures: 0.0
  collected_minerals: 9865.0
  collected_vespene: 1032.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 492.0
  spent_minerals: 9056.0
  spent_vespene: 425.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 1231.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1575.0
    economy: 6050.0
    technology: 725.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 100.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1925.0
    economy: 7000.0
    technology: 575.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1929.12402344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3777.17773438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 859
vespene: 607
food_cap: 90
food_used: 88
food_army: 30
food_workers: 58
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 9
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 6
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 81
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20570
score_details {
  idle_production_time: 20862.625
  idle_worker_time: 2574.1875
  total_value_units: 23950.0
  total_value_structures: 3600.0
  killed_value_units: 12325.0
  killed_value_structures: 750.0
  collected_minerals: 25765.0
  collected_vespene: 5636.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 492.0
  spent_minerals: 25156.0
  spent_vespene: 5050.0
  food_used {
    none: 0.0
    army: 119.5
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7350.0
    economy: 2200.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2875.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6050.0
    economy: 2881.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 775.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5400.0
    economy: 8100.0
    technology: 2000.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 1675.0
    economy: 0.0
    technology: 450.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 11575.0
    economy: 12300.0
    technology: 1950.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 4125.0
    economy: 0.0
    technology: 400.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 17360.6230469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24036.0644531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 659
vespene: 586
food_cap: 200
food_used: 174
food_army: 119
food_workers: 55
idle_worker_count: 2
army_count: 73
warp_gate_count: 0
larva_count: 26

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 70
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 5
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
    add_on {
      unit_type: 101
      build_progress: 0.641250014305
    }
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 376
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1315
    shields: 0
    energy: 0
    build_progress: 0.863124966621
  }
}

score{
 score_type: Melee
score: 21077
score_details {
  idle_production_time: 29442.0
  idle_worker_time: 3134.0
  total_value_units: 27900.0
  total_value_structures: 4200.0
  killed_value_units: 17775.0
  killed_value_structures: 2950.0
  collected_minerals: 29790.0
  collected_vespene: 7068.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 649.0
  spent_minerals: 28631.0
  spent_vespene: 6550.0
  food_used {
    none: 0.0
    army: 82.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10500.0
    economy: 5150.0
    technology: 850.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3825.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9500.0
    economy: 2806.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 1125.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3825.0
    economy: 9000.0
    technology: 2000.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 450.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 0.0
    army: 13450.0
    economy: 14000.0
    technology: 2350.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 5200.0
    economy: 0.0
    technology: 700.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 28866.9023438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 30915.2851562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1209
vespene: 518
food_cap: 200
food_used: 147
food_army: 82
food_workers: 65
idle_worker_count: 5
army_count: 34
warp_gate_count: 0

game_loop:  22800
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 32
}
groups {
  control_group_index: 2
  leader_unit_type: 499
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 5
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 131
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 129
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 98
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 132
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 112
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 74
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 130
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
}

Score:  21077
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
