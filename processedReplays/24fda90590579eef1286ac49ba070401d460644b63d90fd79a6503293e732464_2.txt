----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3437
  player_apm: 132
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3575
  player_apm: 113
}
game_duration_loops: 17916
game_duration_seconds: 799.877258301
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10894
score_details {
  idle_production_time: 1419.875
  idle_worker_time: 440.6875
  total_value_units: 5075.0
  total_value_structures: 3650.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 8040.0
  collected_vespene: 2204.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 537.0
  spent_minerals: 7975.0
  spent_vespene: 2075.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2775.0
    economy: 4300.0
    technology: 1150.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 1325.0
    economy: 0.0
    technology: 400.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2275.0
    economy: 4500.0
    technology: 1150.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 900.0
    economy: 0.0
    technology: 400.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 120.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 744.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 29.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 115
vespene: 129
food_cap: 102
food_used: 87
food_army: 55
food_workers: 32
idle_worker_count: 2
army_count: 30
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 33
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 6849
score_details {
  idle_production_time: 6202.75
  idle_worker_time: 2188.875
  total_value_units: 11650.0
  total_value_structures: 5450.0
  killed_value_units: 5975.0
  killed_value_structures: 550.0
  collected_minerals: 14070.0
  collected_vespene: 4652.0
  collection_rate_minerals: 363.0
  collection_rate_vespene: 201.0
  spent_minerals: 13225.0
  spent_vespene: 4125.0
  food_used {
    none: 0.0
    army: 5.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4250.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5778.0
    economy: 2380.0
    technology: 1056.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2728.0
    economy: 0.0
    technology: 381.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 225.0
    economy: 3125.0
    technology: 1000.0
    upgrade: 300.0
  }
  used_vespene {
    none: 100.0
    army: 125.0
    economy: 0.0
    technology: 275.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 100.0
    army: 6225.0
    economy: 5900.0
    technology: 1950.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 100.0
    army: 3075.0
    economy: 0.0
    technology: 650.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 7733.265625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20499.6035156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1491.28735352
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 806
vespene: 493
food_cap: 94
food_used: 25
food_army: 5
food_workers: 20
idle_worker_count: 9
army_count: 2
warp_gate_count: 0

game_loop:  17916
ui_data{
 single {
  unit {
    unit_type: 54
    player_relative: 1
    health: 70
    shields: 0
    energy: 150
  }
}

Score:  6849
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
