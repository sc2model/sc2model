----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4644
  player_apm: 163
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4504
  player_apm: 235
}
game_duration_loops: 17718
game_duration_seconds: 791.037353516
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9781
score_details {
  idle_production_time: 1517.1875
  idle_worker_time: 963.4375
  total_value_units: 4650.0
  total_value_structures: 3500.0
  killed_value_units: 1300.0
  killed_value_structures: 100.0
  collected_minerals: 8195.0
  collected_vespene: 1800.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 403.0
  spent_minerals: 7600.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 450.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1300.0
    economy: 4550.0
    technology: 1500.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4925.0
    technology: 1075.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1492.64428711
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2034.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 176.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 607
vespene: 474
food_cap: 101
food_used: 67
food_army: 26
food_workers: 39
idle_worker_count: 4
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 55
    player_relative: 1
    health: 140
    shields: 0
    energy: 66
  }
}

score{
 score_type: Melee
score: 18434
score_details {
  idle_production_time: 4972.3125
  idle_worker_time: 4615.875
  total_value_units: 13375.0
  total_value_structures: 6150.0
  killed_value_units: 6175.0
  killed_value_structures: 100.0
  collected_minerals: 19050.0
  collected_vespene: 5204.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 627.0
  spent_minerals: 18525.0
  spent_vespene: 4400.0
  food_used {
    none: 0.0
    army: 90.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4050.0
    economy: 600.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3076.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 876.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 4500.0
    economy: 5900.0
    technology: 2625.0
    upgrade: 950.0
  }
  used_vespene {
    none: 50.0
    army: 1500.0
    economy: 0.0
    technology: 725.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 50.0
    army: 7300.0
    economy: 7350.0
    technology: 2625.0
    upgrade: 950.0
  }
  total_used_vespene {
    none: 50.0
    army: 2325.0
    economy: 0.0
    technology: 725.0
    upgrade: 950.0
  }
  total_damage_dealt {
    life: 5494.91894531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6084.01025391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1461.640625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 462
vespene: 722
food_cap: 141
food_used: 141
food_army: 90
food_workers: 50
idle_worker_count: 11
army_count: 68
warp_gate_count: 0

game_loop:  17718
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 54
  count: 37
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 880
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  18434
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
