----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3673
  player_apm: 160
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3750
  player_apm: 215
}
game_duration_loops: 16753
game_duration_seconds: 747.953979492
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10673
score_details {
  idle_production_time: 1398.125
  idle_worker_time: 648.0625
  total_value_units: 5475.0
  total_value_structures: 4150.0
  killed_value_units: 575.0
  killed_value_structures: 0.0
  collected_minerals: 8455.0
  collected_vespene: 1768.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 694.0
  spent_minerals: 7550.0
  spent_vespene: 1550.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 175.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 75.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2225.0
    economy: 4050.0
    technology: 1650.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2325.0
    economy: 4150.0
    technology: 1650.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 845.733398438
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1097.39550781
    shields: 1997.47949219
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 955
vespene: 218
food_cap: 102
food_used: 81
food_army: 40
food_workers: 40
idle_worker_count: 2
army_count: 16
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 694
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 7
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 71
  count: 2
}
cargo {
  unit {
    unit_type: 81
    player_relative: 1
    health: 80
    shields: 26
    energy: 0
    transport_slots_taken: 2
  }
  passengers {
    unit_type: 694
    player_relative: 1
    health: 100
    shields: 80
    energy: 0
  }
  passengers {
    unit_type: 694
    player_relative: 1
    health: 100
    shields: 100
    energy: 0
  }
  slots_available: 6
}

score{
 score_type: Melee
score: 9210
score_details {
  idle_production_time: 3433.375
  idle_worker_time: 1810.8125
  total_value_units: 15200.0
  total_value_structures: 4800.0
  killed_value_units: 8100.0
  killed_value_structures: 0.0
  collected_minerals: 15640.0
  collected_vespene: 5020.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 627.0
  spent_minerals: 15575.0
  spent_vespene: 4950.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5525.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8150.0
    economy: 75.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 5200.0
    technology: 1650.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 8900.0
    economy: 4850.0
    technology: 1800.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 4050.0
    economy: 0.0
    technology: 400.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 9428.16796875
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6537.39550781
    shields: 8752.10449219
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 115
vespene: 70
food_cap: 142
food_used: 62
food_army: 18
food_workers: 44
idle_worker_count: 44
army_count: 5
warp_gate_count: 6

game_loop:  16753
ui_data{
 
Score:  9210
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
