----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3261
  player_apm: 109
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 354
}
game_duration_loops: 6267
game_duration_seconds: 279.796295166
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5137
score_details {
  idle_production_time: 1842.8125
  idle_worker_time: 0.0
  total_value_units: 3800.0
  total_value_structures: 975.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 5035.0
  collected_vespene: 452.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 156.0
  spent_minerals: 4075.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1400.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 2725.0
    technology: 500.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 3025.0
    technology: 500.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1082.10791016
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3281.67626953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1010
vespene: 352
food_cap: 60
food_used: 33
food_army: 6
food_workers: 27
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  6267
ui_data{
 
Score:  5137
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
