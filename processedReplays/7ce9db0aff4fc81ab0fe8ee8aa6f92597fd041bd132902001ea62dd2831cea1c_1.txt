----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3670
  player_apm: 128
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3723
  player_apm: 123
}
game_duration_loops: 13476
game_duration_seconds: 601.649108887
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10535
score_details {
  idle_production_time: 1417.6875
  idle_worker_time: 763.4375
  total_value_units: 5425.0
  total_value_structures: 4000.0
  killed_value_units: 1075.0
  killed_value_structures: 0.0
  collected_minerals: 8185.0
  collected_vespene: 1700.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 403.0
  spent_minerals: 8100.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 69.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3450.0
    economy: 3750.0
    technology: 1400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 500.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3000.0
    economy: 4450.0
    technology: 1400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 500.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1740.77880859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 752.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 204.918457031
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 135
vespene: 400
food_cap: 110
food_used: 96
food_army: 69
food_workers: 27
idle_worker_count: 3
army_count: 37
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 94
    shields: 0
    energy: 125
    transport_slots_taken: 6
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 86
    shields: 0
    energy: 137
    transport_slots_taken: 8
  }
}

score{
 score_type: Melee
score: 8562
score_details {
  idle_production_time: 2985.75
  idle_worker_time: 1415.1875
  total_value_units: 7775.0
  total_value_structures: 4000.0
  killed_value_units: 4350.0
  killed_value_structures: 0.0
  collected_minerals: 10560.0
  collected_vespene: 2852.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 447.0
  spent_minerals: 10350.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3150.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4800.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 3750.0
    technology: 1400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 500.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 5250.0
    economy: 4450.0
    technology: 1400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 500.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 6050.77978516
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5934.25244141
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2551.0390625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 260
vespene: 1477
food_cap: 110
food_used: 44
food_army: 17
food_workers: 27
idle_worker_count: 3
army_count: 7
warp_gate_count: 0

game_loop:  13476
ui_data{
 multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 106
    shields: 0
    energy: 22
  }
}

Score:  8562
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
