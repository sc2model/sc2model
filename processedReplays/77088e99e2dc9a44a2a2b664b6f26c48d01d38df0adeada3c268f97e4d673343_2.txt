----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2846
  player_apm: 55
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2729
  player_apm: 53
}
game_duration_loops: 20660
game_duration_seconds: 922.385803223
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8870
score_details {
  idle_production_time: 2286.3125
  idle_worker_time: 291.0
  total_value_units: 3650.0
  total_value_structures: 2775.0
  killed_value_units: 950.0
  killed_value_structures: 400.0
  collected_minerals: 5815.0
  collected_vespene: 2436.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 470.0
  spent_minerals: 4600.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 150.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 287.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1475.0
    economy: 2975.0
    technology: 900.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1325.0
    economy: 3125.0
    technology: 900.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 904.830078125
    shields: 925.830078125
    energy: 0.0
  }
  total_damage_taken {
    life: 1103.45800781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 223.360351562
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1217
vespene: 728
food_cap: 54
food_used: 54
food_army: 24
food_workers: 30
idle_worker_count: 1
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 57
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10564
score_details {
  idle_production_time: 9438.875
  idle_worker_time: 2289.375
  total_value_units: 12675.0
  total_value_structures: 5225.0
  killed_value_units: 5350.0
  killed_value_structures: 1275.0
  collected_minerals: 14455.0
  collected_vespene: 5972.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 313.0
  spent_minerals: 12425.0
  spent_vespene: 5075.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3350.0
    economy: 1025.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 6.0
    army: 6398.0
    economy: 50.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3934.0
    economy: 0.0
    technology: 175.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 775.0
    economy: 4550.0
    technology: 1450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 6825.0
    economy: 4750.0
    technology: 1700.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 4050.0
    economy: 0.0
    technology: 875.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 5421.83007812
    shields: 5688.49414062
    energy: 0.0
  }
  total_damage_taken {
    life: 11811.1933594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 776.416992188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1926
vespene: 788
food_cap: 133
food_used: 48
food_army: 13
food_workers: 35
idle_worker_count: 4
army_count: 5
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9263
score_details {
  idle_production_time: 9984.25
  idle_worker_time: 2556.4375
  total_value_units: 12900.0
  total_value_structures: 5225.0
  killed_value_units: 5350.0
  killed_value_structures: 1275.0
  collected_minerals: 15000.0
  collected_vespene: 6076.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 89.0
  spent_minerals: 12275.0
  spent_vespene: 5000.0
  food_used {
    none: 0.0
    army: 3.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3350.0
    economy: 1025.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 6.0
    army: 6898.0
    economy: 250.0
    technology: 700.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4209.0
    economy: 0.0
    technology: 475.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 125.0
    economy: 4350.0
    technology: 800.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 375.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 6975.0
    economy: 4750.0
    technology: 1700.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 4125.0
    economy: 0.0
    technology: 875.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 5422.33007812
    shields: 5748.99414062
    energy: 0.0
  }
  total_damage_taken {
    life: 16693.1933594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 776.416992188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2621
vespene: 967
food_cap: 125
food_used: 36
food_army: 3
food_workers: 33
idle_worker_count: 33
army_count: 2
warp_gate_count: 0

game_loop:  20660
ui_data{
 
Score:  9263
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
