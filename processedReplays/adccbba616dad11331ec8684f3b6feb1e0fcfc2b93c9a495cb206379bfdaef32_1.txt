----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 89
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3408
  player_apm: 68
}
game_duration_loops: 18037
game_duration_seconds: 805.279418945
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14267
score_details {
  idle_production_time: 1092.0625
  idle_worker_time: 52.6875
  total_value_units: 6175.0
  total_value_structures: 4850.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 10285.0
  collected_vespene: 2832.0
  collection_rate_minerals: 2631.0
  collection_rate_vespene: 627.0
  spent_minerals: 9800.0
  spent_vespene: 2425.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2100.0
    economy: 5850.0
    technology: 2350.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 500.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 2300.0
    economy: 5700.0
    technology: 1800.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 300.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 40.1025390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 240.0
    shields: 120.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 535
vespene: 407
food_cap: 133
food_used: 106
food_army: 44
food_workers: 59
idle_worker_count: 0
army_count: 21
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 19
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 25741
score_details {
  idle_production_time: 4705.875
  idle_worker_time: 1512.8125
  total_value_units: 19975.0
  total_value_structures: 8950.0
  killed_value_units: 11650.0
  killed_value_structures: 0.0
  collected_minerals: 24565.0
  collected_vespene: 7476.0
  collection_rate_minerals: 2631.0
  collection_rate_vespene: 895.0
  spent_minerals: 23425.0
  spent_vespene: 7075.0
  food_used {
    none: 0.0
    army: 129.0
    economy: 68.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7350.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4875.0
    economy: 375.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6850.0
    economy: 7700.0
    technology: 3450.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 0.0
    army: 2800.0
    economy: 0.0
    technology: 850.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 0.0
    army: 12025.0
    economy: 8000.0
    technology: 3600.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 0.0
    army: 5800.0
    economy: 0.0
    technology: 850.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 8689.6953125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5949.625
    shields: 8374.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1190
vespene: 401
food_cap: 200
food_used: 197
food_army: 129
food_workers: 68
idle_worker_count: 3
army_count: 55
warp_gate_count: 10

game_loop:  18037
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 53
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  25741
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
