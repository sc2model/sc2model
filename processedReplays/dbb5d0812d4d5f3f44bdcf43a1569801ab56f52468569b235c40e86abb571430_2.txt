----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4116
  player_apm: 167
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 117
}
game_duration_loops: 15592
game_duration_seconds: 696.119995117
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10605
score_details {
  idle_production_time: 1886.1875
  idle_worker_time: 218.6875
  total_value_units: 4875.0
  total_value_structures: 4175.0
  killed_value_units: 475.0
  killed_value_structures: 0.0
  collected_minerals: 8185.0
  collected_vespene: 1920.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 582.0
  spent_minerals: 8125.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1950.0
    economy: 4650.0
    technology: 1675.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 700.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4550.0
    technology: 1675.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 752.646484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 656.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 110
vespene: 95
food_cap: 86
food_used: 79
food_army: 36
food_workers: 41
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 4
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9409
score_details {
  idle_production_time: 4027.6875
  idle_worker_time: 561.25
  total_value_units: 10575.0
  total_value_structures: 6075.0
  killed_value_units: 7775.0
  killed_value_structures: 0.0
  collected_minerals: 14695.0
  collected_vespene: 4488.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 358.0
  spent_minerals: 14612.0
  spent_vespene: 3812.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5250.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5000.0
    economy: 2899.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1975.0
    economy: 74.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1150.0
    economy: 3275.0
    technology: 2175.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 900.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 5500.0
    economy: 6400.0
    technology: 2375.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 2075.0
    economy: 0.0
    technology: 900.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 9316.15820312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13644.3408203
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 133
vespene: 676
food_cap: 86
food_used: 55
food_army: 23
food_workers: 32
idle_worker_count: 3
army_count: 4
warp_gate_count: 0

game_loop:  15592
ui_data{
 multi {
  units {
    unit_type: 484
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  9409
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
