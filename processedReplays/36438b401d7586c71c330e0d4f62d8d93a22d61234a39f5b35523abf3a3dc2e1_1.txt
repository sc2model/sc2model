----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3036
  player_apm: 74
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3126
  player_apm: 55
}
game_duration_loops: 8297
game_duration_seconds: 370.427642822
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4481
score_details {
  idle_production_time: 984.0
  idle_worker_time: 41.25
  total_value_units: 3925.0
  total_value_structures: 3050.0
  killed_value_units: 350.0
  killed_value_structures: 0.0
  collected_minerals: 6080.0
  collected_vespene: 1376.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 335.0
  spent_minerals: 5800.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 5.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1475.0
    economy: 1400.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 275.0
    economy: 1850.0
    technology: 1300.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 3250.0
    technology: 1150.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 869.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4033.82543945
    shields: 4325.32128906
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 330
vespene: 151
food_cap: 23
food_used: 26
food_army: 5
food_workers: 21
idle_worker_count: 1
army_count: 1
warp_gate_count: 0

game_loop:  8297
ui_data{
 
Score:  4481
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
