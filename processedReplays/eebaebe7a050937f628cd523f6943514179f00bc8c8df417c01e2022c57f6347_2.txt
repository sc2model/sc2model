----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3061
  player_apm: 58
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3249
  player_apm: 95
}
game_duration_loops: 27935
game_duration_seconds: 1247.18530273
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9253
score_details {
  idle_production_time: 1913.875
  idle_worker_time: 545.9375
  total_value_units: 4500.0
  total_value_structures: 3125.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 6675.0
  collected_vespene: 1928.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 313.0
  spent_minerals: 6075.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 2200.0
    economy: 2850.0
    technology: 1275.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 975.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 2050.0
    economy: 3550.0
    technology: 1275.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 950.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 711.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 620.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 650
vespene: 303
food_cap: 70
food_used: 62
food_army: 40
food_workers: 22
idle_worker_count: 3
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
multi {
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 62
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 62
  }
}

score{
 score_type: Melee
score: 18095
score_details {
  idle_production_time: 9391.8125
  idle_worker_time: 2748.875
  total_value_units: 10575.0
  total_value_structures: 8675.0
  killed_value_units: 5700.0
  killed_value_structures: 1275.0
  collected_minerals: 16540.0
  collected_vespene: 5404.0
  collection_rate_minerals: 643.0
  collection_rate_vespene: 313.0
  spent_minerals: 16500.0
  spent_vespene: 4400.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3700.0
    economy: 1175.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1450.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2600.0
    economy: 1350.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3150.0
    economy: 5650.0
    technology: 3800.0
    upgrade: 500.0
  }
  used_vespene {
    none: 50.0
    army: 2025.0
    economy: 0.0
    technology: 1325.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 50.0
    army: 5250.0
    economy: 7050.0
    technology: 4100.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 50.0
    army: 2325.0
    economy: 0.0
    technology: 1325.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 11414.6699219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7793.06445312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1221.46972656
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 64
vespene: 981
food_cap: 165
food_used: 104
food_army: 60
food_workers: 44
idle_worker_count: 27
army_count: 19
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 22
  count: 2
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13230
score_details {
  idle_production_time: 18846.75
  idle_worker_time: 6049.0
  total_value_units: 13450.0
  total_value_structures: 8925.0
  killed_value_units: 11775.0
  killed_value_structures: 1450.0
  collected_minerals: 20205.0
  collected_vespene: 5496.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 134.0
  spent_minerals: 18700.0
  spent_vespene: 4625.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8450.0
    economy: 1300.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2725.0
    economy: 150.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6274.0
    economy: 3450.0
    technology: 850.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2163.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 250.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1250.0
    economy: 3750.0
    technology: 3150.0
    upgrade: 500.0
  }
  used_vespene {
    none: 100.0
    army: 600.0
    economy: 0.0
    technology: 1200.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 100.0
    army: 7450.0
    economy: 7500.0
    technology: 4100.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 100.0
    army: 2700.0
    economy: 0.0
    technology: 1325.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 17641.4199219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20459.6855469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4372.32714844
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1272
vespene: 808
food_cap: 150
food_used: 41
food_army: 24
food_workers: 17
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  27935
ui_data{
 
Score:  13230
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
