----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 104
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3387
  player_apm: 62
}
game_duration_loops: 12229
game_duration_seconds: 545.975585938
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13087
score_details {
  idle_production_time: 1545.5
  idle_worker_time: 408.375
  total_value_units: 5675.0
  total_value_structures: 3700.0
  killed_value_units: 225.0
  killed_value_structures: 0.0
  collected_minerals: 9005.0
  collected_vespene: 2732.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 649.0
  spent_minerals: 6575.0
  spent_vespene: 2400.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1975.0
    economy: 4300.0
    technology: 1500.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2575.0
    economy: 4200.0
    technology: 1500.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 3500.0
    economy: 0.0
    technology: 300.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 456.451171875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 25.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2480
vespene: 332
food_cap: 94
food_used: 88
food_army: 42
food_workers: 46
idle_worker_count: 1
army_count: 15
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 14533
score_details {
  idle_production_time: 2317.6875
  idle_worker_time: 489.875
  total_value_units: 7425.0
  total_value_structures: 3800.0
  killed_value_units: 4300.0
  killed_value_structures: 2275.0
  collected_minerals: 11420.0
  collected_vespene: 3588.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 313.0
  spent_minerals: 8125.0
  spent_vespene: 2600.0
  food_used {
    none: 0.0
    army: 58.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2250.0
    economy: 2550.0
    technology: 1425.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 725.0
    economy: 900.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2800.0
    economy: 3400.0
    technology: 1350.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 4125.0
    economy: 4300.0
    technology: 1500.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 3700.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 14961.9619141
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1830.57519531
    shields: 2986.70117188
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3345
vespene: 988
food_cap: 102
food_used: 86
food_army: 58
food_workers: 28
idle_worker_count: 0
army_count: 23
warp_gate_count: 5

game_loop:  12229
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 198
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 34
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 24
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 66
    shields: 50
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
}

Score:  14533
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
