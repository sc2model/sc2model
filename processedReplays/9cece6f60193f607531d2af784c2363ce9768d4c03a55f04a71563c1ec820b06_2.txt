----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 128
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3414
  player_apm: 125
}
game_duration_loops: 15006
game_duration_seconds: 669.957458496
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8915
score_details {
  idle_production_time: 1721.625
  idle_worker_time: 140.0625
  total_value_units: 5675.0
  total_value_structures: 3600.0
  killed_value_units: 2500.0
  killed_value_structures: 0.0
  collected_minerals: 9335.0
  collected_vespene: 1580.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 268.0
  spent_minerals: 7500.0
  spent_vespene: 1075.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1400.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2400.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 3650.0
    technology: 1550.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 3100.0
    economy: 3700.0
    technology: 1550.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 3565.89697266
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1751.0
    shields: 1921.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1885
vespene: 505
food_cap: 102
food_used: 48
food_army: 12
food_workers: 36
idle_worker_count: 0
army_count: 6
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 62
  count: 5
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 13500
score_details {
  idle_production_time: 3587.0625
  idle_worker_time: 147.1875
  total_value_units: 12950.0
  total_value_structures: 4300.0
  killed_value_units: 6250.0
  killed_value_structures: 775.0
  collected_minerals: 14990.0
  collected_vespene: 3560.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 627.0
  spent_minerals: 14125.0
  spent_vespene: 3425.0
  food_used {
    none: 0.0
    army: 58.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3950.0
    economy: 2350.0
    technology: 425.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4575.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2950.0
    economy: 4750.0
    technology: 2150.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 8500.0
    economy: 4150.0
    technology: 1850.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 3750.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 11181.6816406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3566.125
    shields: 4595.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 915
vespene: 135
food_cap: 110
food_used: 99
food_army: 58
food_workers: 39
idle_worker_count: 1
army_count: 23
warp_gate_count: 7

game_loop:  15006
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 9
}
groups {
  control_group_index: 3
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 133
  count: 5
}
multi {
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.0625
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.09375
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
    build_progress: 0.0260416865349
  }
}

Score:  13500
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
