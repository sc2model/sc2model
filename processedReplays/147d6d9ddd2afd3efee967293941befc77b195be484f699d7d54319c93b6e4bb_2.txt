----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4007
  player_apm: 183
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 211
}
game_duration_loops: 13517
game_duration_seconds: 603.479614258
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11907
score_details {
  idle_production_time: 813.5
  idle_worker_time: 113.4375
  total_value_units: 6025.0
  total_value_structures: 4225.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 10205.0
  collected_vespene: 1952.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 470.0
  spent_minerals: 10175.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 59.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2775.0
    economy: 5175.0
    technology: 1725.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 475.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2925.0
    economy: 4975.0
    technology: 1725.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 475.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 200.0
    shields: 598.25
    energy: 0.0
  }
  total_damage_taken {
    life: 855.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 141.43359375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 80
vespene: 202
food_cap: 110
food_used: 104
food_army: 59
food_workers: 43
idle_worker_count: 0
army_count: 39
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 29
}
groups {
  control_group_index: 2
  leader_unit_type: 498
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 22
  count: 1
}
single {
  unit {
    unit_type: 18
    player_relative: 1
    health: 422
    shields: 0
    energy: 0
    build_progress: 0.201250016689
  }
}

score{
 score_type: Melee
score: 17172
score_details {
  idle_production_time: 2306.25
  idle_worker_time: 137.8125
  total_value_units: 10875.0
  total_value_structures: 5775.0
  killed_value_units: 5175.0
  killed_value_structures: 600.0
  collected_minerals: 16275.0
  collected_vespene: 3572.0
  collection_rate_minerals: 2659.0
  collection_rate_vespene: 694.0
  spent_minerals: 16225.0
  spent_vespene: 3150.0
  food_used {
    none: 0.0
    army: 101.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3325.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2450.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 4875.0
    economy: 6100.0
    technology: 2175.0
    upgrade: 725.0
  }
  used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 700.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 0.0
    army: 6625.0
    economy: 6600.0
    technology: 2175.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 700.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 3826.0
    shields: 4334.13085938
    energy: 0.0
  }
  total_damage_taken {
    life: 3336.20458984
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1389.59033203
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 100
vespene: 422
food_cap: 157
food_used: 156
food_army: 101
food_workers: 52
idle_worker_count: 0
army_count: 47
warp_gate_count: 0

game_loop:  13517
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 28
}
groups {
  control_group_index: 2
  leader_unit_type: 500
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 22
  count: 2
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 48
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 33
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 57
  }
}

Score:  17172
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
