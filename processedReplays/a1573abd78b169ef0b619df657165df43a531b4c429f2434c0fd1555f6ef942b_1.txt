----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3393
  player_apm: 97
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 208
}
game_duration_loops: 13004
game_duration_seconds: 580.57623291
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12330
score_details {
  idle_production_time: 1517.625
  idle_worker_time: 309.6875
  total_value_units: 5850.0
  total_value_structures: 4900.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 9595.0
  collected_vespene: 2160.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 649.0
  spent_minerals: 9475.0
  spent_vespene: 2050.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: -25.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2350.0
    economy: 5350.0
    technology: 2200.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 600.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 4900.0
    technology: 2050.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 528.77734375
    shields: 100.0
    energy: 0.0
  }
  total_damage_taken {
    life: 120.0
    shields: 190.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 170
vespene: 110
food_cap: 126
food_used: 89
food_army: 40
food_workers: 49
idle_worker_count: 1
army_count: 17
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 2
}
single {
  unit {
    unit_type: 65
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 11939
score_details {
  idle_production_time: 2661.5
  idle_worker_time: 814.75
  total_value_units: 9400.0
  total_value_structures: 5600.0
  killed_value_units: 2275.0
  killed_value_structures: 0.0
  collected_minerals: 13355.0
  collected_vespene: 3584.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 649.0
  spent_minerals: 13175.0
  spent_vespene: 3100.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3875.0
    economy: -75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1175.0
    economy: 5700.0
    technology: 2200.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 600.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 4800.0
    economy: 5700.0
    technology: 2200.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 600.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 3157.18505859
    shields: 100.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3200.0
    shields: 3072.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 230
vespene: 484
food_cap: 142
food_used: 76
food_army: 20
food_workers: 56
idle_worker_count: 56
army_count: 8
warp_gate_count: 6

game_loop:  13004
ui_data{
 
Score:  11939
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
