----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3452
  player_apm: 198
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3451
  player_apm: 81
}
game_duration_loops: 5439
game_duration_seconds: 242.829437256
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3147
score_details {
  idle_production_time: 2206.375
  idle_worker_time: 114.6875
  total_value_units: 2550.0
  total_value_structures: 1125.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 3185.0
  collected_vespene: 212.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 67.0
  spent_minerals: 3075.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 16.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 250.0
    technology: -125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 2225.0
    technology: 250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1000.0
    economy: 2675.0
    technology: 250.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 264.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2016.95996094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 160
vespene: 112
food_cap: 42
food_used: 20
food_army: 4
food_workers: 16
idle_worker_count: 16
army_count: 0
warp_gate_count: 0

game_loop:  5439
ui_data{
 
Score:  3147
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
