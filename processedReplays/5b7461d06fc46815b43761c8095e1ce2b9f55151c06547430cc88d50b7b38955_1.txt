----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3502
  player_apm: 75
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3476
  player_apm: 69
}
game_duration_loops: 3568
game_duration_seconds: 159.296829224
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1849
score_details {
  idle_production_time: 480.625
  idle_worker_time: 101.5625
  total_value_units: 1500.0
  total_value_structures: 825.0
  killed_value_units: 25.0
  killed_value_structures: 150.0
  collected_minerals: 1475.0
  collected_vespene: 180.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 44.0
  spent_minerals: 1481.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 5.0
    economy: 2.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 781.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 300.0
    economy: 1075.0
    technology: 250.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 200.0
    economy: 2075.0
    technology: 250.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 614.943847656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 869.390625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 44
vespene: 180
food_cap: 28
food_used: 7
food_army: 5
food_workers: 2
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  3568
ui_data{
 
Score:  1849
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
