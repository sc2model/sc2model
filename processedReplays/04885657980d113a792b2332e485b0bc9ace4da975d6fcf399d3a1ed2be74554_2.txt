----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5272
  player_apm: 194
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5004
  player_apm: 216
}
game_duration_loops: 12449
game_duration_seconds: 555.797729492
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10643
score_details {
  idle_production_time: 877.625
  idle_worker_time: 805.25
  total_value_units: 5375.0
  total_value_structures: 4225.0
  killed_value_units: 3150.0
  killed_value_structures: 0.0
  collected_minerals: 9545.0
  collected_vespene: 1580.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 604.0
  spent_minerals: 9325.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 850.0
    economy: 600.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2350.0
    economy: 4350.0
    technology: 1725.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 475.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 5150.0
    technology: 1675.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1736.0
    shields: 1904.5
    energy: 0.0
  }
  total_damage_taken {
    life: 2505.17431641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 674.936035156
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 263
vespene: 230
food_cap: 110
food_used: 87
food_army: 48
food_workers: 37
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 48
  count: 3
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 82
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1480
    shields: 0
    energy: 26
  }
}

score{
 score_type: Melee
score: 14222
score_details {
  idle_production_time: 1622.5625
  idle_worker_time: 884.1875
  total_value_units: 9525.0
  total_value_structures: 4750.0
  killed_value_units: 6275.0
  killed_value_structures: 300.0
  collected_minerals: 13090.0
  collected_vespene: 2764.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 649.0
  spent_minerals: 12975.0
  spent_vespene: 2625.0
  food_used {
    none: 0.0
    army: 90.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4000.0
    economy: 1550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1700.0
    economy: 650.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4350.0
    economy: 4900.0
    technology: 1725.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1575.0
    economy: 0.0
    technology: 475.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 5300.0
    economy: 5850.0
    technology: 1825.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 525.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 4113.0
    shields: 5289.5
    energy: 0.0
  }
  total_damage_taken {
    life: 3654.44921875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1594.70751953
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 158
vespene: 139
food_cap: 134
food_used: 134
food_army: 90
food_workers: 44
idle_worker_count: 1
army_count: 49
warp_gate_count: 0

game_loop:  12449
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 48
  count: 22
}
multi {
  units {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 12
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 20
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 122
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 49
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 57
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

Score:  14222
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
