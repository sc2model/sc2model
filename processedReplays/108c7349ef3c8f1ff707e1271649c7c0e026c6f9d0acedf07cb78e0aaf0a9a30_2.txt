----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3384
  player_apm: 50
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3357
  player_apm: 95
}
game_duration_loops: 12193
game_duration_seconds: 544.368347168
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10876
score_details {
  idle_production_time: 1288.125
  idle_worker_time: 146.5625
  total_value_units: 4700.0
  total_value_structures: 3600.0
  killed_value_units: 450.0
  killed_value_structures: 450.0
  collected_minerals: 8365.0
  collected_vespene: 1736.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 515.0
  spent_minerals: 8225.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 350.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 4150.0
    technology: 2450.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 200.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1625.0
    economy: 4000.0
    technology: 1700.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1044.0
    shields: 1166.0
    energy: 0.0
  }
  total_damage_taken {
    life: 294.0
    shields: 450.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 190
vespene: 161
food_cap: 78
food_used: 78
food_army: 33
food_workers: 44
idle_worker_count: 1
army_count: 17
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 63
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 72
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 74
  count: 5
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 378
    shields: 378
    energy: 0
    build_progress: 0.72884619236
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 355
    shields: 355
    energy: 0
    build_progress: 0.676923036575
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 336
    shields: 336
    energy: 0
    build_progress: 0.636538505554
  }
}

score{
 score_type: Melee
score: 13097
score_details {
  idle_production_time: 1783.875
  idle_worker_time: 281.625
  total_value_units: 8250.0
  total_value_structures: 5200.0
  killed_value_units: 3150.0
  killed_value_structures: 950.0
  collected_minerals: 11305.0
  collected_vespene: 2692.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 649.0
  spent_minerals: 10900.0
  spent_vespene: 2350.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2000.0
    economy: 550.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1275.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2625.0
    economy: 4900.0
    technology: 2600.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 200.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 3900.0
    economy: 5000.0
    technology: 2600.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 2837.25952148
    shields: 2988.50952148
    energy: 0.0
  }
  total_damage_taken {
    life: 984.0
    shields: 1400.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 455
vespene: 342
food_cap: 117
food_used: 93
food_army: 43
food_workers: 50
idle_worker_count: 1
army_count: 22
warp_gate_count: 7

game_loop:  12193
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 63
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 72
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 74
  count: 4
}

Score:  13097
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
