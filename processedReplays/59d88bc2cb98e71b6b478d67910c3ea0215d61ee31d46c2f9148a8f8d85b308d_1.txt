----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2844
  player_apm: 100
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3057
  player_apm: 57
}
game_duration_loops: 19674
game_duration_seconds: 878.364868164
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5650
score_details {
  idle_production_time: 5222.375
  idle_worker_time: 1465.5
  total_value_units: 5350.0
  total_value_structures: 1375.0
  killed_value_units: 950.0
  killed_value_structures: 0.0
  collected_minerals: 5150.0
  collected_vespene: 1600.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 0.0
  spent_minerals: 5025.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 2950.0
    technology: 775.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1300.0
    economy: 4150.0
    technology: 925.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1316.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2086.44628906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 175
vespene: 300
food_cap: 68
food_used: 36
food_army: 11
food_workers: 22
idle_worker_count: 6
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4311
score_details {
  idle_production_time: 12914.5625
  idle_worker_time: 5085.375
  total_value_units: 9650.0
  total_value_structures: 1900.0
  killed_value_units: 1450.0
  killed_value_structures: 100.0
  collected_minerals: 10275.0
  collected_vespene: 3636.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 335.0
  spent_minerals: 8800.0
  spent_vespene: 2300.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 400.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2200.0
    economy: 2025.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 50.0
    economy: 1550.0
    technology: -350.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3050.0
    economy: 6250.0
    technology: 1200.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2201.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7696.44287109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1525
vespene: 1336
food_cap: 30
food_used: 11
food_army: 2
food_workers: 9
idle_worker_count: 9
army_count: 0
warp_gate_count: 0

game_loop:  19674
ui_data{
 
Score:  4311
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
