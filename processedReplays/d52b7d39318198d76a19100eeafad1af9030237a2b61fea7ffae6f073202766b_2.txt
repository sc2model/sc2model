----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 72
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 156
}
game_duration_loops: 6168
game_duration_seconds: 275.376373291
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4465
score_details {
  idle_production_time: 5167.5625
  idle_worker_time: 0.0
  total_value_units: 3550.0
  total_value_structures: 1000.0
  killed_value_units: 775.0
  killed_value_structures: 100.0
  collected_minerals: 3885.0
  collected_vespene: 492.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 179.0
  spent_minerals: 3412.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 950.0
    economy: 2250.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 2500.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1967.75
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 922.893554688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 523
vespene: 92
food_cap: 52
food_used: 35
food_army: 17
food_workers: 18
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  6168
ui_data{
 multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  4465
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
