----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4184
  player_apm: 209
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4420
  player_apm: 300
}
game_duration_loops: 16134
game_duration_seconds: 720.318115234
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8417
score_details {
  idle_production_time: 9809.4375
  idle_worker_time: 237.6875
  total_value_units: 6800.0
  total_value_structures: 1625.0
  killed_value_units: 3500.0
  killed_value_structures: 0.0
  collected_minerals: 8145.0
  collected_vespene: 972.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 335.0
  spent_minerals: 7775.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2050.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4200.0
    technology: 1025.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3050.0
    economy: 4600.0
    technology: 1175.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 5746.78125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2653.78417969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 420
vespene: 72
food_cap: 90
food_used: 80
food_army: 41
food_workers: 39
idle_worker_count: 5
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 13
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13021
score_details {
  idle_production_time: 15995.5625
  idle_worker_time: 664.5625
  total_value_units: 14800.0
  total_value_structures: 2075.0
  killed_value_units: 10450.0
  killed_value_structures: 700.0
  collected_minerals: 15765.0
  collected_vespene: 3856.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 627.0
  spent_minerals: 15500.0
  spent_vespene: 3525.0
  food_used {
    none: 0.0
    army: 68.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7075.0
    economy: 1900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5775.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2600.0
    economy: 5975.0
    technology: 1150.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 150.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 8475.0
    economy: 6975.0
    technology: 1300.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 3450.0
    economy: 0.0
    technology: 250.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 19240.5625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12320.6318359
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 315
vespene: 331
food_cap: 176
food_used: 114
food_army: 68
food_workers: 46
idle_worker_count: 0
army_count: 22
warp_gate_count: 0

game_loop:  16134
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 22
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 688
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 81
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
}

Score:  13021
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
