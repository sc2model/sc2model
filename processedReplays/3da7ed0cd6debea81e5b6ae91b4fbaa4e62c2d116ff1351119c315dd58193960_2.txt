----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3965
  player_apm: 174
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3927
  player_apm: 174
}
game_duration_loops: 13175
game_duration_seconds: 588.210693359
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9346
score_details {
  idle_production_time: 11010.1875
  idle_worker_time: 1550.0625
  total_value_units: 7050.0
  total_value_structures: 1575.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 8480.0
  collected_vespene: 784.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 425.0
  spent_minerals: 8056.0
  spent_vespene: 512.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 731.0
    technology: -250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4900.0
    technology: 900.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2400.0
    economy: 6000.0
    technology: 775.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 480.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1558.13232422
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 474
vespene: 272
food_cap: 122
food_used: 70
food_army: 32
food_workers: 38
idle_worker_count: 0
army_count: 44
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 99
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
    build_progress: 0.00625002384186
  }
}

score{
 score_type: Melee
score: 8749
score_details {
  idle_production_time: 17341.0
  idle_worker_time: 1731.375
  total_value_units: 10850.0
  total_value_structures: 1950.0
  killed_value_units: 1825.0
  killed_value_structures: 0.0
  collected_minerals: 12085.0
  collected_vespene: 2132.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 559.0
  spent_minerals: 11906.0
  spent_vespene: 912.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4300.0
    economy: 1281.0
    technology: -250.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 75.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 5150.0
    technology: 1050.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 5400.0
    economy: 7150.0
    technology: 1200.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 2104.15087891
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10356.3466797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 229
vespene: 1220
food_cap: 122
food_used: 58
food_army: 8
food_workers: 50
idle_worker_count: 50
army_count: 4
warp_gate_count: 0

game_loop:  13175
ui_data{
 
Score:  8749
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
