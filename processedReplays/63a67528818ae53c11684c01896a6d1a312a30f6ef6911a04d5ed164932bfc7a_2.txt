----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5690
  player_apm: 313
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5690
  player_apm: 400
}
game_duration_loops: 22356
game_duration_seconds: 998.10534668
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11486
score_details {
  idle_production_time: 6211.125
  idle_worker_time: 26.5
  total_value_units: 8750.0
  total_value_structures: 1375.0
  killed_value_units: 2850.0
  killed_value_structures: 0.0
  collected_minerals: 11020.0
  collected_vespene: 1908.0
  collection_rate_minerals: 2407.0
  collection_rate_vespene: 694.0
  spent_minerals: 9849.0
  spent_vespene: 1518.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1906.0
    economy: 100.0
    technology: -107.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 518.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5725.0
    technology: 850.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 100.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 3600.0
    economy: 6375.0
    technology: 750.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3205.96411133
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5234.81640625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1221
vespene: 390
food_cap: 130
food_used: 103
food_army: 45
food_workers: 58
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 34
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 65
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18812
score_details {
  idle_production_time: 32430.4375
  idle_worker_time: 371.5
  total_value_units: 27150.0
  total_value_structures: 3575.0
  killed_value_units: 12850.0
  killed_value_structures: 500.0
  collected_minerals: 29286.0
  collected_vespene: 8468.0
  collection_rate_minerals: 2419.0
  collection_rate_vespene: 783.0
  spent_minerals: 29124.0
  spent_vespene: 8243.0
  food_used {
    none: 0.0
    army: 89.5
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9000.0
    economy: 1600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11306.0
    economy: 3325.0
    technology: 43.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4818.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4325.0
    economy: 8100.0
    technology: 1550.0
    upgrade: 1150.0
  }
  used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 600.0
    upgrade: 1150.0
  }
  total_used_minerals {
    none: 0.0
    army: 16250.0
    economy: 12475.0
    technology: 2050.0
    upgrade: 1150.0
  }
  total_used_vespene {
    none: 0.0
    army: 8800.0
    economy: 0.0
    technology: 850.0
    upgrade: 1150.0
  }
  total_damage_dealt {
    life: 15478.9638672
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25819.078125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 212
vespene: 225
food_cap: 200
food_used: 156
food_army: 89
food_workers: 67
idle_worker_count: 1
army_count: 53
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 31
}
groups {
  control_group_index: 2
  leader_unit_type: 499
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16139
score_details {
  idle_production_time: 40733.125
  idle_worker_time: 760.9375
  total_value_units: 31750.0
  total_value_structures: 3575.0
  killed_value_units: 16425.0
  killed_value_structures: 575.0
  collected_minerals: 34122.0
  collected_vespene: 9784.0
  collection_rate_minerals: 2749.0
  collection_rate_vespene: 739.0
  spent_minerals: 33424.0
  spent_vespene: 9593.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10600.0
    economy: 2675.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16781.0
    economy: 4325.0
    technology: 43.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6818.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2250.0
    economy: 7650.0
    technology: 1550.0
    upgrade: 1150.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 600.0
    upgrade: 1150.0
  }
  total_used_minerals {
    none: 0.0
    army: 20050.0
    economy: 13075.0
    technology: 2050.0
    upgrade: 1150.0
  }
  total_used_vespene {
    none: 0.0
    army: 10250.0
    economy: 0.0
    technology: 850.0
    upgrade: 1150.0
  }
  total_damage_dealt {
    life: 19523.9375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 36591.09375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 748
vespene: 191
food_cap: 200
food_used: 103
food_army: 38
food_workers: 65
idle_worker_count: 65
army_count: 10
warp_gate_count: 0

game_loop:  22356
ui_data{
 
Score:  16139
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
