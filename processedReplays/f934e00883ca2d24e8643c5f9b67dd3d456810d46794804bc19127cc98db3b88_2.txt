----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3948
  player_apm: 145
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3957
  player_apm: 193
}
game_duration_loops: 18413
game_duration_seconds: 822.06628418
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7081
score_details {
  idle_production_time: 5781.0625
  idle_worker_time: 23.9375
  total_value_units: 5750.0
  total_value_structures: 1750.0
  killed_value_units: 1900.0
  killed_value_structures: 0.0
  collected_minerals: 7240.0
  collected_vespene: 1572.0
  collection_rate_minerals: 503.0
  collection_rate_vespene: 179.0
  spent_minerals: 7131.0
  spent_vespene: 1225.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: 2081.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 2175.0
    technology: 1400.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 250.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 1600.0
    economy: 4975.0
    technology: 1675.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1742.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4568.96630859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 159
vespene: 347
food_cap: 60
food_used: 35
food_army: 22
food_workers: 13
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 141
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 139
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 139
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8326
score_details {
  idle_production_time: 15619.8125
  idle_worker_time: 188.75
  total_value_units: 12400.0
  total_value_structures: 1825.0
  killed_value_units: 9800.0
  killed_value_structures: 0.0
  collected_minerals: 13630.0
  collected_vespene: 3644.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 492.0
  spent_minerals: 13555.0
  spent_vespene: 3568.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6825.0
    economy: 1550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5106.0
    economy: 2281.0
    technology: 143.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2218.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 75.0
    economy: 75.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 825.0
    economy: 3875.0
    technology: 1400.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 250.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 5700.0
    economy: 6925.0
    technology: 1800.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 350.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 11317.0693359
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13044.0400391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 125
vespene: 76
food_cap: 92
food_used: 53
food_army: 16
food_workers: 37
idle_worker_count: 36
army_count: 2
warp_gate_count: 0

game_loop:  18413
ui_data{
 
Score:  8326
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
