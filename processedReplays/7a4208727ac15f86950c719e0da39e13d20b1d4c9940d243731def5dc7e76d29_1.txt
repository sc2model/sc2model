----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4529
  player_apm: 215
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4299
  player_apm: 268
}
game_duration_loops: 18646
game_duration_seconds: 832.468811035
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9384
score_details {
  idle_production_time: 1594.4375
  idle_worker_time: 377.25
  total_value_units: 5025.0
  total_value_structures: 3725.0
  killed_value_units: 975.0
  killed_value_structures: 100.0
  collected_minerals: 6995.0
  collected_vespene: 1964.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 335.0
  spent_minerals: 6625.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: -200.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 1600.0
    economy: 3750.0
    technology: 1575.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 525.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4000.0
    technology: 1625.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1637.5
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1170.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 420
vespene: 289
food_cap: 78
food_used: 64
food_army: 29
food_workers: 35
idle_worker_count: 2
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 56
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 56
    player_relative: 1
    health: 81
    shields: 0
    energy: 1
  }
}

score{
 score_type: Melee
score: 11668
score_details {
  idle_production_time: 6932.6875
  idle_worker_time: 1425.0625
  total_value_units: 16650.0
  total_value_structures: 6650.0
  killed_value_units: 10900.0
  killed_value_structures: 100.0
  collected_minerals: 18655.0
  collected_vespene: 6288.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 470.0
  spent_minerals: 18000.0
  spent_vespene: 6000.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7750.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8840.0
    economy: 600.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4310.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 5300.0
    technology: 2700.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 1225.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 9300.0
    economy: 6500.0
    technology: 2850.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 4300.0
    economy: 0.0
    technology: 1250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 9665.08691406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13069.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 330.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 615
vespene: 228
food_cap: 117
food_used: 64
food_army: 18
food_workers: 46
idle_worker_count: 46
army_count: 6
warp_gate_count: 0

game_loop:  18646
ui_data{
 
Score:  11668
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
