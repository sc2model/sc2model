----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5346
  player_apm: 282
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4758
  player_apm: 109
}
game_duration_loops: 19451
game_duration_seconds: 868.408813477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13424
score_details {
  idle_production_time: 1819.75
  idle_worker_time: 163.9375
  total_value_units: 4875.0
  total_value_structures: 5175.0
  killed_value_units: 175.0
  killed_value_structures: 0.0
  collected_minerals: 9810.0
  collected_vespene: 2488.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 582.0
  spent_minerals: 9287.0
  spent_vespene: 2237.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2600.0
    economy: 5750.0
    technology: 1825.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 900.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 6050.0
    technology: 1825.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 900.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 771.154296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 40.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 573
vespene: 251
food_cap: 125
food_used: 108
food_army: 51
food_workers: 54
idle_worker_count: 2
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 29
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16143
score_details {
  idle_production_time: 8983.0625
  idle_worker_time: 473.6875
  total_value_units: 22425.0
  total_value_structures: 8125.0
  killed_value_units: 9550.0
  killed_value_structures: 0.0
  collected_minerals: 25800.0
  collected_vespene: 8688.0
  collection_rate_minerals: 2855.0
  collection_rate_vespene: 918.0
  spent_minerals: 24537.0
  spent_vespene: 8437.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6800.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11950.0
    economy: 1800.0
    technology: -14.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5175.0
    economy: 0.0
    technology: -76.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 900.0
    economy: 7250.0
    technology: 2725.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 150.0
    technology: 1600.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 12250.0
    economy: 9650.0
    technology: 2825.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 5425.0
    economy: 300.0
    technology: 1600.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 12685.8398438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15929.4785156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 677.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1267
vespene: 251
food_cap: 200
food_used: 77
food_army: 18
food_workers: 59
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  19451
ui_data{
 
Score:  16143
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
