----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4661
  player_apm: 349
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4418
  player_apm: 148
}
game_duration_loops: 23034
game_duration_seconds: 1028.37536621
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11240
score_details {
  idle_production_time: 1162.0625
  idle_worker_time: 1256.8125
  total_value_units: 5925.0
  total_value_structures: 3250.0
  killed_value_units: 1550.0
  killed_value_structures: 0.0
  collected_minerals: 8300.0
  collected_vespene: 2640.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 627.0
  spent_minerals: 7850.0
  spent_vespene: 1925.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2350.0
    economy: 4800.0
    technology: 1100.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 4000.0
    technology: 1100.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1221.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 319.0
    shields: 566.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 500
vespene: 715
food_cap: 86
food_used: 85
food_army: 41
food_workers: 44
idle_worker_count: 1
army_count: 21
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 78
    player_relative: 1
    health: 49
    shields: 60
    energy: 67
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 115
    shields: 60
    energy: 75
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 20
    shields: 60
    energy: 30
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 12
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 40
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 93
  }
}

score{
 score_type: Melee
score: 20419
score_details {
  idle_production_time: 5062.4375
  idle_worker_time: 1529.1875
  total_value_units: 21075.0
  total_value_structures: 6950.0
  killed_value_units: 9900.0
  killed_value_structures: 75.0
  collected_minerals: 21400.0
  collected_vespene: 8044.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 671.0
  spent_minerals: 21150.0
  spent_vespene: 7775.0
  food_used {
    none: 0.0
    army: 99.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7425.0
    economy: 725.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5475.0
    economy: 1450.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5675.0
    economy: 6650.0
    technology: 2200.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 3175.0
    economy: 0.0
    technology: 750.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 11550.0
    economy: 7600.0
    technology: 2500.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 7425.0
    economy: 0.0
    technology: 750.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 10986.6396484
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6215.0
    shields: 10393.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 300
vespene: 269
food_cap: 181
food_used: 154
food_army: 99
food_workers: 53
idle_worker_count: 0
army_count: 38
warp_gate_count: 6

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 430
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 12
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 21016
score_details {
  idle_production_time: 6922.25
  idle_worker_time: 2477.5625
  total_value_units: 24900.0
  total_value_structures: 7350.0
  killed_value_units: 17725.0
  killed_value_structures: 275.0
  collected_minerals: 25215.0
  collected_vespene: 9676.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 335.0
  spent_minerals: 23625.0
  spent_vespene: 9075.0
  food_used {
    none: 0.0
    army: 87.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12300.0
    economy: 1925.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8425.0
    economy: 1900.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4850.0
    economy: 6550.0
    technology: 2200.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 3025.0
    economy: 0.0
    technology: 750.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 13675.0
    economy: 8400.0
    technology: 2500.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 8725.0
    economy: 0.0
    technology: 750.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 19684.1386719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8628.0
    shields: 13881.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1640
vespene: 601
food_cap: 196
food_used: 140
food_army: 87
food_workers: 52
idle_worker_count: 0
army_count: 34
warp_gate_count: 6

game_loop:  23034
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 4
  count: 22
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
single {
  unit {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
}

Score:  21016
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
