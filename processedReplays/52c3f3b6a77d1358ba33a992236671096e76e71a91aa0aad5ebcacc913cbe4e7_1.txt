----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5189
  player_apm: 233
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5397
  player_apm: 195
}
game_duration_loops: 11784
game_duration_seconds: 526.108154297
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5722
score_details {
  idle_production_time: 848.875
  idle_worker_time: 146.625
  total_value_units: 3350.0
  total_value_structures: 2375.0
  killed_value_units: 525.0
  killed_value_structures: 1550.0
  collected_minerals: 5085.0
  collected_vespene: 1312.0
  collection_rate_minerals: 475.0
  collection_rate_vespene: 223.0
  spent_minerals: 4650.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 500.0
    technology: 1050.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 425.0
    economy: 300.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 775.0
    economy: 2650.0
    technology: 800.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1175.0
    economy: 2725.0
    technology: 950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 2521.70068359
    shields: 3150.04931641
    energy: 0.0
  }
  total_damage_taken {
    life: 2781.81860352
    shields: 3561.30639648
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 485
vespene: 312
food_cap: 47
food_used: 36
food_army: 14
food_workers: 22
idle_worker_count: 16
army_count: 6
warp_gate_count: 1

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 74
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 7
    shields: 80
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 7
    shields: 60
    energy: 46
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 61
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
}

score{
 score_type: Melee
score: 4058
score_details {
  idle_production_time: 881.5
  idle_worker_time: 317.1875
  total_value_units: 3675.0
  total_value_structures: 2575.0
  killed_value_units: 1300.0
  killed_value_structures: 1550.0
  collected_minerals: 5625.0
  collected_vespene: 1508.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 201.0
  spent_minerals: 5400.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 5.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 875.0
    economy: 500.0
    technology: 1050.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1025.0
    economy: 1050.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 275.0
    economy: 2100.0
    technology: 600.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1300.0
    economy: 3075.0
    technology: 950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 2827.70068359
    shields: 3802.04931641
    energy: 0.0
  }
  total_damage_taken {
    life: 4905.92578125
    shields: 6306.2421875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 275
vespene: 533
food_cap: 47
food_used: 28
food_army: 5
food_workers: 22
idle_worker_count: 0
army_count: 3
warp_gate_count: 2

game_loop:  11784
ui_data{
 multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

Score:  4058
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
