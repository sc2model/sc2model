----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3504
  player_apm: 100
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 195
}
game_duration_loops: 13045
game_duration_seconds: 582.406738281
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12309
score_details {
  idle_production_time: 1164.1875
  idle_worker_time: 17.375
  total_value_units: 6575.0
  total_value_structures: 4100.0
  killed_value_units: 2000.0
  killed_value_structures: 175.0
  collected_minerals: 10165.0
  collected_vespene: 2344.0
  collection_rate_minerals: 2015.0
  collection_rate_vespene: 671.0
  spent_minerals: 9600.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 1175.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5400.0
    technology: 1950.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 500.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 5050.0
    technology: 1550.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 4080.81689453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 679.0
    shields: 936.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 615
vespene: 569
food_cap: 118
food_used: 87
food_army: 34
food_workers: 53
idle_worker_count: 0
army_count: 17
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 77
    shields: 2
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 51
    energy: 0
  }
}

score{
 score_type: Melee
score: 15545
score_details {
  idle_production_time: 2533.9375
  idle_worker_time: 17.375
  total_value_units: 10625.0
  total_value_structures: 6150.0
  killed_value_units: 5000.0
  killed_value_structures: 825.0
  collected_minerals: 15000.0
  collected_vespene: 4120.0
  collection_rate_minerals: 2323.0
  collection_rate_vespene: 963.0
  spent_minerals: 13725.0
  spent_vespene: 2650.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3250.0
    economy: 2475.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3550.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2175.0
    economy: 5950.0
    technology: 2700.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 500.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 5725.0
    economy: 6000.0
    technology: 2700.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 500.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 9809.94238281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2350.375
    shields: 3469.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1325
vespene: 1470
food_cap: 149
food_used: 98
food_army: 38
food_workers: 60
idle_worker_count: 0
army_count: 17
warp_gate_count: 10

game_loop:  13045
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 16
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 10
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 68
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 52
    shields: 18
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 73
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 68
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 22
    shields: 9
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 27
    energy: 0
  }
}

Score:  15545
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
