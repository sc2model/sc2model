----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4332
  player_apm: 295
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4189
  player_apm: 137
}
game_duration_loops: 11342
game_duration_seconds: 506.374633789
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12327
score_details {
  idle_production_time: 1229.0
  idle_worker_time: 125.4375
  total_value_units: 6300.0
  total_value_structures: 5150.0
  killed_value_units: 625.0
  killed_value_structures: 0.0
  collected_minerals: 10295.0
  collected_vespene: 2832.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 895.0
  spent_minerals: 9200.0
  spent_vespene: 2400.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 175.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 750.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 5250.0
    technology: 1800.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 6000.0
    technology: 1950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1141.953125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1193.0
    shields: 1061.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1145
vespene: 432
food_cap: 125
food_used: 82
food_army: 30
food_workers: 52
idle_worker_count: 0
army_count: 9
warp_gate_count: 1

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 3
}
single {
  unit {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 200
  }
}

score{
 score_type: Melee
score: 11047
score_details {
  idle_production_time: 1577.3125
  idle_worker_time: 154.0
  total_value_units: 8225.0
  total_value_structures: 5400.0
  killed_value_units: 4000.0
  killed_value_structures: 0.0
  collected_minerals: 12115.0
  collected_vespene: 3732.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 1007.0
  spent_minerals: 10650.0
  spent_vespene: 3475.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2550.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2300.0
    economy: 900.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 5400.0
    technology: 1650.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 700.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 6300.0
    technology: 2100.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 550.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 3528.34960938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2714.0
    shields: 3140.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1515
vespene: 257
food_cap: 125
food_used: 68
food_army: 13
food_workers: 55
idle_worker_count: 1
army_count: 2
warp_gate_count: 1

game_loop:  11342
ui_data{
 
Score:  11047
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
