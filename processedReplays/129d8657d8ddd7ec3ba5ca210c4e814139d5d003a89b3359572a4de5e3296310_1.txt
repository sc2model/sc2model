----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3430
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3470
  player_apm: 123
}
game_duration_loops: 16896
game_duration_seconds: 754.338378906
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8528
score_details {
  idle_production_time: 1606.0
  idle_worker_time: 377.5625
  total_value_units: 4300.0
  total_value_structures: 2975.0
  killed_value_units: 750.0
  killed_value_structures: 75.0
  collected_minerals: 7815.0
  collected_vespene: 988.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 335.0
  spent_minerals: 7375.0
  spent_vespene: 800.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 125.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 675.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1650.0
    economy: 4000.0
    technology: 1200.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 375.0
    economy: 0.0
    technology: 225.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1600.0
    economy: 4550.0
    technology: 1100.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 225.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 836.0
    shields: 841.875
    energy: 0.0
  }
  total_damage_taken {
    life: 950.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 50.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 490
vespene: 188
food_cap: 86
food_used: 64
food_army: 31
food_workers: 31
idle_worker_count: 1
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 2
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 14191
score_details {
  idle_production_time: 4029.6875
  idle_worker_time: 1090.6875
  total_value_units: 11700.0
  total_value_structures: 6275.0
  killed_value_units: 3775.0
  killed_value_structures: 75.0
  collected_minerals: 17265.0
  collected_vespene: 3476.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 671.0
  spent_minerals: 16825.0
  spent_vespene: 2650.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2525.0
    economy: 125.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5100.0
    economy: 1375.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2450.0
    economy: 5550.0
    technology: 2050.0
    upgrade: 750.0
  }
  used_vespene {
    none: 50.0
    army: 700.0
    economy: 0.0
    technology: 525.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 50.0
    army: 7300.0
    economy: 7450.0
    technology: 2250.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 50.0
    army: 1250.0
    economy: 0.0
    technology: 525.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 2971.0
    shields: 2954.75
    energy: 0.0
  }
  total_damage_taken {
    life: 11325.9570312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 506.452636719
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 490
vespene: 826
food_cap: 141
food_used: 91
food_army: 43
food_workers: 48
idle_worker_count: 48
army_count: 14
warp_gate_count: 0

game_loop:  16896
ui_data{
 
Score:  14191
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
