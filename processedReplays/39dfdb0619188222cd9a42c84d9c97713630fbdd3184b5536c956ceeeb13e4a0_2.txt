----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4257
  player_apm: 134
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4321
  player_apm: 208
}
game_duration_loops: 19066
game_duration_seconds: 851.220092773
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12704
score_details {
  idle_production_time: 991.8125
  idle_worker_time: 83.375
  total_value_units: 6700.0
  total_value_structures: 3675.0
  killed_value_units: 2075.0
  killed_value_structures: 0.0
  collected_minerals: 10105.0
  collected_vespene: 2100.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 649.0
  spent_minerals: 9150.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 62.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1400.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3000.0
    economy: 4950.0
    technology: 1250.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 425.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 3300.0
    economy: 5300.0
    technology: 1250.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 425.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3312.39379883
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1032.08056641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 783.981933594
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1004
vespene: 575
food_cap: 118
food_used: 115
food_army: 62
food_workers: 53
idle_worker_count: 0
army_count: 48
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 12
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 22
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 63
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 54
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 142
    shields: 0
    energy: 63
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 14333
score_details {
  idle_production_time: 5896.5625
  idle_worker_time: 1376.25
  total_value_units: 13850.0
  total_value_structures: 6100.0
  killed_value_units: 7875.0
  killed_value_structures: 6125.0
  collected_minerals: 18370.0
  collected_vespene: 5596.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 17050.0
  spent_vespene: 3850.0
  food_used {
    none: 0.0
    army: 86.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3450.0
    economy: 6550.0
    technology: 3175.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4100.0
    economy: 5025.0
    technology: 525.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4150.0
    economy: 2025.0
    technology: 1475.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 400.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 8250.0
    economy: 7500.0
    technology: 2000.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 700.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 37478.703125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19360.2109375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 8596.01269531
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1328
vespene: 1705
food_cap: 143
food_used: 86
food_army: 86
food_workers: 0
idle_worker_count: 0
army_count: 57
warp_gate_count: 0

game_loop:  19066
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 38
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 10
}
groups {
  control_group_index: 6
  leader_unit_type: 46
  count: 8
}
groups {
  control_group_index: 7
  leader_unit_type: 134
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 22
  count: 1
}

Score:  14333
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
