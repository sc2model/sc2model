----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3361
  player_apm: 85
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3062
  player_apm: 60
}
game_duration_loops: 23729
game_duration_seconds: 1059.40429688
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10875
score_details {
  idle_production_time: 3481.75
  idle_worker_time: 45.5625
  total_value_units: 5950.0
  total_value_structures: 1200.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 8975.0
  collected_vespene: 1900.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 627.0
  spent_minerals: 7100.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 975.0
    economy: 4700.0
    technology: 1000.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 200.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4700.0
    technology: 1000.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 375.0
    shields: 555.75
    energy: 0.0
  }
  total_damage_taken {
    life: 1621.91650391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1925
vespene: 600
food_cap: 92
food_used: 67
food_army: 20
food_workers: 43
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 28277
score_details {
  idle_production_time: 24991.625
  idle_worker_time: 311.8125
  total_value_units: 21650.0
  total_value_structures: 3050.0
  killed_value_units: 3075.0
  killed_value_structures: 500.0
  collected_minerals: 24815.0
  collected_vespene: 8712.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 1097.0
  spent_minerals: 21325.0
  spent_vespene: 7825.0
  food_used {
    none: 0.0
    army: 118.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2125.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3950.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6250.0
    economy: 8300.0
    technology: 1400.0
    upgrade: 2075.0
  }
  used_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 550.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 0.0
    army: 10200.0
    economy: 9150.0
    technology: 1750.0
    upgrade: 1575.0
  }
  total_used_vespene {
    none: 0.0
    army: 5000.0
    economy: 0.0
    technology: 800.0
    upgrade: 1575.0
  }
  total_damage_dealt {
    life: 3393.0
    shields: 3924.75
    energy: 0.0
  }
  total_damage_taken {
    life: 9651.08886719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3540
vespene: 887
food_cap: 200
food_used: 177
food_army: 118
food_workers: 59
idle_worker_count: 3
army_count: 32
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 6
}
multi {
  units {
    unit_type: 90
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
  units {
    unit_type: 90
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26476
score_details {
  idle_production_time: 43341.8125
  idle_worker_time: 2585.8125
  total_value_units: 28000.0
  total_value_structures: 3350.0
  killed_value_units: 13325.0
  killed_value_structures: 6900.0
  collected_minerals: 28250.0
  collected_vespene: 11576.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 985.0
  spent_minerals: 26875.0
  spent_vespene: 9025.0
  food_used {
    none: 0.0
    army: 115.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8450.0
    economy: 6200.0
    technology: 2500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 750.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9450.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6100.0
    economy: 8300.0
    technology: 1400.0
    upgrade: 2475.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 550.0
    upgrade: 2475.0
  }
  total_used_minerals {
    none: 0.0
    army: 15550.0
    economy: 9500.0
    technology: 1750.0
    upgrade: 2275.0
  }
  total_used_vespene {
    none: 0.0
    army: 6000.0
    economy: 0.0
    technology: 800.0
    upgrade: 2275.0
  }
  total_damage_dealt {
    life: 25248.0664062
    shields: 24720.4140625
    energy: 0.0
  }
  total_damage_taken {
    life: 19682.9453125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1425
vespene: 2551
food_cap: 200
food_used: 174
food_army: 115
food_workers: 59
idle_worker_count: 25
army_count: 136
warp_gate_count: 0

game_loop:  23729
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 7
}

Score:  26476
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
