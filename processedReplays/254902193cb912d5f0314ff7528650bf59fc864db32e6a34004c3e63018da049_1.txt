----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2676
  player_apm: 106
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2612
  player_apm: 95
}
game_duration_loops: 6124
game_duration_seconds: 273.41192627
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4177
score_details {
  idle_production_time: 4534.4375
  idle_worker_time: 0.0
  total_value_units: 3350.0
  total_value_structures: 975.0
  killed_value_units: 575.0
  killed_value_structures: 300.0
  collected_minerals: 3795.0
  collected_vespene: 432.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 134.0
  spent_minerals: 3550.0
  spent_vespene: 325.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 950.0
    economy: 2175.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 2375.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1928.93847656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1281.46582031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 295
vespene: 107
food_cap: 52
food_used: 35
food_army: 17
food_workers: 18
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  6124
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 105
  count: 18
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  4177
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
