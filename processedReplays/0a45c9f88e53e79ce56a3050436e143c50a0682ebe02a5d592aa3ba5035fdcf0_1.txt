----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3754
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3774
  player_apm: 104
}
game_duration_loops: 10392
game_duration_seconds: 463.9609375
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12751
score_details {
  idle_production_time: 5555.1875
  idle_worker_time: 36.5625
  total_value_units: 7900.0
  total_value_structures: 1725.0
  killed_value_units: 275.0
  killed_value_structures: 0.0
  collected_minerals: 10145.0
  collected_vespene: 1812.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 671.0
  spent_minerals: 9281.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2450.0
    economy: 5650.0
    technology: 1225.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 100.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 6150.0
    technology: 1375.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 200.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 498.4140625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 802.084472656
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 914
vespene: 412
food_cap: 130
food_used: 105
food_army: 54
food_workers: 51
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 23
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 141
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 91
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 123
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13193
score_details {
  idle_production_time: 5904.5
  idle_worker_time: 36.5625
  total_value_units: 7900.0
  total_value_structures: 1725.0
  killed_value_units: 1350.0
  killed_value_structures: 0.0
  collected_minerals: 10795.0
  collected_vespene: 2004.0
  collection_rate_minerals: 2267.0
  collection_rate_vespene: 671.0
  spent_minerals: 10181.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 70.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3050.0
    economy: 5650.0
    technology: 1225.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 100.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 6150.0
    technology: 1375.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 200.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 2030.45019531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1720.70410156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 664
vespene: 304
food_cap: 130
food_used: 121
food_army: 70
food_workers: 51
idle_worker_count: 0
army_count: 19
warp_gate_count: 0

game_loop:  10392
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 19
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 15
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 152
    shields: 0
    energy: 104
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 141
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 127
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  13193
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
