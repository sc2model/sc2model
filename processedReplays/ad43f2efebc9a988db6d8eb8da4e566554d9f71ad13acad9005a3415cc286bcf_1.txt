----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3475
  player_apm: 110
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3580
  player_apm: 134
}
game_duration_loops: 6861
game_duration_seconds: 306.316009521
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5277
score_details {
  idle_production_time: 6453.375
  idle_worker_time: 1.0
  total_value_units: 4350.0
  total_value_structures: 975.0
  killed_value_units: 1500.0
  killed_value_structures: 0.0
  collected_minerals: 4415.0
  collected_vespene: 512.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 156.0
  spent_minerals: 4200.0
  spent_vespene: 425.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1175.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1775.0
    economy: 2325.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 2525.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3172.48291016
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1307.07763672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 265
vespene: 87
food_cap: 60
food_used: 49
food_army: 30
food_workers: 19
idle_worker_count: 0
army_count: 48
warp_gate_count: 0

game_loop:  6861
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
}

Score:  5277
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
