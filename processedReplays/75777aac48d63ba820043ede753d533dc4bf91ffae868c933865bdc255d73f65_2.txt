----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4415
  player_apm: 279
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4612
  player_apm: 201
}
game_duration_loops: 31552
game_duration_seconds: 1408.66967773
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7936
score_details {
  idle_production_time: 1330.0625
  idle_worker_time: 82.1875
  total_value_units: 4250.0
  total_value_structures: 2875.0
  killed_value_units: 1500.0
  killed_value_structures: 100.0
  collected_minerals: 6370.0
  collected_vespene: 1616.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 335.0
  spent_minerals: 6175.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1100.0
    economy: 3450.0
    technology: 1175.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 1350.0
    economy: 4150.0
    technology: 1125.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 700.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2120.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1362.03808594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 239.848144531
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 245
vespene: 191
food_cap: 62
food_used: 54
food_army: 21
food_workers: 31
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 56
    player_relative: 1
    health: 140
    shields: 0
    energy: 109
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21261
score_details {
  idle_production_time: 5959.6875
  idle_worker_time: 231.375
  total_value_units: 15600.0
  total_value_structures: 6325.0
  killed_value_units: 8250.0
  killed_value_structures: 325.0
  collected_minerals: 21090.0
  collected_vespene: 6396.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 671.0
  spent_minerals: 20100.0
  spent_vespene: 5150.0
  food_used {
    none: 0.0
    army: 82.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7000.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5400.0
    economy: 450.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 4400.0
    economy: 6350.0
    technology: 2525.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 100.0
    army: 2175.0
    economy: 0.0
    technology: 850.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 100.0
    army: 9350.0
    economy: 7150.0
    technology: 2525.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 100.0
    army: 2750.0
    economy: 0.0
    technology: 850.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 9712.45898438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9263.71191406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2063.46606445
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 932
vespene: 1129
food_cap: 149
food_used: 137
food_army: 82
food_workers: 55
idle_worker_count: 3
army_count: 44
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 32
  count: 20
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26908
score_details {
  idle_production_time: 14082.25
  idle_worker_time: 3582.5
  total_value_units: 27675.0
  total_value_structures: 7650.0
  killed_value_units: 25175.0
  killed_value_structures: 2425.0
  collected_minerals: 35584.0
  collected_vespene: 10548.0
  collection_rate_minerals: 1545.0
  collection_rate_vespene: 425.0
  spent_minerals: 32500.0
  spent_vespene: 9500.0
  food_used {
    none: 0.0
    army: 104.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 100.0
    army: 19300.0
    economy: 2100.0
    technology: 575.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 100.0
    army: 4975.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13579.0
    economy: 700.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3810.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5350.0
    economy: 7875.0
    technology: 2825.0
    upgrade: 1975.0
  }
  used_vespene {
    none: 100.0
    army: 2025.0
    economy: 150.0
    technology: 1050.0
    upgrade: 1975.0
  }
  total_used_minerals {
    none: 100.0
    army: 18400.0
    economy: 8725.0
    technology: 2825.0
    upgrade: 1775.0
  }
  total_used_vespene {
    none: 100.0
    army: 5325.0
    economy: 300.0
    technology: 1050.0
    upgrade: 1775.0
  }
  total_damage_dealt {
    life: 30257.2773438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19551.703125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4914.63085938
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2739
vespene: 744
food_cap: 196
food_used: 162
food_army: 104
food_workers: 58
idle_worker_count: 18
army_count: 73
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 19
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 689
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26285
score_details {
  idle_production_time: 15254.6875
  idle_worker_time: 5251.25
  total_value_units: 30350.0
  total_value_structures: 8950.0
  killed_value_units: 31000.0
  killed_value_structures: 3725.0
  collected_minerals: 37219.0
  collected_vespene: 10880.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 313.0
  spent_minerals: 35000.0
  spent_vespene: 10125.0
  food_used {
    none: 0.0
    army: 104.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 100.0
    army: 23850.0
    economy: 2975.0
    technology: 1000.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 100.0
    army: 6000.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16246.0
    economy: 750.0
    technology: 225.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4642.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5450.0
    economy: 7825.0
    technology: 3125.0
    upgrade: 1975.0
  }
  used_vespene {
    none: 100.0
    army: 2050.0
    economy: 150.0
    technology: 1250.0
    upgrade: 1975.0
  }
  total_used_minerals {
    none: 100.0
    army: 20300.0
    economy: 9525.0
    technology: 3125.0
    upgrade: 1975.0
  }
  total_used_vespene {
    none: 100.0
    army: 6100.0
    economy: 300.0
    technology: 1250.0
    upgrade: 1975.0
  }
  total_damage_dealt {
    life: 39204.7773438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22579.5703125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5589.08691406
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1853
vespene: 432
food_cap: 200
food_used: 161
food_army: 104
food_workers: 57
idle_worker_count: 17
army_count: 59
warp_gate_count: 0

game_loop:  31552
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 10
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 34
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
}

Score:  26285
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
