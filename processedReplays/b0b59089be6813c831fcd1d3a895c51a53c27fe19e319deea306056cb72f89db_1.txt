----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3427
  player_apm: 76
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3410
  player_apm: 61
}
game_duration_loops: 6612
game_duration_seconds: 295.199157715
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4087
score_details {
  idle_production_time: 312.0625
  idle_worker_time: 284.25
  total_value_units: 2050.0
  total_value_structures: 2050.0
  killed_value_units: 575.0
  killed_value_structures: 0.0
  collected_minerals: 3520.0
  collected_vespene: 1036.0
  collection_rate_minerals: 503.0
  collection_rate_vespene: 313.0
  spent_minerals: 3425.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 3.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 467.0
    technology: 225.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 150.0
    economy: 2250.0
    technology: 600.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 700.0
    economy: 2850.0
    technology: 700.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 861.341796875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3197.11035156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 668.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 101
vespene: 886
food_cap: 46
food_used: 22
food_army: 3
food_workers: 19
idle_worker_count: 5
army_count: 1
warp_gate_count: 0

game_loop:  6612
ui_data{
 single {
  unit {
    unit_type: 27
    player_relative: 1
    health: 1206
    shields: 0
    energy: 0
  }
}

Score:  4087
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
