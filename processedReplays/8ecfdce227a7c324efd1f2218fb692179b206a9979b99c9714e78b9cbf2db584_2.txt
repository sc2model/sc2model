----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3788
  player_apm: 111
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3850
  player_apm: 123
}
game_duration_loops: 7914
game_duration_seconds: 353.328216553
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8232
score_details {
  idle_production_time: 987.4375
  idle_worker_time: 82.375
  total_value_units: 4000.0
  total_value_structures: 3550.0
  killed_value_units: 2100.0
  killed_value_structures: 125.0
  collected_minerals: 6845.0
  collected_vespene: 612.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 0.0
  spent_minerals: 6325.0
  spent_vespene: 550.0
  food_used {
    none: 0.0
    army: 40.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1025.0
    economy: 850.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: -25.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2050.0
    economy: 3150.0
    technology: 1700.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 3200.0
    technology: 1700.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 3728.37939453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 400.0
    shields: 507.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 570
vespene: 62
food_cap: 86
food_used: 70
food_army: 40
food_workers: 30
idle_worker_count: 0
army_count: 20
warp_gate_count: 8

game_loop:  7914
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 136
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 77
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 40
    shields: 26
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 34
    shields: 22
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 4
    energy: 12
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 31
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 87
    shields: 39
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 72
    shields: 26
    energy: 0
  }
}

Score:  8232
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
