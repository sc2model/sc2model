----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4883
  player_apm: 218
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4673
  player_apm: 219
}
game_duration_loops: 13712
game_duration_seconds: 612.185546875
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10610
score_details {
  idle_production_time: 1314.75
  idle_worker_time: 140.625
  total_value_units: 5200.0
  total_value_structures: 3950.0
  killed_value_units: 1150.0
  killed_value_structures: 125.0
  collected_minerals: 8755.0
  collected_vespene: 1928.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 649.0
  spent_minerals: 8525.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 500.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 5050.0
    technology: 1200.0
    upgrade: 575.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 450.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5400.0
    technology: 1200.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 2566.63525391
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2275.70849609
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1813.39453125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 157
vespene: 228
food_cap: 109
food_used: 80
food_army: 34
food_workers: 45
idle_worker_count: 0
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 93
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 135
    shields: 0
    energy: 94
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 97
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15720
score_details {
  idle_production_time: 2646.375
  idle_worker_time: 485.875
  total_value_units: 8375.0
  total_value_structures: 5725.0
  killed_value_units: 4425.0
  killed_value_structures: 125.0
  collected_minerals: 14350.0
  collected_vespene: 3664.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 694.0
  spent_minerals: 13925.0
  spent_vespene: 3150.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3075.0
    economy: 500.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2000.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2250.0
    economy: 6050.0
    technology: 2225.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 650.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3950.0
    economy: 7150.0
    technology: 2225.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 650.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 4806.72363281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5862.99023438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3401.578125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 281
vespene: 514
food_cap: 141
food_used: 100
food_army: 45
food_workers: 52
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  13712
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 26
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 394
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
}

Score:  15720
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
