----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3342
  player_apm: 104
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3373
  player_apm: 71
}
game_duration_loops: 7346
game_duration_seconds: 327.969299316
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6063
score_details {
  idle_production_time: 571.25
  idle_worker_time: 187.5
  total_value_units: 3000.0
  total_value_structures: 2225.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 4815.0
  collected_vespene: 1048.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 313.0
  spent_minerals: 4425.0
  spent_vespene: 450.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 3475.0
    technology: 800.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 600.0
    economy: 3725.0
    technology: 600.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 232.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 624.0
    shields: 1768.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 440
vespene: 598
food_cap: 70
food_used: 43
food_army: 6
food_workers: 35
idle_worker_count: 0
army_count: 3
warp_gate_count: 2

game_loop:  7346
ui_data{
 multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  6063
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
