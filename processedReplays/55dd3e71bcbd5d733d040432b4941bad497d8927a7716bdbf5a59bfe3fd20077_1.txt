----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3555
  player_apm: 121
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 138
}
game_duration_loops: 8268
game_duration_seconds: 369.132904053
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9581
score_details {
  idle_production_time: 337.625
  idle_worker_time: 194.0625
  total_value_units: 4100.0
  total_value_structures: 3425.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 7425.0
  collected_vespene: 1556.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 582.0
  spent_minerals: 6850.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4550.0
    technology: 1250.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4750.0
    technology: 1250.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 375.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 431.0
    shields: 425.25
    energy: 0.0
  }
  total_damage_taken {
    life: 476.794433594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 60.697265625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 625
vespene: 431
food_cap: 86
food_used: 76
food_army: 27
food_workers: 47
idle_worker_count: 1
army_count: 19
warp_gate_count: 0

game_loop:  8268
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 45
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 102
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 74
  }
}

Score:  9581
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
