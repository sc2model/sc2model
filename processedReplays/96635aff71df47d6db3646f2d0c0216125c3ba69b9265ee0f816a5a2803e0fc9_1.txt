----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3578
  player_apm: 72
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 62
}
game_duration_loops: 6100
game_duration_seconds: 272.340423584
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5273
score_details {
  idle_production_time: 4541.125
  idle_worker_time: 18.8125
  total_value_units: 3100.0
  total_value_structures: 975.0
  killed_value_units: 400.0
  killed_value_structures: 350.0
  collected_minerals: 3750.0
  collected_vespene: 448.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 111.0
  spent_minerals: 3625.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 27.5
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 200.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1475.0
    economy: 2675.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 2675.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2839.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 53.2392578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 175
vespene: 298
food_cap: 52
food_used: 51
food_army: 27
food_workers: 24
idle_worker_count: 0
army_count: 35
warp_gate_count: 0

game_loop:  6100
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 25
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 2
  }
}

Score:  5273
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
