----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2443
  player_apm: 89
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 78
}
game_duration_loops: 7909
game_duration_seconds: 353.105010986
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5725
score_details {
  idle_production_time: 677.6875
  idle_worker_time: 185.125
  total_value_units: 2575.0
  total_value_structures: 2175.0
  killed_value_units: 325.0
  killed_value_structures: 0.0
  collected_minerals: 5295.0
  collected_vespene: 1048.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 223.0
  spent_minerals: 3850.0
  spent_vespene: 300.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 300.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 2650.0
    technology: 500.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1100.0
    economy: 3100.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 175.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 761.094238281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3016.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 267.08203125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1477
vespene: 748
food_cap: 62
food_used: 27
food_army: 4
food_workers: 23
idle_worker_count: 0
army_count: 1
warp_gate_count: 0

game_loop:  7909
ui_data{
 single {
  unit {
    unit_type: 27
    player_relative: 1
    health: 1185
    shields: 0
    energy: 0
  }
}

Score:  5725
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
