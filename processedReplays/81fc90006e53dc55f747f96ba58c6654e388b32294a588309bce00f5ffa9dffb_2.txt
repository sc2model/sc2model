----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3977
  player_apm: 180
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3894
  player_apm: 212
}
game_duration_loops: 9958
game_duration_seconds: 444.584594727
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11035
score_details {
  idle_production_time: 989.875
  idle_worker_time: 118.5
  total_value_units: 5800.0
  total_value_structures: 3400.0
  killed_value_units: 1350.0
  killed_value_structures: 0.0
  collected_minerals: 8315.0
  collected_vespene: 2320.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 649.0
  spent_minerals: 7100.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1650.0
    economy: 4750.0
    technology: 1200.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 500.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 4800.0
    technology: 900.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 720.0
    shields: 932.125
    energy: 0.0
  }
  total_damage_taken {
    life: 305.0
    shields: 557.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1265
vespene: 570
food_cap: 101
food_used: 75
food_army: 24
food_workers: 50
idle_worker_count: 0
army_count: 12
warp_gate_count: 3

game_loop:  9958
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 78
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 50
    energy: 199
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 45
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 100
    shields: 0
    energy: 50
  }
  units {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 65
  }
}

Score:  11035
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
