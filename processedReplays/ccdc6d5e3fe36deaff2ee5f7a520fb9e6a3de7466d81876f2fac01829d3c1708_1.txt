----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3381
  player_apm: 62
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3457
  player_apm: 57
}
game_duration_loops: 12294
game_duration_seconds: 548.877563477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7375
score_details {
  idle_production_time: 4156.375
  idle_worker_time: 42.0625
  total_value_units: 4900.0
  total_value_structures: 1825.0
  killed_value_units: 1350.0
  killed_value_structures: 100.0
  collected_minerals: 7395.0
  collected_vespene: 980.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 156.0
  spent_minerals: 5650.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1275.0
    economy: 450.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1150.0
    economy: 2525.0
    technology: 1200.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2225.0
    economy: 3475.0
    technology: 1450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 492.0
    shields: 1146.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3198.01953125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1795
vespene: 55
food_cap: 68
food_used: 42
food_army: 24
food_workers: 18
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
cargo {
  unit {
    unit_type: 95
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
    transport_slots_taken: 7
  }
  passengers {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 55
  }
  passengers {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  slots_available: 248
}

score{
 score_type: Melee
score: 7679
score_details {
  idle_production_time: 5471.1875
  idle_worker_time: 42.0625
  total_value_units: 5600.0
  total_value_structures: 2025.0
  killed_value_units: 4425.0
  killed_value_structures: 100.0
  collected_minerals: 8555.0
  collected_vespene: 1524.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 335.0
  spent_minerals: 5975.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2325.0
    economy: 450.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 325.0
    economy: 2525.0
    technology: 1300.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2750.0
    economy: 3475.0
    technology: 1550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 600.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1162.0
    shields: 3068.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6040.38085938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2630
vespene: 424
food_cap: 60
food_used: 24
food_army: 6
food_workers: 18
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  12294
ui_data{
 
Score:  7679
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
