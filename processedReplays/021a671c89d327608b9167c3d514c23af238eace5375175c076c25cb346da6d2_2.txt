----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4531
  player_apm: 221
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4283
  player_apm: 236
}
game_duration_loops: 12308
game_duration_seconds: 549.502624512
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11864
score_details {
  idle_production_time: 4175.25
  idle_worker_time: 6.8125
  total_value_units: 5850.0
  total_value_structures: 1900.0
  killed_value_units: 250.0
  killed_value_structures: 0.0
  collected_minerals: 8340.0
  collected_vespene: 2524.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 627.0
  spent_minerals: 8250.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 5100.0
    technology: 1450.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 300.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 1250.0
    economy: 5700.0
    technology: 1450.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 300.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 667.226074219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 327.195800781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 140
vespene: 624
food_cap: 114
food_used: 82
food_army: 34
food_workers: 46
idle_worker_count: 0
army_count: 11
warp_gate_count: 0
larva_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 90
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 107
  count: 5
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
    add_on {
      unit_type: 100
      build_progress: 0.237500011921
    }
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12207
score_details {
  idle_production_time: 6193.0
  idle_worker_time: 6.8125
  total_value_units: 9800.0
  total_value_structures: 2150.0
  killed_value_units: 2350.0
  killed_value_structures: 0.0
  collected_minerals: 11725.0
  collected_vespene: 3644.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 627.0
  spent_minerals: 11687.0
  spent_vespene: 2925.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 37.0
    army: 2675.0
    economy: 200.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 37.0
    army: 1025.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 5800.0
    technology: 1450.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 300.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 3525.0
    economy: 6750.0
    technology: 1600.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 3117.92236328
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5047.55322266
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 88
vespene: 719
food_cap: 162
food_used: 78
food_army: 28
food_workers: 49
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  12308
ui_data{
 multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1398
    shields: 0
    energy: 0
  }
}

Score:  12207
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
