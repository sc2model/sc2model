----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3623
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3575
  player_apm: 195
}
game_duration_loops: 6533
game_duration_seconds: 291.672149658
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3355
score_details {
  idle_production_time: 3606.6875
  idle_worker_time: 0.6875
  total_value_units: 2700.0
  total_value_structures: 975.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 3130.0
  collected_vespene: 568.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 134.0
  spent_minerals: 2762.0
  spent_vespene: 281.0
  food_used {
    none: 0.0
    army: 4.5
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1056.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 81.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 350.0
    economy: 1725.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: -25.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 2025.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1680.75976562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2116.28955078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 418
vespene: 287
food_cap: 36
food_used: 17
food_army: 4
food_workers: 13
idle_worker_count: 0
army_count: 1
warp_gate_count: 0

game_loop:  6533
ui_data{
 
Score:  3355
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
