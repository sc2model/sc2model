----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4776
  player_apm: 155
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4681
  player_apm: 201
}
game_duration_loops: 10143
game_duration_seconds: 452.844085693
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8104
score_details {
  idle_production_time: 3620.375
  idle_worker_time: 328.625
  total_value_units: 5800.0
  total_value_structures: 1825.0
  killed_value_units: 2075.0
  killed_value_structures: 400.0
  collected_minerals: 7475.0
  collected_vespene: 1860.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 627.0
  spent_minerals: 7281.0
  spent_vespene: 1275.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1450.0
    economy: 700.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 206.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1325.0
    economy: 4300.0
    technology: 725.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 100.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 5400.0
    technology: 1025.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4630.36669922
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4729.84521484
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 244
vespene: 585
food_cap: 98
food_used: 69
food_army: 30
food_workers: 39
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 688
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 105
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 105
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 89
  count: 1
}
single {
  unit {
    unit_type: 688
    player_relative: 1
    health: 86
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8247
score_details {
  idle_production_time: 3697.0625
  idle_worker_time: 328.625
  total_value_units: 6100.0
  total_value_structures: 1825.0
  killed_value_units: 2875.0
  killed_value_structures: 400.0
  collected_minerals: 7650.0
  collected_vespene: 1928.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 649.0
  spent_minerals: 7581.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1900.0
    economy: 800.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1075.0
    economy: 206.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1550.0
    economy: 4300.0
    technology: 725.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 100.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2275.0
    economy: 5400.0
    technology: 1025.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4880.36669922
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5076.015625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 119
vespene: 553
food_cap: 98
food_used: 75
food_army: 36
food_workers: 39
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  10143
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 688
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 105
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 105
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 89
  count: 1
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1363
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  8247
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
