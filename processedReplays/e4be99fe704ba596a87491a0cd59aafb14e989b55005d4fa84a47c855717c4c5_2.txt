----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3877
  player_apm: 221
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3889
  player_apm: 170
}
game_duration_loops: 30499
game_duration_seconds: 1361.6574707
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11587
score_details {
  idle_production_time: 1270.625
  idle_worker_time: 808.4375
  total_value_units: 4525.0
  total_value_structures: 4000.0
  killed_value_units: 825.0
  killed_value_structures: 0.0
  collected_minerals: 9005.0
  collected_vespene: 2232.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 694.0
  spent_minerals: 7875.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 375.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1325.0
    economy: 5200.0
    technology: 1350.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 725.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1575.0
    economy: 5475.0
    technology: 1250.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 675.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 1207.84912109
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 807.093505859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1180
vespene: 607
food_cap: 85
food_used: 79
food_army: 27
food_workers: 50
idle_worker_count: 1
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26466
score_details {
  idle_production_time: 7255.9375
  idle_worker_time: 4066.8125
  total_value_units: 15150.0
  total_value_structures: 9600.0
  killed_value_units: 9775.0
  killed_value_structures: 0.0
  collected_minerals: 24555.0
  collected_vespene: 7948.0
  collection_rate_minerals: 2939.0
  collection_rate_vespene: 649.0
  spent_minerals: 23937.0
  spent_vespene: 6937.0
  food_used {
    none: 0.0
    army: 97.0
    economy: 71.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7075.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 3425.0
    economy: 2350.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 0.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 4875.0
    economy: 9425.0
    technology: 2575.0
    upgrade: 1550.0
  }
  used_vespene {
    none: 100.0
    army: 2825.0
    economy: 450.0
    technology: 1250.0
    upgrade: 1550.0
  }
  total_used_minerals {
    none: 100.0
    army: 7175.0
    economy: 12425.0
    technology: 2375.0
    upgrade: 1200.0
  }
  total_used_vespene {
    none: 100.0
    army: 3025.0
    economy: 600.0
    technology: 1050.0
    upgrade: 1200.0
  }
  total_damage_dealt {
    life: 11192.1943359
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10493.4199219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 43.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 655
vespene: 1011
food_cap: 200
food_used: 168
food_army: 97
food_workers: 71
idle_worker_count: 8
army_count: 23
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 53
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 5
}
single {
  unit {
    unit_type: 22
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27310
score_details {
  idle_production_time: 18564.75
  idle_worker_time: 8727.3125
  total_value_units: 31775.0
  total_value_structures: 12225.0
  killed_value_units: 25350.0
  killed_value_structures: 0.0
  collected_minerals: 37045.0
  collected_vespene: 13552.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 873.0
  spent_minerals: 33837.0
  spent_vespene: 12462.0
  food_used {
    none: 0.0
    army: 74.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 16075.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 9400.0
    economy: 6925.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 4626.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 1425.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 4175.0
    economy: 7725.0
    technology: 2675.0
    upgrade: 1850.0
  }
  used_vespene {
    none: 100.0
    army: 2950.0
    economy: 450.0
    technology: 1300.0
    upgrade: 1850.0
  }
  total_used_minerals {
    none: 100.0
    army: 16550.0
    economy: 16000.0
    technology: 2775.0
    upgrade: 1850.0
  }
  total_used_vespene {
    none: 100.0
    army: 9575.0
    economy: 1500.0
    technology: 1300.0
    upgrade: 1850.0
  }
  total_damage_dealt {
    life: 30296.7675781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 28294.4199219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 690.25
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3127
vespene: 1008
food_cap: 200
food_used: 117
food_army: 74
food_workers: 43
idle_worker_count: 10
army_count: 24
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 691
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 691
    player_relative: 1
    health: 261
    shields: 0
    energy: 0
  }
  units {
    unit_type: 691
    player_relative: 1
    health: 368
    shields: 0
    energy: 0
  }
  units {
    unit_type: 691
    player_relative: 1
    health: 234
    shields: 0
    energy: 0
  }
  units {
    unit_type: 691
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 691
    player_relative: 1
    health: 66
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23222
score_details {
  idle_production_time: 19307.75
  idle_worker_time: 9119.625
  total_value_units: 34000.0
  total_value_structures: 12225.0
  killed_value_units: 27000.0
  killed_value_structures: 0.0
  collected_minerals: 37290.0
  collected_vespene: 13844.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 895.0
  spent_minerals: 33837.0
  spent_vespene: 12462.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 17125.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 11850.0
    economy: 6925.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 6301.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 1425.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1425.0
    economy: 7725.0
    technology: 2675.0
    upgrade: 1850.0
  }
  used_vespene {
    none: 100.0
    army: 1075.0
    economy: 450.0
    technology: 1300.0
    upgrade: 1850.0
  }
  total_used_minerals {
    none: 100.0
    army: 17900.0
    economy: 16000.0
    technology: 2775.0
    upgrade: 1850.0
  }
  total_used_vespene {
    none: 100.0
    army: 10450.0
    economy: 1500.0
    technology: 1300.0
    upgrade: 1850.0
  }
  total_damage_dealt {
    life: 32920.3867188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 30269.1699219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 690.25
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3372
vespene: 1300
food_cap: 200
food_used: 71
food_army: 28
food_workers: 43
idle_worker_count: 43
army_count: 10
warp_gate_count: 0

game_loop:  30499
ui_data{
 
Score:  23222
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
