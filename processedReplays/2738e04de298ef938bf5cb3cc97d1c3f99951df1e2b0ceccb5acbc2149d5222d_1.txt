----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3316
  player_apm: 127
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3359
  player_apm: 181
}
game_duration_loops: 10151
game_duration_seconds: 453.201263428
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6676
score_details {
  idle_production_time: 5208.1875
  idle_worker_time: 723.8125
  total_value_units: 4900.0
  total_value_structures: 2100.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 5825.0
  collected_vespene: 1576.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 313.0
  spent_minerals: 5550.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 325.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 2775.0
    technology: 1300.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4250.0
    technology: 1450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2072.12744141
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6713.50292969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 325
vespene: 326
food_cap: 70
food_used: 42
food_army: 14
food_workers: 28
idle_worker_count: 8
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 6122
score_details {
  idle_production_time: 5370.875
  idle_worker_time: 882.5625
  total_value_units: 5100.0
  total_value_structures: 2100.0
  killed_value_units: 1000.0
  killed_value_structures: 0.0
  collected_minerals: 5855.0
  collected_vespene: 1592.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 268.0
  spent_minerals: 5550.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 325.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 700.0
    economy: 2775.0
    technology: 800.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4250.0
    technology: 1450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2460.71826172
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7212.78369141
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 355
vespene: 342
food_cap: 64
food_used: 42
food_army: 14
food_workers: 28
idle_worker_count: 26
army_count: 7
warp_gate_count: 0

game_loop:  10151
ui_data{
 
Score:  6122
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
