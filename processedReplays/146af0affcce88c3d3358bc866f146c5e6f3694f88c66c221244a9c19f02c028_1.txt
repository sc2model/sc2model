----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4078
  player_apm: 146
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4085
  player_apm: 117
}
game_duration_loops: 16192
game_duration_seconds: 722.907592773
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12554
score_details {
  idle_production_time: 4621.5625
  idle_worker_time: 310.625
  total_value_units: 7850.0
  total_value_structures: 2225.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 10540.0
  collected_vespene: 1564.0
  collection_rate_minerals: 2715.0
  collection_rate_vespene: 873.0
  spent_minerals: 10300.0
  spent_vespene: 1225.0
  food_used {
    none: 4.5
    army: 23.5
    economy: 73.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2025.0
    economy: 6800.0
    technology: 1225.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 250.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 8150.0
    technology: 1375.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 591.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1078.09375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 290
vespene: 339
food_cap: 128
food_used: 101
food_army: 23
food_workers: 73
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 18
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 4
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 38
  }
}

score{
 score_type: Melee
score: 27378
score_details {
  idle_production_time: 24941.3125
  idle_worker_time: 532.625
  total_value_units: 19700.0
  total_value_structures: 3825.0
  killed_value_units: 5250.0
  killed_value_structures: 0.0
  collected_minerals: 23785.0
  collected_vespene: 6068.0
  collection_rate_minerals: 2883.0
  collection_rate_vespene: 895.0
  spent_minerals: 21350.0
  spent_vespene: 5725.0
  food_used {
    none: 0.0
    army: 120.5
    economy: 77.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1075.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6700.0
    economy: 8750.0
    technology: 2675.0
    upgrade: 1525.0
  }
  used_vespene {
    none: 0.0
    army: 2325.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1525.0
  }
  total_used_minerals {
    none: 0.0
    army: 9700.0
    economy: 10800.0
    technology: 2725.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 950.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 5041.50732422
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2943.81005859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2485
vespene: 343
food_cap: 200
food_used: 197
food_army: 120
food_workers: 77
idle_worker_count: 0
army_count: 106
warp_gate_count: 0

game_loop:  16192
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 129
  count: 95
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 9
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 126
  count: 3
}

Score:  27378
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
