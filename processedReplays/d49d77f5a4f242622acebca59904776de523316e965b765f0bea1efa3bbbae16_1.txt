----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 109
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3167
  player_apm: 60
}
game_duration_loops: 30989
game_duration_seconds: 1383.53405762
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11783
score_details {
  idle_production_time: 10997.125
  idle_worker_time: 29.9375
  total_value_units: 6150.0
  total_value_structures: 1700.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8635.0
  collected_vespene: 2048.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 627.0
  spent_minerals: 8375.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 58.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2775.0
    economy: 4350.0
    technology: 1750.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 400.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2875.0
    economy: 4500.0
    technology: 1650.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 300.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 310
vespene: 273
food_cap: 92
food_used: 92
food_army: 58
food_workers: 34
idle_worker_count: 0
army_count: 51
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 21
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 90
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 25715
score_details {
  idle_production_time: 42529.5625
  idle_worker_time: 364.4375
  total_value_units: 16850.0
  total_value_structures: 3725.0
  killed_value_units: 750.0
  killed_value_structures: 1100.0
  collected_minerals: 20380.0
  collected_vespene: 6760.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 470.0
  spent_minerals: 17400.0
  spent_vespene: 6075.0
  food_used {
    none: 0.0
    army: 127.5
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 1500.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6950.0
    economy: 6000.0
    technology: 2625.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 3675.0
    economy: 0.0
    technology: 750.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 9225.0
    economy: 7200.0
    technology: 2975.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 0.0
    army: 5225.0
    economy: 0.0
    technology: 1000.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 3783.625
    shields: 4908.95117188
    energy: 0.0
  }
  total_damage_taken {
    life: 3444.07421875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3030
vespene: 685
food_cap: 174
food_used: 167
food_army: 127
food_workers: 40
idle_worker_count: 3
army_count: 89
warp_gate_count: 0
larva_count: 17

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10747
score_details {
  idle_production_time: 64529.6875
  idle_worker_time: 2114.9375
  total_value_units: 21300.0
  total_value_structures: 5700.0
  killed_value_units: 1090.0
  killed_value_structures: 6300.0
  collected_minerals: 26485.0
  collected_vespene: 8712.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 25100.0
  spent_vespene: 8700.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 2.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 240.0
    economy: 3950.0
    technology: 2250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 950.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10675.0
    economy: 5400.0
    technology: 3200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5375.0
    economy: 0.0
    technology: 900.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4550.0
    technology: -350.0
    upgrade: 1325.0
  }
  used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 0.0
    army: 13175.0
    economy: 10200.0
    technology: 3300.0
    upgrade: 1325.0
  }
  total_used_vespene {
    none: 0.0
    army: 8125.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1325.0
  }
  total_damage_dealt {
    life: 14805.125
    shields: 18806.4433594
    energy: 0.0
  }
  total_damage_taken {
    life: 53022.6289062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1435
vespene: 12
food_cap: 104
food_used: 24
food_army: 22
food_workers: 2
idle_worker_count: 2
army_count: 5
warp_gate_count: 0

game_loop:  30000
ui_data{
 multi {
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8947
score_details {
  idle_production_time: 65374.5625
  idle_worker_time: 2226.625
  total_value_units: 21300.0
  total_value_structures: 6900.0
  killed_value_units: 1840.0
  killed_value_structures: 7575.0
  collected_minerals: 26485.0
  collected_vespene: 8712.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 25250.0
  spent_vespene: 8700.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 5.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 240.0
    economy: 5375.0
    technology: 2600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 1200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11275.0
    economy: 6100.0
    technology: 3200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5875.0
    economy: 0.0
    technology: 900.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 750.0
    economy: 4000.0
    technology: -350.0
    upgrade: 1325.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 0.0
    army: 13175.0
    economy: 11600.0
    technology: 3300.0
    upgrade: 1325.0
  }
  total_used_vespene {
    none: 0.0
    army: 8125.0
    economy: 0.0
    technology: 1300.0
    upgrade: 1325.0
  }
  total_damage_dealt {
    life: 17095.125
    shields: 20877.1933594
    energy: 0.0
  }
  total_damage_taken {
    life: 57654.78125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1285
vespene: 12
food_cap: 116
food_used: 19
food_army: 14
food_workers: 2
idle_worker_count: 1
army_count: 3
warp_gate_count: 0

game_loop:  30989
ui_data{
 multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 200
  }
}

Score:  8947
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
