----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2653
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2315
  player_apm: 62
}
game_duration_loops: 24916
game_duration_seconds: 1112.39904785
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8660
score_details {
  idle_production_time: 1076.8125
  idle_worker_time: 625.9375
  total_value_units: 3600.0
  total_value_structures: 2500.0
  killed_value_units: 3000.0
  killed_value_structures: 0.0
  collected_minerals: 6470.0
  collected_vespene: 2040.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 313.0
  spent_minerals: 4375.0
  spent_vespene: 1025.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1750.0
    economy: 1250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 900.0
    economy: 3050.0
    technology: 550.0
    upgrade: 0.0
  }
  used_vespene {
    none: 50.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 1625.0
    economy: 3150.0
    technology: 550.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 675.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 5163.47949219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 927.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2145
vespene: 1015
food_cap: 78
food_used: 48
food_army: 21
food_workers: 26
idle_worker_count: 2
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
multi {
  units {
    unit_type: 498
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18270
score_details {
  idle_production_time: 7547.3125
  idle_worker_time: 4690.9375
  total_value_units: 6475.0
  total_value_structures: 8875.0
  killed_value_units: 5075.0
  killed_value_structures: 0.0
  collected_minerals: 15485.0
  collected_vespene: 4384.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 67.0
  spent_minerals: 14087.0
  spent_vespene: 4287.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3525.0
    economy: 1500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1475.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 675.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1400.0
    economy: 8175.0
    technology: 3200.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 1300.0
    economy: 300.0
    technology: 1350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 2775.0
    economy: 8975.0
    technology: 3050.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 1650.0
    economy: 600.0
    technology: 1200.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 8083.39160156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1845.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1448
vespene: 97
food_cap: 200
food_used: 71
food_army: 30
food_workers: 41
idle_worker_count: 21
army_count: 10
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 24002
score_details {
  idle_production_time: 14478.1875
  idle_worker_time: 10338.625
  total_value_units: 7850.0
  total_value_structures: 11250.0
  killed_value_units: 7475.0
  killed_value_structures: 0.0
  collected_minerals: 21885.0
  collected_vespene: 6616.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 963.0
  spent_minerals: 16962.0
  spent_vespene: 5212.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5100.0
    economy: 1500.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2225.0
    economy: 1437.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1350.0
    economy: 187.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 950.0
    economy: 8900.0
    technology: 3100.0
    upgrade: 950.0
  }
  used_vespene {
    none: 50.0
    army: 875.0
    economy: 450.0
    technology: 1350.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 50.0
    army: 3325.0
    economy: 11900.0
    technology: 3200.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 50.0
    army: 2225.0
    economy: 1200.0
    technology: 1350.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 9994.15625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6286.32519531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 4973
vespene: 1404
food_cap: 200
food_used: 65
food_army: 21
food_workers: 44
idle_worker_count: 13
army_count: 9
warp_gate_count: 0

game_loop:  24916
ui_data{
 
Score:  24002
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
