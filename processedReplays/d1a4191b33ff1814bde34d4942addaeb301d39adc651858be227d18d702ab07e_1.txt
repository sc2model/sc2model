----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4116
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4216
  player_apm: 152
}
game_duration_loops: 10932
game_duration_seconds: 488.069763184
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6293
score_details {
  idle_production_time: 1507.625
  idle_worker_time: 386.6875
  total_value_units: 3875.0
  total_value_structures: 2850.0
  killed_value_units: 2100.0
  killed_value_structures: 350.0
  collected_minerals: 5600.0
  collected_vespene: 768.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 179.0
  spent_minerals: 5575.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1800.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 425.0
    economy: 525.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1975.0
    economy: 2600.0
    technology: 900.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2100.0
    economy: 3100.0
    technology: 900.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 4795.03125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1013.48242188
    shields: 1983.98242188
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 75
vespene: 93
food_cap: 94
food_used: 55
food_army: 38
food_workers: 17
idle_worker_count: 1
army_count: 19
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 84
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
}

score{
 score_type: Melee
score: 4235
score_details {
  idle_production_time: 1698.625
  idle_worker_time: 444.9375
  total_value_units: 4475.0
  total_value_structures: 2850.0
  killed_value_units: 2950.0
  killed_value_structures: 350.0
  collected_minerals: 6105.0
  collected_vespene: 880.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 156.0
  spent_minerals: 6025.0
  spent_vespene: 775.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2550.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2500.0
    economy: 525.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 50.0
    economy: 2750.0
    technology: 900.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 3150.0
    technology: 900.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 7104.51757812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2621.60742188
    shields: 3243.98242188
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 130
vespene: 105
food_cap: 94
food_used: 22
food_army: 2
food_workers: 18
idle_worker_count: 1
army_count: 1
warp_gate_count: 4

game_loop:  10932
ui_data{
 
Score:  4235
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
