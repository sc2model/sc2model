----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 44
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 71
}
game_duration_loops: 44235
game_duration_seconds: 1974.91455078
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12075
score_details {
  idle_production_time: 5683.6875
  idle_worker_time: 25.0
  total_value_units: 5900.0
  total_value_structures: 1825.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 8955.0
  collected_vespene: 2020.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 649.0
  spent_minerals: 8200.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2125.0
    economy: 5450.0
    technology: 1225.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 450.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 1875.0
    economy: 5000.0
    technology: 1175.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 400.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 180.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 125.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 805
vespene: 495
food_cap: 90
food_used: 89
food_army: 46
food_workers: 43
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27711
score_details {
  idle_production_time: 29453.0
  idle_worker_time: 91.125
  total_value_units: 16950.0
  total_value_structures: 4125.0
  killed_value_units: 9075.0
  killed_value_structures: 200.0
  collected_minerals: 23915.0
  collected_vespene: 8796.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 1119.0
  spent_minerals: 18900.0
  spent_vespene: 7325.0
  food_used {
    none: 0.0
    army: 76.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5200.0
    economy: 50.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3425.0
    economy: 7750.0
    technology: 2275.0
    upgrade: 2450.0
  }
  used_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 1200.0
    upgrade: 2450.0
  }
  total_used_minerals {
    none: 0.0
    army: 7675.0
    economy: 8900.0
    technology: 2725.0
    upgrade: 1275.0
  }
  total_used_vespene {
    none: 0.0
    army: 3875.0
    economy: 0.0
    technology: 1600.0
    upgrade: 1275.0
  }
  total_damage_dealt {
    life: 6698.54296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7356.34912109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 5065
vespene: 1471
food_cap: 188
food_used: 131
food_army: 76
food_workers: 55
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  20000
ui_data{
 multi {
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 95
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 132
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 481
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 407
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 31401
score_details {
  idle_production_time: 65261.4375
  idle_worker_time: 9965.75
  total_value_units: 26350.0
  total_value_structures: 4125.0
  killed_value_units: 17800.0
  killed_value_structures: 10775.0
  collected_minerals: 30665.0
  collected_vespene: 14636.0
  collection_rate_minerals: 363.0
  collection_rate_vespene: 335.0
  spent_minerals: 23850.0
  spent_vespene: 12275.0
  food_used {
    none: 0.0
    army: 62.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9100.0
    economy: 7050.0
    technology: 4425.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6450.0
    economy: 450.0
    technology: 1100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2950.0
    economy: 7750.0
    technology: 2275.0
    upgrade: 2700.0
  }
  used_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 1200.0
    upgrade: 2700.0
  }
  total_used_minerals {
    none: 0.0
    army: 12375.0
    economy: 8900.0
    technology: 2725.0
    upgrade: 2700.0
  }
  total_used_vespene {
    none: 0.0
    army: 8575.0
    economy: 0.0
    technology: 1600.0
    upgrade: 2700.0
  }
  total_damage_dealt {
    life: 52897.2226562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14858.4003906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 6865
vespene: 2361
food_cap: 188
food_used: 117
food_army: 62
food_workers: 55
idle_worker_count: 30
army_count: 26
warp_gate_count: 0

game_loop:  30000
ui_data{
 multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 56
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 104
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 106
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 29451
score_details {
  idle_production_time: 90896.875
  idle_worker_time: 26828.1875
  total_value_units: 37300.0
  total_value_structures: 4875.0
  killed_value_units: 21375.0
  killed_value_structures: 14600.0
  collected_minerals: 32435.0
  collected_vespene: 19816.0
  collection_rate_minerals: 223.0
  collection_rate_vespene: 940.0
  spent_minerals: 31900.0
  spent_vespene: 17475.0
  food_used {
    none: 0.0
    army: 90.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10800.0
    economy: 9675.0
    technology: 5975.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7425.0
    economy: 900.0
    technology: 1200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 9700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4350.0
    economy: 8650.0
    technology: 2275.0
    upgrade: 3325.0
  }
  used_vespene {
    none: 150.0
    army: 3100.0
    economy: 0.0
    technology: 1200.0
    upgrade: 3325.0
  }
  total_used_minerals {
    none: 300.0
    army: 18750.0
    economy: 10200.0
    technology: 2725.0
    upgrade: 3325.0
  }
  total_used_vespene {
    none: 300.0
    army: 13000.0
    economy: 0.0
    technology: 1600.0
    upgrade: 3325.0
  }
  total_damage_dealt {
    life: 70190.9296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23898.3261719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 585
vespene: 2341
food_cap: 200
food_used: 140
food_army: 90
food_workers: 50
idle_worker_count: 23
army_count: 28
warp_gate_count: 0

game_loop:  40000
ui_data{
 multi {
  units {
    unit_type: 109
    player_relative: 1
    health: 344
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 494
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 54
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 182
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 429
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 30261
score_details {
  idle_production_time: 106513.4375
  idle_worker_time: 32916.0
  total_value_units: 37300.0
  total_value_structures: 4875.0
  killed_value_units: 22675.0
  killed_value_structures: 16375.0
  collected_minerals: 33165.0
  collected_vespene: 22896.0
  collection_rate_minerals: 279.0
  collection_rate_vespene: 985.0
  spent_minerals: 31900.0
  spent_vespene: 17475.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11100.0
    economy: 11200.0
    technology: 6975.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7525.0
    economy: 1050.0
    technology: 1200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 10900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2550.0
    economy: 8650.0
    technology: 2275.0
    upgrade: 3325.0
  }
  used_vespene {
    none: 150.0
    army: 1900.0
    economy: 0.0
    technology: 1200.0
    upgrade: 3325.0
  }
  total_used_minerals {
    none: 300.0
    army: 18750.0
    economy: 10200.0
    technology: 2725.0
    upgrade: 3325.0
  }
  total_used_vespene {
    none: 300.0
    army: 13000.0
    economy: 0.0
    technology: 1600.0
    upgrade: 3325.0
  }
  total_damage_dealt {
    life: 75234.453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25854.3007812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1315
vespene: 5421
food_cap: 200
food_used: 104
food_army: 54
food_workers: 50
idle_worker_count: 23
army_count: 22
warp_gate_count: 0

game_loop:  44235
ui_data{
 
Score:  30261
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
