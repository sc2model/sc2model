----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5398
  player_apm: 288
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5316
  player_apm: 219
}
game_duration_loops: 10936
game_duration_seconds: 488.248352051
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12781
score_details {
  idle_production_time: 801.1875
  idle_worker_time: 155.25
  total_value_units: 6325.0
  total_value_structures: 3825.0
  killed_value_units: 2625.0
  killed_value_structures: 100.0
  collected_minerals: 10195.0
  collected_vespene: 2436.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 671.0
  spent_minerals: 9975.0
  spent_vespene: 1875.0
  food_used {
    none: 0.0
    army: 59.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1675.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2800.0
    economy: 5600.0
    technology: 1375.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 825.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 2950.0
    economy: 5500.0
    technology: 1075.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 50.0
    army: 775.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 1663.0
    shields: 2058.75
    energy: 0.0
  }
  total_damage_taken {
    life: 1209.35644531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 246.328613281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 270
vespene: 561
food_cap: 126
food_used: 111
food_army: 59
food_workers: 52
idle_worker_count: 1
army_count: 32
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 23
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 101
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 72
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 85
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 85
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 132
    shields: 0
    energy: 69
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12448
score_details {
  idle_production_time: 1010.9375
  idle_worker_time: 305.75
  total_value_units: 7525.0
  total_value_structures: 4125.0
  killed_value_units: 7400.0
  killed_value_structures: 200.0
  collected_minerals: 11510.0
  collected_vespene: 2888.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 649.0
  spent_minerals: 11450.0
  spent_vespene: 2150.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4675.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2175.0
    economy: 5800.0
    technology: 1375.0
    upgrade: 450.0
  }
  used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 350.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 50.0
    army: 3700.0
    economy: 5700.0
    technology: 1375.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 50.0
    army: 1025.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 3918.85327148
    shields: 5157.5625
    energy: 0.0
  }
  total_damage_taken {
    life: 2800.765625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 852.877441406
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 110
vespene: 738
food_cap: 126
food_used: 102
food_army: 46
food_workers: 56
idle_worker_count: 4
army_count: 22
warp_gate_count: 0

game_loop:  10936
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 7
}
multi {
  units {
    unit_type: 500
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 132
    shields: 0
    energy: 2
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 85
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
}

Score:  12448
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
