----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2479
  player_apm: 33
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 34
}
game_duration_loops: 9140
game_duration_seconds: 408.064178467
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8164
score_details {
  idle_production_time: 1216.0625
  idle_worker_time: 500.6875
  total_value_units: 2875.0
  total_value_structures: 4175.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 6780.0
  collected_vespene: 1284.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 246.0
  spent_minerals: 6825.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 750.0
    economy: 3500.0
    technology: 2825.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 600.0
    economy: 3450.0
    technology: 2525.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 20.0
    shields: 20.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 5
vespene: 409
food_cap: 70
food_used: 56
food_army: 15
food_workers: 40
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  9140
ui_data{
 production {
  unit {
    unit_type: 18
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.198529422283
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
  build_queue {
    unit_type: 45
    build_progress: 0.0
  }
}

Score:  8164
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
