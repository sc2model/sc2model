----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5346
  player_apm: 282
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4758
  player_apm: 109
}
game_duration_loops: 19451
game_duration_seconds: 868.408813477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 15758
score_details {
  idle_production_time: 4813.625
  idle_worker_time: 16.5
  total_value_units: 8100.0
  total_value_structures: 2700.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 12615.0
  collected_vespene: 2236.0
  collection_rate_minerals: 3191.0
  collection_rate_vespene: 963.0
  spent_minerals: 11943.0
  spent_vespene: 2125.0
  food_used {
    none: 0.0
    army: 36.5
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25.0
    economy: -57.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 225.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 7650.0
    technology: 2050.0
    upgrade: 950.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 550.0
    upgrade: 950.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 8250.0
    technology: 1950.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 40.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 771.154296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 722
vespene: 111
food_cap: 136
food_used: 117
food_army: 36
food_workers: 75
idle_worker_count: 0
army_count: 17
warp_gate_count: 0
larva_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 16
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1487
    shields: 0
    energy: 0
    build_progress: 0.990625023842
  }
}

score{
 score_type: Melee
score: 34548
score_details {
  idle_production_time: 24533.875
  idle_worker_time: 51.4375
  total_value_units: 28150.0
  total_value_structures: 3750.0
  killed_value_units: 18925.0
  killed_value_structures: 100.0
  collected_minerals: 34660.0
  collected_vespene: 10756.0
  collection_rate_minerals: 2687.0
  collection_rate_vespene: 1276.0
  spent_minerals: 27943.0
  spent_vespene: 9625.0
  food_used {
    none: 0.0
    army: 101.0
    economy: 77.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11950.0
    economy: 1800.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6800.0
    economy: -57.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1700.0
    economy: 225.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5300.0
    economy: 10700.0
    technology: 2600.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 0.0
    army: 3250.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 0.0
    army: 15250.0
    economy: 11500.0
    technology: 2800.0
    upgrade: 1725.0
  }
  total_used_vespene {
    none: 0.0
    army: 7200.0
    economy: 0.0
    technology: 1250.0
    upgrade: 1725.0
  }
  total_damage_dealt {
    life: 15929.4785156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12685.8398438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 6767
vespene: 1131
food_cap: 200
food_used: 178
food_army: 101
food_workers: 77
idle_worker_count: 0
army_count: 52
warp_gate_count: 0

game_loop:  19451
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 25
}
groups {
  control_group_index: 2
  leader_unit_type: 499
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 127
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 6
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 77
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 5
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 3
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 63
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 77
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 63
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  34548
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
