----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3265
  player_apm: 135
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3251
  player_apm: 146
}
game_duration_loops: 13768
game_duration_seconds: 614.68572998
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8728
score_details {
  idle_production_time: 971.4375
  idle_worker_time: 490.0625
  total_value_units: 4450.0
  total_value_structures: 3300.0
  killed_value_units: 1150.0
  killed_value_structures: 400.0
  collected_minerals: 6615.0
  collected_vespene: 1988.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 380.0
  spent_minerals: 6000.0
  spent_vespene: 1550.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 1150.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1150.0
    economy: 3950.0
    technology: 1250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 3950.0
    technology: 1050.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3682.01269531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 694.541503906
    shields: 1078.58349609
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 665
vespene: 438
food_cap: 102
food_used: 60
food_army: 21
food_workers: 37
idle_worker_count: 1
army_count: 7
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 14196
score_details {
  idle_production_time: 1997.25
  idle_worker_time: 945.8125
  total_value_units: 7625.0
  total_value_structures: 4800.0
  killed_value_units: 2450.0
  killed_value_structures: 500.0
  collected_minerals: 10910.0
  collected_vespene: 3636.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 627.0
  spent_minerals: 9625.0
  spent_vespene: 2600.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 1200.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2675.0
    economy: 5250.0
    technology: 1550.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3325.0
    economy: 5150.0
    technology: 1550.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 5896.01269531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1300.25
    shields: 2039.58349609
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1335
vespene: 1036
food_cap: 158
food_used: 89
food_army: 46
food_workers: 43
idle_worker_count: 3
army_count: 19
warp_gate_count: 6

game_loop:  13768
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 82
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 74
  count: 6
}
groups {
  control_group_index: 8
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 732
    player_relative: 1
    health: 30
    shields: 30
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 40
    shields: 60
    energy: 49
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 68
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 81
    shields: 60
    energy: 51
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 15
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 15
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 732
    player_relative: 1
    health: 30
    shields: 30
    energy: 0
  }
}

Score:  14196
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
