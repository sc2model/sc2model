----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4017
  player_apm: 163
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3549
  player_apm: 149
}
game_duration_loops: 23609
game_duration_seconds: 1054.04675293
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9500
score_details {
  idle_production_time: 1516.125
  idle_worker_time: 161.625
  total_value_units: 4525.0
  total_value_structures: 3200.0
  killed_value_units: 900.0
  killed_value_structures: 0.0
  collected_minerals: 7520.0
  collected_vespene: 1980.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 335.0
  spent_minerals: 6550.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1400.0
    economy: 3750.0
    technology: 1225.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2075.0
    economy: 4200.0
    technology: 1225.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1834.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1040.06933594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 138.848144531
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1020
vespene: 355
food_cap: 69
food_used: 61
food_army: 28
food_workers: 33
idle_worker_count: 2
army_count: 19
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 692
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 50
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 73
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 90
  }
}

score{
 score_type: Melee
score: 15923
score_details {
  idle_production_time: 8688.4375
  idle_worker_time: 4073.0
  total_value_units: 14850.0
  total_value_structures: 5950.0
  killed_value_units: 10975.0
  killed_value_structures: 650.0
  collected_minerals: 19870.0
  collected_vespene: 4828.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 111.0
  spent_minerals: 19100.0
  spent_vespene: 4175.0
  food_used {
    none: 0.0
    army: 73.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8150.0
    economy: 2450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6950.0
    economy: 700.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1675.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3650.0
    economy: 5350.0
    technology: 2150.0
    upgrade: 900.0
  }
  used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 600.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 50.0
    army: 9950.0
    economy: 6650.0
    technology: 2350.0
    upgrade: 725.0
  }
  total_used_vespene {
    none: 50.0
    army: 2350.0
    economy: 300.0
    technology: 600.0
    upgrade: 725.0
  }
  total_damage_dealt {
    life: 13823.4326172
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9857.35351562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1919.34912109
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 820
vespene: 653
food_cap: 133
food_used: 121
food_army: 73
food_workers: 48
idle_worker_count: 9
army_count: 50
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 32
  count: 28
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 9
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 5
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 20
    shields: 0
    energy: 4
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 14
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11155
score_details {
  idle_production_time: 11737.5625
  idle_worker_time: 6003.3125
  total_value_units: 19675.0
  total_value_structures: 6050.0
  killed_value_units: 16625.0
  killed_value_structures: 1075.0
  collected_minerals: 24190.0
  collected_vespene: 5140.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 22.0
  spent_minerals: 23200.0
  spent_vespene: 5050.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13200.0
    economy: 2550.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13350.0
    economy: 2475.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2900.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 850.0
    economy: 4075.0
    technology: 2150.0
    upgrade: 900.0
  }
  used_vespene {
    none: 50.0
    army: 450.0
    economy: 0.0
    technology: 600.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 50.0
    army: 13900.0
    economy: 6750.0
    technology: 2350.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 50.0
    army: 3225.0
    economy: 300.0
    technology: 600.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 20560.7558594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19339.1835938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2838.06689453
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1040
vespene: 90
food_cap: 118
food_used: 52
food_army: 17
food_workers: 35
idle_worker_count: 3
army_count: 6
warp_gate_count: 0

game_loop:  23609
ui_data{
 multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 200
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1446
    shields: 0
    energy: 18
  }
  units {
    unit_type: 22
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
  }
  units {
    unit_type: 22
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
  }
}

Score:  11155
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
