----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3139
  player_apm: 69
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3242
  player_apm: 72
}
game_duration_loops: 20014
game_duration_seconds: 893.544494629
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10860
score_details {
  idle_production_time: 1018.0
  idle_worker_time: 11.0
  total_value_units: 6250.0
  total_value_structures: 3350.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 8230.0
  collected_vespene: 2280.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 671.0
  spent_minerals: 7875.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2675.0
    economy: 4600.0
    technology: 1100.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2625.0
    economy: 4250.0
    technology: 1100.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 220.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 120.0
    shields: 166.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 405
vespene: 505
food_cap: 102
food_used: 88
food_army: 44
food_workers: 44
idle_worker_count: 0
army_count: 18
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 1
}
single {
  unit {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 10420
score_details {
  idle_production_time: 3974.625
  idle_worker_time: 1240.1875
  total_value_units: 14825.0
  total_value_structures: 6950.0
  killed_value_units: 5175.0
  killed_value_structures: 100.0
  collected_minerals: 18250.0
  collected_vespene: 6220.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 268.0
  spent_minerals: 18200.0
  spent_vespene: 4175.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3650.0
    economy: 600.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7750.0
    economy: 4025.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 525.0
    economy: 3775.0
    technology: 2600.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 250.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 8150.0
    economy: 7400.0
    technology: 2600.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 3375.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 6993.00976562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12121.8955078
    shields: 13600.7705078
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 100
vespene: 2045
food_cap: 86
food_used: 39
food_army: 9
food_workers: 30
idle_worker_count: 1
army_count: 3
warp_gate_count: 1

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 10443
score_details {
  idle_production_time: 3976.375
  idle_worker_time: 1241.0625
  total_value_units: 14825.0
  total_value_structures: 6950.0
  killed_value_units: 5175.0
  killed_value_structures: 100.0
  collected_minerals: 18265.0
  collected_vespene: 6228.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 268.0
  spent_minerals: 18200.0
  spent_vespene: 4175.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3650.0
    economy: 600.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7750.0
    economy: 4025.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 525.0
    economy: 3775.0
    technology: 2600.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 250.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 8150.0
    economy: 7400.0
    technology: 2600.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 3375.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 6993.00976562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 12121.8955078
    shields: 13672.7705078
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 115
vespene: 2053
food_cap: 86
food_used: 39
food_army: 9
food_workers: 30
idle_worker_count: 1
army_count: 3
warp_gate_count: 1

game_loop:  20014
ui_data{
 multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

Score:  10443
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
