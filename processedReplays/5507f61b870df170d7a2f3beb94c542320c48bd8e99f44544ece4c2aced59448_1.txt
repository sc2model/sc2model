----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4133
  player_apm: 113
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4283
  player_apm: 142
}
game_duration_loops: 11996
game_duration_seconds: 535.573059082
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12050
score_details {
  idle_production_time: 1253.25
  idle_worker_time: 182.6875
  total_value_units: 5775.0
  total_value_structures: 4025.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 9550.0
  collected_vespene: 2200.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 627.0
  spent_minerals: 9400.0
  spent_vespene: 1950.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2200.0
    economy: 5200.0
    technology: 2000.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 575.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2450.0
    economy: 5350.0
    technology: 1450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 875.0
    economy: 0.0
    technology: 475.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 60.0
    shields: 40.0
    energy: 0.0
  }
  total_damage_taken {
    life: 374.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 200
vespene: 250
food_cap: 93
food_used: 93
food_army: 44
food_workers: 49
idle_worker_count: 1
army_count: 19
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 45
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 996
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 9250
score_details {
  idle_production_time: 2109.3125
  idle_worker_time: 348.125
  total_value_units: 7100.0
  total_value_structures: 5075.0
  killed_value_units: 3525.0
  killed_value_structures: 100.0
  collected_minerals: 12070.0
  collected_vespene: 3080.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 470.0
  spent_minerals: 10400.0
  spent_vespene: 2175.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1150.0
    economy: 1950.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3250.0
    economy: 1250.0
    technology: 650.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 100.0
    economy: 4100.0
    technology: 1200.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 325.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 3300.0
    economy: 5900.0
    technology: 2050.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 1100.0
    economy: 0.0
    technology: 625.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 2089.5
    shields: 3455.5
    energy: 0.0
  }
  total_damage_taken {
    life: 12713.1894531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 376.685058594
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1720
vespene: 905
food_cap: 93
food_used: 33
food_army: 2
food_workers: 31
idle_worker_count: 31
army_count: 0
warp_gate_count: 0

game_loop:  11996
ui_data{
 
Score:  9250
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
