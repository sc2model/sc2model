----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3058
  player_apm: 127
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2952
  player_apm: 233
}
game_duration_loops: 31631
game_duration_seconds: 1412.19677734
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11330
score_details {
  idle_production_time: 9943.125
  idle_worker_time: 14.3125
  total_value_units: 6400.0
  total_value_structures: 1700.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8445.0
  collected_vespene: 2260.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 671.0
  spent_minerals: 8425.0
  spent_vespene: 1575.0
  food_used {
    none: 11.5
    army: 12.0
    economy: 57.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5300.0
    technology: 950.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 250.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 6000.0
    technology: 1100.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 176.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 827.233398438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 70
vespene: 685
food_cap: 106
food_used: 80
food_army: 12
food_workers: 57
idle_worker_count: 0
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21389
score_details {
  idle_production_time: 37754.125
  idle_worker_time: 833.9375
  total_value_units: 22000.0
  total_value_structures: 2475.0
  killed_value_units: 3700.0
  killed_value_structures: 300.0
  collected_minerals: 25495.0
  collected_vespene: 6544.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 604.0
  spent_minerals: 22375.0
  spent_vespene: 6250.0
  food_used {
    none: 2.0
    army: 81.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2825.0
    economy: 200.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 7150.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4350.0
    economy: 6800.0
    technology: 1475.0
    upgrade: 1225.0
  }
  used_vespene {
    none: 150.0
    army: 2200.0
    economy: 0.0
    technology: 350.0
    upgrade: 1225.0
  }
  total_used_minerals {
    none: 300.0
    army: 13100.0
    economy: 9000.0
    technology: 1625.0
    upgrade: 1225.0
  }
  total_used_vespene {
    none: 300.0
    army: 5425.0
    economy: 0.0
    technology: 450.0
    upgrade: 1225.0
  }
  total_damage_dealt {
    life: 4844.72998047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13656.8974609
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3170
vespene: 294
food_cap: 192
food_used: 138
food_army: 81
food_workers: 55
idle_worker_count: 0
army_count: 54
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21671
score_details {
  idle_production_time: 84486.125
  idle_worker_time: 2876.625
  total_value_units: 31450.0
  total_value_structures: 4000.0
  killed_value_units: 8775.0
  killed_value_structures: 2650.0
  collected_minerals: 36190.0
  collected_vespene: 9956.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 582.0
  spent_minerals: 31875.0
  spent_vespene: 9075.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5600.0
    economy: 2850.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2175.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 300.0
    army: 15075.0
    economy: 3175.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 250.0
    army: 4850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 1950.0
    economy: 7525.0
    technology: 2200.0
    upgrade: 1575.0
  }
  used_vespene {
    none: -100.0
    army: 1050.0
    economy: 0.0
    technology: 800.0
    upgrade: 1575.0
  }
  total_used_minerals {
    none: 300.0
    army: 19900.0
    economy: 11250.0
    technology: 2800.0
    upgrade: 1575.0
  }
  total_used_vespene {
    none: 300.0
    army: 7550.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1575.0
  }
  total_damage_dealt {
    life: 15744.2314453
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 28929.8769531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 4365
vespene: 881
food_cap: 184
food_used: 72
food_army: 28
food_workers: 44
idle_worker_count: 10
army_count: 29
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 503
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 188
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 92
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18156
score_details {
  idle_production_time: 88162.4375
  idle_worker_time: 3062.25
  total_value_units: 34200.0
  total_value_structures: 4900.0
  killed_value_units: 9700.0
  killed_value_structures: 2750.0
  collected_minerals: 37905.0
  collected_vespene: 10676.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 604.0
  spent_minerals: 34125.0
  spent_vespene: 9975.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6400.0
    economy: 2850.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2300.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 300.0
    army: 18425.0
    economy: 3525.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 250.0
    army: 6650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -150.0
    army: 500.0
    economy: 7225.0
    technology: 2200.0
    upgrade: 1575.0
  }
  used_vespene {
    none: -100.0
    army: 0.0
    economy: 0.0
    technology: 800.0
    upgrade: 1575.0
  }
  total_used_minerals {
    none: 300.0
    army: 21700.0
    economy: 12350.0
    technology: 2800.0
    upgrade: 1575.0
  }
  total_used_vespene {
    none: 300.0
    army: 8450.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1575.0
  }
  total_damage_dealt {
    life: 17336.4414062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 35096.078125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3830
vespene: 701
food_cap: 196
food_used: 57
food_army: 12
food_workers: 45
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  31631
ui_data{
 
Score:  18156
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
