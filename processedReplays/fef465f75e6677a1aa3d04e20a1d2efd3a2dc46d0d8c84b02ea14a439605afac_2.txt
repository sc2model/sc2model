----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3570
  player_apm: 201
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3561
  player_apm: 146
}
game_duration_loops: 9851
game_duration_seconds: 439.8074646
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8798
score_details {
  idle_production_time: 2359.0
  idle_worker_time: 418.5625
  total_value_units: 5075.0
  total_value_structures: 3250.0
  killed_value_units: 3800.0
  killed_value_structures: 125.0
  collected_minerals: 6935.0
  collected_vespene: 2016.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 313.0
  spent_minerals: 6225.0
  spent_vespene: 1700.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2250.0
    economy: 1000.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1800.0
    economy: 3150.0
    technology: 1325.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 3350.0
    technology: 1325.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 5589.44726562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3002.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1146.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 707
vespene: 316
food_cap: 78
food_used: 64
food_army: 35
food_workers: 29
idle_worker_count: 4
army_count: 19
warp_gate_count: 0

game_loop:  9851
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 55
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 2
}
single {
  unit {
    unit_type: 55
    player_relative: 1
    health: 140
    shields: 0
    energy: 94
  }
}

Score:  8798
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
