----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5037
  player_apm: 319
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4763
  player_apm: 200
}
game_duration_loops: 17162
game_duration_seconds: 766.214172363
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11839
score_details {
  idle_production_time: 8955.3125
  idle_worker_time: 49.3125
  total_value_units: 7850.0
  total_value_structures: 1700.0
  killed_value_units: 275.0
  killed_value_structures: 0.0
  collected_minerals: 10735.0
  collected_vespene: 1160.0
  collection_rate_minerals: 2351.0
  collection_rate_vespene: 537.0
  spent_minerals: 9881.0
  spent_vespene: 1050.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 875.0
    economy: 281.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 6075.0
    technology: 950.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 250.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 6350.0
    technology: 1100.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 626.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2562.27880859
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 904
vespene: 110
food_cap: 106
food_used: 92
food_army: 34
food_workers: 58
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 105
  count: 27
}
groups {
  control_group_index: 1
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 6
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 4
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12196
score_details {
  idle_production_time: 28394.4375
  idle_worker_time: 607.0625
  total_value_units: 23300.0
  total_value_structures: 2700.0
  killed_value_units: 7250.0
  killed_value_structures: 0.0
  collected_minerals: 23065.0
  collected_vespene: 6312.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 783.0
  spent_minerals: 23006.0
  spent_vespene: 6200.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11325.0
    economy: 1506.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1050.0
    economy: 6675.0
    technology: 1550.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 700.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 11800.0
    economy: 9700.0
    technology: 1900.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 950.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 8642.55175781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22180.8828125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 109
vespene: 112
food_cap: 194
food_used: 76
food_army: 22
food_workers: 54
idle_worker_count: 1
army_count: 2
warp_gate_count: 0

game_loop:  17162
ui_data{
 
Score:  12196
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
