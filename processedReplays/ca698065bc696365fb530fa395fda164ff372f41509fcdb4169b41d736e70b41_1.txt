----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4016
  player_apm: 212
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3935
  player_apm: 118
}
game_duration_loops: 6381
game_duration_seconds: 284.885955811
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5051
score_details {
  idle_production_time: 5588.4375
  idle_worker_time: 3.4375
  total_value_units: 3500.0
  total_value_structures: 1050.0
  killed_value_units: 1025.0
  killed_value_structures: 0.0
  collected_minerals: 4200.0
  collected_vespene: 488.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 156.0
  spent_minerals: 4037.0
  spent_vespene: 425.0
  food_used {
    none: 2.5
    army: 19.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 975.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 625.0
    economy: -38.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1300.0
    economy: 2275.0
    technology: 525.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 50.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1650.0
    economy: 2625.0
    technology: 525.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1831.06591797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1399.09716797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 213
vespene: 63
food_cap: 52
food_used: 41
food_army: 19
food_workers: 20
idle_worker_count: 0
army_count: 35
warp_gate_count: 0

game_loop:  6381
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 35
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 8
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

Score:  5051
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
