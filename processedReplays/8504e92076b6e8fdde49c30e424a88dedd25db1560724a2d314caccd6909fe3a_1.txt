----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3547
  player_apm: 107
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 132
}
game_duration_loops: 17447
game_duration_seconds: 778.938293457
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6434
score_details {
  idle_production_time: 1302.1875
  idle_worker_time: 232.75
  total_value_units: 4875.0
  total_value_structures: 2600.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 6005.0
  collected_vespene: 1204.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 313.0
  spent_minerals: 5950.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 425.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1275.0
    economy: 2900.0
    technology: 1050.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 3900.0
    technology: 1050.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1246.82128906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1114.5
    shields: 2345.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 105
vespene: 329
food_cap: 70
food_used: 57
food_army: 28
food_workers: 28
idle_worker_count: 1
army_count: 13
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 311
  count: 5
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

score{
 score_type: Melee
score: 10903
score_details {
  idle_production_time: 3107.125
  idle_worker_time: 826.4375
  total_value_units: 11550.0
  total_value_structures: 3650.0
  killed_value_units: 6575.0
  killed_value_structures: 250.0
  collected_minerals: 13705.0
  collected_vespene: 2948.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 470.0
  spent_minerals: 12250.0
  spent_vespene: 2050.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4125.0
    economy: 1750.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3675.0
    economy: 1700.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2325.0
    economy: 3950.0
    technology: 1350.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 6200.0
    economy: 5650.0
    technology: 1350.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 8715.64941406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4279.49804688
    shields: 7091.62695312
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1505
vespene: 898
food_cap: 94
food_used: 87
food_army: 46
food_workers: 41
idle_worker_count: 2
army_count: 23
warp_gate_count: 6

game_loop:  17447
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 311
  count: 3
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 60
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 40
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 66
    shields: 0
    energy: 0
  }
}

Score:  10903
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
