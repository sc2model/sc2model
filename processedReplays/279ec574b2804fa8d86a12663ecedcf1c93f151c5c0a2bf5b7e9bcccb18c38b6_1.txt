----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3814
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3672
  player_apm: 156
}
game_duration_loops: 13405
game_duration_seconds: 598.479248047
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7034
score_details {
  idle_production_time: 867.875
  idle_worker_time: 715.5
  total_value_units: 5475.0
  total_value_structures: 2600.0
  killed_value_units: 1625.0
  killed_value_structures: 0.0
  collected_minerals: 6120.0
  collected_vespene: 1764.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 335.0
  spent_minerals: 5825.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 850.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1375.0
    economy: 3300.0
    technology: 600.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2225.0
    economy: 3850.0
    technology: 600.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 2715.27246094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1332.0
    shields: 2373.17504883
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 345
vespene: 314
food_cap: 86
food_used: 59
food_army: 28
food_workers: 31
idle_worker_count: 3
army_count: 12
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 80
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 67
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 6822
score_details {
  idle_production_time: 1780.25
  idle_worker_time: 1214.1875
  total_value_units: 6850.0
  total_value_structures: 4700.0
  killed_value_units: 3550.0
  killed_value_structures: 125.0
  collected_minerals: 8940.0
  collected_vespene: 2832.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 358.0
  spent_minerals: 8750.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3000.0
    economy: 300.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2800.0
    economy: 1450.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 200.0
    economy: 3500.0
    technology: 1450.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 3000.0
    economy: 4950.0
    technology: 1750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 5857.94091797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4359.75
    shields: 5740.92480469
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 240
vespene: 932
food_cap: 118
food_used: 30
food_army: 4
food_workers: 26
idle_worker_count: 3
army_count: 2
warp_gate_count: 3

game_loop:  13405
ui_data{
 
Score:  6822
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
