----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3727
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3718
  player_apm: 146
}
game_duration_loops: 22713
game_duration_seconds: 1014.04394531
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8482
score_details {
  idle_production_time: 1338.0625
  idle_worker_time: 1076.375
  total_value_units: 4500.0
  total_value_structures: 3100.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 6630.0
  collected_vespene: 1552.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 246.0
  spent_minerals: 6500.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1450.0
    economy: 3825.0
    technology: 1375.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1850.0
    economy: 3850.0
    technology: 1275.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 275.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 575.226318359
    shields: 1360.77368164
    energy: 0.0
  }
  total_damage_taken {
    life: 862.232421875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 103.356445312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 180
vespene: 152
food_cap: 78
food_used: 62
food_army: 30
food_workers: 31
idle_worker_count: 1
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 3186
score_details {
  idle_production_time: 6187.8125
  idle_worker_time: 4419.3125
  total_value_units: 12225.0
  total_value_structures: 4675.0
  killed_value_units: 7300.0
  killed_value_structures: 1100.0
  collected_minerals: 13485.0
  collected_vespene: 3376.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 13375.0
  spent_vespene: 3225.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 2.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3925.0
    economy: 2800.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 6400.0
    economy: 4150.0
    technology: 1450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 2175.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 600.0
    economy: 950.0
    technology: 525.0
    upgrade: 200.0
  }
  used_vespene {
    none: -50.0
    army: 450.0
    economy: 0.0
    technology: 50.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 7000.0
    economy: 5400.0
    technology: 2075.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 2625.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 5488.45654297
    shields: 7895.89990234
    energy: 0.0
  }
  total_damage_taken {
    life: 26698.0039062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 548.250488281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 160
vespene: 151
food_cap: 47
food_used: 12
food_army: 10
food_workers: 2
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 134
  count: 1
}
single {
  unit {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 936
score_details {
  idle_production_time: 6212.9375
  idle_worker_time: 4508.5
  total_value_units: 12225.0
  total_value_structures: 4675.0
  killed_value_units: 7500.0
  killed_value_structures: 1200.0
  collected_minerals: 13485.0
  collected_vespene: 3376.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 13375.0
  spent_vespene: 3225.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3925.0
    economy: 3100.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 6850.0
    economy: 5100.0
    technology: 1925.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 2550.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 150.0
    economy: 0.0
    technology: 50.0
    upgrade: 200.0
  }
  used_vespene {
    none: -50.0
    army: 75.0
    economy: 0.0
    technology: 50.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 7000.0
    economy: 5400.0
    technology: 2075.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 2625.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 5768.45654297
    shields: 8175.89990234
    energy: 0.0
  }
  total_damage_taken {
    life: 30534.2539062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 548.250488281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 160
vespene: 151
food_cap: 0
food_used: 2
food_army: 2
food_workers: 0
idle_worker_count: 0
army_count: 1
warp_gate_count: 0

game_loop:  22713
ui_data{
 single {
  unit {
    unit_type: 74
    player_relative: 3
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  936
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
