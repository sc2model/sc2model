----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4328
  player_apm: 90
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 0
}
game_duration_loops: 5914
game_duration_seconds: 264.0362854
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5770
score_details {
  idle_production_time: 256.9375
  idle_worker_time: 67.0
  total_value_units: 2575.0
  total_value_structures: 1800.0
  killed_value_units: 600.0
  killed_value_structures: 400.0
  collected_minerals: 3890.0
  collected_vespene: 880.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 335.0
  spent_minerals: 3625.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 975.0
    economy: 2950.0
    technology: 600.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 875.0
    economy: 2500.0
    technology: 600.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1240.0
    shields: 1328.125
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 315
vespene: 230
food_cap: 55
food_used: 50
food_army: 20
food_workers: 29
idle_worker_count: 1
army_count: 8
warp_gate_count: 0

game_loop:  5914
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 62
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}

Score:  5770
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
