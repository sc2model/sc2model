----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3562
  player_apm: 47
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3574
  player_apm: 37
}
game_duration_loops: 8720
game_duration_seconds: 389.312866211
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8547
score_details {
  idle_production_time: 1407.0625
  idle_worker_time: 388.9375
  total_value_units: 3950.0
  total_value_structures: 3325.0
  killed_value_units: 2125.0
  killed_value_structures: 1125.0
  collected_minerals: 5940.0
  collected_vespene: 1632.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 425.0
  spent_minerals: 5400.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1075.0
    economy: 1125.0
    technology: 950.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: -75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1250.0
    economy: 3675.0
    technology: 1350.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1250.0
    economy: 3575.0
    technology: 1350.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 7454.86425781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 141.446044922
    shields: 535.553955078
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 590
vespene: 482
food_cap: 86
food_used: 61
food_army: 22
food_workers: 37
idle_worker_count: 1
army_count: 11
warp_gate_count: 4

game_loop:  8720
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 133
  count: 3
}
multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 35
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 76
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
}

Score:  8547
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
