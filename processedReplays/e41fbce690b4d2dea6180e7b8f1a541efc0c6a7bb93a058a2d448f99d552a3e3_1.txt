----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3405
  player_apm: 141
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3344
  player_apm: 123
}
game_duration_loops: 16791
game_duration_seconds: 749.650512695
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9876
score_details {
  idle_production_time: 1306.125
  idle_worker_time: 370.9375
  total_value_units: 4850.0
  total_value_structures: 3325.0
  killed_value_units: 575.0
  killed_value_structures: 0.0
  collected_minerals: 8215.0
  collected_vespene: 1488.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 447.0
  spent_minerals: 8125.0
  spent_vespene: 1325.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2400.0
    economy: 4550.0
    technology: 1075.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 675.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 2500.0
    economy: 4250.0
    technology: 1075.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 910.468261719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1458.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 287.338867188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 113
vespene: 163
food_cap: 86
food_used: 85
food_army: 48
food_workers: 35
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 689
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 15633
score_details {
  idle_production_time: 4168.5
  idle_worker_time: 1098.6875
  total_value_units: 11100.0
  total_value_structures: 4325.0
  killed_value_units: 5300.0
  killed_value_structures: 0.0
  collected_minerals: 16370.0
  collected_vespene: 4740.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 671.0
  spent_minerals: 13425.0
  spent_vespene: 3550.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3700.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4350.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 2400.0
    economy: 5050.0
    technology: 1075.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 1500.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 6450.0
    economy: 6050.0
    technology: 1075.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 50.0
    army: 2300.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 5892.19238281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9735.72265625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2756.30151367
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2968
vespene: 1190
food_cap: 134
food_used: 87
food_army: 48
food_workers: 39
idle_worker_count: 2
army_count: 23
warp_gate_count: 0

game_loop:  16791
ui_data{
 multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 42
    shields: 0
    energy: 85
    transport_slots_taken: 1
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 154
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 111
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 78
    shields: 0
    energy: 107
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 74
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 24
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

Score:  15633
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
