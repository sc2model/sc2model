----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 138
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3323
  player_apm: 162
}
game_duration_loops: 10379
game_duration_seconds: 463.380554199
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9717
score_details {
  idle_production_time: 11854.5625
  idle_worker_time: 6.25
  total_value_units: 6400.0
  total_value_structures: 1800.0
  killed_value_units: 2450.0
  killed_value_structures: 0.0
  collected_minerals: 7815.0
  collected_vespene: 1608.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 604.0
  spent_minerals: 6656.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1425.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: -69.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 3700.0
    technology: 1300.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 3950.0
    technology: 1450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 5041.82080078
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1727.82666016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1209
vespene: 308
food_cap: 76
food_used: 66
food_army: 35
food_workers: 31
idle_worker_count: 0
army_count: 32
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 24
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10113
score_details {
  idle_production_time: 12726.75
  idle_worker_time: 6.25
  total_value_units: 6400.0
  total_value_structures: 1800.0
  killed_value_units: 3050.0
  killed_value_structures: 0.0
  collected_minerals: 8130.0
  collected_vespene: 1764.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 537.0
  spent_minerals: 7156.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 33.5
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1650.0
    economy: 1150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 875.0
    economy: -69.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1725.0
    economy: 4200.0
    technology: 1300.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2600.0
    economy: 3950.0
    technology: 1450.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 5696.08789062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2065.27832031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1024
vespene: 464
food_cap: 76
food_used: 74
food_army: 33
food_workers: 31
idle_worker_count: 0
army_count: 29
warp_gate_count: 0

game_loop:  10379
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 7
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
}

Score:  10113
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
