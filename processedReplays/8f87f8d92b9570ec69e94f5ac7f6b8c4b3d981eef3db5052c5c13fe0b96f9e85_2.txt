----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3933
  player_apm: 375
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3906
  player_apm: 120
}
game_duration_loops: 9992
game_duration_seconds: 446.10256958
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7197
score_details {
  idle_production_time: 4609.875
  idle_worker_time: 33.875
  total_value_units: 6250.0
  total_value_structures: 2050.0
  killed_value_units: 2125.0
  killed_value_structures: 200.0
  collected_minerals: 7005.0
  collected_vespene: 1492.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 313.0
  spent_minerals: 6675.0
  spent_vespene: 1375.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1150.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1425.0
    economy: 500.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 3150.0
    technology: 900.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 4000.0
    technology: 1150.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 600.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1921.0
    shields: 2518.125
    energy: 0.0
  }
  total_damage_taken {
    life: 5869.546875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 380
vespene: 117
food_cap: 92
food_used: 59
food_army: 33
food_workers: 26
idle_worker_count: 0
army_count: 13
warp_gate_count: 0

game_loop:  9992
ui_data{
 
Score:  7197
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
