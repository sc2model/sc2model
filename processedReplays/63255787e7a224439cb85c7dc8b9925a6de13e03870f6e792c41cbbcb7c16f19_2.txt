----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 55
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2645
  player_apm: 102
}
game_duration_loops: 20315
game_duration_seconds: 906.982910156
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11499
score_details {
  idle_production_time: 6740.0625
  idle_worker_time: 29.1875
  total_value_units: 6100.0
  total_value_structures: 1700.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 7675.0
  collected_vespene: 2724.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 649.0
  spent_minerals: 7300.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 38.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1800.0
    economy: 5150.0
    technology: 1000.0
    upgrade: 250.0
  }
  used_vespene {
    none: 150.0
    army: 550.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 5100.0
    technology: 1150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 425
vespene: 1574
food_cap: 98
food_used: 81
food_army: 38
food_workers: 43
idle_worker_count: 0
army_count: 30
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 91
    player_relative: 1
    health: 850
    shields: 0
    energy: 0
    add_on {
      unit_type: 504
      build_progress: 0.00468748807907
    }
  }
}

score{
 score_type: Melee
score: 18068
score_details {
  idle_production_time: 25754.3125
  idle_worker_time: 2433.375
  total_value_units: 14750.0
  total_value_structures: 2750.0
  killed_value_units: 7375.0
  killed_value_structures: 1200.0
  collected_minerals: 18045.0
  collected_vespene: 7860.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 649.0
  spent_minerals: 14287.0
  spent_vespene: 6350.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4700.0
    economy: 2450.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4662.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1900.0
    economy: 5700.0
    technology: 1250.0
    upgrade: 725.0
  }
  used_vespene {
    none: 150.0
    army: 1750.0
    economy: 0.0
    technology: 400.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 300.0
    army: 7200.0
    economy: 7450.0
    technology: 1400.0
    upgrade: 725.0
  }
  total_used_vespene {
    none: 300.0
    army: 6500.0
    economy: 0.0
    technology: 500.0
    upgrade: 725.0
  }
  total_damage_dealt {
    life: 14506.9355469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8811.41601562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3808
vespene: 1510
food_cap: 152
food_used: 84
food_army: 39
food_workers: 45
idle_worker_count: 14
army_count: 15
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18475
score_details {
  idle_production_time: 26046.75
  idle_worker_time: 2709.0
  total_value_units: 14900.0
  total_value_structures: 2750.0
  killed_value_units: 7525.0
  killed_value_structures: 1900.0
  collected_minerals: 18300.0
  collected_vespene: 8012.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 649.0
  spent_minerals: 15337.0
  spent_vespene: 7250.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4700.0
    economy: 3300.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4662.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2800.0
    economy: 5850.0
    technology: 1250.0
    upgrade: 725.0
  }
  used_vespene {
    none: 150.0
    army: 2650.0
    economy: 0.0
    technology: 400.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 300.0
    army: 7200.0
    economy: 7600.0
    technology: 1400.0
    upgrade: 725.0
  }
  total_used_vespene {
    none: 300.0
    army: 6500.0
    economy: 0.0
    technology: 500.0
    upgrade: 725.0
  }
  total_damage_dealt {
    life: 17357.4355469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8811.41601562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3013
vespene: 762
food_cap: 152
food_used: 105
food_army: 57
food_workers: 48
idle_worker_count: 14
army_count: 15
warp_gate_count: 0

game_loop:  20315
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 503
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  18475
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
