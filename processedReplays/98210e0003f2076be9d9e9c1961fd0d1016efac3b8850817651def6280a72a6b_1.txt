----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3815
  player_apm: 142
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 186
}
game_duration_loops: 10698
game_duration_seconds: 477.622619629
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9618
score_details {
  idle_production_time: 3559.3125
  idle_worker_time: 218.5625
  total_value_units: 7950.0
  total_value_structures: 1650.0
  killed_value_units: 1000.0
  killed_value_structures: 0.0
  collected_minerals: 9675.0
  collected_vespene: 1480.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 671.0
  spent_minerals: 9562.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1525.0
    economy: 287.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 5900.0
    technology: 1000.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 6250.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2962.19873047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5017.27441406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 163
vespene: 505
food_cap: 100
food_used: 83
food_army: 28
food_workers: 55
idle_worker_count: 17
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 110
  count: 25
}
groups {
  control_group_index: 5
  leader_unit_type: 126
  count: 17
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 130
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 132
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 7668
score_details {
  idle_production_time: 3899.9375
  idle_worker_time: 297.0625
  total_value_units: 8750.0
  total_value_structures: 1650.0
  killed_value_units: 1700.0
  killed_value_structures: 0.0
  collected_minerals: 10390.0
  collected_vespene: 1740.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 492.0
  spent_minerals: 9862.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2350.0
    economy: 612.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 900.0
    economy: 3850.0
    technology: 1000.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3100.0
    economy: 6250.0
    technology: 1150.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3779.19873047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7998.12890625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 578
vespene: 590
food_cap: 100
food_used: 55
food_army: 20
food_workers: 35
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  10698
ui_data{
 single {
  unit {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

Score:  7668
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
