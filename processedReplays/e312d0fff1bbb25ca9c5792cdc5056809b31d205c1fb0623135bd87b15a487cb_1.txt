----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3358
  player_apm: 151
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3364
  player_apm: 107
}
game_duration_loops: 15541
game_duration_seconds: 693.843078613
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14319
score_details {
  idle_production_time: 6304.875
  idle_worker_time: 18.25
  total_value_units: 7900.0
  total_value_structures: 2475.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 11235.0
  collected_vespene: 2284.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 963.0
  spent_minerals: 9975.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 31.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1525.0
    economy: 6775.0
    technology: 1975.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 25.0
    technology: 300.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 1625.0
    economy: 7600.0
    technology: 2125.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 225.0
    economy: 50.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 205.0
    shields: 200.0
    energy: 0.0
  }
  total_damage_taken {
    life: 539.912109375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1310
vespene: 1184
food_cap: 146
food_used: 97
food_army: 31
food_workers: 66
idle_worker_count: 0
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 25748
score_details {
  idle_production_time: 14794.875
  idle_worker_time: 22.9375
  total_value_units: 18750.0
  total_value_structures: 3125.0
  killed_value_units: 6375.0
  killed_value_structures: 600.0
  collected_minerals: 21080.0
  collected_vespene: 6968.0
  collection_rate_minerals: 2323.0
  collection_rate_vespene: 1276.0
  spent_minerals: 17625.0
  spent_vespene: 5900.0
  food_used {
    none: 0.0
    army: 122.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2750.0
    economy: 1700.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1700.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5975.0
    economy: 8225.0
    technology: 1975.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 3475.0
    economy: 25.0
    technology: 300.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 7150.0
    economy: 9350.0
    technology: 2125.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 4850.0
    economy: 50.0
    technology: 400.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 3313.0
    shields: 6608.75
    energy: 0.0
  }
  total_damage_taken {
    life: 4504.22900391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3505
vespene: 1068
food_cap: 200
food_used: 188
food_army: 122
food_workers: 66
idle_worker_count: 0
army_count: 75
warp_gate_count: 0

game_loop:  15541
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 108
  count: 29
}
single {
  unit {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  25748
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
