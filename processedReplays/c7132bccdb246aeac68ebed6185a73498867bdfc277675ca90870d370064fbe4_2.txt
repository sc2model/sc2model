----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3025
  player_apm: 53
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3038
  player_apm: 151
}
game_duration_loops: 4067
game_duration_seconds: 181.575164795
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2545
score_details {
  idle_production_time: 205.3125
  idle_worker_time: 112.75
  total_value_units: 1050.0
  total_value_structures: 975.0
  killed_value_units: 0.0
  killed_value_structures: 250.0
  collected_minerals: 2045.0
  collected_vespene: 0.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 0.0
  spent_minerals: 1225.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 12.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 1275.0
    technology: 300.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 100.0
    economy: 1625.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 378.327148438
    shields: 625.948242188
    energy: 0.0
  }
  total_damage_taken {
    life: 440.0
    shields: 864.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 870
vespene: 0
food_cap: 23
food_used: 14
food_army: 2
food_workers: 12
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  4067
ui_data{
 
Score:  2545
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
