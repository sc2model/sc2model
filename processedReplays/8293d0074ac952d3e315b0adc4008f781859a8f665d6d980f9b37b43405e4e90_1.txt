----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 36
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2247
  player_apm: 40
}
game_duration_loops: 37735
game_duration_seconds: 1684.71582031
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7591
score_details {
  idle_production_time: 997.625
  idle_worker_time: 647.1875
  total_value_units: 3500.0
  total_value_structures: 3250.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 8135.0
  collected_vespene: 1056.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 291.0
  spent_minerals: 6900.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 150.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 3725.0
    technology: 1025.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 3775.0
    technology: 1175.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 581.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2850.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1285
vespene: 156
food_cap: 77
food_used: 51
food_army: 13
food_workers: 36
idle_worker_count: 3
army_count: 6
warp_gate_count: 0

game_loop:  10000
ui_data{
 multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21487
score_details {
  idle_production_time: 6976.9375
  idle_worker_time: 1578.125
  total_value_units: 10775.0
  total_value_structures: 6950.0
  killed_value_units: 3075.0
  killed_value_structures: 0.0
  collected_minerals: 19450.0
  collected_vespene: 5712.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 963.0
  spent_minerals: 16000.0
  spent_vespene: 3975.0
  food_used {
    none: 0.0
    army: 85.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2100.0
    economy: 200.0
    technology: 425.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4350.0
    economy: 6175.0
    technology: 2075.0
    upgrade: 725.0
  }
  used_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 875.0
    upgrade: 725.0
  }
  total_used_minerals {
    none: 0.0
    army: 6250.0
    economy: 6375.0
    technology: 2450.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 925.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 2676.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4931.54638672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 189.356445312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3500
vespene: 1737
food_cap: 147
food_used: 137
food_army: 85
food_workers: 52
idle_worker_count: 3
army_count: 50
warp_gate_count: 0

game_loop:  20000
ui_data{
 single {
  unit {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 41
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 23644
score_details {
  idle_production_time: 17173.5625
  idle_worker_time: 3522.4375
  total_value_units: 23700.0
  total_value_structures: 8375.0
  killed_value_units: 12825.0
  killed_value_structures: 550.0
  collected_minerals: 30090.0
  collected_vespene: 12504.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 828.0
  spent_minerals: 29300.0
  spent_vespene: 11575.0
  food_used {
    none: 0.0
    army: 75.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9000.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11850.0
    economy: 900.0
    technology: 625.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3950.0
    economy: 150.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4000.0
    economy: 6750.0
    technology: 2475.0
    upgrade: 2350.0
  }
  used_vespene {
    none: 0.0
    army: 2650.0
    economy: 150.0
    technology: 1150.0
    upgrade: 2350.0
  }
  total_used_minerals {
    none: 0.0
    army: 14675.0
    economy: 7650.0
    technology: 3000.0
    upgrade: 2100.0
  }
  total_used_vespene {
    none: 0.0
    army: 5775.0
    economy: 300.0
    technology: 1275.0
    upgrade: 2100.0
  }
  total_damage_dealt {
    life: 12683.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20064.5996094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 663.7734375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 840
vespene: 929
food_cap: 155
food_used: 129
food_army: 75
food_workers: 54
idle_worker_count: 1
army_count: 16
warp_gate_count: 0

game_loop:  30000
ui_data{
 single {
  unit {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 33119
score_details {
  idle_production_time: 27827.3125
  idle_worker_time: 4988.5625
  total_value_units: 33550.0
  total_value_structures: 9350.0
  killed_value_units: 20250.0
  killed_value_structures: 5525.0
  collected_minerals: 41950.0
  collected_vespene: 16144.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 291.0
  spent_minerals: 36350.0
  spent_vespene: 15750.0
  food_used {
    none: 0.0
    army: 104.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13150.0
    economy: 4650.0
    technology: 1925.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5400.0
    economy: 150.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14700.0
    economy: 900.0
    technology: 1075.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6050.0
    economy: 150.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5950.0
    economy: 8200.0
    technology: 2075.0
    upgrade: 2850.0
  }
  used_vespene {
    none: 0.0
    army: 4050.0
    economy: 150.0
    technology: 950.0
    upgrade: 2850.0
  }
  total_used_minerals {
    none: 0.0
    army: 20250.0
    economy: 9400.0
    technology: 3050.0
    upgrade: 2700.0
  }
  total_used_vespene {
    none: 0.0
    army: 9800.0
    economy: 600.0
    technology: 1300.0
    upgrade: 2700.0
  }
  total_damage_dealt {
    life: 37197.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 29102.4511719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 951.187988281
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 5650
vespene: 394
food_cap: 193
food_used: 163
food_army: 104
food_workers: 59
idle_worker_count: 6
army_count: 24
warp_gate_count: 0

game_loop:  37735
ui_data{
 multi {
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 250
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 404
    shields: 0
    energy: 0
  }
  units {
    unit_type: 691
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 393
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 148
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 488
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 312
    shields: 0
    energy: 0
  }
  units {
    unit_type: 55
    player_relative: 1
    health: 140
    shields: 0
    energy: 200
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
}

Score:  33119
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
