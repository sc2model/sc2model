----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 93
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2917
  player_apm: 102
}
game_duration_loops: 16812
game_duration_seconds: 750.58807373
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10247
score_details {
  idle_production_time: 5770.75
  idle_worker_time: 0.9375
  total_value_units: 4800.0
  total_value_structures: 1400.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 7105.0
  collected_vespene: 2192.0
  collection_rate_minerals: 1483.0
  collection_rate_vespene: 627.0
  spent_minerals: 6900.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 3650.0
    technology: 1100.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4350.0
    technology: 1100.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 350.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 232.0
    shields: 140.0
    energy: 0.0
  }
  total_damage_taken {
    life: 306.879882812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 255
vespene: 292
food_cap: 76
food_used: 72
food_army: 35
food_workers: 37
idle_worker_count: 0
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 90
  count: 2
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 160
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 160
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13009
score_details {
  idle_production_time: 25702.0625
  idle_worker_time: 121.1875
  total_value_units: 11500.0
  total_value_structures: 2300.0
  killed_value_units: 5750.0
  killed_value_structures: 0.0
  collected_minerals: 15025.0
  collected_vespene: 5584.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 649.0
  spent_minerals: 13425.0
  spent_vespene: 4825.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4750.0
    economy: 1425.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: -200.0
    economy: 4250.0
    technology: 1500.0
    upgrade: 2025.0
  }
  used_vespene {
    none: 150.0
    army: 0.0
    economy: 0.0
    technology: 700.0
    upgrade: 2025.0
  }
  total_used_minerals {
    none: 300.0
    army: 5650.0
    economy: 6500.0
    technology: 1850.0
    upgrade: 1800.0
  }
  total_used_vespene {
    none: 300.0
    army: 2200.0
    economy: 0.0
    technology: 950.0
    upgrade: 1800.0
  }
  total_damage_dealt {
    life: 3310.0
    shields: 2898.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8999.87207031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1650
vespene: 759
food_cap: 84
food_used: 41
food_army: 4
food_workers: 37
idle_worker_count: 37
army_count: 0
warp_gate_count: 0

game_loop:  16812
ui_data{
 
Score:  13009
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
