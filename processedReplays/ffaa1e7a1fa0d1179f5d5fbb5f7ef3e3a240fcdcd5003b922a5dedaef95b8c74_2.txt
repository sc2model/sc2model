----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 158
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3756
  player_apm: 139
}
game_duration_loops: 21868
game_duration_seconds: 976.318115234
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14520
score_details {
  idle_production_time: 8104.6875
  idle_worker_time: 33.875
  total_value_units: 8500.0
  total_value_structures: 1950.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 11045.0
  collected_vespene: 2400.0
  collection_rate_minerals: 2519.0
  collection_rate_vespene: 1007.0
  spent_minerals: 10575.0
  spent_vespene: 2225.0
  food_used {
    none: 0.0
    army: 45.5
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2825.0
    economy: 6700.0
    technology: 1200.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 450.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 7000.0
    technology: 1200.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 450.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 141.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 338.307617188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 520
vespene: 175
food_cap: 130
food_used: 112
food_army: 45
food_workers: 67
idle_worker_count: 0
army_count: 44
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 9
}
groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 60
  }
}

score{
 score_type: Melee
score: 22727
score_details {
  idle_production_time: 32928.625
  idle_worker_time: 605.875
  total_value_units: 22450.0
  total_value_structures: 3075.0
  killed_value_units: 15025.0
  killed_value_structures: 400.0
  collected_minerals: 27520.0
  collected_vespene: 9432.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 671.0
  spent_minerals: 25350.0
  spent_vespene: 7725.0
  food_used {
    none: 0.0
    army: 52.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12300.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8450.0
    economy: 2075.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3600.0
    economy: 6825.0
    technology: 1875.0
    upgrade: 1825.0
  }
  used_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 750.0
    upgrade: 1825.0
  }
  total_used_minerals {
    none: 0.0
    army: 15300.0
    economy: 9450.0
    technology: 2325.0
    upgrade: 1825.0
  }
  total_used_vespene {
    none: 0.0
    army: 5750.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1825.0
  }
  total_damage_dealt {
    life: 15142.2871094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 20234.7226562
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2220
vespene: 1707
food_cap: 152
food_used: 106
food_army: 52
food_workers: 54
idle_worker_count: 3
army_count: 23
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 114
    player_relative: 1
    health: 184
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 86
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 220
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 193
    shields: 0
    energy: 0
  }
  units {
    unit_type: 114
    player_relative: 1
    health: 225
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19909
score_details {
  idle_production_time: 36206.5
  idle_worker_time: 1766.9375
  total_value_units: 26650.0
  total_value_structures: 3375.0
  killed_value_units: 16750.0
  killed_value_structures: 675.0
  collected_minerals: 29345.0
  collected_vespene: 10364.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 671.0
  spent_minerals: 27775.0
  spent_vespene: 9025.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13100.0
    economy: 1050.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3000.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11400.0
    economy: 2206.0
    technology: -200.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4150.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2450.0
    economy: 6925.0
    technology: 1875.0
    upgrade: 1925.0
  }
  used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 750.0
    upgrade: 1925.0
  }
  total_used_minerals {
    none: 0.0
    army: 17700.0
    economy: 10100.0
    technology: 2325.0
    upgrade: 1825.0
  }
  total_used_vespene {
    none: 0.0
    army: 7250.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1825.0
  }
  total_damage_dealt {
    life: 17956.3828125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25309.625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1620
vespene: 1339
food_cap: 158
food_used: 91
food_army: 35
food_workers: 56
idle_worker_count: 2
army_count: 17
warp_gate_count: 0

game_loop:  21868
ui_data{
 single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 36
    shields: 0
    energy: 80
  }
}

Score:  19909
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
