----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3379
  player_apm: 85
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3364
  player_apm: 134
}
game_duration_loops: 20340
game_duration_seconds: 908.099060059
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9510
score_details {
  idle_production_time: 7591.0
  idle_worker_time: 378.875
  total_value_units: 7400.0
  total_value_structures: 1750.0
  killed_value_units: 1700.0
  killed_value_structures: 200.0
  collected_minerals: 10810.0
  collected_vespene: 712.0
  collection_rate_minerals: 2323.0
  collection_rate_vespene: 0.0
  spent_minerals: 8387.0
  spent_vespene: 425.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1200.0
    economy: 450.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1625.0
    economy: -38.0
    technology: 400.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 525.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 750.0
    economy: 5175.0
    technology: 625.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2850.0
    economy: 5825.0
    technology: 1025.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 3593.90185547
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5844.58056641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2473
vespene: 287
food_cap: 98
food_used: 69
food_army: 11
food_workers: 58
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 156
    shields: 0
    energy: 166
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 104
  }
}

score{
 score_type: Melee
score: 16870
score_details {
  idle_production_time: 25584.4375
  idle_worker_time: 887.0
  total_value_units: 20850.0
  total_value_structures: 5075.0
  killed_value_units: 5275.0
  killed_value_structures: 200.0
  collected_minerals: 26110.0
  collected_vespene: 6272.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 671.0
  spent_minerals: 23737.0
  spent_vespene: 6100.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4250.0
    economy: 450.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8875.0
    economy: 1362.0
    technology: 1700.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1075.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1150.0
    economy: 6750.0
    technology: 2525.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 150.0
    army: 750.0
    economy: 0.0
    technology: 650.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 300.0
    army: 11850.0
    economy: 9900.0
    technology: 4575.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 300.0
    army: 4675.0
    economy: 0.0
    technology: 1000.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 8157.18310547
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24799.7890625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2423
vespene: 172
food_cap: 182
food_used: 74
food_army: 25
food_workers: 43
idle_worker_count: 14
army_count: 6
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 502
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15919
score_details {
  idle_production_time: 26110.75
  idle_worker_time: 1145.125
  total_value_units: 21600.0
  total_value_structures: 5075.0
  killed_value_units: 5625.0
  killed_value_structures: 200.0
  collected_minerals: 26345.0
  collected_vespene: 6436.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 649.0
  spent_minerals: 23512.0
  spent_vespene: 5875.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4550.0
    economy: 450.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9475.0
    economy: 1412.0
    technology: 1950.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3300.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1075.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 550.0
    economy: 6700.0
    technology: 2275.0
    upgrade: 850.0
  }
  used_vespene {
    none: 150.0
    army: 300.0
    economy: 0.0
    technology: 650.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 300.0
    army: 12150.0
    economy: 10200.0
    technology: 4575.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 300.0
    army: 4825.0
    economy: 0.0
    technology: 1000.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 8736.70605469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27632.5566406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2883
vespene: 561
food_cap: 182
food_used: 61
food_army: 13
food_workers: 48
idle_worker_count: 14
army_count: 4
warp_gate_count: 0

game_loop:  20340
ui_data{
 multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

Score:  15919
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
