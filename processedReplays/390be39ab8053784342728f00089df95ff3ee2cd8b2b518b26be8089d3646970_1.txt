----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4187
  player_apm: 226
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4349
  player_apm: 116
}
game_duration_loops: 9380
game_duration_seconds: 418.77923584
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8359
score_details {
  idle_production_time: 1140.3125
  idle_worker_time: 430.125
  total_value_units: 4400.0
  total_value_structures: 2975.0
  killed_value_units: 2975.0
  killed_value_structures: 225.0
  collected_minerals: 7080.0
  collected_vespene: 1432.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 291.0
  spent_minerals: 5925.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1600.0
    economy: 950.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1400.0
    economy: 3100.0
    technology: 1175.0
    upgrade: 0.0
  }
  used_vespene {
    none: 50.0
    army: 575.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 1850.0
    economy: 4000.0
    technology: 1175.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 7263.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2341.67041016
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 445.107421875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1202
vespene: 457
food_cap: 70
food_used: 55
food_army: 26
food_workers: 29
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  9380
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 47
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 32
  count: 3
}
single {
  unit {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

Score:  8359
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
