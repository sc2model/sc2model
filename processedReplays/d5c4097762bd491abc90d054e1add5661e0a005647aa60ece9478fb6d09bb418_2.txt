----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4431
  player_apm: 380
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4553
  player_apm: 187
}
game_duration_loops: 14752
game_duration_seconds: 658.617370605
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8951
score_details {
  idle_production_time: 10160.25
  idle_worker_time: 6.4375
  total_value_units: 8000.0
  total_value_structures: 2600.0
  killed_value_units: 1475.0
  killed_value_structures: 0.0
  collected_minerals: 9825.0
  collected_vespene: 2180.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 559.0
  spent_minerals: 9254.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1275.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 1043.0
    technology: 1136.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 225.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1550.0
    economy: 3800.0
    technology: 800.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 6050.0
    technology: 2150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4333.24023438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7037.83789062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 621
vespene: 330
food_cap: 84
food_used: 65
food_army: 30
food_workers: 35
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 50
  }
}

score{
 score_type: Melee
score: 5929
score_details {
  idle_production_time: 11596.8125
  idle_worker_time: 52.75
  total_value_units: 10900.0
  total_value_structures: 2600.0
  killed_value_units: 5900.0
  killed_value_structures: 600.0
  collected_minerals: 11425.0
  collected_vespene: 2308.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 0.0
  spent_minerals: 11254.0
  spent_vespene: 2250.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4050.0
    economy: 1300.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2350.0
    economy: 2968.0
    technology: 1286.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 225.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1050.0
    economy: 2650.0
    technology: 650.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3600.0
    economy: 7100.0
    technology: 2150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 14536.7451172
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 14515.609375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 221
vespene: 58
food_cap: 78
food_used: 39
food_army: 20
food_workers: 19
idle_worker_count: 19
army_count: 8
warp_gate_count: 0

game_loop:  14752
ui_data{
 
Score:  5929
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
