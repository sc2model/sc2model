----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3428
  player_apm: 92
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3365
  player_apm: 64
}
game_duration_loops: 10148
game_duration_seconds: 453.067321777
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8808
score_details {
  idle_production_time: 1173.75
  idle_worker_time: 502.75
  total_value_units: 5425.0
  total_value_structures: 2950.0
  killed_value_units: 2650.0
  killed_value_structures: 0.0
  collected_minerals: 7645.0
  collected_vespene: 1788.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 335.0
  spent_minerals: 6900.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 250.0
    technology: -150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: -75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 3550.0
    technology: 1100.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2500.0
    economy: 3800.0
    technology: 1100.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3613.49902344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1077.0
    shields: 2593.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 795
vespene: 638
food_cap: 86
food_used: 74
food_army: 36
food_workers: 38
idle_worker_count: 2
army_count: 18
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 77
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 9005
score_details {
  idle_production_time: 1220.0
  idle_worker_time: 521.25
  total_value_units: 5925.0
  total_value_structures: 2950.0
  killed_value_units: 2800.0
  killed_value_structures: 0.0
  collected_minerals: 7810.0
  collected_vespene: 1820.0
  collection_rate_minerals: 1623.0
  collection_rate_vespene: 313.0
  spent_minerals: 6900.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1850.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 250.0
    technology: -150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: -75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 3550.0
    technology: 1100.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 3800.0
    technology: 1100.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3871.98583984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1077.0
    shields: 2653.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 960
vespene: 670
food_cap: 86
food_used: 74
food_army: 36
food_workers: 38
idle_worker_count: 2
army_count: 18
warp_gate_count: 4

game_loop:  10148
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 77
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 9
    shields: 70
    energy: 0
  }
  units {
    unit_type: 136
    player_relative: 1
    health: 80
    shields: 100
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 9
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 49
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 23
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 73
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 75
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

Score:  9005
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
