----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3391
  player_apm: 93
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3351
  player_apm: 117
}
game_duration_loops: 10401
game_duration_seconds: 464.362762451
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8962
score_details {
  idle_production_time: 3666.3125
  idle_worker_time: 0.75
  total_value_units: 7050.0
  total_value_structures: 1600.0
  killed_value_units: 1825.0
  killed_value_structures: 0.0
  collected_minerals: 8270.0
  collected_vespene: 1848.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 425.0
  spent_minerals: 7906.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 2081.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1900.0
    economy: 3375.0
    technology: 875.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 200.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 5975.0
    technology: 1025.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 300.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 1696.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3424.73681641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 414
vespene: 598
food_cap: 90
food_used: 68
food_army: 44
food_workers: 24
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 132
    shields: 0
    energy: 62
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 118
  }
}

score{
 score_type: Melee
score: 9373
score_details {
  idle_production_time: 4115.5
  idle_worker_time: 0.75
  total_value_units: 7200.0
  total_value_structures: 1600.0
  killed_value_units: 2050.0
  killed_value_structures: 0.0
  collected_minerals: 8545.0
  collected_vespene: 1984.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 470.0
  spent_minerals: 7906.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 2081.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1900.0
    economy: 3375.0
    technology: 875.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 200.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 5975.0
    technology: 1025.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 300.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 1798.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3452.73681641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 689
vespene: 734
food_cap: 90
food_used: 68
food_army: 44
food_workers: 24
idle_worker_count: 0
army_count: 19
warp_gate_count: 0

game_loop:  10401
ui_data{
 
Score:  9373
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
