----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3400
  player_apm: 226
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3587
  player_apm: 176
}
game_duration_loops: 24381
game_duration_seconds: 1088.51342773
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13706
score_details {
  idle_production_time: 5039.75
  idle_worker_time: 31.3125
  total_value_units: 7700.0
  total_value_structures: 2350.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 10140.0
  collected_vespene: 2572.0
  collection_rate_minerals: 2295.0
  collection_rate_vespene: 963.0
  spent_minerals: 9756.0
  spent_vespene: 2500.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: -69.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2700.0
    economy: 6200.0
    technology: 1450.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 6750.0
    technology: 1450.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 966.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 987.290527344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 434
vespene: 72
food_cap: 112
food_used: 106
food_army: 48
food_workers: 58
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22965
score_details {
  idle_production_time: 26558.4375
  idle_worker_time: 381.875
  total_value_units: 22250.0
  total_value_structures: 4025.0
  killed_value_units: 12200.0
  killed_value_structures: 875.0
  collected_minerals: 27720.0
  collected_vespene: 8676.0
  collection_rate_minerals: 2379.0
  collection_rate_vespene: 873.0
  spent_minerals: 22356.0
  spent_vespene: 6425.0
  food_used {
    none: 0.0
    army: 43.5
    economy: 62.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8600.0
    economy: 1775.0
    technology: 700.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6100.0
    economy: 2606.0
    technology: 625.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2875.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1350.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2475.0
    economy: 7675.0
    technology: 1450.0
    upgrade: 925.0
  }
  used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 600.0
    upgrade: 925.0
  }
  total_used_minerals {
    none: 0.0
    army: 10950.0
    economy: 11600.0
    technology: 2575.0
    upgrade: 925.0
  }
  total_used_vespene {
    none: 0.0
    army: 4950.0
    economy: 0.0
    technology: 1050.0
    upgrade: 925.0
  }
  total_damage_dealt {
    life: 17557.0195312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23707.2929688
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 5414
vespene: 2251
food_cap: 200
food_used: 105
food_army: 43
food_workers: 62
idle_worker_count: 0
army_count: 37
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 14
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 129
    player_relative: 1
    health: 181
    shields: 0
    energy: 150
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 183
    shields: 0
    energy: 139
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 183
    shields: 0
    energy: 200
  }
}

score{
 score_type: Melee
score: 28331
score_details {
  idle_production_time: 41699.1875
  idle_worker_time: 720.8125
  total_value_units: 28550.0
  total_value_structures: 4800.0
  killed_value_units: 16450.0
  killed_value_structures: 1375.0
  collected_minerals: 34537.0
  collected_vespene: 12100.0
  collection_rate_minerals: 2508.0
  collection_rate_vespene: 806.0
  spent_minerals: 27931.0
  spent_vespene: 8875.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11100.0
    economy: 2925.0
    technology: 1000.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9525.0
    economy: 2706.0
    technology: 625.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3625.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1950.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2575.0
    economy: 8275.0
    technology: 1775.0
    upgrade: 1650.0
  }
  used_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 800.0
    upgrade: 1650.0
  }
  total_used_minerals {
    none: 0.0
    army: 15900.0
    economy: 12550.0
    technology: 2900.0
    upgrade: 1275.0
  }
  total_used_vespene {
    none: 0.0
    army: 6475.0
    economy: 0.0
    technology: 1250.0
    upgrade: 1275.0
  }
  total_damage_dealt {
    life: 24371.3867188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 29767.3242188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 6656
vespene: 3225
food_cap: 200
food_used: 106
food_army: 46
food_workers: 60
idle_worker_count: 6
army_count: 37
warp_gate_count: 0

game_loop:  24381
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 92
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 115
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 115
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 115
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 92
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 66
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 198
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 22
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 115
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 91
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 115
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  28331
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
