----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 64
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3049
  player_apm: 76
}
game_duration_loops: 5239
game_duration_seconds: 233.900253296
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4744
score_details {
  idle_production_time: 2412.3125
  idle_worker_time: 0.0
  total_value_units: 2600.0
  total_value_structures: 825.0
  killed_value_units: 150.0
  killed_value_structures: 0.0
  collected_minerals: 3380.0
  collected_vespene: 364.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 111.0
  spent_minerals: 2725.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 2325.0
    technology: 250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1000.0
    economy: 2375.0
    technology: 250.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 353.016601562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 250.733398438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 705
vespene: 264
food_cap: 36
food_used: 39
food_army: 18
food_workers: 21
idle_worker_count: 0
army_count: 28
warp_gate_count: 0

game_loop:  5239
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}

Score:  4744
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
