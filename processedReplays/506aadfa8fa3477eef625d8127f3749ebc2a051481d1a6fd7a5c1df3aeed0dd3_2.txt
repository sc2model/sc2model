----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2541
  player_apm: 100
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2819
  player_apm: 75
}
game_duration_loops: 48800
game_duration_seconds: 2178.72338867
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9356
score_details {
  idle_production_time: 1448.5
  idle_worker_time: 293.1875
  total_value_units: 3475.0
  total_value_structures: 3575.0
  killed_value_units: 450.0
  killed_value_structures: 350.0
  collected_minerals: 7345.0
  collected_vespene: 1336.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 604.0
  spent_minerals: 6775.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1850.0
    economy: 3450.0
    technology: 1675.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 475.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1850.0
    economy: 3400.0
    technology: 1625.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 450.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 2027.32714844
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 81.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 620
vespene: 161
food_cap: 62
food_used: 62
food_army: 37
food_workers: 25
idle_worker_count: 0
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22750
score_details {
  idle_production_time: 6757.125
  idle_worker_time: 3299.8125
  total_value_units: 10375.0
  total_value_structures: 8900.0
  killed_value_units: 2250.0
  killed_value_structures: 1625.0
  collected_minerals: 17280.0
  collected_vespene: 7220.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 761.0
  spent_minerals: 17050.0
  spent_vespene: 4125.0
  food_used {
    none: 0.0
    army: 100.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 900.0
    economy: 2525.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 750.0
    economy: 150.0
    technology: 625.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5000.0
    economy: 7500.0
    technology: 3000.0
    upgrade: 450.0
  }
  used_vespene {
    none: 100.0
    army: 1550.0
    economy: 150.0
    technology: 1075.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 100.0
    army: 5425.0
    economy: 7900.0
    technology: 3625.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 100.0
    army: 1750.0
    economy: 300.0
    technology: 1575.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 9835.95019531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2198.78662109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2396.26806641
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 280
vespene: 3095
food_cap: 164
food_used: 164
food_army: 100
food_workers: 61
idle_worker_count: 25
army_count: 59
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 7
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 27
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 3
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 123
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 36670
score_details {
  idle_production_time: 10517.0625
  idle_worker_time: 11379.375
  total_value_units: 18800.0
  total_value_structures: 12375.0
  killed_value_units: 10575.0
  killed_value_structures: 2400.0
  collected_minerals: 32640.0
  collected_vespene: 12680.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 537.0
  spent_minerals: 26000.0
  spent_vespene: 8075.0
  food_used {
    none: 0.0
    army: 104.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6275.0
    economy: 3750.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6200.0
    economy: 250.0
    technology: 1000.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5400.0
    economy: 9075.0
    technology: 4000.0
    upgrade: 800.0
  }
  used_vespene {
    none: 100.0
    army: 3150.0
    economy: 150.0
    technology: 1800.0
    upgrade: 800.0
  }
  total_used_minerals {
    none: 100.0
    army: 10975.0
    economy: 10075.0
    technology: 5000.0
    upgrade: 625.0
  }
  total_used_vespene {
    none: 100.0
    army: 3825.0
    economy: 300.0
    technology: 2600.0
    upgrade: 625.0
  }
  total_damage_dealt {
    life: 22860.5878906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10902.9931641
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 8861.484375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 6690
vespene: 4605
food_cap: 200
food_used: 179
food_army: 104
food_workers: 75
idle_worker_count: 12
army_count: 50
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 10
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 5
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 188
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 30
    shields: 0
    energy: 182
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 30
    shields: 0
    energy: 186
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 180
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 159
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 183
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 163
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 178
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 191
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 98
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 101
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 97
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 101
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 177
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 183
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 158
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 30
    shields: 0
    energy: 179
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 178
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 363
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 138
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 100
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 128
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 108
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 36102
score_details {
  idle_production_time: 17489.9375
  idle_worker_time: 24832.25
  total_value_units: 29950.0
  total_value_structures: 15050.0
  killed_value_units: 19125.0
  killed_value_structures: 2825.0
  collected_minerals: 43364.0
  collected_vespene: 14648.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 559.0
  spent_minerals: 35962.0
  spent_vespene: 12812.0
  food_used {
    none: 0.0
    army: 139.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11975.0
    economy: 4525.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10123.0
    economy: 5747.0
    technology: 1700.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3148.0
    economy: 488.0
    technology: 1100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 7650.0
    economy: 6525.0
    technology: 3425.0
    upgrade: 1450.0
  }
  used_vespene {
    none: 100.0
    army: 4750.0
    economy: 150.0
    technology: 1600.0
    upgrade: 1450.0
  }
  total_used_minerals {
    none: 100.0
    army: 18025.0
    economy: 13425.0
    technology: 5125.0
    upgrade: 1450.0
  }
  total_used_vespene {
    none: 100.0
    army: 7925.0
    economy: 1200.0
    technology: 2700.0
    upgrade: 1450.0
  }
  total_damage_dealt {
    life: 35010.1914062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 34629.03125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 13000.4853516
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 7243
vespene: 1659
food_cap: 181
food_used: 180
food_army: 139
food_workers: 41
idle_worker_count: 25
army_count: 52
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 26
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 10
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 122
    shields: 0
    energy: 200
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 385
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 80
    shields: 0
    energy: 200
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 52
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 39292
score_details {
  idle_production_time: 27158.125
  idle_worker_time: 28239.8125
  total_value_units: 33750.0
  total_value_structures: 19275.0
  killed_value_units: 27475.0
  killed_value_structures: 3675.0
  collected_minerals: 51156.0
  collected_vespene: 16940.0
  collection_rate_minerals: 1327.0
  collection_rate_vespene: 716.0
  spent_minerals: 42837.0
  spent_vespene: 14712.0
  food_used {
    none: 0.0
    army: 144.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 17225.0
    economy: 5575.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11921.0
    economy: 7565.0
    technology: 3050.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4296.0
    economy: 641.0
    technology: 1825.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 8200.0
    economy: 8600.0
    technology: 2825.0
    upgrade: 1450.0
  }
  used_vespene {
    none: 100.0
    army: 4700.0
    economy: 300.0
    technology: 1475.0
    upgrade: 1450.0
  }
  total_used_minerals {
    none: 100.0
    army: 20275.0
    economy: 17750.0
    technology: 5875.0
    upgrade: 1450.0
  }
  total_used_vespene {
    none: 100.0
    army: 8925.0
    economy: 1800.0
    technology: 3300.0
    upgrade: 1450.0
  }
  total_damage_dealt {
    life: 44696.8828125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 47787.0664062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 15189.5224609
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 8092
vespene: 2000
food_cap: 200
food_used: 188
food_army: 144
food_workers: 44
idle_worker_count: 1
army_count: 53
warp_gate_count: 0

game_loop:  48800
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 51
  count: 14
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 9
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 4
}

Score:  39292
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
