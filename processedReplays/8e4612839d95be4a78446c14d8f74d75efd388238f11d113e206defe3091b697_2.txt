----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3020
  player_apm: 54
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3004
  player_apm: 83
}
game_duration_loops: 10248
game_duration_seconds: 457.531921387
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7842
score_details {
  idle_production_time: 1403.8125
  idle_worker_time: 9.3125
  total_value_units: 4025.0
  total_value_structures: 3625.0
  killed_value_units: 3175.0
  killed_value_structures: 1950.0
  collected_minerals: 7570.0
  collected_vespene: 1172.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 0.0
  spent_minerals: 6475.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 1200.0
    economy: 2400.0
    technology: 900.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 225.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 800.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 2825.0
    technology: 1350.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2100.0
    economy: 3625.0
    technology: 1500.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 14384.1210938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2570.0
    shields: 2893.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1145
vespene: 497
food_cap: 62
food_used: 50
food_army: 22
food_workers: 28
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 7742
score_details {
  idle_production_time: 1465.8125
  idle_worker_time: 9.3125
  total_value_units: 4025.0
  total_value_structures: 3625.0
  killed_value_units: 3175.0
  killed_value_structures: 2575.0
  collected_minerals: 7770.0
  collected_vespene: 1172.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 0.0
  spent_minerals: 6975.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 1200.0
    economy: 3025.0
    technology: 900.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 225.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 800.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 3025.0
    technology: 1350.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2100.0
    economy: 3625.0
    technology: 1500.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 300.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 15539.1210938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2570.0
    shields: 2893.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 845
vespene: 497
food_cap: 62
food_used: 50
food_army: 22
food_workers: 28
idle_worker_count: 0
army_count: 11
warp_gate_count: 0

game_loop:  10248
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  7742
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
