----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3802
  player_apm: 289
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3705
  player_apm: 142
}
game_duration_loops: 18340
game_duration_seconds: 818.807128906
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8586
score_details {
  idle_production_time: 5276.5625
  idle_worker_time: 9.9375
  total_value_units: 7350.0
  total_value_structures: 1500.0
  killed_value_units: 1050.0
  killed_value_structures: 0.0
  collected_minerals: 8410.0
  collected_vespene: 1876.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 582.0
  spent_minerals: 8275.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1600.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1850.0
    economy: 4150.0
    technology: 850.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 100.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3550.0
    economy: 4950.0
    technology: 1000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 995.299316406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3539.74121094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 185
vespene: 101
food_cap: 82
food_used: 81
food_army: 41
food_workers: 40
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 5
  }
}

score{
 score_type: Melee
score: 10850
score_details {
  idle_production_time: 18443.875
  idle_worker_time: 14.8125
  total_value_units: 17950.0
  total_value_structures: 2600.0
  killed_value_units: 5475.0
  killed_value_structures: 0.0
  collected_minerals: 19715.0
  collected_vespene: 6360.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 492.0
  spent_minerals: 18125.0
  spent_vespene: 5375.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3975.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8900.0
    economy: 3175.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 4225.0
    technology: 1400.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 550.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 9500.0
    economy: 8250.0
    technology: 2050.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 4850.0
    economy: 0.0
    technology: 800.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 6217.63525391
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 26056.3125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1640
vespene: 985
food_cap: 124
food_used: 35
food_army: 4
food_workers: 31
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  18340
ui_data{
 
Score:  10850
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
