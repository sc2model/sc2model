----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2798
  player_apm: 120
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 58
}
game_duration_loops: 11309
game_duration_seconds: 504.901306152
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10609
score_details {
  idle_production_time: 1075.8125
  idle_worker_time: 84.5625
  total_value_units: 4750.0
  total_value_structures: 4200.0
  killed_value_units: 1975.0
  killed_value_structures: 700.0
  collected_minerals: 8590.0
  collected_vespene: 1644.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 313.0
  spent_minerals: 7825.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 39.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 200.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2100.0
    economy: 4200.0
    technology: 1700.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2100.0
    economy: 4200.0
    technology: 1700.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2013.58203125
    shields: 2236.40917969
    energy: 0.0
  }
  total_damage_taken {
    life: 671.5
    shields: 722.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 815
vespene: 119
food_cap: 126
food_used: 77
food_army: 39
food_workers: 38
idle_worker_count: 0
army_count: 20
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 18
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 13
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 37
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 46
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 10
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 47
    shields: 23
    energy: 0
    build_progress: 0.40625
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 49
    shields: 25
    energy: 0
    build_progress: 0.43359375
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 48
    shields: 24
    energy: 0
    build_progress: 0.41796875
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 46
    shields: 23
    energy: 0
    build_progress: 0.39453125
  }
}

score{
 score_type: Melee
score: 12765
score_details {
  idle_production_time: 1371.1875
  idle_worker_time: 84.5625
  total_value_units: 6250.0
  total_value_structures: 4200.0
  killed_value_units: 2375.0
  killed_value_structures: 1500.0
  collected_minerals: 10330.0
  collected_vespene: 1960.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 313.0
  spent_minerals: 9375.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 38.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1450.0
    economy: 1050.0
    technology: 750.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2850.0
    economy: 4950.0
    technology: 1700.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3375.0
    economy: 4200.0
    technology: 1700.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 4837.58203125
    shields: 4926.03417969
    energy: 0.0
  }
  total_damage_taken {
    life: 772.0
    shields: 928.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1005
vespene: 435
food_cap: 126
food_used: 93
food_army: 55
food_workers: 38
idle_worker_count: 0
army_count: 30
warp_gate_count: 4

game_loop:  11309
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 25
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 37
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 26
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 26
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
}

Score:  12765
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
