----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3718
  player_apm: 258
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 99
}
game_duration_loops: 18164
game_duration_seconds: 810.949462891
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10330
score_details {
  idle_production_time: 840.4375
  idle_worker_time: 132.5
  total_value_units: 5350.0
  total_value_structures: 3600.0
  killed_value_units: 625.0
  killed_value_structures: 0.0
  collected_minerals: 8125.0
  collected_vespene: 1880.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 358.0
  spent_minerals: 7250.0
  spent_vespene: 1550.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 3800.0
    technology: 1400.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 200.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2300.0
    economy: 4050.0
    technology: 1400.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 200.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1836.37011719
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 665.0
    shields: 1062.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 925
vespene: 330
food_cap: 86
food_used: 82
food_army: 42
food_workers: 40
idle_worker_count: 0
army_count: 19
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 17
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 16
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 8
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 33
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 45
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 19
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 29
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 20
    shields: 10
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 39
    shields: 0
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 21
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 11022
score_details {
  idle_production_time: 3180.3125
  idle_worker_time: 233.4375
  total_value_units: 15775.0
  total_value_structures: 5000.0
  killed_value_units: 10550.0
  killed_value_structures: 0.0
  collected_minerals: 16750.0
  collected_vespene: 5272.0
  collection_rate_minerals: 727.0
  collection_rate_vespene: 627.0
  spent_minerals: 16675.0
  spent_vespene: 4125.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8050.0
    economy: 775.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4400.0
    technology: 1750.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 9750.0
    economy: 5250.0
    technology: 1900.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 3475.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 15560.5546875
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8889.91601562
    shields: 10440.6894531
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 125
vespene: 1147
food_cap: 102
food_used: 80
food_army: 32
food_workers: 48
idle_worker_count: 1
army_count: 11
warp_gate_count: 6

game_loop:  18164
ui_data{
 
Score:  11022
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
