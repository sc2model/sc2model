----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4310
  player_apm: 345
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4360
  player_apm: 148
}
game_duration_loops: 18576
game_duration_seconds: 829.343566895
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10062
score_details {
  idle_production_time: 1113.0
  idle_worker_time: 482.3125
  total_value_units: 6150.0
  total_value_structures: 2825.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 8165.0
  collected_vespene: 1972.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 537.0
  spent_minerals: 7200.0
  spent_vespene: 1325.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 4625.0
    technology: 900.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1925.0
    economy: 5125.0
    technology: 750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1293.41796875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 477.875
    shields: 1049.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1015
vespene: 647
food_cap: 85
food_used: 85
food_army: 33
food_workers: 51
idle_worker_count: 1
army_count: 15
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 495
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 311
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 21
    energy: 79
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 52
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 46
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 85
    shields: 8
    energy: 31
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 58
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 167
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 34
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 94
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
}

score{
 score_type: Melee
score: 14207
score_details {
  idle_production_time: 4826.3125
  idle_worker_time: 2181.25
  total_value_units: 17200.0
  total_value_structures: 8175.0
  killed_value_units: 15000.0
  killed_value_structures: 0.0
  collected_minerals: 22890.0
  collected_vespene: 6892.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 895.0
  spent_minerals: 21125.0
  spent_vespene: 5875.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10975.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8950.0
    economy: 2875.0
    technology: 750.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 5775.0
    technology: 2650.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 750.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 9950.0
    economy: 8575.0
    technology: 3100.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 6150.0
    economy: 0.0
    technology: 750.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 14555.3994141
    shields: 100.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13415.5390625
    shields: 15260.0703125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1815
vespene: 1017
food_cap: 133
food_used: 69
food_army: 10
food_workers: 58
idle_worker_count: 4
army_count: 1
warp_gate_count: 8

game_loop:  18576
ui_data{
 single {
  unit {
    unit_type: 73
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  14207
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
