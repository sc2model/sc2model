----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3281
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3229
  player_apm: 128
}
game_duration_loops: 18933
game_duration_seconds: 845.282226562
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10709
score_details {
  idle_production_time: 1241.0
  idle_worker_time: 406.0
  total_value_units: 6625.0
  total_value_structures: 3350.0
  killed_value_units: 1725.0
  killed_value_structures: 0.0
  collected_minerals: 8505.0
  collected_vespene: 2104.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 582.0
  spent_minerals: 8150.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 37.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1025.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2200.0
    economy: 4350.0
    technology: 1550.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2750.0
    economy: 4600.0
    technology: 1100.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 740.0
    shields: 1622.5
    energy: 0.0
  }
  total_damage_taken {
    life: 741.75
    shields: 916.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 405
vespene: 379
food_cap: 94
food_used: 86
food_army: 37
food_workers: 48
idle_worker_count: 2
army_count: 17
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 17048
score_details {
  idle_production_time: 4876.8125
  idle_worker_time: 3033.125
  total_value_units: 16375.0
  total_value_structures: 8100.0
  killed_value_units: 8500.0
  killed_value_structures: 100.0
  collected_minerals: 21440.0
  collected_vespene: 7108.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 851.0
  spent_minerals: 19375.0
  spent_vespene: 5350.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5650.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6775.0
    economy: 1500.0
    technology: 750.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1250.0
    economy: 6000.0
    technology: 3250.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 900.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 9025.0
    economy: 7500.0
    technology: 3850.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 5000.0
    economy: 0.0
    technology: 900.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 5094.125
    shields: 7041.625
    energy: 0.0
  }
  total_damage_taken {
    life: 7106.0
    shields: 10008.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2115
vespene: 1758
food_cap: 149
food_used: 82
food_army: 21
food_workers: 61
idle_worker_count: 17
army_count: 10
warp_gate_count: 5

game_loop:  18933
ui_data{
 multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

Score:  17048
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
