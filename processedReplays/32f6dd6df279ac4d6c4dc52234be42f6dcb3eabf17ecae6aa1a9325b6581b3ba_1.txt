----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3152
  player_apm: 141
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3600
  player_apm: 186
}
game_duration_loops: 8438
game_duration_seconds: 376.722717285
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3719
score_details {
  idle_production_time: 762.75
  idle_worker_time: 333.5
  total_value_units: 3050.0
  total_value_structures: 1850.0
  killed_value_units: 525.0
  killed_value_structures: 100.0
  collected_minerals: 4485.0
  collected_vespene: 1204.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 3587.0
  spent_vespene: 750.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 4.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 1182.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 400.0
    economy: 1025.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1225.0
    economy: 2650.0
    technology: 600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 486.625
    shields: 1464.375
    energy: 0.0
  }
  total_damage_taken {
    life: 6827.05810547
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 148.885742188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 940
vespene: 454
food_cap: 31
food_used: 13
food_army: 9
food_workers: 4
idle_worker_count: 2
army_count: 3
warp_gate_count: 0

game_loop:  8438
ui_data{
 single {
  unit {
    unit_type: 54
    player_relative: 1
    health: 111
    shields: 0
    energy: 165
  }
}

Score:  3719
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
