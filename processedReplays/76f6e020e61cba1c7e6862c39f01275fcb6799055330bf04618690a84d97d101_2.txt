----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3854
  player_apm: 165
}
game_duration_loops: 26703
game_duration_seconds: 1192.18139648
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11498
score_details {
  idle_production_time: 4225.5625
  idle_worker_time: 66.375
  total_value_units: 8850.0
  total_value_structures: 1575.0
  killed_value_units: 1150.0
  killed_value_structures: 0.0
  collected_minerals: 10670.0
  collected_vespene: 1828.0
  collection_rate_minerals: 2519.0
  collection_rate_vespene: 335.0
  spent_minerals: 10550.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 52.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2175.0
    economy: 5900.0
    technology: 1125.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 200.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 2400.0
    economy: 7250.0
    technology: 1125.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1395.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2905.94287109
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 170
vespene: 253
food_cap: 122
food_used: 110
food_army: 52
food_workers: 58
idle_worker_count: 1
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 110
  count: 18
}
groups {
  control_group_index: 9
  leader_unit_type: 126
  count: 2
}
single {
  unit {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21863
score_details {
  idle_production_time: 14769.5625
  idle_worker_time: 2142.5
  total_value_units: 27350.0
  total_value_structures: 4075.0
  killed_value_units: 12875.0
  killed_value_structures: 700.0
  collected_minerals: 30689.0
  collected_vespene: 8024.0
  collection_rate_minerals: 2060.0
  collection_rate_vespene: 1164.0
  spent_minerals: 28350.0
  spent_vespene: 6175.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 84.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8800.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3325.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11050.0
    economy: 2600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1425.0
    economy: 11025.0
    technology: 2150.0
    upgrade: 1100.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 600.0
    upgrade: 1100.0
  }
  total_used_minerals {
    none: 0.0
    army: 12650.0
    economy: 14875.0
    technology: 1800.0
    upgrade: 1100.0
  }
  total_used_vespene {
    none: 0.0
    army: 4800.0
    economy: 0.0
    technology: 300.0
    upgrade: 1100.0
  }
  total_damage_dealt {
    life: 13124.2636719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 25319.9785156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2389
vespene: 1849
food_cap: 200
food_used: 108
food_army: 24
food_workers: 84
idle_worker_count: 1
army_count: 6
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 110
  count: 5
}
groups {
  control_group_index: 9
  leader_unit_type: 126
  count: 1
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 160
  }
}

score{
 score_type: Melee
score: 28815
score_details {
  idle_production_time: 25727.9375
  idle_worker_time: 5274.75
  total_value_units: 42350.0
  total_value_structures: 6500.0
  killed_value_units: 18775.0
  killed_value_structures: 4975.0
  collected_minerals: 44439.0
  collected_vespene: 14776.0
  collection_rate_minerals: 3415.0
  collection_rate_vespene: 1231.0
  spent_minerals: 39800.0
  spent_vespene: 12200.0
  food_used {
    none: 0.0
    army: 82.0
    economy: 85.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 12150.0
    economy: 5675.0
    technology: 1500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 3925.0
    economy: 150.0
    technology: 250.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16925.0
    economy: 5025.0
    technology: 2675.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6000.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4250.0
    economy: 9800.0
    technology: 450.0
    upgrade: 1575.0
  }
  used_vespene {
    none: 0.0
    army: 3750.0
    economy: 0.0
    technology: 150.0
    upgrade: 1575.0
  }
  total_used_minerals {
    none: 0.0
    army: 21875.0
    economy: 16875.0
    technology: 3125.0
    upgrade: 1575.0
  }
  total_used_vespene {
    none: 0.0
    army: 10150.0
    economy: 0.0
    technology: 800.0
    upgrade: 1575.0
  }
  total_damage_dealt {
    life: 40862.3828125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 54209.015625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 4689
vespene: 2576
food_cap: 194
food_used: 167
food_army: 82
food_workers: 85
idle_worker_count: 0
army_count: 38
warp_gate_count: 0

game_loop:  26703
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 108
  count: 41
}
groups {
  control_group_index: 9
  leader_unit_type: 108
  count: 38
}
multi {
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  28815
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
