----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5338
  player_apm: 209
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5514
  player_apm: 236
}
game_duration_loops: 17985
game_duration_seconds: 802.957824707
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12687
score_details {
  idle_production_time: 1202.875
  idle_worker_time: 114.5625
  total_value_units: 5250.0
  total_value_structures: 4750.0
  killed_value_units: 1025.0
  killed_value_structures: 0.0
  collected_minerals: 9970.0
  collected_vespene: 2440.0
  collection_rate_minerals: 2351.0
  collection_rate_vespene: 627.0
  spent_minerals: 9236.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 350.0
    technology: -339.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1400.0
    economy: 5800.0
    technology: 1750.0
    upgrade: 300.0
  }
  used_vespene {
    none: 100.0
    army: 800.0
    economy: 0.0
    technology: 550.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 100.0
    army: 1525.0
    economy: 6300.0
    technology: 1600.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 100.0
    army: 825.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1301.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2505.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 18.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 780
vespene: 657
food_cap: 117
food_used: 84
food_army: 28
food_workers: 53
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 56
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 3
}
production {
  unit {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  build_queue {
    unit_type: 48
    build_progress: 0.552500009537
  }
}

score{
 score_type: Melee
score: 23137
score_details {
  idle_production_time: 5492.0
  idle_worker_time: 781.9375
  total_value_units: 18975.0
  total_value_structures: 7800.0
  killed_value_units: 13950.0
  killed_value_structures: 0.0
  collected_minerals: 25040.0
  collected_vespene: 7504.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 963.0
  spent_minerals: 24686.0
  spent_vespene: 5875.0
  food_used {
    none: 0.0
    army: 98.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10000.0
    economy: 1600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6929.0
    economy: 1100.0
    technology: -339.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1508.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 4900.0
    economy: 7250.0
    technology: 3150.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 100.0
    army: 1775.0
    economy: 150.0
    technology: 1000.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 100.0
    army: 11525.0
    economy: 8950.0
    technology: 3150.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 100.0
    army: 3150.0
    economy: 300.0
    technology: 1000.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 17659.0585938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 13316.4414062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 4287.65917969
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 394
vespene: 1618
food_cap: 164
food_used: 164
food_army: 98
food_workers: 66
idle_worker_count: 1
army_count: 71
warp_gate_count: 0

game_loop:  17985
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 22
  count: 3
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 129
    shields: 0
    energy: 5
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 143
    shields: 0
    energy: 4
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
}

Score:  23137
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
