----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5337
  player_apm: 288
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5164
  player_apm: 209
}
game_duration_loops: 12144
game_duration_seconds: 542.180664062
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8902
score_details {
  idle_production_time: 8464.1875
  idle_worker_time: 131.0
  total_value_units: 8850.0
  total_value_structures: 1550.0
  killed_value_units: 3550.0
  killed_value_structures: 0.0
  collected_minerals: 9490.0
  collected_vespene: 1312.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 335.0
  spent_minerals: 9300.0
  spent_vespene: 1250.0
  food_used {
    none: 1.0
    army: 44.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2775.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2125.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2000.0
    economy: 4675.0
    technology: 875.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 4475.0
    economy: 5125.0
    technology: 1025.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 6383.31396484
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4755.65673828
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 240
vespene: 62
food_cap: 122
food_used: 87
food_army: 44
food_workers: 38
idle_worker_count: 0
army_count: 22
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 88
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 89
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10991
score_details {
  idle_production_time: 9877.4375
  idle_worker_time: 131.0
  total_value_units: 12000.0
  total_value_structures: 1675.0
  killed_value_units: 5850.0
  killed_value_structures: 200.0
  collected_minerals: 12345.0
  collected_vespene: 2208.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 671.0
  spent_minerals: 12156.0
  spent_vespene: 1981.0
  food_used {
    none: 0.0
    army: 66.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4150.0
    economy: 800.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3281.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 581.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: -50.0
    army: 2725.0
    economy: 5350.0
    technology: 1025.0
    upgrade: 300.0
  }
  used_vespene {
    none: -25.0
    army: 750.0
    economy: 0.0
    technology: 150.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 6250.0
    economy: 6000.0
    technology: 1175.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 10213.0136719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7170.28173828
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 239
vespene: 227
food_cap: 138
food_used: 114
food_army: 66
food_workers: 48
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  12144
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 88
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 89
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 90
  count: 1
}

Score:  10991
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
