----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3399
  player_apm: 102
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3357
  player_apm: 96
}
game_duration_loops: 16203
game_duration_seconds: 723.398681641
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8168
score_details {
  idle_production_time: 1127.4375
  idle_worker_time: 771.5
  total_value_units: 4250.0
  total_value_structures: 3300.0
  killed_value_units: 875.0
  killed_value_structures: 75.0
  collected_minerals: 7460.0
  collected_vespene: 1236.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 201.0
  spent_minerals: 6775.0
  spent_vespene: 1200.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 125.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 828.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1450.0
    economy: 3400.0
    technology: 1275.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 425.0
    economy: 0.0
    technology: 375.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 1700.0
    economy: 4550.0
    technology: 1225.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 550.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 810.0
    shields: 974.875
    energy: 0.0
  }
  total_damage_taken {
    life: 2783.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 433.48828125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 707
vespene: 36
food_cap: 61
food_used: 54
food_army: 28
food_workers: 26
idle_worker_count: 2
army_count: 17
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 3
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 11639
score_details {
  idle_production_time: 3910.8125
  idle_worker_time: 1555.4375
  total_value_units: 9975.0
  total_value_structures: 4450.0
  killed_value_units: 9880.0
  killed_value_structures: 750.0
  collected_minerals: 13140.0
  collected_vespene: 2952.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 425.0
  spent_minerals: 12925.0
  spent_vespene: 2450.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 37.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6380.0
    economy: 2200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3450.0
    economy: 828.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2850.0
    economy: 4800.0
    technology: 1375.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 800.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 6000.0
    economy: 6050.0
    technology: 1375.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 50.0
    army: 1425.0
    economy: 0.0
    technology: 375.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 8716.86425781
    shields: 8228.33300781
    energy: 0.0
  }
  total_damage_taken {
    life: 6745.35546875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1420.81152344
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 237
vespene: 502
food_cap: 125
food_used: 94
food_army: 57
food_workers: 37
idle_worker_count: 2
army_count: 32
warp_gate_count: 0

game_loop:  16203
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 995
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 247
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  11639
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
