----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 151
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3759
  player_apm: 155
}
game_duration_loops: 17546
game_duration_seconds: 783.358215332
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10169
score_details {
  idle_production_time: 1272.125
  idle_worker_time: 636.6875
  total_value_units: 4400.0
  total_value_structures: 3225.0
  killed_value_units: 375.0
  killed_value_structures: 0.0
  collected_minerals: 7765.0
  collected_vespene: 1604.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 313.0
  spent_minerals: 7375.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 225.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: -150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 400.0
    army: 2175.0
    economy: 4150.0
    technology: 1300.0
    upgrade: 250.0
  }
  used_vespene {
    none: 50.0
    army: 400.0
    economy: 0.0
    technology: 325.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 50.0
    army: 2125.0
    economy: 4100.0
    technology: 1050.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 525.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 878.663574219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1147.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 440
vespene: 429
food_cap: 94
food_used: 78
food_army: 44
food_workers: 32
idle_worker_count: 2
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18860
score_details {
  idle_production_time: 5077.6875
  idle_worker_time: 1361.875
  total_value_units: 12975.0
  total_value_structures: 5950.0
  killed_value_units: 3700.0
  killed_value_structures: 0.0
  collected_minerals: 19710.0
  collected_vespene: 5068.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 873.0
  spent_minerals: 17750.0
  spent_vespene: 4450.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5175.0
    economy: -75.0
    technology: -75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 400.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2750.0
    economy: 6600.0
    technology: 2150.0
    upgrade: 1575.0
  }
  used_vespene {
    none: 50.0
    army: 775.0
    economy: 150.0
    technology: 575.0
    upgrade: 1575.0
  }
  total_used_minerals {
    none: 50.0
    army: 8025.0
    economy: 6875.0
    technology: 2150.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 50.0
    army: 2100.0
    economy: 300.0
    technology: 575.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 4657.6875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6883.8203125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 430.754394531
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1997
vespene: 613
food_cap: 157
food_used: 107
food_army: 53
food_workers: 54
idle_worker_count: 2
army_count: 34
warp_gate_count: 0

game_loop:  17546
ui_data{
 multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 74
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1484
    shields: 0
    energy: 43
  }
  units {
    unit_type: 130
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 18
    player_relative: 1
    health: 1419
    shields: 0
    energy: 0
    build_progress: 0.939999997616
  }
}

Score:  18860
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
