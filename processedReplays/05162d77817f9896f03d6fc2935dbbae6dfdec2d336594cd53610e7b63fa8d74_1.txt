----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4079
  player_apm: 228
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3863
  player_apm: 267
}
game_duration_loops: 11673
game_duration_seconds: 521.15246582
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11099
score_details {
  idle_production_time: 1389.875
  idle_worker_time: 49.125
  total_value_units: 4950.0
  total_value_structures: 4500.0
  killed_value_units: 1500.0
  killed_value_structures: 0.0
  collected_minerals: 9165.0
  collected_vespene: 1784.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 403.0
  spent_minerals: 8975.0
  spent_vespene: 1725.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1975.0
    economy: 4900.0
    technology: 2000.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 650.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2275.0
    economy: 5150.0
    technology: 1800.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 500.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 3097.59619141
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 948.163818359
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 542.098876953
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 240
vespene: 59
food_cap: 109
food_used: 83
food_army: 42
food_workers: 41
idle_worker_count: 0
army_count: 16
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 498
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12801
score_details {
  idle_production_time: 2571.8125
  idle_worker_time: 49.125
  total_value_units: 6500.0
  total_value_structures: 5050.0
  killed_value_units: 4350.0
  killed_value_structures: 0.0
  collected_minerals: 11415.0
  collected_vespene: 2336.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 470.0
  spent_minerals: 10525.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3050.0
    economy: 1300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2525.0
    economy: 4900.0
    technology: 2000.0
    upgrade: 475.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 650.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 0.0
    army: 3575.0
    economy: 5350.0
    technology: 2000.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 650.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 7411.12255859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2027.80639648
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1642.6887207
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 940
vespene: 311
food_cap: 125
food_used: 95
food_army: 54
food_workers: 41
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  11673
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 40
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

Score:  12801
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
