----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3871
  player_apm: 118
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 95
}
game_duration_loops: 9186
game_duration_seconds: 410.117889404
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8616
score_details {
  idle_production_time: 936.8125
  idle_worker_time: 790.875
  total_value_units: 4250.0
  total_value_structures: 3300.0
  killed_value_units: 2850.0
  killed_value_structures: 0.0
  collected_minerals: 7125.0
  collected_vespene: 1516.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 313.0
  spent_minerals: 6450.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 19.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1150.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 700.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1050.0
    economy: 3800.0
    technology: 1350.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4100.0
    technology: 1200.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 250.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 2958.80517578
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 662.0
    shields: 1289.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 725
vespene: 366
food_cap: 102
food_used: 50
food_army: 19
food_workers: 31
idle_worker_count: 14
army_count: 9
warp_gate_count: 2

game_loop:  9186
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 2
}

Score:  8616
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
