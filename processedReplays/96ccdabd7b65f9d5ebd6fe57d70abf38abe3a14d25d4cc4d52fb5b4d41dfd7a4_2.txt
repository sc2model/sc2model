----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3756
  player_apm: 141
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3774
  player_apm: 90
}
game_duration_loops: 12835
game_duration_seconds: 573.031066895
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9270
score_details {
  idle_production_time: 1714.25
  idle_worker_time: 586.125
  total_value_units: 3725.0
  total_value_structures: 3950.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 7205.0
  collected_vespene: 1464.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 627.0
  spent_minerals: 7174.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 275.0
    technology: -226.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1725.0
    economy: 3850.0
    technology: 1850.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 975.0
    economy: 4100.0
    technology: 1850.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 677.824707031
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 582.922607422
    shields: 912.077392578
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 81
vespene: 114
food_cap: 78
food_used: 73
food_army: 32
food_workers: 41
idle_worker_count: 2
army_count: 13
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 4
}
production {
  unit {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
  build_queue {
    unit_type: 83
    build_progress: 0.422066450119
  }
}

score{
 score_type: Melee
score: 6923
score_details {
  idle_production_time: 2472.6875
  idle_worker_time: 825.875
  total_value_units: 6350.0
  total_value_structures: 4050.0
  killed_value_units: 2000.0
  killed_value_structures: 0.0
  collected_minerals: 10360.0
  collected_vespene: 2712.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 268.0
  spent_minerals: 8774.0
  spent_vespene: 1850.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 7.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1600.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2775.0
    economy: 2100.0
    technology: 524.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 550.0
    economy: 1775.0
    technology: 1100.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2950.0
    economy: 4200.0
    technology: 1850.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 4006.1328125
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5629.09179688
    shields: 7276.39257812
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1636
vespene: 862
food_cap: 62
food_used: 15
food_army: 8
food_workers: 7
idle_worker_count: 7
army_count: 2
warp_gate_count: 2

game_loop:  12835
ui_data{
 
Score:  6923
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
