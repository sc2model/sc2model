----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3357
  player_apm: 104
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3425
  player_apm: 16
}
game_duration_loops: 7545
game_duration_seconds: 336.853851318
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6155
score_details {
  idle_production_time: 1246.0625
  idle_worker_time: 456.0
  total_value_units: 1200.0
  total_value_structures: 2900.0
  killed_value_units: 250.0
  killed_value_structures: 0.0
  collected_minerals: 4385.0
  collected_vespene: 920.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 358.0
  spent_minerals: 2950.0
  spent_vespene: 750.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 1750.0
    technology: 1650.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 1950.0
    technology: 1650.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 187.0
    shields: 210.0
    energy: 0.0
  }
  total_damage_taken {
    life: 456.0
    shields: 1270.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1485
vespene: 170
food_cap: 23
food_used: 28
food_army: 6
food_workers: 22
idle_worker_count: 22
army_count: 0
warp_gate_count: 0

game_loop:  7545
ui_data{
 
Score:  6155
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
