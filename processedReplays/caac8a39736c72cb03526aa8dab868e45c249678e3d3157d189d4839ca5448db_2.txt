----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4481
  player_apm: 260
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3706
  player_apm: 86
}
game_duration_loops: 3595
game_duration_seconds: 160.50227356
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3493
score_details {
  idle_production_time: 138.125
  idle_worker_time: 131.125
  total_value_units: 1250.0
  total_value_structures: 1300.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 2205.0
  collected_vespene: 288.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 335.0
  spent_minerals: 2100.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 2750.0
    technology: 300.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 2400.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22.0
    shields: 75.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 155
vespene: 288
food_cap: 46
food_used: 24
food_army: 0
food_workers: 24
idle_worker_count: 2
army_count: 0
warp_gate_count: 0

game_loop:  3595
ui_data{
 multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 995
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 306
    shields: 306
    energy: 0
    build_progress: 0.228749990463
  }
}

Score:  3493
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
