----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2440
  player_apm: 131
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2338
  player_apm: 27
}
game_duration_loops: 8607
game_duration_seconds: 384.267883301
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7470
score_details {
  idle_production_time: 1223.9375
  idle_worker_time: 343.4375
  total_value_units: 3500.0
  total_value_structures: 2200.0
  killed_value_units: 1650.0
  killed_value_structures: 550.0
  collected_minerals: 5485.0
  collected_vespene: 1660.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 358.0
  spent_minerals: 4525.0
  spent_vespene: 1325.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 1100.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 2850.0
    technology: 675.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 275.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1500.0
    economy: 3150.0
    technology: 675.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 275.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 4298.17382812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 826.918457031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 202.604492188
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1010
vespene: 335
food_cap: 62
food_used: 56
food_army: 30
food_workers: 26
idle_worker_count: 1
army_count: 14
warp_gate_count: 0

game_loop:  8607
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 48
  count: 11
}
multi {
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 99
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 1
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 55
    player_relative: 1
    health: 140
    shields: 0
    energy: 83
  }
}

Score:  7470
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
