----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4430
  player_apm: 264
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4356
  player_apm: 251
}
game_duration_loops: 24369
game_duration_seconds: 1087.97766113
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 14329
score_details {
  idle_production_time: 1217.4375
  idle_worker_time: 46.9375
  total_value_units: 7150.0
  total_value_structures: 5000.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 11390.0
  collected_vespene: 2364.0
  collection_rate_minerals: 2687.0
  collection_rate_vespene: 671.0
  spent_minerals: 10500.0
  spent_vespene: 2100.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2400.0
    economy: 5900.0
    technology: 2450.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2650.0
    economy: 5800.0
    technology: 2000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 916.729003906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 428.0
    shields: 861.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 940
vespene: 264
food_cap: 117
food_used: 109
food_army: 46
food_workers: 63
idle_worker_count: 0
army_count: 23
warp_gate_count: 7

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 9
}
groups {
  control_group_index: 5
  leader_unit_type: 67
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 467
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 189
    shields: 189
    energy: 0
    build_progress: 0.309615373611
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 167
    shields: 167
    energy: 0
    build_progress: 0.259615361691
  }
}

score{
 score_type: Melee
score: 34483
score_details {
  idle_production_time: 5760.125
  idle_worker_time: 509.1875
  total_value_units: 17800.0
  total_value_structures: 10800.0
  killed_value_units: 8725.0
  killed_value_structures: 2000.0
  collected_minerals: 28070.0
  collected_vespene: 10588.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 1007.0
  spent_minerals: 24725.0
  spent_vespene: 9075.0
  food_used {
    none: 0.0
    army: 117.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5325.0
    economy: 2550.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3450.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5800.0
    economy: 8450.0
    technology: 6300.0
    upgrade: 1850.0
  }
  used_vespene {
    none: 0.0
    army: 3975.0
    economy: 0.0
    technology: 1350.0
    upgrade: 1850.0
  }
  total_used_minerals {
    none: 0.0
    army: 9150.0
    economy: 8300.0
    technology: 4950.0
    upgrade: 1500.0
  }
  total_used_vespene {
    none: 0.0
    army: 6700.0
    economy: 0.0
    technology: 850.0
    upgrade: 1500.0
  }
  total_damage_dealt {
    life: 15298.2041016
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3416.0
    shields: 3857.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3395
vespene: 1513
food_cap: 200
food_used: 183
food_army: 117
food_workers: 66
idle_worker_count: 1
army_count: 45
warp_gate_count: 13

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 20
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 13
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 40224
score_details {
  idle_production_time: 7928.0
  idle_worker_time: 783.0
  total_value_units: 19900.0
  total_value_structures: 15400.0
  killed_value_units: 15575.0
  killed_value_structures: 2000.0
  collected_minerals: 34705.0
  collected_vespene: 14644.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 1343.0
  spent_minerals: 29525.0
  spent_vespene: 10650.0
  food_used {
    none: 0.0
    army: 90.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9900.0
    economy: 2550.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6575.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4500.0
    economy: 9200.0
    technology: 7950.0
    upgrade: 2325.0
  }
  used_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2325.0
  }
  total_used_minerals {
    none: 0.0
    army: 10500.0
    economy: 9400.0
    technology: 7950.0
    upgrade: 2175.0
  }
  total_used_vespene {
    none: 0.0
    army: 7450.0
    economy: 0.0
    technology: 1350.0
    upgrade: 2175.0
  }
  total_damage_dealt {
    life: 20826.6835938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5447.875
    shields: 7408.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5230
vespene: 3994
food_cap: 200
food_used: 154
food_army: 90
food_workers: 64
idle_worker_count: 1
army_count: 31
warp_gate_count: 13

game_loop:  24369
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 82
  count: 6
}
groups {
  control_group_index: 2
  leader_unit_type: 74
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 75
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 13
}
groups {
  control_group_index: 5
  leader_unit_type: 71
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 496
  count: 3
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  40224
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
