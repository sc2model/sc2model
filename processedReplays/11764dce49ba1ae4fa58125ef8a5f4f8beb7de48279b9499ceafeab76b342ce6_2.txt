----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2499
  player_apm: 78
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 114
}
game_duration_loops: 18725
game_duration_seconds: 835.995849609
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8459
score_details {
  idle_production_time: 8035.375
  idle_worker_time: 720.75
  total_value_units: 4550.0
  total_value_structures: 2150.0
  killed_value_units: 425.0
  killed_value_structures: 0.0
  collected_minerals: 6750.0
  collected_vespene: 1284.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 425.0
  spent_minerals: 6200.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 18.5
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 175.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1025.0
    economy: 3825.0
    technology: 1325.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 450.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1050.0
    economy: 4625.0
    technology: 1475.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 550.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 485.879394531
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 928.993164062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 600
vespene: 284
food_cap: 66
food_used: 50
food_army: 18
food_workers: 32
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 107
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16212
score_details {
  idle_production_time: 28696.5
  idle_worker_time: 2393.1875
  total_value_units: 12025.0
  total_value_structures: 3225.0
  killed_value_units: 5175.0
  killed_value_structures: 0.0
  collected_minerals: 14670.0
  collected_vespene: 4792.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 828.0
  spent_minerals: 13800.0
  spent_vespene: 4350.0
  food_used {
    none: 0.0
    army: 59.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2100.0
    economy: 850.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2800.0
    economy: 5400.0
    technology: 2025.0
    upgrade: 975.0
  }
  used_vespene {
    none: 150.0
    army: 1625.0
    economy: 0.0
    technology: 750.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 300.0
    army: 4625.0
    economy: 7300.0
    technology: 2275.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 300.0
    army: 2150.0
    economy: 0.0
    technology: 700.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 5379.54199219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4914.35205078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 920
vespene: 442
food_cap: 120
food_used: 106
food_army: 59
food_workers: 47
idle_worker_count: 1
army_count: 27
warp_gate_count: 0

game_loop:  18725
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 107
  count: 10
}
groups {
  control_group_index: 2
  leader_unit_type: 107
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 111
    player_relative: 1
    health: 90
    shields: 0
    energy: 200
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  16212
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
