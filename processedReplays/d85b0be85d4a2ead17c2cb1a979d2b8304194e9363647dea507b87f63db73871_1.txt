----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3195
  player_apm: 67
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2372
  player_apm: 107
}
game_duration_loops: 22300
game_duration_seconds: 995.605163574
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7757
score_details {
  idle_production_time: 1204.125
  idle_worker_time: 345.3125
  total_value_units: 4575.0
  total_value_structures: 2650.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 6085.0
  collected_vespene: 1672.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 223.0
  spent_minerals: 6000.0
  spent_vespene: 1475.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1600.0
    economy: 2850.0
    technology: 1250.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 200.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2200.0
    economy: 2950.0
    technology: 1100.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 100.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 785.315917969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 554.0
    shields: 764.109619141
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 135
vespene: 197
food_cap: 70
food_used: 53
food_army: 27
food_workers: 25
idle_worker_count: 1
army_count: 13
warp_gate_count: 3

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 9676
score_details {
  idle_production_time: 4741.375
  idle_worker_time: 1627.375
  total_value_units: 11700.0
  total_value_structures: 5250.0
  killed_value_units: 5700.0
  killed_value_structures: 0.0
  collected_minerals: 14535.0
  collected_vespene: 3416.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 291.0
  spent_minerals: 14075.0
  spent_vespene: 3375.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2800.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4925.0
    economy: 1700.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 3550.0
    technology: 2150.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 625.0
    economy: 0.0
    technology: 200.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 6425.0
    economy: 5250.0
    technology: 2450.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 2625.0
    economy: 0.0
    technology: 200.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 4960.62841797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5472.66308594
    shields: 6910.73535156
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 510
vespene: 41
food_cap: 110
food_used: 54
food_army: 25
food_workers: 29
idle_worker_count: 3
army_count: 12
warp_gate_count: 6

game_loop:  20000
ui_data{
 multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 7577
score_details {
  idle_production_time: 5645.3125
  idle_worker_time: 2086.0
  total_value_units: 12925.0
  total_value_structures: 6450.0
  killed_value_units: 6700.0
  killed_value_structures: 0.0
  collected_minerals: 15816.0
  collected_vespene: 4036.0
  collection_rate_minerals: 246.0
  collection_rate_vespene: 313.0
  spent_minerals: 15750.0
  spent_vespene: 3725.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 6.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3300.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6675.0
    economy: 2750.0
    technology: 750.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 375.0
    economy: 3600.0
    technology: 1700.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 200.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 7300.0
    economy: 6450.0
    technology: 2450.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 2975.0
    economy: 0.0
    technology: 200.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 6136.94091797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7968.63574219
    shields: 9772.67480469
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 116
vespene: 311
food_cap: 172
food_used: 13
food_army: 7
food_workers: 6
idle_worker_count: 6
army_count: 3
warp_gate_count: 5

game_loop:  22300
ui_data{
 
Score:  7577
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
