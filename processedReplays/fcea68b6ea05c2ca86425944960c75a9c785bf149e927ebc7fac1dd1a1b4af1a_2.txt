----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4397
  player_apm: 190
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4576
  player_apm: 151
}
game_duration_loops: 23238
game_duration_seconds: 1037.4831543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9627
score_details {
  idle_production_time: 1010.875
  idle_worker_time: 255.4375
  total_value_units: 6150.0
  total_value_structures: 3475.0
  killed_value_units: 1975.0
  killed_value_structures: 0.0
  collected_minerals: 8805.0
  collected_vespene: 2640.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 671.0
  spent_minerals: 8450.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 959.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1675.0
    economy: 4050.0
    technology: 1325.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 350.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2275.0
    economy: 5000.0
    technology: 1325.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1595.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2368.88769531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 808.887695312
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 194
vespene: 508
food_cap: 94
food_used: 71
food_army: 32
food_workers: 39
idle_worker_count: 2
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 76
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 25
  }
}

score{
 score_type: Melee
score: 23703
score_details {
  idle_production_time: 5937.0625
  idle_worker_time: 2377.6875
  total_value_units: 17175.0
  total_value_structures: 7000.0
  killed_value_units: 8975.0
  killed_value_structures: 750.0
  collected_minerals: 24185.0
  collected_vespene: 8356.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 895.0
  spent_minerals: 22375.0
  spent_vespene: 6250.0
  food_used {
    none: 0.0
    army: 100.0
    economy: 70.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6150.0
    economy: 1500.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5693.0
    economy: 700.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2254.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4975.0
    economy: 7700.0
    technology: 2700.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 0.0
    army: 1775.0
    economy: 300.0
    technology: 600.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 0.0
    army: 9575.0
    economy: 9000.0
    technology: 2800.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 0.0
    army: 3400.0
    economy: 600.0
    technology: 600.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 10878.6425781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11517.5644531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5921.38916016
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1302
vespene: 1851
food_cap: 180
food_used: 170
food_army: 100
food_workers: 70
idle_worker_count: 8
army_count: 65
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26104
score_details {
  idle_production_time: 7873.375
  idle_worker_time: 4144.75
  total_value_units: 22100.0
  total_value_structures: 7600.0
  killed_value_units: 14725.0
  killed_value_structures: 3150.0
  collected_minerals: 29015.0
  collected_vespene: 10548.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 806.0
  spent_minerals: 25725.0
  spent_vespene: 7425.0
  food_used {
    none: 0.0
    army: 77.0
    economy: 70.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9500.0
    economy: 5000.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2825.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9843.0
    economy: 700.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3429.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3825.0
    economy: 8300.0
    technology: 2700.0
    upgrade: 1500.0
  }
  used_vespene {
    none: 0.0
    army: 1775.0
    economy: 300.0
    technology: 600.0
    upgrade: 1500.0
  }
  total_used_minerals {
    none: 0.0
    army: 13125.0
    economy: 9600.0
    technology: 2800.0
    upgrade: 1350.0
  }
  total_used_vespene {
    none: 0.0
    army: 4775.0
    economy: 600.0
    technology: 600.0
    upgrade: 1350.0
  }
  total_damage_dealt {
    life: 25815.3125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 16625.1445312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 8749.29394531
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2748
vespene: 2856
food_cap: 200
food_used: 147
food_army: 77
food_workers: 70
idle_worker_count: 11
army_count: 49
warp_gate_count: 0

game_loop:  23238
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 4
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
}

Score:  26104
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
