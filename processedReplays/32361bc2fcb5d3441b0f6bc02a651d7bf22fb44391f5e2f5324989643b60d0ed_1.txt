----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3445
  player_apm: 183
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 89
}
game_duration_loops: 23818
game_duration_seconds: 1063.37780762
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11878
score_details {
  idle_production_time: 3807.6875
  idle_worker_time: 28.625
  total_value_units: 6750.0
  total_value_structures: 2250.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8540.0
  collected_vespene: 2644.0
  collection_rate_minerals: 1259.0
  collection_rate_vespene: 671.0
  spent_minerals: 8556.0
  spent_vespene: 2400.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 281.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2600.0
    economy: 4500.0
    technology: 1650.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 600.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2325.0
    economy: 5050.0
    technology: 1800.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 700.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 5.0
    energy: 0.0
  }
  total_damage_taken {
    life: 736.164550781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 34
vespene: 244
food_cap: 108
food_used: 100
food_army: 65
food_workers: 35
idle_worker_count: 0
army_count: 31
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 70
  }
}

score{
 score_type: Melee
score: 17609
score_details {
  idle_production_time: 15164.3125
  idle_worker_time: 266.25
  total_value_units: 17150.0
  total_value_structures: 4325.0
  killed_value_units: 9850.0
  killed_value_structures: 1000.0
  collected_minerals: 19800.0
  collected_vespene: 7340.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 470.0
  spent_minerals: 19681.0
  spent_vespene: 6500.0
  food_used {
    none: 0.0
    army: 49.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6850.0
    economy: 700.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6975.0
    economy: 556.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2800.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 25.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2425.0
    economy: 5675.0
    technology: 3325.0
    upgrade: 1575.0
  }
  used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1575.0
  }
  total_used_minerals {
    none: 0.0
    army: 9625.0
    economy: 7700.0
    technology: 3775.0
    upgrade: 1225.0
  }
  total_used_vespene {
    none: 0.0
    army: 4875.0
    economy: 0.0
    technology: 1400.0
    upgrade: 1225.0
  }
  total_damage_dealt {
    life: 6850.0
    shields: 6605.875
    energy: 0.0
  }
  total_damage_taken {
    life: 14722.6865234
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 169
vespene: 840
food_cap: 160
food_used: 93
food_army: 49
food_workers: 43
idle_worker_count: 3
army_count: 13
warp_gate_count: 0
larva_count: 9

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 107
  count: 12
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1410
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19194
score_details {
  idle_production_time: 22418.1875
  idle_worker_time: 2545.5
  total_value_units: 22800.0
  total_value_structures: 4475.0
  killed_value_units: 17500.0
  killed_value_structures: 1400.0
  collected_minerals: 24925.0
  collected_vespene: 8000.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 0.0
  spent_minerals: 23706.0
  spent_vespene: 8000.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11375.0
    economy: 1950.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9825.0
    economy: 556.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4150.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 25.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2575.0
    economy: 6450.0
    technology: 3575.0
    upgrade: 1575.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 1050.0
    upgrade: 1575.0
  }
  total_used_minerals {
    none: 0.0
    army: 12825.0
    economy: 8550.0
    technology: 4025.0
    upgrade: 1575.0
  }
  total_used_vespene {
    none: 0.0
    army: 6475.0
    economy: 0.0
    technology: 1400.0
    upgrade: 1575.0
  }
  total_damage_dealt {
    life: 11553.875
    shields: 11243.625
    energy: 0.0
  }
  total_damage_taken {
    life: 17710.3164062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1269
vespene: 0
food_cap: 160
food_used: 108
food_army: 53
food_workers: 55
idle_worker_count: 16
army_count: 18
warp_gate_count: 0

game_loop:  23818
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 107
  count: 18
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 74
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 64
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 56
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  19194
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
