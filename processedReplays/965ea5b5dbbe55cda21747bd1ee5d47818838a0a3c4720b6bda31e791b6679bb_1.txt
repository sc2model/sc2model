----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3802
  player_apm: 289
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3705
  player_apm: 142
}
game_duration_loops: 18340
game_duration_seconds: 818.807128906
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12007
score_details {
  idle_production_time: 1348.0
  idle_worker_time: 584.0
  total_value_units: 5175.0
  total_value_structures: 3900.0
  killed_value_units: 2375.0
  killed_value_structures: 0.0
  collected_minerals: 9780.0
  collected_vespene: 2152.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 649.0
  spent_minerals: 9375.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1600.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 650.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 1825.0
    economy: 5175.0
    technology: 2150.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 650.0
    economy: 0.0
    technology: 600.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2075.0
    economy: 5350.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 650.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 3539.74121094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 995.299316406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 364.736328125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 455
vespene: 252
food_cap: 85
food_used: 85
food_army: 36
food_workers: 47
idle_worker_count: 1
army_count: 24
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 111
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 69
  }
}

score{
 score_type: Melee
score: 29440
score_details {
  idle_production_time: 7888.0
  idle_worker_time: 1266.6875
  total_value_units: 16700.0
  total_value_structures: 8625.0
  killed_value_units: 15125.0
  killed_value_structures: 1075.0
  collected_minerals: 25105.0
  collected_vespene: 8860.0
  collection_rate_minerals: 2435.0
  collection_rate_vespene: 1164.0
  spent_minerals: 21200.0
  spent_vespene: 5075.0
  food_used {
    none: 0.0
    army: 111.0
    economy: 70.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8900.0
    economy: 3175.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3975.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 5750.0
    economy: 8400.0
    technology: 3050.0
    upgrade: 675.0
  }
  used_vespene {
    none: 100.0
    army: 1875.0
    economy: 0.0
    technology: 1075.0
    upgrade: 675.0
  }
  total_used_minerals {
    none: 100.0
    army: 9825.0
    economy: 9150.0
    technology: 3050.0
    upgrade: 675.0
  }
  total_used_vespene {
    none: 100.0
    army: 3225.0
    economy: 0.0
    technology: 1075.0
    upgrade: 675.0
  }
  total_damage_dealt {
    life: 26056.3125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6217.63525391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3975.24560547
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3955
vespene: 3785
food_cap: 200
food_used: 181
food_army: 111
food_workers: 70
idle_worker_count: 4
army_count: 86
warp_gate_count: 0

game_loop:  18340
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 52
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 5
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 28
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 31
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 115
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 54
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 71
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 79
    shields: 0
    energy: 19
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 5
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 20
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 79
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 80
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 18
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  29440
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
