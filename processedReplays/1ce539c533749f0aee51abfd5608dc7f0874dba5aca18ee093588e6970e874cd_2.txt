----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4635
  player_apm: 174
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 270
}
game_duration_loops: 17783
game_duration_seconds: 793.939331055
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11265
score_details {
  idle_production_time: 1314.0625
  idle_worker_time: 97.6875
  total_value_units: 5200.0
  total_value_structures: 3925.0
  killed_value_units: 200.0
  killed_value_structures: 100.0
  collected_minerals: 8985.0
  collected_vespene: 2280.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 470.0
  spent_minerals: 8000.0
  spent_vespene: 1925.0
  food_used {
    none: 0.0
    army: 32.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1700.0
    economy: 4675.0
    technology: 1525.0
    upgrade: 375.0
  }
  used_vespene {
    none: 50.0
    army: 525.0
    economy: 0.0
    technology: 500.0
    upgrade: 375.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4650.0
    technology: 1525.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 500.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 805.25
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1395.875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1035
vespene: 355
food_cap: 94
food_used: 75
food_army: 32
food_workers: 43
idle_worker_count: 1
army_count: 19
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 15
}
groups {
  control_group_index: 2
  leader_unit_type: 35
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 22
  count: 2
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 71
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21359
score_details {
  idle_production_time: 4518.0625
  idle_worker_time: 455.625
  total_value_units: 15650.0
  total_value_structures: 6400.0
  killed_value_units: 9700.0
  killed_value_structures: 1525.0
  collected_minerals: 21895.0
  collected_vespene: 6064.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 671.0
  spent_minerals: 19625.0
  spent_vespene: 5025.0
  food_used {
    none: 0.0
    army: 100.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6950.0
    economy: 2600.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4700.0
    economy: 475.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5200.0
    economy: 6075.0
    technology: 1750.0
    upgrade: 1275.0
  }
  used_vespene {
    none: 50.0
    army: 1775.0
    economy: 0.0
    technology: 550.0
    upgrade: 1275.0
  }
  total_used_minerals {
    none: 50.0
    army: 9900.0
    economy: 7300.0
    technology: 2050.0
    upgrade: 1100.0
  }
  total_used_vespene {
    none: 50.0
    army: 3000.0
    economy: 0.0
    technology: 600.0
    upgrade: 1100.0
  }
  total_damage_dealt {
    life: 14881.0107422
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10724.8027344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2597.06542969
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2320
vespene: 1039
food_cap: 197
food_used: 143
food_army: 100
food_workers: 43
idle_worker_count: 2
army_count: 72
warp_gate_count: 0

game_loop:  17783
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 27
}
groups {
  control_group_index: 2
  leader_unit_type: 54
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 7
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 22
  count: 3
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 51
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 51
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 45
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 87
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  21359
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
