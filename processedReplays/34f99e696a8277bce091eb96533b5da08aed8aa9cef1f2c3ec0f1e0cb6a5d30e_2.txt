----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3558
  player_apm: 131
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3542
  player_apm: 131
}
game_duration_loops: 11894
game_duration_seconds: 531.019226074
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11620
score_details {
  idle_production_time: 1286.0
  idle_worker_time: 288.75
  total_value_units: 5375.0
  total_value_structures: 4000.0
  killed_value_units: 0.0
  killed_value_structures: 425.0
  collected_minerals: 8520.0
  collected_vespene: 2200.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 649.0
  spent_minerals: 8375.0
  spent_vespene: 1750.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2175.0
    economy: 4750.0
    technology: 2050.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 450.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1925.0
    economy: 4900.0
    technology: 1250.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2690.23535156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 220.0
    shields: 396.375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 195
vespene: 450
food_cap: 109
food_used: 85
food_army: 36
food_workers: 49
idle_worker_count: 0
army_count: 14
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 81
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 10677
score_details {
  idle_production_time: 2125.0625
  idle_worker_time: 288.8125
  total_value_units: 7425.0
  total_value_structures: 5600.0
  killed_value_units: 1750.0
  killed_value_structures: 425.0
  collected_minerals: 11540.0
  collected_vespene: 3112.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 627.0
  spent_minerals: 10275.0
  spent_vespene: 2250.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1325.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2625.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 4450.0
    technology: 2350.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 650.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 3575.0
    economy: 5450.0
    technology: 2350.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 650.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 5598.23095703
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3507.25
    shields: 4148.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1315
vespene: 862
food_cap: 94
food_used: 57
food_army: 6
food_workers: 51
idle_worker_count: 0
army_count: 2
warp_gate_count: 8

game_loop:  11894
ui_data{
 single {
  unit {
    unit_type: 68
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  10677
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
