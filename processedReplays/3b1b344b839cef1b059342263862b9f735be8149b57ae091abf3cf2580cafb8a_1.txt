----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3418
  player_apm: 120
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3450
  player_apm: 138
}
game_duration_loops: 21957
game_duration_seconds: 980.291625977
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10383
score_details {
  idle_production_time: 1425.875
  idle_worker_time: 86.8125
  total_value_units: 5300.0
  total_value_structures: 3850.0
  killed_value_units: 900.0
  killed_value_structures: 0.0
  collected_minerals: 8100.0
  collected_vespene: 2108.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 582.0
  spent_minerals: 7875.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 425.0
    economy: 25.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1475.0
    economy: 4550.0
    technology: 1950.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 150.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1750.0
    economy: 4250.0
    technology: 1800.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 689.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 457.119140625
    shields: 864.219238281
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 275
vespene: 458
food_cap: 94
food_used: 83
food_army: 36
food_workers: 45
idle_worker_count: 0
army_count: 16
warp_gate_count: 5

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 77
  count: 13
}
groups {
  control_group_index: 3
  leader_unit_type: 67
  count: 1
}
single {
  unit {
    unit_type: 495
    player_relative: 1
    health: 100
    shields: 60
    energy: 97
  }
}

score{
 score_type: Melee
score: 25524
score_details {
  idle_production_time: 4559.125
  idle_worker_time: 3823.125
  total_value_units: 14175.0
  total_value_structures: 8675.0
  killed_value_units: 1850.0
  killed_value_structures: 0.0
  collected_minerals: 19960.0
  collected_vespene: 6764.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 425.0
  spent_minerals: 19475.0
  spent_vespene: 5650.0
  food_used {
    none: 0.0
    army: 133.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 975.0
    economy: 25.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6150.0
    economy: 7725.0
    technology: 4200.0
    upgrade: 900.0
  }
  used_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 650.0
    upgrade: 900.0
  }
  total_used_minerals {
    none: 0.0
    army: 6925.0
    economy: 7125.0
    technology: 4200.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 3950.0
    economy: 0.0
    technology: 650.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 1739.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1120.11914062
    shields: 1659.21923828
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 535
vespene: 1114
food_cap: 200
food_used: 197
food_army: 133
food_workers: 64
idle_worker_count: 4
army_count: 55
warp_gate_count: 11

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 77
  count: 15
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 2
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 85
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 200
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 15
    shields: 40
    energy: 200
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 15
    shields: 40
    energy: 200
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 15
    shields: 40
    energy: 200
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 15
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 200
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 85
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 495
    player_relative: 1
    health: 40
    shields: 60
    energy: 74
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 113
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 113
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 113
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 113
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 113
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 113
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 112
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 112
  }
}

score{
 score_type: Melee
score: 14649
score_details {
  idle_production_time: 5515.4375
  idle_worker_time: 4340.375
  total_value_units: 17100.0
  total_value_structures: 9475.0
  killed_value_units: 6550.0
  killed_value_structures: 0.0
  collected_minerals: 22485.0
  collected_vespene: 7364.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 447.0
  spent_minerals: 22250.0
  spent_vespene: 6600.0
  food_used {
    none: 0.0
    army: 12.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4525.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9300.0
    economy: 1950.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 600.0
    economy: 5800.0
    technology: 4050.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 650.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 9300.0
    economy: 7925.0
    technology: 4200.0
    upgrade: 900.0
  }
  total_used_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 650.0
    upgrade: 900.0
  }
  total_damage_dealt {
    life: 6255.59912109
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 9661.11914062
    shields: 9348.96875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 285
vespene: 764
food_cap: 200
food_used: 47
food_army: 12
food_workers: 35
idle_worker_count: 35
army_count: 0
warp_gate_count: 10

game_loop:  21957
ui_data{
 
Score:  14649
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
