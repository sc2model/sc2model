----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3218
  player_apm: 99
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3299
  player_apm: 62
}
game_duration_loops: 4348
game_duration_seconds: 194.120681763
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 3049
score_details {
  idle_production_time: 162.75
  idle_worker_time: 33.25
  total_value_units: 1150.0
  total_value_structures: 825.0
  killed_value_units: 925.0
  killed_value_structures: 0.0
  collected_minerals: 2285.0
  collected_vespene: 188.0
  collection_rate_minerals: 503.0
  collection_rate_vespene: 89.0
  spent_minerals: 1525.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 3.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 175.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: -50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 150.0
    economy: 1575.0
    technology: 300.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 100.0
    economy: 1975.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 913.344238281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 920.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 370.25390625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 786
vespene: 38
food_cap: 31
food_used: 18
food_army: 3
food_workers: 14
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  4348
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 1
}
single {
  unit {
    unit_type: 49
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
}

Score:  3049
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
