----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3754
  player_apm: 125
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3774
  player_apm: 104
}
game_duration_loops: 10392
game_duration_seconds: 463.9609375
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11486
score_details {
  idle_production_time: 6873.3125
  idle_worker_time: 0.0
  total_value_units: 5950.0
  total_value_structures: 2375.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 9415.0
  collected_vespene: 1296.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 335.0
  spent_minerals: 7925.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 28.5
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1500.0
    economy: 4850.0
    technology: 1825.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 350.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 1525.0
    economy: 5700.0
    technology: 1975.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 450.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 802.084472656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 498.4140625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1540
vespene: 146
food_cap: 106
food_used: 76
food_army: 28
food_workers: 48
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 90
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 106
  count: 11
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 77
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 90
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 82
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 124
    shields: 0
    energy: 106
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 66
  }
}

score{
 score_type: Melee
score: 11053
score_details {
  idle_production_time: 7201.625
  idle_worker_time: 0.0
  total_value_units: 6350.0
  total_value_structures: 2375.0
  killed_value_units: 700.0
  killed_value_structures: 0.0
  collected_minerals: 10065.0
  collected_vespene: 1388.0
  collection_rate_minerals: 2351.0
  collection_rate_vespene: 313.0
  spent_minerals: 8675.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: -125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1175.0
    economy: 4850.0
    technology: 1825.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 350.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 1825.0
    economy: 5700.0
    technology: 1975.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 450.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 1720.70410156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2030.45019531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1440
vespene: 38
food_cap: 106
food_used: 75
food_army: 27
food_workers: 48
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  10392
ui_data{
 multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 104
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 91
    shields: 0
    energy: 96
  }
}

Score:  11053
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
