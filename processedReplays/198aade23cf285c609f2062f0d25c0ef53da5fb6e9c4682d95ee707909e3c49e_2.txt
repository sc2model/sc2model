----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2858
  player_apm: 97
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2865
  player_apm: 137
}
game_duration_loops: 17084
game_duration_seconds: 762.731811523
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12988
score_details {
  idle_production_time: 7937.625
  idle_worker_time: 427.75
  total_value_units: 7450.0
  total_value_structures: 1725.0
  killed_value_units: 600.0
  killed_value_structures: 0.0
  collected_minerals: 10200.0
  collected_vespene: 2088.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 627.0
  spent_minerals: 9475.0
  spent_vespene: 1600.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2175.0
    economy: 5750.0
    technology: 1375.0
    upgrade: 825.0
  }
  used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 350.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 0.0
    army: 2325.0
    economy: 6100.0
    technology: 1275.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 200.0
    shields: 130.0
    energy: 0.0
  }
  total_damage_taken {
    life: 694.741210938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 775
vespene: 488
food_cap: 122
food_used: 103
food_army: 48
food_workers: 52
idle_worker_count: 2
army_count: 27
warp_gate_count: 0
larva_count: 4

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20313
score_details {
  idle_production_time: 16246.8125
  idle_worker_time: 963.5
  total_value_units: 17500.0
  total_value_structures: 2625.0
  killed_value_units: 14875.0
  killed_value_structures: 200.0
  collected_minerals: 21355.0
  collected_vespene: 5308.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 627.0
  spent_minerals: 17100.0
  spent_vespene: 4625.0
  food_used {
    none: 0.0
    army: 96.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 10775.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4950.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3900.0
    economy: 6200.0
    technology: 1625.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 350.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 8700.0
    economy: 7500.0
    technology: 1775.0
    upgrade: 925.0
  }
  total_used_vespene {
    none: 0.0
    army: 3150.0
    economy: 0.0
    technology: 450.0
    upgrade: 925.0
  }
  total_damage_dealt {
    life: 8114.52783203
    shields: 9433.27148438
    energy: 0.0
  }
  total_damage_taken {
    life: 10581.6445312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 4305
vespene: 683
food_cap: 152
food_used: 151
food_army: 96
food_workers: 55
idle_worker_count: 1
army_count: 42
warp_gate_count: 0

game_loop:  17084
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  20313
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
