----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3549
  player_apm: 159
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 89
}
game_duration_loops: 26906
game_duration_seconds: 1201.24450684
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12932
score_details {
  idle_production_time: 1355.5
  idle_worker_time: 207.6875
  total_value_units: 6100.0
  total_value_structures: 4525.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9770.0
  collected_vespene: 2112.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 649.0
  spent_minerals: 9625.0
  spent_vespene: 1600.0
  food_used {
    none: 0.0
    army: 53.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 2650.0
    economy: 5750.0
    technology: 1925.0
    upgrade: 200.0
  }
  used_vespene {
    none: 100.0
    army: 750.0
    economy: 0.0
    technology: 550.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 2600.0
    economy: 5550.0
    technology: 1775.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 750.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 165.0
    shields: 269.0
    energy: 0.0
  }
  total_damage_taken {
    life: 220.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 195
vespene: 512
food_cap: 118
food_used: 108
food_army: 53
food_workers: 55
idle_worker_count: 2
army_count: 38
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 49
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 689
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 37
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 22
  count: 1
}
multi {
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 183
  }
  units {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 48
  }
}

score{
 score_type: Melee
score: 22549
score_details {
  idle_production_time: 9258.0625
  idle_worker_time: 996.1875
  total_value_units: 19225.0
  total_value_structures: 8525.0
  killed_value_units: 9630.0
  killed_value_structures: 1250.0
  collected_minerals: 24980.0
  collected_vespene: 8344.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 828.0
  spent_minerals: 24925.0
  spent_vespene: 5975.0
  food_used {
    none: 0.0
    army: 62.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6680.0
    economy: 1100.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3300.0
    economy: 7625.0
    technology: 3850.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 150.0
    army: 1450.0
    economy: 300.0
    technology: 1250.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 150.0
    army: 12800.0
    economy: 8150.0
    technology: 3550.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 150.0
    army: 3125.0
    economy: 600.0
    technology: 1025.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 8932.0
    shields: 8848.04589844
    energy: 0.0
  }
  total_damage_taken {
    life: 9819.5
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 105
vespene: 2369
food_cap: 188
food_used: 128
food_army: 62
food_workers: 66
idle_worker_count: 1
army_count: 29
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 15
}
groups {
  control_group_index: 3
  leader_unit_type: 48
  count: 27
}
groups {
  control_group_index: 7
  leader_unit_type: 37
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 22
  count: 2
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 23384
score_details {
  idle_production_time: 16598.75
  idle_worker_time: 2064.6875
  total_value_units: 27425.0
  total_value_structures: 12900.0
  killed_value_units: 11100.0
  killed_value_structures: 1250.0
  collected_minerals: 35730.0
  collected_vespene: 11692.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 425.0
  spent_minerals: 35150.0
  spent_vespene: 8850.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7875.0
    economy: 1100.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3075.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 18157.0
    economy: 200.0
    technology: 1275.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 4356.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1150.0
    economy: 8225.0
    technology: 4950.0
    upgrade: 1250.0
  }
  used_vespene {
    none: 200.0
    army: 650.0
    economy: 450.0
    technology: 1600.0
    upgrade: 1250.0
  }
  total_used_minerals {
    none: 200.0
    army: 19150.0
    economy: 9275.0
    technology: 6375.0
    upgrade: 1000.0
  }
  total_used_vespene {
    none: 200.0
    army: 4925.0
    economy: 900.0
    technology: 1700.0
    upgrade: 1000.0
  }
  total_damage_dealt {
    life: 10683.875
    shields: 12754.2958984
    energy: 0.0
  }
  total_damage_taken {
    life: 25790.5
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 32.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 623
vespene: 2836
food_cap: 200
food_used: 88
food_army: 21
food_workers: 67
idle_worker_count: 67
army_count: 6
warp_gate_count: 0

game_loop:  26906
ui_data{
 
Score:  23384
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
