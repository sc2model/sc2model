----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2917
  player_apm: 62
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3032
  player_apm: 62
}
game_duration_loops: 10793
game_duration_seconds: 481.863983154
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8322
score_details {
  idle_production_time: 1645.4375
  idle_worker_time: 576.9375
  total_value_units: 3250.0
  total_value_structures: 2925.0
  killed_value_units: 1325.0
  killed_value_structures: 0.0
  collected_minerals: 6410.0
  collected_vespene: 1520.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 156.0
  spent_minerals: 6075.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 3450.0
    technology: 1375.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1150.0
    economy: 3800.0
    technology: 1075.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 400.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 846.0
    shields: 825.875
    energy: 0.0
  }
  total_damage_taken {
    life: 2091.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1686.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 277
vespene: 220
food_cap: 70
food_used: 58
food_army: 26
food_workers: 32
idle_worker_count: 1
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 57
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
single {
  unit {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9128
score_details {
  idle_production_time: 2257.125
  idle_worker_time: 661.125
  total_value_units: 3950.0
  total_value_structures: 3325.0
  killed_value_units: 1675.0
  killed_value_structures: 0.0
  collected_minerals: 7020.0
  collected_vespene: 1616.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 156.0
  spent_minerals: 6075.0
  spent_vespene: 1300.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 3550.0
    technology: 1375.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 400.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 3900.0
    technology: 1375.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 400.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 986.0
    shields: 989.875
    energy: 0.0
  }
  total_damage_taken {
    life: 2091.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1686.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 887
vespene: 316
food_cap: 78
food_used: 58
food_army: 26
food_workers: 32
idle_worker_count: 2
army_count: 10
warp_gate_count: 0

game_loop:  10793
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 57
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
  units {
    unit_type: 57
    player_relative: 1
    health: 550
    shields: 0
    energy: 0
  }
}

Score:  9128
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
