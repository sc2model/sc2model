----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2973
  player_apm: 107
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2613
  player_apm: 67
}
game_duration_loops: 6018
game_duration_seconds: 268.679473877
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5256
score_details {
  idle_production_time: 614.25
  idle_worker_time: 129.875
  total_value_units: 1200.0
  total_value_structures: 2800.0
  killed_value_units: 325.0
  killed_value_structures: 625.0
  collected_minerals: 3640.0
  collected_vespene: 716.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 335.0
  spent_minerals: 3400.0
  spent_vespene: 250.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 625.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 100.0
    economy: 2200.0
    technology: 1950.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 2350.0
    technology: 1650.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2128.0
    shields: 2405.25
    energy: 0.0
  }
  total_damage_taken {
    life: 362.855957031
    shields: 646.144042969
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 290
vespene: 466
food_cap: 55
food_used: 25
food_army: 2
food_workers: 23
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  6018
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}

Score:  5256
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
