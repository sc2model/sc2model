----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2984
  player_apm: 61
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2559
  player_apm: 53
}
game_duration_loops: 24056
game_duration_seconds: 1074.00354004
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10561
score_details {
  idle_production_time: 2338.5
  idle_worker_time: 1297.0625
  total_value_units: 3250.0
  total_value_structures: 4775.0
  killed_value_units: 325.0
  killed_value_structures: 0.0
  collected_minerals: 7795.0
  collected_vespene: 1616.0
  collection_rate_minerals: 2519.0
  collection_rate_vespene: 313.0
  spent_minerals: 7800.0
  spent_vespene: 1050.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 400.0
    army: 1250.0
    economy: 4775.0
    technology: 2325.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 400.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1250.0
    economy: 5150.0
    technology: 1925.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 400.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 547.726074219
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 45
vespene: 566
food_cap: 99
food_used: 55
food_army: 25
food_workers: 30
idle_worker_count: 3
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 5
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 30607
score_details {
  idle_production_time: 14970.1875
  idle_worker_time: 4716.6875
  total_value_units: 10425.0
  total_value_structures: 13625.0
  killed_value_units: 6700.0
  killed_value_structures: 350.0
  collected_minerals: 22110.0
  collected_vespene: 7672.0
  collection_rate_minerals: 3163.0
  collection_rate_vespene: 806.0
  spent_minerals: 20975.0
  spent_vespene: 4525.0
  food_used {
    none: 0.0
    army: 120.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3950.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: -100.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 1000.0
    army: 5925.0
    economy: 7900.0
    technology: 6325.0
    upgrade: 600.0
  }
  used_vespene {
    none: 250.0
    army: 1300.0
    economy: 0.0
    technology: 2375.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 250.0
    army: 6275.0
    economy: 8575.0
    technology: 6525.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 250.0
    army: 1300.0
    economy: 0.0
    technology: 2375.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 6828.51757812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 973.937744141
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1185
vespene: 3147
food_cap: 200
food_used: 166
food_army: 120
food_workers: 45
idle_worker_count: 12
army_count: 62
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 26
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 4
}
multi {
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 70
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 85
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 35488
score_details {
  idle_production_time: 22481.5
  idle_worker_time: 8153.5
  total_value_units: 14975.0
  total_value_structures: 15050.0
  killed_value_units: 13175.0
  killed_value_structures: 425.0
  collected_minerals: 27775.0
  collected_vespene: 9988.0
  collection_rate_minerals: 2855.0
  collection_rate_vespene: 761.0
  spent_minerals: 25525.0
  spent_vespene: 7225.0
  food_used {
    none: 0.0
    army: 141.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6950.0
    economy: 2125.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1750.0
    economy: -100.0
    technology: 225.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 600.0
    economy: 400.0
    technology: 300.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  used_minerals {
    none: 600.0
    army: 6975.0
    economy: 8100.0
    technology: 6875.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 250.0
    army: 2650.0
    economy: 0.0
    technology: 2875.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 250.0
    army: 8775.0
    economy: 9200.0
    technology: 7575.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 250.0
    army: 2500.0
    economy: 0.0
    technology: 3275.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 15697.4736328
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3440.66992188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2300
vespene: 2763
food_cap: 200
food_used: 191
food_army: 141
food_workers: 50
idle_worker_count: 15
army_count: 63
warp_gate_count: 0

game_loop:  24056
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 8
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 26
  count: 3
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 4
}
single {
  unit {
    unit_type: 50
    player_relative: 1
    health: 100
    shields: 0
    energy: 21
  }
}

Score:  35488
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
