----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2895
  player_apm: 106
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3434
  player_apm: 120
}
game_duration_loops: 28594
game_duration_seconds: 1276.60693359
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6768
score_details {
  idle_production_time: 9794.5
  idle_worker_time: 89.0625
  total_value_units: 4800.0
  total_value_structures: 1675.0
  killed_value_units: 1500.0
  killed_value_structures: 75.0
  collected_minerals: 6490.0
  collected_vespene: 928.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 156.0
  spent_minerals: 6150.0
  spent_vespene: 850.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1000.0
    economy: 575.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1025.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 475.0
    economy: 3100.0
    technology: 1900.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 3750.0
    technology: 1675.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2771.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1934.72607422
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 390
vespene: 78
food_cap: 44
food_used: 32
food_army: 6
food_workers: 26
idle_worker_count: 0
army_count: 4
warp_gate_count: 0
larva_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16390
score_details {
  idle_production_time: 19153.75
  idle_worker_time: 540.0
  total_value_units: 15900.0
  total_value_structures: 3950.0
  killed_value_units: 11525.0
  killed_value_structures: 550.0
  collected_minerals: 18130.0
  collected_vespene: 6060.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 739.0
  spent_minerals: 16200.0
  spent_vespene: 5550.0
  food_used {
    none: 0.0
    army: 51.5
    economy: 45.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7100.0
    economy: 2850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3350.0
    economy: 1575.0
    technology: 900.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2750.0
    economy: 5425.0
    technology: 2500.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 2375.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 5750.0
    economy: 8450.0
    technology: 3550.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 4000.0
    economy: 0.0
    technology: 550.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 12605.9863281
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11570.3261719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1980
vespene: 510
food_cap: 146
food_used: 96
food_army: 51
food_workers: 42
idle_worker_count: 0
army_count: 17
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 14
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21308
score_details {
  idle_production_time: 35841.125
  idle_worker_time: 2027.8125
  total_value_units: 28450.0
  total_value_structures: 4275.0
  killed_value_units: 20025.0
  killed_value_structures: 1450.0
  collected_minerals: 28220.0
  collected_vespene: 11088.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 335.0
  spent_minerals: 25425.0
  spent_vespene: 10650.0
  food_used {
    none: 0.0
    army: 61.5
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 12825.0
    economy: 4250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 4300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9100.0
    economy: 1825.0
    technology: 900.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4450.0
    economy: 6450.0
    technology: 2800.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 2775.0
    economy: 0.0
    technology: 750.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 15900.0
    economy: 9975.0
    technology: 4150.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 10850.0
    economy: 0.0
    technology: 1150.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 22190.453125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22248.1074219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2845
vespene: 438
food_cap: 192
food_used: 108
food_army: 61
food_workers: 47
idle_worker_count: 15
army_count: 34
warp_gate_count: 0

game_loop:  28594
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 114
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 112
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 9
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 105
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 114
  count: 12
}
multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  21308
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
