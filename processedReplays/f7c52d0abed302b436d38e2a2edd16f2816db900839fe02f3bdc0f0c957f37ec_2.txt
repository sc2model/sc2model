----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3400
  player_apm: 226
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3587
  player_apm: 176
}
game_duration_loops: 24381
game_duration_seconds: 1088.51342773
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12353
score_details {
  idle_production_time: 1168.25
  idle_worker_time: 313.0
  total_value_units: 5650.0
  total_value_structures: 4250.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9495.0
  collected_vespene: 2216.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 537.0
  spent_minerals: 8825.0
  spent_vespene: 1675.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2725.0
    economy: 5000.0
    technology: 1450.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 450.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 2725.0
    economy: 5300.0
    technology: 1300.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 987.290527344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 966.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 134.456054688
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 712
vespene: 541
food_cap: 125
food_used: 101
food_army: 57
food_workers: 44
idle_worker_count: 1
army_count: 41
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 49
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 48
  count: 11
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13886
score_details {
  idle_production_time: 7529.4375
  idle_worker_time: 4639.625
  total_value_units: 16000.0
  total_value_structures: 6700.0
  killed_value_units: 10525.0
  killed_value_structures: 1850.0
  collected_minerals: 20865.0
  collected_vespene: 5804.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 67.0
  spent_minerals: 19900.0
  spent_vespene: 4300.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6100.0
    economy: 2675.0
    technology: 625.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2875.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8600.0
    economy: 1775.0
    technology: 700.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4125.0
    technology: 2850.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 750.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 10350.0
    economy: 6350.0
    technology: 3450.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 750.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 23707.2929688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 17557.0195312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3642.31054688
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1007
vespene: 1504
food_cap: 117
food_used: 54
food_army: 27
food_workers: 27
idle_worker_count: 11
army_count: 12
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 52
  count: 3
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
single {
  unit {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 10800
score_details {
  idle_production_time: 11186.4375
  idle_worker_time: 5850.8125
  total_value_units: 18300.0
  total_value_structures: 6700.0
  killed_value_units: 14800.0
  killed_value_structures: 1850.0
  collected_minerals: 23095.0
  collected_vespene: 5964.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 21750.0
  spent_vespene: 4600.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9525.0
    economy: 2775.0
    technology: 625.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3625.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11145.0
    economy: 2925.0
    technology: 1000.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2831.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 550.0
    economy: 650.0
    technology: 100.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 2575.0
    technology: 2550.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 750.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 12100.0
    economy: 6600.0
    technology: 3450.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 3000.0
    economy: 0.0
    technology: 750.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 29767.3242188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24371.3867188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5302.55566406
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1342
vespene: 1333
food_cap: 78
food_used: 22
food_army: 9
food_workers: 13
idle_worker_count: 8
army_count: 8
warp_gate_count: 0

game_loop:  24381
ui_data{
 
Score:  10800
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
