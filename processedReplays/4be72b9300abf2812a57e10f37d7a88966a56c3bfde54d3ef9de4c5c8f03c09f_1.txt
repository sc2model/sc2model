----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2909
  player_apm: 61
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3146
  player_apm: 58
}
game_duration_loops: 9039
game_duration_seconds: 403.554931641
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6210
score_details {
  idle_production_time: 618.8125
  idle_worker_time: 306.8125
  total_value_units: 3775.0
  total_value_structures: 2150.0
  killed_value_units: 1975.0
  killed_value_structures: 100.0
  collected_minerals: 5330.0
  collected_vespene: 1680.0
  collection_rate_minerals: 1035.0
  collection_rate_vespene: 313.0
  spent_minerals: 4675.0
  spent_vespene: 1500.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 700.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 50.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 2400.0
    technology: 975.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 425.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 2600.0
    technology: 875.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1170.0
    shields: 1628.625
    energy: 0.0
  }
  total_damage_taken {
    life: 1948.80786133
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 705
vespene: 180
food_cap: 47
food_used: 39
food_army: 13
food_workers: 26
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  9039
ui_data{
 single {
  unit {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  6210
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
