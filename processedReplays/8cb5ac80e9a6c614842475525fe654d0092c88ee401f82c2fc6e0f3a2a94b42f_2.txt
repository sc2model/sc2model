----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2788
  player_apm: 149
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2793
  player_apm: 100
}
game_duration_loops: 22340
game_duration_seconds: 997.391052246
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11178
score_details {
  idle_production_time: 1683.3125
  idle_worker_time: 504.5625
  total_value_units: 4450.0
  total_value_structures: 3450.0
  killed_value_units: 650.0
  killed_value_structures: 0.0
  collected_minerals: 8640.0
  collected_vespene: 2288.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 627.0
  spent_minerals: 7200.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 27.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1350.0
    economy: 4700.0
    technology: 1200.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4550.0
    technology: 1200.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 450.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1036.59716797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1275.33276367
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 360.638916016
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1490
vespene: 1388
food_cap: 86
food_used: 71
food_army: 27
food_workers: 43
idle_worker_count: 2
army_count: 13
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 53
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 142
    shields: 0
    energy: 104
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 77
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 133
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 24866
score_details {
  idle_production_time: 13098.3125
  idle_worker_time: 3515.3125
  total_value_units: 13700.0
  total_value_structures: 8075.0
  killed_value_units: 6375.0
  killed_value_structures: 0.0
  collected_minerals: 21245.0
  collected_vespene: 7696.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 783.0
  spent_minerals: 18175.0
  spent_vespene: 5600.0
  food_used {
    none: 0.0
    army: 70.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4525.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4575.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3425.0
    economy: 7050.0
    technology: 3075.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 2250.0
    economy: 300.0
    technology: 1450.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 8000.0
    economy: 7650.0
    technology: 3075.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 2800.0
    economy: 600.0
    technology: 1450.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 7770.796875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6746.46484375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 947.806152344
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 3120
vespene: 2096
food_cap: 180
food_used: 128
food_army: 70
food_workers: 58
idle_worker_count: 6
army_count: 28
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 54
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 196
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 129
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 67
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 124
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 85
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 26189
score_details {
  idle_production_time: 16760.125
  idle_worker_time: 4782.75
  total_value_units: 16025.0
  total_value_structures: 8225.0
  killed_value_units: 12875.0
  killed_value_structures: 500.0
  collected_minerals: 24205.0
  collected_vespene: 8984.0
  collection_rate_minerals: 1539.0
  collection_rate_vespene: 783.0
  spent_minerals: 20025.0
  spent_vespene: 6225.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9425.0
    economy: 1350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3175.0
    economy: 7200.0
    technology: 3075.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 1900.0
    economy: 300.0
    technology: 1450.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 9700.0
    economy: 7800.0
    technology: 3075.0
    upgrade: 1050.0
  }
  total_used_vespene {
    none: 0.0
    army: 3425.0
    economy: 600.0
    technology: 1450.0
    upgrade: 1050.0
  }
  total_damage_dealt {
    life: 17226.2753906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8606.51757812
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1676.08447266
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 4230
vespene: 2759
food_cap: 180
food_used: 123
food_army: 65
food_workers: 58
idle_worker_count: 9
army_count: 33
warp_gate_count: 0

game_loop:  22340
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 200
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 131
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 117
    shields: 0
    energy: 200
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 195
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 142
    shields: 0
    energy: 128
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 142
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 148
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 166
  }
}

Score:  26189
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
