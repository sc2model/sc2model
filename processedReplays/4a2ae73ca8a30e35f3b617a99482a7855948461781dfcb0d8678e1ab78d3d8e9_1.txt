----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2486
  player_apm: 86
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3600
  player_apm: 51
}
game_duration_loops: 32135
game_duration_seconds: 1434.69836426
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7333
score_details {
  idle_production_time: 9269.9375
  idle_worker_time: 0.0
  total_value_units: 3900.0
  total_value_structures: 1650.0
  killed_value_units: 50.0
  killed_value_structures: 225.0
  collected_minerals: 5470.0
  collected_vespene: 1088.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 246.0
  spent_minerals: 5150.0
  spent_vespene: 500.0
  food_used {
    none: 0.0
    army: 22.5
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1125.0
    economy: 3300.0
    technology: 1250.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1450.0
    economy: 3550.0
    technology: 1250.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1755.22753906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 507.592773438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 370
vespene: 588
food_cap: 66
food_used: 52
food_army: 22
food_workers: 25
idle_worker_count: 0
army_count: 25
warp_gate_count: 0

game_loop:  10000
ui_data{
 multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 17478
score_details {
  idle_production_time: 31012.8125
  idle_worker_time: 7.5625
  total_value_units: 15225.0
  total_value_structures: 2725.0
  killed_value_units: 1400.0
  killed_value_structures: 1200.0
  collected_minerals: 15760.0
  collected_vespene: 4968.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 627.0
  spent_minerals: 15075.0
  spent_vespene: 4925.0
  food_used {
    none: 0.0
    army: 93.5
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 1675.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4925.0
    economy: 5250.0
    technology: 2225.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 650.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 7650.0
    economy: 6100.0
    technology: 2575.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 3425.0
    economy: 0.0
    technology: 900.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 6673.41601562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4682.98046875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 735
vespene: 43
food_cap: 154
food_used: 135
food_army: 93
food_workers: 42
idle_worker_count: 1
army_count: 38
warp_gate_count: 0

game_loop:  20000
ui_data{
 multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15157
score_details {
  idle_production_time: 48558.5625
  idle_worker_time: 5916.75
  total_value_units: 22675.0
  total_value_structures: 3825.0
  killed_value_units: 5925.0
  killed_value_structures: 1900.0
  collected_minerals: 22470.0
  collected_vespene: 7612.0
  collection_rate_minerals: 1063.0
  collection_rate_vespene: 335.0
  spent_minerals: 20525.0
  spent_vespene: 7275.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2050.0
    economy: 3825.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10200.0
    economy: 500.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 4650.0
    technology: 3075.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 850.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 12500.0
    economy: 6250.0
    technology: 3875.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 6025.0
    economy: 0.0
    technology: 1100.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 15441.0673828
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 18799.140625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1995
vespene: 337
food_cap: 140
food_used: 65
food_army: 35
food_workers: 30
idle_worker_count: 2
army_count: 25
warp_gate_count: 0

game_loop:  30000
ui_data{
 multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 149
    shields: 0
    energy: 200
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8596
score_details {
  idle_production_time: 51491.875
  idle_worker_time: 6442.8125
  total_value_units: 23525.0
  total_value_structures: 4125.0
  killed_value_units: 9600.0
  killed_value_structures: 1900.0
  collected_minerals: 22880.0
  collected_vespene: 7716.0
  collection_rate_minerals: 167.0
  collection_rate_vespene: 0.0
  spent_minerals: 21375.0
  spent_vespene: 7275.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 9.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4300.0
    economy: 3925.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13175.0
    economy: 2200.0
    technology: 1825.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: -150.0
    economy: 3100.0
    technology: 1700.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 850.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 13200.0
    economy: 6750.0
    technology: 3875.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 6025.0
    economy: 0.0
    technology: 1100.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 17923.953125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 30794.6542969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1555
vespene: 441
food_cap: 132
food_used: 11
food_army: 2
food_workers: 9
idle_worker_count: 9
army_count: 0
warp_gate_count: 0

game_loop:  32135
ui_data{
 
Score:  8596
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
