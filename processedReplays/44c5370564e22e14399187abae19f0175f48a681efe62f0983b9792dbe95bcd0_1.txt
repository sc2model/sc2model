----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3389
  player_apm: 156
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3420
  player_apm: 148
}
game_duration_loops: 34037
game_duration_seconds: 1519.61499023
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8995
score_details {
  idle_production_time: 11907.625
  idle_worker_time: 67.0625
  total_value_units: 5200.0
  total_value_structures: 1800.0
  killed_value_units: 2250.0
  killed_value_structures: 375.0
  collected_minerals: 7875.0
  collected_vespene: 1376.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 358.0
  spent_minerals: 7331.0
  spent_vespene: 675.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1025.0
    economy: 900.0
    technology: 200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 900.0
    economy: 131.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 900.0
    economy: 4700.0
    technology: 1250.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4500.0
    technology: 1400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 4555.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1430.69238281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 594
vespene: 701
food_cap: 66
food_used: 66
food_army: 15
food_workers: 34
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 27835
score_details {
  idle_production_time: 48271.8125
  idle_worker_time: 1198.5
  total_value_units: 17100.0
  total_value_structures: 3600.0
  killed_value_units: 5400.0
  killed_value_structures: 475.0
  collected_minerals: 22455.0
  collected_vespene: 8236.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 1254.0
  spent_minerals: 19956.0
  spent_vespene: 4200.0
  food_used {
    none: 0.0
    army: 93.0
    economy: 64.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2875.0
    economy: 1500.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2000.0
    economy: 1181.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 225.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 5325.0
    economy: 7450.0
    technology: 2850.0
    upgrade: 1775.0
  }
  used_vespene {
    none: 150.0
    army: 1275.0
    economy: 0.0
    technology: 500.0
    upgrade: 1775.0
  }
  total_used_minerals {
    none: 300.0
    army: 8500.0
    economy: 10150.0
    technology: 3200.0
    upgrade: 1475.0
  }
  total_used_vespene {
    none: 300.0
    army: 1775.0
    economy: 0.0
    technology: 750.0
    upgrade: 1475.0
  }
  total_damage_dealt {
    life: 9030.28320312
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5347.12939453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2549
vespene: 4036
food_cap: 182
food_used: 157
food_army: 93
food_workers: 64
idle_worker_count: 0
army_count: 122
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 122
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19560
score_details {
  idle_production_time: 85625.1875
  idle_worker_time: 3129.3125
  total_value_units: 29150.0
  total_value_structures: 4675.0
  killed_value_units: 17700.0
  killed_value_structures: 2775.0
  collected_minerals: 31475.0
  collected_vespene: 11716.0
  collection_rate_minerals: 979.0
  collection_rate_vespene: 0.0
  spent_minerals: 31181.0
  spent_vespene: 8800.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 11925.0
    economy: 4925.0
    technology: 800.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 2525.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 10825.0
    economy: 4656.0
    technology: 850.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 3875.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1700.0
    economy: 4950.0
    technology: 2450.0
    upgrade: 2625.0
  }
  used_vespene {
    none: 150.0
    army: 800.0
    economy: 0.0
    technology: 850.0
    upgrade: 2625.0
  }
  total_used_minerals {
    none: 300.0
    army: 19500.0
    economy: 10725.0
    technology: 3650.0
    upgrade: 2625.0
  }
  total_used_vespene {
    none: 300.0
    army: 4975.0
    economy: 0.0
    technology: 1150.0
    upgrade: 2625.0
  }
  total_damage_dealt {
    life: 30815.2792969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 32924.640625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 344
vespene: 2916
food_cap: 162
food_used: 51
food_army: 29
food_workers: 22
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 109
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 197
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 404
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 430
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 406
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20865
score_details {
  idle_production_time: 102401.0
  idle_worker_time: 3129.3125
  total_value_units: 33900.0
  total_value_structures: 5500.0
  killed_value_units: 21600.0
  killed_value_structures: 3450.0
  collected_minerals: 35205.0
  collected_vespene: 11716.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 0.0
  spent_minerals: 34756.0
  spent_vespene: 10350.0
  food_used {
    none: 0.0
    army: 42.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 15175.0
    economy: 4925.0
    technology: 1250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 3175.0
    economy: 0.0
    technology: 425.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12225.0
    economy: 5006.0
    technology: 975.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3050.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 3875.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2400.0
    economy: 5800.0
    technology: 2700.0
    upgrade: 2625.0
  }
  used_vespene {
    none: 150.0
    army: 1500.0
    economy: 0.0
    technology: 1050.0
    upgrade: 2625.0
  }
  total_used_minerals {
    none: 300.0
    army: 21750.0
    economy: 12425.0
    technology: 4125.0
    upgrade: 2625.0
  }
  total_used_vespene {
    none: 300.0
    army: 6475.0
    economy: 0.0
    technology: 1500.0
    upgrade: 2625.0
  }
  total_damage_dealt {
    life: 37556.3046875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 37216.9609375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 499
vespene: 1366
food_cap: 168
food_used: 81
food_army: 42
food_workers: 39
idle_worker_count: 0
army_count: 10
warp_gate_count: 0
larva_count: 23

game_loop:  34037
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 109
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 109
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1471
    shields: 0
    energy: 0
  }
  units {
    unit_type: 101
    player_relative: 1
    health: 2500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 578
    shields: 0
    energy: 0
    build_progress: 0.316874980927
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

Score:  20865
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
