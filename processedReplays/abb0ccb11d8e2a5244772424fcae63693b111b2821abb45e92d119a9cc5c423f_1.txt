----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3273
  player_apm: 54
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3325
  player_apm: 151
}
game_duration_loops: 17947
game_duration_seconds: 801.261291504
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9535
score_details {
  idle_production_time: 1630.9375
  idle_worker_time: 55.9375
  total_value_units: 4125.0
  total_value_structures: 3450.0
  killed_value_units: 1325.0
  killed_value_structures: 0.0
  collected_minerals: 8085.0
  collected_vespene: 1600.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 313.0
  spent_minerals: 6075.0
  spent_vespene: 1450.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1150.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: 50.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 900.0
    economy: 4050.0
    technology: 1075.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 150.0
    technology: 425.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4100.0
    technology: 1175.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 425.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2140.46923828
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1347.875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2060
vespene: 150
food_cap: 93
food_used: 50
food_army: 18
food_workers: 32
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 27
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
single {
  unit {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 6937
score_details {
  idle_production_time: 5598.6875
  idle_worker_time: 616.0
  total_value_units: 12175.0
  total_value_structures: 6100.0
  killed_value_units: 8350.0
  killed_value_structures: 0.0
  collected_minerals: 14360.0
  collected_vespene: 5704.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 335.0
  spent_minerals: 14025.0
  spent_vespene: 5500.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 10.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 6375.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6479.0
    economy: 2793.0
    technology: 950.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3805.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 2100.0
    technology: 2075.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 925.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 6300.0
    economy: 5250.0
    technology: 3025.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 3675.0
    economy: 300.0
    technology: 925.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 13025.6152344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 18298.7578125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1214.88378906
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 113
vespene: 74
food_cap: 54
food_used: 19
food_army: 9
food_workers: 10
idle_worker_count: 1
army_count: 0
warp_gate_count: 0

game_loop:  17947
ui_data{
 
Score:  6937
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
