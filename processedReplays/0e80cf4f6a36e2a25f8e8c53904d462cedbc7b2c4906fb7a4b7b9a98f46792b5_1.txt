----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2855
  player_apm: 76
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3014
  player_apm: 96
}
game_duration_loops: 23396
game_duration_seconds: 1044.53723145
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10698
score_details {
  idle_production_time: 4559.3125
  idle_worker_time: 28.125
  total_value_units: 5700.0
  total_value_structures: 1600.0
  killed_value_units: 350.0
  killed_value_structures: 100.0
  collected_minerals: 7830.0
  collected_vespene: 2168.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 694.0
  spent_minerals: 7450.0
  spent_vespene: 1550.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1575.0
    economy: 4050.0
    technology: 1500.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 100.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 1825.0
    economy: 4700.0
    technology: 1650.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 200.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 164.316894531
    shields: 147.316894531
    energy: 0.0
  }
  total_damage_taken {
    life: 635.260742188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 430
vespene: 618
food_cap: 100
food_used: 67
food_army: 35
food_workers: 32
idle_worker_count: 0
army_count: 19
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 106
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 14820
score_details {
  idle_production_time: 14625.6875
  idle_worker_time: 306.5
  total_value_units: 13400.0
  total_value_structures: 3200.0
  killed_value_units: 8125.0
  killed_value_structures: 1600.0
  collected_minerals: 16745.0
  collected_vespene: 6412.0
  collection_rate_minerals: 1147.0
  collection_rate_vespene: 515.0
  spent_minerals: 15450.0
  spent_vespene: 4112.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 34.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4900.0
    economy: 1900.0
    technology: 900.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4825.0
    economy: 1800.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2025.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1300.0
    economy: 4950.0
    technology: 1675.0
    upgrade: 1175.0
  }
  used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 300.0
    upgrade: 1175.0
  }
  total_used_minerals {
    none: 0.0
    army: 5925.0
    economy: 7900.0
    technology: 2450.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 2825.0
    economy: 0.0
    technology: 400.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 8176.46240234
    shields: 7933.57519531
    energy: 0.0
  }
  total_damage_taken {
    life: 13794.890625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1345
vespene: 2300
food_cap: 154
food_used: 62
food_army: 28
food_workers: 33
idle_worker_count: 3
army_count: 8
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 2
}
production {
  unit {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 107
    build_progress: 0.0359848737717
  }
}

score{
 score_type: Melee
score: 15942
score_details {
  idle_production_time: 20420.375
  idle_worker_time: 993.625
  total_value_units: 18150.0
  total_value_structures: 3250.0
  killed_value_units: 13175.0
  killed_value_structures: 2075.0
  collected_minerals: 19470.0
  collected_vespene: 7584.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 492.0
  spent_minerals: 18475.0
  spent_vespene: 5662.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 7400.0
    economy: 3125.0
    technology: 1200.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6525.0
    economy: 1900.0
    technology: 625.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2875.0
    economy: 0.0
    technology: -38.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2300.0
    economy: 4850.0
    technology: 1875.0
    upgrade: 1175.0
  }
  used_vespene {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 450.0
    upgrade: 1175.0
  }
  total_used_minerals {
    none: 0.0
    army: 9025.0
    economy: 8100.0
    technology: 2850.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 4425.0
    economy: 0.0
    technology: 700.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 10558.1796875
    shields: 11032.4853516
    energy: 0.0
  }
  total_damage_taken {
    life: 16113.1386719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1045
vespene: 1922
food_cap: 146
food_used: 77
food_army: 46
food_workers: 31
idle_worker_count: 1
army_count: 19
warp_gate_count: 0

game_loop:  23396
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 101
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 77
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 88
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 127
    player_relative: 1
    health: 30
    shields: 0
    energy: 123
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  15942
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
