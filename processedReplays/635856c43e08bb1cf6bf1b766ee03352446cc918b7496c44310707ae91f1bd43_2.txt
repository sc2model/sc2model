----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2802
  player_apm: 39
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 37
}
game_duration_loops: 25296
game_duration_seconds: 1129.36450195
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11623
score_details {
  idle_production_time: 2036.9375
  idle_worker_time: 444.4375
  total_value_units: 3230.0
  total_value_structures: 4350.0
  killed_value_units: 300.0
  killed_value_structures: 0.0
  collected_minerals: 8905.0
  collected_vespene: 2328.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 649.0
  spent_minerals: 7065.0
  spent_vespene: 2275.0
  food_used {
    none: 0.0
    army: 20.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1230.0
    economy: 4100.0
    technology: 1850.0
    upgrade: 475.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 700.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 0.0
    army: 530.0
    economy: 4150.0
    technology: 1850.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 700.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 100.0
    shields: 68.25
    energy: 0.0
  }
  total_damage_taken {
    life: 40.0
    shields: 55.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1890
vespene: 53
food_cap: 86
food_used: 66
food_army: 20
food_workers: 45
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 67
  count: 2
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 32017
score_details {
  idle_production_time: 10644.125
  idle_worker_time: 1755.6875
  total_value_units: 13200.0
  total_value_structures: 10300.0
  killed_value_units: 2300.0
  killed_value_structures: 100.0
  collected_minerals: 23285.0
  collected_vespene: 8112.0
  collection_rate_minerals: 2463.0
  collection_rate_vespene: 985.0
  spent_minerals: 21090.0
  spent_vespene: 8050.0
  food_used {
    none: 0.0
    army: 119.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 650.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7785.0
    economy: 7850.0
    technology: 4250.0
    upgrade: 1775.0
  }
  used_vespene {
    none: 0.0
    army: 5125.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1775.0
  }
  total_used_minerals {
    none: 0.0
    army: 6325.0
    economy: 8500.0
    technology: 4550.0
    upgrade: 1250.0
  }
  total_used_vespene {
    none: 0.0
    army: 4175.0
    economy: 0.0
    technology: 1150.0
    upgrade: 1250.0
  }
  total_damage_dealt {
    life: 1550.0
    shields: 968.25
    energy: 0.0
  }
  total_damage_taken {
    life: 729.0
    shields: 1465.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2245
vespene: 62
food_cap: 200
food_used: 186
food_army: 119
food_workers: 67
idle_worker_count: 3
army_count: 17
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 67
  count: 5
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 37323
score_details {
  idle_production_time: 14964.4375
  idle_worker_time: 2641.6875
  total_value_units: 18120.0
  total_value_structures: 11450.0
  killed_value_units: 15830.0
  killed_value_structures: 6050.0
  collected_minerals: 30995.0
  collected_vespene: 12108.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 806.0
  spent_minerals: 23670.0
  spent_vespene: 8850.0
  food_used {
    none: 0.0
    army: 110.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9130.0
    economy: 4400.0
    technology: 3000.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4800.0
    economy: 0.0
    technology: 550.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1405.0
    economy: 2700.0
    technology: 2550.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 7790.0
    economy: 6950.0
    technology: 2000.0
    upgrade: 2075.0
  }
  used_vespene {
    none: 0.0
    army: 4650.0
    economy: 0.0
    technology: 1150.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 0.0
    army: 9495.0
    economy: 9650.0
    technology: 4550.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 0.0
    army: 5925.0
    economy: 0.0
    technology: 1150.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 25700.5
    shields: 25062.625
    energy: 0.0
  }
  total_damage_taken {
    life: 8500.0
    shields: 9609.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 7375
vespene: 3258
food_cap: 200
food_used: 157
food_army: 110
food_workers: 47
idle_worker_count: 8
army_count: 20
warp_gate_count: 0

game_loop:  25296
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 67
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 82
  count: 18
}
multi {
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 89
    shields: 31
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 27
    shields: 43
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
}

Score:  37323
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
