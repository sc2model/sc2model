----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 121
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3080
  player_apm: 125
}
game_duration_loops: 37483
game_duration_seconds: 1673.46496582
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10984
score_details {
  idle_production_time: 5621.0
  idle_worker_time: 0.0
  total_value_units: 6600.0
  total_value_structures: 1925.0
  killed_value_units: 175.0
  killed_value_structures: 0.0
  collected_minerals: 8300.0
  collected_vespene: 1784.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 470.0
  spent_minerals: 8125.0
  spent_vespene: 1650.0
  food_used {
    none: 0.0
    army: 57.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2600.0
    economy: 4900.0
    technology: 1375.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 200.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 5600.0
    technology: 1525.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1700.0
    economy: 0.0
    technology: 300.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 148.0
    shields: 278.0
    energy: 0.0
  }
  total_damage_taken {
    life: 342.083496094
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 225
vespene: 134
food_cap: 130
food_used: 100
food_army: 57
food_workers: 43
idle_worker_count: 0
army_count: 17
warp_gate_count: 0
larva_count: 9

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 15
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 24771
score_details {
  idle_production_time: 28370.5625
  idle_worker_time: 73.125
  total_value_units: 20250.0
  total_value_structures: 2950.0
  killed_value_units: 13600.0
  killed_value_structures: 0.0
  collected_minerals: 24100.0
  collected_vespene: 8596.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 1142.0
  spent_minerals: 22325.0
  spent_vespene: 7725.0
  food_used {
    none: 0.0
    army: 133.5
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 9650.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6250.0
    economy: 8000.0
    technology: 2300.0
    upgrade: 825.0
  }
  used_vespene {
    none: 0.0
    army: 3025.0
    economy: 0.0
    technology: 850.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 0.0
    army: 10600.0
    economy: 8850.0
    technology: 2400.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 5400.0
    economy: 0.0
    technology: 900.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 7252.75976562
    shields: 7249.99414062
    energy: 0.0
  }
  total_damage_taken {
    life: 8414.5078125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1825
vespene: 871
food_cap: 200
food_used: 194
food_army: 133
food_workers: 61
idle_worker_count: 6
army_count: 51
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 43
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 33214
score_details {
  idle_production_time: 44994.5625
  idle_worker_time: 885.0625
  total_value_units: 35450.0
  total_value_structures: 4350.0
  killed_value_units: 27475.0
  killed_value_structures: 350.0
  collected_minerals: 38410.0
  collected_vespene: 16704.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 806.0
  spent_minerals: 35425.0
  spent_vespene: 15775.0
  food_used {
    none: 0.0
    army: 139.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 18300.0
    economy: 1800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 7725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 8025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 6925.0
    economy: 9300.0
    technology: 2400.0
    upgrade: 2725.0
  }
  used_vespene {
    none: 150.0
    army: 3875.0
    economy: 0.0
    technology: 1000.0
    upgrade: 2725.0
  }
  total_used_minerals {
    none: 300.0
    army: 20475.0
    economy: 10500.0
    technology: 2850.0
    upgrade: 2425.0
  }
  total_used_vespene {
    none: 300.0
    army: 11825.0
    economy: 0.0
    technology: 1400.0
    upgrade: 2425.0
  }
  total_damage_dealt {
    life: 15267.8398438
    shields: 17505.2441406
    energy: 0.0
  }
  total_damage_taken {
    life: 24791.4707031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3035
vespene: 929
food_cap: 200
food_used: 199
food_army: 139
food_workers: 60
idle_worker_count: 6
army_count: 44
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 40
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 7
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 86
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 76
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 57
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 58
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 102
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 195
    shields: 0
    energy: 200
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 139
    shields: 0
    energy: 200
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 123
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 76
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 35374
score_details {
  idle_production_time: 52854.75
  idle_worker_time: 2276.9375
  total_value_units: 50900.0
  total_value_structures: 4700.0
  killed_value_units: 39875.0
  killed_value_structures: 1100.0
  collected_minerals: 49672.0
  collected_vespene: 22452.0
  collection_rate_minerals: 2301.0
  collection_rate_vespene: 1007.0
  spent_minerals: 43825.0
  spent_vespene: 20475.0
  food_used {
    none: 0.0
    army: 92.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25300.0
    economy: 4400.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 11275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 24950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 12900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 4750.0
    economy: 9350.0
    technology: 2400.0
    upgrade: 3275.0
  }
  used_vespene {
    none: 150.0
    army: 3150.0
    economy: 0.0
    technology: 1000.0
    upgrade: 3275.0
  }
  total_used_minerals {
    none: 300.0
    army: 30375.0
    economy: 11000.0
    technology: 2850.0
    upgrade: 3100.0
  }
  total_used_vespene {
    none: 300.0
    army: 17375.0
    economy: 0.0
    technology: 1400.0
    upgrade: 3100.0
  }
  total_damage_dealt {
    life: 23373.8652344
    shields: 26180.1777344
    energy: 0.0
  }
  total_damage_taken {
    life: 38129.1367188
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 5897
vespene: 1977
food_cap: 200
food_used: 150
food_army: 92
food_workers: 58
idle_worker_count: 0
army_count: 43
warp_gate_count: 0

game_loop:  37483
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 107
  count: 26
}
groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 10
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 8
}
multi {
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 108
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
}

Score:  35374
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
