----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4244
  player_apm: 144
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4193
  player_apm: 359
}
game_duration_loops: 14784
game_duration_seconds: 660.046081543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11293
score_details {
  idle_production_time: 4437.9375
  idle_worker_time: 0.0
  total_value_units: 7300.0
  total_value_structures: 1650.0
  killed_value_units: 450.0
  killed_value_structures: 0.0
  collected_minerals: 9155.0
  collected_vespene: 1588.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 649.0
  spent_minerals: 9025.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2175.0
    economy: 5700.0
    technology: 1350.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 100.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2425.0
    economy: 6150.0
    technology: 1250.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 200.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 565.860351562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1338.90478516
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 180
vespene: 463
food_cap: 122
food_used: 107
food_army: 46
food_workers: 53
idle_worker_count: 0
army_count: 16
warp_gate_count: 0
larva_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20397
score_details {
  idle_production_time: 9931.0625
  idle_worker_time: 28.125
  total_value_units: 15700.0
  total_value_structures: 2325.0
  killed_value_units: 7225.0
  killed_value_structures: 650.0
  collected_minerals: 17040.0
  collected_vespene: 4732.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 985.0
  spent_minerals: 16700.0
  spent_vespene: 4200.0
  food_used {
    none: 0.0
    army: 142.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4175.0
    economy: 2325.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1100.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 375.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6275.0
    economy: 6700.0
    technology: 1775.0
    upgrade: 1075.0
  }
  used_vespene {
    none: 0.0
    army: 2275.0
    economy: 0.0
    technology: 300.0
    upgrade: 1075.0
  }
  total_used_minerals {
    none: 0.0
    army: 7450.0
    economy: 8300.0
    technology: 1925.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 3250.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 11206.8603516
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6506.72070312
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 390
vespene: 532
food_cap: 200
food_used: 200
food_army: 142
food_workers: 58
idle_worker_count: 0
army_count: 64
warp_gate_count: 0

game_loop:  14784
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 28
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  20397
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
