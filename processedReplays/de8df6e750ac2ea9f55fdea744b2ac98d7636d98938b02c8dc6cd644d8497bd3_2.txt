----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5388
  player_apm: 319
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5328
  player_apm: 310
}
game_duration_loops: 21558
game_duration_seconds: 962.477905273
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9771
score_details {
  idle_production_time: 7634.9375
  idle_worker_time: 34.75
  total_value_units: 7200.0
  total_value_structures: 1550.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 8585.0
  collected_vespene: 1192.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 492.0
  spent_minerals: 8431.0
  spent_vespene: 875.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 850.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4950.0
    technology: 1250.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 150.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2700.0
    economy: 5425.0
    technology: 725.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 920.819335938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1528.12841797
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 204
vespene: 317
food_cap: 114
food_used: 89
food_army: 41
food_workers: 44
idle_worker_count: 0
army_count: 20
warp_gate_count: 0
larva_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 1
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
    add_on {
      unit_type: 100
      build_progress: 0.72265625
    }
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15078
score_details {
  idle_production_time: 18423.25
  idle_worker_time: 52.8125
  total_value_units: 26300.0
  total_value_structures: 2400.0
  killed_value_units: 15525.0
  killed_value_structures: 0.0
  collected_minerals: 24835.0
  collected_vespene: 6824.0
  collection_rate_minerals: 1987.0
  collection_rate_vespene: 671.0
  spent_minerals: 24431.0
  spent_vespene: 6600.0
  food_used {
    none: 0.0
    army: 69.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 11125.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 12300.0
    economy: 456.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3125.0
    economy: 6925.0
    technology: 1525.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 1075.0
    economy: 0.0
    technology: 250.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 15800.0
    economy: 8275.0
    technology: 1675.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 6375.0
    economy: 0.0
    technology: 350.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 21803.9277344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 26198.8691406
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 454
vespene: 224
food_cap: 176
food_used: 125
food_army: 69
food_workers: 56
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 5
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 137
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 138
    shields: 0
    energy: 150
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 79
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 79
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 142
    shields: 0
    energy: 176
  }
}

score{
 score_type: Melee
score: 11714
score_details {
  idle_production_time: 20248.4375
  idle_worker_time: 378.8125
  total_value_units: 29100.0
  total_value_structures: 2400.0
  killed_value_units: 19825.0
  killed_value_structures: 0.0
  collected_minerals: 27185.0
  collected_vespene: 7560.0
  collection_rate_minerals: 1903.0
  collection_rate_vespene: 671.0
  spent_minerals: 27056.0
  spent_vespene: 7475.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 14250.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16550.0
    economy: 181.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 6100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 425.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1275.0
    economy: 6575.0
    technology: 1525.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 250.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 17900.0
    economy: 8275.0
    technology: 1675.0
    upgrade: 750.0
  }
  total_used_vespene {
    none: 0.0
    army: 7075.0
    economy: 0.0
    technology: 350.0
    upgrade: 750.0
  }
  total_damage_dealt {
    life: 27805.859375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 34959.5273438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 179
vespene: 85
food_cap: 176
food_used: 85
food_army: 29
food_workers: 56
idle_worker_count: 56
army_count: 2
warp_gate_count: 0

game_loop:  21558
ui_data{
 
Score:  11714
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
