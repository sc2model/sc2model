----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3055
  player_apm: 123
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 89
}
game_duration_loops: 6907
game_duration_seconds: 308.369720459
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6962
score_details {
  idle_production_time: 514.25
  idle_worker_time: 19.3125
  total_value_units: 3200.0
  total_value_structures: 2300.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 5310.0
  collected_vespene: 1052.0
  collection_rate_minerals: 1735.0
  collection_rate_vespene: 313.0
  spent_minerals: 4150.0
  spent_vespene: 550.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 650.0
    economy: 3400.0
    technology: 750.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 950.0
    economy: 3300.0
    technology: 750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 675.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 321.0
    shields: 566.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1210
vespene: 502
food_cap: 70
food_used: 53
food_army: 14
food_workers: 37
idle_worker_count: 0
army_count: 7
warp_gate_count: 3

game_loop:  6907
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

Score:  6962
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
