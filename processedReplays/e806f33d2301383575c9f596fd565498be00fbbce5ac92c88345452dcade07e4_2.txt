----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5064
  player_apm: 215
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5433
  player_apm: 226
}
game_duration_loops: 16844
game_duration_seconds: 752.016784668
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 15867
score_details {
  idle_production_time: 5060.8125
  idle_worker_time: 8.625
  total_value_units: 9500.0
  total_value_structures: 2600.0
  killed_value_units: 475.0
  killed_value_structures: 0.0
  collected_minerals: 12605.0
  collected_vespene: 2324.0
  collection_rate_minerals: 2687.0
  collection_rate_vespene: 1052.0
  spent_minerals: 11962.0
  spent_vespene: 2000.0
  food_used {
    none: 0.0
    army: 47.0
    economy: 81.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 12.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2700.0
    economy: 7900.0
    technology: 1600.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 350.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 2400.0
    economy: 8600.0
    technology: 1750.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 450.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 190.0
    shields: 224.125
    energy: 0.0
  }
  total_damage_taken {
    life: 243.332519531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 693
vespene: 324
food_cap: 128
food_used: 128
food_army: 47
food_workers: 81
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 129
  count: 21
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20195
score_details {
  idle_production_time: 36285.1875
  idle_worker_time: 8.625
  total_value_units: 28600.0
  total_value_structures: 3200.0
  killed_value_units: 12825.0
  killed_value_structures: 1025.0
  collected_minerals: 27921.0
  collected_vespene: 8936.0
  collection_rate_minerals: 2799.0
  collection_rate_vespene: 1299.0
  spent_minerals: 27637.0
  spent_vespene: 8625.0
  food_used {
    none: 0.0
    army: 79.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8750.0
    economy: 1525.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11075.0
    economy: 662.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1050.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3950.0
    economy: 8700.0
    technology: 1800.0
    upgrade: 1325.0
  }
  used_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 500.0
    upgrade: 1325.0
  }
  total_used_minerals {
    none: 0.0
    army: 16200.0
    economy: 10650.0
    technology: 2150.0
    upgrade: 1175.0
  }
  total_used_vespene {
    none: 0.0
    army: 6550.0
    economy: 0.0
    technology: 750.0
    upgrade: 1175.0
  }
  total_damage_dealt {
    life: 14217.1855469
    shields: 15457.9521484
    energy: 0.0
  }
  total_damage_taken {
    life: 14970.5175781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 334
vespene: 311
food_cap: 200
food_used: 154
food_army: 79
food_workers: 75
idle_worker_count: 0
army_count: 27
warp_gate_count: 0

game_loop:  16844
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 129
  count: 37
}
groups {
  control_group_index: 2
  leader_unit_type: 111
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 198
    shields: 0
    energy: 138
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 74
    shields: 0
    energy: 136
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 73
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

Score:  20195
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
