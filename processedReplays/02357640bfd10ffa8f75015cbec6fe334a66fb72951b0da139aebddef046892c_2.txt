----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2886
  player_apm: 121
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3206
  player_apm: 137
}
game_duration_loops: 5775
game_duration_seconds: 257.830505371
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4506
score_details {
  idle_production_time: 540.5625
  idle_worker_time: 199.75
  total_value_units: 2075.0
  total_value_structures: 2000.0
  killed_value_units: 125.0
  killed_value_structures: 0.0
  collected_minerals: 3640.0
  collected_vespene: 716.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 223.0
  spent_minerals: 3275.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: -200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 125.0
    economy: 2450.0
    technology: 950.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 625.0
    economy: 2150.0
    technology: 950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 414.170410156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 400.0
    shields: 545.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 415
vespene: 316
food_cap: 47
food_used: 24
food_army: 2
food_workers: 22
idle_worker_count: 22
army_count: 1
warp_gate_count: 4

game_loop:  5775
ui_data{
 
Score:  4506
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
