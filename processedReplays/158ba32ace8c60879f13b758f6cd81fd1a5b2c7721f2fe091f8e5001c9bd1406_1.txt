----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4736
  player_apm: 165
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 256
}
game_duration_loops: 12326
game_duration_seconds: 550.306274414
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11570
score_details {
  idle_production_time: 637.4375
  idle_worker_time: 106.125
  total_value_units: 6200.0
  total_value_structures: 3450.0
  killed_value_units: 1150.0
  killed_value_structures: 350.0
  collected_minerals: 8535.0
  collected_vespene: 2660.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 671.0
  spent_minerals: 8300.0
  spent_vespene: 2300.0
  food_used {
    none: 0.0
    army: 43.0
    economy: 54.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2200.0
    economy: 5400.0
    technology: 1050.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 450.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2100.0
    economy: 5050.0
    technology: 750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3065.6484375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 620.0
    shields: 695.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 285
vespene: 360
food_cap: 126
food_used: 97
food_army: 43
food_workers: 52
idle_worker_count: 0
army_count: 14
warp_gate_count: 2

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 495
  count: 7
}
groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 15316
score_details {
  idle_production_time: 1362.8125
  idle_worker_time: 171.75
  total_value_units: 7975.0
  total_value_structures: 4800.0
  killed_value_units: 3950.0
  killed_value_structures: 600.0
  collected_minerals: 12275.0
  collected_vespene: 3516.0
  collection_rate_minerals: 2239.0
  collection_rate_vespene: 313.0
  spent_minerals: 10300.0
  spent_vespene: 3125.0
  food_used {
    none: 0.0
    army: 55.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 2550.0
    technology: 250.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 550.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2800.0
    economy: 5450.0
    technology: 1500.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 550.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 2900.0
    economy: 6100.0
    technology: 1350.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1875.0
    economy: 0.0
    technology: 550.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 6768.84912109
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1415.0
    shields: 2362.5
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2025
vespene: 391
food_cap: 141
food_used: 110
food_army: 55
food_workers: 53
idle_worker_count: 1
army_count: 18
warp_gate_count: 3

game_loop:  12326
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 495
  count: 8
}
groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 133
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 488
  count: 1
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 10
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

Score:  15316
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
