----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3328
  player_apm: 93
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3332
  player_apm: 82
}
game_duration_loops: 13777
game_duration_seconds: 615.087585449
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5599
score_details {
  idle_production_time: 2201.6875
  idle_worker_time: 56.25
  total_value_units: 3750.0
  total_value_structures: 2825.0
  killed_value_units: 1775.0
  killed_value_structures: 0.0
  collected_minerals: 4835.0
  collected_vespene: 1264.0
  collection_rate_minerals: 615.0
  collection_rate_vespene: 291.0
  spent_minerals: 4675.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 325.0
    economy: 1350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 2225.0
    technology: 1200.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1000.0
    economy: 3375.0
    technology: 1200.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 846.0
    shields: 1035.875
    energy: 0.0
  }
  total_damage_taken {
    life: 619.125
    shields: 710.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 210
vespene: 264
food_cap: 54
food_used: 33
food_army: 15
food_workers: 17
idle_worker_count: 0
army_count: 5
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
  units {
    unit_type: 67
    player_relative: 1
    health: 600
    shields: 600
    energy: 0
  }
}

score{
 score_type: Melee
score: 5843
score_details {
  idle_production_time: 3419.4375
  idle_worker_time: 56.25
  total_value_units: 6050.0
  total_value_structures: 2825.0
  killed_value_units: 2900.0
  killed_value_structures: 100.0
  collected_minerals: 7175.0
  collected_vespene: 2168.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 492.0
  spent_minerals: 6475.0
  spent_vespene: 1900.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 1450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2000.0
    economy: 1200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 500.0
    economy: 2525.0
    technology: 1200.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 2250.0
    economy: 3675.0
    technology: 1200.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1710.73291016
    shields: 2346.60791016
    energy: 0.0
  }
  total_damage_taken {
    life: 1815.0
    shields: 1480.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 750
vespene: 268
food_cap: 54
food_used: 33
food_army: 9
food_workers: 23
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  13777
ui_data{
 
Score:  5843
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
