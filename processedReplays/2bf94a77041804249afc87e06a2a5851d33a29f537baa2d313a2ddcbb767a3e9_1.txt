----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3833
  player_apm: 114
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3919
  player_apm: 166
}
game_duration_loops: 15104
game_duration_seconds: 674.332763672
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4184
score_details {
  idle_production_time: 2464.1875
  idle_worker_time: 1.3125
  total_value_units: 3600.0
  total_value_structures: 1075.0
  killed_value_units: 1800.0
  killed_value_structures: 625.0
  collected_minerals: 4130.0
  collected_vespene: 1604.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 111.0
  spent_minerals: 4075.0
  spent_vespene: 925.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 1200.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 375.0
    economy: 2300.0
    technology: 700.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 2700.0
    technology: 575.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 8211.58886719
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4022.1640625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 105
vespene: 679
food_cap: 46
food_used: 32
food_army: 6
food_workers: 18
idle_worker_count: 0
army_count: 1
warp_gate_count: 0
larva_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 1
}
production {
  unit {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  build_queue {
    unit_type: 126
    build_progress: 0.792500019073
  }
}

score{
 score_type: Melee
score: 4906
score_details {
  idle_production_time: 4649.6875
  idle_worker_time: 483.1875
  total_value_units: 6450.0
  total_value_structures: 2100.0
  killed_value_units: 2625.0
  killed_value_structures: 625.0
  collected_minerals: 7280.0
  collected_vespene: 2476.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 313.0
  spent_minerals: 7125.0
  spent_vespene: 2050.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1100.0
    economy: 1350.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2650.0
    economy: 400.0
    technology: 375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 150.0
    economy: 2150.0
    technology: 1175.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 500.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 3175.0
    economy: 3600.0
    technology: 1700.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 1925.0
    economy: 0.0
    technology: 600.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 9384.70898438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7883.93896484
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 205
vespene: 426
food_cap: 54
food_used: 23
food_army: 2
food_workers: 21
idle_worker_count: 21
army_count: 0
warp_gate_count: 0

game_loop:  15104
ui_data{
 
Score:  4906
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
