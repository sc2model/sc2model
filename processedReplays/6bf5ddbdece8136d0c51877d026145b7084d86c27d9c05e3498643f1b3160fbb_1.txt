----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5125
  player_apm: 286
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4937
  player_apm: 278
}
game_duration_loops: 8115
game_duration_seconds: 362.302062988
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5657
score_details {
  idle_production_time: 949.1875
  idle_worker_time: 93.1875
  total_value_units: 4450.0
  total_value_structures: 2100.0
  killed_value_units: 2175.0
  killed_value_structures: 400.0
  collected_minerals: 5130.0
  collected_vespene: 1352.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 268.0
  spent_minerals: 4700.0
  spent_vespene: 1150.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1400.0
    economy: 350.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1275.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1275.0
    economy: 2100.0
    technology: 950.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 2350.0
    economy: 2150.0
    technology: 950.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3352.39624023
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1135.0
    shields: 1344.54589844
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 480
vespene: 202
food_cap: 55
food_used: 43
food_army: 22
food_workers: 21
idle_worker_count: 0
army_count: 8
warp_gate_count: 4

game_loop:  8115
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}

Score:  5657
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
