----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2563
  player_apm: 69
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2661
  player_apm: 55
}
game_duration_loops: 17633
game_duration_seconds: 787.242431641
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10278
score_details {
  idle_production_time: 1742.4375
  idle_worker_time: 112.875
  total_value_units: 4225.0
  total_value_structures: 3750.0
  killed_value_units: 1450.0
  killed_value_structures: 0.0
  collected_minerals: 8830.0
  collected_vespene: 1948.0
  collection_rate_minerals: 1595.0
  collection_rate_vespene: 627.0
  spent_minerals: 7025.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 17.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 600.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 925.0
    economy: 3850.0
    technology: 1700.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 1275.0
    economy: 3900.0
    technology: 1700.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 350.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1921.04248047
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 440.0
    shields: 681.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1855
vespene: 548
food_cap: 78
food_used: 60
food_army: 17
food_workers: 43
idle_worker_count: 0
army_count: 6
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 495
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 6
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 24040
score_details {
  idle_production_time: 5034.1875
  idle_worker_time: 270.1875
  total_value_units: 12375.0
  total_value_structures: 8450.0
  killed_value_units: 4500.0
  killed_value_structures: 0.0
  collected_minerals: 18905.0
  collected_vespene: 6360.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 940.0
  spent_minerals: 17375.0
  spent_vespene: 4850.0
  food_used {
    none: 0.0
    army: 92.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3000.0
    economy: 1000.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1125.0
    economy: 650.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5050.0
    economy: 6800.0
    technology: 3900.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 650.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 6175.0
    economy: 6750.0
    technology: 3900.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 0.0
    army: 3350.0
    economy: 0.0
    technology: 650.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 5256.12548828
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2511.0
    shields: 3404.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1580
vespene: 1510
food_cap: 165
food_used: 148
food_army: 92
food_workers: 56
idle_worker_count: 0
army_count: 33
warp_gate_count: 6

game_loop:  17633
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 77
  count: 29
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 71
  count: 2
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 82
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 40
    shields: 20
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 45
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 62
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 18
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 117
  }
  units {
    unit_type: 77
    player_relative: 1
    health: 40
    shields: 40
    energy: 167
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 65
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
}

Score:  24040
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
