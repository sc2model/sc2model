----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3406
  player_apm: 145
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3698
  player_apm: 149
}
game_duration_loops: 16722
game_duration_seconds: 746.569946289
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9661
score_details {
  idle_production_time: 1314.125
  idle_worker_time: 1233.75
  total_value_units: 5475.0
  total_value_structures: 3550.0
  killed_value_units: 800.0
  killed_value_structures: 0.0
  collected_minerals: 7665.0
  collected_vespene: 2096.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 649.0
  spent_minerals: 7525.0
  spent_vespene: 1950.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1575.0
    economy: 4350.0
    technology: 1500.0
    upgrade: 400.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 250.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 0.0
    army: 1875.0
    economy: 4200.0
    technology: 1500.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 250.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 715.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 400.0
    shields: 340.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 190
vespene: 146
food_cap: 86
food_used: 67
food_army: 24
food_workers: 43
idle_worker_count: 0
army_count: 11
warp_gate_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 2
}
single {
  unit {
    unit_type: 78
    player_relative: 1
    health: 120
    shields: 60
    energy: 63
  }
}

score{
 score_type: Melee
score: 11730
score_details {
  idle_production_time: 3654.875
  idle_worker_time: 1484.9375
  total_value_units: 12750.0
  total_value_structures: 5250.0
  killed_value_units: 2000.0
  killed_value_structures: 0.0
  collected_minerals: 15415.0
  collected_vespene: 5340.0
  collection_rate_minerals: 1371.0
  collection_rate_vespene: 627.0
  spent_minerals: 14250.0
  spent_vespene: 4775.0
  food_used {
    none: 0.0
    army: 16.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1250.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 6075.0
    economy: 375.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1000.0
    economy: 4450.0
    technology: 2300.0
    upgrade: 550.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 550.0
    upgrade: 550.0
  }
  total_used_minerals {
    none: 0.0
    army: 6825.0
    economy: 4800.0
    technology: 2300.0
    upgrade: 550.0
  }
  total_used_vespene {
    none: 0.0
    army: 3525.0
    economy: 0.0
    technology: 550.0
    upgrade: 550.0
  }
  total_damage_dealt {
    life: 2263.84472656
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4912.40332031
    shields: 4969.34130859
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1215
vespene: 565
food_cap: 134
food_used: 57
food_army: 16
food_workers: 41
idle_worker_count: 41
army_count: 3
warp_gate_count: 9

game_loop:  16722
ui_data{
 
Score:  11730
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
