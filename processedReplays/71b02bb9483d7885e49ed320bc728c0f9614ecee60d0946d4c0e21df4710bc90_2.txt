----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3422
  player_apm: 106
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3415
  player_apm: 151
}
game_duration_loops: 14381
game_duration_seconds: 642.053710938
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11110
score_details {
  idle_production_time: 5209.9375
  idle_worker_time: 14.375
  total_value_units: 6500.0
  total_value_structures: 1200.0
  killed_value_units: 275.0
  killed_value_structures: 0.0
  collected_minerals: 8540.0
  collected_vespene: 1720.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 649.0
  spent_minerals: 7325.0
  spent_vespene: 1525.0
  food_used {
    none: 0.0
    army: 51.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 75.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2125.0
    economy: 4700.0
    technology: 850.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 100.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2425.0
    economy: 5000.0
    technology: 1000.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 554.041992188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 573.764892578
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1265
vespene: 195
food_cap: 116
food_used: 92
food_army: 51
food_workers: 41
idle_worker_count: 0
army_count: 22
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 14
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16939
score_details {
  idle_production_time: 9566.3125
  idle_worker_time: 14.375
  total_value_units: 10000.0
  total_value_structures: 1800.0
  killed_value_units: 7100.0
  killed_value_structures: 1700.0
  collected_minerals: 13845.0
  collected_vespene: 3844.0
  collection_rate_minerals: 1511.0
  collection_rate_vespene: 649.0
  spent_minerals: 11100.0
  spent_vespene: 2900.0
  food_used {
    none: 0.0
    army: 85.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4000.0
    economy: 2575.0
    technology: 825.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 925.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3450.0
    economy: 5450.0
    technology: 1000.0
    upgrade: 825.0
  }
  used_vespene {
    none: 0.0
    army: 1450.0
    economy: 0.0
    technology: 200.0
    upgrade: 825.0
  }
  total_used_minerals {
    none: 0.0
    army: 4700.0
    economy: 6550.0
    technology: 1000.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 2650.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 17328.2734375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5498.22167969
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2795
vespene: 944
food_cap: 178
food_used: 127
food_army: 85
food_workers: 42
idle_worker_count: 0
army_count: 40
warp_gate_count: 0

game_loop:  14381
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 688
  count: 27
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 75
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 135
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 101
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 87
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 128
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 82
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 119
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 59
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 134
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 83
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 119
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
}

Score:  16939
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
