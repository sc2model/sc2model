----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3916
  player_apm: 132
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3566
  player_apm: 117
}
game_duration_loops: 14087
game_duration_seconds: 628.92779541
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10823
score_details {
  idle_production_time: 10017.625
  idle_worker_time: 1.5
  total_value_units: 5450.0
  total_value_structures: 1750.0
  killed_value_units: 375.0
  killed_value_structures: 0.0
  collected_minerals: 8420.0
  collected_vespene: 1568.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 627.0
  spent_minerals: 7173.0
  spent_vespene: 1467.0
  food_used {
    none: 0.0
    army: 36.5
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 275.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 217.0
    economy: -69.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 42.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 4500.0
    technology: 1250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 975.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 4750.0
    technology: 1400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 700.186035156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1545.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1297
vespene: 101
food_cap: 92
food_used: 79
food_army: 36
food_workers: 43
idle_worker_count: 0
army_count: 29
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 22
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 89
  count: 1
}
multi {
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12647
score_details {
  idle_production_time: 18406.625
  idle_worker_time: 1.5
  total_value_units: 12100.0
  total_value_structures: 2050.0
  killed_value_units: 7100.0
  killed_value_structures: 425.0
  collected_minerals: 13410.0
  collected_vespene: 3552.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 649.0
  spent_minerals: 10873.0
  spent_vespene: 3417.0
  food_used {
    none: 0.0
    army: 37.5
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3825.0
    economy: 1925.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3317.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1617.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1975.0
    economy: 4950.0
    technology: 1250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 5450.0
    economy: 5600.0
    technology: 1400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 2725.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 11314.8740234
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7354.90332031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2587
vespene: 135
food_cap: 98
food_used: 86
food_army: 37
food_workers: 49
idle_worker_count: 0
army_count: 23
warp_gate_count: 0
larva_count: 5

game_loop:  14087
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 108
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 89
  count: 1
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 825
    shields: 0
    energy: 0
  }
}

Score:  12647
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
