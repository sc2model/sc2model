----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4216
  player_apm: 274
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4470
  player_apm: 305
}
game_duration_loops: 8685
game_duration_seconds: 387.750274658
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4114
score_details {
  idle_production_time: 5247.625
  idle_worker_time: 11.875
  total_value_units: 4150.0
  total_value_structures: 1025.0
  killed_value_units: 1200.0
  killed_value_structures: 0.0
  collected_minerals: 4730.0
  collected_vespene: 784.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 156.0
  spent_minerals: 4425.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 19.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 975.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 650.0
    technology: 37.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 2375.0
    technology: 400.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1800.0
    economy: 3275.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1740.3671875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2506.65527344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 355
vespene: 384
food_cap: 52
food_used: 26
food_army: 7
food_workers: 18
idle_worker_count: 0
army_count: 8
warp_gate_count: 0

game_loop:  8685
ui_data{
 
Score:  4114
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
