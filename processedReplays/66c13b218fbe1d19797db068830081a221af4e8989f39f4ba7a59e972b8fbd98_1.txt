----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3489
  player_apm: 54
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3518
  player_apm: 57
}
game_duration_loops: 5805
game_duration_seconds: 259.16986084
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5155
score_details {
  idle_production_time: 1521.25
  idle_worker_time: 8.875
  total_value_units: 3300.0
  total_value_structures: 1000.0
  killed_value_units: 125.0
  killed_value_structures: 100.0
  collected_minerals: 3615.0
  collected_vespene: 540.0
  collection_rate_minerals: 1091.0
  collection_rate_vespene: 201.0
  spent_minerals: 3450.0
  spent_vespene: 350.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1050.0
    economy: 2750.0
    technology: 600.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 825.0
    economy: 3100.0
    technology: 450.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 270.0
    shields: 510.0
    energy: 0.0
  }
  total_damage_taken {
    life: 334.588378906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 215
vespene: 190
food_cap: 52
food_used: 52
food_army: 24
food_workers: 28
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  5805
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 106
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 64
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  5155
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
