----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4157
  player_apm: 286
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4226
  player_apm: 142
}
game_duration_loops: 6848
game_duration_seconds: 305.735626221
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4099
score_details {
  idle_production_time: 585.1875
  idle_worker_time: 107.8125
  total_value_units: 2325.0
  total_value_structures: 2000.0
  killed_value_units: 775.0
  killed_value_structures: 100.0
  collected_minerals: 4320.0
  collected_vespene: 680.0
  collection_rate_minerals: 55.0
  collection_rate_vespene: 0.0
  spent_minerals: 3050.0
  spent_vespene: 625.0
  food_used {
    none: 0.0
    army: 1.0
    economy: 15.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 450.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 824.0
    economy: 650.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 344.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 50.0
    economy: 2000.0
    technology: 450.0
    upgrade: 0.0
  }
  used_vespene {
    none: 50.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 50.0
    army: 800.0
    economy: 2950.0
    technology: 500.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 325.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 715.125
    shields: 1069.875
    energy: 0.0
  }
  total_damage_taken {
    life: 3857.24560547
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 916.245605469
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1263
vespene: 36
food_cap: 30
food_used: 16
food_army: 1
food_workers: 15
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  6848
ui_data{
 
Score:  4099
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
