----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4157
  player_apm: 286
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4226
  player_apm: 142
}
game_duration_loops: 6848
game_duration_seconds: 305.735626221
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5581
score_details {
  idle_production_time: 579.375
  idle_worker_time: 208.375
  total_value_units: 3625.0
  total_value_structures: 2050.0
  killed_value_units: 1575.0
  killed_value_structures: 300.0
  collected_minerals: 4130.0
  collected_vespene: 1276.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 335.0
  spent_minerals: 4000.0
  spent_vespene: 1175.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 800.0
    economy: 650.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 2150.0
    technology: 750.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1600.0
    economy: 2350.0
    technology: 750.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 150.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 3857.24560547
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 715.125
    shields: 1069.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 180
vespene: 101
food_cap: 55
food_used: 45
food_army: 23
food_workers: 22
idle_worker_count: 0
army_count: 8
warp_gate_count: 3

game_loop:  6848
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 67
  count: 1
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 75
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 65
    energy: 0
  }
  units {
    unit_type: 80
    player_relative: 1
    health: 114
    shields: 9
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 65
    energy: 0
  }
}

Score:  5581
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
