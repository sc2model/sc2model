----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4555
  player_apm: 187
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5275
  player_apm: 237
}
game_duration_loops: 10325
game_duration_seconds: 460.969665527
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4898
score_details {
  idle_production_time: 9279.8125
  idle_worker_time: 254.25
  total_value_units: 6450.0
  total_value_structures: 1775.0
  killed_value_units: 875.0
  killed_value_structures: 100.0
  collected_minerals: 7385.0
  collected_vespene: 788.0
  collection_rate_minerals: 755.0
  collection_rate_vespene: 0.0
  spent_minerals: 7150.0
  spent_vespene: 775.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 12.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1725.0
    economy: 1850.0
    technology: -75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1100.0
    economy: 1800.0
    technology: 1150.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3275.0
    economy: 4200.0
    technology: 1425.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 820.0
    shields: 2000.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7586.85888672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 285
vespene: 13
food_cap: 62
food_used: 36
food_army: 24
food_workers: 12
idle_worker_count: 0
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 18
}
groups {
  control_group_index: 5
  leader_unit_type: 100
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 17
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 87
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 140
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 0
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 6
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 4038
score_details {
  idle_production_time: 9644.4375
  idle_worker_time: 281.25
  total_value_units: 6550.0
  total_value_structures: 1775.0
  killed_value_units: 1025.0
  killed_value_structures: 100.0
  collected_minerals: 7525.0
  collected_vespene: 788.0
  collection_rate_minerals: 699.0
  collection_rate_vespene: 0.0
  spent_minerals: 7150.0
  spent_vespene: 775.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 12.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2475.0
    economy: 1850.0
    technology: -75.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 250.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 250.0
    economy: 1800.0
    technology: 1150.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3375.0
    economy: 4200.0
    technology: 1425.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 979.25
    shields: 2405.75
    energy: 0.0
  }
  total_damage_taken {
    life: 8664.2421875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 425
vespene: 13
food_cap: 62
food_used: 16
food_army: 4
food_workers: 12
idle_worker_count: 12
army_count: 9
warp_gate_count: 0

game_loop:  10325
ui_data{
 
Score:  4038
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
