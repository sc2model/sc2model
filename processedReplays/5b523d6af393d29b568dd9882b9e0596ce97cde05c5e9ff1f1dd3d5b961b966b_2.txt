----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3001
  player_apm: 100
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3268
  player_apm: 117
}
game_duration_loops: 18708
game_duration_seconds: 835.236877441
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7243
score_details {
  idle_production_time: 1563.25
  idle_worker_time: 1403.75
  total_value_units: 4325.0
  total_value_structures: 3450.0
  killed_value_units: 1700.0
  killed_value_structures: 275.0
  collected_minerals: 6670.0
  collected_vespene: 1448.0
  collection_rate_minerals: 139.0
  collection_rate_vespene: 0.0
  spent_minerals: 6650.0
  spent_vespene: 1125.0
  food_used {
    none: 0.0
    army: 29.0
    economy: 16.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1250.0
    economy: 525.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 700.0
    economy: 900.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 2850.0
    technology: 1225.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2050.0
    economy: 4050.0
    technology: 1350.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 350.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2132.3894043
    shields: 3295.42700195
    energy: 0.0
  }
  total_damage_taken {
    life: 4695.51513672
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 628.9140625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 70
vespene: 323
food_cap: 94
food_used: 45
food_army: 29
food_workers: 16
idle_worker_count: 1
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 14
}
groups {
  control_group_index: 2
  leader_unit_type: 49
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 134
  count: 1
}
single {
  unit {
    unit_type: 32
    player_relative: 1
    health: 56
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 5843
score_details {
  idle_production_time: 4962.4375
  idle_worker_time: 2075.4375
  total_value_units: 7000.0
  total_value_structures: 4025.0
  killed_value_units: 6075.0
  killed_value_structures: 825.0
  collected_minerals: 10860.0
  collected_vespene: 1448.0
  collection_rate_minerals: 1567.0
  collection_rate_vespene: 0.0
  spent_minerals: 10037.0
  spent_vespene: 1350.0
  food_used {
    none: 0.0
    army: 7.0
    economy: 25.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4150.0
    economy: 1375.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 925.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 50.0
    army: 3100.0
    economy: 2900.0
    technology: 462.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 50.0
    army: 400.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 350.0
    economy: 2800.0
    technology: 875.0
    upgrade: 200.0
  }
  used_vespene {
    none: -50.0
    army: 300.0
    economy: 0.0
    technology: 225.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3400.0
    economy: 5950.0
    technology: 1450.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 425.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 6225.38964844
    shields: 6989.39355469
    energy: 0.0
  }
  total_damage_taken {
    life: 17144.4433594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 3150.18359375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 795
vespene: 98
food_cap: 54
food_used: 32
food_army: 7
food_workers: 24
idle_worker_count: 26
army_count: 3
warp_gate_count: 0

game_loop:  18708
ui_data{
 
Score:  5843
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
