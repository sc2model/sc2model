----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2815
  player_apm: 57
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 97
}
game_duration_loops: 29730
game_duration_seconds: 1327.32470703
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7275
score_details {
  idle_production_time: 1923.0
  idle_worker_time: 621.1875
  total_value_units: 2925.0
  total_value_structures: 3750.0
  killed_value_units: 1150.0
  killed_value_structures: 0.0
  collected_minerals: 5905.0
  collected_vespene: 1420.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 313.0
  spent_minerals: 5475.0
  spent_vespene: 550.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 32.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 675.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 300.0
    economy: 350.0
    technology: 450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 775.0
    economy: 3400.0
    technology: 1200.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1075.0
    economy: 3700.0
    technology: 1350.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1499.51953125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1514.0
    shields: 1854.625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 480
vespene: 870
food_cap: 86
food_used: 46
food_army: 14
food_workers: 31
idle_worker_count: 1
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 488
  count: 7
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 14
    shields: 3
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 42
    shields: 0
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 75
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 15926
score_details {
  idle_production_time: 7298.0
  idle_worker_time: 1712.25
  total_value_units: 7535.0
  total_value_structures: 8000.0
  killed_value_units: 5100.0
  killed_value_structures: 0.0
  collected_minerals: 16205.0
  collected_vespene: 5676.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 671.0
  spent_minerals: 15565.0
  spent_vespene: 3450.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 56.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 3175.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1775.0
    economy: 1800.0
    technology: 1650.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2210.0
    economy: 5950.0
    technology: 2550.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 3335.0
    economy: 7350.0
    technology: 3600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 1950.0
    economy: 0.0
    technology: 500.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 5459.59375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6388.52099609
    shields: 7916.51367188
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 690
vespene: 2226
food_cap: 133
food_used: 92
food_army: 36
food_workers: 56
idle_worker_count: 10
army_count: 3
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 67
  count: 4
}
groups {
  control_group_index: 2
  leader_unit_type: 62
  count: 2
}
multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 7132
score_details {
  idle_production_time: 12171.9375
  idle_worker_time: 9726.4375
  total_value_units: 17275.0
  total_value_structures: 10250.0
  killed_value_units: 13700.0
  killed_value_structures: 500.0
  collected_minerals: 22050.0
  collected_vespene: 8052.0
  collection_rate_minerals: 251.0
  collection_rate_vespene: 156.0
  spent_minerals: 21695.0
  spent_vespene: 5850.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 8675.0
    economy: 300.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9375.0
    economy: 6475.0
    technology: 3150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5050.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1775.0
    technology: 2100.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 650.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 9675.0
    economy: 7950.0
    technology: 4950.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 5350.0
    economy: 0.0
    technology: 800.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 15102.1503906
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23233.5488281
    shields: 26674.3535156
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 405
vespene: 2202
food_cap: 23
food_used: 21
food_army: 0
food_workers: 21
idle_worker_count: 9
army_count: 0
warp_gate_count: 0

game_loop:  29730
ui_data{
 
Score:  7132
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
