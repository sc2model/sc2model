----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4397
  player_apm: 110
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4281
  player_apm: 139
}
game_duration_loops: 3304
game_duration_seconds: 147.510299683
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2355
score_details {
  idle_production_time: 448.625
  idle_worker_time: 8.3125
  total_value_units: 1350.0
  total_value_structures: 825.0
  killed_value_units: 75.0
  killed_value_structures: 0.0
  collected_minerals: 1555.0
  collected_vespene: 100.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 22.0
  spent_minerals: 1575.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 6.0
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 50.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 350.0
    economy: 1875.0
    technology: 0.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 50.0
    economy: 2075.0
    technology: 250.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 194.169921875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1315.86962891
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 30
vespene: 100
food_cap: 28
food_used: 24
food_army: 6
food_workers: 18
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  3304
ui_data{
 multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 7
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 7
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 11
    shields: 0
    energy: 0
  }
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
}

Score:  2355
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
