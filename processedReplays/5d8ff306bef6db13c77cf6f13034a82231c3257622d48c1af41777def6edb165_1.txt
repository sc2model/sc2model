----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: -36400
  player_apm: 61
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 81
}
game_duration_loops: 15281
game_duration_seconds: 682.235107422
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12300
score_details {
  idle_production_time: 1971.25
  idle_worker_time: 643.5
  total_value_units: 4400.0
  total_value_structures: 3575.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8580.0
  collected_vespene: 2720.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 671.0
  spent_minerals: 8250.0
  spent_vespene: 2525.0
  food_used {
    none: 0.0
    army: 35.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 1850.0
    economy: 5300.0
    technology: 1575.0
    upgrade: 375.0
  }
  used_vespene {
    none: 50.0
    army: 1250.0
    economy: 0.0
    technology: 850.0
    upgrade: 375.0
  }
  total_used_minerals {
    none: 50.0
    army: 1250.0
    economy: 4700.0
    technology: 1125.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 850.0
    economy: 0.0
    technology: 550.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 24.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 380
vespene: 195
food_cap: 86
food_used: 81
food_army: 35
food_workers: 46
idle_worker_count: 1
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 production {
  unit {
    unit_type: 29
    player_relative: 1
    health: 750
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22438
score_details {
  idle_production_time: 6132.9375
  idle_worker_time: 2257.9375
  total_value_units: 10050.0
  total_value_structures: 6350.0
  killed_value_units: 3775.0
  killed_value_structures: 0.0
  collected_minerals: 15755.0
  collected_vespene: 6508.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 1231.0
  spent_minerals: 13975.0
  spent_vespene: 5425.0
  food_used {
    none: 0.0
    army: 73.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2800.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3750.0
    economy: 7450.0
    technology: 1925.0
    upgrade: 1300.0
  }
  used_vespene {
    none: 50.0
    army: 2475.0
    economy: 300.0
    technology: 925.0
    upgrade: 1300.0
  }
  total_used_minerals {
    none: 50.0
    army: 4250.0
    economy: 7650.0
    technology: 1825.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 50.0
    army: 2850.0
    economy: 600.0
    technology: 925.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 5757.78173828
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1144.88818359
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1830
vespene: 1083
food_cap: 164
food_used: 132
food_army: 73
food_workers: 59
idle_worker_count: 5
army_count: 14
warp_gate_count: 0

game_loop:  15281
ui_data{
 multi {
  units {
    unit_type: 47
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 47
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 47
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 47
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 47
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
  units {
    unit_type: 47
    player_relative: 1
    health: 400
    shields: 0
    energy: 0
  }
}

Score:  22438
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
