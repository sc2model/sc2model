----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4531
  player_apm: 221
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4283
  player_apm: 236
}
game_duration_loops: 12308
game_duration_seconds: 549.502624512
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13139
score_details {
  idle_production_time: 7384.0625
  idle_worker_time: 1.8125
  total_value_units: 8250.0
  total_value_structures: 2250.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 10165.0
  collected_vespene: 2124.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 627.0
  spent_minerals: 10025.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 58.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2575.0
    economy: 5850.0
    technology: 1750.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 250.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 2725.0
    economy: 6400.0
    technology: 1900.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 327.195800781
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 667.226074219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 190
vespene: 549
food_cap: 122
food_used: 113
food_army: 58
food_workers: 53
idle_worker_count: 0
army_count: 36
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 35
}
groups {
  control_group_index: 3
  leader_unit_type: 104
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 89
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 126
  count: 3
}
single {
  unit {
    unit_type: 137
    player_relative: 1
    health: 50
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 16176
score_details {
  idle_production_time: 10945.6875
  idle_worker_time: 1.8125
  total_value_units: 11050.0
  total_value_structures: 2300.0
  killed_value_units: 3900.0
  killed_value_structures: 0.0
  collected_minerals: 13945.0
  collected_vespene: 3656.0
  collection_rate_minerals: 2183.0
  collection_rate_vespene: 940.0
  spent_minerals: 13600.0
  spent_vespene: 3200.0
  food_used {
    none: 0.0
    army: 84.5
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2675.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1350.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3375.0
    economy: 6950.0
    technology: 1750.0
    upgrade: 650.0
  }
  used_vespene {
    none: 150.0
    army: 1400.0
    economy: 0.0
    technology: 250.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 300.0
    army: 4400.0
    economy: 7850.0
    technology: 1900.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 300.0
    army: 2750.0
    economy: 0.0
    technology: 350.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 5047.55322266
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3117.92236328
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 395
vespene: 456
food_cap: 194
food_used: 142
food_army: 84
food_workers: 58
idle_worker_count: 0
army_count: 27
warp_gate_count: 0

game_loop:  12308
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 36
}
groups {
  control_group_index: 3
  leader_unit_type: 104
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 89
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 129
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 126
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 108
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 117
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 88
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 104
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  16176
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
