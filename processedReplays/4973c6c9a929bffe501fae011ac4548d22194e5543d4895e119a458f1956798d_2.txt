----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4757
  player_apm: 197
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5289
  player_apm: 279
}
game_duration_loops: 8633
game_duration_seconds: 385.42868042
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8842
score_details {
  idle_production_time: 6530.875
  idle_worker_time: 4.0625
  total_value_units: 6900.0
  total_value_structures: 1925.0
  killed_value_units: 1650.0
  killed_value_structures: 0.0
  collected_minerals: 8725.0
  collected_vespene: 948.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 492.0
  spent_minerals: 7831.0
  spent_vespene: 700.0
  food_used {
    none: 0.0
    army: 19.5
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1550.0
    economy: -69.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1400.0
    economy: 4575.0
    technology: 900.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 50.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3350.0
    economy: 5175.0
    technology: 900.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1872.31201172
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2121.38378906
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 944
vespene: 248
food_cap: 96
food_used: 60
food_army: 19
food_workers: 41
idle_worker_count: 0
army_count: 7
warp_gate_count: 0

game_loop:  8633
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 5
}
groups {
  control_group_index: 2
  leader_unit_type: 126
  count: 5
}
groups {
  control_group_index: 3
  leader_unit_type: 106
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 4
}
multi {
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 67
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 120
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 68
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 108
  }
  units {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 83
  }
}

Score:  8842
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
