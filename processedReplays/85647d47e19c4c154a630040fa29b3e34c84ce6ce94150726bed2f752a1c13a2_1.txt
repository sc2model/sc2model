----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4221
  player_apm: 173
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4401
  player_apm: 176
}
game_duration_loops: 15076
game_duration_seconds: 673.082702637
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10379
score_details {
  idle_production_time: 7576.4375
  idle_worker_time: 7.0
  total_value_units: 8100.0
  total_value_structures: 1475.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 9455.0
  collected_vespene: 1724.0
  collection_rate_minerals: 1875.0
  collection_rate_vespene: 492.0
  spent_minerals: 8850.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 59.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 650.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1225.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2450.0
    economy: 4725.0
    technology: 900.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3850.0
    economy: 5325.0
    technology: 900.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1844.14013672
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2480.16650391
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 655
vespene: 149
food_cap: 106
food_used: 106
food_army: 59
food_workers: 47
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13678
score_details {
  idle_production_time: 14824.0625
  idle_worker_time: 14.3125
  total_value_units: 16900.0
  total_value_structures: 1750.0
  killed_value_units: 5625.0
  killed_value_structures: 0.0
  collected_minerals: 16530.0
  collected_vespene: 4548.0
  collection_rate_minerals: 1931.0
  collection_rate_vespene: 940.0
  spent_minerals: 16325.0
  spent_vespene: 3975.0
  food_used {
    none: 0.0
    army: 94.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4450.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5500.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3825.0
    economy: 6000.0
    technology: 900.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 1175.0
    economy: 0.0
    technology: 250.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 9750.0
    economy: 6750.0
    technology: 1050.0
    upgrade: 350.0
  }
  total_used_vespene {
    none: 0.0
    army: 3425.0
    economy: 0.0
    technology: 350.0
    upgrade: 350.0
  }
  total_damage_dealt {
    life: 9977.77929688
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10128.8105469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 255
vespene: 573
food_cap: 170
food_used: 146
food_army: 94
food_workers: 52
idle_worker_count: 0
army_count: 33
warp_gate_count: 0

game_loop:  15076
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 116
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 110
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 103
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 16
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 53
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 62
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 118
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 58
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 141
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 60
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 136
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 130
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

Score:  13678
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
