----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3545
  player_apm: 130
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3604
  player_apm: 62
}
game_duration_loops: 13009
game_duration_seconds: 580.799438477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12738
score_details {
  idle_production_time: 8967.875
  idle_worker_time: 240.9375
  total_value_units: 6600.0
  total_value_structures: 2225.0
  killed_value_units: 500.0
  killed_value_structures: 0.0
  collected_minerals: 10005.0
  collected_vespene: 2308.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 671.0
  spent_minerals: 9075.0
  spent_vespene: 900.0
  food_used {
    none: 0.0
    army: 31.5
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 575.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1625.0
    economy: 5550.0
    technology: 1825.0
    upgrade: 450.0
  }
  used_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 200.0
    upgrade: 450.0
  }
  total_used_minerals {
    none: 0.0
    army: 2150.0
    economy: 5700.0
    technology: 1975.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 300.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 515.0
    shields: 304.25
    energy: 0.0
  }
  total_damage_taken {
    life: 1158.04785156
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 980
vespene: 1408
food_cap: 90
food_used: 80
food_army: 31
food_workers: 49
idle_worker_count: 1
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 12
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 89
  count: 4
}
groups {
  control_group_index: 8
  leader_unit_type: 126
  count: 1
}
single {
  unit {
    unit_type: 126
    player_relative: 1
    health: 175
    shields: 0
    energy: 97
  }
}

score{
 score_type: Melee
score: 13419
score_details {
  idle_production_time: 14992.8125
  idle_worker_time: 310.5
  total_value_units: 11200.0
  total_value_structures: 2900.0
  killed_value_units: 2600.0
  killed_value_structures: 0.0
  collected_minerals: 13565.0
  collected_vespene: 3104.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 335.0
  spent_minerals: 13325.0
  spent_vespene: 2275.0
  food_used {
    none: 0.0
    army: 37.5
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2650.0
    economy: 850.0
    technology: 425.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2100.0
    economy: 5400.0
    technology: 1975.0
    upgrade: 975.0
  }
  used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 200.0
    upgrade: 975.0
  }
  total_used_minerals {
    none: 0.0
    army: 4900.0
    economy: 7350.0
    technology: 2550.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 300.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 1954.0
    shields: 1842.25
    energy: 0.0
  }
  total_damage_taken {
    life: 6514.59082031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 290
vespene: 829
food_cap: 136
food_used: 83
food_army: 37
food_workers: 46
idle_worker_count: 0
army_count: 23
warp_gate_count: 0

game_loop:  13009
ui_data{
 multi {
  units {
    unit_type: 104
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 70
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 80
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 119
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 126
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 76
    shields: 0
    energy: 138
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 73
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
}

Score:  13419
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
