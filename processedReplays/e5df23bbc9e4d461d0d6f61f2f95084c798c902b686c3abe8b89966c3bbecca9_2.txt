----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3439
  player_apm: 268
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_apm: 142
}
game_duration_loops: 7794
game_duration_seconds: 347.970703125
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4533
score_details {
  idle_production_time: 5669.4375
  idle_worker_time: 919.0
  total_value_units: 3950.0
  total_value_structures: 1350.0
  killed_value_units: 1100.0
  killed_value_structures: 375.0
  collected_minerals: 4430.0
  collected_vespene: 728.0
  collection_rate_minerals: 783.0
  collection_rate_vespene: 0.0
  spent_minerals: 4350.0
  spent_vespene: 300.0
  food_used {
    none: 0.0
    army: 10.5
    economy: 18.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 525.0
    economy: 775.0
    technology: 50.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 825.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 625.0
    economy: 2150.0
    technology: 950.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 3100.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 50.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 5390.75
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2386.97314453
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 130
vespene: 428
food_cap: 44
food_used: 28
food_army: 10
food_workers: 17
idle_worker_count: 17
army_count: 9
warp_gate_count: 0

game_loop:  7794
ui_data{
 
Score:  4533
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
