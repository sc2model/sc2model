----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4374
  player_apm: 228
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4490
  player_apm: 230
}
game_duration_loops: 42099
game_duration_seconds: 1879.55078125
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6362
score_details {
  idle_production_time: 13330.0
  idle_worker_time: 11.1875
  total_value_units: 5750.0
  total_value_structures: 1425.0
  killed_value_units: 850.0
  killed_value_structures: 400.0
  collected_minerals: 6520.0
  collected_vespene: 692.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 67.0
  spent_minerals: 6175.0
  spent_vespene: 650.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 35.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 400.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1175.0
    economy: 3525.0
    technology: 800.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 150.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 3050.0
    economy: 3825.0
    technology: 950.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 7542.09277344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2382.75830078
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 395
vespene: 42
food_cap: 58
food_used: 53
food_army: 18
food_workers: 35
idle_worker_count: 0
army_count: 20
warp_gate_count: 0
larva_count: 12

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 19
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 4
  leader_unit_type: 126
  count: 2
}
multi {
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19708
score_details {
  idle_production_time: 44795.625
  idle_worker_time: 30.5
  total_value_units: 20450.0
  total_value_structures: 3000.0
  killed_value_units: 5825.0
  killed_value_structures: 400.0
  collected_minerals: 21350.0
  collected_vespene: 5908.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 873.0
  spent_minerals: 21025.0
  spent_vespene: 4825.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 72.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4375.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4175.0
    economy: 1600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4025.0
    economy: 7550.0
    technology: 2250.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 900.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 11400.0
    economy: 9700.0
    technology: 2400.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 3100.0
    economy: 0.0
    technology: 950.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 11553.7773438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 10346.5273438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 375
vespene: 1083
food_cap: 152
food_used: 137
food_army: 65
food_workers: 66
idle_worker_count: 0
army_count: 50
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 9
  count: 50
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 129
  count: 1
}
multi {
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 200
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 9
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 107
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 20870
score_details {
  idle_production_time: 82434.25
  idle_worker_time: 866.125
  total_value_units: 37350.0
  total_value_structures: 4850.0
  killed_value_units: 22525.0
  killed_value_structures: 2450.0
  collected_minerals: 37570.0
  collected_vespene: 11700.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 694.0
  spent_minerals: 37550.0
  spent_vespene: 10800.0
  food_used {
    none: 0.0
    army: 88.0
    economy: 65.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15350.0
    economy: 3100.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5875.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 16425.0
    economy: 2325.0
    technology: 1725.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4350.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5000.0
    economy: 8125.0
    technology: 1425.0
    upgrade: 1025.0
  }
  used_vespene {
    none: 0.0
    army: 2700.0
    economy: 0.0
    technology: 600.0
    upgrade: 1025.0
  }
  total_used_minerals {
    none: 0.0
    army: 24400.0
    economy: 12000.0
    technology: 3450.0
    upgrade: 1025.0
  }
  total_used_vespene {
    none: 0.0
    army: 7025.0
    economy: 0.0
    technology: 1500.0
    upgrade: 1025.0
  }
  total_damage_dealt {
    life: 35051.390625
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 38234.453125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 70
vespene: 900
food_cap: 190
food_used: 153
food_army: 88
food_workers: 65
idle_worker_count: 9
army_count: 73
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 105
  count: 73
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 5
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18674
score_details {
  idle_production_time: 133022.4375
  idle_worker_time: 1970.5625
  total_value_units: 56950.0
  total_value_structures: 5975.0
  killed_value_units: 34325.0
  killed_value_structures: 5400.0
  collected_minerals: 53995.0
  collected_vespene: 15304.0
  collection_rate_minerals: 1959.0
  collection_rate_vespene: 470.0
  spent_minerals: 54025.0
  spent_vespene: 14750.0
  food_used {
    none: 0.0
    army: 61.5
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 22650.0
    economy: 5900.0
    technology: 1600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8925.0
    economy: 150.0
    technology: 500.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 30225.0
    economy: 4575.0
    technology: 1725.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7875.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 4700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2725.0
    economy: 7825.0
    technology: 1850.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 950.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 0.0
    army: 40150.0
    economy: 14350.0
    technology: 4375.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 0.0
    army: 11725.0
    economy: 0.0
    technology: 2300.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 56956.8007812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 64537.671875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 20
vespene: 554
food_cap: 164
food_used: 104
food_army: 61
food_workers: 43
idle_worker_count: 0
army_count: 54
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 112
  count: 54
}
groups {
  control_group_index: 3
  leader_unit_type: 101
  count: 5
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19678
score_details {
  idle_production_time: 144008.75
  idle_worker_time: 1980.3125
  total_value_units: 59250.0
  total_value_structures: 5975.0
  killed_value_units: 39725.0
  killed_value_structures: 9025.0
  collected_minerals: 56510.0
  collected_vespene: 16068.0
  collection_rate_minerals: 1315.0
  collection_rate_vespene: 492.0
  spent_minerals: 56525.0
  spent_vespene: 14750.0
  food_used {
    none: 0.0
    army: 71.5
    economy: 33.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 50.0
    army: 26225.0
    economy: 7950.0
    technology: 3175.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 50.0
    army: 10000.0
    economy: 150.0
    technology: 1150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 31975.0
    economy: 5000.0
    technology: 1725.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7975.0
    economy: 0.0
    technology: 700.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 4700.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 2350.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3175.0
    economy: 7250.0
    technology: 2300.0
    upgrade: 1875.0
  }
  used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 950.0
    upgrade: 1875.0
  }
  total_used_minerals {
    none: 0.0
    army: 42450.0
    economy: 14350.0
    technology: 4375.0
    upgrade: 1875.0
  }
  total_used_vespene {
    none: 0.0
    army: 11725.0
    economy: 0.0
    technology: 2300.0
    upgrade: 1875.0
  }
  total_damage_dealt {
    life: 78740.8984375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 69309.3828125
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 35
vespene: 1318
food_cap: 164
food_used: 104
food_army: 71
food_workers: 33
idle_worker_count: 0
army_count: 81
warp_gate_count: 0

game_loop:  42099
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 112
  count: 81
}
groups {
  control_group_index: 3
  leader_unit_type: 101
  count: 5
}
multi {
  units {
    unit_type: 112
    player_relative: 1
    health: 95
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 192
    shields: 0
    energy: 187
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 123
    shields: 0
    energy: 0
  }
  units {
    unit_type: 129
    player_relative: 1
    health: 134
    shields: 0
    energy: 186
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 445
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 32
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 19
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 10
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 109
    player_relative: 1
    health: 418
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 112
    player_relative: 1
    health: 48
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 12
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 105
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
}

Score:  19678
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
