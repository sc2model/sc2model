----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3740
  player_apm: 197
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3650
  player_apm: 148
}
game_duration_loops: 14485
game_duration_seconds: 646.696899414
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11769
score_details {
  idle_production_time: 1712.875
  idle_worker_time: 303.5625
  total_value_units: 4975.0
  total_value_structures: 4275.0
  killed_value_units: 550.0
  killed_value_structures: 0.0
  collected_minerals: 9135.0
  collected_vespene: 1984.0
  collection_rate_minerals: 1455.0
  collection_rate_vespene: 492.0
  spent_minerals: 8925.0
  spent_vespene: 1825.0
  food_used {
    none: 0.0
    army: 45.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 550.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2225.0
    economy: 5300.0
    technology: 1900.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 525.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2025.0
    economy: 5075.0
    technology: 1425.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 425.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 821.15234375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 152.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 260
vespene: 159
food_cap: 125
food_used: 87
food_army: 45
food_workers: 41
idle_worker_count: 2
army_count: 21
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 55
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 22
  count: 2
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 15826
score_details {
  idle_production_time: 4737.0625
  idle_worker_time: 732.875
  total_value_units: 8975.0
  total_value_structures: 6650.0
  killed_value_units: 8000.0
  killed_value_structures: 125.0
  collected_minerals: 15000.0
  collected_vespene: 4076.0
  collection_rate_minerals: 1819.0
  collection_rate_vespene: 716.0
  spent_minerals: 14200.0
  spent_vespene: 3025.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 47.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4775.0
    economy: 1600.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1625.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2675.0
    economy: 6125.0
    technology: 2750.0
    upgrade: 500.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 150.0
    technology: 725.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 0.0
    army: 5175.0
    economy: 6725.0
    technology: 2750.0
    upgrade: 500.0
  }
  total_used_vespene {
    none: 0.0
    army: 1450.0
    economy: 300.0
    technology: 725.0
    upgrade: 500.0
  }
  total_damage_dealt {
    life: 11777.6621094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2944.67504883
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 974.189697266
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 850
vespene: 1051
food_cap: 156
food_used: 101
food_army: 54
food_workers: 47
idle_worker_count: 0
army_count: 34
warp_gate_count: 0

game_loop:  14485
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 500
  count: 4
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 22
  count: 3
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 500
    player_relative: 1
    health: 90
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 1
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 99
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 18
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 46
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

Score:  15826
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
