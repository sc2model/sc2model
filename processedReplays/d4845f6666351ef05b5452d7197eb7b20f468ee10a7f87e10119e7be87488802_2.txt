----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_apm: 121
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3080
  player_apm: 125
}
game_duration_loops: 37483
game_duration_seconds: 1673.46496582
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11679
score_details {
  idle_production_time: 792.125
  idle_worker_time: 473.0
  total_value_units: 5300.0
  total_value_structures: 3800.0
  killed_value_units: 200.0
  killed_value_structures: 0.0
  collected_minerals: 8540.0
  collected_vespene: 2364.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 694.0
  spent_minerals: 8225.0
  spent_vespene: 1475.0
  food_used {
    none: 0.0
    army: 41.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2375.0
    economy: 5150.0
    technology: 1300.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 300.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 1975.0
    economy: 4700.0
    technology: 1300.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 300.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 342.083496094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 148.0
    shields: 278.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 365
vespene: 889
food_cap: 118
food_used: 92
food_army: 41
food_workers: 49
idle_worker_count: 3
army_count: 15
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 11
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 1000
    shields: 1000
    energy: 0
  }
  units {
    unit_type: 59
    player_relative: 1
    health: 387
    shields: 387
    energy: 0
    build_progress: 0.318125009537
  }
}

score{
 score_type: Melee
score: 17161
score_details {
  idle_production_time: 5149.125
  idle_worker_time: 3792.3125
  total_value_units: 21400.0
  total_value_structures: 6500.0
  killed_value_units: 8625.0
  killed_value_structures: 0.0
  collected_minerals: 21805.0
  collected_vespene: 7556.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 851.0
  spent_minerals: 21650.0
  spent_vespene: 7250.0
  food_used {
    none: 0.0
    army: 56.0
    economy: 68.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5800.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9450.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3175.0
    economy: 7050.0
    technology: 2550.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 700.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 13325.0
    economy: 6700.0
    technology: 2550.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 7775.0
    economy: 0.0
    technology: 700.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 8414.5078125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 7252.75976562
    shields: 7249.99414062
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 205
vespene: 306
food_cap: 173
food_used: 124
food_army: 56
food_workers: 68
idle_worker_count: 21
army_count: 21
warp_gate_count: 8

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 20
}
groups {
  control_group_index: 3
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 20788
score_details {
  idle_production_time: 11430.1875
  idle_worker_time: 10369.1875
  total_value_units: 34375.0
  total_value_structures: 8075.0
  killed_value_units: 23000.0
  killed_value_structures: 0.0
  collected_minerals: 34685.0
  collected_vespene: 12828.0
  collection_rate_minerals: 447.0
  collection_rate_vespene: 582.0
  spent_minerals: 34375.0
  spent_vespene: 12275.0
  food_used {
    none: 0.0
    army: 71.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 14975.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 8025.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 18100.0
    economy: 1800.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4275.0
    economy: 7175.0
    technology: 2950.0
    upgrade: 1400.0
  }
  used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 900.0
    upgrade: 1400.0
  }
  total_used_minerals {
    none: 0.0
    army: 22975.0
    economy: 8575.0
    technology: 2550.0
    upgrade: 1400.0
  }
  total_used_vespene {
    none: 0.0
    army: 12600.0
    economy: 0.0
    technology: 700.0
    upgrade: 1400.0
  }
  total_damage_dealt {
    life: 24791.4707031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 15267.8398438
    shields: 17505.2441406
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 360
vespene: 553
food_cap: 200
food_used: 117
food_army: 71
food_workers: 46
idle_worker_count: 4
army_count: 22
warp_gate_count: 8

game_loop:  30000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 19
}
groups {
  control_group_index: 4
  leader_unit_type: 133
  count: 4
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 4
}
multi {
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 173
    shields: 100
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 150
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 137
    shields: 50
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 83
    player_relative: 1
    health: 200
    shields: 100
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 68
    shields: 53
    energy: 0
  }
  units {
    unit_type: 74
    player_relative: 1
    health: 80
    shields: 80
    energy: 0
  }
  units {
    unit_type: 4
    player_relative: 1
    health: 200
    shields: 128
    energy: 0
  }
  units {
    unit_type: 82
    player_relative: 1
    health: 8
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 16537
score_details {
  idle_production_time: 17542.5625
  idle_worker_time: 12397.875
  total_value_units: 42725.0
  total_value_structures: 9625.0
  killed_value_units: 37850.0
  killed_value_structures: 0.0
  collected_minerals: 40765.0
  collected_vespene: 14972.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 179.0
  spent_minerals: 40100.0
  spent_vespene: 14575.0
  food_used {
    none: 0.0
    army: 25.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 24950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 12900.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 25100.0
    economy: 4325.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 11275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1325.0
    economy: 6675.0
    technology: 2950.0
    upgrade: 1550.0
  }
  used_vespene {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 900.0
    upgrade: 1550.0
  }
  total_used_minerals {
    none: 0.0
    army: 27925.0
    economy: 11075.0
    technology: 2950.0
    upgrade: 1550.0
  }
  total_used_vespene {
    none: 0.0
    army: 16250.0
    economy: 0.0
    technology: 900.0
    upgrade: 1550.0
  }
  total_damage_dealt {
    life: 38129.1367188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 23373.8652344
    shields: 26180.1777344
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 715
vespene: 397
food_cap: 200
food_used: 65
food_army: 25
food_workers: 40
idle_worker_count: 7
army_count: 9
warp_gate_count: 8

game_loop:  37483
ui_data{
 
Score:  16537
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
