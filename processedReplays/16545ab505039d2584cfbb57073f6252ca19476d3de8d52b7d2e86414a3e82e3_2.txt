----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3313
  player_apm: 247
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3312
  player_apm: 225
}
game_duration_loops: 8341
game_duration_seconds: 372.392059326
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6882
score_details {
  idle_production_time: 1226.875
  idle_worker_time: 107.25
  total_value_units: 4250.0
  total_value_structures: 3000.0
  killed_value_units: 3150.0
  killed_value_structures: 75.0
  collected_minerals: 6430.0
  collected_vespene: 152.0
  collection_rate_minerals: 1119.0
  collection_rate_vespene: 0.0
  spent_minerals: 6200.0
  spent_vespene: 150.0
  food_used {
    none: 0.0
    army: 50.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2150.0
    economy: 1075.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2600.0
    economy: 2550.0
    technology: 1250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 3000.0
    economy: 2900.0
    technology: 1250.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 5899.39208984
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1008.25
    shields: 1553.75
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 280
vespene: 2
food_cap: 78
food_used: 70
food_army: 50
food_workers: 20
idle_worker_count: 0
army_count: 25
warp_gate_count: 6

game_loop:  8341
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 133
  count: 6
}
groups {
  control_group_index: 4
  leader_unit_type: 73
  count: 22
}
groups {
  control_group_index: 5
  leader_unit_type: 136
  count: 1
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 40
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 84
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 50
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 23
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 19
    shields: 23
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 64
    shields: 39
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 3
    shields: 40
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 96
    shields: 8
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 76
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 40
    energy: 0
  }
}

Score:  6882
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
