----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 2916
  player_apm: 75
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2944
  player_apm: 82
}
game_duration_loops: 31796
game_duration_seconds: 1419.56335449
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11237
score_details {
  idle_production_time: 1567.8125
  idle_worker_time: 393.6875
  total_value_units: 4000.0
  total_value_structures: 4000.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 8385.0
  collected_vespene: 1752.0
  collection_rate_minerals: 2631.0
  collection_rate_vespene: 537.0
  spent_minerals: 7075.0
  spent_vespene: 1550.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 1050.0
    economy: 4150.0
    technology: 2250.0
    upgrade: 475.0
  }
  used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 425.0
    upgrade: 475.0
  }
  total_used_minerals {
    none: 0.0
    army: 1100.0
    economy: 4550.0
    technology: 1900.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 650.0
    economy: 0.0
    technology: 400.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 82.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 159.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1360
vespene: 202
food_cap: 78
food_used: 64
food_army: 21
food_workers: 43
idle_worker_count: 1
army_count: 7
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 6
}
single {
  unit {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 22643
score_details {
  idle_production_time: 6565.0
  idle_worker_time: 1741.0
  total_value_units: 13875.0
  total_value_structures: 7075.0
  killed_value_units: 3375.0
  killed_value_structures: 0.0
  collected_minerals: 22380.0
  collected_vespene: 6988.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 515.0
  spent_minerals: 20175.0
  spent_vespene: 5275.0
  food_used {
    none: 0.0
    army: 69.0
    economy: 36.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2600.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5250.0
    economy: 1300.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 450.0
    army: 3450.0
    economy: 5800.0
    technology: 2850.0
    upgrade: 1725.0
  }
  used_vespene {
    none: 50.0
    army: 2000.0
    economy: 0.0
    technology: 625.0
    upgrade: 1725.0
  }
  total_used_minerals {
    none: 50.0
    army: 8300.0
    economy: 7150.0
    technology: 2800.0
    upgrade: 1475.0
  }
  total_used_vespene {
    none: 50.0
    army: 2875.0
    economy: 0.0
    technology: 625.0
    upgrade: 1475.0
  }
  total_damage_dealt {
    life: 4660.15527344
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 8563.64550781
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 806.381347656
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2255
vespene: 1713
food_cap: 200
food_used: 105
food_army: 69
food_workers: 36
idle_worker_count: 14
army_count: 28
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 10
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 958
    shields: 0
    energy: 0
    build_progress: 0.952884614468
  }
}

score{
 score_type: Melee
score: 29407
score_details {
  idle_production_time: 14592.25
  idle_worker_time: 8501.6875
  total_value_units: 24575.0
  total_value_structures: 7525.0
  killed_value_units: 18775.0
  killed_value_structures: 4625.0
  collected_minerals: 35905.0
  collected_vespene: 8852.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 179.0
  spent_minerals: 30375.0
  spent_vespene: 6725.0
  food_used {
    none: 0.0
    army: 110.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12050.0
    economy: 4425.0
    technology: 1500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4575.0
    economy: 0.0
    technology: 850.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11700.0
    economy: 2000.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5800.0
    economy: 6200.0
    technology: 2750.0
    upgrade: 2075.0
  }
  used_vespene {
    none: 50.0
    army: 1925.0
    economy: 0.0
    technology: 775.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 50.0
    army: 17350.0
    economy: 8350.0
    technology: 3100.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 50.0
    army: 3325.0
    economy: 0.0
    technology: 775.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 37963.8085938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 22312.5898438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 6014.83398438
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 5580
vespene: 2127
food_cap: 181
food_used: 165
food_army: 110
food_workers: 55
idle_worker_count: 11
army_count: 82
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 9
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 28
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 38
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 31
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 8
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 31295
score_details {
  idle_production_time: 16187.1875
  idle_worker_time: 9680.0625
  total_value_units: 26475.0
  total_value_structures: 7775.0
  killed_value_units: 24250.0
  killed_value_structures: 5575.0
  collected_minerals: 38620.0
  collected_vespene: 9100.0
  collection_rate_minerals: 1763.0
  collection_rate_vespene: 246.0
  spent_minerals: 31725.0
  spent_vespene: 6725.0
  food_used {
    none: 0.0
    army: 105.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 15200.0
    economy: 5700.0
    technology: 1700.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6250.0
    economy: 0.0
    technology: 975.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13150.0
    economy: 2000.0
    technology: 350.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 50.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 5550.0
    economy: 6550.0
    technology: 2750.0
    upgrade: 2075.0
  }
  used_vespene {
    none: 50.0
    army: 2100.0
    economy: 0.0
    technology: 775.0
    upgrade: 2075.0
  }
  total_used_minerals {
    none: 50.0
    army: 18750.0
    economy: 8600.0
    technology: 3100.0
    upgrade: 2075.0
  }
  total_used_vespene {
    none: 50.0
    army: 3825.0
    economy: 0.0
    technology: 775.0
    upgrade: 2075.0
  }
  total_damage_dealt {
    life: 46811.1171875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 24367.3183594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 7352.08398438
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 6945
vespene: 2375
food_cap: 189
food_used: 160
food_army: 105
food_workers: 55
idle_worker_count: 10
army_count: 78
warp_gate_count: 0

game_loop:  31796
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 7
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 9
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 132
    shields: 0
    energy: 1
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 68
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 49
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 144
    shields: 0
    energy: 50
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 47
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 132
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 107
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 103
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 43
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 120
    shields: 0
    energy: 58
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 67
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 56
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 24
    shields: 0
    energy: 2
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  31295
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
