----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5217
  player_apm: 288
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 5084
  player_apm: 258
}
game_duration_loops: 3459
game_duration_seconds: 154.430419922
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 2913
score_details {
  idle_production_time: 255.3125
  idle_worker_time: 61.3125
  total_value_units: 850.0
  total_value_structures: 1400.0
  killed_value_units: 50.0
  killed_value_structures: 100.0
  collected_minerals: 2025.0
  collected_vespene: 0.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 0.0
  spent_minerals: 1912.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 17.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 25.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1550.0
    technology: 1200.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 1650.0
    technology: 600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 872.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 266.375732422
    shields: 371.174316406
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 163
vespene: 0
food_cap: 39
food_used: 17
food_army: 0
food_workers: 17
idle_worker_count: 2
army_count: 0
warp_gate_count: 0

game_loop:  3459
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 16
    shields: 4
    energy: 0
  }
}

Score:  2913
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
