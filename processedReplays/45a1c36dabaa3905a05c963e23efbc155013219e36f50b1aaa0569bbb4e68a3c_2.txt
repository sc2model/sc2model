----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3242
  player_apm: 71
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2353
  player_apm: 44
}
game_duration_loops: 60170
game_duration_seconds: 2686.34814453
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8586
score_details {
  idle_production_time: 1817.4375
  idle_worker_time: 277.875
  total_value_units: 3275.0
  total_value_structures: 2775.0
  killed_value_units: 1125.0
  killed_value_structures: 0.0
  collected_minerals: 5835.0
  collected_vespene: 1976.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 313.0
  spent_minerals: 4475.0
  spent_vespene: 975.0
  food_used {
    none: 0.0
    army: 21.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1050.0
    economy: 3400.0
    technology: 825.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 3350.0
    technology: 825.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 675.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1449.26855469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 196.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1410
vespene: 1001
food_cap: 86
food_used: 50
food_army: 21
food_workers: 28
idle_worker_count: 1
army_count: 10
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 18
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 3
}
cargo {
  unit {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 185
    transport_slots_taken: 4
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  passengers {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  slots_available: 4
}

score{
 score_type: Melee
score: 19827
score_details {
  idle_production_time: 7637.75
  idle_worker_time: 1224.9375
  total_value_units: 8100.0
  total_value_structures: 7800.0
  killed_value_units: 1125.0
  killed_value_structures: 0.0
  collected_minerals: 18650.0
  collected_vespene: 6552.0
  collection_rate_minerals: 2211.0
  collection_rate_vespene: 627.0
  spent_minerals: 17600.0
  spent_vespene: 5950.0
  food_used {
    none: 0.0
    army: 67.0
    economy: 68.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 25.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 3550.0
    economy: 7300.0
    technology: 3150.0
    upgrade: 400.0
  }
  used_vespene {
    none: 50.0
    army: 2025.0
    economy: 0.0
    technology: 1100.0
    upgrade: 400.0
  }
  total_used_minerals {
    none: 50.0
    army: 2900.0
    economy: 7750.0
    technology: 3150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 1800.0
    economy: 0.0
    technology: 1100.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 2201.26855469
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 546.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1100
vespene: 602
food_cap: 189
food_used: 135
food_army: 67
food_workers: 68
idle_worker_count: 0
army_count: 18
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 55
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 7
}
single {
  unit {
    unit_type: 132
    player_relative: 1
    health: 1500
    shields: 0
    energy: 101
  }
}

score{
 score_type: Melee
score: 41604
score_details {
  idle_production_time: 9593.6875
  idle_worker_time: 2228.8125
  total_value_units: 17950.0
  total_value_structures: 9875.0
  killed_value_units: 3800.0
  killed_value_structures: 125.0
  collected_minerals: 33930.0
  collected_vespene: 13524.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 940.0
  spent_minerals: 25475.0
  spent_vespene: 10400.0
  food_used {
    none: 0.0
    army: 122.0
    economy: 73.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1450.0
    economy: 1100.0
    technology: 125.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2850.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 7100.0
    economy: 7800.0
    technology: 4875.0
    upgrade: 2300.0
  }
  used_vespene {
    none: 50.0
    army: 4300.0
    economy: 0.0
    technology: 1200.0
    upgrade: 2300.0
  }
  total_used_minerals {
    none: 50.0
    army: 9050.0
    economy: 8250.0
    technology: 4875.0
    upgrade: 2125.0
  }
  total_used_vespene {
    none: 50.0
    army: 5250.0
    economy: 0.0
    technology: 1200.0
    upgrade: 2125.0
  }
  total_damage_dealt {
    life: 5393.04296875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2973.64404297
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 8505
vespene: 3124
food_cap: 197
food_used: 195
food_army: 122
food_workers: 73
idle_worker_count: 5
army_count: 45
warp_gate_count: 0

game_loop:  30000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 32
  count: 10
}
groups {
  control_group_index: 8
  leader_unit_type: 35
  count: 5
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 7
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 57
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 27
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 124
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 125
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 25864
score_details {
  idle_production_time: 14658.8125
  idle_worker_time: 11942.625
  total_value_units: 27025.0
  total_value_structures: 11725.0
  killed_value_units: 21350.0
  killed_value_structures: 5075.0
  collected_minerals: 39370.0
  collected_vespene: 15944.0
  collection_rate_minerals: 1175.0
  collection_rate_vespene: 291.0
  spent_minerals: 31700.0
  spent_vespene: 12525.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 11875.0
    economy: 3050.0
    technology: 3225.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 6525.0
    economy: 0.0
    technology: 1150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 13250.0
    economy: 4125.0
    technology: 2725.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 7300.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 1150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 725.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 850.0
    economy: 5425.0
    technology: 2050.0
    upgrade: 2475.0
  }
  used_vespene {
    none: 50.0
    army: 475.0
    economy: 0.0
    technology: 875.0
    upgrade: 2475.0
  }
  total_used_minerals {
    none: 50.0
    army: 15000.0
    economy: 10000.0
    technology: 4925.0
    upgrade: 2475.0
  }
  total_used_vespene {
    none: 50.0
    army: 8375.0
    economy: 0.0
    technology: 1250.0
    upgrade: 2475.0
  }
  total_damage_dealt {
    life: 38442.2539062
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 35376.7539062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 7720
vespene: 3419
food_cap: 111
food_used: 74
food_army: 14
food_workers: 60
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  40000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 4
}
single {
  unit {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 21013
score_details {
  idle_production_time: 18015.625
  idle_worker_time: 22714.375
  total_value_units: 32175.0
  total_value_structures: 14825.0
  killed_value_units: 26750.0
  killed_value_structures: 5425.0
  collected_minerals: 41370.0
  collected_vespene: 16392.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 38637.0
  spent_vespene: 15137.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 28.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 13975.0
    economy: 3700.0
    technology: 3225.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 9525.0
    economy: 0.0
    technology: 1150.0
    upgrade: 0.0
  }
  lost_minerals {
    none: -38.0
    army: 14400.0
    economy: 7950.0
    technology: 3925.0
    upgrade: 0.0
  }
  lost_vespene {
    none: -38.0
    army: 7875.0
    economy: 0.0
    technology: 825.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 1150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 250.0
    army: 4000.0
    economy: 3500.0
    technology: 1375.0
    upgrade: 2475.0
  }
  used_vespene {
    none: 250.0
    army: 2000.0
    economy: 0.0
    technology: 650.0
    upgrade: 2475.0
  }
  total_used_minerals {
    none: 250.0
    army: 18500.0
    economy: 11900.0
    technology: 5450.0
    upgrade: 2475.0
  }
  total_used_vespene {
    none: 250.0
    army: 10025.0
    economy: 0.0
    technology: 1525.0
    upgrade: 2475.0
  }
  total_damage_dealt {
    life: 44365.8203125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 53983.7539062
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 2783
vespene: 1255
food_cap: 127
food_used: 88
food_army: 60
food_workers: 28
idle_worker_count: 27
army_count: 25
warp_gate_count: 0

game_loop:  50000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 134
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 134
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 34
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 6
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8213
score_details {
  idle_production_time: 20716.4375
  idle_worker_time: 33359.1875
  total_value_units: 35875.0
  total_value_structures: 15275.0
  killed_value_units: 29525.0
  killed_value_structures: 8075.0
  collected_minerals: 41370.0
  collected_vespene: 16392.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 40437.0
  spent_vespene: 15887.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 14750.0
    economy: 5950.0
    technology: 5075.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 9625.0
    economy: 0.0
    technology: 1600.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 12.0
    army: 19600.0
    economy: 10700.0
    technology: 5875.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 12.0
    army: 10525.0
    economy: 0.0
    technology: 1700.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 1150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 150.0
    economy: 750.0
    technology: 325.0
    upgrade: 2475.0
  }
  used_vespene {
    none: 200.0
    army: 75.0
    economy: 0.0
    technology: 75.0
    upgrade: 2475.0
  }
  total_used_minerals {
    none: 250.0
    army: 20900.0
    economy: 11900.0
    technology: 5900.0
    upgrade: 2475.0
  }
  total_used_vespene {
    none: 250.0
    army: 11325.0
    economy: 0.0
    technology: 1525.0
    upgrade: 2475.0
  }
  total_damage_dealt {
    life: 59884.8007812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 77986.4296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 983
vespene: 505
food_cap: 15
food_used: 3
food_army: 2
food_workers: 1
idle_worker_count: 1
army_count: 1
warp_gate_count: 0

game_loop:  60000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 134
  count: 1
}
groups {
  control_group_index: 1
  leader_unit_type: 134
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 8113
score_details {
  idle_production_time: 20748.3125
  idle_worker_time: 33360.5625
  total_value_units: 35875.0
  total_value_structures: 15275.0
  killed_value_units: 29525.0
  killed_value_structures: 8075.0
  collected_minerals: 41370.0
  collected_vespene: 16392.0
  collection_rate_minerals: 0.0
  collection_rate_vespene: 0.0
  spent_minerals: 40637.0
  spent_vespene: 15887.0
  food_used {
    none: 0.0
    army: 2.0
    economy: 1.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 300.0
    army: 14750.0
    economy: 5950.0
    technology: 5075.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 250.0
    army: 9625.0
    economy: 0.0
    technology: 1600.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 12.0
    army: 19600.0
    economy: 10700.0
    technology: 5875.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 12.0
    army: 10525.0
    economy: 0.0
    technology: 1700.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 50.0
    army: 1150.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  used_minerals {
    none: 200.0
    army: 150.0
    economy: 850.0
    technology: 325.0
    upgrade: 2475.0
  }
  used_vespene {
    none: 200.0
    army: 75.0
    economy: 0.0
    technology: 75.0
    upgrade: 2475.0
  }
  total_used_minerals {
    none: 250.0
    army: 20900.0
    economy: 11900.0
    technology: 5900.0
    upgrade: 2475.0
  }
  total_used_vespene {
    none: 250.0
    army: 11325.0
    economy: 0.0
    technology: 1525.0
    upgrade: 2475.0
  }
  total_damage_dealt {
    life: 59884.8007812
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 77986.4296875
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 5.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 783
vespene: 505
food_cap: 15
food_used: 3
food_army: 2
food_workers: 1
idle_worker_count: 0
army_count: 1
warp_gate_count: 0

game_loop:  60170
ui_data{
 
Score:  8113
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
