----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4458
  player_apm: 93
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4586
  player_apm: 109
}
game_duration_loops: 5236
game_duration_seconds: 233.766311646
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4691
score_details {
  idle_production_time: 97.1875
  idle_worker_time: 111.75
  total_value_units: 2200.0
  total_value_structures: 1450.0
  killed_value_units: 825.0
  killed_value_structures: 0.0
  collected_minerals: 3015.0
  collected_vespene: 876.0
  collection_rate_minerals: 867.0
  collection_rate_vespene: 313.0
  spent_minerals: 2675.0
  spent_vespene: 750.0
  food_used {
    none: 0.0
    army: 15.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 350.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 750.0
    economy: 2300.0
    technology: 475.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 600.0
    economy: 2450.0
    technology: 350.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1055.19677734
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 205.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 390
vespene: 126
food_cap: 47
food_used: 39
food_army: 15
food_workers: 24
idle_worker_count: 1
army_count: 3
warp_gate_count: 0

game_loop:  5236
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 46
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 27
  count: 1
}
multi {
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
}

Score:  4691
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
