----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3971
  player_apm: 82
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3902
  player_apm: 195
}
game_duration_loops: 13994
game_duration_seconds: 624.775756836
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 12827
score_details {
  idle_production_time: 1164.625
  idle_worker_time: 480.5625
  total_value_units: 6025.0
  total_value_structures: 3950.0
  killed_value_units: 100.0
  killed_value_structures: 0.0
  collected_minerals: 9375.0
  collected_vespene: 2352.0
  collection_rate_minerals: 2127.0
  collection_rate_vespene: 649.0
  spent_minerals: 9350.0
  spent_vespene: 1775.0
  food_used {
    none: 0.0
    army: 54.0
    economy: 53.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2750.0
    economy: 5650.0
    technology: 1550.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 975.0
    economy: 0.0
    technology: 450.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 2500.0
    economy: 5250.0
    technology: 1400.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 975.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 142.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 60.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 13.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 75
vespene: 577
food_cap: 110
food_used: 107
food_army: 54
food_workers: 51
idle_worker_count: 2
army_count: 35
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 23
}
groups {
  control_group_index: 2
  leader_unit_type: 45
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
}

score{
 score_type: Melee
score: 15393
score_details {
  idle_production_time: 2813.3125
  idle_worker_time: 1083.5
  total_value_units: 10150.0
  total_value_structures: 5200.0
  killed_value_units: 7375.0
  killed_value_structures: 575.0
  collected_minerals: 14580.0
  collected_vespene: 4288.0
  collection_rate_minerals: 1287.0
  collection_rate_vespene: 649.0
  spent_minerals: 14125.0
  spent_vespene: 2850.0
  food_used {
    none: 0.0
    army: 66.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5150.0
    economy: 1675.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2250.0
    economy: 1150.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 3450.0
    economy: 5050.0
    technology: 1750.0
    upgrade: 575.0
  }
  used_vespene {
    none: 50.0
    army: 1500.0
    economy: 0.0
    technology: 450.0
    upgrade: 575.0
  }
  total_used_minerals {
    none: 50.0
    army: 5450.0
    economy: 6550.0
    technology: 1750.0
    upgrade: 575.0
  }
  total_used_vespene {
    none: 50.0
    army: 1650.0
    economy: 0.0
    technology: 450.0
    upgrade: 575.0
  }
  total_damage_dealt {
    life: 10760.3710938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5254.87011719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1108.47412109
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 505
vespene: 1438
food_cap: 141
food_used: 107
food_army: 66
food_workers: 41
idle_worker_count: 2
army_count: 38
warp_gate_count: 0

game_loop:  13994
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 32
  count: 36
}
groups {
  control_group_index: 3
  leader_unit_type: 21
  count: 5
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 35
    player_relative: 1
    health: 109
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 51
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 168
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 24
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 126
    shields: 0
    energy: 69
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 23
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 21
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 142
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 26
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 157
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 57
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 35
    player_relative: 1
    health: 29
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 51
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 32
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 15
    shields: 0
    energy: 0
  }
}

Score:  15393
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
