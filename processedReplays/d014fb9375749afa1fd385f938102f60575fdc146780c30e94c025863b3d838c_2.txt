----------------------- Replay info ------------------------
map_name: "Catallena LE (Void)"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3491
  player_apm: 78
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3628
  player_apm: 180
}
game_duration_loops: 10784
game_duration_seconds: 481.462158203
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13070
score_details {
  idle_production_time: 3534.625
  idle_worker_time: 37.3125
  total_value_units: 10250.0
  total_value_structures: 2075.0
  killed_value_units: 2150.0
  killed_value_structures: 350.0
  collected_minerals: 11720.0
  collected_vespene: 1800.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 470.0
  spent_minerals: 11575.0
  spent_vespene: 1800.0
  food_used {
    none: 0.0
    army: 88.0
    economy: 49.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1700.0
    economy: 250.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 950.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3850.0
    economy: 5725.0
    technology: 1350.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 100.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 4025.0
    economy: 7375.0
    technology: 1500.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2313.22119141
    shields: 2885.05761719
    energy: 0.0
  }
  total_damage_taken {
    life: 3614.92919922
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 195
vespene: 0
food_cap: 178
food_used: 137
food_army: 88
food_workers: 49
idle_worker_count: 0
army_count: 38
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 33
}
groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 33
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13948
score_details {
  idle_production_time: 4064.25
  idle_worker_time: 37.3125
  total_value_units: 11450.0
  total_value_structures: 2075.0
  killed_value_units: 5075.0
  killed_value_structures: 1200.0
  collected_minerals: 12935.0
  collected_vespene: 2088.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 492.0
  spent_minerals: 12325.0
  spent_vespene: 1925.0
  food_used {
    none: 0.0
    army: 88.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2750.0
    economy: 1700.0
    technology: 600.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 825.0
    economy: 950.0
    technology: 125.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3850.0
    economy: 5975.0
    technology: 1350.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 100.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 4925.0
    economy: 7375.0
    technology: 1500.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 1775.0
    economy: 0.0
    technology: 200.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 5612.83789062
    shields: 6270.07519531
    energy: 0.0
  }
  total_damage_taken {
    life: 4941.99072266
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 660
vespene: 163
food_cap: 178
food_used: 136
food_army: 88
food_workers: 48
idle_worker_count: 0
army_count: 45
warp_gate_count: 0

game_loop:  10784
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 28
}
groups {
  control_group_index: 2
  leader_unit_type: 110
  count: 28
}
groups {
  control_group_index: 4
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
single {
  unit {
    unit_type: 129
    player_relative: 1
    health: 200
    shields: 0
    energy: 79
  }
}

Score:  13948
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
