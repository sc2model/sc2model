----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3741
  player_apm: 184
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3777
  player_apm: 176
}
game_duration_loops: 19531
game_duration_seconds: 871.98046875
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7952
score_details {
  idle_production_time: 1016.5625
  idle_worker_time: 686.125
  total_value_units: 4050.0
  total_value_structures: 3200.0
  killed_value_units: 1475.0
  killed_value_structures: 350.0
  collected_minerals: 6625.0
  collected_vespene: 2020.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 313.0
  spent_minerals: 6587.0
  spent_vespene: 1406.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 29.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1075.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1050.0
    economy: 50.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 1425.0
    economy: 3400.0
    technology: 1050.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 575.0
    economy: 0.0
    technology: 400.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 2075.0
    economy: 3400.0
    technology: 1050.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 50.0
    army: 525.0
    economy: 0.0
    technology: 400.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4219.41992188
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1445.82641602
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 50.8447265625
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 88
vespene: 614
food_cap: 86
food_used: 59
food_army: 30
food_workers: 28
idle_worker_count: 2
army_count: 15
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 54
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
single {
  unit {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 19543
score_details {
  idle_production_time: 5434.9375
  idle_worker_time: 2020.0
  total_value_units: 13000.0
  total_value_structures: 7725.0
  killed_value_units: 4050.0
  killed_value_structures: 875.0
  collected_minerals: 19265.0
  collected_vespene: 5684.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 604.0
  spent_minerals: 17712.0
  spent_vespene: 4581.0
  food_used {
    none: 0.0
    army: 59.0
    economy: 51.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2750.0
    economy: 1425.0
    technology: 450.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4650.0
    economy: 50.0
    technology: -38.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1350.0
    economy: 0.0
    technology: -19.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 50.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 25.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2875.0
    economy: 7250.0
    technology: 2875.0
    upgrade: 650.0
  }
  used_vespene {
    none: 50.0
    army: 1400.0
    economy: 150.0
    technology: 900.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 50.0
    army: 7625.0
    economy: 7350.0
    technology: 2875.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 50.0
    army: 2775.0
    economy: 300.0
    technology: 900.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 10346.8183594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6718.45019531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2262.88964844
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1596
vespene: 1097
food_cap: 200
food_used: 110
food_army: 59
food_workers: 51
idle_worker_count: 6
army_count: 19
warp_gate_count: 0

game_loop:  19531
ui_data{
 
Score:  19543
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
