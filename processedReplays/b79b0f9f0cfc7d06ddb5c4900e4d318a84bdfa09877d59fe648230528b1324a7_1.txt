----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2753
  player_apm: 92
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 2384
  player_apm: 58
}
game_duration_loops: 16500
game_duration_seconds: 736.658569336
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10965
score_details {
  idle_production_time: 1540.9375
  idle_worker_time: 429.1875
  total_value_units: 3650.0
  total_value_structures: 3800.0
  killed_value_units: 750.0
  killed_value_structures: 0.0
  collected_minerals: 9035.0
  collected_vespene: 2380.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 559.0
  spent_minerals: 7550.0
  spent_vespene: 1100.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 625.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 500.0
    economy: 4550.0
    technology: 2250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1200.0
    economy: 4050.0
    technology: 1650.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 250.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1318.17480469
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 840.0
    shields: 917.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1535
vespene: 1280
food_cap: 94
food_used: 51
food_army: 8
food_workers: 43
idle_worker_count: 3
army_count: 4
warp_gate_count: 4

game_loop:  10000
ui_data{
 multi {
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
  units {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 11089
score_details {
  idle_production_time: 4840.6875
  idle_worker_time: 764.5
  total_value_units: 13950.0
  total_value_structures: 6850.0
  killed_value_units: 6550.0
  killed_value_structures: 400.0
  collected_minerals: 16015.0
  collected_vespene: 5124.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 335.0
  spent_minerals: 15000.0
  spent_vespene: 4900.0
  food_used {
    none: 0.0
    army: 36.0
    economy: 23.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4875.0
    economy: 250.0
    technology: 400.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 1425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5400.0
    economy: 2000.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2150.0
    economy: 3550.0
    technology: 2250.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 700.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 7550.0
    economy: 5550.0
    technology: 2850.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 4150.0
    economy: 0.0
    technology: 700.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 12594.8320312
    shields: 50.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6868.75
    shields: 8729.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1065
vespene: 224
food_cap: 117
food_used: 59
food_army: 36
food_workers: 23
idle_worker_count: 1
army_count: 14
warp_gate_count: 8

game_loop:  16500
ui_data{
 
Score:  11089
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
