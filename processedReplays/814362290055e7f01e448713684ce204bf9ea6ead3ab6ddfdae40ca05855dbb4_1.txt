----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3971
  player_apm: 187
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4040
  player_apm: 118
}
game_duration_loops: 8030
game_duration_seconds: 358.507171631
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6859
score_details {
  idle_production_time: 576.5
  idle_worker_time: 39.1875
  total_value_units: 3700.0
  total_value_structures: 2450.0
  killed_value_units: 2775.0
  killed_value_structures: 650.0
  collected_minerals: 5125.0
  collected_vespene: 1600.0
  collection_rate_minerals: 951.0
  collection_rate_vespene: 335.0
  spent_minerals: 4350.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 23.0
    economy: 27.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1400.0
    economy: 1350.0
    technology: 300.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 575.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 216.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1150.0
    economy: 2950.0
    technology: 650.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1700.0
    economy: 3150.0
    technology: 650.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 6852.05908203
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1139.74072266
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 234.693847656
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 753
vespene: 606
food_cap: 87
food_used: 50
food_army: 23
food_workers: 27
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  8030
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 28
  count: 1
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 484
    player_relative: 1
    health: 66
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 41
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 123
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 13
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
  units {
    unit_type: 692
    player_relative: 1
    health: 113
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  6859
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
