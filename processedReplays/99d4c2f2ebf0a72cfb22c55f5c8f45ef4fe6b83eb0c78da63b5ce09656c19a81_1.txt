----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4339
  player_apm: 181
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4585
  player_apm: 165
}
game_duration_loops: 16091
game_duration_seconds: 718.39831543
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10843
score_details {
  idle_production_time: 986.75
  idle_worker_time: 570.0625
  total_value_units: 5250.0
  total_value_structures: 3850.0
  killed_value_units: 800.0
  killed_value_structures: 250.0
  collected_minerals: 8660.0
  collected_vespene: 2308.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 604.0
  spent_minerals: 8700.0
  spent_vespene: 2300.0
  food_used {
    none: 0.0
    army: 33.0
    economy: 46.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 350.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1800.0
    economy: 4900.0
    technology: 1450.0
    upgrade: 700.0
  }
  used_vespene {
    none: 0.0
    army: 875.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_used_minerals {
    none: 0.0
    army: 1900.0
    economy: 4450.0
    technology: 1450.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 400.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 2047.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 310.0
    shields: 439.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 10
vespene: 8
food_cap: 102
food_used: 79
food_army: 33
food_workers: 45
idle_worker_count: 4
army_count: 15
warp_gate_count: 3

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 311
  count: 8
}
production {
  unit {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
  build_queue {
    unit_type: 81
    build_progress: 0.572499990463
  }
  build_queue {
    unit_type: 4
    build_progress: 0.0
  }
}

score{
 score_type: Melee
score: 20978
score_details {
  idle_production_time: 3404.8125
  idle_worker_time: 1866.25
  total_value_units: 14050.0
  total_value_structures: 5900.0
  killed_value_units: 6950.0
  killed_value_structures: 550.0
  collected_minerals: 18205.0
  collected_vespene: 5248.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 671.0
  spent_minerals: 17825.0
  spent_vespene: 4525.0
  food_used {
    none: 0.0
    army: 109.0
    economy: 59.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2700.0
    economy: 3400.0
    technology: 350.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 950.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2000.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 5875.0
    economy: 6550.0
    technology: 2650.0
    upgrade: 1000.0
  }
  used_vespene {
    none: 0.0
    army: 2200.0
    economy: 0.0
    technology: 550.0
    upgrade: 1000.0
  }
  total_used_minerals {
    none: 0.0
    army: 7875.0
    economy: 6500.0
    technology: 2200.0
    upgrade: 700.0
  }
  total_used_vespene {
    none: 0.0
    army: 2975.0
    economy: 0.0
    technology: 400.0
    upgrade: 700.0
  }
  total_damage_dealt {
    life: 10467.1542969
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2898.54003906
    shields: 4045.66503906
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 430
vespene: 723
food_cap: 181
food_used: 168
food_army: 109
food_workers: 59
idle_worker_count: 8
army_count: 46
warp_gate_count: 7

game_loop:  16091
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 2
  leader_unit_type: 71
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 74
  count: 29
}
groups {
  control_group_index: 4
  leader_unit_type: 81
  count: 1
}
multi {
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 34
    shields: 7
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 70
    energy: 0
  }
  units {
    unit_type: 311
    player_relative: 1
    health: 70
    shields: 51
    energy: 0
  }
}

Score:  20978
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
