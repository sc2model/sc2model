----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 47
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 107
}
game_duration_loops: 339
game_duration_seconds: 15.1349849701
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1220
score_details {
  idle_production_time: 60.875
  idle_worker_time: 0.0
  total_value_units: 750.0
  total_value_structures: 300.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 120.0
  collected_vespene: 0.0
  collection_rate_minerals: 671.0
  collection_rate_vespene: 0.0
  spent_minerals: 100.0
  spent_vespene: 0.0
  food_used {
    none: 0.0
    army: 0.0
    economy: 14.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 0.0
    economy: 1150.0
    technology: 0.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 0.0
    economy: 1100.0
    technology: 0.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 70
vespene: 0
food_cap: 14
food_used: 14
food_army: 0
food_workers: 13
idle_worker_count: 0
army_count: 0
warp_gate_count: 0

game_loop:  339
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 86
  count: 1
}
single {
  unit {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

Score:  1220
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
