----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3063
  player_apm: 110
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 3297
  player_apm: 72
}
game_duration_loops: 23314
game_duration_seconds: 1040.8762207
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 6314
score_details {
  idle_production_time: 1509.9375
  idle_worker_time: 331.1875
  total_value_units: 2375.0
  total_value_structures: 3650.0
  killed_value_units: 250.0
  killed_value_structures: 0.0
  collected_minerals: 6560.0
  collected_vespene: 904.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 156.0
  spent_minerals: 5800.0
  spent_vespene: 625.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 22.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 225.0
    economy: 650.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 675.0
    economy: 2150.0
    technology: 1850.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 350.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 700.0
    economy: 3100.0
    technology: 1700.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 100.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 300.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2507.37548828
    shields: 2588.32202148
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 810
vespene: 279
food_cap: 55
food_used: 35
food_army: 13
food_workers: 22
idle_worker_count: 0
army_count: 6
warp_gate_count: 4

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 84
  count: 1
}
single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 7852
score_details {
  idle_production_time: 4330.25
  idle_worker_time: 594.625
  total_value_units: 6825.0
  total_value_structures: 5300.0
  killed_value_units: 3150.0
  killed_value_structures: 0.0
  collected_minerals: 10385.0
  collected_vespene: 2892.0
  collection_rate_minerals: 391.0
  collection_rate_vespene: 156.0
  spent_minerals: 10400.0
  spent_vespene: 2275.0
  food_used {
    none: 0.0
    army: 13.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1250.0
    economy: 1650.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 3525.0
    economy: 750.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 850.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 675.0
    economy: 2150.0
    technology: 2750.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 4075.0
    economy: 3300.0
    technology: 2900.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1500.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 3127.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4797.37548828
    shields: 5138.69726562
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 35
vespene: 617
food_cap: 63
food_used: 33
food_army: 13
food_workers: 20
idle_worker_count: 0
army_count: 7
warp_gate_count: 6

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 59
  count: 1
}
multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 11
    shields: 22
    energy: 0
    build_progress: 0.1875
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 7216
score_details {
  idle_production_time: 5573.0
  idle_worker_time: 594.625
  total_value_units: 7825.0
  total_value_structures: 5300.0
  killed_value_units: 4200.0
  killed_value_structures: 500.0
  collected_minerals: 11095.0
  collected_vespene: 3296.0
  collection_rate_minerals: 391.0
  collection_rate_vespene: 179.0
  spent_minerals: 11025.0
  spent_vespene: 2900.0
  food_used {
    none: 0.0
    army: 9.0
    economy: 20.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1400.0
    economy: 2550.0
    technology: 500.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4400.0
    economy: 750.0
    technology: 600.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1725.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 425.0
    economy: 2150.0
    technology: 2750.0
    upgrade: 250.0
  }
  used_vespene {
    none: 0.0
    army: 525.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_used_minerals {
    none: 0.0
    army: 4575.0
    economy: 3300.0
    technology: 2900.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 350.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 5366.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5077.37548828
    shields: 5704.19726562
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 120
vespene: 396
food_cap: 63
food_used: 29
food_army: 9
food_workers: 20
idle_worker_count: 0
army_count: 5
warp_gate_count: 6

game_loop:  23314
ui_data{
 multi {
  units {
    unit_type: 76
    player_relative: 1
    health: 24
    shields: 48
    energy: 0
    build_progress: 0.5546875
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 23
    shields: 47
    energy: 0
    build_progress: 0.5390625
  }
}

Score:  7216
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
