----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3107
  player_apm: 68
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3550
  player_apm: 167
}
game_duration_loops: 26778
game_duration_seconds: 1195.52990723
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11028
score_details {
  idle_production_time: 1460.5
  idle_worker_time: 43.0
  total_value_units: 4800.0
  total_value_structures: 3450.0
  killed_value_units: 1450.0
  killed_value_structures: 0.0
  collected_minerals: 9280.0
  collected_vespene: 2460.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 671.0
  spent_minerals: 7262.0
  spent_vespene: 1400.0
  food_used {
    none: 0.0
    army: 24.0
    economy: 44.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 900.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 800.0
    economy: -75.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1450.0
    economy: 3900.0
    technology: 1400.0
    upgrade: 150.0
  }
  used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_used_minerals {
    none: 0.0
    army: 2000.0
    economy: 3900.0
    technology: 1400.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 350.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 2011.94580078
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 560.0
    shields: 712.125
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 2068
vespene: 1060
food_cap: 78
food_used: 68
food_army: 24
food_workers: 44
idle_worker_count: 0
army_count: 10
warp_gate_count: 5

game_loop:  10000
ui_data{
 single {
  unit {
    unit_type: 84
    player_relative: 1
    health: 20
    shields: 20
    energy: 0
  }
}

score{
 score_type: Melee
score: 19661
score_details {
  idle_production_time: 5542.1875
  idle_worker_time: 1579.6875
  total_value_units: 19975.0
  total_value_structures: 6650.0
  killed_value_units: 7250.0
  killed_value_structures: 0.0
  collected_minerals: 22045.0
  collected_vespene: 8228.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 806.0
  spent_minerals: 21812.0
  spent_vespene: 6325.0
  food_used {
    none: 0.0
    army: 76.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 4225.0
    economy: 2250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 8075.0
    economy: -75.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 3375.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4300.0
    economy: 6750.0
    technology: 2350.0
    upgrade: 1050.0
  }
  used_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 450.0
    upgrade: 1050.0
  }
  total_used_minerals {
    none: 0.0
    army: 12875.0
    economy: 6750.0
    technology: 2350.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 5550.0
    economy: 0.0
    technology: 450.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 10616.3369141
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5120.0
    shields: 6123.875
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 283
vespene: 1903
food_cap: 200
food_used: 134
food_army: 76
food_workers: 58
idle_worker_count: 4
army_count: 29
warp_gate_count: 8

game_loop:  20000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 74
  count: 20
}
multi {
  units {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
  units {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
}

score{
 score_type: Melee
score: 16274
score_details {
  idle_production_time: 8984.5
  idle_worker_time: 3588.1875
  total_value_units: 28725.0
  total_value_structures: 7250.0
  killed_value_units: 19300.0
  killed_value_structures: 0.0
  collected_minerals: 30510.0
  collected_vespene: 11976.0
  collection_rate_minerals: 1203.0
  collection_rate_vespene: 671.0
  spent_minerals: 28662.0
  spent_vespene: 11575.0
  food_used {
    none: 0.0
    army: 22.0
    economy: 39.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 13100.0
    economy: 2250.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 3950.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 15875.0
    economy: 1475.0
    technology: -113.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 8175.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1125.0
    economy: 5950.0
    technology: 2500.0
    upgrade: 1350.0
  }
  used_vespene {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 650.0
    upgrade: 1350.0
  }
  total_used_minerals {
    none: 0.0
    army: 18500.0
    economy: 7100.0
    technology: 2500.0
    upgrade: 1350.0
  }
  total_used_vespene {
    none: 0.0
    army: 11725.0
    economy: 0.0
    technology: 650.0
    upgrade: 1350.0
  }
  total_damage_dealt {
    life: 22662.9023438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11974.1894531
    shields: 15289.7871094
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1898
vespene: 401
food_cap: 200
food_used: 61
food_army: 22
food_workers: 39
idle_worker_count: 6
army_count: 6
warp_gate_count: 8

game_loop:  26778
ui_data{
 multi {
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 104
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 133
    player_relative: 1
    health: 500
    shields: 238
    energy: 0
  }
}

Score:  16274
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
