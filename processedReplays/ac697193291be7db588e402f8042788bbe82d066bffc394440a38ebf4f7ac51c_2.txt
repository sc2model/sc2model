----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 2929
  player_apm: 83
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 51
}
game_duration_loops: 10965
game_duration_seconds: 489.54309082
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9538
score_details {
  idle_production_time: 2207.8125
  idle_worker_time: 358.9375
  total_value_units: 4100.0
  total_value_structures: 3650.0
  killed_value_units: 0.0
  killed_value_structures: 0.0
  collected_minerals: 6450.0
  collected_vespene: 2088.0
  collection_rate_minerals: 1343.0
  collection_rate_vespene: 313.0
  spent_minerals: 5925.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1675.0
    economy: 3100.0
    technology: 1800.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 250.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1425.0
    economy: 3050.0
    technology: 1800.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1225.0
    economy: 0.0
    technology: 250.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 575
vespene: 63
food_cap: 70
food_used: 58
food_army: 28
food_workers: 29
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 62
  count: 3
}
multi {
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
  units {
    unit_type: 62
    player_relative: 1
    health: 500
    shields: 500
    energy: 0
  }
}

score{
 score_type: Melee
score: 8329
score_details {
  idle_production_time: 2670.875
  idle_worker_time: 358.9375
  total_value_units: 4700.0
  total_value_structures: 3650.0
  killed_value_units: 475.0
  killed_value_structures: 0.0
  collected_minerals: 7455.0
  collected_vespene: 2324.0
  collection_rate_minerals: 1427.0
  collection_rate_vespene: 335.0
  spent_minerals: 5925.0
  spent_vespene: 2025.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 31.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 475.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 425.0
    economy: 3150.0
    technology: 1800.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 225.0
    economy: 0.0
    technology: 250.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 1675.0
    economy: 3150.0
    technology: 1800.0
    upgrade: 300.0
  }
  total_used_vespene {
    none: 0.0
    army: 1475.0
    economy: 0.0
    technology: 250.0
    upgrade: 300.0
  }
  total_damage_dealt {
    life: 791.922851562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 400.0
    shields: 800.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1580
vespene: 299
food_cap: 70
food_used: 39
food_army: 8
food_workers: 31
idle_worker_count: 0
army_count: 4
warp_gate_count: 0

game_loop:  10965
ui_data{
 
Score:  8329
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
