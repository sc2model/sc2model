----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3545
  player_apm: 130
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3604
  player_apm: 62
}
game_duration_loops: 13009
game_duration_seconds: 580.799438477
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13737
score_details {
  idle_production_time: 1428.75
  idle_worker_time: 60.375
  total_value_units: 6325.0
  total_value_structures: 5300.0
  killed_value_units: 675.0
  killed_value_structures: 0.0
  collected_minerals: 10515.0
  collected_vespene: 2672.0
  collection_rate_minerals: 2071.0
  collection_rate_vespene: 671.0
  spent_minerals: 10175.0
  spent_vespene: 2200.0
  food_used {
    none: 0.0
    army: 30.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1875.0
    economy: 5550.0
    technology: 2600.0
    upgrade: 650.0
  }
  used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 350.0
    upgrade: 650.0
  }
  total_used_minerals {
    none: 0.0
    army: 2375.0
    economy: 5550.0
    technology: 2150.0
    upgrade: 400.0
  }
  total_used_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 350.0
    upgrade: 400.0
  }
  total_damage_dealt {
    life: 1158.04785156
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 515.0
    shields: 304.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 390
vespene: 472
food_cap: 149
food_used: 85
food_army: 30
food_workers: 55
idle_worker_count: 0
army_count: 14
warp_gate_count: 8

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 136
    player_relative: 1
    health: 80
    shields: 100
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 80
    energy: 0
  }
}

score{
 score_type: Melee
score: 18331
score_details {
  idle_production_time: 2514.125
  idle_worker_time: 60.375
  total_value_units: 11425.0
  total_value_structures: 5900.0
  killed_value_units: 3925.0
  killed_value_structures: 425.0
  collected_minerals: 16005.0
  collected_vespene: 4176.0
  collection_rate_minerals: 2519.0
  collection_rate_vespene: 895.0
  spent_minerals: 14600.0
  spent_vespene: 3725.0
  food_used {
    none: 0.0
    army: 58.0
    economy: 66.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2650.0
    economy: 850.0
    technology: 425.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3050.0
    economy: 6250.0
    technology: 2900.0
    upgrade: 850.0
  }
  used_vespene {
    none: 0.0
    army: 2175.0
    economy: 0.0
    technology: 350.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 0.0
    army: 6650.0
    economy: 6250.0
    technology: 2600.0
    upgrade: 650.0
  }
  total_used_vespene {
    none: 0.0
    army: 4175.0
    economy: 0.0
    technology: 350.0
    upgrade: 650.0
  }
  total_damage_dealt {
    life: 6514.59082031
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1954.0
    shields: 1842.25
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1455
vespene: 451
food_cap: 149
food_used: 124
food_army: 58
food_workers: 66
idle_worker_count: 0
army_count: 20
warp_gate_count: 11

game_loop:  13009
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
multi {
  units {
    unit_type: 73
    player_relative: 1
    health: 73
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 136
    player_relative: 1
    health: 80
    shields: 100
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 4
    shields: 0
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 50
    energy: 0
  }
  units {
    unit_type: 73
    player_relative: 1
    health: 100
    shields: 19
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 76
    player_relative: 1
    health: 40
    shields: 49
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 350
    energy: 0
  }
  units {
    unit_type: 141
    player_relative: 1
    health: 10
    shields: 332
    energy: 0
  }
}

Score:  18331
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
