----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3397
  player_apm: 82
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 3371
  player_apm: 61
}
game_duration_loops: 10293
game_duration_seconds: 459.540985107
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 7060
score_details {
  idle_production_time: 1255.75
  idle_worker_time: 256.0625
  total_value_units: 4900.0
  total_value_structures: 3450.0
  killed_value_units: 1700.0
  killed_value_structures: 0.0
  collected_minerals: 7885.0
  collected_vespene: 2000.0
  collection_rate_minerals: 1007.0
  collection_rate_vespene: 335.0
  spent_minerals: 7225.0
  spent_vespene: 1550.0
  food_used {
    none: 0.0
    army: 8.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 900.0
    technology: 550.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 350.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 450.0
    economy: 3325.0
    technology: 1050.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 275.0
    economy: 0.0
    technology: 100.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 4600.0
    technology: 1200.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1512.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 5236.98925781
    shields: 6677.765625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 710
vespene: 450
food_cap: 63
food_used: 50
food_army: 8
food_workers: 42
idle_worker_count: 2
army_count: 3
warp_gate_count: 1

game_loop:  10000
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 311
  count: 1
}
groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 4
  leader_unit_type: 59
  count: 1
}
single {
  unit {
    unit_type: 311
    player_relative: 1
    health: 67
    shields: 70
    energy: 0
  }
}

score{
 score_type: Melee
score: 6782
score_details {
  idle_production_time: 1292.375
  idle_worker_time: 292.6875
  total_value_units: 4900.0
  total_value_structures: 3450.0
  killed_value_units: 1700.0
  killed_value_structures: 0.0
  collected_minerals: 8085.0
  collected_vespene: 2072.0
  collection_rate_minerals: 895.0
  collection_rate_vespene: 335.0
  spent_minerals: 7325.0
  spent_vespene: 1575.0
  food_used {
    none: 0.0
    army: 10.0
    economy: 42.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1300.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1000.0
    economy: 900.0
    technology: 1000.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 450.0
    economy: 0.0
    technology: 450.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 100.0
    economy: 500.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 550.0
    economy: 3325.0
    technology: 600.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 0.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 1550.0
    economy: 4600.0
    technology: 1200.0
    upgrade: 150.0
  }
  total_used_vespene {
    none: 0.0
    army: 750.0
    economy: 0.0
    technology: 250.0
    upgrade: 150.0
  }
  total_damage_dealt {
    life: 1512.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 6421.98925781
    shields: 7207.640625
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 810
vespene: 497
food_cap: 63
food_used: 52
food_army: 10
food_workers: 42
idle_worker_count: 2
army_count: 4
warp_gate_count: 1

game_loop:  10293
ui_data{
 
Score:  6782
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
