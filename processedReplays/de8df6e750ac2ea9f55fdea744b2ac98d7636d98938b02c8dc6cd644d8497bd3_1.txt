----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5388
  player_apm: 319
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5328
  player_apm: 310
}
game_duration_loops: 21558
game_duration_seconds: 962.477905273
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 11581
score_details {
  idle_production_time: 7684.25
  idle_worker_time: 10.125
  total_value_units: 7900.0
  total_value_structures: 1900.0
  killed_value_units: 1025.0
  killed_value_structures: 0.0
  collected_minerals: 9070.0
  collected_vespene: 2048.0
  collection_rate_minerals: 1679.0
  collection_rate_vespene: 627.0
  spent_minerals: 8987.0
  spent_vespene: 1875.0
  food_used {
    none: 0.0
    army: 60.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 850.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 75.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 175.0
    economy: 0.0
    technology: -163.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 25.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3275.0
    economy: 4700.0
    technology: 1425.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1125.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 3400.0
    economy: 5350.0
    technology: 1150.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 1275.0
    economy: 0.0
    technology: 450.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1528.12841797
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 920.819335938
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 133
vespene: 173
food_cap: 114
food_used: 103
food_army: 60
food_workers: 43
idle_worker_count: 0
army_count: 38
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 3
}
groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 7
}
groups {
  control_group_index: 2
  leader_unit_type: 9
  count: 8
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 4
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 151
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18737
score_details {
  idle_production_time: 26527.375
  idle_worker_time: 113.0
  total_value_units: 27750.0
  total_value_structures: 2850.0
  killed_value_units: 17600.0
  killed_value_structures: 0.0
  collected_minerals: 26410.0
  collected_vespene: 7964.0
  collection_rate_minerals: 2855.0
  collection_rate_vespene: 806.0
  spent_minerals: 25737.0
  spent_vespene: 7700.0
  food_used {
    none: 0.0
    army: 123.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12300.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 4500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 11125.0
    economy: 100.0
    technology: -163.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 4300.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4450.0
    economy: 8325.0
    technology: 1425.0
    upgrade: 600.0
  }
  used_vespene {
    none: 0.0
    army: 2000.0
    economy: 0.0
    technology: 350.0
    upgrade: 600.0
  }
  total_used_minerals {
    none: 0.0
    army: 17250.0
    economy: 9125.0
    technology: 1575.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 7550.0
    economy: 0.0
    technology: 450.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 26198.8691406
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 21803.9277344
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 723
vespene: 264
food_cap: 190
food_used: 190
food_army: 123
food_workers: 67
idle_worker_count: 0
army_count: 47
warp_gate_count: 0

game_loop:  20000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 48
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}
multi {
  units {
    unit_type: 110
    player_relative: 1
    health: 122
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 102
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 136
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 76
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 25
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 128
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 78
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 72
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 84
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 101
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 138
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 33
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 103
    player_relative: 1
    health: 200
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 52
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 688
    player_relative: 1
    health: 120
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
  units {
    unit_type: 110
    player_relative: 1
    health: 145
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 18626
score_details {
  idle_production_time: 27706.3125
  idle_worker_time: 113.0
  total_value_units: 31650.0
  total_value_structures: 2850.0
  killed_value_units: 23450.0
  killed_value_structures: 0.0
  collected_minerals: 29655.0
  collected_vespene: 8908.0
  collection_rate_minerals: 2827.0
  collection_rate_vespene: 806.0
  spent_minerals: 28962.0
  spent_vespene: 8875.0
  food_used {
    none: 0.0
    army: 121.0
    economy: 67.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 16550.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 6100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 14250.0
    economy: 100.0
    technology: -163.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 5475.0
    economy: 0.0
    technology: -150.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 200.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 4400.0
    economy: 8325.0
    technology: 1425.0
    upgrade: 750.0
  }
  used_vespene {
    none: 0.0
    army: 1850.0
    economy: 0.0
    technology: 350.0
    upgrade: 750.0
  }
  total_used_minerals {
    none: 0.0
    army: 20025.0
    economy: 9325.0
    technology: 1575.0
    upgrade: 600.0
  }
  total_used_vespene {
    none: 0.0
    army: 8475.0
    economy: 0.0
    technology: 450.0
    upgrade: 600.0
  }
  total_damage_dealt {
    life: 34959.5273438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 27805.859375
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 743
vespene: 33
food_cap: 200
food_used: 188
food_army: 121
food_workers: 67
idle_worker_count: 0
army_count: 43
warp_gate_count: 0

game_loop:  21558
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 5
}
groups {
  control_group_index: 1
  leader_unit_type: 108
  count: 2
}
groups {
  control_group_index: 2
  leader_unit_type: 688
  count: 44
}
groups {
  control_group_index: 3
  leader_unit_type: 126
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 100
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 86
  count: 1
}

Score:  18626
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
