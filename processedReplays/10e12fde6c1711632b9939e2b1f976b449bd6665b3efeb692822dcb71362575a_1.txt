----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4581
  player_apm: 174
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4492
  player_apm: 193
}
game_duration_loops: 12328
game_duration_seconds: 550.395568848
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8806
score_details {
  idle_production_time: 997.8125
  idle_worker_time: 382.5625
  total_value_units: 5525.0
  total_value_structures: 2925.0
  killed_value_units: 1300.0
  killed_value_structures: 0.0
  collected_minerals: 8980.0
  collected_vespene: 1628.0
  collection_rate_minerals: 2099.0
  collection_rate_vespene: 313.0
  spent_minerals: 7800.0
  spent_vespene: 1425.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1050.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1700.0
    economy: 600.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 950.0
    economy: 4300.0
    technology: 950.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 325.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2300.0
    economy: 4700.0
    technology: 950.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 1373.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3527.57324219
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 176.621826172
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 1228
vespene: 203
food_cap: 86
food_used: 59
food_army: 18
food_workers: 39
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 1
}
groups {
  control_group_index: 7
  leader_unit_type: 28
  count: 1
}
groups {
  control_group_index: 8
  leader_unit_type: 27
  count: 1
}
groups {
  control_group_index: 9
  leader_unit_type: 21
  count: 3
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 36
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 30
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 12571
score_details {
  idle_production_time: 1636.375
  idle_worker_time: 576.125
  total_value_units: 8300.0
  total_value_structures: 4825.0
  killed_value_units: 1900.0
  killed_value_structures: 0.0
  collected_minerals: 12245.0
  collected_vespene: 2428.0
  collection_rate_minerals: 3331.0
  collection_rate_vespene: 627.0
  spent_minerals: 11600.0
  spent_vespene: 2325.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 55.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1800.0
    economy: 700.0
    technology: -114.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 600.0
    economy: 0.0
    technology: -114.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 300.0
    army: 2250.0
    economy: 5500.0
    technology: 1850.0
    upgrade: 300.0
  }
  used_vespene {
    none: 50.0
    army: 750.0
    economy: 0.0
    technology: 475.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 50.0
    army: 3800.0
    economy: 6650.0
    technology: 1850.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 1150.0
    economy: 0.0
    technology: 475.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1532.0
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4415.59228516
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 316.947021484
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 693
vespene: 103
food_cap: 117
food_used: 99
food_army: 44
food_workers: 55
idle_worker_count: 60
army_count: 31
warp_gate_count: 0

game_loop:  12328
ui_data{
 
Score:  12571
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
