----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4720
  player_apm: 201
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4815
  player_apm: 300
}
game_duration_loops: 8989
game_duration_seconds: 401.322662354
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 5869
score_details {
  idle_production_time: 787.0625
  idle_worker_time: 216.3125
  total_value_units: 4400.0
  total_value_structures: 2175.0
  killed_value_units: 1225.0
  killed_value_structures: 0.0
  collected_minerals: 6275.0
  collected_vespene: 1092.0
  collection_rate_minerals: 1651.0
  collection_rate_vespene: 67.0
  spent_minerals: 6125.0
  spent_vespene: 700.0
  food_used {
    none: 0.0
    army: 18.0
    economy: 26.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1175.0
    economy: 50.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 950.0
    economy: 1210.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 100.0
    army: 875.0
    economy: 3150.0
    technology: 750.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 1725.0
    economy: 4150.0
    technology: 600.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 325.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 2032.66552734
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3596.35644531
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 899.560546875
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 152
vespene: 392
food_cap: 46
food_used: 44
food_army: 18
food_workers: 24
idle_worker_count: 0
army_count: 9
warp_gate_count: 0

game_loop:  8989
ui_data{
 
Score:  5869
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
