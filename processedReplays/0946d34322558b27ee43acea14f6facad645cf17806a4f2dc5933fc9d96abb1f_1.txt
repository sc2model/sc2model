----------------------- Replay info ------------------------
map_name: "Ascension to Aiur LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3400
  player_apm: 86
}
player_info {
  player_info {
    player_id: 2
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 92
}
game_duration_loops: 16251
game_duration_seconds: 725.541687012
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8664
score_details {
  idle_production_time: 1156.3125
  idle_worker_time: 193.625
  total_value_units: 4550.0
  total_value_structures: 3125.0
  killed_value_units: 1100.0
  killed_value_structures: 0.0
  collected_minerals: 6655.0
  collected_vespene: 1436.0
  collection_rate_minerals: 923.0
  collection_rate_vespene: 380.0
  spent_minerals: 6475.0
  spent_vespene: 1250.0
  food_used {
    none: 0.0
    army: 46.0
    economy: 30.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 600.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 100.0
    economy: 300.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 2225.0
    economy: 3425.0
    technology: 1275.0
    upgrade: 100.0
  }
  used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 325.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 50.0
    army: 2125.0
    economy: 4025.0
    technology: 1075.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 50.0
    army: 625.0
    economy: 0.0
    technology: 325.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 1356.25585938
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1201.625
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 350.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 203
vespene: 186
food_cap: 78
food_used: 76
food_army: 46
food_workers: 30
idle_worker_count: 0
army_count: 20
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 7
  leader_unit_type: 132
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 27
  count: 1
}
single {
  unit {
    unit_type: 689
    player_relative: 1
    health: 180
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 5327
score_details {
  idle_production_time: 3192.3125
  idle_worker_time: 1061.375
  total_value_units: 7425.0
  total_value_structures: 4225.0
  killed_value_units: 4500.0
  killed_value_structures: 500.0
  collected_minerals: 10860.0
  collected_vespene: 2744.0
  collection_rate_minerals: 559.0
  collection_rate_vespene: 0.0
  spent_minerals: 10050.0
  spent_vespene: 1975.0
  food_used {
    none: 0.0
    army: 4.0
    economy: 13.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2975.0
    economy: 1000.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 825.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 4225.0
    economy: 2050.0
    technology: 852.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1200.0
    economy: 0.0
    technology: 125.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 800.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 150.0
    economy: 1625.0
    technology: 1050.0
    upgrade: 200.0
  }
  used_vespene {
    none: 50.0
    army: 50.0
    economy: 150.0
    technology: 200.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 50.0
    army: 4375.0
    economy: 4725.0
    technology: 1775.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 1250.0
    economy: 300.0
    technology: 325.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 7164.05859375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 19278.2832031
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 549.522949219
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 833
vespene: 769
food_cap: 31
food_used: 17
food_army: 4
food_workers: 13
idle_worker_count: 12
army_count: 2
warp_gate_count: 0

game_loop:  16251
ui_data{
 
Score:  5327
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
