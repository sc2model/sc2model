----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Random
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_apm: 47
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_apm: 51
}
game_duration_loops: 6455
game_duration_seconds: 288.189758301
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4477
score_details {
  idle_production_time: 2248.0625
  idle_worker_time: 76.0
  total_value_units: 2100.0
  total_value_structures: 1075.0
  killed_value_units: 50.0
  killed_value_structures: 0.0
  collected_minerals: 3350.0
  collected_vespene: 1008.0
  collection_rate_minerals: 419.0
  collection_rate_vespene: 313.0
  spent_minerals: 2781.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 4.5
    economy: 16.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 125.0
    economy: 481.0
    technology: -375.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 225.0
    economy: 1950.0
    technology: 575.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 150.0
    economy: 2800.0
    technology: 575.0
    upgrade: 100.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_damage_dealt {
    life: 239.392578125
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2066.81738281
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 619
vespene: 908
food_cap: 36
food_used: 20
food_army: 4
food_workers: 16
idle_worker_count: 16
army_count: 1
warp_gate_count: 0

game_loop:  6455
ui_data{
 
Score:  4477
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
