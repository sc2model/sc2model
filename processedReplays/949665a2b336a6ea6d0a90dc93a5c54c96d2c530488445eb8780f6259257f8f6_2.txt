----------------------- Replay info ------------------------
map_name: "Odyssey LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 4172
  player_apm: 225
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4093
  player_apm: 141
}
game_duration_loops: 10090
game_duration_seconds: 450.477874756
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 9070
score_details {
  idle_production_time: 7466.3125
  idle_worker_time: 5.875
  total_value_units: 6700.0
  total_value_structures: 1750.0
  killed_value_units: 1125.0
  killed_value_structures: 150.0
  collected_minerals: 9170.0
  collected_vespene: 1356.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 470.0
  spent_minerals: 7456.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 26.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 875.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1225.0
    economy: 681.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 125.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 975.0
    economy: 3875.0
    technology: 1025.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 325.0
    economy: 0.0
    technology: 150.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 5025.0
    technology: 1325.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2024.02880859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4402.24023438
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1764
vespene: 356
food_cap: 76
food_used: 67
food_army: 26
food_workers: 40
idle_worker_count: 0
army_count: 13
warp_gate_count: 0
larva_count: 6

game_loop:  10000
ui_data{
 groups {
  control_group_index: 0
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 1
  leader_unit_type: 110
  count: 13
}
groups {
  control_group_index: 5
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 7
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 8
  leader_unit_type: 86
  count: 2
}
groups {
  control_group_index: 9
  leader_unit_type: 86
  count: 2
}
multi {
  units {
    unit_type: 86
    player_relative: 1
    health: 1500
    shields: 0
    energy: 0
  }
  units {
    unit_type: 100
    player_relative: 1
    health: 2000
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 9136
score_details {
  idle_production_time: 7517.5
  idle_worker_time: 5.875
  total_value_units: 6800.0
  total_value_structures: 1750.0
  killed_value_units: 1125.0
  killed_value_structures: 150.0
  collected_minerals: 9300.0
  collected_vespene: 1392.0
  collection_rate_minerals: 1791.0
  collection_rate_vespene: 492.0
  spent_minerals: 7756.0
  spent_vespene: 1000.0
  food_used {
    none: 0.0
    army: 28.0
    economy: 41.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 875.0
    economy: 100.0
    technology: 150.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1300.0
    economy: 681.0
    technology: 150.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 150.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 200.0
    economy: 75.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 1200.0
    economy: 3875.0
    technology: 1025.0
    upgrade: 300.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 150.0
    upgrade: 300.0
  }
  total_used_minerals {
    none: 0.0
    army: 2550.0
    economy: 5125.0
    technology: 1325.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 250.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 2126.02880859
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 4638.02636719
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1594
vespene: 392
food_cap: 84
food_used: 69
food_army: 28
food_workers: 40
idle_worker_count: 0
army_count: 12
warp_gate_count: 0

game_loop:  10090
ui_data{
 
Score:  9136
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
