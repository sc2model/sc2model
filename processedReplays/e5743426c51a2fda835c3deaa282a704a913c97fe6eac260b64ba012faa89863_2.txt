----------------------- Replay info ------------------------
map_name: "Abyssal Reef LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 5165
  player_apm: 256
}
player_info {
  player_info {
    player_id: 2
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 5251
  player_apm: 90
}
game_duration_loops: 7115
game_duration_seconds: 317.656097412
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4643
score_details {
  idle_production_time: 711.9375
  idle_worker_time: 279.1875
  total_value_units: 3125.0
  total_value_structures: 1850.0
  killed_value_units: 850.0
  killed_value_structures: 100.0
  collected_minerals: 4315.0
  collected_vespene: 1328.0
  collection_rate_minerals: 811.0
  collection_rate_vespene: 358.0
  spent_minerals: 3925.0
  spent_vespene: 950.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 21.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 500.0
    economy: 350.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 100.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1400.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 400.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 625.0
    economy: 1900.0
    technology: 700.0
    upgrade: 50.0
  }
  used_vespene {
    none: 0.0
    army: 300.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_used_minerals {
    none: 0.0
    army: 1525.0
    economy: 2050.0
    technology: 700.0
    upgrade: 50.0
  }
  total_used_vespene {
    none: 0.0
    army: 500.0
    economy: 0.0
    technology: 200.0
    upgrade: 50.0
  }
  total_damage_dealt {
    life: 1186.82592773
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1268.41210938
    shields: 1086.3371582
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 440
vespene: 378
food_cap: 39
food_used: 32
food_army: 11
food_workers: 21
idle_worker_count: 0
army_count: 2
warp_gate_count: 0

game_loop:  7115
ui_data{
 multi {
  units {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 450
    energy: 0
  }
  units {
    unit_type: 71
    player_relative: 1
    health: 450
    shields: 374
    energy: 0
  }
}

Score:  4643
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
