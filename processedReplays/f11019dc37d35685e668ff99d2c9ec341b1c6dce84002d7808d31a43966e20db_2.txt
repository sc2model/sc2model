----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: -36400
  player_apm: 76
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 2790
  player_apm: 79
}
game_duration_loops: 12775
game_duration_seconds: 570.352294922
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 10243
score_details {
  idle_production_time: 1088.1875
  idle_worker_time: 117.1875
  total_value_units: 5300.0
  total_value_structures: 3125.0
  killed_value_units: 3450.0
  killed_value_structures: 450.0
  collected_minerals: 8230.0
  collected_vespene: 1688.0
  collection_rate_minerals: 1707.0
  collection_rate_vespene: 313.0
  spent_minerals: 7325.0
  spent_vespene: 1625.0
  food_used {
    none: 0.0
    army: 48.0
    economy: 40.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1475.0
    economy: 1675.0
    technology: 275.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 375.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 400.0
    economy: -250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 2400.0
    economy: 4150.0
    technology: 925.0
    upgrade: 200.0
  }
  used_vespene {
    none: 0.0
    army: 1000.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_used_minerals {
    none: 0.0
    army: 2450.0
    economy: 4400.0
    technology: 925.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 0.0
    army: 900.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 6089.70898438
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 1362.89501953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 824.40234375
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 955
vespene: 63
food_cap: 102
food_used: 88
food_army: 48
food_workers: 38
idle_worker_count: 1
army_count: 27
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 4
  leader_unit_type: 54
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 4
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 2
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 49
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 49
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 51
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 150
    shields: 0
    energy: 3
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
}

score{
 score_type: Melee
score: 13498
score_details {
  idle_production_time: 2128.5625
  idle_worker_time: 194.5625
  total_value_units: 7525.0
  total_value_structures: 3950.0
  killed_value_units: 4500.0
  killed_value_structures: 1450.0
  collected_minerals: 11400.0
  collected_vespene: 2648.0
  collection_rate_minerals: 1231.0
  collection_rate_vespene: 627.0
  spent_minerals: 10425.0
  spent_vespene: 2575.0
  food_used {
    none: 0.0
    army: 65.0
    economy: 43.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 2325.0
    economy: 2025.0
    technology: 725.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 575.0
    economy: 0.0
    technology: 300.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 1100.0
    economy: -250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3250.0
    economy: 4950.0
    technology: 1525.0
    upgrade: 350.0
  }
  used_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 425.0
    upgrade: 350.0
  }
  total_used_minerals {
    none: 0.0
    army: 3800.0
    economy: 4900.0
    technology: 1475.0
    upgrade: 250.0
  }
  total_used_vespene {
    none: 0.0
    army: 1525.0
    economy: 0.0
    technology: 375.0
    upgrade: 250.0
  }
  total_damage_dealt {
    life: 11176.2558594
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2265.58105469
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 1103.94482422
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 1025
vespene: 73
food_cap: 110
food_used: 108
food_army: 65
food_workers: 43
idle_worker_count: 0
army_count: 33
warp_gate_count: 0

game_loop:  12775
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 22
}
groups {
  control_group_index: 2
  leader_unit_type: 33
  count: 3
}
groups {
  control_group_index: 5
  leader_unit_type: 21
  count: 6
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 3
}
multi {
  units {
    unit_type: 48
    player_relative: 1
    health: 37
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 102
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 89
    shields: 0
    energy: 0
  }
  units {
    unit_type: 33
    player_relative: 1
    health: 175
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 44
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 51
    player_relative: 1
    health: 105
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 54
    player_relative: 1
    health: 134
    shields: 0
    energy: 109
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 55
    shields: 0
    energy: 0
  }
  units {
    unit_type: 48
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
}

Score:  13498
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
