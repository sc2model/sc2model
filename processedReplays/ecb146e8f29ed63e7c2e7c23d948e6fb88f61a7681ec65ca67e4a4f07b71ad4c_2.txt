----------------------- Replay info ------------------------
map_name: "Mech Depot LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 3894
  player_apm: 54
}
player_info {
  player_info {
    player_id: 2
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4095
  player_apm: 88
}
game_duration_loops: 5405
game_duration_seconds: 241.311477661
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 4480
score_details {
  idle_production_time: 122.4375
  idle_worker_time: 115.125
  total_value_units: 2100.0
  total_value_structures: 1350.0
  killed_value_units: 1225.0
  killed_value_structures: 0.0
  collected_minerals: 2995.0
  collected_vespene: 820.0
  collection_rate_minerals: 839.0
  collection_rate_vespene: 313.0
  spent_minerals: 2550.0
  spent_vespene: 400.0
  food_used {
    none: 0.0
    army: 11.0
    economy: 24.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 125.0
    economy: 1050.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 265.0
    economy: 52.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 111.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 550.0
    economy: 2400.0
    technology: 300.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 650.0
    economy: 2500.0
    technology: 300.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 200.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 989.0
    shields: 1788.0
    energy: 0.0
  }
  total_damage_taken {
    life: 509.292236328
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 75.1499023438
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 2
minerals: 471
vespene: 409
food_cap: 47
food_used: 35
food_army: 11
food_workers: 24
idle_worker_count: 0
army_count: 6
warp_gate_count: 0

game_loop:  5405
ui_data{
 groups {
  control_group_index: 1
  leader_unit_type: 48
  count: 8
}
groups {
  control_group_index: 2
  leader_unit_type: 21
  count: 2
}
groups {
  control_group_index: 4
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 1
}
groups {
  control_group_index: 6
  leader_unit_type: 132
  count: 1
}
multi {
  units {
    unit_type: 45
    player_relative: 1
    health: 40
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 35
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 45
    shields: 0
    energy: 0
  }
  units {
    unit_type: 45
    player_relative: 1
    health: 14
    shields: 0
    energy: 0
  }
}

Score:  4480
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
