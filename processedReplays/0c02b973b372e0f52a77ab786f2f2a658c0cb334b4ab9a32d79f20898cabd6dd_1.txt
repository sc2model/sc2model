----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Terran
    race_actual: Terran
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 5377
  player_apm: 320
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 6516
  player_apm: 639
}
game_duration_loops: 16074
game_duration_seconds: 717.639343262
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 13108
score_details {
  idle_production_time: 818.875
  idle_worker_time: 143.8125
  total_value_units: 5925.0
  total_value_structures: 4550.0
  killed_value_units: 775.0
  killed_value_structures: 0.0
  collected_minerals: 10640.0
  collected_vespene: 2268.0
  collection_rate_minerals: 2491.0
  collection_rate_vespene: 627.0
  spent_minerals: 10650.0
  spent_vespene: 1875.0
  food_used {
    none: 0.0
    army: 44.0
    economy: 60.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 575.0
    economy: 200.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 450.0
    economy: 250.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 50.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 150.0
    army: 2200.0
    economy: 6150.0
    technology: 1850.0
    upgrade: 500.0
  }
  used_vespene {
    none: 50.0
    army: 725.0
    economy: 0.0
    technology: 550.0
    upgrade: 500.0
  }
  total_used_minerals {
    none: 50.0
    army: 2250.0
    economy: 6600.0
    technology: 1500.0
    upgrade: 200.0
  }
  total_used_vespene {
    none: 50.0
    army: 575.0
    economy: 0.0
    technology: 350.0
    upgrade: 200.0
  }
  total_damage_dealt {
    life: 1812.57275391
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 921.0
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 40
vespene: 393
food_cap: 141
food_used: 104
food_army: 44
food_workers: 57
idle_worker_count: 0
army_count: 26
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 3
  leader_unit_type: 48
  count: 14
}
groups {
  control_group_index: 4
  leader_unit_type: 55
  count: 2
}
groups {
  control_group_index: 5
  leader_unit_type: 132
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 21
  count: 7
}
multi {
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 37
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 27
    player_relative: 1
    health: 1250
    shields: 0
    energy: 0
    add_on {
      unit_type: 39
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 28
    player_relative: 1
    health: 1300
    shields: 0
    energy: 0
    add_on {
      unit_type: 42
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      player_relative: 1
      health: 400
      shields: 0
      energy: 0
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      build_progress: 0.572499990463
    }
  }
  units {
    unit_type: 21
    player_relative: 1
    health: 1000
    shields: 0
    energy: 0
    add_on {
      unit_type: 38
      build_progress: 0.578750014305
    }
  }
}

score{
 score_type: Melee
score: 15531
score_details {
  idle_production_time: 3740.25
  idle_worker_time: 862.0625
  total_value_units: 15925.0
  total_value_structures: 7150.0
  killed_value_units: 6750.0
  killed_value_structures: 0.0
  collected_minerals: 22700.0
  collected_vespene: 5156.0
  collection_rate_minerals: 2155.0
  collection_rate_vespene: 582.0
  spent_minerals: 22425.0
  spent_vespene: 4225.0
  food_used {
    none: 0.0
    army: 34.0
    economy: 63.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5125.0
    economy: 850.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 775.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 9025.0
    economy: 0.0
    technology: 100.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1800.0
    economy: 0.0
    technology: 75.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 150.0
    economy: 400.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 50.0
    army: 1700.0
    economy: 6900.0
    technology: 2850.0
    upgrade: 850.0
  }
  used_vespene {
    none: 50.0
    army: 300.0
    economy: 0.0
    technology: 725.0
    upgrade: 850.0
  }
  total_used_minerals {
    none: 50.0
    army: 10325.0
    economy: 7650.0
    technology: 2950.0
    upgrade: 850.0
  }
  total_used_vespene {
    none: 50.0
    army: 2150.0
    economy: 0.0
    technology: 800.0
    upgrade: 850.0
  }
  total_damage_dealt {
    life: 11454.8046875
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11175.3876953
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 2210.453125
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 325
vespene: 931
food_cap: 200
food_used: 97
food_army: 34
food_workers: 63
idle_worker_count: 63
army_count: 12
warp_gate_count: 0

game_loop:  16074
ui_data{
 
Score:  15531
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
