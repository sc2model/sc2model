----------------------- Replay info ------------------------
map_name: "Acolyte LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Protoss
    race_actual: Protoss
  }
  player_result {
    player_id: 1
    result: Victory
  }
  player_mmr: 3649
  player_apm: 139
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Defeat
  }
  player_mmr: 4329
  player_apm: 141
}
game_duration_loops: 25638
game_duration_seconds: 1144.63342285
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 8987
score_details {
  idle_production_time: 936.375
  idle_worker_time: 442.75
  total_value_units: 4050.0
  total_value_structures: 4225.0
  killed_value_units: 1500.0
  killed_value_structures: 350.0
  collected_minerals: 8275.0
  collected_vespene: 1212.0
  collection_rate_minerals: 1847.0
  collection_rate_vespene: 470.0
  spent_minerals: 7575.0
  spent_vespene: 700.0
  food_used {
    none: 0.0
    army: 14.0
    economy: 48.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 1100.0
    economy: 450.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 250.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 0.0
    economy: 500.0
    technology: 1050.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 850.0
    economy: 4525.0
    technology: 1650.0
    upgrade: 0.0
  }
  used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_used_minerals {
    none: 0.0
    army: 850.0
    economy: 4475.0
    technology: 2250.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 550.0
    economy: 0.0
    technology: 150.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 4485.74609375
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 2670.0
    shields: 3781.99707031
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 750
vespene: 512
food_cap: 78
food_used: 62
food_army: 14
food_workers: 47
idle_worker_count: 1
army_count: 4
warp_gate_count: 0

game_loop:  10000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 2
}
groups {
  control_group_index: 6
  leader_unit_type: 62
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 67
  count: 1
}
single {
  unit {
    unit_type: 80
    player_relative: 1
    health: 70
    shields: 100
    energy: 0
  }
}

score{
 score_type: Melee
score: 22085
score_details {
  idle_production_time: 5201.9375
  idle_worker_time: 1293.5
  total_value_units: 11960.0
  total_value_structures: 11825.0
  killed_value_units: 8250.0
  killed_value_structures: 350.0
  collected_minerals: 25425.0
  collected_vespene: 6620.0
  collection_rate_minerals: 1399.0
  collection_rate_vespene: 1007.0
  spent_minerals: 22475.0
  spent_vespene: 5475.0
  food_used {
    none: 0.0
    army: 56.0
    economy: 58.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 5325.0
    economy: 750.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 2425.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 2730.0
    economy: 2900.0
    technology: 3450.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 1550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 3590.0
    economy: 7050.0
    technology: 3000.0
    upgrade: 625.0
  }
  used_vespene {
    none: 0.0
    army: 2400.0
    economy: 0.0
    technology: 650.0
    upgrade: 625.0
  }
  total_used_minerals {
    none: 0.0
    army: 4960.0
    economy: 9425.0
    technology: 5850.0
    upgrade: 450.0
  }
  total_used_vespene {
    none: 0.0
    army: 2900.0
    economy: 0.0
    technology: 650.0
    upgrade: 450.0
  }
  total_damage_dealt {
    life: 9751.45996094
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 11788.4296875
    shields: 14227.2099609
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 3000
vespene: 1145
food_cap: 196
food_used: 114
food_army: 56
food_workers: 58
idle_worker_count: 14
army_count: 6
warp_gate_count: 3

game_loop:  20000
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 488
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 3
}
groups {
  control_group_index: 6
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 67
  count: 3
}
multi {
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 488
    player_relative: 1
    health: 130
    shields: 60
    energy: 111
    add_on {
      unit_type: 10
      build_progress: 0.838750004768
    }
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 114
    shields: 0
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
}

score{
 score_type: Melee
score: 29114
score_details {
  idle_production_time: 11026.25
  idle_worker_time: 1918.875
  total_value_units: 20210.0
  total_value_structures: 15825.0
  killed_value_units: 20700.0
  killed_value_structures: 1700.0
  collected_minerals: 31440.0
  collected_vespene: 11544.0
  collection_rate_minerals: 2043.0
  collection_rate_vespene: 1231.0
  spent_minerals: 31165.0
  spent_vespene: 8775.0
  food_used {
    none: 0.0
    army: 104.0
    economy: 61.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 12625.0
    economy: 3800.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 5875.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 5080.0
    economy: 3450.0
    technology: 4500.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 2550.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 6870.0
    economy: 8300.0
    technology: 3750.0
    upgrade: 875.0
  }
  used_vespene {
    none: 0.0
    army: 4400.0
    economy: 0.0
    technology: 950.0
    upgrade: 875.0
  }
  total_used_minerals {
    none: 0.0
    army: 10760.0
    economy: 11625.0
    technology: 7650.0
    upgrade: 875.0
  }
  total_used_vespene {
    none: 0.0
    army: 6250.0
    economy: 0.0
    technology: 950.0
    upgrade: 875.0
  }
  total_damage_dealt {
    life: 25720.3476562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 17299.6796875
    shields: 20720.2109375
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 325
vespene: 2769
food_cap: 200
food_used: 165
food_army: 104
food_workers: 61
idle_worker_count: 3
army_count: 13
warp_gate_count: 3

game_loop:  25638
ui_data{
 groups {
  control_group_index: 2
  leader_unit_type: 10
  count: 1
}
groups {
  control_group_index: 3
  leader_unit_type: 59
  count: 5
}
groups {
  control_group_index: 6
  leader_unit_type: 133
  count: 3
}
groups {
  control_group_index: 9
  leader_unit_type: 67
  count: 5
}
multi {
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 10
    player_relative: 1
    health: 350
    shields: 350
    energy: 64
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 114
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 135
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
  units {
    unit_type: 79
    player_relative: 1
    health: 250
    shields: 150
    energy: 0
  }
}

Score:  29114
Result:  [player_id: 1
result: Victory
, player_id: 2
result: Defeat
]
