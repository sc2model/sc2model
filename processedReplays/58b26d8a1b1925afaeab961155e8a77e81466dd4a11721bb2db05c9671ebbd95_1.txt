----------------------- Replay info ------------------------
map_name: "Interloper LE"
player_info {
  player_info {
    player_id: 1
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 1
    result: Defeat
  }
  player_mmr: 4574
  player_apm: 160
}
player_info {
  player_info {
    player_id: 2
    race_requested: Zerg
    race_actual: Zerg
  }
  player_result {
    player_id: 2
    result: Victory
  }
  player_mmr: 4567
  player_apm: 284
}
game_duration_loops: 5048
game_duration_seconds: 225.37286377
game_version: "3.16.1.55958"
data_build: 55958
base_build: 55958
data_version: "5BD7C31B44525DAB46E64C4602A81DC2"

------------------------------------------------------------
score{
 score_type: Melee
score: 1959
score_details {
  idle_production_time: 1377.375
  idle_worker_time: 0.5625
  total_value_units: 2350.0
  total_value_structures: 825.0
  killed_value_units: 400.0
  killed_value_structures: 0.0
  collected_minerals: 2320.0
  collected_vespene: 176.0
  collection_rate_minerals: 251.0
  collection_rate_vespene: 0.0
  spent_minerals: 2287.0
  spent_vespene: 100.0
  food_used {
    none: 0.0
    army: 3.5
    economy: 8.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_minerals {
    none: 0.0
    army: 300.0
    economy: 100.0
    technology: 0.0
    upgrade: 0.0
  }
  killed_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_minerals {
    none: 0.0
    army: 675.0
    economy: 812.0
    technology: 0.0
    upgrade: 0.0
  }
  lost_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_minerals {
    none: 0.0
    army: 0.0
    economy: 150.0
    technology: 0.0
    upgrade: 0.0
  }
  friendly_fire_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  used_minerals {
    none: 0.0
    army: 225.0
    economy: 1125.0
    technology: 250.0
    upgrade: 100.0
  }
  used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 100.0
  }
  total_used_minerals {
    none: 0.0
    army: 900.0
    economy: 2225.0
    technology: 250.0
    upgrade: 0.0
  }
  total_used_vespene {
    none: 0.0
    army: 0.0
    economy: 0.0
    technology: 0.0
    upgrade: 0.0
  }
  total_damage_dealt {
    life: 675.360351562
    shields: 0.0
    energy: 0.0
  }
  total_damage_taken {
    life: 3050.85058594
    shields: 0.0
    energy: 0.0
  }
  total_healed {
    life: 0.0
    shields: 0.0
    energy: 0.0
  }
}

player_common{
 player_id: 1
minerals: 83
vespene: 76
food_cap: 30
food_used: 11
food_army: 3
food_workers: 8
idle_worker_count: 0
army_count: 3
warp_gate_count: 0

game_loop:  5048
ui_data{
 single {
  unit {
    unit_type: 105
    player_relative: 1
    health: 20
    shields: 0
    energy: 0
  }
}

Score:  1959
Result:  [player_id: 1
result: Defeat
, player_id: 2
result: Victory
]
