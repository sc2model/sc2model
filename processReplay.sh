#!/bin/bash
#FILES=/media/vv/DATA/svn/statPaper/StarCraftII.2.3.16.1/Replays/*
FILES=$(find /media/thehedgeify/Data/svn/statpaper/humanReplays -type f | shuf -n 100000)
for f in $FILES
do
  # take action on each file. $f store current file name
  filename=$(basename "$f")
  terrcount=$(python /media/thehedgeify/Data/svn/s2protocol/s2protocol/s2_cli.py $f --metadata | grep -c "Terr")
  if [ "$terrcount" -eq "4" ]; then
    echo "Processing $f file..."
    python /media/thehedgeify/Data/svn/statpaper/replay.py --replay $f --step_mul 224 --norender --observed_player 1 >> processedReplaysD/${filename%%.*}_1.txt
    python /media/thehedgeify/Data/svn/statpaper/replay.py --replay $f --step_mul 224 --norender --observed_player 2 >> processedReplaysD/${filename%%.*}_2.txt
  fi
done

