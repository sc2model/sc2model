# normal header for including libraries as needed 
# without file dialog
library <- function(package, ..., character.only=FALSE){
    if (!character.only)
        package <- as.character(substitute(package))
    yesno <- require(package, ..., character.only = TRUE)
    if(!yesno){
        try(install.packages(package, dependencies=TRUE))
        yesno <- require(package, ..., character.only = TRUE)
    }
    invisible(yesno)
} 

library(BBmisc)
library(reticulate) 
library(tensorflow)


library(keras)
#install_keras()

lido=FALSE
source("loadData.R")
models = c("ann_model.R")
#ticks = seq(224, 152671, 224)
#ticks = seq(6720, 7300, 224)
ticks = c(13440)
#ticks = seq(6720, 26880, 224)


# total = loadData(which="terran", feature_select="ds" )
# # remove tied games
# total = total[(total$result!="Tie"),]
# # do more preprocessing
# total$result = as.factor(as.character(total$result))
# #print(nrow(total))
# total = normalize(total, method = "range", range = c(0, 1))
# #print(total)


# global control variables
repeats = 1
batchsize = 30
maxiteration = 200
trainSplit = 0.9



# transforms the provided data frame into 2 elems: a column vector for the given column (colname)
# that is seen as ground truth/result, and a data frame without this column
splitOffResultCol <- function(basedata, colname) {
	res <- basedata[,colname]
	features <- basedata[ , !(names(basedata) %in% colname)]	
	#print(head(res))
	#print(head(features))
	return(list(res, features))
}


###################### main program

hiddenLayerSize = 20
dropout = 0.3
timeWindow = 1
timesteps = timeWindow+1
lstmActivation='relu'
denseActivation='sigmoid'


accs = matrix(nrow=length(ticks), ncol=repeats)
#plot(0, xlim=c(min(ticks), max(ticks)), ylim=c(0,1))

for(t in 1:length(ticks)){
if(0==1) {
	ticks_req = seq(max(1, ticks[t]/224-timeWindow)*224, ticks[t], 224) 
    total = loadData(which="terran2", tick=ticks_req, feature_select ="ds", dataAgg="none", both=FALSE)
	# normalize
	total[,4:ncol(total)-1] = BBmisc::normalize(total[,4:ncol(total)-1], method = "range", range = c(0, 1))
	inputs = ncol(total)-2
	# outputs are one hot encoded
	outputs = 2
	
	# remove tied games
	total = total[(total$result!="Tie"),]
	# do more preprocessing
	total$result = as.factor(as.character(total$result))
	#print(nrow(total))
	gameset = unique(total$id)
	gamenum = length(gameset)
	# we have to reformat from 2D (time inlined) to 3D (id, tick, feature)
	mat3 = array( 0, dim=c(gamenum, timeWindow+1, inputs))
	# also make a result array
	truth = array( "Defeat", dim=c(gamenum, timeWindow+1))
		
	gamenum = 1
	# go over all games 
	incomplete = NULL
	for(game in gameset) {
		# for each game, go over the set of time points
		ticknum = 1
		for(tick in ticks_req) {		
			thisgame = total[(total$id==game),]	
			origline = thisgame[(thisgame$game_loop==tick),]	
			if( nrow(origline) == 1 ) {
				mat3[gamenum, ticknum, ] = as.numeric(as.vector(origline[2:(ncol(origline)-1)]))
				truth[gamenum, ticknum] = as.factor(unlist(origline[ncol(origline)]))
				#if( gamenum == 1 ) {
				#	encoded = truth[gamenum, ticknum]
				#	rawval = origline[ncol(origline)]
				#}				
			} else {
				if( is.null(incomplete) ) {
					incomplete = gamenum
				} else {
					incomplete = c(incomplete,gamenum)				
				}
				print(paste("warning! empty line found for game",game,", tick",tick))
			}
			ticknum = ticknum + 1
		}		
		gamenum = gamenum + 1
	}
	
	# just for checking:
	#print(truth)
	#print(incomplete)

	# remove incomplete games 
	incomplete = unique(incomplete)
	mat3 = mat3[setdiff(1:nrow(mat3),incomplete),,]
#for testing dense network break down into 2D	
#mat3 = mat3[,2,]	
	truth = truth[setdiff(1:nrow(truth),incomplete),]
	save(mat3,file="mat3preload")
	save(truth,file="truthpreload")
} else {
	load(file="mat3preload")
	load(file="truthpreload")
}
	
	
	print(paste("starting tick",ticks[t],"with",nrow(mat3),"data sets"))
	
	for(runs in 1:repeats) {
		print(paste("run",runs,"of",repeats,"started."))

		# split data set 
		data_train_idx<- sample(1:nrow(mat3), trainSplit*round(nrow(mat3)))
		data_test_idx <- setdiff(1:nrow(mat3),data_train_idx)		
		
		train = mat3[data_train_idx,,]
		test  = mat3[data_test_idx,,]
		
		# remove game_loop
		#train = mat3[data_train_idx,,2:inputs]
		#test  = mat3[data_test_idx,,2:inputs]
		
		#test = total[!(1:nrow(total) %in% trainsample),]
		#print(head(test))
		trainS = truth[data_train_idx,] 
		testS = truth[data_test_idx,]
		#print(testS)
		
		#y_train <- to_categorical(matrix(data=(as.integer(trainS)-1),nrow=nrow(trainS)), outputs)
		#y_train <- matrix(data=(as.integer(trainS)-1),nrow=nrow(trainS))
		y_train <- to_categorical(as.integer(trainS[,ncol(trainS)]) -1)
		x_train <- train  
		#y_test <- to_categorical(matrix(data=(as.integer(testS)-1), nrow=nrow(testS)), outputs)	
		#y_test <- matrix(data=(as.integer(testS)-1), nrow=nrow(testS))
		y_test <- to_categorical(as.integer(testS[,ncol(testS)]) -1)
		x_test <- test    

		
		# keras ANN
		if(0 == 0) {			
			# build model
			model <- keras_model_sequential() 
			model %>% 
				layer_flatten(input_shape=list(timesteps, inputs)) %>%
				#layer_gru(units = hiddenLayerSize, return_sequences=TRUE,
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_conv_1d(filters = hiddenLayerSize, kernel_size = timesteps, padding="same",
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_conv_1d(filters = hiddenLayerSize, kernel_size = timesteps, padding="same",
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_conv_1d(filters = hiddenLayerSize, kernel_size = timesteps, padding="same",
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_conv_1d(filters = hiddenLayerSize, kernel_size = timesteps, padding="same",
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_conv_1d(filters = hiddenLayerSize, kernel_size = timesteps, padding="same",
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_conv_1d(filters = hiddenLayerSize, kernel_size = timesteps, padding="same",
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_simple_rnn(units = hiddenLayerSize, return_sequences=TRUE,
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_simple_rnn(units = hiddenLayerSize, return_sequences=TRUE,
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 
				#layer_lstm(units = hiddenLayerSize, return_sequences=FALSE,
				#	input_shape=list(timesteps, inputs), activation=lstmActivation) %>% 			
				#layer_lstm(units = hiddenLayerSize, return_sequences=TRUE,  					input_shape=list(timesteps, inputs), activation=lstmActivation) %>% # returns a sequence of vectors of dimension hiddenLayerSize
				layer_dense(units = hiddenLayerSize, activation = denseActivation) %>% # returns a sequence of vectors of dimension 				
				#layer_lstm(units = hiddenLayerSize, return_sequences=TRUE, activation=lstmActivation) %>% # returns a sequence of vectors 
				#layer_lstm(units = hiddenLayerSize, return_sequences=TRUE, activation=lstmActivation) %>% # returns a sequence of vectors 
				#layer_lstm(units = hiddenLayerSize, return_sequences=TRUE, activation=lstmActivation) %>% # returns a sequence of vectors 
				#layer_lstm(units = hiddenLayerSize, activation=lstmActivation) %>%  # return a single vector of dim hiddenLayerSize
				#layer_lstm(units = hiddenLayerSize, return_sequences=TRUE) %>% # returns a sequence of vectors of dimension hiddenLayerSize
				#layer_lstm(units = hiddenLayerSize, return_sequences=FALSE)%>%   # return a single vector of dimension hiddenLayerSize
				#layer_dense(units = hiddenLayerSize, activation = 'relu', input_shape = c(inputs)) %>% 
				#layer_dropout(rate = dropout) %>% 
				layer_dense(units = hiddenLayerSize, activation = denseActivation) %>% 				
				#layer_dropout(rate = dropout) %>% 
				layer_dense(units = hiddenLayerSize, activation = denseActivation) %>% 				
				#layer_dropout(rate = dropout) %>% 
				#layer_dense(units = hiddenLayerSize, activation = denseActivation) %>% 				
				#layer_lstm(units = 32) %>%
				#layer_dense(units = hiddenLayerSize, activation = 'relu') %>%
				#layer_dropout(rate = dropout) %>%
				#layer_dense(units = hiddenLayerSize, activation = 'relu') %>%
				#layer_dropout(rate = dropout) %>%
				#layer_dense(units = hiddenLayerSize, activation = 'relu') %>%
				#layer_dense(units = hiddenLayerSize, activation = 'relu') %>%
				#layer_dropout(rate = dropout-0.1) %>%
				#layer_dense(units = hiddenLayerSize, activation = 'relu') %>%
				#layer_dropout(rate = dropout-0.05) %>%
				#layer_dense(units = hiddenLayerSize, activation = 'relu') %>%
				#layer_dropout(rate = dropout) %>%
				#layer_dense(units = hiddenLayerSize, activation = 'relu') %>%
				#layer_flatten() %>%
				layer_dropout(rate = dropout) %>%
				layer_dense(units = outputs, activation = 'softmax')		
			print(summary(model))

			# add optimizer
			model %>% compile(
				loss = 'categorical_crossentropy',
				optimizer = optimizer_adam(lr=0.001, beta_1=0.9, beta_2=0.999, decay=0.0), 
				#optimizer = optimizer_sgd(lr=0.1, momentum=0.0, decay=0.01, nesterov=FALSE),
				#optimizer_rmsprop(),
				metrics = c('accuracy')
			)
			# adam defaults: lr=0.001, beta_1=0.9, beta_2=0.999, decay=0.0
			
			print("training model")
			# train model		
			history <- model %>% fit(
				x = x_train, y = y_train, epochs = maxiteration, 
				batch_size = batchsize, validation_split = 0.1, verbose = 0
			)
			plot(history)
			
			perf <- model %>% evaluate(x_test, y_test)	
			#print(perf)	
		}	
		
		accs[t,runs] = perf$acc
		

	}	# for runs

	#lines(ticks, rowMeans(accs[t,]))				
} # for ticks

print(accs)
