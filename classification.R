# normal header for including libraries as needed 
# without file dialog
library <- function(package, ..., character.only=FALSE){
    if (!character.only)
        package <- as.character(substitute(package))
    yesno <- require(package, ..., character.only = TRUE)
    if(!yesno){
        try(install.packages(package, dependencies=TRUE))
        yesno <- require(package, ..., character.only = TRUE)
    }
    invisible(yesno)
} 

library(rpart)
library(rattle)
library(rpart.plot)
library(RColorBrewer)
library(randomForest)
library(partykit)
library(BBmisc)

library(reticulate) 
library(tensorflow)

#pyconfs = reticulate::py_discover_config("tensorflow")
#use_python(pyconfs)
## note for windows: python path can be forced by environment variable RETICULATE_PYTHON
# reticulate::use_python("PATH_TO_PYTHON") 
#path_to_python <- "C:\\usr\\WinPython-64bit-3.6.2.0Qt5\\python-3.6.2.amd64"
#path_to_python <- "D:\\Program Files\\WinPython-64bit-3.5.3.1Qt5\\python-3.5.3.amd64"
#use_python(path_to_python) 
#py_module_available("keras")
#py_config()

library(keras)




# global control variables
repeats = 5
batchsize = 10
maxiteration = 100


load("terran_games/processedData")
#attach("processedData")
#load("terran_games/replayData38976")

#finalData=replayDataT
newdata1 = merge(x=replayInfo[,c(1:6,11:14)], y=finalData[finalData$player_id==1,], by.x="id", by.y="game_id")
colnames(newdata1)[3] = "race"
colnames(newdata1)[4] = "result"
colnames(newdata1)[5] = "mmr"
colnames(newdata1)[6] = "apm"
newdata2 = merge(x=replayInfo[,c(1,2,7:14)], y=finalData[finalData$player_id==2,], by.x="id", by.y="game_id")
colnames(newdata2)[3] = "race"
colnames(newdata2)[4] = "result"
colnames(newdata2)[5] = "mmr"
colnames(newdata2)[6] = "apm"
newdata= rbind(newdata1, newdata2)

# remove tied games
#noties = total[(total$player1_result!="Tie"),]
total = newdata



# preprocess 
preprocess_ds = function(df) { 
  rows = which(df$game_loop<=(1344/2))
  rows = union(rows, which(df$result=='Tie'))
  ridx = c(1,2,3,5,6,7,8,9,10,11,13,27,32,37,39,40,42,45,47,49,50,51,52,54,55,57,59,60,61,62,67,69,72,77,79,83,86,89,99)
  ridx = union(ridx, which(apply(df, 2, sd)==0))
  
  new = df[-rows, -ridx]
  
  return(new)
}

preprocess_expert = function(df) {
  rows = which(df$game_loop<=(1344/2))
  rows = union(rows, which(df$result=='Tie'))
  columns = c("result", "total_value_units", "killed_value_units", "killed_value_structures", 
              "collection_rate_minerals", "food_used_army", "food_used_economy", 
              "killed_minerals_army", "killed_minerals_economy", "lost_minerals_army", 
              "lost_minerals_economy", "total_damage_dealt_life", "total_damage_taken_life", 
              "food_army", "food_workers", "minerals")
  new = subset(df, select = columns)
  new = new[-rows, ]
  
  return(new)
}




# computes and returns the accuracy rate of the given prediction (via predict)
# on the provided test data. colname is the name of the column in testdata that 
# was predicted
compAccuracy <- function(prediction, testdata, colname) {

	right = 0
	if( !is.factor(prediction)) {
		# decision tree case
		rows = nrow(prediction)
		if( nrow(testdata) != rows) {
			print("prediction and testdata sets have different lengths!")
			return(-1)
		}
		for(r in 1:rows) {
			classname = colnames(pred)[which.max(pred[r,])]			
			if(classname == testdata[r,colname]) {
				right = right + 1
				#print(paste(classname,testdata[r,colname],"right"))
			} else {
				#print(paste(classname,testdata[r,colname],"wrong"))			
			}
		}
	} else {
		# random forest etc.
		len = length(prediction)
		if( nrow(testdata) != len) {
			print("prediction and testdata sets have different lengths!")
			return(-1)
		}
		for(r in 1:len) {
			classname = pred[r]			
			if(classname == testdata[r,colname]) {
				right = right + 1
				#print(paste(classname,testdata[r,colname],"right"))
			} else {
				#print(paste(classname,testdata[r,colname],"wrong"))			
			}
		}	
		rows = len
	}
	
	return(right/rows)	
}

# transforms the provided data frame into 2 elems: a column vector for the given column (colname)
# that is seen as ground truth/result, and a data frame without this column
splitOffResultCol <- function(basedata, colname) {
	res <- basedata[,colname]
	features <- basedata[ , !(names(basedata) %in% colname)]	
	#print(head(res))
	#print(head(features))
	return(list(res, features))
}

# choose a configurable batch from the given data (given in the format returned by splitOffResultCol, 2 cols)
# the return format is the same, but with only size randomly selected lines
chooseBatch <- function(basedata, size) {
	rows = sample(nrow(basedata[[2]]), size)
	batchResult = basedata[[1]][rows]
	batchFeatures = basedata[[2]][rows,]	
	#print(batchResult)
	#print(batchFeatures)
	return(list(batchResult,batchFeatures))
}

# takes a result vector and encodes it in one hot encoding (one column for each possible
# classification result, only 1 of these is 1, all others zero)
toOneHot <- function(resVector){
	#print(resVector)
	classes = levels(as.factor(resVector))
	cols = length(classes)
	oneHotMat = matrix(data=0,nrow=length(resVector),ncol=cols)
	
	for(r in 1:length(resVector)) {
		colnum = which(levels(as.factor(resVector)) == resVector[r])
		oneHotMat[r,colnum] = 1
	}
	#print(resVector)
	#print(oneHotMat)	
	return(oneHotMat)
}

# initialize weights
weight_variable <- function(shape) {
  initial <- tf$truncated_normal(shape, stddev=0.1)
  tf$Variable(initial)
}

# initialize bias values
bias_variable <- function(shape) {
  initial <- tf$constant(0.1, shape=shape)
  tf$Variable(initial)
}


###################### main program

hiddenLayerSize = 100

# do more preprocessing
total = preprocess_ds(total)
total$result = as.factor(total$result)
total = normalize(total)
#print(nrow(total))

# statistics (make vector to store the single run results)
results = vector(mode="numeric", length=0)


for(runs in 1:repeats) {

	# split data set 
	trainsize = round(nrow(total)/2)
	trainsample = sample(nrow(total), size=trainsize, replace = FALSE, prob = NULL)
	#print(trainsample)

	train = total[trainsample,]
	test = total[!(1:nrow(total) %in% trainsample),]
	#print(head(test))

	form = paste("result ~",paste(colnames(total[,-1]), collapse="+"))
	trainS = splitOffResultCol(train,"result") 
	testS = splitOffResultCol(test,"result")
	
	# tensorflow ANN
	if(0 == 0) {	
		inputs = ncol(total)-1
		# outputs are one hot encoded
		outputs = 2
		
		x <- tf$placeholder(tf$float32, shape(NULL, inputs))
		W1 <- weight_variable(shape(inputs, hiddenLayerSize))
		b1 <- bias_variable(shape(hiddenLayerSize))
		hLayer1 <- tf$nn$relu(tf$matmul(x, W1) + b1)	

		W2 <- weight_variable(shape(hiddenLayerSize, hiddenLayerSize))
		b2 <- bias_variable(shape(hiddenLayerSize))
		hLayer2 <- tf$nn$relu(tf$matmul(hLayer1, W2) + b2)	
		
		W3 <- weight_variable(shape(hiddenLayerSize, outputs))	
		b3 <- tf$Variable(tf$zeros(shape(outputs)))
		y <- tf$nn$softmax(tf$matmul(hLayer2, W3) + b3)					
	
		# output ground truth
		y_ <- tf$placeholder(tf$float32, shape(NULL, outputs))
		cross_entropy <- tf$reduce_mean(-tf$reduce_sum(y_ * tf$log(y), reduction_indices=1L))
	
		train_step <- tf$train$AdamOptimizer(1e-4)$minimize(cross_entropy)
		
		init <- tf$global_variables_initializer()
		sess <- tf$Session()
		sess$run(init)

		# this is only for tensorflow accuracy computation
		correct_prediction <- tf$equal(tf$argmax(y, 1L), tf$argmax(y_, 1L))
		accuracy <- tf$reduce_mean(tf$cast(correct_prediction, tf$float32))

		# training	
		for (i in 1:maxiteration) {
			print(paste("training tensorflow NN, iteration",i,"of",maxiteration))
			batch <- chooseBatch(trainS,batchsize)			
			batch_xs <- as.matrix(batch[[2]])
			batch_ys <- toOneHot(batch[[1]])
			sess$run(train_step, feed_dict = dict(x = batch_xs, y_ = batch_ys))
		}		
		
		
		# this is only for tensorflow accuracy computation
		correct_prediction <- tf$equal(tf$argmax(y, 1L), tf$argmax(y_, 1L))
		accuracy <- tf$reduce_mean(tf$cast(correct_prediction, tf$float32))
		
		testcases = as.matrix(testS[[2]])
		testlabels = toOneHot(testS[[1]]) 
		testCases = as.matrix(trainS[[2]])    #as.matrix(testS[[2]])
		testAnswers = toOneHot(trainS[[1]])   #toOneHot(testS[[1]])
		acc = sess$run(accuracy, feed_dict=dict(x = testCases, y_ = testAnswers ))	
		
	}	
	
	# rpart and random forest
	if(0 == 1) {
#			fit <- randomForest(form, data = train)
	fit <- randomForest(result ~ 	
	#fit <- cforest(result ~ 
	game_loop+score+idle_production_time+idle_worker_time+total_value_units+total_value_structures+killed_value_units+killed_value_structures+collected_minerals+collected_vespene+collection_rate_minerals+collection_rate_vespene+spent_minerals+spent_vespene+spent_vespene+food_used_army+food_used_economy+killed_minerals_army+     killed_minerals_economy+killed_minerals_technology+killed_vespene_army+lost_minerals_army+lost_minerals_economy+lost_vespene_army+ friendly_fire_minerals_army+friendly_fire_vespene_army+used_minerals_army+used_minerals_economy+used_minerals_technology+used_minerals_upgrade+used_vespene_army+used_vespene_technology+used_vespene_upgrade+total_used_minerals_army+total_used_minerals_economy+total_used_minerals_technology+total_used_minerals_upgrade+total_used_vespene_army+total_used_vespene_technology+total_used_vespene_upgrade+total_damage_dealt_life+total_damage_taken_life+total_healed_life+minerals+vespene+food_cap+food_used+food_army+                
	food_workers+idle_worker_count+army_count, data=train, ntree=2000)
#	fit <- rpart(form, data = train, method = "class")
#print(fit)
	}

	if(0 == 1) {	
		fancyRpartPlot(fit)
		pdf( file="dtree.pdf", bg="white", width=6, height=6, onefile=FALSE,
				family="URWHelvetica" )
			fancyRpartPlot(fit)
		dev.off()		
	}

	if(0 == 1) {		
		pred = predict(fit,newdata=test)
		acc = compAccuracy(prediction=pred, testdata=test, "result")			
	} 
	
	results = c(results, acc)
}

print(results)
print(summary(results))				
				
				